def quiet_sh(cmd) {
    sh('#!/bin/sh -e\n' + cmd)
}

pipeline {
    agent {
        docker {
            image 'registry.jeroensteenbeeke.nl/maven:latest'
            label 'docker'
        }
    }

    triggers {
        upstream(upstreamProjects: 'andalite/master,lux', threshold: hudson.model.Result.SUCCESS)
        pollSCM('H/5 * * * *')
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
        disableConcurrentBuilds()
    }

    environment {
        MAVEN_DEPLOY_USER = credentials('MAVEN_DEPLOY_USER')
        MAVEN_DEPLOY_PASSWORD = credentials('MAVEN_DEPLOY_PASSWORD')
    }


    stages {
        stage('Cleanup') {
            steps {
                sh "for f in \$(find * | grep jacoco.exec\$); do rm \$f; done"
            }
        }
        stage('Maven') {
            steps {
                sh 'echo `git log -n 1 --pretty=format:"%h"` > '+ env.WORKSPACE +'/core/src/main/java/com/jeroensteenbeeke/hyperion/revision.txt'
                sh 'echo `git log -n 1 --pretty=format:"%s"` > '+ env.WORKSPACE +'/core/src/main/java/com/jeroensteenbeeke/hyperion/commit-title.txt'
                sh 'echo `git log -n 1 --pretty=format:"%b"` > '+ env.WORKSPACE +'/core/src/main/java/com/jeroensteenbeeke/hyperion/commit-notes.txt'
                sh 'mvn -B -U clean verify install -P-disable-slow-tests'
            }
        }
        stage('Coverage') {
            when {
                expression {
                    currentBuild.result == 'SUCCESS' || currentBuild.result == null
                }
            }

            steps {
                jacoco changeBuildStatus: true, exclusionPattern: '**/*_Query.class,**/*Test*.class,**/HelpMojo.class,**/cssparser/*.class', maximumBranchCoverage: '52', maximumClassCoverage: '64', maximumComplexityCoverage: '50', maximumInstructionCoverage: '66', maximumLineCoverage: '62', maximumMethodCoverage: '59'
            }
        }
        stage('Deploy') {
            when {
                expression {
                    (currentBuild.result == 'SUCCESS' || currentBuild.result == null) && (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'experimental')
                }
            }

            steps {
                script {
                    if (env.MAVEN_DEPLOY_USER == null || env.MAVEN_DEPLOY_USER.isEmpty()) {
                        error('Deployment user not set')
                    } else if (env.MAVEN_DEPLOY_PASSWORD == null || env.MAVEN_DEPLOY_PASSWORD.isEmpty()) {
                        error('Deployment password not set')
                    }
                }
                quiet_sh 'mkdir -p /home/jenkins/.m2'
                quiet_sh 'echo "<settings><servers><server><id>repo-jeroensteenbeeke-releases</id><username>'+ env.MAVEN_DEPLOY_USER +'</username><password>'+ env.MAVEN_DEPLOY_PASSWORD +'</password></server><server><id>repo-jeroensteenbeeke-snapshots</id><username>'+ env.MAVEN_DEPLOY_USER + '</username><password>'+ env.MAVEN_DEPLOY_PASSWORD +'</password></server></servers></settings>" > /home/jenkins/.m2/settings.xml'
                sh 'mvn deploy -s /home/jenkins/.m2/settings.xml -DskipTests=true'
            }
        }
    }

    post {
        always {
            script {
                if (currentBuild.result == null) {
                    currentBuild.result = 'SUCCESS'
                }
            }

            step([$class                  : 'Mailer',
                  notifyEveryUnstableBuild: true,
                  sendToIndividuals       : true,
                  recipients              : 'j.steenbeeke@gmail.com'
            ])
            junit allowEmptyResults: true, testResults: '**/target/test-reports/*.xml'
            checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '**/checkstyle-result.xml', unHealthy: ''

        }
    }

}
