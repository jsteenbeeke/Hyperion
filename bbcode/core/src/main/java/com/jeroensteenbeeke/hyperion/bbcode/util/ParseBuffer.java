/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

/**
 * Wrapper around StringBuilder for easy use within a parser
 */
public class ParseBuffer {
	private StringBuilder buffer;

	ParseBuffer() {
		buffer = new StringBuilder();
	}

	/**
	 * Clears the parse buffer, returning all remaining data
	 * @return The data still in the buffer
	 */
	public String clear() {
		String res = buffer.toString();
		buffer = new StringBuilder();

		return res;
	}

	/**
	 * Checks the amount of text in the buffer
	 * @return The length of the buffer
	 */
	public int length() {
		return buffer.length();
	}

	/**
	 * Adds the given text to the buffer
	 * @param data The data to add
	 */
	public void append(String data) {
		buffer.append(data);
	}
}
