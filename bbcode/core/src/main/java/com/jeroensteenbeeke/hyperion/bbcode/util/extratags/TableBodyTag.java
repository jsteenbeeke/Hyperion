/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast.TableBodyNode;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Tag representing a table body, rendered as {@code <tbody></tbody>}, that specifically requires
 * a given child tag (usually: {@code [tr][/tr]})
 */
public class TableBodyTag extends StrictAllowedChildTag {
	private final boolean showButton;

	/**
	 * Creates a new TableBodyTag, with the given table row tag, and the option to show a button
	 * @param tr The table row tag
	 * @param showButton Whether or not a button should be shown
	 */
	public TableBodyTag(TableRowTag tr, boolean showButton) {
		super("[tbody]", "[/tbody]", tr);
		this.showButton = showButton;
	}

	@Override
	public BBAstNode createNodeOnAccept(BBAstNode current) {
		return new TableBodyNode(current);
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		if (showButton) {
			return BBCodeJavaScriptHelper.createSimpleButton("tbody");
		}

		return Optional.empty();
	}
}
