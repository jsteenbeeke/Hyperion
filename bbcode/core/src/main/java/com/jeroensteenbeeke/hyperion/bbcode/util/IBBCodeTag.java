/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

/**
 * Representation of a BBCode tag. Implementations of this class can be used to create more tags
 */
public interface IBBCodeTag {
	/**
	 * Determines whether the given stream is in a state to start parsing this tag. Generally speaking, this method
	 * should check for the opening tag of whatever tag this class implements
	 * @param stream The stream to check. <b>This stream should not be modified. Do not use consumeToken calls in this
	 *                 method</b>
	 * @return {@code true} if this object can parse from the given stream, {@code false} otherwise.
	 */
	boolean canParse(BBTokenStream stream);

	/**
	 * Parses the tag defined by this class, adding the tag to the given node if successful
	 * @param parser The parser being used to parse the text
	 * @param current The current node (if any) any newly created node should be placed under
	 * @param buffer The buffer used to store "excess" data: text content that is strictly not part of the tag, but
	 *                  should be outputted nevertheless. You can generally ignore this parameter
	 *               and defer any text content inside your text to the parser object instead
	 * @return An ActionResult indicating either success or a reason for failure
	 */
	@Nonnull
	ActionResult parse(@Nonnull BBParser parser, @Nullable BBAstNode current, @Nonnull ParseBuffer buffer);

	/**
	 * Create a Javascript snippet for creating an editor button for this tag
	 * @return A Javascript call, or {@code null} if no such editor button is required.
	 * @see com.jeroensteenbeeke.hyperion.bbcode.util.extratags.BBCodeJavaScriptHelper
	 */
	@Nonnull
	Optional<String> getCreateButtonJavascript();

	/**
	 * Get the character sequence that terminates this tag
 	 * @return A String denoting the character sequence terminating this tag
	 */
	@Nonnull
	String getTerminator();
}
