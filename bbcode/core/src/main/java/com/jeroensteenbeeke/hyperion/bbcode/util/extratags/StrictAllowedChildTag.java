/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.google.common.base.Joiner;
import com.jeroensteenbeeke.hyperion.bbcode.util.*;
import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Base tag class that enforces the use of a limited number of child tags, and no others. Generally used to
 * implement HTML concepts such as lists and tables
 */
public abstract class StrictAllowedChildTag implements IBBCodeTag {
	private final String openTag;

	private final String closeTag;

	private final IBBCodeTag[] expectedListItemTags;

	/**
	 * Create a new tag with the specified open and close tags, and a number of tags that are
	 * allowed as child tags
	 * @param openTag The open tag of this tag
	 * @param closeTag The close tag of this tag
	 * @param firstExpectedTag The first (mandatory) child tag allowed
	 * @param additionalTags Any other child tags that are allowed
	 */
	protected StrictAllowedChildTag(@Nonnull String openTag, @Nonnull String closeTag,
									@Nonnull IBBCodeTag firstExpectedTag, @Nonnull IBBCodeTag... additionalTags) {
		this.openTag = openTag;
		this.closeTag = closeTag;

		this.expectedListItemTags = new IBBCodeTag[1 + additionalTags.length];
		this.expectedListItemTags[0] = firstExpectedTag;
		int i = 1;
		for (IBBCodeTag tag : additionalTags) {
			this.expectedListItemTags[i++] = tag;
		}
	}

	@Override
	public final boolean canParse(BBTokenStream stream) {
		return stream.peekTokens(openTag.length()).equals(openTag);
	}

	@Nonnull
	@Override
	public final ActionResult parse(@Nonnull BBParser parser, BBAstNode current,
									@Nonnull ParseBuffer buffer) {
		BBAstNode node = createNodeOnAccept(current);
		current.addChild(node);

		BBTokenStream tokenStream = parser.getTokenStream();
		tokenStream.consumeTokens(openTag.length());

		while (!tokenStream.peekTokens(closeTag.length()).equals(closeTag)) {
			String next = tokenStream.peekTokens(1);
			// Skip whitespace
			if (!next.isEmpty() && next.trim().isEmpty()) {
				tokenStream.consumeToken();
				continue;
			}

			IBBCodeTag target = null;

			for (IBBCodeTag tag : expectedListItemTags) {
				if (tag.canParse(tokenStream)) {
					target = tag;
					break;
				}
			}

			if (target == null) {
				if (next.startsWith("[")) {
					next = tokenStream.peekUntil(']').map(tag -> tag + "]").orElse(next);
				}

				return ActionResult.error(
								"Invalid child content in %s, found %s, expected one of %s",
								openTag, next,
								Joiner.on(", ").join(expectedListItemTags));
			}

			ActionResult targetResult = target.parse(parser, node, buffer);
			if (!targetResult.isOk()) {
				return targetResult;
			}
		}

		tokenStream.consumeTokens(closeTag.length());

		return ActionResult.ok();
	}

	/**
	 * Creates a child node for the given tag, assuming it was identified as being parseable
	 * @param current The current (parent) node of the context
	 * @return The newly created node
	 */
	public abstract BBAstNode createNodeOnAccept(@Nullable BBAstNode current);

	@Nonnull
	@Override
	public final String getTerminator() {
		return closeTag;
	}

	@Override
	public String toString() {
		return openTag;
	}

}
