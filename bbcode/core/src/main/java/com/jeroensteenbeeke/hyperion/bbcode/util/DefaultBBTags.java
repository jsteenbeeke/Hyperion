/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import java.util.Optional;
import java.util.Set;

import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;

/**
 * Default BBCode tags supported by every editor
 */
enum DefaultBBTags implements IBBCodeTag {
	YOUTUBE("[youtube]", "[/youtube]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseYoutube(current, buffer);
		}
	},
	BOLD("[b]", "[/b]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseBold(current, buffer);
		}
	},
	UNDERLINE("[u]", "[/u]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseUnderline(current, buffer);
		}
	},
	ITALIC("[i]", "[/i]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseItalic(current, buffer);
		}
	},
	STRIKETHROUGH("[s]", "[/s]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseStrikeThrough(current, buffer);
		}
	},
	QUOTE("[quote", "[/quote]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseQuote(current, buffer);
		}
	},
	URL("[url", "[/url]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseUrl(current, buffer);
		}
	},
	IMG("[img", "[/img]") {
		@Nonnull
		@Override
		public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
			return parser.parseImage(current, buffer);
		}
	};

	private final String opening;

	final String terminator;

	DefaultBBTags(String opening, String terminator) {
		this.opening = opening;
		this.terminator = terminator;
	}

	@Override
	public boolean canParse(BBTokenStream stream) {

		return opening.equals(stream.peekTokens(opening.length()));
	}

	@Nonnull
	@Override
	public String getTerminator() {
		return terminator;
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		// Default implementation already contains these buttons, no added
		// Javascript required
		return Optional.empty();
	}

	@Override
	public String toString() {
		return opening.endsWith("]") ? opening : opening + "]";
	}

	/**
	 * Tag scope that encompasses all default tags
	 */
	public static TagScope SCOPE = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Override
		@Nonnull
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(DefaultBBTags.values());
		}
	};
}
