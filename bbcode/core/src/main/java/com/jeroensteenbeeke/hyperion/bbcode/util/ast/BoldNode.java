/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.ast;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.DefaultNode;

import javax.annotation.Nonnull;

/**
 * AST node that represents a piece of boldfaced text. Generally speaking you should not need to
 * create instances of this class yourself
 */
public class BoldNode extends DefaultNode {

	/**
	 * Create a new BoldNode
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *                  node
	 */
	public BoldNode(BBAstNode parent) {
		super(parent);
	}

	@Override
	public void renderTo(
			@Nonnull
					StringBuilder builder, Integer targetCharacters) {
		builder.append("<b>");

		for (BBAstNode child : getChildren()) {
			child.renderTo(builder, targetCharacters);

			if (limitReached(builder, targetCharacters)) {
				break;
			}
		}

		builder.append("</b>");

	}

	@Override
	public void renderForEditor(
			@Nonnull
					StringBuilder builder) {
		builder.append("[b]");

		for (BBAstNode child : getChildren()) {
			child.renderForEditor(builder);
		}

		builder.append("[/b]");
	}
}
