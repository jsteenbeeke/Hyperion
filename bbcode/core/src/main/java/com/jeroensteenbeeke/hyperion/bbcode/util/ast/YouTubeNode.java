/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.ast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.BBRegistry;
import com.jeroensteenbeeke.hyperion.bbcode.util.DefaultNode;

import javax.annotation.Nonnull;

/**
 * AST node that represents an embedded YouTube video. Generally speaking you should not need to
 * create instances of this class yourself
 */
public class YouTubeNode extends DefaultNode {
	public static final String PATTERN = "(\\w|_|-|[a-zA-Z]|[0-9])+";

	public static final String URL_PATTERN = "http(s)?://www\\.youtube\\.com/watch\\?v=((\\w|_|-|[a-zA-Z]|[0-9])+)";

	/**
	 * Create a new YouTubeNode
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *                  node
	 */
	public YouTubeNode(BBAstNode parent) {
		super(parent);
	}

	@Override
	public void renderTo(
			@Nonnull
					StringBuilder builder, Integer targetCharacters) {
		StringBuilder children = new StringBuilder();
		for (BBAstNode node : getChildren()) {
			node.renderTo(children, null);
		}

		String videoId = children.toString().trim();

		if (videoId.matches(PATTERN)) {
			builder.append("<iframe id=\"ytplayer-");
			builder.append(videoId);
			builder.append("\" type=\"text/html\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/");
			builder.append(videoId);
			builder.append("?autoplay=0");

			final String origin = BBRegistry.getOrigin();
			if (origin != null) {
				builder.append("&origin=").append(origin);
			}
			builder.append("\" frameborder=\"0\"></iframe>");
		} else {
			Matcher matcher = Pattern.compile(URL_PATTERN).matcher(videoId);
			if (matcher.matches()) {
				videoId = matcher.group(2);

				// builder.append("<div class=\"youtube-replace\">")
				// .append(videoId).append("</div>");
				//
				builder.append("<iframe id=\"ytplayer-");
				builder.append(videoId);
				builder.append("\" type=\"text/html\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/");
				builder.append(videoId);
				builder.append("?autoplay=0&origin=https://www.tysanclan.com/\" frameborder=\"0\"></iframe>");
			} else {
				builder.append(videoId);
			}
		}
	}

	@Override
	public void renderForEditor(
			@Nonnull
					StringBuilder builder) {
		builder.append("[youtube]");

		for (BBAstNode child : getChildren()) {
			child.renderForEditor(builder);
		}

		builder.append("[/youtube]");
	}
}
