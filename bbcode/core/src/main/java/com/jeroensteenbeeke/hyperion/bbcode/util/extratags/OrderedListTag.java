/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.SimpleTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast.OrderedListNode;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Tag representing an ordered list, rendered as {@code <ol></ol>}, that specifically requires
 * a given child tag (usually: {@code [li][/li]})
 */
public class OrderedListTag extends StrictAllowedChildTag {
	/**
	 * Static instance of OrderedListTag
	 */
	public static final OrderedListTag INSTANCE = new OrderedListTag();

	/**
	 * Create a new OrderedListTag
	 */
	private OrderedListTag() {
		super("[ol]", "[/ol]", ListItemTag.INSTANCE);
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		return BBCodeJavaScriptHelper.createSimpleButton("ol");
	}

	@Override
	public BBAstNode createNodeOnAccept(BBAstNode current) {
		return new OrderedListNode(current);
	}

}
