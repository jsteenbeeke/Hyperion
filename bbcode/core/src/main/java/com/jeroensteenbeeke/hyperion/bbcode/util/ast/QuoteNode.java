/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.ast;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.DefaultNode;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * AST node that represents a piece of text placed in a blockquote. Generally speaking you should
 * not need to create instances of this class yourself
 */
public class QuoteNode extends DefaultNode {
	private final String user;

	/**
	 * Create a new QuoteNode
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *                  node
	 *               @param user The user being quoted, or {@code null} if this is not an
	 *                              explicit quote
	 */
	public QuoteNode(@Nullable BBAstNode parent, @Nullable String user) {
		super(parent);

		this.user = user;
	}

	@Override
	public void renderTo(
			@Nonnull
					StringBuilder builder, Integer targetCharacters) {
		builder.append("<blockquote><p><b>");

		if (user == null) {
			builder.append("Quote");
		} else {
			builder.append("Quoting ");
			builder.append(user);
		}

		builder.append("</b></p> <p>");
		for (BBAstNode node : getChildren()) {
			node.renderTo(builder, targetCharacters);

			if (limitReached(builder, targetCharacters)) {
				builder.append("&hellip;");
				break;
			}
		}
		builder.append("</p></blockquote>");
	}

	@Override
	public void renderForEditor(
			@Nonnull
					StringBuilder builder) {
		builder.append("[quote");

		if (user != null) {
			builder.append("=");
			builder.append(user);
		}
		builder.append("]");

		for (BBAstNode child : getChildren()) {
			child.renderForEditor(builder);
		}

		builder.append("[/quote]");
	}
}
