/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import com.jeroensteenbeeke.hyperion.bbcode.util.ast.BoldNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.ImageNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.ItalicNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.QuoteNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.StrikeThroughNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.TextNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.URLNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.UnderlineNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.ast.YouTubeNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;

/**
 * Internal parser class for dealing with BBCode fragment
 */
public class BBParser {
	private static final char CLOSE = ']';

	private static final char OPEN = '[';

	private static final Pattern WIDTH = Pattern.compile("width=(\\d+)");

	private static final Pattern HEIGHT = Pattern.compile("height=(\\d+)");

	private BBTokenStream tokenStream;

	private RootNode root;

	private Set<TagScope> scopes;

	/**
	 * Create a new parser for the given input. Generally speaking you
	 * do not need to construct instances of this class yourself,
	 * unless you are not using standard HTML rendering
	 * @param input The input to parse
	 * @param scopes The tag scopes to use for parsing
	 */
	public BBParser(@Nonnull String input, @Nonnull Set<TagScope> scopes) {
		this.tokenStream = new BBTokenStream(input);
		this.scopes = scopes;

	}

	/**
	 * Returns the tokenstream associated with this parser
	 *
	 * @return A BBTokenStream instance
	 */
	public BBTokenStream getTokenStream() {
		return tokenStream;
	}

	ActionResult parseStrikeThrough(BBAstNode current, ParseBuffer buffer) {
		StrikeThroughNode node = new StrikeThroughNode(current);

		current.addChild(node);

		tokenStream.consumeTokens(3);

		return parseTag(buffer, node, DefaultBBTags.STRIKETHROUGH);
	}

	ActionResult parseImage(BBAstNode current, ParseBuffer buffer) {

		tokenStream.consumeTokens(4);

		ImageNode node;

		if ("]".equals(tokenStream.peekTokens(1))) {
			tokenStream.consumeTokens(1);
			node = new ImageNode(current);
		} else {
			Optional<String> optionalprops = tokenStream.peekUntil(CLOSE);
			if (!optionalprops.isPresent()) {
				return ActionResult.error("Unclosed [img] tag");
			}
			String propstr = optionalprops.get();
			tokenStream.consumeTokens(propstr.length() + 1);

			Integer width = null;
			Integer height = null;

			String[] props = propstr.split("\\s");
			for (String prop : props) {
				String p = prop.trim();

				if (p.isEmpty()) { continue; }

				Matcher w = WIDTH.matcher(p);
				Matcher h = HEIGHT.matcher(p);

				if (w.matches()) {
					width = Integer.parseInt(w.group(1));
				} else if (h.matches()) {
					height = Integer.parseInt(h.group(1));
				} else {
					return ActionResult.error(
							"Invalid image property expression: " + p);
				}
			}

			node = new ImageNode(current, width, height);
		}

		current.addChild(node);
		return parseTag(buffer, node, DefaultBBTags.IMG);

	}

	ActionResult parseUrl(BBAstNode current, ParseBuffer buffer) {
		tokenStream.consumeTokens(4);

		String link = null;
		if ("=".equals(tokenStream.peekTokens(1))) {
			tokenStream.consumeToken();
			Optional<String> optionalUrl = tokenStream.peekUntil(CLOSE);
			if (!optionalUrl.isPresent()) {
				return ActionResult.error("Unclosed [url] tag");
			}
			link = optionalUrl.get();
			tokenStream.consumeTokens(link.length());
		}

		tokenStream.consumeToken();

		URLNode node = new URLNode(current, link);
		current.addChild(node);

		parseTag(buffer, node, DefaultBBTags.URL);

		if (node.getChildren().isEmpty()) {
			return ActionResult.error("Empty link text");
		} else {
			StringBuilder children = new StringBuilder();
			for (BBAstNode child : node.getChildren()) {
				child.renderTo(children, null);
			}

			if (children.length() == 0) {
				return ActionResult.error("Empty link text");
			}
		}

		return ActionResult.ok();
	}

	ActionResult parseQuote(BBAstNode current, ParseBuffer buffer) {
		tokenStream.consumeTokens(6);

		String user = null;
		if ("=".equals(tokenStream.peekTokens(1))) {
			tokenStream.consumeToken();
			Optional<String> optionalUserRef = tokenStream.peekUntil(CLOSE);
			if (!optionalUserRef.isPresent()) {
				return ActionResult.error("Unclosed [quote] tag");
			}
			user = optionalUserRef.get();
			tokenStream.consumeTokens(user.length());
		}

		tokenStream.consumeToken();

		QuoteNode node = new QuoteNode(current, user);
		current.addChild(node);

		return parseTag(buffer, node, DefaultBBTags.QUOTE);
	}

	ActionResult parseItalic(BBAstNode current, ParseBuffer buffer) {
		ItalicNode node = new ItalicNode(current);

		current.addChild(node);

		tokenStream.consumeTokens(3);

		return parseTag(buffer, node, DefaultBBTags.ITALIC);
	}

	ActionResult parseUnderline(BBAstNode current, ParseBuffer buffer) {
		UnderlineNode node = new UnderlineNode(current);
		current.addChild(node);

		tokenStream.consumeTokens(3);

		return parseTag(buffer, node, DefaultBBTags.UNDERLINE);

	}

	ActionResult parseBold(BBAstNode current, ParseBuffer buffer) {
		BoldNode node = new BoldNode(current);
		current.addChild(node);

		tokenStream.consumeTokens(3);

		return parseTag(buffer, node, DefaultBBTags.BOLD);
	}

	ActionResult parseTag(ParseBuffer buffer, BBAstNode node, IBBCodeTag rec) {
		String lookahead = tokenStream.peekTokens(rec.getTerminator().length());

		String prevLookahead = null;

		while (!rec.getTerminator().equals(lookahead)) {
			if (!tokenStream.hasMoreTokens() || lookahead.equals(prevLookahead)) {
				return ActionResult.error("Missing %s tag",
						rec.getTerminator());
			}

			parseText(node, buffer);

			prevLookahead = lookahead;

			lookahead = tokenStream.peekTokens(rec.getTerminator().length());
		}

		if (buffer.length() > 0) {
			node.addChild(new TextNode(root, buffer.clear()));
		}

		tokenStream.consumeTokens(rec.getTerminator().length());

		return ActionResult.ok();
	}

	ActionResult parseYoutube(BBAstNode current, ParseBuffer buffer) {
		YouTubeNode node = new YouTubeNode(current);
		current.addChild(node);

		tokenStream.consumeTokens(9);

		return parseTag(buffer, node, DefaultBBTags.YOUTUBE);
	}

	/**
	 * Returns a tree-like representation of the BBcode-enhanced text
	 * @return A TypedResult either containing the root node or a message explaining why
	 * the text could not be parsed
	 */
	public TypedResult<RootNode> getAbstractSyntaxTree() {
		if (root == null) {
			this.root = new RootNode();

			return parse().createIfOk(() -> this.root);
		}

		return TypedResult.ok(root);
	}

	/**
	 * Parses the given input
	 *
	 * @return An ActionResult indicating if the parsing completed successfully, or if not: why it failed
	 */
	public ActionResult parse() {
		ParseBuffer buffer = new ParseBuffer();

		while (tokenStream.hasMoreTokens()) {
			ActionResult result = parseText(this.root, buffer);
			if (!result.isOk()) {
				return result;
			}
		}

		if (buffer.length() > 0) {
			root.addChild(new TextNode(root, buffer.clear()));
		}

		return ActionResult.ok();
	}

	private ActionResult parseText(BBAstNode node, ParseBuffer buffer) {
		if (!tokenStream.hasMoreTokens()) { return ActionResult.ok(); }

		String peek = tokenStream.peekTokens(1);

		if (OPEN == peek.charAt(0)) {
			if (buffer.length() > 0) {
				node.addChild(new TextNode(node, buffer.clear()));
			}

			for (IBBCodeTag tag : BBCodeUtil.allTags(scopes)) {
				if (tag.canParse(tokenStream)) {
					return tag.parse(this, node, buffer);
				}
			}

			// Not a tag
			buffer.append(peek);
			tokenStream.consumeToken();
		} else {
			buffer.append(peek);
			tokenStream.consumeToken();
		}

		return ActionResult.ok();
	}

	/**
	 * Node implementation that represents an entire document
	 */
	public static class RootNode extends DefaultNode {
		RootNode() {
			super(null);
		}

		@Override
		public void renderTo(
				@Nonnull
						StringBuilder builder, Integer targetCharacters) {
			for (BBAstNode node : getChildren()) {
				node.renderTo(builder, targetCharacters);

				if (limitReached(builder, targetCharacters)) {
					break;
				}
			}
		}

		@Override
		public void renderForEditor(
				@Nonnull
						StringBuilder builder) {
			for (BBAstNode node : getChildren()) {
				node.renderForEditor(builder);
			}
		}
	}
}
