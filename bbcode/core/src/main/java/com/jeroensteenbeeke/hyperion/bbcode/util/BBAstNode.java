/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

/**
 * Node in a BBCode document Abstract Syntax Tree. This interface represents a subset of a piece of
 * text that contains BBCode tags
 */
public interface BBAstNode {
	/**
	 * Gets the nodes underneath the current node
	 * @return A List of BBAstNode objects
	 */
	@Nonnull
	List<BBAstNode> getChildren();

	/**
	 * Gets the parent of the current node, if one exists (the root node will not have a parent)
	 * @return A BBAstNode object, or {@code null} if this is a root node
	 */
	@Nonnull
	Optional<BBAstNode> getParent();

	/**
	 * Renders the current node as HTML to the given StringBuilder
	 * @param builder The StringBuilder to write HTML to
	 * @param targetCharacters The desired maximum number of characters for the output, for
	 *                            truncation purposes. Implementing classes are expected to honor
	 *                            this, but are allowed to exceed it if the generated HTML would
	 *                            otherwise be invalid
	 */
	void renderTo(@Nonnull StringBuilder builder, @Nullable Integer targetCharacters);

	/**
	 * Renders the current node as BBCode to the given StringBuilder. This essentially reverses
	 * the parse operation
	 * @param builder The StringBuilder to write BBCode to
	 */
	void renderForEditor(@Nonnull StringBuilder builder);

	/**
	 * Add the given node as a child to the current node
	 * @param node The node to add
	 */
	void addChild(@Nullable BBAstNode node);
}
