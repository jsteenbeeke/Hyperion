/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Default implementation of the BBAstNode class, containing logic that generally does
 * not differ between implementations
 */
public abstract class DefaultNode implements BBAstNode {
	private final List<BBAstNode> children = Lists.newLinkedList();

	private final BBAstNode parent;

	/**
	 * Create a new node with (optionally), the given node as parent
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *                  node
	 */
	protected DefaultNode(@Nullable BBAstNode parent) {
		this.parent = parent;
	}

	@Override
	public void addChild(BBAstNode node) {
		children.add(node);
	}

	@Nonnull
	@Override
	public List<BBAstNode> getChildren() {
		return ImmutableList.copyOf(children);
	}

	@Nonnull
	@Override
	public Optional<BBAstNode> getParent() {
		return Optional.ofNullable(parent);
	}

	/**
	 * Checks if the given StringBuilder has reached the given limit (if any)
	 * @param builder A StringBuilder that output can be written to
	 * @param targetCharacters An (optional) number of characters that should be the target limit of the StringBuilder
	 * @return {@code true} if either the StringBuilder contains enough content, or no limit was given, {@code false}
	 * if the limit was given but not reached yet
	 */
	protected static boolean limitReached(@Nonnull StringBuilder builder,
										  @Nullable Integer targetCharacters) {
		return targetCharacters != null && builder.length() >= targetCharacters;

	}
}
