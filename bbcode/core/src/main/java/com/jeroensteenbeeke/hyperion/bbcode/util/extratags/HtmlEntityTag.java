/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.jeroensteenbeeke.hyperion.bbcode.util.*;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast.HtmlEntityNode;
import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Tag representing an HTML entity, such as &amp;amp; or &amp;#248;
 */
public class HtmlEntityTag implements IBBCodeTag {
	private static final String OPEN_TAG = "[#";

	/**
	 * Single instance of the HTML entity tag
	 */
	public static final HtmlEntityTag INSTANCE = new HtmlEntityTag();

	private HtmlEntityTag() {

	}

	@Override
	public boolean canParse(BBTokenStream stream) {
		if (!stream.peekTokens(OPEN_TAG.length()).equals(OPEN_TAG)) {
			return false;
		}

		Optional<String> content = stream.peekUntil(']');

		return content.map(c -> c.matches("\\[##?[a-zA-Z0-9]+")).orElse(false);
	}

	@Nonnull
	@Override
	public ActionResult parse(@Nonnull BBParser parser, BBAstNode current, @Nonnull ParseBuffer buffer) {
		BBTokenStream stream = parser.getTokenStream();
		stream.consumeTokens(OPEN_TAG.length());

		StringBuilder entity = new StringBuilder();

		while (!getTerminator().equals(stream.peekTokens(1))) {
			entity.append(stream.nextToken());
			stream.consumeToken();
		}

		stream.consumeToken();

		current.addChild(new HtmlEntityNode(current, entity.toString()));

		return ActionResult.ok();
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		return Optional.empty();
	}

	@Nonnull
	@Override
	public String getTerminator() {
		return "]";
	}

	@Override
	public String toString() {
		return "HTML Entity";
	}
}
