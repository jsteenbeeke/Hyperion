/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.google.common.collect.ImmutableSet;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.HtmlEntityTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class for parsing BBCode and outputting HTML
 *
 * @author steenbeeke
 */
public final class BBCodeUtil {
	private static final String[] ENTITIES = {"&lt;", "&gt;", "&amp;",
			"&quot;"};

	private BBCodeUtil() {

	}

	/**
	 * Filters the given URL, making sure it starts with either an HTTP or HTTPS prefix, or is a server-relative URL
	 *
	 * @param url The URL to filter
	 * @return Either the URL specified, or a pound sign (#) if the given URL was not acceptable
	 */
	@Nonnull
	public static String replaceUnsafeUrl(@Nullable String url) {
		if (url == null) { return "#"; }

		if (url.startsWith("http://") || url.startsWith("https://")
				|| url.startsWith("/")) {
			return url;
		}

		return "#";
	}

	/**
	 * Removes all tags from the given text, using the given scopes to identify tags
	 * @param original The original text to filter
	 * @param scopes The scopes to use to identify tags
	 * @return A stripped version of the original
	 */
	@Nonnull
	public static String stripTags(@Nullable String original, @Nonnull TagScope... scopes) {
		return stripTags(original, asSet(scopes));
	}

	private static Set<TagScope> asSet(TagScope... rest) {
		return ImmutableSet.<TagScope>builder().add(rest).build();
	}

	/**
	 * Turns all tags in a String into HTML entities
	 *
	 * @param original The original string
	 * @param scopes   All applicable scopes
	 * @return A String that has no HTML in it, only text and entities
	 */
	public static String stripTags(@Nullable String original, @Nonnull Set<TagScope> scopes) {
		if (original == null) {
			return "";
		}

		StringBuilder res = new StringBuilder();

		for (int i = 0; i < original.length(); i++) {
			char next = original.charAt(i);
			switch (next) {
				case '<':
					res.append("&lt;");
					break;
				case '>':
					res.append("&gt;");
					break;
				case '&':
					final String on = original.substring(i);
					boolean replace = true;

					for (String ent : ENTITIES) {
						if (on.startsWith(ent)) {
							replace = false;
							break;
						}
					}

					boolean htmlEntityTagEnabled = allTags(scopes).contains(
							HtmlEntityTag.INSTANCE);
					if (replace) {
						if (htmlEntityTagEnabled) {
							res.append("[#amp]");
						} else {
							res.append("&amp;");
						}
					} else {
						if (htmlEntityTagEnabled) {
							final String rest = original.substring(i+1);
							final int close = rest.indexOf(';');
							if (close != -1) {
								final String entity = rest.substring(0, close);
								res.append("[#");
								res.append(entity);
								res.append("]");
								i += entity.length() + 1;
							}

						} else {
							res.append(next);
						}

					}
					break;
				case '"':
					res.append("&quot;");
					break;
				default:
					res.append(next);
			}
		}

		return res.toString();
	}

	/**
	 * Checks the given String to see if it is a valid BBCode-enhanced text
	 *
	 * @param input  The text to check
	 * @param scopes The tag scopes to check against
	 * @return A BBCodeVerification object
	 */
	@Nonnull
	public static ActionResult verify(@Nonnull String input,
									  @Nonnull TagScope... scopes) {
		return verify(input, asSet(scopes));
	}

	/**
	 * Checks the given String to see if it is a valid BBCode-enhanced text
	 *
	 * @param input  The text to check
	 * @param scopes The tag scopes to check against
	 * @return A BBCodeVerification object
	 */
	@Nonnull
	public static ActionResult verify(@Nonnull String input,
									  @Nonnull Set<TagScope> scopes) {
		return new BBParser(input, scopes).getAbstractSyntaxTree().asSimpleResult();
	}

	/**
	 * Converts the given BBCode input to HTML
	 *
	 * @param input        The input to convert
	 * @param targetLength The target length of the HTML block. The parser will attempt to limit the resulting
	 *                     HTML to this number of characters, without sacrificing validity
	 * @param scopes       The scopes to use for parsing tags
	 * @return A {@code TypedResult} either containing the rendered HTML or a message explaining why it failed
	 */
	@Nonnull
	public static TypedResult<String> toHtml(@Nonnull String input, int targetLength,
											 @Nonnull TagScope... scopes) {
		return toHtml(input, targetLength, asSet(scopes));
	}

	/**
	 * Converts the given BBCode input to HTML
	 *
	 * @param input  The input to convert
	 * @param scopes The scopes to use for parsing tags
	 * @return A {@code TypedResult} either containing the rendered HTML or a message explaining why it failed
	 */
	@Nonnull
	public static TypedResult<String> toHtml(@Nonnull String input,
											 @Nonnull TagScope... scopes) {
		return toHtml(input, null, asSet(scopes));
	}

	/**
	 * Converts the given BBCode input to HTML
	 *
	 * @param input  The input to convert
	 * @param scopes The scopes to use for parsing tags
	 * @return A {@code TypedResult} either containing the rendered HTML or a message explaining why it failed
	 */
	@Nonnull
	public static TypedResult<String> toHtml(@Nonnull String input,
											 @Nonnull Set<TagScope> scopes) {
		return toHtml(input, null, scopes);
	}

	/**
	 * Converts the given BBCode input to HTML
	 *
	 * @param input            The input to convert
	 * @param targetCharacters The target length of the HTML block. The parser will attempt to limit the resulting
	 *                         HTML to this number of characters, without sacrificing validity
	 * @param scopes           The scopes to use for parsing tags
	 * @return A {@code TypedResult} either containing the rendered HTML or a message explaining why it failed
	 */
	@Nonnull
	public static TypedResult<String> toHtml(@Nonnull String input,
											 @Nullable Integer targetCharacters, @Nonnull Set<TagScope> scopes) {
		return new BBParser(input, scopes).getAbstractSyntaxTree().map(root -> {
			StringBuilder sb = new StringBuilder();
			root.renderTo(sb, targetCharacters);
			return sb.toString();
		});
	}

	/**
	 * Takes the given input, converts it to an AST, and then renders it back as BBCode text
	 * @param input The input to convert
	 * @param scopes The scopes to use for parsing tags
	 * @return A {@code TypedResult} either containing the rendered BBCode or a message explaining why it failed
	 */
	@Nonnull
	public static TypedResult<String> forEditor(@Nonnull String input, @Nonnull TagScope... scopes) {
		return forEditor(input, Arrays.stream(scopes).collect(Collectors.toSet()));
	}

	/**
	 * Takes the given input, converts it to an AST, and then renders it back as BBCode text
	 * @param input The input to convert
	 * @param scopes The scopes to use for parsing tags
	 * @return A {@code TypedResult} either containing the rendered BBCode or a message explaining why it failed
	 */
	@Nonnull
	public static TypedResult<String> forEditor(@Nonnull String input,
								   @Nonnull Set<TagScope> scopes) {
		return new BBParser(input, scopes).getAbstractSyntaxTree().map(tree -> {
			StringBuilder sb = new StringBuilder();
			tree.renderForEditor(sb);
			return sb.toString();
		});
	}

	/**
	 * Extracts all tags (of type IBBCodeTag) from the given set of scopes, and adds all tags from the default
	 * BBCode set of tags
	 * @param scopes The scopes to extract from
	 * @return A set of IBBCodeTag objects
	 */
	@Nonnull
	public static Set<IBBCodeTag> allTags(@Nonnull Set<TagScope> scopes) {
		return ImmutableSet.<TagScope>builder().addAll(scopes)
				.add(DefaultBBTags.SCOPE).build().stream()
				.map(TagScope::getTags).flatMap(Set::stream)
				.collect(Collectors.toSet());

	}

}
