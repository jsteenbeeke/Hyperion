/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.SimpleTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast.ListItemNode;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Tag representing a list item, rendered as {@code <li></li>}
 */
public class ListItemTag extends SimpleTag {
	/**
	 * Static instance of the List item tag
	 */
	public static final ListItemTag INSTANCE = new ListItemTag();

	private ListItemTag() {
		super("[li]", "[/li]");
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		return BBCodeJavaScriptHelper.createSimpleButton("li");
	}

	@Override
	public BBAstNode createNodeOnAccept(BBAstNode current) {
		return new ListItemNode(current);
	}

}
