/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;

/**
 * Simple open/close tag combination that allows pretty much anything as child
 */
public abstract class SimpleTag implements IBBCodeTag {
	private final String openTag;

	private final String closeTag;

	/**
	 * Create a new SimpleTag with the given open and close tag
	 * @param openTag The open tag for this tag
	 * @param closeTag The close tag for this tag
	 */
	protected SimpleTag(@Nonnull String openTag, @Nonnull String closeTag) {
		super();
		this.openTag = openTag;
		this.closeTag = closeTag;
	}

	/**
	 * Returns the opening tag of this tag pair
	 * @return The open tag
	 */
	@Nonnull
	public String getOpenTag() {
		return openTag;
	}

	@Override
	public final boolean canParse(@Nonnull BBTokenStream stream) {

		return stream.peekTokens(openTag.length()).equals(openTag);
	}

	@Nonnull
	@Override
	public final ActionResult parse(@Nonnull BBParser parser, BBAstNode current,
									@Nonnull ParseBuffer buffer) {
		BBAstNode node = createNodeOnAccept(current);
		current.addChild(node);

		parser.getTokenStream().consumeTokens(openTag.length());

		return parser.parseTag(buffer, node, this);
	}

	/**
	 * Creates a child node for the given tag, assuming it was identified as being parseable
	 * @param current The current (parent) node of the context
	 * @return The newly created node
	 */
	public abstract BBAstNode createNodeOnAccept(BBAstNode current);

	@Nonnull
	@Override
	public final String getTerminator() {
		return closeTag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((closeTag == null) ? 0 : closeTag.hashCode());
		result = prime * result + ((openTag == null) ? 0 : openTag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleTag other = (SimpleTag) obj;
		if (closeTag == null) {
			if (other.closeTag != null)
				return false;
		} else if (!closeTag.equals(other.closeTag))
			return false;
		if (openTag == null) {
			if (other.openTag != null)
				return false;
		} else if (!openTag.equals(other.openTag))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return openTag;
	}
}
