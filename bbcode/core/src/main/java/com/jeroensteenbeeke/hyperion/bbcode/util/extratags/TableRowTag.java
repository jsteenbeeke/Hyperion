/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast.TableRowNode;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Tag representing a table row, rendered as {@code <tr></tr>}, that requires either regular or header cells as child
 */
public class TableRowTag extends StrictAllowedChildTag {
	private final boolean showButton;

	/**
	 * Creates a TableRowTag
	 * @param td The TableCellTag to allow as child
	 * @param th The TableHeaderCelLTag to allow as child
	 * @param showButton Whether or not a button should be shown in the editor
	 */
	public TableRowTag(TableCellTag td, TableHeaderCellTag th,
			boolean showButton) {
		super("[tr]", "[/tr]", td, th);
		this.showButton = showButton;
	}

	@Override
	public BBAstNode createNodeOnAccept(BBAstNode current) {
		return new TableRowNode(current);
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		if (showButton) {
			return BBCodeJavaScriptHelper.createSimpleButton("tr");
		}

		return Optional.empty();
	}
}
