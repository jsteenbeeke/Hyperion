/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.DefaultNode;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * AST node that represents a HTML entity. Generally speaking you should not need to
 * create instances of this class yourself
 */
public class HtmlEntityNode extends DefaultNode {
	private final String entityBody;

	/**
	 * Create a new HtmlEntityNode
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *                  node
	 *               @param entityBody The body of the entity, representing the characters between the ampersand and semicolon
	 */
	public HtmlEntityNode(@Nullable BBAstNode parent, @Nonnull String entityBody) {
		super(parent);
		this.entityBody = entityBody;
	}

	@Override
	public void renderTo(
			@Nonnull
					StringBuilder builder, Integer targetCharacters) {
		builder.append('&');
		builder.append(entityBody);
		builder.append(';');
	}

	@Override
	public void renderForEditor(
			@Nonnull
					StringBuilder builder) {
		if ("quot".equals(entityBody)) {
			builder.append('"');
		} else if ("amp".equals(entityBody)) {
			builder.append('&');
		} else {
			builder.append("[#");
			builder.append(entityBody);
			builder.append(']');
		}
	}
}
