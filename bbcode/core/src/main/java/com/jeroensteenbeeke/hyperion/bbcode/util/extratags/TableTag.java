/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast.TableNode;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Tag representing a table, rendered as {@code <table></table>}, that allows any basic table
 * element as child (thead, tbody, tfoot and tr).
 */
public class TableTag extends StrictAllowedChildTag {
	private final boolean showButton;

	/**
	 * Creates a new table tag, with the given child element identifiers
	 * @param thead The {@code [thead]} tag implementation to use
	 * @param tbody The {@code [tbody]} tag implementation to use
	 * @param tfoot The {@code [tfoot]} tag implementation to use
	 * @param tr The {@code [tr]} tag implementation to use
	 * @param showButton Whether or not this tag should have a button in the editor
	 */
	public TableTag(TableHeadTag thead, TableBodyTag tbody,
			TableFooterTag tfoot, TableRowTag tr, boolean showButton) {
		super("[table]", "[/table]", thead, tbody, tfoot, tr);
		this.showButton = showButton;
	}

	@Override
	public BBAstNode createNodeOnAccept(BBAstNode current) {
		return new TableNode(current);
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		if (showButton) {
			return BBCodeJavaScriptHelper.createSimpleButton("table");
		}

		return Optional.empty();
	}
}
