/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.ast;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeUtil;
import com.jeroensteenbeeke.hyperion.bbcode.util.DefaultNode;

import javax.annotation.Nonnull;

/**
 * AST node that represents an image. Generally speaking you should not need to
 * create instances of this class yourself
 */
public class ImageNode extends DefaultNode {
	private Integer width;

	private Integer height;

	/**
	 * Create a new ImageNode
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *                  node
	 */
	public ImageNode(BBAstNode parent) {
		this(parent, null, null);
	}

	/**
	 * Create a new ImageNode with the given with and height
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *                  node
	 * @param width The width of the image
	 * @param height The height of the image
	 */
	public ImageNode(BBAstNode parent, Integer width, Integer height) {
		super(parent);
		this.width = width;
		this.height = height;
	}

	@Override
	public void renderTo(
			@Nonnull
					StringBuilder builder, Integer targetCharacters) {
		StringBuilder children = new StringBuilder();
		for (BBAstNode node : getChildren()) {
			node.renderTo(children, null);
		}

		builder.append("<img alt=\"");
		builder.append(BBCodeUtil.replaceUnsafeUrl(children.toString()));
		builder.append("\" ");

		if (width != null || height != null) {
			builder.append("style=\"");
			if (width != null) {
				builder.append("width: ");
				builder.append(width);
				builder.append("px;");

				if (height != null) {
					builder.append(' ');
				}
			}
			if (height != null) {
				builder.append("height: ");
				builder.append(height);
				builder.append("px;");
			}
			builder.append("\" ");
		}
		builder.append("src=\"");
		builder.append(BBCodeUtil.replaceUnsafeUrl(children.toString()));
		builder.append("\" />");
	}

	@Override
	public void renderForEditor(
			@Nonnull
					StringBuilder builder) {
		builder.append("[img");

		if (width != null) {
			builder.append(" width=");
			builder.append(width);
		}
		if (height != null) {
			builder.append(" height=");
			builder.append(height);
		}
		builder.append("]");

		for (BBAstNode child : getChildren()) {
			child.renderForEditor(builder);
		}

		builder.append("[/img]");
	}
}
