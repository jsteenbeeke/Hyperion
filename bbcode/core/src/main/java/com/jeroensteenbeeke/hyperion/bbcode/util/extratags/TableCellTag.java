/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.SimpleTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ast.TableCellNode;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Tag representing a table cell, rendered as {@code <td></td>}
 */
public class TableCellTag extends SimpleTag {
	private final boolean showButton;

	/**
	 * Create a new TableCellTag
	 * @param showButton Whether or not this tag should have a button in the editor
	 */
	public TableCellTag(boolean showButton) {
		super("[td]", "[/td]");
		this.showButton = showButton;
	}

	@Nonnull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		if (showButton) {
			return BBCodeJavaScriptHelper.createSimpleButton("td");
		}

		return Optional.empty();
	}

	@Override
	public BBAstNode createNodeOnAccept(BBAstNode current) {
		return new TableCellNode(current);
	}

}
