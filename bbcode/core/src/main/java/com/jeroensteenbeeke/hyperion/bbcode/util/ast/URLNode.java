/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.ast;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeUtil;
import com.jeroensteenbeeke.hyperion.bbcode.util.DefaultNode;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * AST node that represents a clickable link. Generally speaking you should not need to
 * create instances of this class yourself
 */
public class URLNode extends DefaultNode {
	private final String link;

	/**
	 * Create a new BoldNode
	 *
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *               node
	 * @param link   The link specified in the tag, or {@code null} if the children
	 *               of this node are expected to denote the URL
	 */
	public URLNode(
			@Nullable
					BBAstNode parent,
			@Nullable
					String link) {
		super(parent);

		this.link = link;
	}

	@Override
	public void renderTo(
			@Nonnull
					StringBuilder builder, Integer targetCharacters) {
		if (builder.length() > 0
				&& !Character.isWhitespace(builder.toString().charAt(
				builder.length() - 1))) {
			builder.append(" ");
		}
		builder.append("<a href=\"");

		StringBuilder children = new StringBuilder();
		for (BBAstNode node : getChildren()) {
			node.renderTo(children, targetCharacters);
		}

		if (link == null) {
			// Child nodes are link
			builder.append(BBCodeUtil.replaceUnsafeUrl(children.toString()));
			builder.append("\">");
			builder.append(children.toString());
		} else {
			// Child nodes are descriptive text
			builder.append(BBCodeUtil.replaceUnsafeUrl(link));
			builder.append("\">");
			builder.append(children.toString());
		}

		builder.append("</a>");
	}

	@Override
	public void renderForEditor(
			@Nonnull
					StringBuilder builder) {
		builder.append("[url");

		if (link != null) {
			builder.append("=");
			builder.append(link);
		}
		builder.append("]");

		for (BBAstNode child : getChildren()) {
			child.renderForEditor(builder);
		}

		builder.append("[/url]");
	}
}
