package com.jeroensteenbeeke.hyperion.bbcode.util;

import org.junit.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.junit.Assert.assertEquals;

public class TestUrlSanitizer {
	@Test
	public void testHttp() {
		accept("http://www.google.com");
		accept("http://www.microsoft.com");
		accept("http://www.twitter.com");
	}

	@Test
	public void testNull() {
		decline(null);
	}

	@Test
	public void testHttps() {
		accept("https://www.google.com/");
		accept("https://www.microsoft.com/");
		accept("https://www.twitter.com/");
	}

	@Test
	public void testPaths() {
		accept("/path/to/some/resource");
		accept("/junit/roxxors/your/boxxors/");
		decline("../relative/path");
	}

	@Test
	public void testXss() {
		decline("javascript:alert('Hello! I am a hacker!');");
		decline("jAvAsCrIpT:alert('Do you want a cookie?');");
	}

	private void accept(@Nullable String input) {
		assertEquals(input, BBCodeUtil.replaceUnsafeUrl(input));
	}

	public void decline(@Nullable String input) {
		assertEquals("#", BBCodeUtil.replaceUnsafeUrl(input));
	}
}
