package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.HtmlEntityTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ListItemTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.OrderedListTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.UnorderedListTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Set;

import static org.junit.Assert.*;

public class TestBBCodeBaseMethods {
	private static final String BUTTON_REGEX = "createInsertButton\\(\\s*'.*',\\s*'.*',\\s*'.*'\\s*\\)";
	private TagScope entityScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(HtmlEntityTag.INSTANCE);
		}
	};

	private TagScope listScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(OrderedListTag.INSTANCE, UnorderedListTag.INSTANCE, ListItemTag.INSTANCE);
		}
	};

	private TagScope tableScope = new BBCodeTest.TableTagScope(true);

	private TagScope noButtonTableScope = new BBCodeTest.TableTagScope(true);

	@Test
	public void testJavascriptCreationScripts() {
		Set<IBBCodeTag> tags = BBCodeUtil
				.allTags(ImmutableSet.of(entityScope, listScope, tableScope, noButtonTableScope));

		assertFalse(tags.isEmpty());

		for (IBBCodeTag tag : tags) {
			tag.getCreateButtonJavascript().ifPresent(js -> assertTrue(js.matches(BUTTON_REGEX)));
		}

	}

	@Test
	public void testToString() {
		Set<IBBCodeTag> tags = BBCodeUtil
				.allTags(ImmutableSet.of(entityScope, listScope, tableScope, noButtonTableScope));

		assertFalse(tags.isEmpty());

		for (IBBCodeTag tag : tags) {
			assertFalse(String.format("Tag %s has no toString implementation", tag.getClass().getSimpleName()), tag.toString().startsWith("com.jeroensteenbeeke"));
			assertNotEquals(String.format("Tag %s has an empty toString()", tag.getClass().getSimpleName()), "",
					tag.toString());
		}

	}

	@Test
	public void testOpenTag() {
		Set<IBBCodeTag> tags = BBCodeUtil
				.allTags(ImmutableSet.of(entityScope, listScope, tableScope, noButtonTableScope));

		assertFalse(tags.isEmpty());

		for (IBBCodeTag tag : tags) {
			if (tag instanceof SimpleTag) {
				SimpleTag st = (SimpleTag) tag;
				assertNotNull(String.format("Tag %s has no getOpenTag()", tag.getClass().getSimpleName()), st.getOpenTag());
				assertNotEquals(String.format("Tag %s returns an empty getOpenTag()", tag.getClass().getSimpleName()), 0, st.getOpenTag().length());
			}
		}
	}
}
