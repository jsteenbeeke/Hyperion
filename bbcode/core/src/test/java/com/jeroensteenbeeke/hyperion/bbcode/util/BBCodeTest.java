package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.google.common.collect.ImmutableSet;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.*;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

class BBCodeTest {
	static <T> void assertResult(@Nonnull T expected, @Nonnull TypedResult<T> actual) {
		assertTrue(actual.isOk());
		assertEquals(expected, actual.getObject());
	}

	static <T> void assertFailure(@Nonnull String expectedError, @Nonnull TypedResult<T> actual) {
		assertFalse(actual.isOk());
		assertEquals(expectedError, actual.getMessage());
	}

	static BBCodeBuilder tag(@Nonnull String tag) {
		return new BBCodeBuilder(tag);
	}

	static class BBCodeBuilder {
		private final String tag;

		private BBCodeBuilder(@Nonnull String tag) {
			this.tag = tag;
		}

		public String around(@Nonnull String text) {
			return String.format("[%s]%s[/%s]", tag, text, tag);
		}
	}

	static class TableTagScope implements TagScope {
		private final boolean showButton;

		TableTagScope(boolean showButton) {
			this.showButton = showButton;
		}

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			TableCellTag td = new TableCellTag(showButton);
			TableHeaderCellTag th = new TableHeaderCellTag(showButton);
			TableRowTag tr = new TableRowTag(td, th, showButton);
			TableFooterTag tfoot = new TableFooterTag(tr, showButton);
			TableBodyTag tbody = new TableBodyTag(tr, showButton);
			TableHeadTag thead = new TableHeadTag(tr, showButton);
			TableTag table = new TableTag(thead, tbody, tfoot, tr, showButton);

			return ImmutableSet.<IBBCodeTag>builder().add(td).add(th).add(tr).add(tfoot).add(tbody).add(thead)
					.add(table).build();
		}
	}
}
