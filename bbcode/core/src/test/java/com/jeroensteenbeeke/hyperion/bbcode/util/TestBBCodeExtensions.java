/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import static com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeTest.assertFailure;
import static com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeTest.assertResult;
import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Test;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ListItemTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.OrderedListTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.UnorderedListTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;

import javax.annotation.Nonnull;

public class TestBBCodeExtensions {
	private TagScope testScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			Builder<IBBCodeTag> builder = ImmutableSet.<IBBCodeTag>builder();

			builder.add(ListItemTag.INSTANCE);
			builder.add(UnorderedListTag.INSTANCE);
			builder.add(OrderedListTag.INSTANCE);

			return builder.build();
		}
	};

	private TagScope tableScope = new BBCodeTest.TableTagScope(false);

	@Test
	public void testListItemNode() {
		assertResult("<li>Je moeder!</li>",
				BBCodeUtil.toHtml("[li]Je moeder![/li]", testScope));

		assertResult("<li><li>Je moeder!</li></li>",
				BBCodeUtil.toHtml("[li][li]Je moeder![/li][/li]", testScope));

		assertResult("<li><li>Je</li> <li>moeder!</li></li>",
				BBCodeUtil.toHtml("[li][li]Je[/li] [li]moeder![/li][/li]",
						testScope));

	}

	@Test
	public void testUnorderedListNode() {
		assertResult("<ul>\n</ul>\n", BBCodeUtil.toHtml("[ul][/ul]", testScope));
		assertResult("<ul>\n</ul>\n",
				BBCodeUtil.toHtml("[ul]   [/ul]", testScope));
		assertResult("<ul>\n<li></li></ul>\n",
				BBCodeUtil.toHtml("[ul]  [li][/li] [/ul]", testScope));

	}

	@Test()
	public void testUnorderedListNodeInvalidElement() {
		assertFailure("Invalid child content in [ul], found [b], expected one of [li]", BBCodeUtil.toHtml(
				"[ul] [b]Je moeder![/b] [li][/li] [/ul]", testScope));

	}

	@Test
	public void testOrderedListNode() {
		assertResult("<ol></ol>", BBCodeUtil.toHtml("[ol][/ol]", testScope));
		assertResult("<ol></ol>", BBCodeUtil.toHtml("[ol]   [/ol]", testScope));
		assertResult("<ol><li></li></ol>",
				BBCodeUtil.toHtml("[ol]  [li][/li] [/ol]", testScope));

	}

	@Test()
	public void testOrderedListNodeInvalidElement() {
		assertFailure("Invalid child content in [ol], found [b], expected one of [li]", BBCodeUtil.toHtml(
				"[ol] [b]Je moeder![/b] [li][/li] [/ol]", testScope));

	}

	@Test()
	public void testValidTables() {
		String[] tables = {
				"[table][tr][th]Test[/th][th]Test[/th][/tr][tr][td]-[/td][td][/td][/tr][/table]",
				"[table][thead][tr][th]Test[/th][th]Test[/th][/tr][/thead][tr][td]-[/td][td][/td][/tr][/table]",
				"[table][tr][th]Test[/th][th]Test[/th][/tr][tbody][tr][td]-[/td][td][/td][/tr][/tbody][/table]",
				"[table][tr][th]Test[/th][th]Test[/th][/tr][tfoot][tr][td]-[/td][td][/td][/tr][/tfoot][/table]",
		};

		for (String table : tables) {
			assertResult(table.replace('[', '<').replace(']', '>'), BBCodeUtil.toHtml(table, testScope, tableScope));
		}
	}

}
