package com.jeroensteenbeeke.hyperion.bbcode.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BBTokenStreamTest {
	@Test
	public void testMethods() {
		final String input = "[b]This is BBCode[/b]";

		BBTokenStream stream = new BBTokenStream(input);

		assertEquals(input.length(), stream.getRemainingTokens());

		stream.consumeToken();

		assertEquals(input.length()-1, stream.getRemainingTokens());

		stream.skipTokens(2);

		assertEquals(input.length()-3, stream.getRemainingTokens());
	}
}
