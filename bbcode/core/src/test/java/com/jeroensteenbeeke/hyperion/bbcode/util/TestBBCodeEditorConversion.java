package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.*;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Set;

import static com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeTest.assertResult;
import static com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeTest.tag;

public class TestBBCodeEditorConversion {
	private static final String BASE_INPUT = "This is a test string";
	private static final String GOOGLE = "http://www.google.com";
	private static final String YT_VID = "kOJL1YoVTBI";

	private TagScope entityScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(HtmlEntityTag.INSTANCE);
		}
	};

	private TagScope listScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(OrderedListTag.INSTANCE, UnorderedListTag.INSTANCE, ListItemTag.INSTANCE);
		}
	};

	private TagScope nonButtonTableScope = new BBCodeTest.TableTagScope(false);

	private TagScope buttonTableScope = new BBCodeTest.TableTagScope(true);

	@Test
	public void testReverseSimpleTags() {
		assertResult(BASE_INPUT, BBCodeUtil.forEditor(BASE_INPUT, entityScope));
		assertResult(tag("b").around(BASE_INPUT), BBCodeUtil.forEditor(tag("b").around(BASE_INPUT), entityScope));
		assertResult(tag("i").around(BASE_INPUT), BBCodeUtil.forEditor(tag("i").around(BASE_INPUT), entityScope));
		assertResult(tag("u").around(BASE_INPUT), BBCodeUtil.forEditor(tag("u").around(BASE_INPUT), entityScope));
		assertResult(tag("s").around(BASE_INPUT), BBCodeUtil.forEditor(tag("s").around(BASE_INPUT), entityScope));
		assertResult(tag("quote").around(BASE_INPUT),
				BBCodeUtil.forEditor(tag("quote").around(BASE_INPUT), entityScope));
		assertResult(tag("url").around(GOOGLE),
				BBCodeUtil.forEditor(tag("url").around(GOOGLE), entityScope));
		assertResult(tag("youtube").around(YT_VID), BBCodeUtil.forEditor(tag("youtube").around(YT_VID), entityScope));
		assertResult("\"", BBCodeUtil.forEditor("\"", entityScope));
		assertResult("&", BBCodeUtil.forEditor("&", entityScope));
		assertResult("[#emdash]", BBCodeUtil.forEditor("[#emdash]", entityScope));
	}

	@Test
	public void testReverseListTags() {
		assertResult(tag("ol").around("\n\t" + tag("li").around(BASE_INPUT) + "\n"),
				BBCodeUtil.forEditor(tag("ol").around(tag("li").around(BASE_INPUT)), entityScope, listScope));
		assertResult(tag("ul").around("\n\t" + tag("li").around(BASE_INPUT) + "\n"),
				BBCodeUtil.forEditor(tag("ul").around(tag("li").around(BASE_INPUT)), entityScope, listScope));

	}

	@Test
	public void testReverseTableTags() {
		String[] tables = {
				"[table][tr][th]Test[/th][th]Test[/th][/tr][tr][td]-[/td][td][/td][/tr][/table]",
				"[table][thead][tr][th]Test[/th][th]Test[/th][/tr][/thead][tr][td]-[/td][td][/td][/tr][/table]",
				"[table][tr][th]Test[/th][th]Test[/th][/tr][tbody][tr][td]-[/td][td][/td][/tr][/tbody][/table]",
				"[table][tr][th]Test[/th][th]Test[/th][/tr][tfoot][tr][td]-[/td][td][/td][/tr][/tfoot][/table]",
		};
		for (String table : tables) {
			assertResult(table, BBCodeUtil.forEditor(
					table, nonButtonTableScope));
			assertResult(table, BBCodeUtil.forEditor(
					table, buttonTableScope));
		}
	}


}
