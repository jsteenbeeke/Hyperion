/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import static com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeTest.assertFailure;
import static com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeTest.assertResult;
import static org.junit.Assert.*;

import java.util.Optional;
import java.util.Set;

import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ListItemTag;
import com.jeroensteenbeeke.hyperion.test.Equivalence;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.HtmlEntityTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;

import javax.annotation.Nonnull;

public class TestBBCodeParser {

	private TagScope entityScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(HtmlEntityTag.INSTANCE);
		}
	};

	@Test(timeout = 3000L)
	public void testInvalid() {
		assertResult("[Je moeder]", BBCodeUtil.toHtml("[Je moeder]"));
	}

	@Test()
	public void testUnclosed() {
		assertFailure("Missing [/b] tag", BBCodeUtil.toHtml("[b]"));
	}

	@Test
	public void testBold() {
		assertResult("<b>Test</b>", BBCodeUtil.toHtml("[b]Test[/b]"));
	}

	@Test
	public void testItalic() {
		assertResult("<i>Test</i>", BBCodeUtil.toHtml("[i]Test[/i]"));
	}

	@Test
	public void testUnderline() {
		assertResult("<span class=\"underline\">Test</span>",
				BBCodeUtil.toHtml("[u]Test[/u]"));
	}

	@Test
	public void testStrike() {
		assertResult("<span class=\"strikethrough\">Test</span>",
				BBCodeUtil.toHtml("[s]Test[/s]"));
	}

	@Test
	public void testQuote() {
		assertResult(
				"<blockquote><p><b>Quote</b></p> <p>Test</p></blockquote>",
				BBCodeUtil.toHtml("[quote]Test[/quote]"));
		assertResult(
				"<blockquote><p><b>Quoting Prospero</b></p> <p>Test</p></blockquote>",
				BBCodeUtil.toHtml("[quote=Prospero]Test[/quote]"));
	}

	@Test
	public void testLink() {
		assertResult(
				"<a href=\"http://www.youtube.com/watch?v=a-79sbicwTQ\">http://www.youtube.com/watch?v=a-79sbicwTQ</a>",
				BBCodeUtil
						.toHtml("[url]http://www.youtube.com/watch?v=a-79sbicwTQ[/url]"));
		assertResult(
				"<a href=\"http://www.youtube.com/watch?v=a-79sbicwTQ\">Swedish Christmas Song</a>",
				BBCodeUtil
						.toHtml("[url=http://www.youtube.com/watch?v=a-79sbicwTQ]Swedish Christmas Song[/url]"));
	}

	@Test
	public void testYoutube() {
		assertResult(
				"<iframe id=\"ytplayer-a-79sbicwTQ\" type=\"text/html\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/a-79sbicwTQ?autoplay=0\" frameborder=\"0\"></iframe>",
				BBCodeUtil.toHtml("[youtube]a-79sbicwTQ[/youtube]"));

		BBRegistry.setOrigin("http://www.jeroensteenbeeke.nl");

		assertResult(
				"<iframe id=\"ytplayer-a-79sbicwTQ\" type=\"text/html\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/a-79sbicwTQ?autoplay=0&origin=http://www.jeroensteenbeeke.nl\" frameborder=\"0\"></iframe>",
				BBCodeUtil.toHtml("[youtube]a-79sbicwTQ[/youtube]"));

		BBRegistry.setOrigin(null);

		assertResult(
				"<iframe id=\"ytplayer-a-79sbicwTQ\" type=\"text/html\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/a-79sbicwTQ?autoplay=0\" frameborder=\"0\"></iframe>",
				BBCodeUtil.toHtml("[youtube]a-79sbicwTQ[/youtube]"));
	}

	@Test
	public void testImage() {
		assertResult(
				"<img alt=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" src=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" />",
				BBCodeUtil
						.toHtml("[img]http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg[/img]"));

		assertResult(
				"<img alt=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" style=\"width: 50px;\" src=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" />",
				BBCodeUtil
						.toHtml("[img width=50]http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg[/img]"));

		assertResult(
				"<img alt=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" style=\"height: 50px;\" src=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" />",
				BBCodeUtil
						.toHtml("[img height=50]http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg[/img]"));

		assertResult(
				"<img alt=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" style=\"width: 50px; height: 50px;\" src=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" />",
				BBCodeUtil
						.toHtml("[img height=50 width=50]http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg[/img]"));

		assertResult(
				"<img alt=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" style=\"width: 50px; height: 50px;\" src=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" />",
				BBCodeUtil
						.toHtml("[img width=50 height=50]http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg[/img]"));

		assertResult(
				"<img alt=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" style=\"width: 50px; height: 50px;\" src=\"http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg\" />",
				BBCodeUtil
						.toHtml("[img    width=50    height=50]http://www.mentalfloss.com/sites/default/legacy/blogs/wp-content/uploads/2010/02/400pancake_bunny.jpg[/img]"));

	}

	@Test
	public void testSentences() {
		assertResult(
				"Seriously, Brent Weeks, <b>what is wrong with you!?!</b>",
				BBCodeUtil
						.toHtml("Seriously, Brent Weeks, [b]what is wrong with you!?![/b]"));
	}

	@Test
	public void testStrip() {
		assertEquals("", BBCodeUtil.stripTags(null));
		assertEquals("&amp;", BBCodeUtil.stripTags("&"));
		assertEquals("&lt;script&gt;&lt;/script&gt;",
				BBCodeUtil.stripTags("<script></script>"));
		assertEquals("&lt;script&gt;&lt;/script&gt;",
				BBCodeUtil.stripTags(BBCodeUtil.stripTags("<script></script>")));
		assertEquals("&lt;script&gt;&lt;/script&gt;",
				BBCodeUtil.stripTags(BBCodeUtil.stripTags(BBCodeUtil
						.stripTags("<script></script>"))));
		assertEquals("&quot;",
				BBCodeUtil.stripTags(BBCodeUtil.stripTags(BBCodeUtil
						.stripTags("\""))));

		assertEquals("[#amp]", BBCodeUtil.stripTags("[#amp]"));
		assertEquals("Bacon [#amp] Eggs", BBCodeUtil.stripTags("Bacon & Eggs", entityScope));
		assertEquals("Bacon [#amp] Eggs", BBCodeUtil.stripTags("Bacon &amp; Eggs", entityScope));
	}

	@Test
	public void testSimpleTagEquality() {
		assertEquals(ListItemTag.INSTANCE, ListItemTag.INSTANCE);
		assertNotEquals(ListItemTag.INSTANCE, null);

		SimpleTestTag a = new SimpleTestTag("[yes]", null);
		SimpleTestTag b = new SimpleTestTag("[yes]", "[/yes]");


		Equivalence.nonSymmetric(a, b);

	}

	private static class SimpleTestTag extends SimpleTag {
		public SimpleTestTag(@Nonnull String openTag, @Nonnull String closeTag) {
			super(openTag, closeTag);
		}

		@Override
		public BBAstNode createNodeOnAccept(BBAstNode current) {
			return null;
		}

		@Nonnull
		@Override
		public Optional<String> getCreateButtonJavascript() {
			return Optional.empty();
		}
	}
}
