/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.web.components;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeUtil;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.lux.TypedResult;
import org.apache.wicket.Session;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Panel for displaying text enhanced with BBCode. Will take the provided text and convert it to HTML,
 * rendering any existing HTML in a sanitized fashion
 */
public class BBCodePanel extends Panel {
	private static final long serialVersionUID = 1L;

	private BBCodeModel codeModel;

	/**
	 * Create a new BBCodePanel
	 *
	 * @param id     The Wicket ID of the panel
	 * @param text   A Wicket model of the text to display
	 * @param scopes The TagScopes to use. These indicate what tags are available for use
	 */
	public BBCodePanel(@Nonnull String id, @Nonnull IModel<String> text,
					   @Nonnull Set<TagScope> scopes) {
		super(id);

		codeModel = new BBCodeModel(text, scopes);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(new Label("text", codeModel).setEscapeModelStrings(false)
				.setRenderBodyOnly(true));
	}

	/**
	 * Sets the limit in characters of the text to display. If set, this will cause the displayed text to be
	 * truncated after approximately the given limit, without breaking the rendering of tags and such
	 *
	 * @param limit The limit in characters
	 * @return The current panel
	 */
	public BBCodePanel setLimit(int limit) {
		if (codeModel != null) {
			codeModel.setLimit(limit);
		}
		return this;
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		response.render(getCssHeaderItem());
	}

	/**
	 * Get the HeaderItem for the CSS file used by the BBCode components
	 *
	 * @return A CssHeaderItem that points to the proper CSS file
	 */
	protected CssHeaderItem getCssHeaderItem() {
		return CssHeaderItem.forUrl("css/bbcode.css");
	}

	private static class BBCodeModel implements IModel<String> {
		private static final long serialVersionUID = 1L;

		private final IModel<String> delegate;

		private final Set<TagScope> scopes;

		private Integer limit = null;

		private BBCodeModel(@Nonnull IModel<String> delegate,
							@Nonnull Set<TagScope> scopes) {
			this.delegate = delegate;
			this.scopes = scopes;
		}

		private void setLimit(Integer limit) {
			this.limit = limit;
		}

		@Override
		public String getObject() {
			final String original = delegate.getObject();

			TypedResult<String> htmlResult;

			if (limit != null) {
				htmlResult = BBCodeUtil.toHtml(original, this.limit, scopes);
			} else {
				htmlResult = BBCodeUtil.toHtml(original, scopes);
			}

			if (htmlResult.isOk()) {
				return htmlResult.getObject();
			}

			Session.get().error(htmlResult.getMessage());

			// Should not occur when input is done using BBCodeTextArea
			return BBCodeUtil.stripTags(original, scopes);
		}

		@Override
		public void setObject(String object) {
			delegate.setObject(object);
		}

		@Override
		public void detach() {
			delegate.detach();
		}
	}

}
