/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.web.components;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.bbcode.util.BBCodeUtil;
import com.jeroensteenbeeke.hyperion.bbcode.util.IBBCodeTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import org.apache.wicket.Component;
import org.apache.wicket.Session;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.ComponentModel;
import org.apache.wicket.model.IModel;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * TextArea for inputting BBCode text. Provides the user with a button bar for quickly adding tags.
 */
public class BBCodeTextArea extends TextArea<String> {

	private static final long serialVersionUID = 1L;

	private final Set<TagScope> scopes;

	/**
	 * Creates a new TextArea
	 *
	 * @param id     The Wicket ID of the TextArea
	 * @param text   The initial text to display
	 * @param scopes The TagScopes to use. These indicate what tags are available for use
	 */
	public BBCodeTextArea(String id, String text, TagScope... scopes) {
		this(id, text, Sets.newHashSet(scopes));
	}

	/**
	 * Creates a new TextArea
	 *
	 * @param id     The Wicket ID of the TextArea
	 * @param text   The initial text to display
	 * @param scopes The TagScopes to use. These indicate what tags are available for use
	 */
	public BBCodeTextArea(String id, String text, Set<TagScope> scopes) {
		this(id, new BBCodeFilterModel(text, scopes));
	}

	/**
	 * Creates a new TextArea
	 *
	 * @param id    The Wicket ID of the TextArea
	 * @param model The model to use for the TextArea. Contains the text as inputted, as well as the tag scopes to use
	 */
	public BBCodeTextArea(String id, BBCodeFilterModel model) {
		super(id, model);
		model.wrapOnAssignment(this);
		this.scopes = model.getScopes();

		initialize();
	}

	private void initialize() {
		add(new BBCodeValidator(scopes));
		setOutputMarkupId(true);
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		response.render(CssHeaderItem.forUrl("css/bbcode.css"));
		response.render(JavaScriptHeaderItem
				.forReference(BBCodeTextAreaPluginResource.get()));

		List<String> buttonScripts = BBCodeUtil.allTags(scopes).stream()
				.map(IBBCodeTag::getCreateButtonJavascript)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.filter(Objects::nonNull).collect(Collectors.toList());

		response.render(OnDomReadyHeaderItem.forScript(String.format(
				"$('#%s').bbedit([%s]);", getMarkupId(),
				Joiner.on(", ").join(buttonScripts))));
	}

	/**
	 * Model for containing editor data, as well as relevant tag scopes
	 */
	public static final class BBCodeFilterModel extends ComponentModel<String> {
		private static final long serialVersionUID = 1L;

		private String data;

		private final Set<TagScope> scopes;

		/**
		 * Create a new BBCode model
		 *
		 * @param data   The text as entered by the user
		 * @param scopes The set of active scopes
		 */
		public BBCodeFilterModel(@Nonnull String data,
								 @Nonnull Set<TagScope> scopes) {
			super();
			this.scopes = scopes;
			this.data = BBCodeUtil.forEditor(data, scopes).allResults().findFirst().orElse("");
		}

		/**
		 * Get the set of tag scopes used by this model
		 *
		 * @return An immutable set of tag scopes
		 */
		public Set<TagScope> getScopes() {
			return ImmutableSet.copyOf(scopes);
		}

		@Override
		public String getObject(Component component) {
			return data;
		}


		@Override
		public void setObject(Component component, String object) {
			this.data =
					BBCodeUtil.forEditor(object, scopes).ifNotOk(component::error).asOptional().orElse
							(object);
		}
	}

}
