package com.jeroensteenbeeke.hyperion.bbcode.web.pages;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.bbcode.util.IBBCodeTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.*;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.hyperion.bbcode.web.components.BBCodePanel;
import com.jeroensteenbeeke.hyperion.bbcode.web.components.BBCodeTextArea;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import javax.annotation.Nonnull;
import java.util.Set;

public class BBCodeTestPage extends WebPage {
	private static final TagScope entityScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(HtmlEntityTag.INSTANCE);
		}
	};

	private static final TagScope listScope = new TagScope() {
		private static final long serialVersionUID = 1L;

		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Sets.newHashSet(OrderedListTag.INSTANCE, UnorderedListTag.INSTANCE, ListItemTag.INSTANCE);
		}
	};

	private static final TagScope tableScope = new TagScope() {
		@Nonnull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			TableCellTag td = new TableCellTag(true);
			TableHeaderCellTag th = new TableHeaderCellTag(true);
			TableRowTag tr = new TableRowTag(td, th, true);
			TableFooterTag tfoot = new TableFooterTag(tr, true);
			TableBodyTag tbody = new TableBodyTag(tr, true);
			TableHeadTag thead = new TableHeadTag(tr, true);
			TableTag table = new TableTag(thead, tbody, tfoot, tr, true);

			return ImmutableSet.<IBBCodeTag>builder().add(td).add(th).add(tr).add(tfoot).add(tbody).add(thead)
					.add(table).build();
		}
	};


	private static final TagScope[] scopes = { entityScope, tableScope, listScope };

	public BBCodeTestPage() {
		final IModel<String> textModel = Model.of("");

		add(new BBCodePanel("panel", textModel, Sets.newHashSet(scopes)));
		BBCodeTextArea editor = new BBCodeTextArea("editor", "", scopes);
		Form<Void> updateForm = new Form<Void>("form") {
			@Override
			protected void onSubmit() {
				String editorText = editor.getModelObject();
				textModel.setObject(editorText);
			}
		};
		updateForm.add(editor);
		add(updateForm);
	}
}
