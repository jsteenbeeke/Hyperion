package com.jeroensteenbeeke.hyperion.bbcode.web.components;

import com.jeroensteenbeeke.hyperion.bbcode.web.pages.BBCodeTestPage;
import org.apache.wicket.Component;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WicketComponentTest {
	private WicketTester tester;

	@Before
	public void initTester() {
		if (tester == null) {
			tester = new WicketTester(BBCodeTestPage.class);
		}
	}

	@Test
	public void testForm() {
		tester.startPage(BBCodeTestPage.class);
		tester.assertRenderedPage(BBCodeTestPage.class);

		tester.assertLabel("panel:text", "");

		FormTester form = tester.newFormTester("form");

		form.setValue("editor", "[b]This is a test[/b]");

		form.submit();

		tester.assertNoFeedbackMessage(FeedbackMessage.ERROR);

		tester.assertRenderedPage(BBCodeTestPage.class);
		tester.assertLabel("panel:text", "<b>This is a test</b>");

		form = tester.newFormTester("form");

		assertEquals("[b]This is a test[/b]", form.getTextComponentValue("editor"));

		BBCodePanel panel = (BBCodePanel) tester.getComponentFromLastRenderedPage("panel");
		panel.setLimit(1);

		form.submit();

		tester.assertLabel("panel:text", "<b>This</b>");

		form = tester.newFormTester("form");

		form.setValue("editor", "[b]This is a test");

		form.submit();

		tester.assertFeedbackMessages(IFeedbackMessageFilter.ALL, "Missing [/b] tag");
		assertEquals("[b]This is a test", form.getTextComponentValue("editor"));

		Label label = (Label) tester.getComponentFromLastRenderedPage("panel:text");
		label.setDefaultModelObject("[b]This is a test");
		assertEquals("[b]This is a test", label.getDefaultModelObjectAsString());


	}
}
