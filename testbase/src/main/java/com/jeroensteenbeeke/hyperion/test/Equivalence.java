package com.jeroensteenbeeke.hyperion.test;

import javax.annotation.Nonnull;

import static org.junit.Assert.*;

/**
 * Utility class for testing object equivalence through their equals method
 */
public class Equivalence {

	private static final String TRANSITIVE_REQUIREMENT =
			"To properly test transitive equality, please create 3 distinct objects that are functionally equal";

	private Equivalence() {

	}

	/**
	 * Ensures that two objects are symmetrically equivalent and produce the same hashcode
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @throws AssertionError If the objects are not symmetrically equivalent, or, barring that, their hashcodes differ
	 */
	public static void properHashcode(@Nonnull Object a, @Nonnull Object b) {
		symmetric(a, b);

		assertEquals("Two symmetrically equivalent objects have different hashcodes", a.hashCode(), b.hashCode());
	}

	/**
	 * Ensures that the three given objects are transitively equivalent (that is, if a equals b, and b equals c, then
	 * a must equal c). This method requires the three parameters to be distinct objects
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @param c The third object to check
	 * @throws AssertionError If the objects are not distinct, or if they are not transitively equivalent
	 */
	public static void transitive(@Nonnull Object a, @Nonnull Object b, @Nonnull Object c) {
		assertFalse(
				TRANSITIVE_REQUIREMENT,
				a == b);
		assertFalse(TRANSITIVE_REQUIREMENT, b == c);
		assertFalse(TRANSITIVE_REQUIREMENT, a == c);

		assertEquals("Transitivity: A does not equal B", a, b);
		assertEquals("Transitivity: B does not equal C", b, c);
		assertEquals("Transitivity: A does not equal C", a, c);
	}

	/**
	 * Ensures that two objects are symmetrically equivalent (that is: if a equals b, then b must be equal to a)
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @throws AssertionError If the objects are not distinct, or if they are not symmetrically equivalent
	 */
	public static void symmetric(Object a, Object b) {
		assertEquals("Symmetry: A does not equal B", a, b);
		assertEquals("Symmetry: B does not equal A", b, a);
	}

	/**
	 * Ensures that the given object is reflexively equivalent. That is, it must be equal to itself
	 *
	 * @param a The object to check
	 * @throws AssertionError If the object is not reflexively equivalent
	 */
	public static void reflexive(Object a) {
		assertEquals("Reflexivity: A does not equal A", a, a);
	}

	/**
	 * Ensures that the given three objects are equivalent in a variety of ways. They are tested for reflexivity,
	 * symmetry and transitivity, and their
	 * hashcodes are also checked
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @param c The third object to check
	 * @throws AssertionError If any two objects are not fully equivalents
	 */
	public static void equivalent(Object a, Object b, Object c) {
		reflexive(a);
		reflexive(b);
		reflexive(c);

		symmetric(a, b);
		symmetric(b, c);

		transitive(a, b, c);
		transitive(c, b, a);

		properHashcode(a, b);
		properHashcode(b, c);
		properHashcode(a, c);
	}

	/**
	 * Tests that two object are not equivalent in a symmetric way.
	 *
	 * @param a The first object to check
	 * @param b The second object to check
	 * @throws AssertionError If a is equal to b, or if b is equal to a
	 */
	public static void nonSymmetric(Object a, Object b) {
		assertNotEquals("Symmetry: A does not equal B", a, b);
		assertNotEquals("Symmetry: B does not equal A", b, a);
	}
}
