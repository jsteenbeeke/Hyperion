/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.test.matcher;

/**
 * Hamcrest matchers for working with comparables
 */
public final class ComparableMatchers {
	private ComparableMatchers() {
	}

	/**
	 * Matcher for enforcing a greater than rule
	 * @param <T> The type of the value under test
	 * @param value The value that is the lower bound. The value under test should be greater than this
	 * @return A matcher that will perform a greater than check
	 */
	public static <T extends Comparable<T>> ComparableMatcher<T> greaterThan(T value) {
		return new ComparableMatcher<T>(value) {
			@Override
			public boolean comparesFavorably(int comparisonResult) {
				return comparisonResult > 0;
			}

			@Override
			public String getComparisonText() {
				return "greater than ";
			}
		};
	}

	/**
	 * Matcher for enforcing a greater than or equal to rule
	 * @param value The value that is the lower bound. The value under test should be greater than or equal to this
	 * @param <T> The type of the value under test
	 * @return A matcher that will perform a greater than or equal to check
	 */
	public static <T extends Comparable<T>> ComparableMatcher<T> greaterThanOrEqualTo(
			T value) {
		return new ComparableMatcher<T>(value) {
			@Override
			public boolean comparesFavorably(int comparisonResult) {
				return comparisonResult >= 0;
			}

			@Override
			public String getComparisonText() {
				return "greater than or equal to ";
			}
		};
	}

	/**
	 * Matcher for enforcing a less than or equal to rule
	 * @param value The value that is the upper bound. The value under test should be less than or equal to this
	 * @param <T> The type of the value under test
	 * @return A matcher that will perform a less than or equal to check
	 */
	public static <T extends Comparable<T>> ComparableMatcher<T> lessThanOrEqualTo(T value) {
		return new ComparableMatcher<T>(value) {
			@Override
			public boolean comparesFavorably(int comparisonResult) {
				return comparisonResult <= 0;
			}

			@Override
			public String getComparisonText() {
				return "less than or equal to ";
			}
		};
	}

	/**
	 * Matcher for enforcing a less than rule
	 * @param value The value that is the upper bound. The value under test should be less than this
	 * @param <T> The type of the value under test
	 * @return A matcher that will perform a less than check
	 */
	public static <T extends Comparable<T>> ComparableMatcher<T> lessThan(T value) {
		return new ComparableMatcher<T>(value) {
			@Override
			public boolean comparesFavorably(int comparisonResult) {
				return comparisonResult < 0;
			}

			@Override
			public String getComparisonText() {
				return "less than ";
			}
		};
	}
}
