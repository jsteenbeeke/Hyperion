package com.jeroensteenbeeke.hyperion.test.matcher;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import javax.annotation.Nonnull;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Hamcrest matchers for regular expression operations
 */
public class PatternMatchers {
	/**
	 * Matcher to compare against a regex pattern
	 *
	 * @param pattern The pattern to match again
	 * @return A matcher
	 */
	public static TypeSafeDiagnosingMatcher<String> matchesPattern(@Nonnull String pattern) {
		return new TypeSafeDiagnosingMatcher<String>() {
			@Override
			protected boolean matchesSafely(String item, Description mismatchDescription) {
				Pattern compiledPattern = Pattern.compile(pattern);

				Matcher matcher = compiledPattern.matcher(item);

				if (matcher.matches()) {
					return true;
				}

				mismatchDescription
						.appendText("input ")
						.appendValue(item)
						.appendText(" does not match pattern ")
						.appendText(pattern);

				return false;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("matches pattern ");
				description.appendText(pattern);
			}
		};
	}



}
