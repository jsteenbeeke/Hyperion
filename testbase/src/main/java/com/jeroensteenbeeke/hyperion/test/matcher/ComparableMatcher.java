/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.test.matcher;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * Type-safe Hamcrest matcher prototype that can be used to compare objects that implement the comparable interface
 * @param <T> The type of the comparable to test
 */
public abstract class ComparableMatcher<T extends Comparable<T>> extends
		TypeSafeDiagnosingMatcher<T> {
	private final T expected;

	ComparableMatcher(T expected) {
		this.expected = expected;
	}


	@Override
	public void describeTo(Description description) {
		description.appendText(getComparisonText());
		description.appendText(" ");
		description.appendValue(expected);
	}

	/**
	 * Describes the relation between comparables we're trying to check
	 * @return A String describing the relation
	 */
	public abstract String getComparisonText();

	/**
	 * Determines whether or not the comparison result is expected
	 * @param comparisonResult The result to test
	 * @return {@code true} if the result is expected, {@code false} otherwise
	 */
	public abstract boolean comparesFavorably(int comparisonResult);

	@Override
	protected boolean matchesSafely(T item, Description mismatchDescription) {
		if (!comparesFavorably(item.compareTo(expected))) {
			mismatchDescription.appendText("actual value ");
			mismatchDescription.appendValue(item);

			return false;
		}

		return true;
	}

}
