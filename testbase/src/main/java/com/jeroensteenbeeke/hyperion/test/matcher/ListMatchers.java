package com.jeroensteenbeeke.hyperion.test.matcher;

import org.hamcrest.Description;
import org.hamcrest.SelfDescribing;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.internal.SelfDescribingValue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Hamcrest matchers for working with lists
 */
public class ListMatchers {
	/**
	 * Creates a matcher that ensures the value under test is a list with the given elements
	 *
	 * @param elements The elements the list should contain, in order
	 * @param <T>      The type of elements contained in the list
	 * @return A matcher that will ensure the list under test is equal to the given list
	 */
	@SafeVarargs
	public static <T> TypeSafeDiagnosingMatcher<List<T>> isList(final T... elements) {
		return new TypeSafeDiagnosingMatcher<List<T>>() {
			@Override
			protected boolean matchesSafely(List<T> item, Description mismatchDescription) {
				if (item.size() != elements.length) {
					describeList(item, mismatchDescription);
					return false;
				}

				for (int i = 0; i < item.size(); i++) {
					if (!elements[i].equals(item.get(i))) {
						describeList(item, mismatchDescription);
						return false;
					}
				}

				return true;
			}

			private void describeList(List<T> item, Description mismatchDescription) {
				List<SelfDescribing> actual =
						item.stream().map(Object::toString).map(s -> "" + '"' + s + '"').map(SelfDescribingValue::new).collect(
								Collectors.toList());
				mismatchDescription.appendText("list is ");
				mismatchDescription.appendList("{", ", ", "}", actual);
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("list is ");
				List<SelfDescribing> values =
						Arrays.stream(elements).map(Object::toString).map(s -> "" + '"' + s + '"').map(SelfDescribingValue::new).collect(
								Collectors.toList());
				description.appendList("{", ", ", "}", values);
			}
		};
	}
}
