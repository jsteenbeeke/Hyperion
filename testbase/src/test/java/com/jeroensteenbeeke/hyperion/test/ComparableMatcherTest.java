package com.jeroensteenbeeke.hyperion.test;

import org.junit.Test;

import static com.jeroensteenbeeke.hyperion.test.matcher.ComparableMatchers.*;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class ComparableMatcherTest {
	@Test
	public void testLessThan() {
		assertThat(5, lessThan(12));
		assertThat(5, not(lessThan(3)));
		assertThat(5, not(lessThan(5)));
	}

	@Test
	public void testLessThanEquals() {
		assertThat(5, lessThanOrEqualTo(12));
		assertThat(5, not(lessThanOrEqualTo(3)));
		assertThat(5, lessThanOrEqualTo(5));
	}

	@Test
	public void testGreaterThan() {
		assertThat(5, not(greaterThan(12)));
		assertThat(5, greaterThan(3));
		assertThat(5, not(greaterThan(5)));
	}

	@Test
	public void testGreaterThanEquals() {
		assertThat(5, not(greaterThanOrEqualTo(12)));
		assertThat(5, greaterThanOrEqualTo(3));
		assertThat(5, greaterThanOrEqualTo(5));
	}
}
