package com.jeroensteenbeeke.hyperion.test;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class EquivalenceTest {
	@Test
	public void testEquivalentValues() {
		Set<String> a = new HashSet<>();
		Set<String> b = new HashSet<>();
		Set<String> c = new HashSet<>();

		a.add("a");
		a.add("b");
		a.add("c");

		b.add("a");
		b.add("a");
		b.add("b");
		b.add("b");
		b.add("c");
		b.add("c");

		c.add("c");
		c.add("c");
		c.add("c");
		c.add("b");
		c.add("b");
		c.add("b");
		c.add("a");
		c.add("a");
		c.add("a");

		Equivalence.equivalent(a, b, c);
	}
}
