package com.jeroensteenbeeke.hyperion.test;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.jeroensteenbeeke.hyperion.test.matcher.ListMatchers.isList;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class ListMatchersTest {
	@Test
	public void testListEquivalence() {
		List<String> ab = Arrays.asList("a", "b");
		List<String> abc = Arrays.asList("a", "b", "c");
		List<String> cba = Arrays.asList("c", "b", "a");

		assertThat(ab, isList("a", "b"));
		assertThat(abc, isList("a", "b", "c"));
		assertThat(abc, not(isList("a", "b")));
		assertThat(ab, not(isList("a", "b", "c")));
		assertThat(cba, not(isList("a", "b", "c")));
	}
}
