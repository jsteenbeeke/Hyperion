# Scenario 2: Multimodule project

Using these instructions, you can create a multi-module Maven project with:

* A JAX-RS API project
* An automatically generated Retrofit API project for Android
* A REST backend that uses:
  * JAX-RS ([Resteasy](http://resteasy.jboss.org/))
  * [Spring](http://spring.io)
  * JPA ([Hibernate](https://hibernate.org))
* A Wicket frontend, with:
  * Client-side JAX-RS ([Resteasy](http://resteasy.jboss.org/))
  
This project will be outfitted with a whole array of code generators to make your life easier.

## Creating the project

To create the project, your Maven installation will need to be configured to use the
[repo.jeroensteenbeeke.nl](https://repo.jeroensteenbeeke.nl) repository. 

Once you have this configured, use the following Maven quickstart command:

```bash
mvn archetype:generate \
        -DarchetypeGroupId=com.jeroensteenbeeke \
        -DarchetypeArtifactId=hyperion-quickstart-bs-multimodule \
        -DarchetypeVersion=2.0-SNAPSHOT
```
