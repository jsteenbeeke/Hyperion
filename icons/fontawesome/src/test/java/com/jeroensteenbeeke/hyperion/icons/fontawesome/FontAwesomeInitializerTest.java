package com.jeroensteenbeeke.hyperion.icons.fontawesome;

import org.apache.wicket.mock.MockApplication;
import org.apache.wicket.mock.MockHomePage;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class FontAwesomeInitializerTest {
	@Test
	public void runInitializer() {
		WicketTester tester = new WicketTester(new TestApplication());
		tester.startPage(MockHomePage.class);
		tester.assertRenderedPage(MockHomePage.class);

		tester.assertContains("fontawesome.css");
	}
}

class TestApplication extends MockApplication {
	@Override
	protected void init() {
		super.init();
		FontAwesomeInitializer.get().initialize(this);
	}
}
