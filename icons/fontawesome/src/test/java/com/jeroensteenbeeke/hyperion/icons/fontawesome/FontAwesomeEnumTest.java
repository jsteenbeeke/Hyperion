package com.jeroensteenbeeke.hyperion.icons.fontawesome;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FontAwesomeEnumTest {
	@Test
	public void testValuesGenerated() {
		FontAwesome[] icons = FontAwesome.values();
		assertTrue(icons.length > 0);

		for (FontAwesome icon: icons) {
			assertNotNull(String.format("Icon %s has CSS defined", icon.name()), icon
					.getCssClasses());
			assertFalse(String.format("Icon %s has CSS defined", icon.name()), icon.getCssClasses()
																			 .isEmpty());
		}
	}

}
