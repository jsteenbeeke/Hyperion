package com.jeroensteenbeeke.hyperion.icons.fontawesome;

import com.jeroensteenbeeke.hyperion.icons.IconCssContributorListener;
import com.jeroensteenbeeke.hyperion.icons.IconInitializer;
import org.apache.wicket.protocol.http.WebApplication;

import javax.annotation.Nonnull;

/**
 * Icon initializer for FontAwesome icons
 */
public class FontAwesomeInitializer implements IconInitializer {
	private static final FontAwesomeInitializer INSTANCE = new FontAwesomeInitializer();

	@Override
	public void initialize(@Nonnull WebApplication application) {
		application.getHeaderContributorListeners().add(
				new IconCssContributorListener(
						FontAwesomeCssReference.FONTAWESOME,
						FontAwesomeCssReference.FONTAWESOME_REGULAR,
						FontAwesomeCssReference.FONTAWESOME_BRANDS,
						FontAwesomeCssReference.FONTAWESOME_SOLID
						));
		application.mountResource("css/fontawesome.css", FontAwesomeCssReference.FONTAWESOME);
		application.mountResource("css/fontawesome-regular.css", FontAwesomeCssReference.FONTAWESOME_REGULAR);
		application.mountResource("css/fontawesome-brands.css", FontAwesomeCssReference.FONTAWESOME_BRANDS);
		application.mountResource("css/fontawesome-solid.css", FontAwesomeCssReference.FONTAWESOME_SOLID);

		FontAwesomeFile.FA_BRANDS_EOT.mount(application);
		FontAwesomeFile.FA_BRANDS_SVG.mount(application);
		FontAwesomeFile.FA_BRANDS_TTF.mount(application);
		FontAwesomeFile.FA_BRANDS_WOFF.mount(application);
		FontAwesomeFile.FA_BRANDS_WOFF2.mount(application);
		FontAwesomeFile.FA_REGULAR_EOT.mount(application);
		FontAwesomeFile.FA_REGULAR_SVG.mount(application);
		FontAwesomeFile.FA_REGULAR_TTF.mount(application);
		FontAwesomeFile.FA_REGULAR_WOFF.mount(application);
		FontAwesomeFile.FA_REGULAR_WOFF2.mount(application);
		FontAwesomeFile.FA_SOLID_EOT.mount(application);
		FontAwesomeFile.FA_SOLID_SVG.mount(application);
		FontAwesomeFile.FA_SOLID_TTF.mount(application);
		FontAwesomeFile.FA_SOLID_WOFF.mount(application);
		FontAwesomeFile.FA_SOLID_WOFF2.mount(application);
	}

	/**
	 * Get a singleton instance of the FontAwesome initializer
	 * @return The initializer
	 */
	public static synchronized FontAwesomeInitializer get() {
		return INSTANCE;
	}
}
