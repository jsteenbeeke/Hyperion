package com.jeroensteenbeeke.hyperion.icons.fontawesome;

import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.PackageResourceReference;

/**
 * Reference to resource files required by fontawesome
 */
public class FontAwesomeFile extends PackageResourceReference {
	/**
	 * EOT file for FontAwesome Brands
	 */
	public static final FontAwesomeFile FA_BRANDS_EOT = new FontAwesomeFile("fa-brands-400.eot");
	/**
	 * SVG file for FontAwesome Brands
	 */
	public static final FontAwesomeFile FA_BRANDS_SVG = new FontAwesomeFile("fa-brands-400.svg");
	/**
	 * TTF file for FontAwesome Brands
	 */
	public static final FontAwesomeFile FA_BRANDS_TTF = new FontAwesomeFile("fa-brands-400.ttf");
	/**
	 * WOFF file for FontAwesome Brands
	 */
	public static final FontAwesomeFile FA_BRANDS_WOFF = new FontAwesomeFile("fa-brands-400.woff");
	/**
	 * WOFF2 file for FontAwesome Brands
	 */
	public static final FontAwesomeFile FA_BRANDS_WOFF2 = new FontAwesomeFile
			("fa-brands-400.woff2");

	/**
	 * EOT file for FontAwesome Regular
	 */
	public static final FontAwesomeFile FA_REGULAR_EOT = new FontAwesomeFile("fa-regular-400.eot");
	/**
	 * SVG file for FontAwesome Regular
	 */
	public static final FontAwesomeFile FA_REGULAR_SVG = new FontAwesomeFile("fa-regular-400.svg");
	/**
	 * TTF file for FontAwesome Regular
	 */
	public static final FontAwesomeFile FA_REGULAR_TTF = new FontAwesomeFile("fa-regular-400.ttf");
	/**
	 * WOFF file for FontAwesome Regular
	 */
	public static final FontAwesomeFile FA_REGULAR_WOFF = new FontAwesomeFile
			("fa-regular-400.woff");
	/**
	 * WOFF2 file for FontAwesome Regular
	 */
	public static final FontAwesomeFile FA_REGULAR_WOFF2 = new FontAwesomeFile
			("fa-regular-400.woff2");

	/**
	 * EOT file for FontAwesome Solid
	 */
	public static final FontAwesomeFile FA_SOLID_EOT = new FontAwesomeFile("fa-solid-900.eot");
	/**
	 * SVG file for FontAwesome Solid
	 */
	public static final FontAwesomeFile FA_SOLID_SVG = new FontAwesomeFile("fa-solid-900.svg");
	/**
	 * TTF file for FontAwesome Solid
	 */
	public static final FontAwesomeFile FA_SOLID_TTF = new FontAwesomeFile("fa-solid-900.ttf");
	/**
	 * WOFF file for FontAwesome Solid
	 */
	public static final FontAwesomeFile FA_SOLID_WOFF = new FontAwesomeFile("fa-solid-900.woff");
	/**
	 * WOFF2 file for FontAwesome Solid
	 */
	public static final FontAwesomeFile FA_SOLID_WOFF2 = new FontAwesomeFile("fa-solid-900.woff2");

	private FontAwesomeFile(String name) {
		super(FontAwesomeFile.class, String.format("fonts/%s", name));
	}

	/**
	 * Mount this reference on the given application
	 * @param application The application to mount the reference on
	 */
	void mount(WebApplication application) {
		application.mountResource("web".concat(getName()), this);
	}
}
