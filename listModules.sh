#!/bin/bash

for i in $(find * | \
	grep pom.xml$ | \
	grep -v archetype-resources | \
	grep -v target | \
	xargs cat | \
	grep artifactId | \
	grep hyperion- | \
	tr -d '[:blank:]' | \
	sed 's_<artifactId>__g' | \
	sed 's_</artifactId>__g' | \
	grep -v parent$ | \
	sort | \
	uniq); do
	echo "<dependency>" 
	echo "	<groupId>com.jeroensteenbeeke</groupId>"
	echo "	<artifactId>$i</artifactId>"
	echo "	<version>\${hyperion.version}</version>"
	echo "</dependency>" 
done
