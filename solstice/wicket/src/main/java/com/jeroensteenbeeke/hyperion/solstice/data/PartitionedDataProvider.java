/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;

import com.google.common.collect.Iterators;
import com.jeroensteenbeeke.hyperion.data.DomainObject;

/**
 * DataProvider that partitions the results of another dataprovider into segments
 * of the indicated size.
 *
 * Suppose you had a DataProvider that returns the integers 1 to 10, and you'd partition
 * that with size 3, then your result would be: [[1,2,3],[4,5,6],[7,8,9],[10]].
 *
 * This technique is generally used for display purposes: to achieve the effect of 3 elements of the
 * same type on a single row (though in modern CSS libraries there is generally no need to).
 *
 * @param <T> The type entity returned by the dataprovider
 */
public class PartitionedDataProvider<T extends DomainObject> implements
		IDataProvider<List<T>> {
	private static final long serialVersionUID = 1L;

	private final IDataProvider<T> delegate;

	private final int listSize;

	/**
	 * Constructor
	 * @param delegate The dataprovider whose result to partition
	 * @param listSize The (max) number of results in each partition
	 */
	public PartitionedDataProvider(IDataProvider<T> delegate, int listSize) {
		super();
		this.delegate = delegate;
		this.listSize = listSize;
	}

	@Override
	public void detach() {
		delegate.detach();
	}

	@Override
	public Iterator<List<T>> iterator(long first, long count) {
		@SuppressWarnings("unchecked")
		Iterator<T> base = (Iterator<T>) delegate.iterator(listSize * first,
				count * listSize);

		return Iterators.partition(base, listSize);
	}

	@Override
	public IModel<List<T>> model(List<T> object) {
		return ModelMaker.wrap(object);
	}

	@Override
	public long size() {
		return new BigDecimal(delegate.size()).divide(new BigDecimal(listSize),
				0, RoundingMode.CEILING).longValue();
	}
}
