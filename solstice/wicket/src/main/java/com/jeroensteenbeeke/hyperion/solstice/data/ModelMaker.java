/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import io.vavr.collection.Array;
import io.vavr.collection.Queue;
import io.vavr.collection.Seq;
import io.vavr.collection.Vector;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.ListModel;

import com.google.common.collect.Lists;
import com.jeroensteenbeeke.hyperion.data.DomainObject;

/**
 * Factory class for creating Hibernate-compatible Wicket models
 *
 * @author Jeroen Steenbeeke
 */
public abstract class ModelMaker {
	/**
	 * Wraps the given entity in a Wicket model
	 * @param entity The entity to wrap
	 * @param <T> The type of entity
	 * @return A writable IModel containing the entity
	 */
	public static <T extends DomainObject> IModel<T> wrap(T entity) {
		return new PersistenceModel<>(entity);
	}

	/**
	 * Wraps a list of entities in a read-only Wicket model. Shorthand for {@code wrapList}
	 * @param list The list
	 * @param <T> The type of entity
	 * @return A read-only IModel containing the list
	 */
	public static <T extends DomainObject> IModel<List<T>> wrap(List<T> list) {
		return wrapList(list);
	}

	/**
	 * Wraps a list of entities in a writable Wicket model
	 * @param list The list
	 * @param <T> The type of entity
	 * @return A writable IModel containing the list
	 */
	public static <T extends DomainObject> IModel<List<T>> wrapWritable(List<T> list) {
		return new WritableEntityListModelAdapter<T>(list);
	}

	/**
	 * Wraps a list of entities in a read-only Wicket model
	 * @param list The list of entities
	 * @param <T> The type of entity
	 * @return A read-only list model
	 */
	public static <T extends DomainObject> IModel<List<T>> wrapList(
			List<T> list) {
		return new EntityListModel<>(list);
	}

	/**
	 * Wraps a Vavr sequence in a read-only Wicket model
	 * @param seq The sequence
	 * @param <T> The type of entity
	 * @return A read-only sequence model
	 */
	public static <T extends DomainObject> IModel<? extends Seq<T>> wrapSeq(
			Seq<T> seq) {
		if (seq instanceof Array) {
			return EntitySeqModel.ofArray((Array<T>) seq);
		} else if (seq instanceof io.vavr.collection.List) {
			return EntitySeqModel.ofList((io.vavr.collection.List<T>) seq);
		} else if (seq instanceof Queue) {
			return EntitySeqModel.ofQueue((Queue<T>) seq);
		} else if (seq instanceof Vector) {
			return EntitySeqModel.ofVector((Vector<T>) seq);
		} else {
			return EntitySeqModel.ofSeq(seq);
		}
	}

	/**
	 * Wraps an empty model of the given entity class
	 * @param domainObjectClass The class to wrap
	 * @param <T> The type of entity normally encapsulated in this model
	 * @return An empty writable model
	 */
	public static <T extends DomainObject> IModel<T> wrap(
			Class<T> domainObjectClass) {
		return new PersistenceModel<>(domainObjectClass);
	}

	/**
	 * Creates a List model for all values of the given enum class
	 * @param enumClass The enum class to get the values of
	 * @param <T> The enum type
	 * @return A read-only model of the enum values
	 */
	public static <T extends Enum<T>> IModel<? extends List<T>> forEnum(
			Class<T> enumClass) {
		try {
			Method method = enumClass.getMethod("values");
			@SuppressWarnings("unchecked")
			T[] values = (T[]) method.invoke(null);
			List<T> list = Lists.newArrayList(values);

			return () -> list;
		} catch (NoSuchMethodException | SecurityException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new IllegalStateException("Enum without values() method");
		}
	}
}
