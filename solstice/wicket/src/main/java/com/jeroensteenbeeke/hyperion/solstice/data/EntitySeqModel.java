package com.jeroensteenbeeke.hyperion.solstice.data;

import com.jeroensteenbeeke.hyperion.data.BaseEntityFinder;
import com.jeroensteenbeeke.hyperion.data.DomainObject;
import io.vavr.Function0;
import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.collection.*;
import io.vavr.control.Option;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.hibernate.Hibernate;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Optional;

/**
 * Model that wraps a Vavr sequence of entities
 * @param <T> The type of entity
 * @param <S> The type of sequence
 */
class EntitySeqModel<T extends DomainObject, S extends Seq<T>> implements IModel<S> {
	private static final long serialVersionUID = 1L;

	private Class<T> entityClass;

	private Seq<Serializable> ids;

	@Inject
	private BaseEntityFinder entityFinder;

	private transient S seq;

	private final Function0<S> newSeq;

	private final Function2<S,T,S> appendSeq;

	@SuppressWarnings("unchecked")
	private EntitySeqModel(@Nonnull S seq,
						   @Nonnull Function0<S> newSeq,
						   @Nonnull Function2<S,T,S> appendSeq) {
		this.seq = seq;
		this.ids = Array.empty();
		for (Serializable serializable : seq.map(DomainObject::getDomainObjectId)) {
			this.ids = this.ids.append(serializable);
		}

		this.newSeq = newSeq;
		this.appendSeq = appendSeq;
		this.entityClass = !seq.isEmpty() && seq.get(0) != null ? Hibernate.getClass(seq.get(0)) : null;
		for (T t : seq) {
			Class<T> tClass = Hibernate.getClass(t);
			if (entityClass == null) {
				entityClass = tClass;
			}else if (!entityClass.isAssignableFrom(tClass)) {
				if (entityClass.getSuperclass() != null
						&& entityClass.getSuperclass().isAssignableFrom(tClass)) {
					entityClass = (Class<T>) entityClass.getSuperclass();
				}
			}
		}

		detach(); // Hibernate bug
	}

	@Override
	public void detach() {
		this.entityFinder = null;
		this.seq = null;
	}

	@Override
	public S getObject() {
		if (seq == null) {
			seq = load();
		}

		return seq;
	}

	@Override
	public void setObject(S object) {
		throw new UnsupportedOperationException();
	}

	private S load() {
		if (entityFinder == null) {
			Injector.get().inject(this);
		}

		if (entityClass != null) {
			S seq = newSeq.apply();

			for (Serializable id : ids) {
				Option<T> entity = entityFinder.getEntity(entityClass, id);
				seq = entity.isDefined() ? appendSeq.apply(seq, entity.get()) : seq;
			}

			return seq;
		}

		return newSeq.apply();
	}

	/**
	 * Creates a new model for a List
	 * @param list The list to wrap
	 * @param <E> The type of entity in the list
	 * @return A read-only model of the List
	 */
	static <E extends DomainObject> EntitySeqModel<E,List<E>> ofList(
			@Nonnull List<E> list) {
		return new EntitySeqModel<>(list, List::empty, List::append);
	}

	/**
	 * Creates a new model for a Queue
	 * @param queue The queue to wrap
	 * @param <E> The type of entity in the queue
	 * @return A read-only model of the Queue
	 */
	static <E extends DomainObject> EntitySeqModel<E,Queue<E>> ofQueue(
			@Nonnull Queue<E> queue) {
		return new EntitySeqModel<>(queue, Queue::empty, Queue::append);
	}

	/**
	 * Creates a new model for an Array
	 * @param array The array to wrap
	 * @param <E> The type of entity in the array
	 * @return A read-only model of the Array
	 */

	static <E extends DomainObject> EntitySeqModel<E,Array<E>> ofArray(
			@Nonnull Array<E> array) {
		return new EntitySeqModel<>(array, Array::empty, Array::append);
	}

	/**
	 * Creates a new model for a Vector
	 * @param vector The vector to wrap
	 * @param <E> The type of entity in the vector
	 * @return A read-only model of the Vector
	 */

	static <E extends DomainObject> EntitySeqModel<E,Vector<E>> ofVector(
			@Nonnull Vector<E> vector) {
		return new EntitySeqModel<>(vector, Vector::empty, Vector::append);
	}

	/**
	 * Creates a new model for a sequence of undefined type, converting it to a linked List. Does not work too well
	 * with infinite streams and such
	 * @param seq The sequence to wrap
	 * @param <T> The type of entity in the sequence
	 * @return A read-only model of the sequence
	 */
	static <T extends DomainObject> IModel<? extends Seq<T>> ofSeq(Seq<T> seq) {
		return new EntitySeqModel<>(seq, List::empty, Seq::append);
	}

}
