/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data.factory;

import java.util.List;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.web.IEntityEncapsulatorFactory;
import com.jeroensteenbeeke.hyperion.solstice.data.FilterDataProvider;
import com.jeroensteenbeeke.hyperion.solstice.data.ModelMaker;

import javax.annotation.Nonnull;

/**
 * Implementation of IEntityEncapsulatorFactory that wraps entities using
 * FilterDataProvider, PersistenceModel and EntityListModel
 */
public class SolsticeEntityEncapsulatorFactory implements IEntityEncapsulatorFactory {
	@Nonnull
	@Override
	public <T extends DomainObject, F extends SearchFilter<T, F>> IDataProvider<T> createDataProvider(@Nonnull F filter, @Nonnull DAO<T, F> dao) {
		return FilterDataProvider.of(filter, dao);
	}

	@Nonnull
	@Override
	public <T extends DomainObject> IModel<T> createModel(@Nonnull T object) {
		return ModelMaker.wrap(object);
	}
	
	@Override
	public <T extends DomainObject> IModel<T> createModel(@Nonnull Class<T> modelClass) {
		return ModelMaker.wrap(modelClass);
	}

	@Override
	@Nonnull
	public <T extends DomainObject> IModel<List<T>> createListModel(@Nonnull List<T> list) {
		return ModelMaker.wrapList(list);
	}
	
	

}
