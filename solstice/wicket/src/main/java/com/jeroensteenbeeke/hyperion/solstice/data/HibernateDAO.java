/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.solstice.dao.common.BaseHibernateDAO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * A Data Access Object that uses the Hibernate ORM package
 *
 * @param <T> The domain object loadable by this DAO
 * @param <F> The type of filter to use with this DAO
 * @author Jeroen Steenbeeke
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public abstract class HibernateDAO<T extends DomainObject,F extends SearchFilter<T,F>> extends BaseHibernateDAO<T,F> implements DAO<T,F> {


}
