/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.Optional;

import io.vavr.control.Option;
import org.apache.wicket.model.IModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jeroensteenbeeke.hyperion.data.BaseEntityFinder;
import com.jeroensteenbeeke.hyperion.data.DomainObject;

import javax.annotation.Nonnull;

public class TestPersistenceModel {
	private SessionFactory sf;

	private Session current;

	@Before
	public void initHibernate() {
		Configuration config = new Configuration();
		config = config.addAnnotatedClass(Lolcat.class);

		config = config.setProperty("hibernate.hbm2ddl.auto", "create");
		config = config.setProperty("hibernate.dialect",
				"org.hibernate.dialect.H2Dialect");
		config = config.setProperty("hibernate.show_sql", "true");
		config = config.setProperty("hibernate.connection.driver_class",
				"org.h2.Driver");
		config = config.setProperty("hibernate.connection.url",
				"jdbc:h2:mem:hyperion;DB_CLOSE_DELAY=-1");
		config = config.setProperty("hibernate.connection.username", "sa");
		config = config.setProperty("hibernate.connection.password", "");
		config = config.setProperty("hibernate.connection.pool_size", "50");
		config = config.setProperty("hibernate.connection.provider_class",
				"org.hibernate.connection.DriverManagerConnectionProvider");
		// config = config.setProperty("hibernate.c3p0.min_size", "5");
		// config = config.setProperty("hibernate.c3p0.max_size", "20");
		// config = config.setProperty("hibernate.c3p0.timeout", "1800");
		// config = config.setProperty("hibernate.c3p0.max_statements", "50");

		final ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(config.getProperties()).build();

		sf = config.buildSessionFactory(serviceRegistry);

	}

	@Test
	public void testPersistenceModel() {
		current = sf.openSession();
		Transaction t = current.beginTransaction();

		Lolcat t1 = new Lolcat();
		Lolcat t2 = new Lolcat();

		t2.setParent(t1);

		current.save(t1);
		current.save(t2);
		current.flush();

		IModel<Lolcat> model = new PersistenceModel<Lolcat>(t1) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void injectDAO() {
				setDao(new BaseEntityFinder() {

					@Nonnull @Override
					public <T extends DomainObject> Option<T> getEntity(
							@Nonnull Class<T> entityClass, @Nonnull Serializable id) {
						return Option.of(current.load(entityClass, id));
					}
				});
			}

		};

		t.commit();
		current.close();

		model.detach();

		current = sf.openSession();

		assertEquals(1, model.getObject().getChildren().size());

		current.close();
	}

	@After
	public void tearItAllDown() {
		sf.close();
	}
}
