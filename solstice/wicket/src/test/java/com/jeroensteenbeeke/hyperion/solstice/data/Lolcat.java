/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;

@Entity
public class Lolcat extends BaseDomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Lolcat parent;

	@OneToMany(mappedBy = "parent")
	private List<Lolcat> children;

	@Override
	public Long getDomainObjectId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lolcat getParent() {
		return parent;
	}

	public void setParent(Lolcat parent) {
		this.parent = parent;
	}

	public List<Lolcat> getChildren() {
		return children;
	}

	public void setChildren(List<Lolcat> children) {
		this.children = children;
	}

}
