package com.jeroensteenbeeke.hyperion.solstice;

import com.jeroensteenbeeke.hyperion.solstice.spring.db.EnableSolstice;
import com.jeroensteenbeeke.hyperion.solstice.spring.db.SolsticeDatabaseConfiguration;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import static org.junit.Assert.assertNotNull;

public class TestEnableSolsticeAnnotation {
	@Test
	public void createConfig() {
		System.setProperty("database.type", "h2");
		System.setProperty("database.poolsize", "5");
		System.setProperty("database.timeout", "30000");
		System.setProperty("database.url", "jdbc:h2:mem:TestEnableSolsticeAnnotation");

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext
				(TestConfig.class);
		context.start();

		assertNotNull(context.getBean(SolsticeDatabaseConfiguration.class));
	}

	@Configuration
	@EnableSolstice(entityBasePackage = "com.jeroensteenbeeke.hyperion.solstice.spring")
	public static class TestConfig {

	}
}
