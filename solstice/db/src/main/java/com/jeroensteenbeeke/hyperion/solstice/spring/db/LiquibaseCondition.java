package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import javax.annotation.Nonnull;

/**
 * Spring condition for activating Liquibase functionality
 */
public class LiquibaseCondition implements Condition {
	@Override
	public boolean matches(@Nonnull ConditionContext context,
						   @Nonnull AnnotatedTypeMetadata metadata) {
		ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();

		if (beanFactory == null) {
			return false;
		}

		SolsticeDatabaseConfiguration databaseConfiguration =
				beanFactory.getBean(SolsticeDatabaseConfiguration.class);

		return databaseConfiguration.getLiquibaseChangelog().filter(c -> !c.equals("")).isDefined();
	}
}
