package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Spring activator annotation that enables Solstice (Spring + Hibernate/JPA with Liquibase) functionality
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(SolsticeImportBeanHandler.class)
public @interface EnableSolstice {
	/**
	 * The base package in which the JPA entities are defined
	 * @return A fully qualified package name
	 */
	String entityBasePackage();

	/**
	 * Optional: the location of the main liquibase changelog file
	 *
	 * Example: classpath:/your/base/package/entities/liquibase/db.changelog-master.xml
	 *
	 * @return The liquibase changelog path.
	 */
	String liquibaseChangelog() default "";
}
