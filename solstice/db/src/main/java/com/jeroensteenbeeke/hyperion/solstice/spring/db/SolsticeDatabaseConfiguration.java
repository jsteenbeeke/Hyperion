package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import io.vavr.control.Option;

import javax.annotation.Nonnull;

/**
 * DTO for database configuration, as specified by the {@code @EnableSolstice} annotation
 */
public class SolsticeDatabaseConfiguration {
	private final String entityBasePackage;

	private final String liquibaseChangelog;

	/**
	 * Constructor
	 * @param entityBasePackage the entity base package
	 * @param liquibaseChangelog The location of the liquibase changelog
	 */
	SolsticeDatabaseConfiguration(String entityBasePackage, String liquibaseChangelog) {
		this.entityBasePackage = entityBasePackage;
		this.liquibaseChangelog = liquibaseChangelog;
	}

	/**
	 * Gets the base (root) package entities are defined in
	 * @return The fully qualified domain name of the package
	 */
	@Nonnull
	public String getEntityBasePackage() {
		return entityBasePackage;
	}

	/**
	 * Gets the location of the Liquibase changelog
	 * @return Optionally the changelog
	 */
	@Nonnull
	public Option<String> getLiquibaseChangelog() {
		return Option.of(liquibaseChangelog);
	}
}
