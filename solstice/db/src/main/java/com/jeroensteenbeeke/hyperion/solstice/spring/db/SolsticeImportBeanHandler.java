package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import org.springframework.beans.factory.annotation.AnnotatedGenericBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Handler class for the {@code @EnableSolstice} annotation
 */
class SolsticeImportBeanHandler implements ImportBeanDefinitionRegistrar {

	private static final String SOLSTICE_ANNOTATION_FQDN =
			"com.jeroensteenbeeke.hyperion.solstice.spring.db.EnableSolstice";

	@Override
	public void registerBeanDefinitions(@Nonnull AnnotationMetadata importingClassMetadata,
												  @Nonnull BeanDefinitionRegistry registry) {


		if (importingClassMetadata.hasAnnotation(SOLSTICE_ANNOTATION_FQDN)) {
			Map<String, Object> solsticeConfig =
					importingClassMetadata.getAnnotationAttributes(SOLSTICE_ANNOTATION_FQDN);

			if (solsticeConfig != null) {
				SolsticeDatabaseConfiguration config = new SolsticeDatabaseConfiguration
						((String) solsticeConfig.get("entityBasePackage"), (String)
								solsticeConfig.get("liquibaseChangelog"));

				AnnotatedGenericBeanDefinition beandef = new AnnotatedGenericBeanDefinition
						(SolsticeDatabaseConfiguration.class);
				beandef.setInstanceSupplier(() -> config);
				registry.registerBeanDefinition("solstice-data-config", beandef);

				registry.registerBeanDefinition("solstice-config-class", new RootBeanDefinition
						(SolsticeConfig.class));
			} else {
				throw new IllegalStateException("SolsticeImportBeanHandler used directly without " +
						"@EnableSolstice annotation");
			}
		} else {
			throw new IllegalStateException("SolsticeImportBeanHandler used directly without " +
					"@EnableSolstice annotation");
		}
	}
}
