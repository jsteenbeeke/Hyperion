package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import com.jeroensteenbeeke.hyperion.data.BaseEntityFinder;
import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.lux.TypedResult;
import io.vavr.control.Option;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Basic implementation of an EntityFinder. Used primarily by other Hyperion projects that work with Wicket and databases
 */
@Component("EntityFinder")
@Transactional(propagation = Propagation.NESTED, readOnly = true)
class EntityFinderImpl implements BaseEntityFinder {
	private EntityManager entityManager;

	EntityFinderImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Nonnull
	public <T extends DomainObject> Option<T> getEntity(
		@Nonnull Class<T> entityClass, @Nonnull Serializable id) {
		return Option.of(entityManager.find(entityClass, id)).map(e -> TypedResult.ok(e).map(ent -> {
			entityManager.refresh(ent);
			return ent;
		}).orElse(msg -> e));
	}
}
