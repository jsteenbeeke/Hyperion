package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import javax.annotation.Nonnull;

/**
 * Spring condition for enabling H2 database connectivity in Solstice
 */
public class H2Condition implements Condition {

	@Override
	public boolean matches(@Nonnull ConditionContext context,
						   @Nonnull AnnotatedTypeMetadata metadata) {
		return "h2".equals(context.getEnvironment().getProperty("database.type"));
	}

}
