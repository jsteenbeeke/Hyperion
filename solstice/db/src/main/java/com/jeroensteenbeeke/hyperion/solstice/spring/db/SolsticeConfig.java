package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import java.nio.file.Path;
import java.util.Properties;

import com.jeroensteenbeeke.hyperion.data.BaseEntityFinder;
import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationMetadataStore;
import liquibase.integration.spring.SpringLiquibase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.annotation.PostConstruct;
import javax.persistence.*;

/**
 * Spring configuration class for Solstice. Configures all the required beans for either a backend/service or a standalone
 * webapplication.
 */
@Configuration
public class SolsticeConfig {
	private static final Logger log = LoggerFactory.getLogger(SolsticeConfig.class);

	/**
	 * Notifies user that Solstice was configured
	 */
	@PostConstruct
	public void logInitialization() {
		log.info("Solstice configuration complete");
	}

	/**
	 * Create an application metadata store bean
	 *
	 * @param storeFile The file that contains the metadata
	 * @return A store bean
	 */
	@Bean
	@Conditional(WritableHyperionConfigDirCondition.class)
	public ApplicationMetadataStore metadata(@Value("${hyperion.configdir:${user.home}/.hyperion}/" + ApplicationMetadataStore.STORE_FILENAME) String storeFile) {
		return new ApplicationMetadataStore(Path.of(storeFile).toFile());
	}

	/**
	 * Creates a HikariCP datasource if H2 is enabled
	 *
	 * @param databasePoolsize The poolsize for the database
	 * @param databaseTimeout  The connection timeout for the database
	 * @param databaseUrl      The URL at which the database is reachable
	 * @param databaseUsername The username with which to log in
	 * @param databasePassword The password with which to log in
	 * @return A HikariDataSource that connects to H2
	 */
	@Bean
	@Conditional(H2Condition.class)
	public HikariDataSource h2DataSource(
		@Value("${database.poolsize}") int databasePoolsize,
		@Value("${database.timeout}") int databaseTimeout,
		@Value("${database.url}") String databaseUrl,
		@Value("${database.username}") String databaseUsername,
		@Value("${database.password}") String databasePassword) {
		HikariConfig config = new HikariConfig();
		config.setPoolName("springHikariCP");
		config.setAutoCommit(false);
		config.setConnectionTestQuery("SELECT 1");
		config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
		config.setMaximumPoolSize(databasePoolsize);
		config.setIdleTimeout(databaseTimeout);

		config.setUsername(databaseUsername);
		config.setPassword(databasePassword);
		config.setJdbcUrl(databaseUrl);

		Properties props = new Properties();
		props.put("url", databaseUrl);
		config.setDataSourceProperties(props);

		return new HikariDataSource(config);
	}

	/**
	 * Creates a JPA vendor adapter for use with H2
	 *
	 * @param databaseShowSQL Whether or not SQL statements should be logged
	 * @return A JpaVendorAdapter
	 */
	@Bean
	@Conditional(H2Condition.class)
	public JpaVendorAdapter h2Adapter(
		@Value("${database.showsql:false}") boolean databaseShowSQL) {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.H2Dialect");
		vendorAdapter.setShowSql(databaseShowSQL);
		vendorAdapter.getJpaPropertyMap().put("hibernate.connection.release_mode", "after_transaction");
		return vendorAdapter;
	}

	/**
	 * Configures a HikariCP datasource if Postgres is enabled
	 *
	 * @param databasePoolsize The poolsize for the database
	 * @param databaseTimeout  The connection timeout for the database
	 * @param databaseUrl      The URL at which the database is reachable
	 * @param databaseUsername The username with which to log in
	 * @param databasePassword The password with which to log in
	 * @return A HikariDataSource that connects to PostgreSQL
	 */
	@Bean
	@Conditional(PostgreSQLCondition.class)
	public HikariDataSource pgDataSource(
		@Value("${database.poolsize}") int databasePoolsize,
		@Value("${database.timeout}") int databaseTimeout,
		@Value("${database.url}") String databaseUrl,
		@Value("${database.username}") String databaseUsername,
		@Value("${database.password}") String databasePassword) {
		HikariConfig config = new HikariConfig();
		config.setPoolName("springHikariCP");
		config.setAutoCommit(false);
		config.setConnectionTestQuery("SELECT 1");
		config.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
		config.setMaximumPoolSize(databasePoolsize);
		config.setIdleTimeout(databaseTimeout);

		Properties props = new Properties();
		props.put("url", databaseUrl);
		props.put("user", databaseUsername);
		props.put("password", databasePassword);

		config.setDataSourceProperties(props);

		return new HikariDataSource(config);
	}

	/**
	 * Creates a JPA vendor adapter for use with PostgreSQL
	 *
	 * @param databaseShowSQL Whether or not SQL statements should be logged
	 * @return A JpaVendorAdapter
	 */
	@Bean
	@Conditional(PostgreSQLCondition.class)
	public JpaVendorAdapter postgresAdapter(
		@Value("${database.showsql:false}") boolean databaseShowSQL) {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter
			.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");
		vendorAdapter.setShowSql(databaseShowSQL);
		vendorAdapter.getJpaPropertyMap().put("hibernate.connection.release_mode", "after_transaction");

		return vendorAdapter;
	}

	/**
	 * Creates a Transaction Manager
	 *
	 * @param entityManagerFactory The factory for entity managers
	 * @return A transaction manager
	 */
	@Bean
	public JpaTransactionManager transactionManager(
		EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager manager = new JpaTransactionManager();
		manager.setEntityManagerFactory(entityManagerFactory);

		return manager;
	}

	/**
	 * Creates the entity manager factory
	 *
	 * @param databaseDialect The database dialect
	 * @param databaseShowSQL Whether or not SQL should be logged
	 * @param configuration   The configuration that was read from the annotations
	 * @param dataSource      The datasource for the database
	 * @return An EntityManagerFactory
	 */
	@Bean("entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
		@Value("${database.dialect}") String databaseDialect,
		@Value("${database.showsql:false}") boolean databaseShowSQL,
		SolsticeDatabaseConfiguration configuration,
		HikariDataSource dataSource) {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(dataSource);
		factory.setPackagesToScan(configuration.getEntityBasePackage());

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabasePlatform(databaseDialect);
		vendorAdapter.setShowSql(databaseShowSQL);
		factory.setJpaVendorAdapter(vendorAdapter);

		return factory;
	}

	/**
	 * Optionally create a liquibase instance
	 *
	 * @param liquibaseContexts The contexts we are running under
	 * @param configuration     The configuration that was read from the annotations
	 * @param dataSource        The datasource for the database
	 * @return A SpringLiquibase instance
	 */
	@Bean
	@Conditional(LiquibaseCondition.class)
	public SpringLiquibase liquibase(
		@Value("${liquibase.contexts:production}") String liquibaseContexts,
		SolsticeDatabaseConfiguration configuration,
		HikariDataSource dataSource) {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		configuration.getLiquibaseChangelog().peek(liquibase::setChangeLog);
		liquibase.setContexts(liquibaseContexts);

		return liquibase;
	}

	/**
	 * Creates a new EntityManager that flushes on commit by default (manual flushes also possible)
	 *
	 * @param factory The EntityManagerFactory
	 * @return An EntityManager
	 */
	@Bean
	public EntityManager entityManager(EntityManagerFactory factory) {
		EntityManager entityManager = factory.createEntityManager();
		entityManager.setFlushMode(FlushModeType.COMMIT);
		return entityManager;
	}

	/**
	 * Creates an EntityFinder to locate entities by class and ID
	 *
	 * @param manager The EntityManager
	 * @return An EntityFinder
	 */
	@Bean("EntityFinder")
	public BaseEntityFinder entityFinder(EntityManager manager) {
		return new EntityFinderImpl(manager);
	}
}
