package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import java.util.Set;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;

/**
 * Spring condition that enables PostgreSQL as database in Solstice
 */
public class PostgreSQLCondition implements Condition {
	private static final Set<String> KEYS = ImmutableSet.of("pg", "postgres",
			"psql", "postgresql");

	@Override
	public boolean matches(@Nonnull ConditionContext context,
						   @Nonnull AnnotatedTypeMetadata metadata) {
		return KEYS.contains(context.getEnvironment().getProperty("database.type"));
	}

}
