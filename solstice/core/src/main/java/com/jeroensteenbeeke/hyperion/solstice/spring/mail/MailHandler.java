package com.jeroensteenbeeke.hyperion.solstice.spring.mail;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import io.vavr.control.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.jeroensteenbeeke.lux.ActionResult;

/**
 * Bean that offers a simplified API for sending e-mails
 */
public class MailHandler {
	private static final String SMTP_AUTH = "mail.smtp.auth";

	private static final String STARTTLS = "mail.smtp.starttls.enable";

	@Autowired(required = false)
	private MailCredentials credentials;

	private JavaMailSender mailSender;

	/**
	 * Returns the mail sender, if properly configured
	 *
	 * @return Optionally a mail sender
	 */
	public Option<JavaMailSender> getMailSender() {
		if (credentials == null) {
			return Option.none();
		}

		if (mailSender == null) {
			if (credentials.getHost().isEmpty()) {
				return Option.none();
			}

			JavaMailSenderImpl impl = new JavaMailSenderImpl();
			impl.setHost(credentials.getHost());
			impl.setPort(credentials.getPort());
			Option<String> username = credentials.getUsername();
			Option<String> password = credentials.getPassword();

			username.peek(impl::setUsername);
			password.peek(impl::setPassword);

			if (username.isDefined() && password.isDefined()) {
				impl.getJavaMailProperties().put(SMTP_AUTH, "true");
			}
			if (credentials.enableTLS()) {
				impl.getJavaMailProperties().put(STARTTLS, "true");
			}

			mailSender = impl;
			return Option.of(impl);
		}

		return Option.of(mailSender);
	}

	/**
	 * Sends a HTML mail with the given body
	 *
	 * @param body The mail body
	 * @return A builder
	 */
	public From sendHTML(String body) {
		return sender -> subject -> recipient -> internalSendMail(sender, recipient, subject, body, true);
	}

	/**
	 * Sends a plain text mail with the given body
	 *
	 * @param body The mail body
	 * @return A builder
	 */
	public From sendPlain(String body) {
		return sender -> subject -> recipient -> internalSendMail(sender, recipient, subject, body, false);
	}

	/**
	 * Builder
	 */
	public interface From {
		/**
		 * Specifies the sender (FROM:) of an e-mail
		 * @param sender The address ending the e-mail
		 * @return A builder
		 */
		@Nonnull
		WithSubject from(@Nonnull String sender);
	}

	/**
	 * Builder
	 */
	public interface WithSubject {
		/**
		 * Specifies the subject of an e-mail
		 * @param subject The subject of the e-mail
		 * @return A builder
		 */
		@Nonnull
		To withSubject(@Nonnull String subject);
	}

	/**
	 * Builder
	 */
	public interface To {
		/**
		 * Specifies the recipient (TO:) of an e-mail
		 * @param recipient The subject of the e-mail
		 * @return A builder
		 */
		@Nonnull
		ActionResult to(@Nonnull String recipient);
	}

	private ActionResult internalSendMail(String sender, String recipient, String subject, String body, boolean html) {
		return getMailSender().map(mailSender -> {
			MimeMessage msg = mailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(msg);

			try {
				helper.setFrom(sender);
				helper.addTo(recipient);
				helper.setSubject(subject);
				helper.setText(body, html);

				mailSender.send(msg);

				return ActionResult.ok();
			} catch (MailException | MessagingException ex) {
				return ActionResult.error(ex.getMessage());
			}

		}).getOrElse(() -> ActionResult.error("Mail not configured"));
	}
}
