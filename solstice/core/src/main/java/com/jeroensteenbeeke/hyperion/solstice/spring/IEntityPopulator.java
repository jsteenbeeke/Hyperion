/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.spring;

/**
 * Interface that defines a class as an EntityPopulator: a class that can create entities
 * on application start. Generally speaking this is used to build in-memory test databases,
 * though it can also be used bootstrapping production databases
 */
public interface IEntityPopulator {
	/**
	 * Method that is called to create entities. If this populator needs to run only once then you need
	 * to include the relevant integrity checks inside this method.
	 */
	void createEntities();

	/**
	 * The priority of the populator. Populators with lower numbers get run first. There is no additional sorting
	 * done so equal priorities lead to unpredictable execution order
	 * @return The priority
	 */
	int getPriority();

	/**
	 * Retrieves the description of this populator. Generally defaults to the classname
	 * @return The description of the populator
	 */
	default String getDescription() {
		return getClass().getSimpleName();
	}

	/**
	 * Proxy circumvention method to obtain the actual class of the populator
	 * @return The populator class
	 */
	default Class<? extends IEntityPopulator> getPopulatorClass() {
		return getClass();
	}
}
