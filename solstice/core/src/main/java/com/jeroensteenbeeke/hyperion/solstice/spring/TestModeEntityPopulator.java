/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.spring;

import java.util.Comparator;
import java.util.Map;

import com.jeroensteenbeeke.hyperion.util.Datasets;
import io.vavr.collection.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Executes entity populators on application startup (more specifically: application refresh)
 */
public class TestModeEntityPopulator implements
	ApplicationListener<ContextRefreshedEvent> {
	private static final Logger log = LoggerFactory
		.getLogger(TestModeEntityPopulator.class);

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.info("Running entity populators");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestAttributes attr = new ServletRequestAttributes(request);

		RequestContextHolder.setRequestAttributes(attr);

		Map<String, IEntityPopulator> beansOfType = event
			.getApplicationContext()
			.getBeansOfType(IEntityPopulator.class);

		List<IEntityPopulator> forCurrentDataset = List.ofAll(beansOfType.values())
													   .filter(this::isDatasetActive);


		log.info("Running {}/{} populators", forCurrentDataset.size(), beansOfType.size());

		List.ofAll(beansOfType.values())
			.filter(p -> !isDatasetActive(p))
			.forEach(populator -> log.info("\tSkipping {}", populator.getDescription()));

		forCurrentDataset
			.sorted(Comparator.comparing(IEntityPopulator::getPriority))
			.forEach(populator -> {
				log.info("\tRunning {}", populator.getDescription());
				populator.createEntities();
				log.info("\tCompleted {}", populator.getDescription());
			});

		RequestContextHolder.resetRequestAttributes();
	}

	private boolean isDatasetActive(IEntityPopulator populator) {
		return Datasets.INSTANCE.getActiveDataset().equals(Datasets.readFromClass(populator.getPopulatorClass()).orElse(Datasets.DEFAULT));

	}

}
