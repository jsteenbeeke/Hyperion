package com.jeroensteenbeeke.hyperion.solstice.spring;

import com.jeroensteenbeeke.lux.ActionResult;
import io.vavr.control.Option;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Persistent store of application metadata. Generally speaking this storage is located
 * in the same directory as the file defining properties
 */
public class ApplicationMetadataStore {
	public static final String STORE_FILENAME = "persistent-store.properties";
	private final File storefile;

	/**
	 * Constructor
	 *
	 * @param storefile The file where we store the metadata
	 */
	public ApplicationMetadataStore(File storefile) {
		if (!storefile.exists()) {
			// First, check if parent directory exists
			if (!storefile.getParentFile().exists()) {
				if (!storefile.getParentFile().mkdirs()) {
					throw new IllegalStateException("Application metadata store file does not exist, and neither did its parent directory. Attempt to create the directory failed");
				}
			}

			try (FileOutputStream fos = new FileOutputStream(storefile)) {
				new Properties().storeToXML(fos, "");
			} catch (IOException e) {
				throw new IllegalStateException("Application metadata store file does not exist and could not be created", e);
			}
		}

		this.storefile = storefile;
	}

	/**
	 * Writes the given String to the application Metadata
	 *
	 * @param key   The data's key
	 * @param value The value
	 * @return An ActionResult indicating success or the reason for failure
	 */
	public ActionResult writeString(@Nonnull String key, @Nonnull String value) {
		return writeProperty(key, Prefixes.String, value);
	}

	/**
	 * Writes the given Integer to the application Metadata
	 *
	 * @param key   The data's key
	 * @param value The value
	 * @return An ActionResult indicating success or the reason for failure
	 */
	public ActionResult writeInteger(@Nonnull String key, @Nonnull Integer value) {
		return writeProperty(key, Prefixes.Integer, value.toString());
	}

	/**
	 * Writes the given Boolean to the application Metadata
	 *
	 * @param key   The data's key
	 * @param value The value
	 * @return An ActionResult indicating success or the reason for failure
	 */
	public ActionResult writeBoolean(@Nonnull String key, @Nonnull Boolean value) {
		return writeProperty(key, Prefixes.Boolean, value.toString());
	}

	/**
	 * Writes the given Double to the application Metadata
	 *
	 * @param key   The data's key
	 * @param value The value
	 * @return An ActionResult indicating success or the reason for failure
	 */
	public ActionResult writeDouble(@Nonnull String key, @Nonnull Double value) {
		return writeProperty(key, Prefixes.Double, value.toString());
	}

	/**
	 * Reads application metadata with the given key, trying to parse it as an Integer
	 *
	 * @param key The key to search
	 * @return Optionally the property value
	 */
	public Option<String> readString(@Nonnull final String key) {
		return readProperty(key)
			.filter(v -> v.startsWith(Prefixes.String))
			.map(s -> s.substring(Prefixes.String.length()));
	}

	/**
	 * Reads application metadata with the given key, trying to parse it as an Integer
	 *
	 * @param key The key to search
	 * @return Optionally the property value
	 */
	public Option<Integer> readInteger(@Nonnull final String key) {
		return readProperty(key)
			.filter(v -> v.startsWith(Prefixes.Integer))
			.map(s -> s.substring(Prefixes.Integer.length()))
			.toTry()
			.mapTry(Integer::parseInt)
			.toOption();
	}

	/**
	 * Reads application metadata with the given key, trying to parse it as an Boolean
	 *
	 * @param key The key to search
	 * @return Optionally the property value
	 */
	public Option<Boolean> readBoolean(@Nonnull final String key) {
		return readProperty(key)
			.filter(v -> v.startsWith(Prefixes.Boolean))
			.map(s -> s.substring(Prefixes.Boolean.length()))
			.toTry()
			.mapTry(Boolean::parseBoolean)
			.toOption();
	}


	/**
	 * Reads application metadata with the given key, trying to parse it as a Double
	 *
	 * @param key The key to search
	 * @return Optionally the property value
	 */
	public Option<Double> readDouble(@Nonnull final String key) {
		return readProperty(key)
			.filter(v -> v.startsWith(Prefixes.Double))
			.map(s -> s.substring(Prefixes.Double.length()))
			.toTry()
			.mapTry(Double::parseDouble)
			.toOption();
	}


	private Option<String> readProperty(@Nonnull final String key) {
		synchronized (storefile) {
			Properties props = new Properties();
			try (FileInputStream fis = new FileInputStream(storefile)) {
				props.loadFromXML(fis);

				return Option.of(props.getProperty(key));
			} catch (IOException e) {
				return Option.none();
			}
		}
	}

	private ActionResult writeProperty(@Nonnull final String key, @Nonnull String prefix, @Nonnull final String value) {
		synchronized (storefile) {
			Properties props = new Properties();
			try (FileInputStream fis = new FileInputStream(storefile)) {
				props.loadFromXML(fis);
			} catch (IOException e) {
				return ActionResult.error(e.getMessage());
			}

			props.setProperty(key, prefix + value);

			try (FileOutputStream fos = new FileOutputStream(storefile)) {
				props.storeToXML(fos, "");
				fos.flush();
			} catch (IOException e) {
				return ActionResult.error(e.getMessage());
			}
			return ActionResult.ok();
		}
	}

	private static class Prefixes {
		private static final String Boolean = "$B$";

		private static final String Double = "$D$";

		private static final String Integer = "$I$";

		private static final String String = "$S$";
	}
}
