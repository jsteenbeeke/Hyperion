package com.jeroensteenbeeke.hyperion.solstice.spring.mail;

import java.util.Arrays;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.jeroensteenbeeke.hyperion.util.Asserts;
import io.vavr.control.Option;

/**
 * DTO for common e-mail settings
 */
public class MailCredentials {

	/**
	 * Common SMTP ports
	 */
	public enum KnownPorts {
		/**
		 * The default non-secure SMTP port
		 */
		NONSECURE(25, false),
		/**
		 * A common non-secure port if using port 25 is not an option
		 */
		NONSECURE_OVER_1000(2525, false),
		/**
		 * Default port for SMTP with StartTLS
		 */
		STARTTLS(587, true),
		/**
		 * Non-default port for SMTP with StartTLS to avoid using a port below 1000
		 */
		STARTTLS_OVER_1000(2587, true);

		private final int port;

		private final boolean secure;

		/**
		 * Constructor
		 * @param port The port number
		 * @param secure Whether or not it is a port used by a secure protocol
		 */
		KnownPorts(int port, boolean secure) {
			this.port = port;
			this.secure = secure;
		}

		/**
		 * Indicates whether or not the port in question is normally used for secured (TLS) traffic
		 * @return {@code true} if the port is generally used securely, {@code false} otherwise
		 */
		public boolean isSecure() {
			return this.secure;
		}

		/**
		 * Returns the integer port number
		 * @return The port number
		 */
		public int getPort() {
			return port;
		}

		/**
		 * Indicates whether or not the port is, to the best of our knowledge, a valid port for SMTP
		 * communication
		 * @param port The port in question
		 * @return {@code true} if the port is valid for SMTP. {@code false} otherwise
		 */
		public static boolean isValidPort(int port) {
			return stream()
					.anyMatch(kp -> kp.port == port);
		}

		/**
		 * Returns a Stream of all known ports
		 * @return A stream
		 */
		public static Stream<KnownPorts> stream() {
			return Arrays.stream(KnownPorts.values());
		}
	}

	private final String host;

	private final int port;

	private String username;

	private String password;

	/**
	 * Constructor
	 * @param host The hostname to connect to
	 * @param port The port to connect to
	 */
	public MailCredentials(@Nonnull String host, int port) {
		this(host, port, null, null);
	}

	/**
	 * Construtor
	 * @param host The hostname to connect to
	 * @param port The port to connect to
	 * @param username The username to authenticate with (optional, though I would hope any production mail server you connect to has mandatory authentication)
	 * @param password The password to authenticate with (optional, though I would hope any production mail server you connect to has mandatory authentication)
	 */
	public MailCredentials(@Nonnull String host, int port, @Nullable String username,
			@Nullable String password) {
		this.host = Asserts.objectParam("host").notNull(host);
		this.port = Asserts.numberParam("port").withValue(port)
				.satisfies(KnownPorts::isValidPort);
		this.username = username;
		this.password = password;
	}

	/**
	 * The hostname of the SMTP server
	 * @return The hostname
	 */
	public String getHost() {
		return host;
	}

	/**
	 * The port of the SMTP server
	 * @return The port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * The username to authenticate with
	 * @return Optionally the username
	 */
	public Option<String> getUsername() {
		return Option.of(username);
	}

	/**
	 * The password to authenticate with
	 * @return Optionally the password
	 */
	public Option<String> getPassword() {
		return Option.of(password);
	}

	/**
	 * Whether or not TLS should be enabled, based on the port number.
	 * @return {@code true} if we should use TLS, {@code false} otherwise.
	 */
	public boolean enableTLS() {
		return KnownPorts.stream().filter(kp -> kp.getPort() == port).anyMatch(KnownPorts::isSecure);
	}
	
	

}
