package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.junit.Test;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertNotNull;

public class RestEasyProxyProviderTest implements ResteasyProxyProvider {
	@Test
	public void testProxyCreation() throws Exception {
		AwesomeAPI proxy = proxyOf(AwesomeAPI.class);

		assertNotNull(proxy);

		AwesomerAPI proxy2 = proxyOf(AwesomerAPI.class);
		assertNotNull(proxy2);
	}

	@Override public String getBackendBaseUrl() {
		return "http://localhost:8080/";
	}

	@Override
	public int getConnectionPoolSize() {
		return 12;
	}


}

interface AwesomeAPI extends ListableResource<Resource,Resource_Query> {
	@Override
	@Path("/count")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	Response list(@BeanParam Resource_Query query,
				  @QueryParam("offset") Long offset,
				  @QueryParam("count") Long count);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Override
	Response count(
			@BeanParam Resource_Query query);
}

@Path("/v2")
interface AwesomerAPI extends ListableResource<Resource,Resource_Query> {
	@Override
	@Path("/count")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	Response list(@BeanParam Resource_Query query,
				  @QueryParam("offset") Long offset,
				  @QueryParam("count") Long count);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Override
	Response count(
			@BeanParam Resource_Query query);
}

class Resource {
	private String data;

	public String getData() {
		return data;
	}

	public Resource setData(String data) {
		this.data = data;
		return this;
	}
}

class Resource_Query implements QueryObject<Resource> {
	private static final long serialVersionUID = -9058000434220828664L;

	private int i;

	@Override
	public int getNextSortIndex() {
		return i++;
	}

	@Override
	public boolean matches(Resource object) {
		return true;
	}
}
