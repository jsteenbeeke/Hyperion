package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class ConditionalProxyProviderTest implements ConditionalProxyProvider {

	@Test
	public void testProxyCreation() throws Exception {
		AwesomeAPI proxy = proxyOf(AwesomeAPI.class);

		assertNotNull(proxy);

		AwesomerAPI proxy2 = proxyOf(AwesomerAPI.class);
		assertNotNull(proxy2);

		TestMode.init(Mockito::mock);

		proxy = proxyOf(AwesomeAPI.class);

		assertNotNull(proxy);

		proxy2 = proxyOf(AwesomerAPI.class);
		assertNotNull(proxy2);
	}

	@Override public String getBackendBaseUrl() {
		return "http://localhost:8080/";
	}

	@Override
	public int getConnectionPoolSize() {
		return 12;
	}


}
