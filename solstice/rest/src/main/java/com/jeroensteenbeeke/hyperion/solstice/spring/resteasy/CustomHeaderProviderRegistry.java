package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Central registry for JAX-RS header providers
 */
public enum CustomHeaderProviderRegistry {
	/**
	 * Singleton
	 */
	instance;
	
	private List<CustomHeaderProvider> providers;
	
	private CustomHeaderProviderRegistry() {
		providers = Lists.newLinkedList();
	}

	/**
	 * Registers a custom header provider
	 * @param provider The provider to use
	 */
	public void registerProvider(CustomHeaderProvider provider) {
		this.providers.add(provider);
	}

	/**
	 * Get an immutable list of the current providers
	 * @return The providers
	 */
	List<CustomHeaderProvider> getProviders() {
		return ImmutableList.copyOf(providers);
	}
}
