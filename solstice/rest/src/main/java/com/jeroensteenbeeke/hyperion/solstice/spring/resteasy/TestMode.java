package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import javax.annotation.Nonnull;

/**
 * Singleton class for indicating the application is running
 * in test mode. Useful for switching between regular operation and
 * mock-based operation
 */
public class TestMode {
	private static TestMode testMode;

	/**
	 * Checks whether or not test mode is enabled
	 * @return {@code true} if test mode is enabled, {@code false} otherwise
	 */
	static boolean isEnabled() {
		return testMode != null;
	}

	/**
	 * Get the current test mode instance
	 * @return The instance
	 */
	static TestMode getInstance() {
		return testMode;
	}

	private final IProxyProvider proxyProvider;

	/**
	 * Initialize test mode with the given provider
	 * @param provider The provider to use to create proxies
	 */
	public static void init(@Nonnull IProxyProvider provider) {
		testMode = new TestMode(provider);
	}

	private TestMode(@Nonnull IProxyProvider proxyProvider) {
		this.proxyProvider = proxyProvider;
	}

	/**
	 * Create a proxy instance of the given class
	 * @param targetClass The class to proxy
	 * @param <T> The type of the class to proxy
	 * @return An instance of the class
	 */
	<T> T proxy(Class<T> targetClass) {
		return proxyProvider.proxy(targetClass);
	}

	/**
	 * Interface to abstract the creation of proxies
	 */
	@FunctionalInterface
	public interface IProxyProvider {
		/**
		 * Create a proxy instance of the given class
		 * @param targetClass The class to proxy
		 * @param <T> The type of the class to proxy
		 * @return An instance of the class
		 */
		<T> T proxy(Class<T> targetClass);
	}
}
