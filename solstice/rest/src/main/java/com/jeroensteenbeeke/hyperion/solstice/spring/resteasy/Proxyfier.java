package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import org.jboss.resteasy.client.jaxrs.ProxyConfig;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.i18n.Messages;
import org.jboss.resteasy.client.jaxrs.internal.proxy.*;
import org.jboss.resteasy.util.IsHttpMethod;

import javax.ws.rs.Path;
import javax.ws.rs.client.WebTarget;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class Proxyfier {
	private static final Class<?>[] cClassArgArray = new Class[]{Class.class};

	private static final ProxyConfig DEFAULT_CONFIG = initProxyConfig();

	private static ProxyConfig initProxyConfig() {
		ClassLoader loader;
		if (System.getSecurityManager() == null) {
			loader = Thread.currentThread().getContextClassLoader();
		} else {
			loader = AccessController.doPrivileged((PrivilegedAction<ClassLoader>) () -> Thread
					.currentThread()
					.getContextClassLoader());
		}

		return new ProxyConfig(loader, null, null);
	}

	static <T> T proxy(Class<T> iface, WebTarget base) {
		return internalProxy(iface, base, DEFAULT_CONFIG);
	}

	private static <T> T internalProxy(Class<T> iface, WebTarget base, ProxyConfig config) {
		if (iface.isAnnotationPresent(Path.class)) {
			Path path = iface.getAnnotation(Path.class);
			if (!path.value().equals("") && !path.value().equals("/")) {
				base = base.path(path.value());
			}
		}

		HashMap<Method, MethodInvoker> methodMap = new HashMap<>();
		Method[] var4 = iface.getMethods();
		int var5 = var4.length;

		Method getName = null;

		for (int var6 = 0; var6 < var5; ++var6) {
			Method method = var4[var6];

			if (method.isBridge()) {
				continue;
			}

			if (!"getName".equals(method.getName()) || method.getParameterCount() > 0) {
				if (!"as".equals(method.getName()) || !Arrays
						.equals(method.getParameterTypes(), cClassArgArray)) {


					Set<String> httpMethods = IsHttpMethod.getHttpMethods(method);
					MethodInvoker invoker;
					if ((httpMethods == null || httpMethods.size() == 0) && method.isAnnotationPresent(Path.class) && method
							.getReturnType()
							.isInterface()) {
						invoker = new SubResourceInvoker((ResteasyWebTarget) base, method, config);
					} else {
						invoker = createClientInvoker(iface, method, (ResteasyWebTarget) base, config);
					}

					methodMap.put(method, invoker);
				}
			} else {
				getName = method;
			}
		}

		for (int var6 = 0; var6 < var5; ++var6) {
			Method method = var4[var6];

			if (method.isBridge()) {
				Class<?>[] bridgeParams = method.getParameterTypes();

				methodMap.entrySet().stream()
						 .filter(e -> e.getKey().getName().equals(method.getName()))
						 .filter(e -> e.getKey().getParameterCount() == method.getParameterCount())
						 .filter(e -> e.getKey().getReturnType().equals(method.getReturnType()))
						 .filter(e -> !e.getKey().isBridge())
						 .filter(e -> {
							 Class<?>[] concreteParams = e.getKey().getParameterTypes();
							 for (int i = 0; i < method.getParameterCount(); i++) {
								 if (!bridgeParams[i].isAssignableFrom(concreteParams[i])) {
									 return false;
								 }
							 }

							 return true;
						 }).map(Map.Entry::getValue)
						 .findAny().ifPresent(i -> methodMap.put(method, i));
			}
		}

		Class<?>[] intfs = new Class[]{iface, ResteasyClientProxy.class};
		ClientProxy clientProxy = new ClientProxy(methodMap, base, config);
		clientProxy.setClazz(iface);
		@SuppressWarnings("unchecked")
		T t = (T) Proxy.newProxyInstance(config.getLoader(), intfs, clientProxy);


		if (getName != null && isListableResource(iface)) {
			methodMap.put(getName,
						  args -> iface.getSimpleName());

		}

		return t;

	}

	private static boolean isListableResource(Class<?> iface) {
		if ("com.jeroensteenbeeke.hyperion.rest.query.ListableResource".equals(iface.getName())) {
			return true;
		}

		Class<?> superclass = iface.getSuperclass();

		if (superclass != null && isListableResource(superclass)) {
			return true;
		}

		Class<?>[] interfaces = iface.getInterfaces();
		if (interfaces != null) {
			for (Class<?> anInterface : interfaces) {
				if (isListableResource(anInterface)) {
					return true;
				}
			}

		}

		return false;
	}

	private static <T> ClientInvoker
	createClientInvoker(Class<T> clazz, Method method, ResteasyWebTarget base, ProxyConfig config) {
		Set<String> httpMethods = IsHttpMethod.getHttpMethods(method);
		if (httpMethods != null && httpMethods.size() == 1) {
			ClientInvoker invoker = new ClientInvoker(base, clazz, method, config);
			invoker.setHttpMethod(httpMethods.iterator().next());
			return invoker;
		} else {
			throw new RuntimeException(Messages.MESSAGES.mustUseExactlyOneHttpMethod(method.toString()));
		}
	}
}
