package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Client-side header provider for JAX-RS calls
 */
public interface CustomHeaderProvider {
	/**
	 * Add headers to the given map of headers
	 * @param headers The current headers. Add custom headers to this parameter
	 */
	void addHeaders(MultivaluedMap<String, Object> headers);
}
