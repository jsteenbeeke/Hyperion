package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

/**
 * JAX-RS client filter that applies custom headers
 */
public class CustomHeaderFilter implements ClientRequestFilter {

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		CustomHeaderProviderRegistry.instance.getProviders().forEach(
				provider -> provider.addHeaders(requestContext.getHeaders()));
	}

}
