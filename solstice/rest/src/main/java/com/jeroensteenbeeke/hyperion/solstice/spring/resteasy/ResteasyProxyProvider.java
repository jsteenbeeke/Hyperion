package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

/**
 * Convenience interface to quickly create RestEasy client proxies, with sensible defaults, and
 * allowing for extensions. This interface should generally be implemented by your Spring
 * configuration
 */
public interface ResteasyProxyProvider {
	/**
	 * Returns the base URL of the service to communicate with
	 * @return The URL of the service, to which all resource paths are relative
	 */
	String getBackendBaseUrl();

	/**
	 * Returns the size of the connection pool for the RestEasy clients
	 * @return The size of the connection pool
	 */
	int getConnectionPoolSize();

	/**
	 * Decorator method to allow the implementing client to further configure the
	 * ResteasyClientBuilder
	 * @param builder The builder to decorate
	 */
	default void decorateClientBuilder(ResteasyClientBuilder builder) {

	}

	/**
	 * Decorator method to allow the implementing client to further configure the
	 * ResteasyClient
	 * @param client The client to decorate
	 */
	default void decorateClient(ResteasyClient client) {

	}

	/**
	 * Decorator method to allow the implementing client to further configure the
	 * ResteasyWebTarget
	 * @param target The target to decorate
	 */
	default void decorateTarget(ResteasyWebTarget target) {

	}

	/**
	 * Creates a proxy of the given class, relative to the service URL
	 * @param serviceClass The class to proxy
	 * @param <T> The type of the class
	 * @return An instance of T that is proxied to perform rest calls
	 */
	default <T> T proxyOf(Class<T> serviceClass) {
		ResteasyClientBuilder builder = new ResteasyClientBuilder();
		builder.connectionPoolSize(getConnectionPoolSize());
		decorateClientBuilder(builder);

		ResteasyClient client = builder.build();
		client.register(CustomHeaderFilter.class);
		decorateClient(client);
		ResteasyWebTarget target = client.target(getBackendBaseUrl());
		decorateTarget(target);
		return Proxyfier.proxy(serviceClass, target);
	}
}
