package com.jeroensteenbeeke.hyperion.solstice.dao.common;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.ComparableFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IStringFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.StringFilterField;
import com.jeroensteenbeeke.hyperion.rest.querysupport.ILongProperty;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.hibernate.metamodel.model.domain.internal.SingularAttributeImpl;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.verification.VerificationMode;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.io.Serializable;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class BaseHibernateDAOTest {
	@Test
	public void testCRUD() {
		TestDAO dao = new TestDAO();

		ETest object = new ETest();
		dao.save(object);

		verify(dao.entityManager).persist(object);

		dao.update(object);

		verify(dao.entityManager).merge(object);

		Option<ETest> rv = dao.load(5L);
		assertTrue(rv.isEmpty());
		verify(dao.entityManager).find(ETest.class, 5L);

		dao.delete(object);
		verify(dao.entityManager).remove(object);

		dao.evict(object);
		verify(dao.entityManager).detach(object);

		dao.flush();
		verify(dao.entityManager).flush();
	}

	@Test
	public void testFindFunctions() {
		TestDAO dao = new TestDAO();

		dao.findAll();
		verify(dao.entityManager).getCriteriaBuilder();

		dao.countAll();
		verify(dao.entityManager, times(2)).getCriteriaBuilder();

		dao.findByFilter(new ETestFilter());
		verify(dao.entityManager, times(3)).getCriteriaBuilder();

		dao.findByFilter(new ETestFilter(),0 , 20);
		verify(dao.entityManager, times(4)).getCriteriaBuilder();

		dao.countByFilter(new ETestFilter());
		verify(dao.entityManager, times(5)).getCriteriaBuilder();

		Option<ETest> unique = dao.getUniqueByFilter(new ETestFilter());
		assertTrue(unique.isEmpty());
		verify(dao.entityManager, times(6)).getCriteriaBuilder();
	}

	@Test
	public void testAggregations() {
		TestDAO dao = new TestDAO();

		dao.max(ETest_.username);
		verify(dao.entityManager).getCriteriaBuilder();
		dao.max(ETest_.username, new ETestFilter());
		verify(dao.entityManager, times(2)).getCriteriaBuilder();
		dao.min(ETest_.username);
		verify(dao.entityManager, times(3)).getCriteriaBuilder();
		dao.min(ETest_.username, new ETestFilter());
		verify(dao.entityManager, times(4)).getCriteriaBuilder();

		dao.sum(ETest_.logins);
		verify(dao.entityManager, times(5)).getCriteriaBuilder();
		dao.sum(ETest_.logins, new ETestFilter());
		verify(dao.entityManager, times(6)).getCriteriaBuilder();
	}

	@Test
	public void testAttributeFunctions() {
		TestDAO dao = new TestDAO();

		dao.property(ETest_.username, new ETestFilter());
		verify(dao.entityManager).getCriteriaBuilder();

		dao.properties(ETest_.username, new ETestFilter());
		verify(dao.entityManager, times(2)).getCriteriaBuilder();
	}
}

class ETest extends BaseDomainObject {
	@Column
	private String name;

	@Override
	public Serializable getDomainObjectId() {
		return 5L;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

@StaticMetamodel(ETest.class)
class ETest_ {
	public static volatile SingularAttribute<ETest, String> username = initUsernameAttribute();

	public static volatile SingularAttribute<ETest, Long> logins = initLoginsAttribute();

	@SuppressWarnings("unchecked")
	private static SingularAttribute<ETest,String> initUsernameAttribute() {
		ManagedType<ETest> type = mock(ManagedType.class);
		when(type.getJavaType()).thenReturn(ETest.class);

		SingularAttribute<ETest,String> attr = mock(SingularAttribute.class);
		when(attr.getBindableJavaType()).thenReturn(String.class);
		when(attr.getDeclaringType()).thenReturn(type);

		return attr;
	}

	@SuppressWarnings("unchecked")
	private static SingularAttribute<ETest,Long> initLoginsAttribute() {
		ManagedType<ETest> type = mock(ManagedType.class);
		when(type.getJavaType()).thenReturn(ETest.class);

		SingularAttribute<ETest,Long> attr = mock(SingularAttribute.class);
		when(attr.getBindableJavaType()).thenReturn(Long.class);
		when(attr.getDeclaringType()).thenReturn(type);

		return attr;
	}

}

class ETestFilter extends SearchFilter<ETest,ETestFilter> {
	private IStringFilterField<ETest,ETestFilter> stringFilterField = new StringFilterField<ETest,String,ETestFilter>(ETest_.username, this);

	private IFilterField<ETest,Long,ETestFilter> longFilterField = new ComparableFilterField<>(ETest_.logins, this);
}

class TestDAO extends BaseHibernateDAO<ETest,ETestFilter> {
	@SuppressWarnings("unchecked")
	TestDAO() {
		Root<ETest> rootEntry = mock(Root.class);
		CriteriaQuery<ETest> all = mock(CriteriaQuery.class);
		Expression<Long> countExpression = mock(Expression.class);

		CriteriaQuery<ETest> cq = mock(CriteriaQuery.class);
		when(cq.from(eq(ETest.class))).thenReturn(rootEntry);
		when(cq.select(eq(rootEntry))).thenReturn(all);

		CriteriaQuery<Long> long_cq = mock(CriteriaQuery.class);
		when(long_cq.from(ETest.class)).thenReturn(rootEntry);

		CriteriaQuery<String> string_cq = mock(CriteriaQuery.class);
		when(string_cq.from(ETest.class)).thenReturn(rootEntry);

		TypedQuery<ETest> query = mock(TypedQuery.class);
		TypedQuery<Long> longTypedQuery = mock(TypedQuery.class);
		when(longTypedQuery.getSingleResult()).thenReturn(0L);

		CriteriaBuilder cb = mock(CriteriaBuilder.class);

		when(cb.createQuery(eq(ETest.class))).thenReturn(cq);
		when(cb.createQuery(eq(Long.class))).thenReturn(long_cq);
		when(cb.createQuery(eq(String.class))).thenReturn(string_cq);
		when(cb.count(any())).thenReturn(countExpression);

		this.entityManager = mock(EntityManager.class);
		when(entityManager.merge(any(ETest.class))).thenAnswer((Answer<ETest>) invocation -> {
			Object[] args = invocation.getArguments();
			return (ETest) args[0];
		});
		when(entityManager.find(any(), any())).thenReturn(null);
		when(entityManager.getCriteriaBuilder()).thenReturn(cb);
		when(entityManager.createQuery(any(CriteriaQuery.class))).then((Answer<TypedQuery<?>>) invocation ->
			invocation.getArgument(0) == cq ? query : longTypedQuery);
	}
}
