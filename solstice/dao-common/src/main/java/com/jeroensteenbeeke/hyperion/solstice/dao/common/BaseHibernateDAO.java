/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.dao.common;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseDAO;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.IComparableFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.JPA;
import io.vavr.collection.*;
import io.vavr.control.Option;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static io.vavr.control.Option.*;

/**
 * A Data Access Object that uses the Hibernate ORM package
 *
 * @param <T> The domain object loadable by this DAO
 * @param <F> The search filter class to use with this DAO
 * @author Jeroen Steenbeeke
 */
@SuppressWarnings("unchecked")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public abstract class BaseHibernateDAO<T extends DomainObject, F extends BaseSearchFilter<T, F>> implements BaseDAO<T, F> {
	private final Class<T> domainClass = (Class<T>) ((ParameterizedType) getClass()
		.getGenericSuperclass()).getActualTypeArguments()[0];

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	protected EntityManager entityManager;

	@Override
	public final long countAll() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		query.select(builder.count(query.from(domainClass)));

		TypedQuery<Long> longQuery = entityManager.createQuery(query);
		return longQuery.getSingleResult();
	}

	@Override
	public final void evict(T object) {
		entityManager.detach(object);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	@Override
	public void delete(T object) {
		T target = object;

		if (!entityManager.contains(object)) {
			target = entityManager.merge(object);
		}

		entityManager.remove(target);
	}

	@Override
	public final Seq<T> findAll() {
		/*
		 * Yes, the Javadoc for streamAll() says this is a bad idea.
		 * Calling this method itself is a bad idea too,
		 * so sue me for doing it this way
		 */
		return streamAll().collect(Array.collector());
	}

	@Override
	public Stream<T> streamAll() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(domainClass);
		Root<T> rootEntry = cq.from(domainClass);
		CriteriaQuery<T> all = cq.select(rootEntry);
		TypedQuery<T> allQuery = entityManager.createQuery(all);

		return allQuery.getResultStream();
	}

	@Override
	public final long countByFilter(F filter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);

		Root<T> root = criteriaQuery.from(domainClass);

		Predicate[] predicateArray = filterToPredicates(filter,
														createJPA(builder, criteriaQuery, root));

		criteriaQuery.select(builder.count(root));
		criteriaQuery.where(predicateArray);
		return entityManager.createQuery(criteriaQuery).getSingleResult();
	}

	private <A extends Serializable, R extends Serializable> JPA<A, R> createJPA(CriteriaBuilder builder, AbstractQuery<A> criteriaQuery, Root<R> root) {
		return new JPA<A, R>(builder, root, criteriaQuery, f -> {
			Subquery query = criteriaQuery.subquery(f.getEntityClass());
			Root<A> subqueryRoot = query.from(f.getEntityClass());
			query.select(subqueryRoot);
			query.where(filterToPredicates(f, createJPA(builder, query, subqueryRoot)));

			return query;
		});
	}

	@Override
	public final Option<T> getUniqueByFilter(F filter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = builder.createQuery(domainClass);

		Root<T> root = criteriaQuery.from(domainClass);

		Predicate[] predicateArray = filterToPredicates(filter,
														createJPA(builder, criteriaQuery, root));

		criteriaQuery.select(root);
		criteriaQuery.where(predicateArray);
		try {
			return of(entityManager.createQuery(criteriaQuery).getSingleResult());
		} catch (NoResultException nre) {
			return none();
		}
	}

	@Override
	public final Seq<T> findByFilter(F filter) {
		return streamByFilter(filter).collect(Array.collector());
	}

	@Override
	public Stream<T> streamByFilter(F filter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = builder.createQuery(domainClass);

		Root<T> root = criteriaQuery.from(domainClass);

		criteriaQuery.select(root);
		criteriaQuery.where(filterToPredicates(filter,
											   createJPA(builder, criteriaQuery, root)));
		criteriaQuery.orderBy(filterToOrders(filter, createJPA(builder, criteriaQuery, root)).asJava());
		return entityManager.createQuery(criteriaQuery).getResultStream();
	}

	@Override
	public final Seq<T> findByFilter(F filter, long offset,
									 long count) {
		return streamByFilter(filter, offset, count).collect(Array.collector());
	}

	@Override
	public Stream<T> streamByFilter(F filter, long offset, long count) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = builder.createQuery(domainClass);

		Root<T> root = criteriaQuery.from(domainClass);

		criteriaQuery.select(root);
		criteriaQuery.where(filterToPredicates(filter, createJPA(builder, criteriaQuery, root)));
		criteriaQuery.orderBy(filterToOrders(filter, createJPA(builder, criteriaQuery, root)).asJava());

		TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
		query.setMaxResults((int) count);
		query.setFirstResult((int) offset);

		return query.getResultStream();
	}

	/**
	 * Load the object with the indicated ID
	 *
	 * @param id The ID of the requested object
	 * @return The loaded object, or <code>null</code> if it does not exist
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	@Override
	public Option<T> load(Serializable id) {
		return of(entityManager.find(domainClass, id));
	}

	/**
	 * Save the indicated object
	 *
	 * @param object The object to save
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	@Override
	public void save(T object) {
		entityManager.persist(object);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	@Override
	public void update(T object) {
		entityManager.merge(object);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void flush() {
		entityManager.flush();
	}

	@Override
	public <FT extends Comparable<? super FT> & Serializable> Option<FT> max(IComparableFilterField<T, FT, ? extends F> field) {
		return max(field.getAttribute(), field.getFilter());
	}

	@Override
	public <FT extends Comparable<? super FT> & Serializable> Option<FT> min(IComparableFilterField<T, FT, ? extends F> field) {
		return min(field.getAttribute(), field.getFilter());
	}

	@Override
	public <FT extends Number & Comparable<? super FT> & Serializable> Option<FT> sum(IComparableFilterField<T, FT, ? extends F> field) {
		return sum(field.getAttribute(), field.getFilter());
	}

	@Override
	public <FT extends Serializable> Option<FT> property(IFilterField<T, FT, ? extends F> field) {
		return property(field.getAttribute(), field.getFilter());
	}

	@Override
	public <FT extends Serializable> Seq<FT> properties(IFilterField<T, FT, ? extends F> field) {
		return properties(field.getAttribute(), field.getFilter());
	}

	/**
	 * Calculates the sum of the specified attribute
	 *
	 * @param attribute           The attribute
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The containing entity
	 * @param <A>                 The attribute type
	 * @return Optionally the sum
	 */
	protected final <ENTITY_ATTRIBUTE extends Number, CONTAINING_ENTITY extends DomainObject, A extends SingularAttribute<? super
		CONTAINING_ENTITY, ENTITY_ATTRIBUTE>>
	Option<ENTITY_ATTRIBUTE> sum(A attribute) {
		return numericAggregate(CriteriaBuilder::sum, attribute, null);
	}

	/**
	 * Calculates the sum of the specified attribute, filtered by the given filter
	 *
	 * @param attribute           The attribute
	 * @param filter              The search filter
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The containing entity
	 * @param <A>                 The attribute type
	 * @return Optionally the sum
	 */
	protected final <ENTITY_ATTRIBUTE extends Number, CONTAINING_ENTITY extends DomainObject, A extends SingularAttribute<? super
		CONTAINING_ENTITY, ENTITY_ATTRIBUTE>>
	Option<ENTITY_ATTRIBUTE> sum(A attribute, F filter) {
		return numericAggregate(CriteriaBuilder::sum, attribute, filter);
	}

	/**
	 * Fetches the given property of this DAO's entity, according to the given filter, expecting a unique result
	 *
	 * @param attribute           The attribute to fetch
	 * @param filter              The filter to apply
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return Optionally the sum
	 */
	protected final <ENTITY_ATTRIBUTE extends Serializable, CONTAINING_ENTITY extends DomainObject, A extends SingularAttribute<?
		super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>>
	Option<ENTITY_ATTRIBUTE> property(
		A attribute, F filter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ENTITY_ATTRIBUTE> criteriaQuery = builder
			.createQuery(attribute.getBindableJavaType());

		Root<CONTAINING_ENTITY> root = (Root<CONTAINING_ENTITY>) criteriaQuery
			.from(attribute.getDeclaringType().getJavaType());

		criteriaQuery.select(
			root.get(attribute));

		if (filter != null) {
			criteriaQuery.where(filterToPredicates(filter,
												   createJPA(builder, criteriaQuery, root)));
		}
		try {
			return of(
				entityManager.createQuery(criteriaQuery).getSingleResult());
		} catch (NoResultException e) {
			return none();
		}
	}

	/**
	 * Fetches the given property of this DAO's entity, according to the given filter
	 *
	 * @param attribute           The attribute to fetch
	 * @param filter              The filter to apply
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return A vavr seq of the given property
	 */
	protected final <ENTITY_ATTRIBUTE extends Serializable, CONTAINING_ENTITY extends DomainObject, A extends SingularAttribute<?
		super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>>
	Seq<ENTITY_ATTRIBUTE> properties(
		A attribute, F filter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ENTITY_ATTRIBUTE> criteriaQuery = builder
			.createQuery(attribute.getBindableJavaType());

		Root<CONTAINING_ENTITY> root = (Root<CONTAINING_ENTITY>) criteriaQuery
			.from(attribute.getDeclaringType().getJavaType());

		criteriaQuery.select(
			root.get(attribute));

		if (filter != null) {
			criteriaQuery.where(filterToPredicates(filter,
												   createJPA(builder, criteriaQuery, root)));
		}
		try {
			return
				entityManager.createQuery(criteriaQuery).getResultStream().collect(Array.collector());
		} catch (NoResultException e) {
			return List.empty();
		}
	}

	/**
	 * Returns the maximum value of the given attribute
	 *
	 * @param attribute           The attribute
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return Optionally the max value
	 */
	protected final <ENTITY_ATTRIBUTE extends Comparable<ENTITY_ATTRIBUTE> & Serializable, CONTAINING_ENTITY extends DomainObject, A extends
		SingularAttribute<?
			super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>> Option<ENTITY_ATTRIBUTE> max(
		A attribute) {
		return aggregate(CriteriaBuilder::greatest, attribute, null);
	}

	/**
	 * Fetches the maximum value of the given attribute, according to the given filter
	 *
	 * @param attribute           The attribute to fetch
	 * @param filter              The filter to apply
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return Optionally the maximum value
	 */
	protected final <ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable, CONTAINING_ENTITY extends DomainObject, A extends
		SingularAttribute<?
			super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>> Option<ENTITY_ATTRIBUTE> max(
		A attribute, F filter) {
		return aggregate(CriteriaBuilder::greatest, attribute, filter);
	}

	/**
	 * Fetches the minimum value of the given attribute, according to the given filter
	 *
	 * @param attribute           The attribute to fetch
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return Optionally the minimum value
	 */
	protected final <ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable, CONTAINING_ENTITY extends DomainObject, A extends
		SingularAttribute<?
			super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>> Option<ENTITY_ATTRIBUTE> min(
		A attribute) {
		return aggregate(CriteriaBuilder::least, attribute, null);
	}

	/**
	 * Fetches the minimum value of the given attribute, according to the given filter
	 *
	 * @param attribute           The attribute to fetch
	 * @param filter              The filter to apply
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return Optionally the minimum value
	 */
	protected final <ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable, CONTAINING_ENTITY extends DomainObject, A extends
		SingularAttribute<?
			super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>> Option<ENTITY_ATTRIBUTE> min(
		A attribute, F filter) {
		return aggregate(CriteriaBuilder::least, attribute, filter);
	}

	/**
	 * Applies a given aggregate function on the given attribute, based on the results of the given filter
	 *
	 * @param aggregationFunction The CriteriaBuilder method to apply
	 * @param attribute           The attribute to aggregate
	 * @param filter              The filter to search by
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return Optionally the aggregated result
	 */
	protected final <ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable, CONTAINING_ENTITY extends DomainObject, A extends
		SingularAttribute<?
			super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>> Option<ENTITY_ATTRIBUTE> aggregate(
		BiFunction<CriteriaBuilder, Expression<ENTITY_ATTRIBUTE>, Expression<ENTITY_ATTRIBUTE>> aggregationFunction,
		A attribute, F filter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

		Class<ENTITY_ATTRIBUTE> bindableJavaType = attribute.getBindableJavaType();
		CriteriaQuery<ENTITY_ATTRIBUTE> criteriaQuery = builder.createQuery(bindableJavaType);

		Class<? super CONTAINING_ENTITY> javaType = attribute.getDeclaringType().getJavaType();
		Root<CONTAINING_ENTITY> root = (Root<CONTAINING_ENTITY>) criteriaQuery.from(javaType);

		criteriaQuery.select(aggregationFunction.apply(builder, root.get(attribute)));

		if (filter != null) {
			criteriaQuery.where(filterToPredicates(filter,
												   createJPA(builder, criteriaQuery, root)));
		}
		try {
			return of(entityManager.createQuery(criteriaQuery).getSingleResult());
		} catch (NoResultException e) {
			return none();
		}
	}

	/**
	 * Applies a given numeric aggregate function on the given attribute, based on the results of the given filter
	 *
	 * @param aggregationFunction The CriteriaBuilder method to apply
	 * @param attribute           The attribute to aggregate
	 * @param filter              The filter to search by
	 * @param <ENTITY_ATTRIBUTE>  The attribute type
	 * @param <CONTAINING_ENTITY> The entity type
	 * @param <A>                 The JPA attribute type
	 * @return Optionally the aggregated result
	 */
	protected final <ENTITY_ATTRIBUTE extends Number, CONTAINING_ENTITY extends DomainObject, A extends
		SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>>
	Option<ENTITY_ATTRIBUTE> numericAggregate(
		BiFunction<CriteriaBuilder, Expression<ENTITY_ATTRIBUTE>, Expression<ENTITY_ATTRIBUTE>>
			aggregationFunction,
		A attribute, F filter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ENTITY_ATTRIBUTE> criteriaQuery = builder
			.createQuery(attribute.getBindableJavaType());

		Root<CONTAINING_ENTITY> root = (Root<CONTAINING_ENTITY>) criteriaQuery
			.from(attribute.getDeclaringType().getJavaType());

		criteriaQuery.select(aggregationFunction.apply(builder, root.get(attribute)));

		if (filter != null) {
			criteriaQuery.where(filterToPredicates(filter,
												   createJPA(builder, criteriaQuery, root)));
		}
		try {
			return of(
				entityManager.createQuery(criteriaQuery).getSingleResult());
		} catch (NoResultException e) {
			return none();
		}
	}

	private <ENTITY_ATTRIBUTE extends Serializable, CONTAINING_ENTITY extends DomainObject> Predicate[]
	filterToPredicates(BaseSearchFilter<?, ?> filter, JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa) {
		Seq<IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ?>> usedFilterFields = getUsedFilterFields(filter,
																										 IFilterField::isSet);
		return usedFilterFields.map(f -> f.toPredicate(jpa)).filter(Option::isDefined)
							   .map(Option::get)
							   .toJavaArray(Predicate.class);
	}

	private <ENTITY_ATTRIBUTE extends Serializable, CONTAINING_ENTITY extends DomainObject> Seq<Order> filterToOrders(
		F filter, JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa) {
		Seq<IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ?>> stream = getUsedFilterFields(filter,
																							   t -> true);
		return stream.filter(f -> f.orderByIndex().isDefined())
					 .sorted(Comparator.comparing(f -> f.orderByIndex().get()))
					 .map(o -> o.toOrderBy(jpa))
					 .filter(Option::isDefined)
					 .flatMap(Option::get)
					 .filter(Objects::nonNull);

	}

	private <ENTITY_ATTRIBUTE extends Serializable, CONTAINING_ENTITY extends DomainObject> Seq<IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ?>> getUsedFilterFields(
		BaseSearchFilter<?, ?> filter,
		java.util.function.Predicate<? super IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ?>> finalStep) {
		Array<Field> fields = Array.of(filter.getClass().getDeclaredFields());
		Array<Field> filterFields = fields.filter(f -> IFilterField.class.isAssignableFrom(f.getType()));
		Array<IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ?>> castStream = filterFields.map(f -> {
			try {
				boolean acessible = f.isAccessible();
				if (!acessible) {
					f.setAccessible(true);
				}
				IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ?> filterField =
					(IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ?>) f
						.get(filter);
				if (!acessible) {
					f.setAccessible(false);
				}
				return filterField;
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}

		});

		return castStream.filter(finalStep);
	}

}
