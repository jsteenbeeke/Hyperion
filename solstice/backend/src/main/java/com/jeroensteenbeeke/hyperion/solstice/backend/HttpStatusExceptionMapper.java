package com.jeroensteenbeeke.hyperion.solstice.backend;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * ExceptionMapper for HttpStatusException
 */
@Provider
public class HttpStatusExceptionMapper implements ExceptionMapper<HttpStatusException> {

	@Override
	public Response toResponse(HttpStatusException exception) {
		return exception.toResponse();
	}
}
