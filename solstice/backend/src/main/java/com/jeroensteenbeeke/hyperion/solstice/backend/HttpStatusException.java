package com.jeroensteenbeeke.hyperion.solstice.backend;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Exception representing an HTTP status error, that should automatically be mapped to a JAX-RS Response object
 */
public class HttpStatusException extends RuntimeException {
	private static final long serialVersionUID = 5687270656950774671L;

	private final String mimeType;

	private final Response.Status status;

	private final String message;

	private HttpStatusException(String mimeType, Response.Status status, String message) {
		this.mimeType = mimeType;
		this.status = status;
		this.message = message;
	}

	/**
	 * Transforms this exception into a JAX-RS response
	 *
	 * @return The response representing this exception
	 */
	public Response toResponse() {
		return Response.status(status).entity(message).type(mimeType).build();
	}

	/**
	 * Create a new status exception (builder)
	 *
	 * @param status The status to return as part of this exception
	 * @return A builder
	 */
	public static WithMessage forStatus(@Nonnull Response.Status status) {
		return message -> mimeType -> new HttpStatusException(mimeType, status, message);
	}

	/**
	 * Builder element
	 */
	@FunctionalInterface
	public interface WithMessage {
		/**
		 * Specify the exception message
		 *
		 * @param message The message
		 * @return A builder
		 */
		WithMimeType withMessage(@Nonnull String message);
	}

	/**
	 * Builder element
	 */
	@FunctionalInterface
	public interface WithMimeType {
		/**
		 * Create the exception with the given mimeType
		 *
		 * @param mimeType the MIME type to use
		 * @return The created exception
		 */
		HttpStatusException ofType(@Nonnull String mimeType);

		/**
		 * Create the exception as plain text
		 *
		 * @return The exception
		 */
		default HttpStatusException asPlainText() {
			return ofType(MediaType.TEXT_PLAIN);
		}

		/**
		 * Create the exception as JSON
		 *
		 * @return The exception
		 */
		default HttpStatusException asJson() {
			return ofType(MediaType.APPLICATION_JSON);
		}
	}
}
