package com.jeroensteenbeeke.hyperion.solstice.backend;

import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertNotNull;

public class HttpStatusExceptionTest {
	@Test
	public void createExceptionAndApplyMapper() {
		HttpStatusExceptionConfig config = new HttpStatusExceptionConfig();
		HttpStatusExceptionMapper mapper = config.mapper();

		HttpStatusException plainText = HttpStatusException
			.forStatus(Response.Status.OK)
			.withMessage("OK")
			.asPlainText();
		HttpStatusException json = HttpStatusException
			.forStatus(Response.Status.NOT_FOUND)
			.withMessage("{ \"message\": \"Not found!\"}")
			.asJson();
		HttpStatusException yolo = HttpStatusException
			.forStatus(Response.Status.BAD_GATEWAY)
			.withMessage("BAD GATEWAY!")
			.ofType("text/yolo");

		assertNotNull(plainText);
		assertNotNull(json);
		assertNotNull(yolo);

		assertNotNull(mapper.toResponse(plainText));
		assertNotNull(mapper.toResponse(json));
		assertNotNull(mapper.toResponse(yolo));

	}
}
