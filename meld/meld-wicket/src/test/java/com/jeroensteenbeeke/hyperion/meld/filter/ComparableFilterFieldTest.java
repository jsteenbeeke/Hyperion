package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.ParameterRegistry;
import org.hibernate.query.criteria.internal.compile.RenderingContext;
import org.hibernate.query.criteria.internal.predicate.AbstractSimplePredicate;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.io.Serializable;
import java.util.Collection;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyVararg;
import static org.mockito.Mockito.*;

public class ComparableFilterFieldTest {
	@Test
	public void testBetween() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.between("a", "b");

		assertTrue(filter.name.isSet());
		assertEquals("a", filter.name.first.getObject());
		assertEquals("b", filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.BETWEEN, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testEqualTo() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.equalTo("c");

		assertTrue(filter.name.isSet());
		assertEquals("c", filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.EQ, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testGreaterThan() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.greaterThan("d");

		assertTrue(filter.name.isSet());
		assertEquals("d", filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.GT, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testGreaterThanOrEqualTo() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.greaterThanOrEqualTo("e");

		assertTrue(filter.name.isSet());
		assertEquals("e", filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.GE, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testNotNull() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.isNotNull();

		assertTrue(filter.name.isSet());
		assertNull(filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.NOT_NULL, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testNull() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.isNull();

		assertTrue(filter.name.isSet());
		assertNull(filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.NULL, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testLessThan() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.lessThan("f");

		assertTrue(filter.name.isSet());
		assertEquals("f", filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.LT, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testLessThanOrEqualTo() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.lessThanOrEqualTo("g");

		assertTrue(filter.name.isSet());
		assertEquals("g", filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.LE, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testNotBetween() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.notBetween("h", "i");

		assertTrue(filter.name.isSet());
		assertEquals("h", filter.name.first.getObject());
		assertEquals("i", filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.NOT_BETWEEN, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testNotEqualTo() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.notEqualTo("j");

		assertTrue(filter.name.isSet());
		assertEquals("j", filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(ComparableFilterField.ComparisonType.NE, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@Test
	public void testIn() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.in(Array.of("k", "l", "m"));

		assertTrue(filter.name.isSet());
		assertNull(filter.name.first.getObject());
		assertNull(filter.name.second.getObject());
		assertEquals(Array.of("k", "l", "m"), filter.name.allowedValues.getObject());
		assertEquals(ComparableFilterField.ComparisonType.IN, filter.name.type);

		checkOrderByAndPredicate(filter);
	}

	@SuppressWarnings("unchecked")
	private void checkOrderByAndPredicate(FooBarFilter filter) {
		Predicate pred = mock(Predicate.class);

		Path<FooBar> path = mock(Path.class);
		when(path.in((Collection<?>) any())).thenReturn(pred);

		CriteriaBuilder builder = mock(CriteriaBuilder.class);
		when(builder.equal(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.notEqual(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.between(any(Expression.class), any(String.class), any(String.class))).thenReturn(pred);
		when(builder.not(any(Expression.class))).thenReturn(pred);
		when(builder.greaterThan(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.greaterThanOrEqualTo(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.lessThan(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.lessThanOrEqualTo(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.isNull(any(Expression.class))).thenReturn(pred);
		when(builder.isNotNull(any(Expression.class))).thenReturn(pred);

		Root<FooBar> root = mock(Root.class);
		when(root.get(any(SingularAttribute.class))).thenReturn(path);

		JPA<String, FooBar> jpa = new JPA<String, FooBar>(
				builder,
				root,
				mock(CriteriaQuery.class), f -> mock(Subquery.class)
		);
		assertEquals(Option.none(), filter.name.toOrderBy(jpa).map(Seq::size));
		assertNotNull(filter.name.toPredicate(jpa));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyIn() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.in(Array.of());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullIn() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.in(null);
	}

	@Test(expected = IllegalStateException.class)
	public void testUnset() {
		FooBarFilter filter = new FooBarFilter();
		filter.name.toPredicate(null);
	}

	@Test
	public void testInvalidDirectAccess() {
		assertEquals(Option.none(), ComparableFilterField.ComparisonType.IN.toPredicate(null, null, null, null));
	}

	@Test(expected = IllegalStateException.class)
	public void testIllegalOr() {
		FooBarFilter filter = new FooBarFilter();
		filter.orName();
	}

	@Test(expected = IllegalStateException.class)
	public void testInCompleteOr() {
		FooBarFilter filter = new FooBarFilter();
		filter.name("John").orName();

		filter.name().toPredicate(mockJPA());
	}

	@Test
	public void testConjunction() {
		FooBarFilter filter = new FooBarFilter();
		filter.name("John").orName("Steve");

		assertEquals(MockCriteriaBuilder.OR, filter.name().toPredicate(mockJPA()).getOrNull());

		filter = new FooBarFilter();
		filter.name("John").orName("Steve").orName("Bob");

		assertEquals(MockCriteriaBuilder.OR, filter.name().toPredicate(mockJPA()).getOrNull());
	}

	@Test
	public void testDisjunction() {
		FooBarFilter filter = new FooBarFilter();
		filter.name("John").andName("Steve");

		assertEquals(MockCriteriaBuilder.AND, filter.name().toPredicate(mockJPA()).getOrNull());

		filter = new FooBarFilter();
		filter.name("John").andName("Steve").andName("Bob");

		assertEquals(MockCriteriaBuilder.AND, filter.name().toPredicate(mockJPA()).getOrNull());
	}

	@SuppressWarnings("unchecked")
	private <E extends Serializable, F extends DomainObject> JPA<F, E> mockJPA() {
		Root<E> mockRoot = mock(Root.class);
		CriteriaBuilder mockBuilder = new MockCriteriaBuilder();

		JPA<F, E> jpa = (JPA<F, E>) mock(JPA.class);

		when(jpa.getRoot()).thenReturn(mockRoot);
		when(jpa.getBuilder()).thenReturn(mockBuilder);

		return jpa;
	}

}

class FooBarFilter extends SearchFilter<FooBar, FooBarFilter> {
	ComparableFilterField<FooBar, String, FooBarFilter> name = new ComparableFilterField<>(FooBar_.name, this);

	public ComparableFilterField<FooBar, String, FooBarFilter> name() {
		return setLastUserFilterField(this.name);
	}

	public FooBarFilter name(String value) {
		return setLastUserFilterField(this.name).equalTo(value);
	}

	public ComparableFilterField<FooBar, String, FooBarFilter> orName() {
		return disjunction(this.name.createNew());
	}

	public FooBarFilter orName(String value) {
		return disjunction(this.name.createNew()).equalTo(value);
	}

	public ComparableFilterField<FooBar, String, FooBarFilter> andName() {
		return conjunction(this.name.createNew());
	}

	public FooBarFilter andName(String value) {
		return conjunction(this.name.createNew()).equalTo(value);
	}
}

@Entity
class FooBar extends BaseDomainObject {
	@Id
	Long id;

	@Column
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Serializable getDomainObjectId() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}

@StaticMetamodel(FooBar.class)
class FooBar_ {
	public static volatile SingularAttribute<FooBar, String> name = mock(SingularAttribute.class);

}
