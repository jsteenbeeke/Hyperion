/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.meld.web;

import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;

/**
 * Utility class for encapsulating entities
 */
public final class EntityEncapsulator {

	private static IEntityEncapsulatorFactory factory = new NullEntityEncapsulatorFactory();

	private EntityEncapsulator() {
	}

	/**
	 * Sets the factory for encapsulating entities
	 * @param factory The factory to use to create entities
	 */
	public static void setFactory(@Nonnull IEntityEncapsulatorFactory factory) {
		if (factory == null)
			throw new IllegalArgumentException("Factory may not be null");
		EntityEncapsulator.factory = factory;
	}

	/**
	 * Creates a dataprovider for the given filter and DAO
	 * @param filter The SearchFilter to use as input
	 * @param dao The DAO to execute the filter on
	 * @param <T> The entity type
	 * @param <F> The filter type
	 * @return A dataprovider
	 */
	@Nonnull
	public static <T extends DomainObject,F extends SearchFilter<T,F>> IDataProvider<T> createProvider(
			@Nonnull F filter, @Nonnull DAO<T,F> dao) {
		if (filter == null)
			throw new IllegalArgumentException("filter may not be null");

		if (dao == null)
			throw new IllegalArgumentException("dao may not be null");

		return factory.createDataProvider(filter, dao);
	}

	/**
	 * Creates a model for a single entity
	 * @param object The entity
	 * @param <T> The entity type
	 * @return An IModel containing the entity
	 */
	@Nonnull
	public static <T extends DomainObject> IModel<T> createModel(
			@Nonnull T object) {
		if (object == null)
			throw new IllegalArgumentException("object may not be null");

		return factory.createModel(object);
	}

	/**
	 * Creates an empty model for entities of the given type
	 * @param modelClass The type of model
	 * @param <T> The type of model
	 * @return An empty IModel
	 */
	@Nonnull
	public static <T extends DomainObject> IModel<T> createNullModel(
			Class<T> modelClass) {
		if (modelClass == null)
			throw new IllegalArgumentException("modelClass may not be null");

		return factory.createModel(modelClass);
	}

	/**
	 * Creates a model for a list of entities
	 * @param list The list of entities
	 * @param <T> The entity type
	 * @return An IModel containing the list
	 */
	@Nonnull
	public static <T extends DomainObject> IModel<List<T>> createListModel(
			@Nonnull List<T> list) {
		if (list == null)
			throw new IllegalArgumentException("list may not be null");

		return factory.createListModel(list);
	}
}
