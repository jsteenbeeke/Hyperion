package com.jeroensteenbeeke.hyperion.meld.filter;


import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import org.danekja.java.util.function.serializable.SerializableFunction;

import javax.annotation.Nonnull;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;

/**
 * Field representing a String column in an entity
 * @param <R> The type of entity
 * @param <DISCARD> Unused. Our code generators expect a middle parameter, but in inheritance this is overriden with String
 * @param <F> The type of containing filter
 */
public class StringFilterField<R extends DomainObject, DISCARD, F extends SearchFilter<R, F>>
		extends SimpleFilterField<R, String, F> implements IStringFilterField<R,F> {
	private boolean caseSensitive = true;

	private boolean like = false;

	private boolean negated = false;

	/**
	 * Constructor
	 * @param attribute The attribute representing the entity value
	 * @param filter The containing filter
	 */
	public StringFilterField(@Nonnull SingularAttribute<? super R, String> attribute,
							 @Nonnull F filter) {
		super(attribute, filter);
	}

	@Override
	public F equalsIgnoreCase(String target) {
		assertNotSet();

		this.caseSensitive = false;
		return equalTo(target);
	}

	@Override
	public F like(String expression) {
		assertNotSet();

		this.like = true;
		return equalTo(expression);
	}

	@Override
	public F ilike(String expression) {
		assertNotSet();

		this.like = true;
		return equalsIgnoreCase(expression);
	}

	@Nonnull
	@Override
	public F notEqualsIgnoreCase(@Nonnull String target) {
		assertNotSet();

		this.caseSensitive = false;
		this.negated = true;

		return equalTo(target);	}

	@Nonnull
	@Override
	public F unlike(@Nonnull String expression) {
		assertNotSet();

		this.negated = true;

		return like(expression);
	}

	@Nonnull
	@Override
	public F iUnlike(@Nonnull String expression) {
		assertNotSet();

		this.negated = true;
		this.caseSensitive = false;

		return like(expression);
	}

	@Override
	protected <FT extends Serializable> Expression<String> transform(JPA<FT, R> jpa, CriteriaBuilder builder,
																	 Expression<String> base) {
		if (!caseSensitive) {
			return builder.upper(base);
		} else {
			return base;
		}
	}

	@Override
	protected String transform(String base) {
		if (!caseSensitive) {
			return base.toUpperCase();
		} else {
			return base;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	protected <FT extends Serializable> Predicate createPredicate(CriteriaBuilder builder, Expression<?> left, String right, SerializableFunction<BaseSearchFilter<?, ?>, Subquery<FT>> subqueryCreator) {
		if (like) {
			if (negated) {
				return builder.not(builder.like((Expression<String>) left, right));
			}
			return builder.like((Expression<String>) left, right);
		} else {
			if (negated) {
				return builder.not(super.createPredicate(builder, left, right, subqueryCreator));
			}
			return super.createPredicate(builder, left, right, subqueryCreator);
		}
	}

	@Override
	public StringFilterField<R, DISCARD, F> createNew() {
		return new StringFilterField<>(getAttribute(), getFilter());
	}
}
