/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.meld.web;

import java.util.Collections;
import java.util.Iterator;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.jeroensteenbeeke.hyperion.data.DomainObject;

class NullDataProvider<T extends DomainObject> implements IDataProvider<T> {

	private static final long serialVersionUID = 1L;

	@Override
	public void detach() {
	}

	@Override
	public Iterator<? extends T> iterator(long first, long count) {
		return Collections.emptyIterator();
	}

	@Override
	public long size() {
		return 0;
	}

	@Override
	public IModel<T> model(T object) {
		return Model.of();
	}

}
