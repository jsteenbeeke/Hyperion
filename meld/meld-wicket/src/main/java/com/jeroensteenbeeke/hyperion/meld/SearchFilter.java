/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.meld;

import java.lang.reflect.Field;

import org.apache.wicket.model.IDetachable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeroensteenbeeke.hyperion.data.DomainObject;

/**
 * SearchFilter base class, primarily meant to set generic bounds
 * @param <T> The type of entity
 * @param <F> The current type
 */
public abstract class SearchFilter<T extends DomainObject, F extends SearchFilter<T,F>> extends BaseSearchFilter<T, F>
		implements IDetachable {

	private static final Logger log = LoggerFactory
			.getLogger(SearchFilter.class);

	private static final long serialVersionUID = 1L;

	private int lastOrderByIndex = 0;

	@Override
	public int getAndIncrementLastOrderByIndex() {
		return lastOrderByIndex++;
	}

	@Override
	public void detach() {
		for (Field field : getClass().getDeclaredFields()) {
			if (field.getType().isAssignableFrom(IDetachable.class)) {
				try {
					((IDetachable) field.get(this)).detach();
				} catch (IllegalArgumentException | IllegalAccessException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

}
