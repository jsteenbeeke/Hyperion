/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.meld.web;

import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.util.ListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;

/**
 * Dummy implementation, default if none is set
 */
public class NullEntityEncapsulatorFactory implements
		IEntityEncapsulatorFactory {
	private static final Logger log = LoggerFactory
			.getLogger(NullEntityEncapsulatorFactory.class);

	@Nonnull
	@Override
	public <T extends DomainObject, F extends SearchFilter<T, F>> IDataProvider<T> createDataProvider(@Nonnull F filter, @Nonnull DAO<T, F> dao) {
		log.error("No entity encapsulator set!");
		return new NullDataProvider<>();
	}

	@Nonnull
	@Override
	public <T extends DomainObject> IModel<T> createModel(@Nonnull T object) {
		log.error("No entity encapsulator set!");
		return new Model<>(object);
	}
	
	@Override
	public <T extends DomainObject> IModel<T> createModel(Class<T> modelClass) {
		log.error("No entity encapsulator set!");
		return new Model<>(null);
	}

	@Nonnull
	@Override
	public <T extends DomainObject> IModel<List<T>> createListModel(@Nonnull List<T> list) {
		log.error("No entity encapsulator set!");
		return new ListModel<>(new LinkedList<>());
	}

}
