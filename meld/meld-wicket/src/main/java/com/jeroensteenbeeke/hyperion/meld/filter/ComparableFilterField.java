package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import javax.annotation.Nonnull;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;

/**
 * Filter field representing a comparable value (though not a String)
 * @param <CONTAINING_ENTITY> The entity queried
 * @param <ENTITY_ATTRIBUTE> The type of the field in the entity
 * @param <F> The filter type containing this field
 */
public class ComparableFilterField<
		CONTAINING_ENTITY extends DomainObject,
		ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable,
		F extends SearchFilter<CONTAINING_ENTITY,F>>
		extends
		com.jeroensteenbeeke.hyperion.meld.filter.FilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, SingularAttribute<CONTAINING_ENTITY, ENTITY_ATTRIBUTE>, F> implements
		IDetachable, IComparableFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> {

	private static final long serialVersionUID = 1L;

	IModel<Seq<ENTITY_ATTRIBUTE>> allowedValues = new SeqModel<>(List.empty());

	IModel<ENTITY_ATTRIBUTE> first = Model.of();

	IModel<ENTITY_ATTRIBUTE> second = Model.of();

	ComparisonType type = null;

	private IFilterField<CONTAINING_ENTITY, ?, F> junction;

	private JunctionType junctionType;

	/**
	 * Constructor
	 * @param attribute The attribute represented by this filterfield
	 * @param filter The filter containing this filterField
	 */
	public ComparableFilterField(@Nonnull SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute, @Nonnull F filter) {
		super(attribute, filter);
	}

	@Nonnull
	@Override
	public F in(@Nonnull Seq<ENTITY_ATTRIBUTE> values) {
		assertNotSet();

		if (values == null || values.isEmpty()) {
			throw new IllegalArgumentException("in requires a non-null non-empty list");
		}

		allowedValues = new SeqModel<>(values);
		first = Model.of();
		second = Model.of();
		type = ComparisonType.IN;
		return getFilter();
	}

	@Nonnull
	@Override
	public F equalTo(ENTITY_ATTRIBUTE value) {
		assertNotSet();

		type = ComparisonType.EQ;
		first = Model.of(value);
		second = Model.of();
		return getFilter();
	}

	@Nonnull
	@Override
	public F notEqualTo(ENTITY_ATTRIBUTE value) {
		assertNotSet();

		type = ComparisonType.NE;
		first = Model.of(value);
		second = Model.of();
		return getFilter();
	}

	@Nonnull
	@Override
	public F greaterThan(ENTITY_ATTRIBUTE lowerBound) {
		assertNotSet();

		type = ComparisonType.GT;
		first = Model.of(lowerBound);
		second = Model.of();
		return getFilter();
	}

	@Nonnull
	@Override
	public F greaterThanOrEqualTo(ENTITY_ATTRIBUTE lowerBound) {
		assertNotSet();

		type = ComparisonType.GE;
		first = Model.of(lowerBound);
		second = Model.of();
		return getFilter();
	}

	@Nonnull
	@Override
	public F lessThan(ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.LT;
		first = Model.of(upperBound);
		second = Model.of();
		return getFilter();
	}

	@Nonnull
	@Override
	public F lessThanOrEqualTo(ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.LE;
		first = Model.of(upperBound);
		second = Model.of();
		return getFilter();
	}

	@Nonnull
	@Override
	public F between(ENTITY_ATTRIBUTE lowerBound, ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.BETWEEN;
		first = Model.of(lowerBound);
		second = Model.of(upperBound);
		return getFilter();
	}

	@Nonnull
	@Override
	public F notBetween(ENTITY_ATTRIBUTE lowerBound, ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.NOT_BETWEEN;
		first = Model.of(lowerBound);
		second = Model.of(upperBound);
		return getFilter();
	}


	@Nonnull
	@Override
	public F isNull() {
		assertNotSet();

		type = ComparisonType.NULL;
		first = Model.of();
		second = Model.of();
		return getFilter();
	}

	@Nonnull
	@Override
	public F isNotNull() {
		assertNotSet();

		type = ComparisonType.NOT_NULL;
		first = Model.of();
		second = Model.of();
		return getFilter();
	}

	@Override
	public boolean isSet() {
		return type != null;
	}

	@Override
	public void detach() {
		first.detach();
		second.detach();
		allowedValues.detach();
	}

	@Override
	@Nonnull
	public final Option<Seq<Order>> toOrderBy(JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa) {
		return getOrderBy().map(orderBy -> {
			CriteriaBuilder builder = jpa.getBuilder();
			Root<CONTAINING_ENTITY> root = jpa.getRoot();

			if (orderBy.isAscending()) {
				return List.of(builder.asc(root.get((SingularAttribute<? super CONTAINING_ENTITY, ?>) getAttribute())));
			} else {
				return List.of(builder.desc(root.get((SingularAttribute<? super CONTAINING_ENTITY, ?>) getAttribute())));
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	@Nonnull
	public <FT extends Serializable> Option<Predicate> toPredicate(JPA<FT, CONTAINING_ENTITY> jpa) {
		if (type == null) {
			throw new IllegalStateException(
					"This filter does not have a type set. This means it should not be included in a query, but yet " +
							"it is");
		}

		IFilterField<CONTAINING_ENTITY, FT, F> castJunction = (IFilterField<CONTAINING_ENTITY, FT, F>) junction;

		return processJunction(junctionType, castJunction, jpa, this::internalCreatePredicate);
	}

	private <FT extends Serializable> Option<Predicate> internalCreatePredicate(JPA<FT, CONTAINING_ENTITY> jpa) {
		CriteriaBuilder builder = jpa.getBuilder();
		Root<CONTAINING_ENTITY> root = jpa.getRoot();

		Expression<ENTITY_ATTRIBUTE> path = root.get(getAttribute());
		if (!allowedValues.getObject().isEmpty()) {
			Seq<ENTITY_ATTRIBUTE> values = allowedValues.getObject();

			return Option.of(path.in(values.asJava()));
		}

		return type.toPredicate(builder, path, first,
				second);
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> and(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			junction.and(target);
			return this;
		}

		this.junctionType = JunctionType.AND;
		this.junction = target;

		return this;
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> or(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			junction.or(target);
			return this;
		}

		this.junctionType = JunctionType.OR;
		this.junction = target;


		return this;
	}

	@Override
	public ComparableFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> createNew() {
		return new ComparableFilterField<>(getAttribute(), getFilter());
	}

	enum ComparisonType {
		NULL {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder,
					Expression<? extends T>
							expression,
					IModel<T> first,
					IModel<T> second) {
				return Option.of(builder.isNull(expression));
			}
		}, NOT_NULL {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder,
					Expression<? extends T>
							expression,
					IModel<T> first,
					IModel<T> second) {
				return Option.of(builder.isNotNull(expression));
			}
		},
		GT {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.greaterThan(expression, first.getObject()));
			}
		},
		GE {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.greaterThanOrEqualTo(expression,
						first.getObject()));

			}
		},
		LT {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.lessThan(expression, first.getObject()));
			}
		},
		LE {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.lessThanOrEqualTo(expression, first.getObject()));
			}
		},
		EQ {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.equal(expression, first.getObject()));
			}
		},
		NE {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.notEqual(expression, first.getObject()));
			}
		},
		BETWEEN {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.between(expression, first.getObject(),
						second.getObject()));
			}
		},
		NOT_BETWEEN {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				return Option.of(builder.not(builder.between(expression, first.getObject(),
						second.getObject())));
			}
		}, IN {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					IModel<T> first, IModel<T> second) {
				// Should never be reached
				return Option.none();
			}
		};

		public abstract <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
				CriteriaBuilder builder, Expression<? extends T> expression,
				IModel<T> first, IModel<T> second);
	}

	private static class SeqModel<T extends Serializable> implements IModel<Seq<T>> {
		private static final long serialVersionUID = 2059453317418947273L;

		private final Seq<T> seq;

		private SeqModel(Seq<T> seq) {
			this.seq = seq;
		}

		@Override
		public Seq<T> getObject() {
			return seq;
		}
	}
}
