/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.meld.web;

import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;

/**
 * Factory for creating Wicket models and dataproviders for entities
 */
public interface IEntityEncapsulatorFactory {
	/**
	 * Creates a dataprovider for the given filter and DAO
	 * @param filter The SearchFilter to use as input
	 * @param dao The DAO to execute the filter on
	 * @param <T> The entity type
	 * @param <F> The filter type
	 * @return A dataprovider
	 */
	@Nonnull
	<T extends DomainObject, F extends SearchFilter<T,F>> IDataProvider<T> createDataProvider(
			@Nonnull F filter, @Nonnull DAO<T,F> dao);

	/**
	 * Creates a model for a single entity
	 * @param object The entity
	 * @param <T> The entity type
	 * @return An IModel containing the entity
	 */
	@Nonnull
	<T extends DomainObject> IModel<T> createModel(@Nonnull T object);

	/**
	 * Creates a model for a list of entities
	 * @param list The list of entities
	 * @param <T> The entity type
	 * @return An IModel containing the list
	 */
	@Nonnull
	<T extends DomainObject> IModel<List<T>> createListModel(
			@Nonnull List<T> list);

	/**
	 * Creates an empty model for entities of the given type
	 * @param modelClass The type of model
	 * @param <T> The type of model
	 * @return An empty IModel
	 */
	<T extends DomainObject> IModel<T> createModel(Class<T> modelClass);
}
