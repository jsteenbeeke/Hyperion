package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import org.danekja.java.util.function.serializable.SerializableFunction;

/**
 * Simple filter field: filter field for a property that is not comparable and not a String (it may be an entity, but that
 * case is handled by a subclass)
 *
 * @param <CONTAINING_ENTITY> The type of entity we're querying
 * @param <ENTITY_ATTRIBUTE> The type of attribute represented by this field
 * @param <F> The type of containing filter
 */
public class SimpleFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends Serializable, F
		extends SearchFilter<CONTAINING_ENTITY, F>>
		extends
		com.jeroensteenbeeke.hyperion.meld.filter.FilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, SingularAttribute<?
				super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>, F>
		implements
		IDetachable, IBasicFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> {

	private static final long serialVersionUID = 1L;

	private IModel<ENTITY_ATTRIBUTE> model = Model.of();

	private boolean checkNull = false;

	private boolean negated = false;

	private JunctionType junctionType;

	private IFilterField<CONTAINING_ENTITY, ?, F> junction;

	/**
	 * Constructor
	 * @param attribute The attribute for which this field filters
	 * @param filter The filter this field is declared in
	 */
	public SimpleFilterField(@Nonnull SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute,
							 @Nonnull F filter) {
		super(attribute, filter);
	}

	@Override
	public boolean isSet() {

		return model.getObject() != null || checkNull;
	}

	@Nonnull
	@Override
	public final F equalTo(@Nonnull ENTITY_ATTRIBUTE value) {
		assertNotSet();

		this.model = createModel(value);
		this.checkNull = false;
		return getFilter();
	}

	/**
	 * Sets the current field as having to be either equal to the given value or null
	 * @param value The value to check against
	 * @return The containing filter
	 */
	public final F equalToOrNull(ENTITY_ATTRIBUTE value) {
		assertNotSet();

		this.model = createModel(value);
		this.checkNull = true;
		return getFilter();
	}

	@Nonnull
	@Override
	public F notEqualTo(@Nonnull ENTITY_ATTRIBUTE value) {
		assertNotSet();

		this.model = createModel(value);
		this.checkNull = false;
		this.negated = true;
		return getFilter();
	}

	/**
	 * Equivalent to equalTo
	 * @param value The value to set
	 * @return The current filter
	 */
	public final F set(ENTITY_ATTRIBUTE value) {
		return equalTo(value);
	}

	@Nonnull
	@Override
	public final F isNull() {
		assertNotSet();

		this.checkNull = true;
		this.model = Model.of((ENTITY_ATTRIBUTE) null);
		return getFilter();
	}

	@Nonnull
	@Override
	public final F isNotNull() {
		assertNotSet();

		this.checkNull = false;
		this.negated = true;
		this.model = Model.of((ENTITY_ATTRIBUTE) null);
		return getFilter();
	}

	/**
	 * Creates a Wicket Model for the given value. Can be overridden when specialized models
	 * are needed, such as for entities
	 * @param value The value to create a model for
	 * @return A new Model
	 */
	protected IModel<ENTITY_ATTRIBUTE> createModel(ENTITY_ATTRIBUTE value) {
		return Model.of(value);
	}

	@Override
	public final void detach() {
		this.model.detach();
	}

	@Nonnull
	@Override
	@SuppressWarnings("unchecked")
	public final <FT extends Serializable> Option<Predicate> toPredicate(JPA<FT, CONTAINING_ENTITY> jpa) {
		IFilterField<CONTAINING_ENTITY, FT, F> castJunction = (IFilterField<CONTAINING_ENTITY, FT, F>) junction;

		return processJunction(junctionType, castJunction, jpa, this::internalCreatePredicate);
	}

	private <FT extends Serializable> Option<Predicate> internalCreatePredicate(JPA<FT, CONTAINING_ENTITY> jpa) {
		CriteriaBuilder builder = jpa.getBuilder();
		Root<CONTAINING_ENTITY> root = jpa.getRoot();

		Path<ENTITY_ATTRIBUTE> attributePath = root.get((SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>) getAttribute());

		ENTITY_ATTRIBUTE object = model.getObject();

		if (checkNull) {
			if (!negated) {
				// IS NULL
				if (object != null) {
					return Option.of(builder.or(builder.isNull(attributePath),
												builder.equal(attributePath, object)));
				}

				return Option.of(builder.isNull(attributePath));
			} else {
				// IS NOT NULL
				return Option.of(builder.isNotNull(attributePath));
			}
		}

		if (negated) {
			return Option.of(builder.not(createPredicate(builder,
														 transform(jpa, builder, attributePath),
														 transform(object), jpa.getSubqueryCreator())));
		}

		return Option.of(createPredicate(builder, transform(jpa, builder, attributePath), transform(object), jpa.getSubqueryCreator()));
	}

	/**
	 * Plugin method allowing the default operation (equal) to be overridden by a different operation (such as LIKE)
	 *
	 * @param builder The criteria builder
	 * @param left    The expression to check
	 * @param right   The expected value
	 * @param subqueryCreator Function for creating a subquery based on another search filter
	 * @param <FT> The type of field the subquery returns
	 * @return The resulting predicate
	 */
	protected <FT extends Serializable> Predicate createPredicate(CriteriaBuilder builder, Expression<?> left,
										ENTITY_ATTRIBUTE right, SerializableFunction<BaseSearchFilter<?,?>,
										Subquery<FT>> subqueryCreator) {
		return builder.equal(left, right);
	}

	/**
	 * Plugin method allowing subclasses to add custom behaviors. An example application
	 * is to allow case-insensitive comparisons
	 *
	 * @param jpa JPA facade
	 * @param builder The CriteriaBuilder used by the predicate creation
	 * @param base    The base expression
	 * @param <FT> The field type of the attribute we're creating an Expression for
	 * @return The resulting expression
	 */
	protected <FT extends Serializable> Expression<?> transform(JPA<FT, CONTAINING_ENTITY> jpa,
													 CriteriaBuilder builder,
													 Expression<ENTITY_ATTRIBUTE> base) {
		return base;
	}

	/**
	 * Plugin method allowing suclasses to add custom behaviors. An example application is to allow case-insensitive
	 * comparisons
	 *
	 * @param base The base value
	 * @return The resulting value
	 */
	protected ENTITY_ATTRIBUTE transform(ENTITY_ATTRIBUTE base) {
		return base;
	}

	@Nonnull
	@Override
	public final Option<Seq<Order>> toOrderBy(JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa) {
		return getOrderBy().map(orderBy -> {
			CriteriaBuilder builder = jpa.getBuilder();
			Root<CONTAINING_ENTITY> root = jpa.getRoot();

			if (orderBy.isAscending()) {
				return List.of(builder.asc(root.get((SingularAttribute<? super CONTAINING_ENTITY, ?>) getAttribute())));
			} else {
				return List.of(builder.desc(root.get((SingularAttribute<? super CONTAINING_ENTITY, ? >) getAttribute())));
			}
		});
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> and(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			junction.and(target);
			return this;
		}

		this.junctionType = JunctionType.AND;
		this.junction = target;

		return this;
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> or(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			junction.or(target);
			return this;
		}

		this.junctionType = JunctionType.OR;
		this.junction = target;

		return this;
	}

	@Override
	public SimpleFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> createNew() {
		return new SimpleFilterField<>(getAttribute(), getFilter());
	}

}
