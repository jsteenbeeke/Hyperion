/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.meld;

import com.jeroensteenbeeke.hyperion.data.DomainObject;

/**
 * A data access object for accessing data
 * 
 * @param <T> The type of domain object
 * @param <F> The type of filter used with this DAO
 * @author Jeroen Steenbeeke
 */
public interface DAO<T extends DomainObject,F extends SearchFilter<T,F>> extends BaseDAO<T, F> {

}
