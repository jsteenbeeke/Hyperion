package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import org.danekja.java.util.function.serializable.SerializableFunction;

import javax.annotation.Nonnull;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;
import java.util.function.Function;

/**
 * Filter field representing an entity value, usually columns that reference other tables
 * @param <CONTAINING_ENTITY> The entity containing the field
 * @param <ENTITY_ATTRIBUTE> The entity matching the field's attribute
 * @param <F> The current searchfilter
 * @param <EF> The type of searchfilter one would use to find entities of type ENTITY_ATTRIBUTE
 */
public class EntityFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends DomainObject, F
		extends SearchFilter<CONTAINING_ENTITY,F>, EF extends BaseSearchFilter<ENTITY_ATTRIBUTE,EF>>
		extends SimpleFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F>
		implements IEntityFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F, EF> {
	private static final long serialVersionUID = 1L;

	private Long explicitId;

	private SingularAttribute<? super ENTITY_ATTRIBUTE, Long> attributeToId;

	private EF subFilter;

	/**
	 * Constructor
	 * @param attribute The attribute to create a field for
	 * @param filter The filter this field is defined in
	 * @param attributeToId A function for extracting IDs from the attribute modelled by this entity
	 */
	public EntityFilterField(SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute,
							 F filter,
							 SingularAttribute<? super ENTITY_ATTRIBUTE, Long> attributeToId
	) {
		super(attribute, filter);
		this.attributeToId = attributeToId;
	}

	@Nonnull
	@Override
	public F id(@Nonnull Long id) {
		assertNotSet();

		this.explicitId = id;
		return getFilter();
	}

	@Override
	public boolean isSet() {
		return subFilter != null || explicitId != null || super.isSet();
	}

	@Nonnull
	@Override
	public F byFilter(@Nonnull EF filter) {
		assertNotSet();

		this.subFilter = filter;

		return getFilter();
	}

	@Override
	@Nonnull
	protected <FT extends Serializable> Predicate createPredicate(CriteriaBuilder builder, Expression<?> left,
										ENTITY_ATTRIBUTE right, Function<BaseSearchFilter<?,?>,
										Subquery<FT>> subqueryCreator) {
		if (subFilter != null) {
			return left.in(subqueryCreator.apply(subFilter));
		}

		if (explicitId != null) {
			return builder.equal(left, explicitId);
		}

		return super.createPredicate(builder, left, right, subqueryCreator);
	}

	@Override
	@Nonnull
	protected <FT extends Serializable>  Expression<?> transform(JPA<FT, CONTAINING_ENTITY> jpa,
									  CriteriaBuilder builder,
									  Expression<ENTITY_ATTRIBUTE> base) {
		if (explicitId != null) {
			return jpa.getRoot().get((SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>) getAttribute()).get(attributeToId);
		}

		return super.transform(jpa, builder, base);
	}

	@Override
	@Nonnull
	public EntityFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F, EF> createNew() {
		return new EntityFilterField<>(getAttribute(), getFilter(), attributeToId);
	}
}
