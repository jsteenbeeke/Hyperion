package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.util.Asserts;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

import javax.annotation.Nonnull;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;

/**
 * Filter field representing a comparable value (though not a String)
 * @param <CONTAINING_ENTITY> The entity queried
 * @param <ENTITY_ATTRIBUTE> The type of the field in the entity
 * @param <F> The filter type containing this field
 */
public class ComparableFilterField<CONTAINING_ENTITY extends DomainObject,
		ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable,
		F extends SearchFilter<CONTAINING_ENTITY,F>>
		extends
		com.jeroensteenbeeke.hyperion.meld.filter.FilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, SingularAttribute<CONTAINING_ENTITY, ENTITY_ATTRIBUTE>, F>
		implements IComparableFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> {

	private static final long serialVersionUID = 1L;

	Seq<ENTITY_ATTRIBUTE> allowedValues;

	ENTITY_ATTRIBUTE first = null;

	ENTITY_ATTRIBUTE second = null;

	ComparisonType type = null;

	private IFilterField<CONTAINING_ENTITY, ?, F> junction;

	private JunctionType junctionType;

	/**
	 * Constructor
	 * @param attribute The metadata attribute representing the queried field
	 * @param filter The filter containing the field
	 */
	public ComparableFilterField(@Nonnull SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute, @Nonnull F filter) {
		super(attribute, filter);
	}

	@Nonnull
	@Override
	public F in(@Nonnull Seq<ENTITY_ATTRIBUTE> values) {
		assertNotSet();

		Asserts.objectParam("values").notNull(values);

		if (values.isEmpty()) {
			throw new IllegalArgumentException("in requires a non-null non-empty list");
		}

		allowedValues = values;
		first = null;
		second = null;
		type = ComparisonType.IN;
		return getFilter();
	}

	@Nonnull
	@Override
	public F isNull() {
		assertNotSet();

		type = ComparisonType.NULL;
		first = null;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F isNotNull() {
		assertNotSet();

		type = ComparisonType.NOT_NULL;
		first = null;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F equalTo(ENTITY_ATTRIBUTE value) {
		assertNotSet();

		type = ComparisonType.EQ;
		first = value;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F notEqualTo(ENTITY_ATTRIBUTE value) {
		assertNotSet();

		type = ComparisonType.NE;
		first = value;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F greaterThan(ENTITY_ATTRIBUTE lowerBound) {
		assertNotSet();

		type = ComparisonType.GT;
		first = lowerBound;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F greaterThanOrEqualTo(ENTITY_ATTRIBUTE lowerBound) {
		assertNotSet();

		type = ComparisonType.GE;
		first = lowerBound;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F lessThan(ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.LT;
		first = upperBound;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F lessThanOrEqualTo(ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.LE;
		first = upperBound;
		second = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public F between(ENTITY_ATTRIBUTE lowerBound, ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.BETWEEN;
		first = lowerBound;
		second = upperBound;
		return getFilter();
	}

	@Nonnull
	@Override
	public F notBetween(ENTITY_ATTRIBUTE lowerBound, ENTITY_ATTRIBUTE upperBound) {
		assertNotSet();

		type = ComparisonType.NOT_BETWEEN;
		first = lowerBound;
		second = upperBound;
		return getFilter();
	}

	@Override
	public boolean isSet() {
		return type != null;
	}

	@Override
	@Nonnull
	public final Option<Seq<Order>> toOrderBy(JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa) {
		return convertOrderByToOrderList(jpa, Root::get);
	}

	@Override
	@SuppressWarnings("unchecked")
	@Nonnull
	public <FT extends Serializable> Option<Predicate> toPredicate(JPA<FT, CONTAINING_ENTITY> jpa) {
		if (type == null) {
			throw new IllegalStateException(
					"This filter does not have a type set. This means it should not be included in a query, but yet " +
							"it is");
		}

		IFilterField<CONTAINING_ENTITY, FT, F> castJunction = (IFilterField<CONTAINING_ENTITY, FT, F>) junction;

		return processJunction(junctionType, castJunction, jpa, this::internalCreatePredicate);
	}

	private Option<Predicate> internalCreatePredicate(JPA<?, CONTAINING_ENTITY> jpa) {
		CriteriaBuilder builder = jpa.getBuilder();
		Root<CONTAINING_ENTITY> root = jpa.getRoot();

		if (allowedValues != null) {
			SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute = getAttribute();
			Path<ENTITY_ATTRIBUTE> attributePath = root.get(attribute);
			return Option.of(attributePath.in(allowedValues.asJava()));
		}

		return type.toPredicate(builder, root.get(getAttribute()), first,
				second);
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> and(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			 junction.and(target);
			 return this;
		}

		this.junctionType = JunctionType.AND;
		this.junction = target;

		return this;
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> or(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			junction.or(target);
			return this;
		}

		this.junctionType = JunctionType.OR;
		this.junction = target;

		return this;
	}

	@Override
	public ComparableFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> createNew() {
		return new ComparableFilterField<>(getAttribute(), getFilter());
	}

	enum ComparisonType {
		NULL {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(CriteriaBuilder builder,
																							 Expression<? extends T>
																									 expression,
																							 T first, T second) {
				return Option.of(builder.isNull(expression));
			}
		}, NOT_NULL {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(CriteriaBuilder builder,
																							 Expression<? extends T>
																									 expression,
																							 T first, T second) {
				return Option.of(builder.isNotNull(expression));
			}
		},
		GT {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.greaterThan(expression, first));
			}
		},
		GE {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.greaterThanOrEqualTo(expression, first));

			}
		},
		LT {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.lessThan(expression, first));
			}
		},
		LE {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.lessThanOrEqualTo(expression, first));
			}
		},
		EQ {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.equal(expression, first));
			}
		},
		NE {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.notEqual(expression, first));
			}
		},
		BETWEEN {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.between(expression, first, second));
			}
		}, NOT_BETWEEN {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.of(builder.not(builder.between(expression, first, second)));
			}
		}, IN {
			@Override
			public <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
					CriteriaBuilder builder, Expression<? extends T> expression,
					T first, T second) {
				return Option.none();
			}
		};

		public abstract <T extends Comparable<? super T> & Serializable, R> Option<Predicate> toPredicate(
				CriteriaBuilder builder, Expression<? extends T> expression,
				T first, T second);
	}
}
