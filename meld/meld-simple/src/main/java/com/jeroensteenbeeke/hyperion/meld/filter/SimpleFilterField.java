package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;

import java.io.Serializable;
import java.util.function.Function;

import javax.annotation.Nonnull;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

/**
 * Simple filter field: filter field for a property that is not comparable and not a String (it may be an entity, but that
 * case is handled by a subclass)
 *
 * @param <CONTAINING_ENTITY> The type of entity we're querying
 * @param <ENTITY_ATTRIBUTE> The type of attribute represented by this field
 * @param <FILTERTYPE> The type of containing filter
 */
public class SimpleFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends Serializable, FILTERTYPE
		extends SearchFilter<CONTAINING_ENTITY, FILTERTYPE>>
		extends
		com.jeroensteenbeeke.hyperion.meld.filter.FilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, SingularAttribute<? super CONTAINING_ENTITY,ENTITY_ATTRIBUTE>, FILTERTYPE>
		implements IBasicFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, FILTERTYPE> {

	private static final long serialVersionUID = 1L;

	private boolean negated = false;

	private ENTITY_ATTRIBUTE object = null;

	private boolean checkNull = false;

	private IFilterField<CONTAINING_ENTITY, ?, FILTERTYPE> junction;

	private JunctionType junctionType;

	/**
	 * Constructor
	 * @param attribute The attribute for which this field filters
	 * @param filter The filter this field is declared in
	 */
	public SimpleFilterField(@Nonnull SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute,
							 @Nonnull FILTERTYPE filter) {
		super(attribute, filter);
	}

	@Override
	public boolean isSet() {
		return object != null || checkNull;
	}

	/**
	 * Set the current field as negated, inverting its meaning
	 * @param negated {@code true} if we want to negate the current field, {@code false} otherwise
	 * @return This field
	 */
	protected SimpleFilterField setNegated(boolean negated) {
		this.negated = negated;
		return this;
	}

	@Nonnull
	@Override
	public final FILTERTYPE equalTo(@Nonnull ENTITY_ATTRIBUTE value) {
		assertNotSet();

		this.object = value;
		this.checkNull = false;
		this.negated = false;
		return getFilter();
	}

	/**
	 * Sets the current field as having to be either equal to the given value or null
	 * @param value The value to check against
	 * @return The containing filter
	 */
	@Nonnull
	public FILTERTYPE equalToOrNull(@Nonnull ENTITY_ATTRIBUTE value) {
		assertNotSet();

		this.object = value;
		this.checkNull = true;
		this.negated = false;
		return getFilter();
	}

	@Nonnull
	@Override
	public FILTERTYPE notEqualTo(@Nonnull ENTITY_ATTRIBUTE value) {
		assertNotSet();

		this.object = value;
		this.checkNull = false;
		this.negated = true;
		return getFilter();
	}

	/**
	 * Equivalent to equalTo
	 * @param value The value to set
	 * @return The current filter
	 */
	public FILTERTYPE set(ENTITY_ATTRIBUTE value) {
		return equalTo(value);
	}

	@Nonnull
	@Override
	public final FILTERTYPE isNull() {
		assertNotSet();

		this.checkNull = true;
		this.object = null;
		return getFilter();
	}

	@Nonnull
	@Override
	public final FILTERTYPE isNotNull() {
		assertNotSet();

		this.checkNull = false;
		this.object = null;
		return getFilter();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Nonnull
	public final <FT extends Serializable> Option<Predicate> toPredicate(JPA<FT, CONTAINING_ENTITY> jpa) {
		IFilterField<CONTAINING_ENTITY, FT, FILTERTYPE> castJunction = (IFilterField<CONTAINING_ENTITY, FT, FILTERTYPE>) junction;

		return processJunction(junctionType, castJunction, jpa, this::internalCreatePredicate);
	}

	@Nonnull
	private <FT extends Serializable> Option<Predicate> internalCreatePredicate(JPA<FT, CONTAINING_ENTITY> jpa) {
		CriteriaBuilder builder = jpa.getBuilder();
		Root<CONTAINING_ENTITY> root = jpa.getRoot();

		Path<ENTITY_ATTRIBUTE> attributePath = root.get((SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>) getAttribute());
		if (checkNull) {
			if (!negated) {
				// IS NULL
				if (object != null) {
					return Option.of(builder.or(builder.isNull(attributePath),
							builder.equal(attributePath, object)));
				}

				return Option.of(builder.isNull(attributePath));
			} else {
				// IS NOT NULL
				return Option.of(builder.isNotNull(attributePath));
			}
		}

		if (negated) {
			return Option.of(builder.not(createPredicate(builder,
					transform(jpa, builder, attributePath),
					transform(object), jpa.getSubqueryCreator())));
		}

		return Option.of(createPredicate(builder, transform(jpa, builder, attributePath), transform(object), jpa.getSubqueryCreator()));
	}

	/**
	 * Plugin method allowing the default operation (equal) to be overridden by a different operation (such as LIKE)
	 *
	 * @param builder The criteria builder
	 * @param left    The expression to check
	 * @param right   The expected value
	 * @param subqueryCreator Function to create a subquery based on a containing filter
	 * @param <FT> The type of field we're creating an expression for
	 * @return The resulting predicate
	 */
	protected <FT extends Serializable> Predicate createPredicate(CriteriaBuilder builder, Expression<?> left,
										ENTITY_ATTRIBUTE right , Function<BaseSearchFilter<?,?>,
			Subquery<FT>> subqueryCreator) {
		return builder.equal(left, right);
	}

	/**
	 * Plugin method allowing subclasses to add custom behaviors. An example application
	 * is to allow case-insensitive comparisons
	 *
	 * @param jpa JPA facade
	 * @param builder The CriteriaBuilder used by the predicate creation
	 * @param base    The base expression
	 * @param <FT> The type of field we're creating an expression for
	 * @return The resulting expression
	 */
	protected <FT extends Serializable> Expression<?> transform(JPA<FT, CONTAINING_ENTITY> jpa,
													 CriteriaBuilder builder,
													 Expression<ENTITY_ATTRIBUTE> base) {
		return base;
	}

	/**
	 * Plugin method allowing suclasses to add custom behaviors. An example application is to allow case-insensitive
	 * comparisons
	 *
	 * @param base The base value
	 * @return The resulting value
	 */
	protected ENTITY_ATTRIBUTE transform(ENTITY_ATTRIBUTE base) {
		return base;
	}

	@Nonnull
	@Override
	public final Option<Seq<Order>> toOrderBy(JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa) {
		return convertOrderByToOrderList(jpa, Root::get);
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, FILTERTYPE>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, FILTERTYPE> and(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			junction.and(target);
			return this;
		}

		this.junctionType = JunctionType.AND;
		this.junction = target;

		return this;
	}

	@Override
	public <N extends IFilterField<CONTAINING_ENTITY, ?, FILTERTYPE>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, FILTERTYPE> or(N target) {
		if (!isSet()) {
			throw new IllegalStateException("Cannot create an AND statement without the current field being set");
		}

		if (junction != null) {
			junction.or(target);
			return this;
		}

		this.junctionType = JunctionType.OR;
		this.junction = target;


		return this;
	}

	@Override
	public SimpleFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, FILTERTYPE> createNew() {
		return new SimpleFilterField<>(getAttribute(), getFilter());
	}
}
