package com.jeroensteenbeeke.hyperion.meld.rest;

import com.google.common.collect.ImmutableList;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.IBasicFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IComparableFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IEntityFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IStringFilterField;
import com.jeroensteenbeeke.hyperion.rest.querysupport.*;

import javax.annotation.Nonnull;

import com.jeroensteenbeeke.lux.TypedResult;
import org.danekja.java.util.function.serializable.SerializableBiConsumer;
import org.danekja.java.util.function.serializable.SerializableFunction;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Mapper class to automatically transform QueryObject instances to SearchFilter instances. Typical
 * use-cases are service methods that accept a QueryObject as BeanParam and subsequently make
 * calls to a backing DAO.
 * <p>
 * Recommend usage is to create a {@code static final} instance of RESTQueryBinding as part of the
 * service implementation. Once constructed a binding is immutable, so it is thread-safe
 *
 * @param <Q> The type of QueryObject
 * @param <F> The type of SearchFilter
 */
public class RESTQueryBinding<Q extends QueryObject, F extends BaseSearchFilter<?,F>> implements
		Serializable {

	private final Class<Q> sourceClass;

	private final Class<F> targetClass;

	private final List<FieldBinding<Q, ?, F>> bindings;

	private RESTQueryBinding(Class<Q> sourceClass, Class<F> targetClass,
							 List<FieldBinding<Q, ?, F>> bindings) {
		this.sourceClass = sourceClass;
		this.targetClass = targetClass;
		this.bindings = ImmutableList.copyOf(bindings);
	}

	/**
	 * Converts the given query to a searchfilter
	 *
	 * @param query The query to convert
	 * @return A searchfilter instance
	 */
	public F convert(Q query) {
		try {
			F target = targetClass.getConstructor().newInstance();

			bindings.forEach(f -> applyBinding(f, query, target));
			Map<IQueryProperty<?>, IFilterField<?, ?,F>> fieldMapping = new HashMap<>();

			bindings.forEach(b -> fieldMapping.put(b.getSourcePropertyFunction().apply(query),
					b.getTargetPropertyFunction().apply(target)));

			List<IQueryProperty<?>> sortableProperties = new LinkedList<>();
			fieldMapping.forEach((k, v) -> {
				if (k != null) {
					OrderBy propertyOrder = k.getPropertyOrder();
					if (propertyOrder.getIndex() != OrderBy.IGNORE.getIndex()) {
						sortableProperties.add(k);
					}
				}
			});

			sortableProperties.stream()
							  .filter(Objects::nonNull)
							  .sorted(Comparator.comparing(IQueryProperty::getPropertyOrder))
							  .forEach(p -> fieldMapping.get(p).orderBy(
									  p.getPropertyOrder().isAscending()));

			return target;
		} catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
			throw new IllegalStateException("Could not map query to searchfilter");
		}
	}

	private <N extends IQueryProperty> void applyBinding(FieldBinding<Q, N, F> binding, Q query,
														 F target) {
		N prop = binding.getSourcePropertyFunction().apply(query);

		if (prop != null && prop.isSet()) {
			binding.getTargetSetter().accept(prop, target);
		}
	}

	/**
	 * Create a binding from the given source class
	 *
	 * @param sourceClass The class to create a binding for
	 * @param <Q>         The type of class to bind
	 * @return A builder for creating the binding
	 */
	@Nonnull
	public static <Q extends QueryObject<?>> BindingBuilder<Q> bind(Class<Q> sourceClass) {
		return new BindingBuilder<>(sourceClass);
	}

	/**
	 * Builder object for binding a QueryObject to a SearchFilter
	 *
	 * @param <Q> The type of QueryObject to bind
	 */
	public static class BindingBuilder<Q extends QueryObject<?>> {

		private final Class<Q> sourceClass;

		private BindingBuilder(Class<Q> sourceClass) {
			this.sourceClass = sourceClass;
		}

		/**
		 * Specifies the SearchFilter class to bind the QueryObject to
		 *
		 * @param targetClass The class of the SearchFilter
		 * @param <F>         The type of SearchFilter
		 * @return A Builder object for specifying field bindings
		 */
		public <F extends BaseSearchFilter<?,F>> BindingFieldCollector<Q, F> to(
				Class<F> targetClass) {
			return new BindingFieldCollector<>(sourceClass, targetClass);
		}
	}

	/**
	 * Builder object that allows specific fields to be bound from the QueryObject to the
	 * SearchFilter
	 *
	 * @param <Q> The type of QueryObject
	 * @param <F> The type of SearchFilter
	 */
	public static class BindingFieldCollector<Q extends QueryObject<?>, F extends
			BaseSearchFilter<?,F>> {

		private final Class<Q> sourceClass;

		private final Class<F> targetClass;

		private final List<FieldBinding<Q, ?, F>> bindings;

		private BindingFieldCollector(Class<Q> sourceClass, Class<F> targetClass) {
			this.sourceClass = sourceClass;
			this.targetClass = targetClass;
			this.bindings = new LinkedList<>();
		}

		/**
		 * Binds a temporal field in a REST filter to a Date field in a SearchFilter
		 *
		 * @param field          The field in the REST filter
		 * @param property       The field in the SearchFilter
		 * @param temporalToDate The function to convert the temporal type to a regulare Date
		 * @param <QF>           The type of field in the REST filter
		 * @param <C>            The type of temporal field
		 * @return The current instance of BindingFieldCollector
		 */
		public <QF extends IComparableProperty<C, Q>, C extends Comparable<? super C> & Temporal &
				Serializable> BindingFieldCollector<Q, F>
		withTemporal(
				SerializableFunction<Q, QF> field,
				SerializableFunction<F, ? extends
						IComparableFilterField<?, Date, F>> property,
				SerializableFunction<C, Date> temporalToDate
		) {
			SerializableBiConsumer<QF, F> consumer = (queryProperty, target) -> {
				IComparableFilterField<?, Date, ?> prop = property.apply(target);

				Date primary = Optional.ofNullable(queryProperty.getPrimary()).map
						(temporalToDate).orElse(null);
				switch (queryProperty.getComparisonType()) {
					case NONE:
						return;
					case EQUALS:
						if (queryProperty.isNegated()) {
							prop.notEqualTo(primary);
						} else {
							prop.equalTo(primary);
						}
						return;
					case NULL:
						if (queryProperty.isNegated()) {
							prop.isNotNull();
						} else {
							prop.isNull();
						}
						return;
					case BETWEEN:
						Date secondary = Optional.ofNullable(queryProperty.getSecondary()).map
								(temporalToDate).orElse(null);
						if (queryProperty.isNegated()) {
							prop.notBetween(primary, secondary);
						} else {
							prop.between(primary, secondary);
						}
						return;
					case LESS_THAN:
						if (queryProperty.isNegated()) {
							prop.greaterThanOrEqualTo(primary);
						} else {
							prop.lessThan(primary);
						}
						return;
					case GREATER_THAN:
						if (queryProperty.isNegated()) {
							prop.lessThanOrEqualTo(primary);
						} else {
							prop.greaterThan(primary);
						}
						return;
					case GREATER_THAN_OR_EQUAL_TO:
						if (queryProperty.isNegated()) {
							prop.lessThan(primary);
						} else {
							prop.greaterThanOrEqualTo(primary);
						}
						return;
					case LESS_THAN_OR_EQUAL_TO:
						if (queryProperty.isNegated()) {
							prop.greaterThan(primary);
						} else {
							prop.lessThanOrEqualTo(primary);
						}
				}

			};
			bindings.add(new FieldBinding<>(field, consumer, property));
			return this;
		}

		/**
		 * Binds a comparable field in a REST filter to a comparable field in a SearchFilter
		 * @param field Function for locating the field in the REST filter
		 * @param property Function for locating the field in the SearchFilter
		 * @param <QF> The type of REST filter
		 * @param <C> The type of comparable
		 * @return The current instance of BindingFieldCollector
		 */
		public <QF extends IComparableProperty<C, Q>, C extends Comparable<? super C> &
				Serializable> BindingFieldCollector<Q, F>
		withComparable(
				SerializableFunction<Q, QF> field,
				SerializableFunction<F, ? extends
						IComparableFilterField<?, C, F>> property) {
			SerializableBiConsumer<QF, F> consumer = (queryProperty, target) -> {
				IComparableFilterField<?, C, F> prop = property.apply(target);

				C primary = queryProperty.getPrimary();
				switch (queryProperty.getComparisonType()) {
					case NONE:
						return;
					case EQUALS:
						if (queryProperty.isNegated()) {
							prop.notEqualTo(primary);
						} else {
							prop.equalTo(primary);
						}
						return;
					case NULL:
						if (queryProperty.isNegated()) {
							prop.isNotNull();
						} else {
							prop.isNull();
						}
						return;
					case BETWEEN:
						C secondary = queryProperty.getSecondary();
						if (queryProperty.isNegated()) {
							prop.notBetween(primary, secondary);
						} else {
							prop.between(primary, secondary);
						}
						return;
					case LESS_THAN:
						if (queryProperty.isNegated()) {
							prop.greaterThanOrEqualTo(primary);
						} else {
							prop.lessThan(primary);
						}
						return;
					case GREATER_THAN:
						if (queryProperty.isNegated()) {
							prop.lessThanOrEqualTo(primary);
						} else {
							prop.greaterThan(primary);
						}
						return;
					case GREATER_THAN_OR_EQUAL_TO:
						if (queryProperty.isNegated()) {
							prop.lessThan(primary);
						} else {
							prop.greaterThanOrEqualTo(primary);
						}
						return;
					case LESS_THAN_OR_EQUAL_TO:
						if (queryProperty.isNegated()) {
							prop.greaterThan(primary);
						} else {
							prop.lessThanOrEqualTo(primary);
						}
				}

			};
			bindings.add(new FieldBinding<>(field, consumer, property));
			return this;
		}

		/**
		 * Binds a Long field in the REST filter to an entity in the SearchFilter
		 * @param field The Long field
		 * @param property The entity field
		 * @return The current instance of BindingFieldCollector
		 */
		public BindingFieldCollector<Q, F> withEntity(
				SerializableFunction<Q, ILongProperty<Q>> field,
				SerializableFunction<F, ? extends IEntityFilterField<?, ?, F, ?>> property) {
			SerializableBiConsumer<ILongProperty<Q>, F> consumer =
					(ILongProperty<Q> queryProperty, F target) -> {
						switch (queryProperty.getComparisonType()) {

							case NULL:
								if (queryProperty.isNegated()) {
									property.apply(target).isNotNull();
								} else {
									property.apply(target).isNull();
								}
								return;
							case EQUALS:
								if (!queryProperty.isNegated()) {
									Long primary = queryProperty.getPrimary();
									property.apply(target).id(primary);
									return;
								}
							default:
								throw new IllegalArgumentException(
										"Unsupported entity operation: " + queryProperty
												.getComparisonType().name());
						}


					};
			bindings.add(new FieldBinding<>(field, consumer, property));
			return this;
		}

		/**
		 * Binds an integer field in the REST filter to an enum property in the SearchFilter
 		 * @param field The integer field in the REST filter
		 * @param property The Enum property in the SearchFilter
		 * @param integerToEnumFunction Function for converting the integer to an enum value
		 * @param <T> The type of enum
		 * @return The current instance of BindingFieldCollector
		 */
		public <T extends Enum<T>> BindingFieldCollector<Q, F> withIntegerEnum(
				SerializableFunction<Q, IIntegerProperty> field,
				SerializableFunction<F, ? extends IBasicFilterField<?, T, F>> property,
				SerializableFunction<Integer, T> integerToEnumFunction
		) {
			SerializableBiConsumer<IIntegerProperty, F> consumer =
					(IIntegerProperty queryProperty, F target) -> {
						Integer primary = (Integer) queryProperty.getPrimary();
						switch (queryProperty.getComparisonType()) {

							case NULL:
								if (queryProperty.isNegated()) {
									property.apply(target).isNotNull();
								} else {
									property.apply(target).isNull();
								}
								return;
							case EQUALS:
								if (!queryProperty.isNegated()) {
									property.apply(target).equalTo(
											integerToEnumFunction.apply(primary));
									return;
								} else {
									property.apply(target).notEqualTo(
											integerToEnumFunction.apply(primary));
									return;
								}
							default:
								throw new IllegalArgumentException(
										"Unsupported enum operation: " + queryProperty
												.getComparisonType().name());
						}


					};
			bindings.add(new FieldBinding<>(field, consumer, property));
			return this;
		}

		/**
		 * Binds a String field in the REST filter to an enum property in the SearchFilter
		 * @param field The String field in the REST filter
		 * @param property The Enum property in the SearchFilter
		 * @param stringToEnumFunction Function for converting the String to an enum value
		 * @param <T> The type of enum
		 * @return The current instance of BindingFieldCollector
		 */
		public <T extends Enum<T>> BindingFieldCollector<Q, F> withStringEnum(
				SerializableFunction<Q, IStringProperty<Q>> field,
				SerializableFunction<F, ? extends IBasicFilterField<?, T, F>> property,
				SerializableFunction<String, T> stringToEnumFunction
		) {
			SerializableBiConsumer<IStringProperty<Q>, F> consumer =
					(queryProperty, target) -> {
						switch (queryProperty.getType()) {

							case NULL:
								if (queryProperty.isNegated()) {
									property.apply(target).isNotNull();
								} else {
									property.apply(target).isNull();
								}
								return;
							case EQUALS:
								if (!queryProperty.isNegated()) {
									property.apply(target).equalTo(
											stringToEnumFunction.apply(queryProperty.getValue()));
									return;
								} else {
									property.apply(target).notEqualTo(
											stringToEnumFunction.apply(queryProperty.getValue()));
									return;
								}
							default:
								throw new IllegalArgumentException(
										"Unsupported enum operation: " + queryProperty.getType()
																					  .name());
						}


					};
			bindings.add(new FieldBinding<>(field, consumer, property));
			return this;
		}

		/**
		 * Binds the given boolean in the REST filter to a field in the SearchFilter
		 * @param field The field in the REST filter
		 * @param property The property in the SearchFilter
		 * @return The current instance of BindingFieldCollector
		 */
		public BindingFieldCollector<Q, F> withBoolean(
				SerializableFunction<Q, IBooleanProperty<Q>> field,
				SerializableFunction<F, ? extends IBasicFilterField<?, Boolean,
						F>> property) {
			SerializableBiConsumer<IBooleanProperty<Q>, F> consumer =
					(queryProperty, target) -> {
						IBasicFilterField<?, Boolean, F> filterField = property.apply(target);
						if (queryProperty.isExplicitNull()) {
							if (queryProperty.isNegated()) {
								filterField.isNotNull();
							} else {
								filterField.isNull();
							}
						} else {
							if (queryProperty.isNegated()) {
								filterField.equalTo(!queryProperty.getUnwrappedValue());
							} else {
								filterField.equalTo(queryProperty.getUnwrappedValue());
							}
						}
					};
			bindings.add(new FieldBinding<>(field, consumer, property));
			return this;
		}

		/**
		 * Binds a REST filter String property to SearchFilter property
		 * @param field The REST filter property
		 * @param property The SearchFilter property
		 * @return The current instance of BindingFieldCollector
		 */
		public BindingFieldCollector<Q, F> withString(
				SerializableFunction<Q, IStringProperty<Q>> field,
				SerializableFunction<F, ? extends IStringFilterField<
						?, F>> property) {
			SerializableBiConsumer<IStringProperty<Q>, F> consumer =
					(queryProperty, target) -> {
						IStringFilterField<?, F> filterField = property.apply(target);
						switch (queryProperty.getType()) {
							case NONE:
								return;
							case EQUALS:
								if (queryProperty.isNegated()) {
									filterField.notEqualTo(queryProperty.getValue());
								} else {
									filterField.equalTo(queryProperty.getValue());
								}
								return;
							case EQUALS_IGNORECASE:
								if (queryProperty.isNegated()) {
									filterField.notEqualsIgnoreCase(queryProperty.getValue());
								} else {
									filterField.equalsIgnoreCase(queryProperty.getValue());
								}
								return;
							case LIKE:
								if (queryProperty.isNegated()) {
									filterField.unlike(queryProperty.getValue());
								} else {
									filterField.like(queryProperty.getValue());
								}
								return;
							case ILIKE:
								if (queryProperty.isNegated()) {
									filterField.iUnlike(queryProperty.getValue());
								} else {
									filterField.ilike(queryProperty.getValue());
								}
								return;
							case NULL:
								if (queryProperty.isNegated()) {
									filterField.isNotNull();
								} else {
									filterField.isNull();
								}
						}
					};
			bindings.add(new FieldBinding<>(field, consumer, property));
			return this;
		}

		/**
		 * Build the binding
		 * @return A RESTQueryBinding
		 */
		public RESTQueryBinding<Q, F> build() {
			Map<IQueryProperty, String> propertiesToTest = new HashMap<>();
			Method[] methods = sourceClass.getMethods();

			return TypedResult.attempt(() -> sourceClass.getConstructor().newInstance()).map((Q source) -> {
				for (Method m : methods) {
					if (Modifier.isPublic(m.getModifiers()) && m.getParameterCount() == 0 &&
							IQueryProperty.class.isAssignableFrom(m.getReturnType())) {
							propertiesToTest.put((IQueryProperty) m.invoke(source), m.getName());
					}
				}

				List<IQueryProperty<?>> props = bindings.stream().<IQueryProperty<?>>map(b -> b
						.getSourcePropertyFunction()
						.apply(source))
						.filter(Objects::nonNull)
						.collect(Collectors.toList());

				props.forEach(propertiesToTest::remove);

				if (!propertiesToTest.isEmpty()) {
					throw new IllegalStateException(
							"Invalid binding: there are unmapped properties: ".concat(
									propertiesToTest.values().stream().sorted()
													.collect(Collectors.joining(", "))));
				}

				List<FieldBinding<Q, ?, F>> bindings = this.bindings;
				return new RESTQueryBinding<>(sourceClass, targetClass, bindings);
			}).throwIfNotOk(IllegalStateException::new);
		}


	}

	@Override
	public String toString() {
		return "RESTQueryBinding{" +
				"sourceClass=" + sourceClass +
				", targetClass=" + targetClass +
				'}';
	}

}
