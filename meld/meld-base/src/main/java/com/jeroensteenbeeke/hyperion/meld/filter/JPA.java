package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import org.danekja.java.util.function.serializable.SerializableFunction;

import java.io.Serializable;
import java.util.function.Function;

import javax.persistence.criteria.*;

/**
 * Facade for various JPA classes, used in constructing a query from search filters
 * @param <T> What you're selecting (SELECT T FROM R)
 * @param <R> From where you're selecting it (SELECT T FROM R)
 */
public class JPA<T extends Serializable,R extends Serializable> {
	private final CriteriaBuilder builder;
	
	private final Root<R> root;
	
	private final AbstractQuery<T> baseQuery;

	private final SerializableFunction<BaseSearchFilter<?,?>, Subquery<T>> subqueryCreator;

	/**
	 * Constructor
	 * @param builder A CriteriaBuilder
	 * @param root The query root
	 * @param baseQuery The base querry
	 * @param subqueryCreator Function for creating subqueries based on a given search filter
	 */
	public JPA(CriteriaBuilder builder, Root<R> root, AbstractQuery<T> baseQuery, SerializableFunction<BaseSearchFilter<?,?>, Subquery<T>> subqueryCreator) {
		this.builder = builder;
		this.root = root;
		this.baseQuery = baseQuery;
		this.subqueryCreator = subqueryCreator;
	}

	/**
	 * Returns the CriteriaBuilder
	 * @return The CriteriaBuilder
	 */
	public CriteriaBuilder getBuilder() {
		return builder;
	}

	/**
	 * Returns the query root
	 * @return The root
	 */
	public Root<R> getRoot() {
		return root;
	}

	/**
	 * Returns the base query
	 * @return The base query
	 */
	public AbstractQuery<T> getBaseQuery() {
		return baseQuery;
	}

	/**
	 * Returns logic for creating a subquery based on a given search filter
	 * @return A function (that can be serialized)
	 */
	public SerializableFunction<BaseSearchFilter<?,?>, Subquery<T>> getSubqueryCreator() {
		return subqueryCreator;
	}
}
