package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

import javax.annotation.Nonnull;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;
import java.util.function.BiFunction;

/**
 * Base implementation of a filter field
 *
 * @param <CONTAINING_ENTITY> The entity that contains the attribute in question
 * @param <ENTITY_ATTRIBUTE> The attribute, i.e. the type of the field
 * @param <A> The type of attribute of the JPA metadata
 * @param <F>
 */
public abstract class FilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends Serializable, A
		extends SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>, F extends BaseSearchFilter<CONTAINING_ENTITY,F>>
		implements IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> {

	private static final long serialVersionUID = 1L;

	private final A attribute;

	private final F filter;

	private OrderBy orderBy;

	/**
	 * Constructor
	 * @param attribute The attribute from the JPA metadata that defines the property
	 * @param filter The filter that contains this field
	 */
	@SuppressWarnings("unchecked")
	protected FilterField(@Nonnull SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute, @Nonnull F filter) {
		this.attribute = (A) attribute;
		this.filter = filter;
	}

	/**
	 * Gets the containing filter
	 * @return A filter
	 */
	public F getFilter() {
		return filter;
	}

	/**
	 * Returns the attribute from the JPA metadata
	 * @return The attribute
	 */
	@Nonnull
	@Override
	public A getAttribute() {
		return attribute;
	}

	@Override
	public F orderBy(boolean ascending) {
		this.orderBy = new OrderBy(filter.getAndIncrementLastOrderByIndex(), ascending);
		return filter;
	}

	/**
	 * Retrieves the OrderBy statement for this field, if any
	 * @return Optionally an OrderBy
	 */
	protected Option<OrderBy> getOrderBy() {
		return Option.of(orderBy);
	}

	@Override
	@Nonnull
	public Option<Integer> orderByIndex() {
		return getOrderBy().map(OrderBy::getIndex);
	}

	/**
	 * Convert this property's order by data (if any) to JPA Order objects
	 * @param jpa The JPA context
	 * @param getAttribute Logic to get the relevant attribute
	 * @return Optionally a sequence of orderBy statements
	 */
	protected Option<Seq<Order>> convertOrderByToOrderList(JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa,
														   BiFunction<Root<CONTAINING_ENTITY>, A, Expression<?>>
															getAttribute) {
		return getOrderBy().map(orderBy -> {
			CriteriaBuilder builder = jpa.getBuilder();
			Root<CONTAINING_ENTITY> root = jpa.getRoot();

			if (orderBy.isAscending()) {
				return List.of(builder.asc(getAttribute.apply(root, getAttribute())));
			} else {
				return List.of(builder.desc(getAttribute.apply(root, getAttribute())));
			}
		});
	}

	/**
	 * Internal representation of OrderBy statements
	 */
	protected static class OrderBy implements Serializable {

		private static final long serialVersionUID = 1L;

		private final int index;

		private final boolean ascending;

		private OrderBy(int index, boolean ascending) {
			super();
			this.index = index;
			this.ascending = ascending;
		}

		/**
		 * Returns the index of the statement
		 * @return The index
		 */
		public int getIndex() {
			return index;
		}

		/**
		 * Whether or not the field should be sorted in ascending or descending order
		 * @return {@code true} if sorting in ascending order. {@code false} otherwise
		 */
		public boolean isAscending() {
			return ascending;
		}


	}
}
