package com.jeroensteenbeeke.hyperion.meld.filter;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * If a given field has more than one custom filter, this annotation can group them
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface CustomFilters {
	/**
	 * The filters applicable to the field
	 * @return All applicable custom filters
	 */
	CustomFilter[] value();
}
