package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;

import javax.annotation.Nonnull;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;

/**
 * A basic filter field
 *
 * @param <CONTAINING_ENTITY> The entity to which the field belongs
 * @param <ENTITY_ATTRIBUTE> The attribute type contained by the field
 * @param <FILTER> The filter type containing this field
 */
public interface IBasicFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends Serializable, FILTER extends BaseSearchFilter<CONTAINING_ENTITY,FILTER>>
		extends IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, FILTER> {
	/**
	 * Entities should have the current field equal to the given value
	 * @param value The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	FILTER equalTo(@Nonnull ENTITY_ATTRIBUTE value);

	/**
	 * Entities should have the current field not equal to the given value
	 * @param value The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	FILTER notEqualTo(@Nonnull ENTITY_ATTRIBUTE value);

	/**
	 * Entities should have the current field not set
	 * @return The containing filter
	 */
	@Nonnull
	FILTER isNull();

	/**
	 * Entities should have the current field set to any value
	 * @return The containing filter
	 */
	@Nonnull
	FILTER isNotNull();

}
