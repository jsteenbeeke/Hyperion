package com.jeroensteenbeeke.hyperion.meld;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.filter.IBasicFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IEntityFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IFilterField;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * Abstract search filter implementation
 * @param <T> The type of object this filter searches
 * @param <F> The current type
 */
public abstract class BaseSearchFilter <T extends DomainObject, F extends BaseSearchFilter<T,F>> implements Serializable {
	private int lastOrderByIndex = 0;

	@SuppressWarnings("unchecked")
	private Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass()
			.getGenericSuperclass()).getActualTypeArguments()[0];


	private IFilterField<T,?,F> lastUserFilterField;

	/**
	 * You shouldn't really call this method except from filterfields, but it
	 * doesn't do much harm if you do anyway
	 *
	 * @return The last index used for an orderBy clause
	 */
	public int getAndIncrementLastOrderByIndex() {
		return lastOrderByIndex++;
	}

	/**
	 * Get the entity class for which this filter serves
	 * @return The entity class
	 */
	public Class<T> getEntityClass() {
		return entityClass;
	}

	/**
	 * Create a new disjunction (OR) with the last used filter field
	 * @param newField The field to form a disjunction with
	 * @param <N> The type of field
	 * @return The given field
	 */
	protected final <N extends IFilterField<T,?,F>> N disjunction(N newField) {
		if (lastUserFilterField == null)
			throw new IllegalStateException("Cannot use disjunction without first setting another field");

		lastUserFilterField.or(newField);

		setLastUserFilterField(newField);

		return newField;
	}

	/**
	 * Create a new disjunction (AND) with the last used filter field
	 * @param newField The field to form a disjunction with
	 * @param <N> The type of field
	 * @return The given field
	 */
	protected final <N extends IFilterField<T,?,F>> N conjunction(N newField) {
		if (lastUserFilterField == null)
			throw new IllegalStateException("Cannot use conjunction without first setting another field");

		lastUserFilterField.and(newField);

		setLastUserFilterField(newField);

		return newField;
	}

	/**
	 * Sets the given field to be the last used filter field for purposes of junctions
	 * @param lastUserFilterField The field
	 * @param <N> The field type
	 * @return The field
	 */
	protected final <N extends IFilterField<T,?,F>> N setLastUserFilterField(N lastUserFilterField) {
		this.lastUserFilterField = lastUserFilterField;
		return lastUserFilterField;
	}
}
