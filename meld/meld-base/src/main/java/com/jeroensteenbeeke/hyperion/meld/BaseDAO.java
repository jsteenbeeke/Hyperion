package com.jeroensteenbeeke.hyperion.meld;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.filter.IComparableFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IFilterField;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

import java.io.Serializable;
import java.util.stream.Stream;

/**
 * Base definition of a DAO, which can be used in conjunction with filters to simplify data access
 * @param <T> The type of entity to search for
 * @param <F> The type of filter that can be used with this DAO
 */
public interface BaseDAO<T extends DomainObject, F extends BaseSearchFilter<T,F>> {
	/**
	 * Counts all elements of this type in the database
	 * @return The number of rows in the database
	 */
	long countAll();

	/**
	 * Delets the given object from the database
	 * @param object The object to delete
	 */
	void delete(T object);

	/**
	 * Ensures that the given object no longer persists in the cache
	 *
	 * @param object
	 *            The object to evict
	 */
	void evict(T object);

	/**
	 * Finds all records of this type and returns them as a sequence. WARNING: This method
	 * should be used sparingly. For large tables, or tables with large objects, this will cause
	 * huge memory usage.
	 * @return A sequence containing all entities of this type
	 */
	Seq<T> findAll();

	/**
	 * Streams all records of this type. WARNING: It is a bad idea to collect the results of this method
	 * into a collection unless we're dealing with small tables.
	 * @return A Stream of the results
	 */
	Stream<T> streamAll();

	/**
	 * Retrieves the entity with the given ID, if it exists
	 * @param id The entity's ID
	 * @return Optionally the entity
	 */
	Option<T> load(Serializable id);

	/**
	 * Saves the given entity
	 * @param object The object to save
	 */
	void save(T object);

	/**
	 * Updates the given object
	 * @param object The object
	 * @deprecated JPA does not require updates to be explicit. Any changes to entities that were retrieved from database
	 * are automatically persisted on transaction commit. When using outside of transaction, use explicit flush
	 */
	@Deprecated
	void update(T object);

	/**
	 * Writes changes to database (note that this is not a commit)
	 */
	void flush();

	/**
	 * Retrieves a single entity based on the given filter
	 * @param filter A filter that should yield a unique result
	 * @return Optionally the entity
	 */
	Option<T> getUniqueByFilter(F filter);

	/**
	 * Counts how many entities matching the given filter exist
	 * @param filter The filter to apply
	 * @return The number of entities matching the filter
	 */
	long countByFilter(F filter);

	/**
	 * Finds all entities matching the given filter
	 * @param filter The filter to apply
	 * @return A sequence of entities matching the filter
	 */
	Seq<T> findByFilter(F filter);

	/**
	 * Finds all entities matching the given filter, returning them as a Java stream
	 * @param filter The filter to apply
	 * @return A stream of entities
	 */
	Stream<T> streamByFilter(F filter);

	/**
	 * Finds all entities matching the given filter
	 * @param filter The filter to apply
	 * @param offset The index of the first result (0-based)
	 * @param count The number of results to fetch
	 * @return A sequence of entities matching the filter
	 */
	Seq<T> findByFilter(F filter, long offset, long count);

	/**
	 * Finds all entities matching the given filter, returning them as a Java stream
	 * @param filter The filter to apply
	 * @param offset The index of the first result (0-based)
	 * @param count The number of results to fetch
	 * @return A stream of entities matching the filter
	 */
	Stream<T> streamByFilter(F filter, long offset, long count);

	/**
	 * Calculates the maximum value of the given filter field, filtered as indicated by the filter that contains it
	 * @param field The field to filter by
	 * @param <FT> The type of field
	 * @return Optionally the maximum value
	 */
	<FT extends Comparable<? super FT> & Serializable> Option<FT> max(IComparableFilterField<T,FT,? extends F> field);

	/**
	 * Calculates the minimum value of the given filter field, filtered as indicated by the filter that contains it
	 * @param field The field to filter by
	 * @param <FT> The type of field
	 * @return Optionally the maximum value
	 */
	<FT extends Comparable<? super FT> & Serializable> Option<FT> min(IComparableFilterField<T,FT,? extends F> field);

	/**
	 * Calculates the sum of the given filter field, filtered as indicated by the filter that contains it
	 * @param field The field to filter by
	 * @param <FT> The type of field
	 * @return Optionally the maximum value
	 */
	<FT extends Number & Comparable<? super FT> & Serializable> Option<FT> sum(IComparableFilterField<T,FT,? extends F> field);

	/**
	 * Retrieves the given property. This requires the filter containing the indicated property
	 * to only return a single result
	 * @param field The field to return
	 * @param <FT> The type represented by the field
	 * @return Optionally the indicated property
	 */
	<FT extends Serializable> Option<FT> property(IFilterField<T,FT,? extends F> field);

	/**
	 * Retrieves a list of the given property, filtered as indicated by the filter that contains the field
	 * @param field The field to return
	 * @param <FT> The type of value contained by the field
	 * @return A sequence of the given property
	 */
	<FT extends Serializable> Seq<FT> properties(IFilterField<T,FT,? extends F> field);


}
