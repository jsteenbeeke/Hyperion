package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

import java.io.Serializable;
import java.util.function.Function;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.SingularAttribute;

/**
 * Definition of a filter field: a field that can be used to construct database queries
 *
 * @param <CONTAINING_ENTITY> The entity for which queries are made
 * @param <ENTITY_ATTRIBUTE>  The attribute we're trying to manipulate
 * @param <F>                 The filter type that contains this field
 */
public interface IFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends Serializable, F extends BaseSearchFilter<CONTAINING_ENTITY, F>>
	extends Serializable {
	/**
	 * Returns the filter that contains this field
	 * @return The filter
	 */
	F getFilter();

	/**
	 * Returns the JPA metadata attribute for the field
	 *
	 * @param <A> The type of attribute
	 * @return The attribute
	 */
	<A extends SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE>> A getAttribute();

	/**
	 * Optionally returns a predicate representing this field
	 *
	 * @param jpa  The JPA context used to construct the query
	 * @param <FT> The expected field type
	 * @return An Option that either contains a predicate, or is empty
	 */
	@Nonnull
	<FT extends Serializable> Option<Predicate> toPredicate(JPA<FT, CONTAINING_ENTITY> jpa);

	/**
	 * Optionally return a sequence of OrderBy instructions
	 * @param jpa The JPA context for constructing the orders
	 * @return Optionally a sequence of orders
	 */
	@Nonnull
	Option<Seq<Order>> toOrderBy(JPA<ENTITY_ATTRIBUTE, CONTAINING_ENTITY> jpa);

	/**
	 * Indicated whether this field is set
	 * @return {@code true} if this field has a value, {@code false} otherwise
	 */
	boolean isSet();

	/**
	 * Returns the index for order by statements, indicated where in the list of order statements
	 * this field belongs
	 * @return Optionally an index
	 */
	@Nonnull
	default Option<Integer> orderByIndex() {
		return Option.none();
	}

	/**
	 * Instructs the filter to order by this statement
	 * @param ascending {@code true} if you want to sort this field in ascending order. {@code false} for descending order
	 * @return The containing filter
	 */
	F orderBy(boolean ascending);

	/**
	 * Method for internal use. Processes junctions (AND and OR)
	 * @param junctionType The type of junction
	 * @param junction The field with which we're combining
	 * @param jpa The JPA context
	 * @param internalCreatePredicate The method for creating predicated
	 * @param <JT> The type of value contained by the junction attribute
	 * @return Optionally a predicate
	 */
	default <JT extends Serializable> Option<Predicate> processJunction(JunctionType junctionType,
																		IFilterField<CONTAINING_ENTITY, JT, F> junction,
																		JPA<JT, CONTAINING_ENTITY> jpa,
																		Function<JPA<JT, CONTAINING_ENTITY>, Option<Predicate>> internalCreatePredicate
	) {
		CriteriaBuilder builder = jpa.getBuilder();

		if (junctionType != null) {

			if (junction == null || !junction.isSet()) {
				throw new IllegalStateException(String.format("Incomplete %s filter config", junctionType.name()));
			}

			switch (junctionType) {
				case OR:
					return Option.of(builder.or(internalCreatePredicate
										  .apply(jpa)
										  .getOrElseThrow(IllegalStateException::new), junction
										  .toPredicate(jpa)
										  .getOrElseThrow(IllegalStateException::new)));
				case AND:
					return Option.of(builder.and(internalCreatePredicate
										   .apply(jpa)
										   .getOrElseThrow(IllegalStateException::new), junction
										   .toPredicate(jpa)
										   .getOrElseThrow(IllegalStateException::new)));
			}
		}

		return internalCreatePredicate.apply(jpa);
	}


	/**
	 * Creates a conjunction (AND)
	 *
	 * @param target The field with which to form the conjunction
	 * @param <N> The type of target field
	 *
	 * @return A new filter field to create a conjunction with
	 */
	<N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> and(N target);

	/**
	 * Creates a disjunction (OR)
	 *
	 * @param target The field with which to form the disjunction
	 * @param <N> The type of target field
	 *
	 * @return A new filter field to create a disjunction with
	 */
	<N extends IFilterField<CONTAINING_ENTITY, ?, F>> IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> or(N target);

	/**
	 * Creates a new field similar to the current one. For internal use
	 * @return The new field
	 */
	IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> createNew();

	/**
	 * The type of junction
	 */
	enum JunctionType {
		/**
		 * Conjunction
		 */
		AND,
		/**
		 * Disjunction
		 */
		OR
	}

	/**
	 * FOR INTERNAL USE ONLY
	 * <p>
	 * Checks if the current property is already set, and if so: throws an exception
	 */
	default void assertNotSet() {
		if (isSet()) {
			throw new IllegalStateException("Repeat invocation of filter method. Use the and/or helper functions instead");
		}
	}
}
