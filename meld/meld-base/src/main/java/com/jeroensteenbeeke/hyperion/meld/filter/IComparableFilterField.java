package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import io.vavr.collection.Seq;

import javax.annotation.Nonnull;
import javax.persistence.metamodel.Attribute;
import java.io.Serializable;

/**
 * A filter field of a comparable value
 * @param <CONTAINING_ENTITY> The entity to which the field belongs
 * @param <ENTITY_ATTRIBUTE> The attribute type contained by the field
 * @param <F> The filter type containing this field
 */
public interface IComparableFilterField<
		CONTAINING_ENTITY extends DomainObject,
		ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable,
		F extends BaseSearchFilter<CONTAINING_ENTITY,F>> extends IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE,F> {
	/**
	 * Entities should have the current field not set
	 * @return The containing filter
	 */
	@Nonnull
	F isNull();

	/**
	 * Entities should have the current field set to any value
	 * @return The containing filter
	 */
	@Nonnull
	F isNotNull();

	/**
	 * Entities should have the current field equal to the given value
	 * @param value The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F equalTo(ENTITY_ATTRIBUTE value);

	/**
	 * Entities should have the current field not equal to the given value
	 * @param value The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F notEqualTo(ENTITY_ATTRIBUTE value);

	/**
	 * Entities should have the current field greater than the given value
	 * @param lowerBound The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F greaterThan(ENTITY_ATTRIBUTE lowerBound);

	/**
	 * Entities should have the current field greater than or equal to the given value
	 * @param lowerBound The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F greaterThanOrEqualTo(ENTITY_ATTRIBUTE lowerBound);

	/**
	 * Entities should have the current field less than the given value
	 * @param upperBound The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F lessThan(ENTITY_ATTRIBUTE upperBound);

	/**
	 * Entities should have the current field less than or equal to the given value
	 * @param upperBound The value to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F lessThanOrEqualTo(ENTITY_ATTRIBUTE upperBound);

	/**
	 * Entities should have the current field between the two given values (inclusive)
	 * @param lowerBound The lower bound to filter by
	 * @param upperBound The upper bound to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F between(ENTITY_ATTRIBUTE lowerBound, ENTITY_ATTRIBUTE upperBound);

	/**
	 * Entities should have the current field not between the two given values (inclusive)
	 * @param lowerBound The lower bound to filter by
	 * @param upperBound The upper bound to filter by
	 * @return The containing filter
	 */
	@Nonnull
	F notBetween(ENTITY_ATTRIBUTE lowerBound, ENTITY_ATTRIBUTE upperBound);

	/**
	 * Entities should have the current field with a value equal to one of the given values
	 * @param values The values to check
	 * @return The containing filter
	 */
	@Nonnull
	F in(@Nonnull Seq<ENTITY_ATTRIBUTE> values);

}
