package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;

import javax.annotation.Nonnull;
import javax.persistence.metamodel.SingularAttribute;

/**
 * Filter field containing a String
 * @param <CONTAINING_ENTITY> The entity containing the String
 * @param <F> The filter type
 */
public interface IStringFilterField<CONTAINING_ENTITY extends DomainObject, F extends BaseSearchFilter<CONTAINING_ENTITY, F>>
		extends IBasicFilterField<CONTAINING_ENTITY, String, F> {
	/**
	 * Sets this field to use a case insensitive equals
	 * @param target The expression to match against
	 * @return The current filter
	 */
	@Nonnull
	F equalsIgnoreCase(@Nonnull String target);

	/**
	 * Sets this field to use a negated case insensitive equals
	 * @param target The expression to match against
	 * @return The current filter
	 */
	@Nonnull
	F notEqualsIgnoreCase(@Nonnull String target);

	/**
	 * Sets this field to use a case insensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@Nonnull
	F like(@Nonnull String expression);

	/**
	 * Sets this field to use a negated case sensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@Nonnull
	F unlike(@Nonnull String expression);

	/**
	 * Sets this field to use a case insensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@Nonnull
	F ilike(@Nonnull String expression);

	/**
	 * Sets this field to use a negated case insensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@Nonnull
	F iUnlike(@Nonnull String expression);


}
