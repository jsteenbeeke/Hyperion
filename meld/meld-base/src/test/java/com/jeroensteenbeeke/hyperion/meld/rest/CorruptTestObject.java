package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.rest.query.*;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

/**
 * Dummy implementation of a query object for testing purposes
 */
public class CorruptTestObject implements QueryObject<String> {
	private IntegerProperty<CorruptTestObject> intEnumVal = new IntegerProperty<>(this, "intEnum");


	private int nextSortIndex;

	public CorruptTestObject() throws IllegalAccessException {
		throw new IllegalAccessException();
	}

	public IntegerProperty<CorruptTestObject> intEnumVal() {
		return intEnumVal;
	}

	@Override
	public int getNextSortIndex() {
		return nextSortIndex++;
	}

	@Override
	public boolean matches(String object) {
		return true;
	}
}
