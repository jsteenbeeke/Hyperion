package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.IBasicFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IComparableFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IEntityFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IStringFilterField;
import org.mockito.Mockito;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

public class TestFilter extends BaseSearchFilter<TestEntity, TestFilter> {

	private TestBasicFilterField<TestEntity, Boolean, TestFilter> booleanVal = basicMock();

	private TestStringFilterField<TestEntity, TestFilter> name = stringMock();

	private TestBasicFilterField<TestEntity, TestEnum, TestFilter> enumVal = basicMock();

	private TestComparableFilterField<TestEntity, BigDecimal, TestFilter> bigdecimal =
			comparableMock();

	private TestComparableFilterField<TestEntity, BigInteger, TestFilter> biginteger =
			comparableMock();

	private TestComparableFilterField<TestEntity, Double, TestFilter> doubleVal = comparableMock();

	private TestComparableFilterField<TestEntity, Float, TestFilter> floatVal = comparableMock();

	private TestComparableFilterField<TestEntity, Integer, TestFilter> integer = comparableMock();

	private TestComparableFilterField<TestEntity, LocalDate, TestFilter> localDate = comparableMock();

	private TestComparableFilterField<TestEntity, LocalDateTime, TestFilter> localDateTime =
			comparableMock();

	private TestComparableFilterField<TestEntity, Long, TestFilter> longVal = comparableMock();

	private TestComparableFilterField<TestEntity, Short, TestFilter> shortVal = comparableMock();

	private TestComparableFilterField<TestEntity, ZonedDateTime, TestFilter> zonedDateTime =
			comparableMock();

	private TestComparableFilterField<TestEntity, Date, TestFilter> classicDate =
			comparableMock();

	private TestEntityFilterField<TestEntity, TestOwner, TestFilter, TestOwnerFilter> owner = entityMock();

	@SuppressWarnings("unchecked")
	private <P extends Serializable> TestBasicFilterField<TestEntity, P, TestFilter> basicMock() {
		return (TestBasicFilterField<TestEntity, P, TestFilter>) Mockito.mock(TestBasicFilterField
																				   .class);
	}

	@SuppressWarnings("unchecked")
	private <P extends DomainObject, PF extends BaseSearchFilter<P, PF>> TestEntityFilterField<TestEntity, P, TestFilter, PF> entityMock() {
		return Mockito.mock(TestEntityFilterField.class);
	}

	@SuppressWarnings("unchecked")
	private TestStringFilterField<TestEntity, TestFilter> stringMock() {
		return (TestStringFilterField<TestEntity, TestFilter>) Mockito.mock(TestStringFilterField.class);
	}

	@SuppressWarnings("unchecked")
	private <C extends Comparable<? super C> & Serializable> TestComparableFilterField<TestEntity, C,
			TestFilter> comparableMock() {
		return (TestComparableFilterField<TestEntity, C, TestFilter>) Mockito
				.mock(TestComparableFilterField.class);
	}

	public TestBasicFilterField<TestEntity, Boolean, TestFilter> booleanVal() {
		return booleanVal;
	}

	public TestBasicFilterField<TestEntity, TestEnum, TestFilter> enumVal() {
		return enumVal;
	}

	public TestStringFilterField<TestEntity, TestFilter> name() {
		return name;
	}

	public TestComparableFilterField<TestEntity, ZonedDateTime, TestFilter> zonedDateTime() {
		return zonedDateTime;
	}

	public TestComparableFilterField<TestEntity, BigDecimal, TestFilter> bigdecimal() {
		return bigdecimal;
	}

	public TestComparableFilterField<TestEntity, BigInteger, TestFilter> biginteger() {
		return biginteger;
	}

	public TestComparableFilterField<TestEntity, Double, TestFilter> doubleVal() {
		return doubleVal;
	}

	public TestComparableFilterField<TestEntity, Float, TestFilter> floatVal() {
		return floatVal;
	}

	public TestComparableFilterField<TestEntity, Integer, TestFilter> integer() {
		return integer;
	}

	public TestComparableFilterField<TestEntity, LocalDate, TestFilter> localDate() {
		return localDate;
	}

	public TestComparableFilterField<TestEntity, LocalDateTime, TestFilter> localDateTime() {
		return localDateTime;
	}

	public TestComparableFilterField<TestEntity, Long, TestFilter> longVal() {
		return longVal;
	}

	public TestComparableFilterField<TestEntity, Short, TestFilter> shortVal() {
		return shortVal;
	}

	public TestComparableFilterField<TestEntity, Date, TestFilter> classicDate() {
		return classicDate;
	}

	public TestEntityFilterField<TestEntity, TestOwner, TestFilter, TestOwnerFilter> owner() {
		return owner;
	}
}

class TestOwnerFilter extends BaseSearchFilter<TestOwner, TestOwnerFilter> {

}

interface TestComparableFilterField<
		CONTAINING_ENTITY extends DomainObject,
		ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable,
		F extends BaseSearchFilter<CONTAINING_ENTITY, F>> extends IComparableFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> {
}

interface TestStringFilterField<CONTAINING_ENTITY extends DomainObject, F extends BaseSearchFilter<CONTAINING_ENTITY, F>> extends IStringFilterField<CONTAINING_ENTITY, F> {

}

interface TestEntityFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends DomainObject, F
		extends BaseSearchFilter<CONTAINING_ENTITY, F>, EF extends BaseSearchFilter<ENTITY_ATTRIBUTE, EF>> extends IEntityFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F, EF> {

}

interface TestBasicFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends Serializable, FILTER extends BaseSearchFilter<CONTAINING_ENTITY,FILTER>> extends IBasicFilterField<CONTAINING_ENTITY,ENTITY_ATTRIBUTE,FILTER> {

}
