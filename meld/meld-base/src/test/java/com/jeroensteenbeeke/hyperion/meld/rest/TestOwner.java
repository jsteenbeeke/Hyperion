package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;

import javax.persistence.Id;
import java.io.Serializable;

public class TestOwner extends BaseDomainObject {
	@Id
	private Long id;

	@Override
	public Serializable getDomainObjectId() {
		return id;
	}
}
