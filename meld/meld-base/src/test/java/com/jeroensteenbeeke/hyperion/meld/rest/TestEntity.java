package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class TestEntity extends BaseDomainObject{
	@Id
	private Long id;

	@ManyToOne
	private TestOwner owner;

	@Override
	public Serializable getDomainObjectId() {
		return id;
	}


}
