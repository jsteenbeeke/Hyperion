package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.rest.query.IntegerProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

/**
 * Dummy implementation of a query object for testing purposes
 */
public class CorruptTestObject2 implements QueryObject<String> {
	private IntegerProperty<CorruptTestObject2> intEnumVal = new IntegerProperty<>(this, "intEnum");


	private int nextSortIndex;

	public CorruptTestObject2() throws InstantiationException {
		throw new InstantiationException();
	}

	public IntegerProperty<CorruptTestObject2> intEnumVal() {
		return intEnumVal;
	}

	@Override
	public int getNextSortIndex() {
		return nextSortIndex++;
	}

	@Override
	public boolean matches(String object) {
		return true;
	}
}
