package com.jeroensteenbeeke.hyperion.password.argon2;

import org.junit.Test;

import java.util.function.Consumer;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotEquals;

public class Argon2PasswordHasherTest {
	private static final int MANY_TIMES = 100;

	private final int ITERATIONS = 4;

	@Test
	public void testConsistency() {
		final String password = "test";

		assertNotEquals(password, Argon2PasswordHasher.hashNewPassword(password.toCharArray()).withRecommendedMinimumHashLength().withIterations(ITERATIONS).withPHCIssue9DefaultMemorySettings().withPHCIssue9DefaultParallelism());

		manyTimes("consistency", length -> {
			String hash1 = Argon2PasswordHasher
					.hashNewPassword(password.toCharArray()).withHashLength(length).withIterations(ITERATIONS).withMemoryInKiB(4000)
					.withParallelism(3);
			assertTrue(Argon2PasswordHasher.checkExistingPassword(password.toCharArray()).withHash(hash1));
		});
	}

	@Test
	public void testPasswordCollisions() {
		final String password = "test";

		manyTimes("password collisions", length -> {
			String hash1 = Argon2PasswordHasher
					.hashNewPassword(password.toCharArray()).withHashLength(length).withIterations(ITERATIONS).withPHCIssue9DefaultMemorySettings()
					.withPHCIssue9DefaultParallelism();
			String hash2 = Argon2PasswordHasher
					.hashNewPassword(password.toCharArray()).withHashLength(length).withIterations(ITERATIONS).withPHCIssue9DefaultMemorySettings()
					.withPHCIssue9DefaultParallelism();

			assertNotEquals("Hashes should differ accross salts", hash1, hash2);
		});

	}

	private void manyTimes(String label, Consumer<Integer> operation) {
		System.out.printf("Running %d iterations of %s", MANY_TIMES, label).println();
		for (int i = 0; i < MANY_TIMES; i++) {
			System.out.printf("\t%d", (i+1));
			for (int k = 16; k <= 64; k += 16) {
				System.out.printf("\t%s", k);
				operation.accept(k);
				System.out.flush();
			}
			System.out.println();
		}

	}
}
