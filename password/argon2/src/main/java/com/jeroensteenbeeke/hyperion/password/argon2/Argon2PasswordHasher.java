package com.jeroensteenbeeke.hyperion.password.argon2;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Utility class for creating OWASP-grade Argon2 secure password hashes
 */
public class Argon2PasswordHasher {
	private static final int MEMORY_DEFAULT_PHC_9 = 128000;

	private static final int PARALLELISM_DEFAULT_PHC_9 = 4;

	private static final int HASH_LENGTH_MINIMUM = 16;

	private static final Argon2 ARGON_2_INSTANCE = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id);

	/**
	 * Hash a new password (i.e. a password which does not yet have a corresponding salt). You should generally call
	 * this
	 * method when creating a new user, or when a user changes their password to avoid reusing the salt.
	 *
	 * @param password The password to hash
	 * @return A builder that will ask for further information
	 */
	public static NewPasswordHashBuilderStep1 hashNewPassword(char[] password) {
		return hashLength -> iterations -> memoryInKb -> parallelism -> {
			synchronized (ARGON_2_INSTANCE) {
				try {
					return ARGON_2_INSTANCE.hash(iterations, memoryInKb, parallelism,
												 password);
				} finally {
					ARGON_2_INSTANCE.wipeArray(password);
				}
			}
		};

	}

	/**
	 * Hash an existing password (i.e. a password which has a corresponding salt)
	 *
	 * @param password The password to hash
	 * @return A builder that will ask for further information
	 */
	public static ExistingPasswordHashBuilderStep1 checkExistingPassword(char[] password) {
		return hash -> {
			synchronized (ARGON_2_INSTANCE) {
				try {
					return ARGON_2_INSTANCE.verify(hash, password);
				} finally {
					ARGON_2_INSTANCE.wipeArray(password);
				}
			}
		};

	}


	/**
	 * First step in the password hash builder for existing passwords. Asks for the salt to use
	 */
	public interface ExistingPasswordHashBuilderStep1 extends
			Predicate<String> {
		/**
		 * Sets the hash for the existing password
		 *
		 * @param hash The hash to use
		 * @return A builder that will continue the hashing process
		 */
		default boolean withHash(String hash) {
			return test(hash);
		}
	}


	/**
	 * First step in the password hash builder. Asks for the salt length to use
	 */
	public interface NewPasswordHashBuilderStep1 extends Function<Integer, NewPasswordHashBuilderStep2> {
		/**
		 * Sets the hash length to use for hashing the password
		 *
		 * @param length The number of bytes to use for the hash
		 * @return A builder that will continue the hashing process
		 */
		default NewPasswordHashBuilderStep2 withHashLength(int length) {
			return apply(length);
		}

		/**
		 * Sets the hash length to use for hashing the password, using
		 * a 16 byte recommended minimum
		 *
		 * @return A builder that will continue the hashing process
		 */
		default NewPasswordHashBuilderStep2 withRecommendedMinimumHashLength() {
			return apply(HASH_LENGTH_MINIMUM);
		}
	}

	/**
	 * Second step in the password hash builder. Asks for the number of iterations
	 */
	public interface NewPasswordHashBuilderStep2 extends Function<Integer, NewPasswordHashBuilderStep3> {
		/**
		 * Sets the number of iterations for the Argon2 algorithm
		 *
		 * @param iterations The number of iterations to apply
		 * @return A builder that will continue the hashing process
		 */
		default NewPasswordHashBuilderStep3 withIterations(int iterations) {
			return apply(iterations);
		}
	}

	/**
	 * Third step in the password hash builder. Asks for the amount of memory to use
	 */
	public interface NewPasswordHashBuilderStep3 extends Function<Integer, NewPasswordHashBuilderStep4> {
		/**
		 * Sets the number of kilobytes to consume for the hashing process
		 *
		 * @param memory The memory in kilobytes to use for the algorithm
		 * @return A builder that will continue the hashing process
		 */
		default NewPasswordHashBuilderStep4 withMemoryInKiB(int memory) {
			return apply(memory);
		}

		/**
		 * Sets the number of kilobytes to consume for the hashing process, defaulting
		 * to the recommended from the OWASP password storage cheat sheet as published
		 * on the 28th of February 2019 (taken from PHC issue 9): 128MB
		 *
		 * @return A builder that will continue the hashing process
		 */
		default NewPasswordHashBuilderStep4 withPHCIssue9DefaultMemorySettings() {
			return apply(MEMORY_DEFAULT_PHC_9);
		}
	}

	/**
	 * Fourth step in the password hash builder. Asks for the degree of parallelism it is allowed to use
	 */
	public interface NewPasswordHashBuilderStep4 extends Function<Integer, String> {
		/**
		 * Sets the number of threads to use for computing the hash
		 *
		 * @param parallelism The number of threads
		 * @return The computed hash
		 */
		default String withParallelism(int parallelism) {
			return apply(parallelism);
		}

		/**
		 * Sets the number of threads to use for computing the hash, defaulting
		 * to the recommended from the OWASP password storage cheat sheet as published
		 * on the 28th of February 2019 (taken from PHC issue 9): 4
		 *
		 * @return The computed hash
		 */
		default String withPHCIssue9DefaultParallelism() {
			return apply(PARALLELISM_DEFAULT_PHC_9);
		}
	}

}
