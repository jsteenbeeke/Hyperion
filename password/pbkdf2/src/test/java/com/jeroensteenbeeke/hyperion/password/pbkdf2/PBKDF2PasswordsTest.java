/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.password.pbkdf2;

import org.junit.Assert;
import org.junit.Test;

import java.util.function.Consumer;

import com.jeroensteenbeeke.hyperion.password.pbkdf2.PBKDF2PasswordHasher.HashAndSalt;
import com.jeroensteenbeeke.hyperion.password.pbkdf2.PBKDF2PasswordHasher.KeyLength;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PBKDF2PasswordsTest {
	private static final int MANY_TIMES = 100;
	private static final int SALT_LENGTH = 4;

	private final int ITERATIONS = 10000;

	@Test
	public void testConsistency() {
		final String password = "test";

		manyTimes("consistency", length -> {
			HashAndSalt hashAndSalt = PBKDF2PasswordHasher
					.hashNewPassword(password).withSaltLength(SALT_LENGTH).withKeyLength(length).andIterations(ITERATIONS)
					.ifNotOk(Assert::assertNull).getObject();
			String hashExisting = PBKDF2PasswordHasher
					.hashExistingPassword(
							password).withSalt(hashAndSalt.getSalt()).withKeyLength(
							length).andIterations(ITERATIONS).ifNotOk(Assert::assertNull).getObject();

			assertEquals(
					"Repeat hashing with same salt should yield same hash",
					hashAndSalt.getHash(), hashExisting);
		});
	}

	@Test
	public void testUserCollisions() {
		final String password = "test";

		manyTimes("user collisions", length -> {

			HashAndSalt hashAndSalt1 = PBKDF2PasswordHasher
					.hashNewPassword(password).withSaltLength(SALT_LENGTH).withKeyLength(length).andIterations(ITERATIONS)
					.ifNotOk(Assert::assertNull).getObject();
			HashAndSalt hashAndSalt2 = PBKDF2PasswordHasher
					.hashNewPassword(password).withSaltLength(SALT_LENGTH).withKeyLength(length).andIterations(ITERATIONS)
					.ifNotOk(Assert::assertNull).getObject();

			assertNotEquals("Hashes should differ accross users",
					hashAndSalt1.getHash(), hashAndSalt2.getHash());
			assertNotEquals("Salts should differ accross users",
					hashAndSalt1.getSalt(), hashAndSalt2.getSalt());
		});
	}

	@Test
	public void testPasswordCollisions() {
		final String password = "test";
		final String salt1 = "salt";
		final String salt2 = "tlas";

		manyTimes("password collisions", length -> {
			String hash1 = PBKDF2PasswordHasher
					.hashExistingPassword(
							password).withSalt(salt1.getBytes()).withKeyLength(
							length).andIterations(ITERATIONS).ifNotOk(Assert::assertNull).getObject();
			String hash2 = PBKDF2PasswordHasher
					.hashExistingPassword(
							password).withSalt(salt2.getBytes()).withKeyLength(
							length).andIterations(ITERATIONS).ifNotOk(Assert::assertNull).getObject();

			assertNotEquals("Hashes should differ accross salts", hash1, hash2);
		});

	}

	private void manyTimes(String label, Consumer<KeyLength> operation) {
		System.out.printf("Running %d iterations of %s", MANY_TIMES, label).println();
		for (int i = 0; i < MANY_TIMES; i++) {
			System.out.printf("\t%d", (i+1));
			for (KeyLength length: KeyLength.values()) {
				System.out.printf("\t%s", length.getLength());
				operation.accept(length);
				System.out.flush();
			}
			System.out.println();
		}

	}

}
