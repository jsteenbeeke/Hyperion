/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.password.pbkdf2;

import com.jeroensteenbeeke.hyperion.util.Randomizer;
import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.util.Base64;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Utility class for creating OWASP-grade PBKDF2 secure password hashes
 */
public final class PBKDF2PasswordHasher {
	/**
	 * Allowed key lengths
	 */
	public enum KeyLength {
		Length256(256),
		Length384(384),
		Length512(512),
		Length768(768),
		Length1024(1024);

		private final int length;

		KeyLength(int length) {
			this.length = length;
		}

		/**
		 * Gets the length in bytes of the current KeyLength
		 *
		 * @return The key length to use. Generally this is a (sum of) multiple(s) of 2
		 */
		public int getLength() {
			return length;
		}
	}

	private PBKDF2PasswordHasher() {

	}

	/**
	 * Container object for the hash and salt, both Base64-encoded
	 */
	public static final class HashAndSalt {
		private final String hash;

		private final byte[] salt;

		private HashAndSalt(byte[] hash, byte[] salt) {
			this.hash = Base64.getEncoder().encodeToString(hash);
			this.salt = salt;
		}

		/**
		 * @return The base64 encoded hash
		 */
		public String getHash() {
			return hash;
		}

		/**
		 * @return The base64 encoded salt
		 */
		public byte[] getSalt() {
			return salt;
		}

	}

	/**
	 * Hash a new password (i.e. a password which does not yet have a corresponding salt). You should generally call
	 * this
	 * method when creating a new user, or when a user changes their password to avoid reusing the salt.
	 *
	 * @param password The password to hash
	 * @return A builder that will ask for further information
	 */
	public static NewPasswordHashBuilderStep1 hashNewPassword(String password) {
		return saltLength -> keyLength -> iterations -> {
			byte[] salt = Randomizer.randomBytes(saltLength);
			return hashPassword(password.toCharArray(),
								salt, iterations, keyLength.getLength()
			).map(hash -> new HashAndSalt(hash, salt));
		};

	}

	/**
	 * Hash an existing password (i.e. a password which has a corresponding salt)
	 *
	 * @param password The password to hash
	 * @return A builder that will ask for further information
	 */
	public static ExistingPasswordHashBuilderStep1 hashExistingPassword(String password) {
		return salt -> keyLength -> iterations -> hashPassword(password.toCharArray(),
															   salt, iterations, keyLength.getLength()).map(hash -> Base64.getEncoder().encodeToString(hash));

	}

	private static TypedResult<byte[]> hashPassword(final char[] password,
													final byte[] salt, final int iterations, final int keyLength) {
		return TypedResult.attempt(() -> {
			SecretKeyFactory skf = SecretKeyFactory
					.getInstance("PBKDF2WithHmacSHA512");
			PBEKeySpec spec = new PBEKeySpec(password, salt, iterations,
											 keyLength);
			SecretKey key = skf.generateSecret(spec);

			return key.getEncoded();
		});
	}

	/**
	 * First step in the password hash builder for existing passwords. Asks for the salt to use
	 */
	public interface ExistingPasswordHashBuilderStep1 extends
			Function<byte[], PasswordHashBuilderStep2<String>> {
		/**
		 * Sets the salt to use for hashing the password
		 *
		 * @param salt The salt to use
		 * @return A builder that will continue the hashing process
		 */
		default PasswordHashBuilderStep2<String> withSalt(byte[] salt) {
			return apply(salt);
		}
	}


	/**
	 * First step in the password hash builder. Asks for the salt to use
	 */
	public interface NewPasswordHashBuilderStep1 extends Function<Integer, PasswordHashBuilderStep2<HashAndSalt>> {
		/**
		 * Sets the salt length to use for hashing the password
		 *
		 * @param length The number of bytes to use for the salt
		 * @return A builder that will continue the hashing process
		 */
		default PasswordHashBuilderStep2<HashAndSalt> withSaltLength(int length) {
			return apply(length);
		}
	}

	/**
	 * Second step in the password hash builder. Asks for the key length to use
	 *
	 * @param <T> The type of result given once the builder sequence has been successfully completed
	 */
	public interface PasswordHashBuilderStep2<T>
			extends Function<KeyLength, PasswordHashBuilderStep3<T>> {


		/**
		 * Sets the key length to use for the hash algorithm
		 *
		 * @param keyLength The length to use. Should not be {@code null}
		 * @return A builder that will finalize the hashing process
		 */
		default PasswordHashBuilderStep3<T> withKeyLength(@Nonnull KeyLength keyLength) {
			return apply(keyLength);
		}
	}

	/**
	 * Second step in the password hash builder. Asks for the number of iterations to use
	 *
	 * @param <T> The type of result given once the builder sequence has been successfully completed
	 */
	public interface PasswordHashBuilderStep3<T>
			extends Function<Integer, TypedResult<T>> {

		/**
		 * Sets the number of iterations to use, and calculates the hash
		 *
		 * @param iterations The iterations to use
		 * @return A TypedResult, either containing the indicated return type, or an error message indicating why it
		 * could not be calculated
		 */
		default TypedResult<T> andIterations(int iterations) {
			return apply(iterations);
		}
	}

}
