package com.jeroensteenbeeke.hyperion.passwordpolicy;

/**
 * Contains convenience methods to check passwords against a number of rules
 */
public class PasswordPolicy {
	/**
	 * Determines whether or not a password is common, by comparing it against the top million
	 * passwords found on https://github.com/danielmiessler/SecLists/tree/master/Passwords/Common-Credentials
	 *
	 * @param password The password to check
	 * @return {@code true} if the password was on the list (=bad), {@code false} otherwise
	 */
	public static boolean isCommonPassword(String password) {
		return ListFileAnalyzer.fileContains("10-million-password-list-top-1000000.txt",
				password::equals);
	}

	/**
	 * Determines whether or not the given password is a common topology, by comparing it against a list
	 * of pregenerated topologies (based on both the top password list and sample lists from Pathwell
	 *
	 * @param password The password to verify
	 * @return {@code true} if the password's topology is common (=bad), {@code false} otherwise
	 */
	public static boolean isCommonTopology(String password) {
		return ListFileAnalyzer.fileContains("topologies.txt", getTopology(password)::equals);
	}

	/**
	 * Determines the password's topology
	 *
	 * @param password The password to check
	 * @return The determined topology (u=uppercase,l=lowercase,d=digit,s=symbol)
	 */
	static String getTopology(String password) {
		StringBuilder topology = new StringBuilder();

		for (char c : password.toCharArray()) {
			if (Character.isUpperCase(c)) {
				topology.append('u');
			} else if (Character.isLowerCase(c)) {
				topology.append('l');
			} else if (Character.isDigit(c)) {
				topology.append('d');
			} else {
				topology.append('s');
			}

		}

		return topology.toString();
	}

//	public static void main(String[] args) throws IOException {
//		File file = File.createTempFile("topologies", ".txt");
//
//		Set<String> topologies = new HashSet<>();
//
//		topologies.add("ullllldd");
//		topologies.add("ulllllldd");
//		topologies.add("ullldddd");
//		topologies.add("llllllld");
//		topologies.add("ullllllldd");
//		topologies.add("ulllllld");
//		topologies.add("ullllldddd");
//		topologies.add("ulllldddd");
//		topologies.add("lllllldd");
//		topologies.add("ullllllld");
//		topologies.add("ullllddd");
//		topologies.add("ulldddds");
//		topologies.add("llllllll");
//		topologies.add("ulllllddd");
//		topologies.add("llllllldd");
//		topologies.add("llsddlddl");
//		topologies.add("lllllllld");
//		topologies.add("ullllldds");
//		topologies.add("ulllllldddd");
//		topologies.add("ulllllllldd");
//		topologies.add("ulllllds");
//		topologies.add("ulllllllld");
//		topologies.add("ullllldddds");
//		topologies.add("lllllllll");
//		topologies.add("lllllllldd");
//		topologies.add("ullllllddd");
//		topologies.add("lllllddd");
//		topologies.add("ullldddds");
//		topologies.add("ullllllldddd");
//		topologies.add("ulllllsdd");
//		topologies.add("uuuuuudl");
//		topologies.add("lllldddd");
//		topologies.add("ddulllllll");
//		topologies.add("ullsdddd");
//		topologies.add("ulllldds");
//		topologies.add("ullllllds");
//		topologies.add("ddullllll");
//		topologies.add("llllsddd");
//		topologies.add("llllllllld");
//		topologies.add("llllldddd");
//		topologies.add("llllllllll");
//		topologies.add("llllllddd");
//		topologies.add("ullllllllldd");
//		topologies.add("ullllllllld");
//		topologies.add("ddddddul");
//		topologies.add("ulllllllddd");
//		topologies.add("ulllllldds");
//		topologies.add("uuuuuuds");
//		topologies.add("uudllldddu");
//		topologies.add("ullllsdd");
//		topologies.add("ulllllsd");
//		topologies.add("lllsdddd");
//		topologies.add("lllllldddd");
//		topologies.add("ullllllldds");
//		topologies.add("ddulllll");
//		topologies.add("ulllllllds");
//		topologies.add("ullllddds");
//		topologies.add("ulllldddds");
//		topologies.add("ulllsdddd");
//		topologies.add("ullllsddd");
//		topologies.add("ulllllldddds");
//		topologies.add("ulllddds");
//		topologies.add("llllsdddd");
//		topologies.add("llllllsdd");
//		topologies.add("lllllldds");
//		topologies.add("ddddulll");
//		topologies.add("dddddddd");
//		topologies.add("ullllllsd");
//		topologies.add("uldddddd");
//		topologies.add("llllllsd");
//		topologies.add("udllllllld");
//		topologies.add("lllllllllll");
//		topologies.add("lllllllllld");
//		topologies.add("llllldds");
//		topologies.add("llllddds");
//		topologies.add("ulllllllldddd");
//		topologies.add("uuuuuuuu");
//		topologies.add("ulllsddd");
//		topologies.add("ullllllsdd");
//		topologies.add("ulllllddds");
//		topologies.add("lllllsdd");
//		topologies.add("ullllsdddd");
//		topologies.add("ulllddddd");
//		topologies.add("ulldddddd");
//		topologies.add("ullddddd");
//		topologies.add("llllllllldd");
//		topologies.add("llllllldds");
//		topologies.add("lllllllddd");
//		topologies.add("llllllds");
//		topologies.add("llldddds");
//		topologies.add("uuullldddd");
//		topologies.add("ulllllsddd");
//		topologies.add("ulllllllsd");
//		topologies.add("llllllllsd");
//		topologies.add("llllllldddd");
//		topologies.add("ulllllsdddd");
//		topologies.add("lllllllds");
//		topologies.add("lllldddds");
//		topologies.add("ddddullll");
//		topologies.add("uudllldddd");
//
//		int last = 0;
//
//		try (InputStreamReader isr = new InputStreamReader(
//				ListFileAnalyzer.class.getResourceAsStream(
//						"10-million-password-list-top-1000000.txt"));
//			 BufferedReader br = new BufferedReader(isr)) {
//
//			String pw;
//			while ((pw = br.readLine()) != null) {
//				topologies.add(getTopology(pw));
//				if (last < topologies.size()) {
//					System.out.print("+");
//					last++;
//					if (last % 80 == 0) {
//						System.out.println();
//					}
//				}
//			}
//			System.out.println();
//			System.out.printf("Determined %d topologies", topologies.size()).println();
//		}
//
//		try (PrintStream p = new PrintStream(new FileOutputStream(file))) {
//			topologies.stream().sorted().forEach(p::println);
//			p.flush();
//		}
//	}
}
