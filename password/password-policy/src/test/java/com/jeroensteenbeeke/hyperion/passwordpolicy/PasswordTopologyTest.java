package com.jeroensteenbeeke.hyperion.passwordpolicy;

import org.junit.Test;

import static com.jeroensteenbeeke.hyperion.passwordpolicy.PasswordPolicy.getTopology;
import static com.jeroensteenbeeke.hyperion.passwordpolicy.PasswordPolicy.isCommonTopology;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PasswordTopologyTest {
	@Test
	public void testTopologies() {
		assertTopology("password");
		assertTopology("Password");
		assertTopology("1337");

		assertNonTopology("sadghj3q4787678dgshjdsfghj");
		assertNonTopology("sp00n$$_______");

	}

	private void assertTopology(String password) {
		assertTrue(String.format("Password '%s' (topology '%s') expected restricted but found approved", password, getTopology(password)),
				isCommonTopology(password));
	}

	private void assertNonTopology(String password) {
		assertFalse(String.format("Password '%s' (topology '%s') expected approved but found restricted", password, getTopology(password)),
				isCommonTopology(password));
	}
}
