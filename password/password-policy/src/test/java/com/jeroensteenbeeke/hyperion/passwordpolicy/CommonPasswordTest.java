package com.jeroensteenbeeke.hyperion.passwordpolicy;

import org.junit.Test;

import static com.jeroensteenbeeke.hyperion.passwordpolicy.PasswordPolicy.isCommonPassword;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CommonPasswordTest {
	@Test
	public void testPassword() {
		assertCommon("password");
		assertCommon("Password");
		assertCommon("vjht008");
		assertUncommon("BVlxCzxWso");
	}

	private void assertCommon(String password) {
		assertTrue(String.format("Password '%s' expected restricted but found approved", password), isCommonPassword(password));
	}

	private void assertUncommon(String password) {
		assertFalse(String.format("Password '%s' expected approved but found restricted", password), isCommonPassword(password));
	}
}
