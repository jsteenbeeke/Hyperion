package com.jeroensteenbeeke.hyperion.passwordpolicy;

import org.junit.Test;

import java.io.IOException;

public class ListFileAnalyzerTest {
	@Test(expected = IOException.class)
	public void readInvalidFile() throws Throwable {
		try {
			ListFileAnalyzer.fileContains("yolo.txt", "YOLO"::equals);
		} catch (RuntimeException e) {
			throw e.getCause();
		}
	}
}
