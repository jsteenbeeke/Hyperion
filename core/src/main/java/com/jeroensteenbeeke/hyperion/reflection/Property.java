/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.reflection;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Interface representing a property (a field with accessors)
 *
 * @param <T> The type of the class that contains the property
 * @param <F> The type of the property's field
 */
public interface Property<T, F> extends Serializable {
	/**
	 * Returns the type of the owning class
	 *
	 * @return The class the property is defined in
	 */
	@Nonnull
	Class<T> getOwningClass();

	/**
	 * Get the type of the property
	 *
	 * @return The class of the property
	 */
	@Nonnull
	Class<F> getPropertyType();

	/**
	 * Get the accessor-method used to read the value of the property
	 *
	 * @return A getter {@code Method}
	 */
	@Nonnull
	Method getter();

	/**
	 * Get the accessor-method used to write the value of the property
	 *
	 * @return A setter {@code Method}, or {@code null} if the property is final
	 */
	@CheckForNull
	Method setter();

	/**
	 * Return the field that is encapsulated in this property
	 *
	 * @return A {@code Field}
	 */
	@Nonnull
	Field field();

	/**
	 * Get the name of the field
	 *
	 * @return The name of the field
	 */
	String name();

	/**
	 * Sets the property, by invoking the setter method if it exists
	 *
	 * @param target The object to set the property of
	 * @param value  The value to set the property to
	 * @throws IllegalStateException If the setter can not be invoked
	 */
	void set(T target, F value);

	/**
	 * Get the property, by invoking the getter method on the target object
	 *
	 * @param target The object to invoke the getter on
	 * @return The value of the property
	 * @throws IllegalStateException If the setter can not be invoked
	 */
	F get(T target);

	/**
	 * Checks if the property is final
	 *
	 * @return {@code true} if the property is immutable, {@code false} otherwise
	 */
	boolean isFinal();
}
