/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.Lists;
import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;

/**
 * Utility class for common actions using Java reflection
 */
public final class Reflector {
	private enum Accessor {
		GET, SET;

		private final String prefix;

		Accessor() {
			this.prefix = name().toLowerCase();
		}

		public String getAccessorName(String field) {
			StringBuilder result = new StringBuilder();

			result.append(prefix);
			result.append(Character.toUpperCase(field.charAt(0)));
			result.append(field.substring(1));

			return result.toString();
		}
	}

	private Reflector() {

	}

	/**
	 * Invokes the getter of the given property, if it exists
	 *
	 * @param target   The object to invoke the getter on
	 * @param property The name of the property to get. For instance, if your property is named &quot;foo&quot;,
	 *                 then this method will search for a method named &quot;getFoo&quot;
	 * @param <T>      The type of the property
	 * @return A {@code TypedResult} either containing the getter's, or the reason for not being able to invoke it
	 */
	@SuppressWarnings("unchecked")
	public static <T> TypedResult<T> invokeGetter(Object target, String property) {
		Class<?> targetClass = target.getClass();
		StringBuilder getterName = new StringBuilder().append("get").append(
				property.substring(0, 1).toUpperCase()).append(
				property.substring(1));
		return TypedResult.attempt(() -> {
			Method method = targetClass.getMethod(getterName.toString());

			return (T) method.invoke(target);
		});

	}

	/**
	 * Attempts to get the class for the given fully qualified domain name
	 *
	 * @param fqdn The fully qualified domain name of the class
	 * @param <T>  The object type of the expected class
	 * @return A TypedResult either containing the class, or a message explaining why it could not be created
	 */
	@SuppressWarnings("unchecked")
	public static <T> TypedResult<Class<T>> getClass(String fqdn) {
		return TypedResult.attempt(() -> (Class<T>) Class.forName(fqdn));
	}

	/**
	 * Attempts to get information about a property through reflection
	 *
	 * @param classRef The class to check the property of
	 * @param name     The name of the property
	 * @param <T>      The type of the containing class
	 * @param <F>      The type of the property
	 * @return A TypedResult either containing the property, or a message explaining why it could not be created
	 */
	public static <T, F> TypedResult<Property<T, F>> getProperty(Class<T> classRef, String name) {
		return TypedResult.attempt(() ->
				classRef.getDeclaredField(name)).flatMap(f -> getProperty(classRef, f));
	}

	/**
	 * Attempts to get information about a property through reflection
	 *
	 * @param classRef The class to check the property of
	 * @param f        The field that belongs to the property
	 * @param <T>      The type of the containing class
	 * @param <F>      The type of the property
	 * @return A TypedResult either containing the property, or a message explaining why it could not be created
	 */
	public static <T, F> TypedResult<Property<T, F>> getProperty(Class<T> classRef, Field f) {
		boolean isFinal = Modifier.isFinal(f.getModifiers());

		String fieldName = f.getName();

		if (isFinal) {
			return TypedResult.attempt(() -> f.getDeclaringClass().getMethod(Accessor.GET.getAccessorName(fieldName)))
					.map(getter -> new DefaultProperty<T, F>(classRef, getter, null, f, fieldName, true));
		} else {
			return TypedResult.attempt(() -> f.getDeclaringClass().getMethod(Accessor.GET.getAccessorName(fieldName)))
					.map(getter -> Lists.newArrayList(getter, f.getDeclaringClass().getMethod(
							Accessor.SET.getAccessorName(fieldName), f.getType())))
					.map(accessors -> {
						// Check if both methods are set
						accessors.get(0).toString();
						accessors.get(1).toString();
						return accessors;
					})
					.map(accessors -> new DefaultProperty<T, F>(classRef, accessors.get(0), accessors.get(1), f,
							fieldName, false));
		}
	}

	/**
	 * Returns a list of properties for the given object
	 *
	 * @param object The object to analyze
	 * @param <T>    The type of the object
	 * @return A List containing all properties for the given object
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<Property<T, ?>> getProperties(@Nonnull T object) {
		return getProperties((Class<T>) object.getClass());
	}

	/**
	 * Returns a list of properties for the given class
	 *
	 * @param classRef The class to check for properties
	 * @param <T>      The type of the class
	 * @return A List containing all properties for the given class
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<Property<T, ?>> getProperties(Class<T> classRef) {
		List<Property<T, ?>> properties = new LinkedList<>();
		List<Class<? super T>> refs = getSuperClasses(classRef, true);

		for (Class<? super T> ref : refs) {
			for (Field f : ref.getDeclaredFields()) {
				if (!Modifier.isStatic(f.getModifiers())) {
					getProperty(classRef, f).allResults().forEach(p -> properties.add(p));
				}

			}
		}

		return properties;
	}

	/**
	 * Determine all superclasses for the given class, not including the class itself, and not including java.lang
	 * .Object
	 *
	 * @param classRef The class to analyze
	 * @param <T>      The type of the class
	 * @return A list of all classes that are superclasses of the current class
	 */
	public static <T> List<Class<? super T>> getSuperClasses(Class<T> classRef) {
		return getSuperClasses(classRef, false);
	}

	/**
	 * Determine all superclasses for the given class, not including the class itself, and not including java.lang
	 * .Object
	 *
	 * @param classRef       The class to analyze
	 * @param includeCurrent Whether or not to include the current class in this list
	 * @param <T>            The type of the class
	 * @return A list of all classes that are superclasses of the current class
	 */
	public static <T> List<Class<? super T>> getSuperClasses(Class<T> classRef,
															 boolean includeCurrent) {
		List<Class<? super T>> results = new LinkedList<Class<? super T>>();
		Class<? super T> next = classRef;
		if (!includeCurrent) {
			next = next.getSuperclass();
		}
		while (next != Object.class) {
			results.add(classRef);
			next = next.getSuperclass();
		}

		return results;
	}

	/**
	 * Attempt to instantiate the given class with the given parameters
	 *
	 * @param classRef The class to instantiate
	 * @param params   The parameters to pass to the constructor
	 * @param <T>      The type of the class instance
	 * @return A TypedResult either containing the instantiated object, or a message explaining why it can't be done
	 */
	public static <T> TypedResult<T> instance(Class<T> classRef, Object... params) {
		Class<?>[] paramTypes = new Class<?>[params.length];
		int i = 0;
		for (Object param : params) {
			paramTypes[i++] = param != null ? param.getClass() : null;
		}

		return TypedResult.attempt(() -> {
			Constructor<T> c = classRef.getConstructor(paramTypes);
			return c.newInstance(params);
		});
	}

	private static class DefaultProperty<T, F> implements Property<T, F> {
		private static final long serialVersionUID = 1L;

		private transient Method getter;
		private transient Method setter;
		private transient Field field;
		private String fieldName;
		private final boolean isFinal;
		private transient Class<T> owningClass;
		private transient Class<F> propertyType;

		private String fqdn;

		@SuppressWarnings("unchecked")
		DefaultProperty(Class<T> owningClass, Method getter,
						Method setter, Field field, String name, boolean isFinal) {
			super();
			this.getter = getter;
			this.setter = setter;
			this.field = field;
			this.propertyType = (Class<F>) field.getType();
			this.fieldName = name;
			this.owningClass = owningClass;
			this.fqdn = owningClass.getName();
			this.isFinal = isFinal;
		}

		@Nonnull
		@Override
		public Class<T> getOwningClass() {
			if (owningClass == null) {
				attach();
			}

			return owningClass;
		}

		@Nonnull
		@Override
		public Class<F> getPropertyType() {
			return propertyType;
		}

		@SuppressWarnings("unchecked")
		private void attach() {
			TypedResult<Class<T>> result = Reflector.getClass(fqdn);
			TypedResult<Property<T, F>> copy = result.flatMap(clazz -> Reflector.getProperty(clazz, fieldName));

			if (!copy.isOk()) {
				throw new IllegalStateException(String.format("Can not attach property: %s", copy.getMessage()));
			}

			Property<T, F> property = copy.getObject();

			this.owningClass = result.getObject();
			this.getter = property.getter();
			this.setter = property.setter();
			this.field = property.field();
			this.propertyType = (Class<F>) field.getType();
		}

		@Override
		public Method getter() {
			if (getter == null) { attach(); }

			return getter;
		}

		@Override
		public Method setter() {
			if (setter == null && !isFinal) { attach(); }

			return setter;
		}

		@Nonnull
		@Override
		public Field field() {
			if (field == null) { attach(); }

			return field;
		}

		@Override
		public String name() {
			return fieldName;
		}

		@Override
		@SuppressWarnings("unchecked")
		public F get(T target) {
			try {
				if (getter == null) { attach(); }

				return (F) getter.invoke(target);
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
				throw new IllegalStateException("Could not invoke getter on property "
						+ fieldName + " of class " + fqdn, e);
			}
		}

		@Override
		public void set(T target, F value) {
			try {
				if (!isFinal) {
					if (setter == null) { attach(); }

					setter.invoke(target, value);
				} else {
					throw new IllegalStateException("Tried to invoke setter on final property "
							+ fieldName + " of class " + fqdn);
				}
			} catch (IllegalAccessException | InvocationTargetException e) {
				throw new IllegalStateException("Could not invoke setter on property "
						+ fieldName + " of class " + fqdn, e);
			}
		}

		@Override
		public boolean isFinal() {
			return isFinal;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fieldName == null) ? 0 : fieldName.hashCode());
			result = prime * result + ((fqdn == null) ? 0 : fqdn.hashCode());
			result = prime * result + (isFinal ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) { return true; }
			if (obj == null) { return false; }
			if (getClass() != obj.getClass()) { return false; }
			DefaultProperty<?, ?> other = (DefaultProperty<?, ?>) obj;
			if (fieldName == null) {
				if (other.fieldName != null) { return false; }
			} else if (!fieldName.equals(other.fieldName)) { return false; }
			if (fqdn == null) {
				if (other.fqdn != null) { return false; }
			} else if (!fqdn.equals(other.fqdn)) { return false; }
			if (isFinal != other.isFinal) { return false; }
			return true;
		}

	}
}
