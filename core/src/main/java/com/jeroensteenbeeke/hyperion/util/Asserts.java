/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import java.util.function.Predicate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Convenience class for creating assert statements for validating variables and parameters
 */
public class Asserts {
	private Asserts() {

	}

	/**
	 * Builder for assertions on objects
	 */
	public static class AssertObjectBuilder {
		private final String type;

		private final String parameterName;

		private AssertObjectBuilder(String type, String parameterName) {
			this.type = type;
			this.parameterName = parameterName;
		}

		/**
		 * Assert that the given value is not null
		 * @param param The value to check
		 * @param <T> The type of the value
		 * @return The value, if it is not null
		 * @throws IllegalArgumentException if the value is null
		 */
		@Nonnull
		public <T> T notNull(@Nullable T param) {
			if (param == null) {
				throw new IllegalArgumentException(String.format(
						"%s %s should not be null", type, parameterName));
			}

			return param;
		}
	}

	/**
	 * Builder for assertions on numbers
	 */
	public static class AssertNumberBuilder {
		private final String type;

		private final String parameterName;

		private AssertNumberBuilder(String type, String parameterName) {
			this.type = type;
			this.parameterName = parameterName;
		}

		/**
		 * Specifies the value of the number
		 * @param value The value of the number, may not be null
		 * @param <T> The type of the number
		 * @return A Builder to finish the assert statement
		 */
		public <T extends Number & Comparable<T>> AssertNumberBuilderPhase2<T> withValue(
				@Nonnull T value) {
			// Trigger exception if value is null
			value.toString();

			return new AssertNumberBuilderPhase2<T>(type, parameterName, value);
		}
	}

	/**
	 * Builder class for determining the constraint the number should pass
	 * @param <T> The type of the number
	 */
	public static class AssertNumberBuilderPhase2<T extends Number & Comparable<T>> {
		private final String type;

		private final String parameterName;

		private final T value;

		private AssertNumberBuilderPhase2(@Nonnull String type,
										  @Nonnull String parameterName, @Nonnull T value) {
			this.type = type;
			this.parameterName = parameterName;
			this.value = value;
		}

		/**
		 * Assert that the number is at least the given value
		 * @param min The minimum value the number may be
		 * @return The number
		 * @throws IllegalArgumentException If the number is less than the minimum, or if an invalid minimum is given
		 */
		public T atLeast(@Nonnull T min) {
			if (min.compareTo(value) > 0) {
				throw new IllegalArgumentException(
						String.format("%s %s is %s, but must be at least %s",
								type, parameterName, value, min));
			}

			return value;
		}

		/**
		 * Assert that the number is at most the given value
		 * @param max The maximum value the number may be
		 * @return The number
		 * @throws IllegalArgumentException If the number is more than the maximum, or if an invalid maximum is given
		 */
		public T atMost(@Nonnull T max) {
			if (max.compareTo(value) < 0) {
				throw new IllegalArgumentException(
						String.format("%s %s is %s, but must be at most %s",
								type, parameterName, value, max));
			}

			return value;
		}

		/**
		 * Asserts that the number is within the given range
		 * @param min The minimum value the number may have
		 * @param max The maximum value the number may have
		 * @return The number
		 * @throws IllegalArgumentException If the number is outside the range, or if an invalid range is given
		 */
		public T between(@Nonnull T min, @Nonnull T max) {
			if (min.compareTo(max) > 0) {
				throw new IllegalArgumentException(
						"Maximum argument must be equal to or greater to minimum argument");
			}

			if (max.compareTo(value) < 0 || min.compareTo(value) > 0) {
				throw new IllegalArgumentException(String.format(
						"%s %s is %s, but must be between %s and %s", type,
						parameterName, value, min, max));
			}

			return value;
		}

		/**
		 * Asserts that the given number satisfies the given predicate
		 * @param predicate The predicate to test the number against
		 * @return The number
		 * @throws IllegalArgumentException If the number does not satisfy the predicate
		 */
		@Nonnull
		public T satisfies(@Nonnull Predicate<T> predicate) {
			if (!predicate.test(value)) {
				throw new IllegalArgumentException(String.format(
						"Invalid value for %s: %s", parameterName, value));
			}

			return value;
		}
	}

	/**
	 * Start creating an assertion based on an object parameter
	 * @param parameter The name of the parameter, used for feedback messages
	 * @return A builder to continue building the assertion
	 */
	public static AssertObjectBuilder objectParam(@Nonnull String parameter) {
		// Trigger exception if parameter is null
		parameter.intern();

		return new AssertObjectBuilder("Parameter", parameter);
	}

	/**
	 * Start creating an assertion based on a number parameter
	 * @param parameter The name of the parameter, used for feedback messages
	 * @return A builder to continue building the assertion
	 */
	public static AssertNumberBuilder numberParam(@Nonnull String parameter) {
		// Trigger exception if parameter is null
		parameter.intern();

		return new AssertNumberBuilder("Parameter", parameter);
	}

	/**
	 * Start creating an assertion based on an object variable
	 * @param variable The name of the variable
	 * @return A builder to continue building the assertion
	 */
	public static AssertObjectBuilder objectVariable(@Nonnull String variable) {
		// Trigger exception if parameter is null
		variable.intern();

		return new AssertObjectBuilder("Variable", variable);
	}

	/**
	 * Start creating an assertion based on a number variable
	 * @param variable The name of the variable
	 * @return A builder to continue building the assertion
	 */
	public static AssertNumberBuilder numberVariable(@Nonnull String variable) {
		// Trigger exception if parameter is null
		variable.intern();

		return new AssertNumberBuilder("Variable", variable);
	}
}
