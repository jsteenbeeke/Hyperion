/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import com.google.common.io.Files;
import com.jeroensteenbeeke.lux.TypedResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileCacheImageInputStream;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Collections;
import java.util.Iterator;

/**
 * Utility class for common image-based operations
 */
public final class ImageUtil {
	private static final Logger log = LoggerFactory.getLogger(ImageUtil.class);

	private ImageUtil() {

	}

	/**
	 * Resizes the given image to the target width and height, maintaining aspect ratio
	 * (downsizing the target size to
	 * match the aspect ratio)
	 *
	 * @param image        The image to resize
	 * @param targetWidth  The desired width
	 * @param targetHeight The desired height
	 * @return A TypedResult, either containing the resized image or a message explaining why it
	 * could not be resized
	 */
	public static TypedResult<byte[]> resize(byte[] image, int targetWidth,
											 int targetHeight) {
		return TypedResult.attempt(() ->
				ImageIO.read(new ByteArrayInputStream(image))
		).map(sourceImage -> {
			int thumbWidth = targetWidth;
			int thumbHeight = targetHeight;

			// Make sure the aspect ratio is maintained, so the image is not
			// skewed
			double thumbRatio = (double) thumbWidth / (double) thumbHeight;
			int imageWidth = sourceImage.getWidth(null);
			int imageHeight = sourceImage.getHeight(null);

			double imageRatio = (double) imageWidth / (double) imageHeight;
			if (thumbRatio < imageRatio) {
				thumbHeight = (int) (thumbWidth / imageRatio);
			} else {
				thumbWidth = (int) (thumbHeight * imageRatio);
			}

			BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight,
					BufferedImage.TYPE_INT_RGB);
			Graphics2D graphics2D = thumbImage.createGraphics();

			AffineTransform at = AffineTransform.getScaleInstance(
					(double) thumbWidth / sourceImage.getWidth(null),
					(double) thumbHeight / sourceImage.getHeight(null));
			graphics2D.drawRenderedImage(sourceImage, at);

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			ImageIO.write(thumbImage, getBlobType(image), out);

			out.flush();

			return out.toByteArray();
		});
	}

	/**
	 * Determines if the given image is a valid web image (i.e. a valid GIF, JPEG or PNG image)
	 *
	 * @param image The image to check
	 * @return {@code} true if the image is a valid web image, {@code false} otherwise
	 */
	public static boolean isWebImage(byte[] image) {
		return isGIFImage(image) || isJPEGImage(image) || isPNGImage(image);
	}

	/**
	 * Checks if an image is a GIF file
	 *
	 * @param image The image to check
	 * @return {@code true} if we suspect this file is a GIF file, {@code false}
	 * otherwise
	 */
	public static boolean isGIFImage(byte[] image) {
		return image.length > 6 && image[0] == (byte) 0x47
				&& image[1] == (byte) 0x49 && image[2] == (byte) 0x46
				&& image[3] == (byte) 0x38
				&& (image[4] == (byte) 0x39 || image[4] == (byte) 0x37)
				&& image[5] == (byte) 0x61;
	}

	/**
	 * Checks if an image is a JPEG file
	 *
	 * @param image The image to check
	 * @return {@code true} if we suspect this file is a JPEG file, {@code false}
	 * otherwise
	 */
	public static boolean isJPEGImage(byte[] image) {
		return image.length > 2 && image[0] == (byte) 0xFF
				&& image[1] == (byte) 0xD8;
	}

	/**
	 * Checks if an image is a PNG file
	 *
	 * @param image The image to check
	 * @return {@code true} if we suspect this file is a PNG file, {@code false}
	 * otherwise
	 */
	public static boolean isPNGImage(byte[] image) {
		return image.length > 8 && image[0] == -119 && image[1] == 80
				&& image[2] == 78 && image[3] == 71 && image[4] == 13
				&& image[5] == 10 && image[6] == 26 && image[7] == 10;
	}

	/**
	 * Determines the image dimensions of the given stream
	 *
	 * @param stream The stream to determine the image dimensions of
	 * @return A TypedResult either containing the dimensions of the image, or the reasons for
	 * failure
	 */
	public static TypedResult<Dimension> getImageDimensions(
			@Nonnull
					InputStream stream) {
		return TypedResult.ok(stream).flatMap(in -> {
			try (PushbackInputStream pb = new PushbackInputStream(in, 9)) {
				byte[] header = new byte[9];
				if (pb.read(header) < 9) {
					return TypedResult.fail("Could not read image header from stream");
				}
				pb.unread(header);

				return getImageDimensions(() -> header, () -> pb);
			}
		});
	}

	/**
	 * Determines the image dimensions of the given file, without loading the entire
	 * file into memory
	 *
	 * @param file The file to determine the image dimensions of
	 * @return A TypedResult either containing the dimensions of the image, or the
	 * reasons for failure
	 */
	public static TypedResult<Dimension> getImageDimensions(
			@Nonnull
					File file) {
		return getImageDimensions(() -> extractImageHeader(file),
				() -> new FileInputStream(file));
	}

	private static TypedResult<Dimension> getImageDimensions(
			@Nonnull
					ExceptionalSupplier<byte[], IOException> headerSupplier,
			@Nonnull
					ExceptionalSupplier<InputStream, IOException> streamSupplier) {
		Iterator<ImageReader> iter = Collections.emptyIterator();

		try {
			byte[] header = headerSupplier.get();

			if (isPNGImage(header)) {
				iter = ImageIO.getImageReadersBySuffix("png");
			} else if (isGIFImage(header)) {
				iter = ImageIO.getImageReadersBySuffix("gif");
			} else if (isJPEGImage(header)) {
				iter = ImageIO.getImageReadersBySuffix("jpg");
			}

			if (iter.hasNext()) {
				ImageReader reader = iter.next();
				File tmp = null;

				try {
					tmp = Files.createTempDir();
					ImageInputStream stream =
							new FileCacheImageInputStream(streamSupplier.get(), tmp);
					reader.setInput(stream);
					int width = reader.getWidth(reader.getMinIndex());
					int height = reader.getHeight(reader.getMinIndex());
					return TypedResult.ok(new Dimension(width, height));

				} finally {
					reader.dispose();
					if (tmp != null) {
						FileUtil.deleteTree(tmp);
					}
				}
			} else {
				return TypedResult.fail("No reader found for image");
			}
		} catch (IOException e) {
			return TypedResult.fail(e.getMessage());
		}
	}

	private static byte[] extractImageHeader(
			@Nonnull
					File image) throws IOException {
		try (FileInputStream fis = new FileInputStream(image)) {
			byte[] header = new byte[9];
			if (fis.read(header) != 9) {
				throw new IOException("Failed to read image header");
			}

			return header;
		}
	}

	/**
	 * Determines the dimensions of the given image
	 *
	 * @param image The image to check
	 * @return A TypedResult either containing the dimensions of the image, or an explanation
	 * as to why they could
	 * not
	 * be determined
	 * @deprecated This method loads the entire image into memory, please consider using
	 * {@link #getImageDimensions(File)}
	 */
	@Deprecated
	public static TypedResult<Dimension> getImageDimensions(byte[] image) {
		if (isGIFImage(image) || isJPEGImage(image) || isPNGImage(image)) {

			return TypedResult.attempt(() -> {
				Image img = ImageIO.read(new ByteArrayInputStream(image));

				return new Dimension(img.getWidth(null), img.getHeight(null));
			});

		}
		return TypedResult.fail("Given array is not a recognized image format");
	}

	/**
	 * Determines the blob type of the given image, which is required for some image operations
	 *
	 * @param image The image to analyze
	 * @return The blob type (jpg, png, gif or unknown)
	 */

	public static String getBlobType(byte[] image) {
		if (isJPEGImage(image)) {
			return "jpg";
		} else if (isPNGImage(image)) {
			return "png";
		} else if (isGIFImage(image)) {
			return "gif";
		}

		return "unknown";
	}

	/**
	 * Returns the image type in a format that Apache Wicket understands (used for ImageResources)
	 *
	 * @param image The image to check
	 * @return The image type (jpeg, png, gif or unknown)
	 */
	public static String getWicketFormatType(byte[] image) {
		if (isJPEGImage(image)) {
			return "jpeg";
		} else if (isPNGImage(image)) {
			return "png";
		} else if (isGIFImage(image)) {
			return "gif";
		}

		return "unknown";
	}

	/**
	 * Returns the MIME type of the given image
	 *
	 * @param image The image to analyze
	 * @return The MIME type (image/jpeg, image/png, image/gif or application/octet-stream if
	 * unrecognized)
	 */
	public static String getMimeType(byte[] image) {
		if (isJPEGImage(image)) {
			return "image/jpeg";
		} else if (isPNGImage(image)) {
			return "image/png";
		} else if (isGIFImage(image)) {
			return "image/gif";
		}

		return "application/octet-stream";
	}

	/**
	 * Returns the MIME type of the given file
	 *
	 * @param file The file to analyze
	 * @return The MIME type (image/jpeg, image/png, image/gif or application/octet-stream if
	 * unrecognized)
	 */
	public static TypedResult<String> getMimeType(File file) {
		return TypedResult.attempt(() -> {
			byte[] header = extractImageHeader(file);

			if (isJPEGImage(header)) {
				return "image/jpeg";
			} else if (isPNGImage(header)) {
				return "image/png";
			} else if (isGIFImage(header)) {
				return "image/gif";
			}

			return "application/octet-stream";
		});
	}

	/**
	 * Determines the file extension of an image based on the given MIME type
	 *
	 * @param mimeType The MIME type to check
	 * @return The extension belonging to the given MIME type (.jpg, .png, .gif, or .bin if
	 * unrecognized)
	 */
	public static String getExtensionByMimeType(String mimeType) {
		if ("image/jpeg".equals(mimeType)) {
			return ".jpg";
		} else if ("image/png".equals(mimeType)) {
			return ".png";
		} else if ("image/gif".equals(mimeType)) {
			return ".gif";
		}

		return ".bin";
	}

	@FunctionalInterface
	private interface ExceptionalSupplier<T, E extends Throwable> {
		T get() throws E;
	}
}
