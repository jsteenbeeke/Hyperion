/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import java.security.MessageDigest;

import com.jeroensteenbeeke.lux.TypedResult;

/**
 * Utility class for quickly creating commonly used hashes in String format
 */
public final class HashUtil {
	private HashUtil() {

	}

	/**
	 * Calculate the sha512 hash of the given input
	 *
	 * @param input The input to hash
	 * @return The sha512 hash, as a hexadecimal String
	 */
	public static TypedResult<String> sha512Hash(String input) {
		return TypedResult.attempt(() -> {
			MessageDigest sha512Digest = MessageDigest.getInstance("SHA-512");
			sha512Digest.update(input.getBytes());
			return byteArrayToString(sha512Digest.digest());
		});
	}

	/**
	 * Calculate the sha256 hash of the given input
	 *
	 * @param input The input to hash
	 * @return The sha256 hash, as a hexadecimal String
	 */
	public static TypedResult<String> sha256Hash(String input) {
		return TypedResult.attempt(() -> {
			MessageDigest sha256Digest = MessageDigest.getInstance("SHA-256");
			sha256Digest.update(input.getBytes());
			return byteArrayToString(sha256Digest.digest());
		});

	}

	/**
	 * Calculate the sha1 hash of the given input
	 *
	 * @param input The input to hash
	 * @return The sha1 hash, as a hexadecimal String
	 * @deprecated SHA-1 is considered too weak for most uses. Use SHA-256 or higher instead unless you really have no
	 * choice
	 */
	@Deprecated
	public static TypedResult<String> sha1Hash(String input) {
		return TypedResult.attempt(() -> {
			MessageDigest sha1Digest = MessageDigest.getInstance("SHA-1");
			sha1Digest.update(input.getBytes());
			return byteArrayToString(sha1Digest.digest());
		});

	}

	/**
	 * Calculate the md5 hash of the given input
	 *
	 * @param input The input to hash
	 * @return The md5 hash, as a hexadecimal String
	 * @deprecated MD5 is considered completely broken for most intents and purposes. You should not be using this
	 * method,
	 * and whatever your reasons for it are, they are wrong. Why the hell is this method still here? To educate you!
	 * Don't be a moron, don't use MD5
	 */
	@Deprecated
	public static TypedResult<String> md5Hash(String input) {
		return TypedResult.attempt(() -> {
			MessageDigest md5Digest = MessageDigest.getInstance("MD5");
			md5Digest.update(input.getBytes());
			return byteArrayToString(md5Digest.digest());
		});
	}

	/**
	 * Converts the given byte-array to a hexadecimal representation
	 *
	 * @param array The array to convert
	 * @return A hexadecimal representation of the array
	 */
	public static String byteArrayToString(byte[] array) {
		StringBuilder builder = new StringBuilder();

		for (byte next : array) {
			byte lower = (byte) (next & 0x0F);

			byte higher = (byte) ((next & 0xF0) >>> 4);

			builder.append(getHexChar(higher));
			builder.append(getHexChar(lower));
		}

		return builder.toString();
	}

	/**
	 * Converts the given byte to a hexadecimal character. Due to the return type only being a single character, the
	 * maximum
	 * value of the byte is equal to 15 (or 0x0F, if you prefer hexadecimal). Any higher values require more than 1
	 * byte.
	 *
	 * @param convertible The byte to convert. Should be at least 0 and at most 15.
	 * @return A hexadecimal representation of the given byte.
	 * @see HashUtil#byteArrayToString(byte[])
	 */
	static char getHexChar(byte convertible) {
		if (convertible > 15 || convertible < 0) {
			throw new IllegalArgumentException(
					"Byte value " + convertible + " cannot be expressed as a single character");
		}
		switch (convertible) {
			case 10:
				return 'a';
			case 11:
				return 'b';
			case 12:
				return 'c';
			case 13:
				return 'd';
			case 14:
				return 'e';
			case 15:
				return 'f';
			default:
				return Integer.toString(convertible).charAt(0);
		}
	}

}
