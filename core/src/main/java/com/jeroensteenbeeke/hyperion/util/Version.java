/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * Representation of a version that can easily be compared
 */
public class Version implements Comparable<Version> {
	private final String original;

	private final List<VersionPart> parts;

	private Version(String original, List<VersionPart> parts) {
		super();
		this.original = original;
		this.parts = parts;
	}

	/**
	 * Create a version of the given String representation
	 *
	 * @param representation The representation of the version. Any non-alphanumeric characters will be used as
	 *                       separators for comparison purposes
	 * @return A Version object that represents the given representation
	 */
	public static Version of(String representation) {
		List<String> p = StringUtil.split(representation, c -> !Character.isLetterOrDigit(c));

		List<VersionPart> parts = Lists.newArrayListWithExpectedSize(p.size());

		for (String segment : p) {
			for (VersionPartType type : VersionPartType.values()) {
				if (segment.matches(type.getRegex())) {
					parts.add(new VersionPart(type, segment));
					break;
				}
			}
		}

		return new Version(representation, parts);

	}

	@Override
	public String toString() {
		return original;
	}

	@Override
	public int compareTo(Version o) {
		List<VersionPart> compareA = Lists.newArrayList(parts);
		List<VersionPart> compareB = Lists.newArrayList(o.parts);

		while (compareA.size() < compareB.size()) {
			compareA.add(new VersionPart(VersionPartType.STANDARD, "0"));
		}

		while (compareB.size() < compareA.size()) {
			compareB.add(new VersionPart(VersionPartType.STANDARD, "0"));
		}

		for (int i = 0; i < compareA.size(); i++) {
			int comp = compareA.get(i).compareTo(compareB.get(i));
			if (comp != 0) {
				return comp;
			}
		}

		return 0;
	}

	private static class VersionPart implements Comparable<VersionPart> {
		private final VersionPartType type;

		private final String representation;

		private VersionPart(VersionPartType type, String representation) {
			super();
			this.type = type;
			this.representation = representation;
		}

		@Override
		public int compareTo(VersionPart o) {
			int typeEquivalence = o.type.compareTo(type);

			if (typeEquivalence == 0) {
				return type.compare(representation, o.representation);
			}

			return typeEquivalence;
		}

	}

	/**
	 * Definition of different types of version parts
	 */
	public enum VersionPartType {
		/**
		 * Numerical representation, one or more digits
		 */
		STANDARD("\\d+") {
			@Override
			public int compare(String versionA, String versionB) {
				Integer a = Integer.parseInt(versionA);
				Integer b = Integer.parseInt(versionB);

				return Integer.compare(a, b);
			}
		},
		/**
		 * Release candidates, one or more digits prefixed with the case-insensitive letters RC
		 */
		RELEASE_CANDIDATE("(r|R)(c|C)\\d+") {
			@Override
			public int compare(String versionA, String versionB) {
				Integer a = Integer.parseInt(versionA.substring(2));
				Integer b = Integer.parseInt(versionB.substring(2));

				return Integer.compare(a, b);
			}
		},
		/**
		 * Milestone releases, one or more digits prefixed with the case-insensitive letter M
		 */
		MILESTONE("(m|M)\\d+") {
			@Override
			public int compare(String versionA, String versionB) {
				Integer a = Integer.parseInt(versionA.substring(1));
				Integer b = Integer.parseInt(versionB.substring(1));

				return Integer.compare(a, b);
			}
		},
		/**
		 * Testing releases, zero or more digits suffixed with an alphabetic indicator
		 */
		TESTING("\\d*[a-zA-Z]+") {
			@Override
			public int compare(String versionA, String versionB) {
				Integer a = extractNumeric(versionA);
				Integer b = extractNumeric(versionB);

				int compare = Integer.compare(a, b);
				if (compare == 0) {
					return versionA.compareTo(versionB);
				}

				return compare;

			}

			private Integer extractNumeric(String version) {
				StringBuilder buffer = new StringBuilder();
				for (char c : version.toCharArray()) {
					if (!Character.isDigit(c)) {
						break;
					}
					buffer.append(c);
				}

				if (buffer.length() > 0) {
					return Integer.parseInt(buffer.toString());
				}

				return 0;
			}
		},
		/**
		 * String representation. Anythign that does not match the other types
		 */
		STRING(".*") {
			@Override
			public int compare(String versionA, String versionB) {
				return versionA.compareTo(versionB);
			}
		};

		private final String regex;

		VersionPartType(String regex) {
			this.regex = regex;
		}

		/**
		 * Returns the regular expression that can be used to match this version part type
		 *
		 * @return A Java regular expression
		 */
		public String getRegex() {
			return regex;
		}

		/**
		 * Compares two version String according to the pattern and rules for this type of part
		 *
		 * @param versionA The first version to compare
		 * @param versionB The second version to compare
		 * @return 0, if both versions are equal. A negative integer if {@code versionB > versionA} and a positive
		 * integer if {@code versionA > versionB}
		 */
		public abstract int compare(String versionA, String versionB);
	}
}
