/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

/**
 * Utility class for time-related operations
 */
public final class TimeUtil {
	private static final DateTimeFormatter FORMATTER_YEAR_MONTH_DAY = new DateTimeFormatterBuilder()
			.appendYear(4, 4).appendLiteral('-').appendMonthOfYear(2)
			.appendLiteral('-').appendDayOfMonth(2).toFormatter();

	private static final DateTimeFormatter FORMATTER_YEAR_MONTH_DAY_HOUR_MINUTE = new DateTimeFormatterBuilder()
			.appendYear(4, 4).appendLiteral('-').appendMonthOfYear(2)
			.appendLiteral('-').appendDayOfMonth(2).appendLiteral(' ').appendHourOfDay(2).appendLiteral(':')
			.appendMinuteOfHour(2).toFormatter();

	private TimeUtil() {

	}

	/**
	 * Returns a Multimap with all dates in the given date's month grouped by week
	 *
	 * @param referenceDate The date to use as a basis
	 * @return A Multimap that has the week number as key, and the days of that week as values (inserted in
	 * chronological order)
	 */
	@Nonnull
	public static Multimap<Integer, LocalDate> getMonthPerWeek(
			@Nonnull LocalDate referenceDate) {
		LocalDate current = referenceDate.withDayOfMonth(1);

		final int month = current.getMonthOfYear();

		final Multimap<Integer, LocalDate> weekToDays = LinkedListMultimap
				.create();

		while (current.getMonthOfYear() == month) {
			weekToDays.put(current.getWeekOfWeekyear(), current);

			current = current.plusDays(1);
		}
		return weekToDays;
	}

	/**
	 * Formats the given date in yyyy-MM-dd format
	 *
	 * @param date The date to format
	 * @return An optional that contains the given date
	 */
	@Nonnull
	public static Optional<String> formatYearMonthDay(@Nullable DateTime date) {
		return Optional.ofNullable(date).map(FORMATTER_YEAR_MONTH_DAY::print);
	}

	/**
	 * Formats the given date in yyyy-MM-dd hh:mm format
	 *
	 * @param date The date to format
	 * @return An optional that contains the given date
	 */
	@Nonnull
	public static Optional<String> formatYearMonthDayHourMinute(@Nullable DateTime date) {
		return Optional.ofNullable(date).map(FORMATTER_YEAR_MONTH_DAY_HOUR_MINUTE::print);

	}

}
