package com.jeroensteenbeeke.hyperion.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Class for creating randomness
 */
public final class Randomizer {
	private static final SecureRandom random = initRandom();
	
	private static final String AVAILABLE_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	private Randomizer() {
		
	}
	
	private static SecureRandom initRandom() {
		try {
			return SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			// If SHA1PRNG does not exist, then go with default implementation
			return new SecureRandom();
		}
	}

	/**
	 * Create a string of random alphanumerical characters, with the given length
	 * @param length The target length
	 * @return A String of the given length containing random alphanumerical characters
	 */
	public static String random(int length) {
		StringBuilder password = new StringBuilder();

		for (int i = 0; i < length; i++) {
			password.append(randomCharacter());
		}

		return password.toString();
	}
	
	private static char randomCharacter() {
		return AVAILABLE_CHARS.charAt(random.nextInt(AVAILABLE_CHARS.length()));
	}

	/**
	 * Create a byte array of the given length with random data
	 * @param count The length of the array to create
	 * @return An array of random bytes, with size equal to count
	 */
	public static byte[] randomBytes(int count) {
		Asserts.numberParam("count").withValue(count).atLeast(0);

		byte[] result = new byte[count];
		
		random.nextBytes(result);
		
		return result;
	}
}
