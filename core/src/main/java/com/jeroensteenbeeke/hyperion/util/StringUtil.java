package com.jeroensteenbeeke.hyperion.util;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.function.Predicate;

/**
 * String-based operations
 */
public final class StringUtil {
	private StringUtil() {

	}

	/**
	 * Converts the first character of the given String to uppercase
	 *
	 * @param input The string to process
	 * @return A new String which is identical to the input String, except for the first character, which is converted
	 * to uppercase
	 */
	public static String capitalizeFirst(String input) {
		if (input.length() == 0) {
			return input;
		}
		return input.substring(0, 1).toUpperCase().concat(input.substring(1));
	}

	/**
	 * Converts the first character of the given String to lowercase
	 *
	 * @param input The string to process
	 * @return A new String which is identical to the input String, except for the first character, which is converted
	 * to lowercase
	 */
	public static String lowercaseFirst(String input) {
		if (input.length() == 0) {
			return input;
		}
		return input.substring(0, 1).toLowerCase().concat(input.substring(1));
	}

	/**
	 * Truncates the given String, limiting in length to a given amount of character
	 *
	 * @param input The input String to truncate
	 * @return A builder object that finalizes the truncate operation
	 */
	public static TruncateBuilder truncate(String input) {
		return new TruncateBuilder(input);
	}

	/**
	 * Builder class that asks for the maximum String length
	 */
	public static class TruncateBuilder {
		private final String input;

		private TruncateBuilder(String input) {
			this.input = input;
		}

		/**
		 * Performs the truncate, limiting the String to the given number of characters. If the String is already
		 * smaller than this number, the original String is returned
		 *
		 * @param characters The maximum number of characters the String may have. Must be 0 or greater
		 * @return The truncated String
		 * @throws IllegalArgumentException If {@code characters} is less than 0
		 */
		public String to(int characters) {
			int l = Asserts.numberParam("characters").withValue(characters).atLeast(0);

			if (input.length() <= l) {
				return input;
			}

			return input.substring(0, l);
		}
	}

	/**
	 * Splits a String according to a given character predicate. Works like String.split, but does not use regex
	 * @param input The input to split
	 * @param splitPredicate The predicate to test characters against
	 * @return A list of component String
	 */
	public static List<String> split(String input, Predicate<Character> splitPredicate) {
		if (input.length() == 0) {
			return ImmutableList.of(input);
		}

		ImmutableList.Builder<String> result = ImmutableList.builder();
		StringBuilder builder = new StringBuilder();

		for (char c: input.toCharArray()) {
			if (splitPredicate.test(c)) {
				result.add(builder.toString());
				builder = new StringBuilder();
			} else {
				builder.append(c);
			}
		}

		result.add(builder.toString());

		return result.build();
	}
}
