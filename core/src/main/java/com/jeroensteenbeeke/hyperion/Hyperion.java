package com.jeroensteenbeeke.hyperion;

import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Version information class
 */
public class Hyperion {
	private static final Logger log = LoggerFactory.getLogger(Hyperion.class);

	private static HyperionSettings settings;

	/**
	 * Returns the revision (git hash) of the current Hyperion version
	 *
	 * @return A git hash, if known, an empty Option otherwise
	 */
	public static Option<String> getRevision() {
		return getResourceAsString("revision.txt").filter(s -> !s.isEmpty());
	}

	/**
	 * Returns the commit title (the first line of the commit message) of the current Hyperion version
	 *
	 * @return The commit title, if known, an empty Option otherwise
	 */
	public static Option<String> getCommitTitle() {
		return getResourceAsString("commit-title.txt").filter(s -> !s.isEmpty());
	}

	/**
	 * Returns the commit notes (everything from the second line of the commit message) of the current Hyperion version
	 *
	 * @return The commit notes, if known, an empty Option otherwise
	 */
	public static Option<String> getCommitNotes() {
		return getResourceAsString("commit-notes.txt").filter(s -> !s.isEmpty());
	}

	/**
	 * Gets user settings for use with Hyperion
	 *
	 * @return A HyperionSettings object
	 */
	public static synchronized HyperionSettings getSettings() {
		if (settings != null) {
			return settings;
		}
		Path userHome = Path.of(System.getProperty("user.home"));
		Path hyperionRc = userHome.resolve(".hyperionrc");
		if (Files.exists(hyperionRc)) {
			Properties props = new Properties();
			try (FileInputStream fis = new FileInputStream(hyperionRc.toFile())) {
				props.load(fis);

				return settings = new HyperionSettings(props.getProperty("user.display.name", System.getProperty("user.name", "unknown")));
			} catch (IOException e) {
				log.error("Could not open .hyperionrc, falling back to defaults");
			}
		}
		return settings = new HyperionSettings(System.getProperty("user.home", "unknown"));


	}

	private static Option<String> getResourceAsString(String filename) {
		try (InputStream stream = Hyperion.class.getResourceAsStream(filename); ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			int i;

			if (stream == null) {
				return Option.none();
			}

			while ((i = stream.read()) != -1) {
				bos.write(i);
			}

			return Option.of(new String(bos.toByteArray(), StandardCharsets.UTF_8).trim());
		} catch (IOException ioe) {
			return Option.none();
		}
	}
}
