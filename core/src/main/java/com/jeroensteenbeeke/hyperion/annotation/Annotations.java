/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.annotation;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;

import com.google.common.collect.Maps;

/**
 * Convenience class for working with annotations through reflection
 */
public class Annotations implements Serializable {
	private static final long serialVersionUID = 1L;

	private Map<Class<? extends Annotation>, Annotation> annotations;

	private Annotations() {
		annotations = Maps.newHashMap();
	}

	/**
	 * Checks if the given construct is annotated with the indicated class
	 * @param annotationClass The annotation to check for
	 * @return {@code true} if the annotation is present, {@code false otherwise}
	 */
	public boolean hasAnnotation(Class<? extends Annotation> annotationClass) {
		return annotations.containsKey(annotationClass);
	}

	/**
	 * Returns the construct's annotation of the given type, if it exists
	 * @param annotationClass The class to check for
	 * @param <T> The type of the annotation return
	 * @return An Optional that may contain the annotation of the given type
	 */
	@SuppressWarnings("unchecked")
	public <T extends Annotation> Optional<T> getAnnotation(Class<T> annotationClass) {
		return Optional.ofNullable((T) annotations.get(annotationClass));
	}

	/**
	 * Returns an annotations handle for the given field
	 * @param field The field to check for annotations
	 * @return An {@code Annotations} object
	 */
	public static Annotations fromField(Field field) {
		Annotations annotations = new Annotations();
		for (Annotation annotation : field.getAnnotations()) {
			annotations.annotations
					.put(annotation.annotationType(), annotation);
		}
		return annotations;
	}

	/**
	 * Returns an annotations handle for the given class
	 * @param clazz The class to check for annotations
	 * @return An {@code Annotations} object
	 */
	public static Annotations fromClass(Class<?> clazz) {
		Annotations annotations = new Annotations();
		for (Annotation annotation : clazz.getAnnotations()) {
			annotations.annotations
					.put(annotation.annotationType(), annotation);
		}
		return annotations;
	}

	/**
	 * Returns an annotations handle for the given method
	 * @param method The method to check for annotations
	 * @return An {@code Annotations} object
	 */
	public static Annotations fromMethod(Method method) {
		Annotations annotations = new Annotations();
		for (Annotation annotation : method.getAnnotations()) {
			annotations.annotations
					.put(annotation.annotationType(), annotation);
		}
		return annotations;
	}
}
