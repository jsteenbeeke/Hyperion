/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.collections;

import java.io.Serializable;

/**
 * A serializable 3-tuple
 * @param <T> The type of the first element of the tuple
 * @param <U> The type of the second element of the tuple
 * @param <V> The type the third element of the tuple
 */
public final class Triple<T extends Serializable, U extends Serializable, V extends Serializable>
		implements Serializable {

	private static final long serialVersionUID = 1L;

	private final T first;

	private final U second;

	private final V third;

	/**
	 * Create a new triple with the given values
	 * @param first The first value
	 * @param second The second value
	 * @param third The third value
	 */
	public Triple(T first, U second, V third) {
		super();
		this.first = first;
		this.second = second;
		this.third = third;
	}

	/**
	 * Get the first value of the tuple
	 * @return The first value
	 */
	public T getFirst() {
		return first;
	}

	/**
	 * Get the second value of the tuple
	 * @return The second value
	 */
	public U getSecond() {
		return second;
	}

	/**
	 * Get the third value of the tuple
	 * @return The third value
	 */
	public V getThird() {
		return third;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		result = prime * result + ((third == null) ? 0 : third.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		Triple<?, ?, ?> other = (Triple<?, ?, ?>) obj;
		if (first == null) {
			if (other.first != null) { return false; }
		} else if (!first.equals(other.first)) { return false; }
		if (second == null) {
			if (other.second != null) { return false; }
		} else if (!second.equals(other.second)) { return false; }
		if (third == null) {
			if (other.third != null) { return false; }
		} else if (!third.equals(other.third)) { return false; }
		return true;
	}

}
