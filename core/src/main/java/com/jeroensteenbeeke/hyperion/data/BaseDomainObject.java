/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.data;


/**
 * Base class for creating DomainObject classes. Has some basic
 * logic that works well with JPA-based solutions
 */
public abstract class BaseDomainObject implements DomainObject {
	private static final long serialVersionUID = 1L;

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
			* result
			+ ((getDomainObjectId() == null) ? 0 : getDomainObjectId()
			.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public final boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}

		if (obj instanceof DomainObject) {
			DomainObject domainObject = (DomainObject) obj;

			if (getEntityClass() != domainObject.getEntityClass()) {
				return false;
			}

			if (domainObject instanceof BaseDomainObject) {
				BaseDomainObject other = (BaseDomainObject) domainObject;

				if (getDomainObjectId() == null) {
					return other.getDomainObjectId() == null;
				} else return getDomainObjectId().equals(other.getDomainObjectId());
			}
		}

		return false;
	}
}
