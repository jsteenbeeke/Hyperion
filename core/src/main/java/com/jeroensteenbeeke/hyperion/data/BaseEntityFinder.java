/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.data;

import io.vavr.control.Option;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Optional;

/**
 * Base class for retrieving domain objects from a data repository
 * 
 * @author Jeroen Steenbeeke
 */
public interface BaseEntityFinder {
	/**
	 * Retrieves an entity of the given type from the repository
	 * @param entityClass The class of the entity to retrieve
	 * @param id The identifier of the entity
	 * @param <T> The type of the entity to return
	 * @return An optional which will contain the given entity if it exists
	 */
	@Nonnull
	<T extends DomainObject> Option<T> getEntity(@Nonnull Class<T> entityClass,
												 @Nonnull Serializable id);
}
