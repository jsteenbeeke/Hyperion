/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class MathUtilTest {
	@Test
	public void testGCD() {
		assertThat(MathUtil.greatestCommonDivisor(15, 20), equalTo(5));
		assertThat(MathUtil.greatestCommonDivisor(125, 20), equalTo(5));
		assertThat(MathUtil.greatestCommonDivisor(120, 20), equalTo(20));
		assertThat(MathUtil.greatestCommonDivisor(44, 20), equalTo(4));
		assertThat(MathUtil.greatestCommonDivisor(15L, 20L), equalTo(5L));
		assertThat(MathUtil.greatestCommonDivisor(125L, 20L), equalTo(5L));
		assertThat(MathUtil.greatestCommonDivisor(44L, 20L), equalTo(4L));
	}

	@Test
	public void testLCD() {
		assertThat(MathUtil.lowestCommonDenominator(2, 3), equalTo(6));
		assertThat(MathUtil.lowestCommonDenominator(2L, 3L), equalTo(6L));
		assertThat(MathUtil.lowestCommonDenominator(12, 18), equalTo(36));
		assertThat(MathUtil.lowestCommonDenominator(12L, 18L), equalTo(36L));
	}
}
