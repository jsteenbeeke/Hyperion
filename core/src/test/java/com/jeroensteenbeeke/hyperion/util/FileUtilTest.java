package com.jeroensteenbeeke.hyperion.util;

import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FileUtilTest {
	private File tempDir;

	@Before
	public void createTempDir() {
		tempDir = Files.createTempDir();
	}

	@After
	public void cleanupTempDir() {
		FileUtil.deleteTree(tempDir);
	}

	@Test
	public void testDeleteTree() throws IOException {
		assertEquals(0, tempDir.listFiles().length);

		File test = new File(tempDir,"test");
		assertFalse(test.exists());
		FileUtil.deleteTree(test);
		assertFalse(test.exists());
		assertEquals(0, tempDir.listFiles().length);

		assertTrue(test.mkdir());
		assertTrue(test.exists());
		FileUtil.deleteTree(test);
		assertFalse(test.exists());
		assertEquals(0, tempDir.listFiles().length);

		assertTrue(test.mkdir());
		assertTrue(test.exists());

		File l1f1 = new File(test,"1");
		assertTrue(l1f1.createNewFile());
		File l1f2 = new File(test,"2");
		assertTrue(l1f2.createNewFile());

		File l1d1 = new File(test, "dir");
		assertTrue(l1d1.mkdir());

		File l2f1 = new File(l1d1,"1");
		assertTrue(l2f1.createNewFile());
		File l2f2 = new File(l1d1,"2");
		assertTrue(l2f2.createNewFile());

		assertTrue(l2f2.exists());
		assertTrue(l2f1.exists());
		assertTrue(l1d1.isDirectory());
		assertTrue(l1f2.exists());
		assertTrue(l1f1.exists());
		assertTrue(test.isDirectory());
		FileUtil.deleteTree(test);
		assertFalse(l2f2.exists());
		assertFalse(l2f1.exists());
		assertFalse(l1d1.exists());
		assertFalse(l1f2.exists());
		assertFalse(l1f1.exists());
		assertFalse(test.exists());

	}

	@Test
	public void testSanitation() {
		assertEquals("test_input", FileUtil.makeFilenameSafe("test input"));
		assertEquals("test_input", FileUtil.makeFilenameSafe("test  input"));
		assertEquals("test_input", FileUtil.makeFilenameSafe("test   input"));
		assertEquals("test_input", FileUtil.makeFilenameSafe("test\tinput"));
		assertEquals("test_input", FileUtil.makeFilenameSafe("test\t\tinput"));
		assertEquals("lots_of_", FileUtil.makeFilenameSafe("LOTS OF $$$$$!"));
		assertEquals("i_am_the_α_and_the_ω", FileUtil.makeFilenameSafe("I am the α and the ω"));
		assertEquals("1337_h4xx0r", FileUtil.makeFilenameSafe("1337 h4xx0r!"));
		assertEquals("test_input_", FileUtil.makeFilenameSafe("test_____input____"));

	}
}
