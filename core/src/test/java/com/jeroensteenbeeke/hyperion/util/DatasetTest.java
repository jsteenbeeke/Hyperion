package com.jeroensteenbeeke.hyperion.util;

import com.jeroensteenbeeke.hyperion.annotation.Dataset;
import org.junit.Test;

import static org.junit.Assert.*;

@Dataset("test")
public class DatasetTest {
	@Test
	public void testExtraction() {
		assertTrue(Datasets.readFromClass(DatasetTest.class).isPresent());
		assertTrue(Datasets.readFromClass(DatasetTest.class).isPresent());
		assertTrue(Datasets.readFromClass(DatasetTest.class).filter("test"::equals).isPresent());
		assertFalse(Datasets.readFromClass(AssertsTest.class).isPresent());

		assertTrue(Datasets.readFromObject(this).isPresent());
		assertTrue(Datasets.readFromObject(this).isPresent());
		assertTrue(Datasets.readFromObject(this).filter("test"::equals).isPresent());
	}

	@Test
	public void testChain() {
		assertEquals(Datasets.DEFAULT, Datasets.INSTANCE.getActiveDataset());
		Datasets.INSTANCE.set("FooBar");
		assertEquals("FooBar", Datasets.INSTANCE.getActiveDataset());
		Datasets.INSTANCE.unset();
		assertEquals(Datasets.DEFAULT, Datasets.INSTANCE.getActiveDataset());
		Datasets.INSTANCE.unset();
		assertEquals(Datasets.DEFAULT, Datasets.INSTANCE.getActiveDataset());
		Datasets.INSTANCE.set(Datasets.DEFAULT);
		assertEquals(Datasets.DEFAULT, Datasets.INSTANCE.getActiveDataset());
		Datasets.INSTANCE.unset();
		Datasets.INSTANCE.unset();
		assertEquals(Datasets.DEFAULT, Datasets.INSTANCE.getActiveDataset());

	}
}
