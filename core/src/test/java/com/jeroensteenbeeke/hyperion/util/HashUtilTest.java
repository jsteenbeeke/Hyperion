/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import com.jeroensteenbeeke.lux.TypedResult;
import org.junit.Assert;
import org.junit.Test;

import java.util.function.Consumer;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class HashUtilTest {
	@Test
	@Deprecated
	public void hashAllTheThings() {
		final String input = "konijn";

		TypedResult<String> md5 = HashUtil.md5Hash(input);
		TypedResult<String> sha1 = HashUtil.sha1Hash(input);
		TypedResult<String> sha256 = HashUtil.sha256Hash(input);
		TypedResult<String> sha512 = HashUtil.sha512Hash(input);

		assertTrue(md5.isOk());
		assertTrue(sha1.isOk());
		assertTrue(sha256.isOk());
		assertTrue(sha512.isOk());

		assertNotNull(md5.getObject());
		assertNotNull(sha1.getObject());
		assertNotNull(sha256.getObject());
		assertNotNull(sha512.getObject());
	}

	@Test
	public void testInvalidByteConversion() {
		for (byte b = Byte.MIN_VALUE; b < 0; b++) {
			tryConversion(b, Assert::assertNotNull);

		}
		for (byte b = 0; b < 16; b++) {
			tryConversion(b, Assert::assertNull);
		}

		for (byte b = 16; b < Byte.MAX_VALUE; b++) {
			tryConversion(b, Assert::assertNotNull);
		}
		tryConversion(Byte.MAX_VALUE, Assert::assertNotNull);
	}

	private void tryConversion(byte b, Consumer<Exception> assertion) {
		Exception e = null;
		try {
			HashUtil.getHexChar(b);
		} catch (Exception ex) {
			e = ex;
		}
		assertion.accept(e);
	}
}
