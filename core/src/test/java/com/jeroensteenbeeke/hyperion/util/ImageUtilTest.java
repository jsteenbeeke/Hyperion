package com.jeroensteenbeeke.hyperion.util;

import com.jeroensteenbeeke.lux.TypedResult;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.awt.*;
import java.io.*;
import java.util.function.Consumer;

import static org.junit.Assert.*;

public class ImageUtilTest {
	static byte[] jpg;

	static byte[] png;

	static byte[] gif;

	@BeforeClass
	public static void initImageFiles() {
		jpg = load("test.jpg");
		png = load("test.png");
		gif = load("test.gif");
	}


	@Test
	public void testJpeg() throws IOException {
		testImage(jpg, this::assertJPEG);
	}

	@Test
	public void testGif() throws IOException {
		testImage(gif, this::assertGIF);
	}

	@Test
	public void testPNG() throws IOException {
		testImage(png, this::assertPNG);
	}


	@Test
	public void testWicketLogic() {
		assertEquals("png", ImageUtil.getWicketFormatType(png));
		assertEquals("jpeg", ImageUtil.getWicketFormatType(jpg));
		assertEquals("gif", ImageUtil.getWicketFormatType(gif));
		assertEquals("unknown", ImageUtil.getWicketFormatType(new byte[0]));
	}

	@Test
	public void testBlobLogic() {
		assertEquals("png", ImageUtil.getBlobType(png));
		assertEquals("jpg", ImageUtil.getBlobType(jpg));
		assertEquals("gif", ImageUtil.getBlobType(gif));
		assertEquals("unknown", ImageUtil.getBlobType(new byte[0]));
	}

	@Test
	public void testMimeTypeLogic() throws IOException {
		assertEquals("image/png", ImageUtil.getMimeType(png));
		assertEquals("image/jpeg", ImageUtil.getMimeType(jpg));
		assertEquals("image/gif", ImageUtil.getMimeType(gif));
		assertEquals("application/octet-stream", ImageUtil.getMimeType(new byte[0]));

		assertEquals(".png", ImageUtil.getExtensionByMimeType("image/png"));
		assertEquals(".gif", ImageUtil.getExtensionByMimeType("image/gif"));
		assertEquals(".jpg", ImageUtil.getExtensionByMimeType("image/jpeg"));
		assertEquals(".bin", ImageUtil.getExtensionByMimeType("application/octet-stream"));

		assertEquals("image/png", ImageUtil.getMimeType(toTempFile(png)).getObject());
		assertEquals("image/jpeg", ImageUtil.getMimeType(toTempFile(jpg)).getObject());
		assertEquals("image/gif", ImageUtil.getMimeType(toTempFile(gif)).getObject());
		assertEquals("application/octet-stream", ImageUtil.getMimeType(toTempFile("image_header".getBytes())).getObject());


	}

	private static byte[] load(String filename) {
		try (InputStream in = ImageUtilTest.class.getResourceAsStream(filename);
			 ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			int i;
			if (in == null) {
				return null;
			}

			while ((i = in.read()) != -1) {
				bos.write(i);
			}
			bos.flush();

			return bos.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

	private void assertPNG(byte[] data) {
		assertNotNull(data);
		assertFalse(ImageUtil.isJPEGImage(data));
		assertFalse(ImageUtil.isGIFImage(data));
		assertTrue(ImageUtil.isPNGImage(data));
		assertTrue(ImageUtil.isWebImage(data));
	}

	private void assertGIF(byte[] data) {
		assertNotNull(data);
		assertFalse(ImageUtil.isJPEGImage(data));
		assertTrue(ImageUtil.isGIFImage(data));
		assertFalse(ImageUtil.isPNGImage(data));
		assertTrue(ImageUtil.isWebImage(data));
	}

	private void assertJPEG(byte[] data) {
		assertNotNull(data);
		assertTrue(ImageUtil.isJPEGImage(data));
		assertFalse(ImageUtil.isGIFImage(data));
		assertFalse(ImageUtil.isPNGImage(data));
		assertTrue(ImageUtil.isWebImage(data));
	}

	private void testImage(byte[] data, Consumer<byte[]> assertion) throws IOException {
		assertion.accept(data);

		ImageUtil.getImageDimensions(data).ifNotOk(Assert::assertNull)
				.flatMap((Dimension dim) -> ImageUtil.resize(data, dim.width * 2, dim.height * 2))
				.ifNotOk(Assert::assertNull).allResults().forEach(assertion);

		ImageUtil.getImageDimensions(data).ifNotOk(Assert::assertNull)
				.flatMap((Dimension dim) -> ImageUtil.resize(data, dim.width / 2, dim.height / 2))
				.ifNotOk(Assert::assertNull).allResults().forEach(assertion);

		ImageUtil.getImageDimensions(new ByteArrayInputStream(data)).ifNotOk(Assert::assertNull)
				.flatMap((Dimension dim) -> ImageUtil.resize(data, dim.width * 2, dim.height * 2))
				.ifNotOk(Assert::assertNull).allResults().forEach(assertion);

		ImageUtil.getImageDimensions(new ByteArrayInputStream(data)).ifNotOk(Assert::assertNull)
				.flatMap((Dimension dim) -> ImageUtil.resize(data, dim.width / 2, dim.height / 2))
				.ifNotOk(Assert::assertNull).allResults().forEach(assertion);

		File tmp = toTempFile(data);

		ImageUtil.getImageDimensions(tmp).ifNotOk(Assert::assertNull)
				.flatMap((Dimension dim) -> ImageUtil.resize(data, dim.width * 2, dim.height * 2))
				.ifNotOk(Assert::assertNull).allResults().forEach(assertion);

		ImageUtil.getImageDimensions(tmp).ifNotOk(Assert::assertNull)
				.flatMap((Dimension dim) -> ImageUtil.resize(data, dim.width / 2, dim.height / 2))
				.ifNotOk(Assert::assertNull).allResults().forEach(assertion);

		tmp.delete();
	}

	private File toTempFile(byte[] data) throws IOException {
		File tmp = File.createTempFile("imageutiltest", ".tmp");
		FileOutputStream fos = new FileOutputStream(tmp);
		fos.write(data);
		fos.flush();
		fos.close();

		return tmp;
	}

	@Test
	public void testInvalidImage() throws IOException {
		TypedResult<Dimension> result =
				ImageUtil.getImageDimensions(new ByteArrayInputStream("I am not an image".getBytes()));

		assertFalse(result.isOk());
		assertEquals("No reader found for image", result.getMessage());

		result =
				ImageUtil.getImageDimensions(new ByteArrayInputStream("short".getBytes()));

		assertFalse(result.isOk());
		assertEquals("Could not read image header from stream", result.getMessage());

		File tmp = toTempFile("head".getBytes());

		result = ImageUtil.getImageDimensions(tmp);

		assertFalse(result.isOk());
		assertEquals("Failed to read image header", result.getMessage());

		tmp.delete();

		result = ImageUtil.getImageDimensions("head".getBytes());

		assertFalse(result.isOk());
		assertEquals("Given array is not a recognized image format", result.getMessage());

	}

}
