package com.jeroensteenbeeke.hyperion.util;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RandomizerTest {
	private static final int MANY_TIMES = 10000;

	private static final int LENGTH = 50;

	@Test
	public void testRandomizer() {
		Set<String> randoms = new HashSet<>();
		manyTimes(() ->
				assertTrue(randoms.add(Randomizer.random(LENGTH)))
		);

		assertEquals(MANY_TIMES, randoms.size());
	}

	private void manyTimes(Runnable operation) {
		for (int i = 0; i < MANY_TIMES; i++) {
			operation.run();
		}

	}
}
