package com.jeroensteenbeeke.hyperion.util;

import com.jeroensteenbeeke.lux.TypedResult;
import org.junit.Test;

import static com.jeroensteenbeeke.hyperion.test.matcher.ListMatchers.isList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class StringUtilTest {
	@Test
	public void testCapitalizationFunctions() {
		assertEquals("test", StringUtil.lowercaseFirst("test"));
		assertEquals("test", StringUtil.lowercaseFirst("Test"));
		assertEquals("tEST", StringUtil.lowercaseFirst("TEST"));
		assertEquals("i am a Test Case", StringUtil.lowercaseFirst("I am a Test Case"));

		assertEquals("t", StringUtil.lowercaseFirst("t"));
		assertEquals("t", StringUtil.lowercaseFirst("T"));
		assertEquals("", StringUtil.lowercaseFirst(""));

		assertEquals("Test", StringUtil.capitalizeFirst("test"));
		assertEquals("Test", StringUtil.capitalizeFirst("Test"));
		assertEquals("TEST", StringUtil.capitalizeFirst("TEST"));
		assertEquals("I am a Test Case", StringUtil.capitalizeFirst("I am a Test Case"));

		assertEquals("T", StringUtil.capitalizeFirst("t"));
		assertEquals("T", StringUtil.capitalizeFirst("T"));
		assertEquals("", StringUtil.capitalizeFirst(""));
	}

	@Test
	public void testTruncation() {
		assertFalse(TypedResult.attempt(() -> StringUtil.truncate("test").to(-1)).isOk());
		assertEquals("", StringUtil.truncate("test").to(0));
		assertEquals("t", StringUtil.truncate("test").to(1));
		assertEquals("te", StringUtil.truncate("test").to(2));
		assertEquals("tes", StringUtil.truncate("test").to(3));
		assertEquals("test", StringUtil.truncate("test").to(4));
		assertEquals("test", StringUtil.truncate("test").to(5));
		assertEquals("test", StringUtil.truncate("test").to(6));
	}

	@Test
	public void testPredicateSplit() {
		assertThat(StringUtil.split("", c -> true), isList(""));
		assertThat(StringUtil.split("test", c -> false), isList("test"));
		assertThat(StringUtil.split("1.0", c -> !Character.isLetterOrDigit(c)), isList("1", "0"));
	}

}
