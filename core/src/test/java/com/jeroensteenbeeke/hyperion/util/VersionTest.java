/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class VersionTest {

	@Test
	public void testSelfComparison() {
		Version a = Version.of("1");
		Version b = Version.of("1.0");
		Version c = Version.of("1.0.0");
		
		assertEquals(0, a.compareTo(a));
		assertEquals(0, b.compareTo(b));
		assertEquals(0, c.compareTo(c));
	}

	@Test
	public void testMixedSelfComparison() {
		Version a = Version.of("1.0-alpha");
		Version b = Version.of("1.0-rc1");
		Version c= Version.of("1.0-M1");
		
		assertEquals(0, a.compareTo(a));
		assertEquals(0, b.compareTo(b));
		assertEquals(0, c.compareTo(c));
	}
	
	@Test
	public void testNumericComparison() {
		Version a = Version.of("1.0");
		Version b = Version.of("1.1");
		Version c = Version.of("1.1.0");
		Version d = Version.of("2.0");
		
		assertEquals(-1, a.compareTo(b));
		assertEquals(0, b.compareTo(c));
		assertEquals(-1, c.compareTo(d));
		
		assertEquals(1, b.compareTo(a));
		assertEquals(0, c.compareTo(b));
		assertEquals(1, d.compareTo(c));
	}
	
	@Test
	public void testNonNumericComparison() {
		Version a = Version.of("1.0");
		Version b = Version.of("1.1-alpha");
		Version c = Version.of("1.1.M1");
		Version d = Version.of("1.1.RC1");
		Version e = Version.of("1.1");
		Version f = Version.of("1.1.");
		Version g = Version.of("1.1.32alpha");

		assertEquals("1.0", a.toString());

		assertEquals(0, a.compareTo(a));
		assertEquals(0, b.compareTo(b));
		assertEquals(0, c.compareTo(c));
		assertEquals(0, d.compareTo(d));
		assertEquals(0, e.compareTo(e));
		assertEquals(0, f.compareTo(f));
		assertEquals(0, g.compareTo(g));

		assertEquals(-1, a.compareTo(b));
		assertEquals(-1, b.compareTo(c));
		assertEquals(-1, c.compareTo(d));
		assertEquals(-1, d.compareTo(e));
		assertEquals(-4, f.compareTo(e));
		assertEquals(-3, g.compareTo(e));
		assertEquals(1, g.compareTo(b));
		assertEquals(-1, f.compareTo(g));
		
		assertEquals(-2, c.compareTo(e));
		assertEquals(-3, b.compareTo(e));
		assertEquals(-1, a.compareTo(e));
		
		assertEquals(-2, b.compareTo(d));
		assertEquals(-1, a.compareTo(d));
		
		assertEquals(-1, a.compareTo(c));
		
		// And reverse
		assertEquals(1, b.compareTo(a));
		assertEquals(1, c.compareTo(b));
		assertEquals(1, d.compareTo(c));
		assertEquals(1, e.compareTo(d));
		
		assertEquals(2, e.compareTo(c));
		assertEquals(3, e.compareTo(b));
		assertEquals(1, e.compareTo(a));
		
		assertEquals(2, d.compareTo(b));
		assertEquals(1, d.compareTo(a));
		
		assertEquals(-1, a.compareTo(c));
	}
}
