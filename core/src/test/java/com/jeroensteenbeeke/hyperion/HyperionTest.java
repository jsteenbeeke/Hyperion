package com.jeroensteenbeeke.hyperion;

import io.vavr.control.Option;
import org.junit.Test;

import static com.jeroensteenbeeke.hyperion.test.matcher.ComparableMatchers.greaterThan;
import static com.jeroensteenbeeke.hyperion.test.matcher.PatternMatchers.matchesPattern;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.*;

public class HyperionTest {
	@Test
	public void testPresentRevision() {
		assumeNotNull(Hyperion.class.getResourceAsStream("revision.txt"));

		Option<String> revision = Hyperion.getRevision();

		assertTrue(revision.isDefined());
		assertThat(revision.get(), matchesPattern("^([a-fA-F0-9])+$"));
	}

	@Test
	public void testAbsentRevision() {
		assumeTrue(Hyperion.class.getResourceAsStream("revision.txt") == null);

		Option<String> revision = Hyperion.getRevision();

		assertTrue(revision.isEmpty());
	}

	@Test
	public void testPresentCommitTitle() {
		assumeNotNull(Hyperion.class.getResourceAsStream("commit-title.txt"));

		Option<String> revision = Hyperion.getCommitTitle();

		assertTrue(revision.isDefined());
		assertThat(revision.get().length(), greaterThan(0));
	}

	@Test
	public void testAbsentCommitTitle() {
		assumeTrue(Hyperion.class.getResourceAsStream("commit-title.txt") == null);

		Option<String> revision = Hyperion.getCommitTitle();

		assertTrue(revision.isEmpty());
	}

	@Test
	public void testPresentCommitNotes() {
		assumeNotNull(Hyperion.class.getResourceAsStream("commit-notes.txt"));

		Option<String> revision = Hyperion.getCommitNotes();

		// Notes can be empty, in which case they also yield an empty option. We cannot test for content
		assertNotNull(revision);
	}

	@Test
	public void testAbsentCommitNotes() {
		assumeTrue(Hyperion.class.getResourceAsStream("commit-notes.txt") == null);

		Option<String> revision = Hyperion.getCommitNotes();

		assertTrue(revision.isEmpty());
	}


	@Test
	public void testSettingsInvocation() {
		HyperionSettings settings = Hyperion.getSettings();

		assertThat(settings, notNullValue());
		assertThat(settings.getUserDisplayName(), notNullValue());
	}
}
