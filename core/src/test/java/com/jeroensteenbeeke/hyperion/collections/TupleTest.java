package com.jeroensteenbeeke.hyperion.collections;

import com.jeroensteenbeeke.hyperion.test.Equivalence;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class TupleTest {
	@Test
	public void testDual() {
		Dual<String, Integer> a = new Dual<>("one", 2);
		Dual<String, Integer> b = new Dual<>("one", 2);
		Dual<String, Integer> c = new Dual<>("one", 3);
		Dual<String, Integer> d = new Dual<>("two", 2);
		Dual<String, String> e = new Dual<>("one", "2");
		Dual<String, String> f = new Dual<>("one", "2");
		Dual<String, String> g = new Dual<>("one", null);
		Dual<String, String> g2 = new Dual<>("one", null);
		Dual<String, String> h = new Dual<>(null, "2");
		Dual<String, String> h2 = new Dual<>(null, "2");
		Dual<String, String> i = new Dual<>(null, null);


		Equivalence.symmetric(a, b);
		Equivalence.reflexive(a);
		Equivalence.reflexive(b);
		Equivalence.reflexive(e);
		Equivalence.symmetric(e, f);
		Equivalence.reflexive(f);
		Equivalence.symmetric(g, g);
		Equivalence.symmetric(g, g2);
		Equivalence.reflexive(h);
		Equivalence.symmetric(h, h2);
		Equivalence.symmetric(i, i);

		// First element
		Equivalence.symmetric(a.getFirst(), b.getFirst());
		Equivalence.reflexive(a.getFirst());
		Equivalence.reflexive(b.getFirst());
		Equivalence.reflexive(e.getFirst());
		Equivalence.symmetric(e.getFirst(), f.getFirst());
		Equivalence.reflexive(f.getFirst());

		// Second element
		Equivalence.symmetric(a.getSecond(), b.getSecond());
		Equivalence.reflexive(a.getSecond());
		Equivalence.reflexive(b.getSecond());
		Equivalence.reflexive(e.getSecond());
		Equivalence.symmetric(e.getSecond(), f.getSecond());
		Equivalence.reflexive(f.getSecond());

		Equivalence.nonSymmetric(a, "a");
		Equivalence.nonSymmetric(a, c);
		Equivalence.nonSymmetric(a, d);
		Equivalence.nonSymmetric(b, c);
		Equivalence.nonSymmetric(b, d);
		Equivalence.nonSymmetric(c, d);
		Equivalence.nonSymmetric(a, e);
		Equivalence.nonSymmetric(b, e);
		Equivalence.nonSymmetric(c, e);
		Equivalence.nonSymmetric(d, e);
		Equivalence.nonSymmetric(a, f);
		Equivalence.nonSymmetric(b, f);
		Equivalence.nonSymmetric(c, f);
		Equivalence.nonSymmetric(d, f);
		Equivalence.nonSymmetric(a, g);
		Equivalence.nonSymmetric(b, g);
		Equivalence.nonSymmetric(c, g);
		Equivalence.nonSymmetric(d, g);
		Equivalence.nonSymmetric(e, g);
		Equivalence.nonSymmetric(f, g);
		Equivalence.nonSymmetric(a, h);
		Equivalence.nonSymmetric(b, h);
		Equivalence.nonSymmetric(c, h);
		Equivalence.nonSymmetric(d, h);
		Equivalence.nonSymmetric(e, h);
		Equivalence.nonSymmetric(f, h);
		Equivalence.nonSymmetric(g, h);
		Equivalence.nonSymmetric(a, i);
		Equivalence.nonSymmetric(b, i);
		Equivalence.nonSymmetric(c, i);
		Equivalence.nonSymmetric(d, i);
		Equivalence.nonSymmetric(e, i);
		Equivalence.nonSymmetric(f, i);
		Equivalence.nonSymmetric(g, i);
		Equivalence.nonSymmetric(h, i);
		assertFalse(a.equals(null));

		Set<Dual<String, Integer>> firstDualSet = new HashSet<>();
		firstDualSet.add(a);
		firstDualSet.add(b);
		firstDualSet.add(c);
		firstDualSet.add(d);

		assertEquals(3, firstDualSet.size());

		Set<Dual<String, String>> secondDualSet = new HashSet<>();
		secondDualSet.add(e);
		secondDualSet.add(f);
		secondDualSet.add(g);
		secondDualSet.add(h);
		secondDualSet.add(i);

		assertEquals(4, secondDualSet.size());

	}

	@Test
	public void testTriple() {
		Triple<String, Integer, Boolean> a = new Triple<>("one", 2, false);
		Triple<String, Integer, Boolean> b = new Triple<>("one", 2, false);
		Triple<String, Integer, Boolean> c = new Triple<>("one", 3, false);
		Triple<String, Integer, Boolean> d = new Triple<>("two", 2, true);
		Triple<String, Boolean, String> e = new Triple<>("one", true, "2");
		Triple<String, Boolean, String> f = new Triple<>("one", true, "2");
		Triple<String, Boolean, String> g = new Triple<>("one", true, null);
		Triple<String, Boolean, String> g2 = new Triple<>("one", true, null);

		Triple<String, Boolean, String> h = new Triple<>("one", null, "2");
		Triple<String, Boolean, String> h2 = new Triple<>("one", null, "2");

		Triple<String, Boolean, String> i = new Triple<>(null, true, "2");
		Triple<String, Boolean, String> i2 = new Triple<>(null, true, "2");

		Triple<String, Boolean, String> j = new Triple<>("one", null, null);
		Triple<String, Boolean, String> k = new Triple<>(null, true, null);
		Triple<String, Boolean, String> l = new Triple<>(null, null, "2");


		// Whole objects
		Equivalence.symmetric(a, b);
		Equivalence.reflexive(a);
		Equivalence.reflexive(b);
		Equivalence.reflexive(e);
		Equivalence.symmetric(e, f);
		Equivalence.reflexive(f);
		Equivalence.reflexive(g);
		Equivalence.reflexive(h);
		Equivalence.reflexive(i);
		Equivalence.reflexive(j);
		Equivalence.reflexive(k);
		Equivalence.reflexive(l);
		Equivalence.symmetric(g, g2);
		Equivalence.symmetric(h, h2);
		Equivalence.symmetric(i, i2);

		// First element
		Equivalence.symmetric(a.getFirst(), b.getFirst());
		Equivalence.symmetric(b.getFirst(), a.getFirst());
		Equivalence.symmetric(a.getFirst(), a.getFirst());
		Equivalence.symmetric(b.getFirst(), b.getFirst());
		Equivalence.symmetric(e.getFirst(), e.getFirst());
		Equivalence.symmetric(e.getFirst(), f.getFirst());
		Equivalence.symmetric(f.getFirst(), e.getFirst());
		Equivalence.symmetric(f.getFirst(), f.getFirst());

		// Second element
		Equivalence.symmetric(a.getSecond(), b.getSecond());
		Equivalence.symmetric(b.getSecond(), a.getSecond());
		Equivalence.symmetric(a.getSecond(), a.getSecond());
		Equivalence.symmetric(b.getSecond(), b.getSecond());
		Equivalence.symmetric(e.getSecond(), e.getSecond());
		Equivalence.symmetric(e.getSecond(), f.getSecond());
		Equivalence.symmetric(f.getSecond(), e.getSecond());
		Equivalence.symmetric(f.getSecond(), f.getSecond());

		// Third element
		Equivalence.symmetric(a.getThird(), b.getThird());
		Equivalence.symmetric(b.getThird(), a.getThird());
		Equivalence.symmetric(a.getThird(), a.getThird());
		Equivalence.symmetric(b.getThird(), b.getThird());
		Equivalence.symmetric(e.getThird(), e.getThird());
		Equivalence.symmetric(e.getThird(), f.getThird());
		Equivalence.symmetric(f.getThird(), e.getThird());
		Equivalence.symmetric(f.getThird(), f.getThird());

		Equivalence.nonSymmetric(a, "a");
		Equivalence.nonSymmetric("a", a);
		Equivalence.nonSymmetric(a, c);
		Equivalence.nonSymmetric(a, d);
		Equivalence.nonSymmetric(b, c);
		Equivalence.nonSymmetric(b, d);
		Equivalence.nonSymmetric(c, d);

		Equivalence.nonSymmetric(a, c);
		Equivalence.nonSymmetric(a, d);
		Equivalence.nonSymmetric(b, c);
		Equivalence.nonSymmetric(b, d);
		Equivalence.nonSymmetric(c, d);
		Equivalence.nonSymmetric(a, e);
		Equivalence.nonSymmetric(b, e);
		Equivalence.nonSymmetric(c, e);
		Equivalence.nonSymmetric(d, e);
		Equivalence.nonSymmetric(a, f);
		Equivalence.nonSymmetric(b, f);
		Equivalence.nonSymmetric(c, f);
		Equivalence.nonSymmetric(d, f);
		Equivalence.nonSymmetric(a, g);
		Equivalence.nonSymmetric(b, g);
		Equivalence.nonSymmetric(c, g);
		Equivalence.nonSymmetric(d, g);
		Equivalence.nonSymmetric(e, g);
		Equivalence.nonSymmetric(f, g);
		Equivalence.nonSymmetric(a, h);
		Equivalence.nonSymmetric(b, h);
		Equivalence.nonSymmetric(c, h);
		Equivalence.nonSymmetric(d, h);
		Equivalence.nonSymmetric(e, h);
		Equivalence.nonSymmetric(f, h);
		Equivalence.nonSymmetric(g, h);
		Equivalence.nonSymmetric(a, i);
		Equivalence.nonSymmetric(b, i);
		Equivalence.nonSymmetric(c, i);
		Equivalence.nonSymmetric(d, i);
		Equivalence.nonSymmetric(e, i);
		Equivalence.nonSymmetric(f, i);
		Equivalence.nonSymmetric(g, i);
		Equivalence.nonSymmetric(h, i);
		Equivalence.nonSymmetric(a, j);
		Equivalence.nonSymmetric(b, j);
		Equivalence.nonSymmetric(c, j);
		Equivalence.nonSymmetric(d, j);
		Equivalence.nonSymmetric(e, j);
		Equivalence.nonSymmetric(f, j);
		Equivalence.nonSymmetric(g, j);
		Equivalence.nonSymmetric(h, j);
		Equivalence.nonSymmetric(i, j);
		Equivalence.nonSymmetric(a, k);
		Equivalence.nonSymmetric(b, k);
		Equivalence.nonSymmetric(c, k);
		Equivalence.nonSymmetric(d, k);
		Equivalence.nonSymmetric(e, k);
		Equivalence.nonSymmetric(f, k);
		Equivalence.nonSymmetric(g, k);
		Equivalence.nonSymmetric(h, k);
		Equivalence.nonSymmetric(i, k);
		Equivalence.nonSymmetric(j, k);
		Equivalence.nonSymmetric(a, l);
		Equivalence.nonSymmetric(b, l);
		Equivalence.nonSymmetric(c, l);
		Equivalence.nonSymmetric(d, l);
		Equivalence.nonSymmetric(e, l);
		Equivalence.nonSymmetric(f, l);
		Equivalence.nonSymmetric(g, l);
		Equivalence.nonSymmetric(h, l);
		Equivalence.nonSymmetric(i, l);
		Equivalence.nonSymmetric(j, l);
		Equivalence.nonSymmetric(k, l);
		assertFalse(a.equals(null));


		Set<Triple<String, Integer, Boolean>> firstTripleSet = new HashSet<>();
		firstTripleSet.add(a);
		firstTripleSet.add(b);
		firstTripleSet.add(c);
		firstTripleSet.add(d);

		assertEquals(3, firstTripleSet.size());

		Set<Triple<String, Boolean, String>> secondTripleSet = new HashSet<>();
		secondTripleSet.add(e);
		secondTripleSet.add(f);
		secondTripleSet.add(g);
		secondTripleSet.add(h);
		secondTripleSet.add(i);
		secondTripleSet.add(j);
		secondTripleSet.add(k);
		secondTripleSet.add(l);

		assertEquals(7, secondTripleSet.size());
	}
}
