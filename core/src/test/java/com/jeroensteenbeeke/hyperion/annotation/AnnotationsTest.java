package com.jeroensteenbeeke.hyperion.annotation;

import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.annotation.XmlElement;
import java.beans.Transient;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AnnotationsTest {
	@Test
	public void testClassFunctionality() {
		Annotations a = Annotations.fromClass(AnnotationsTestClass.class);

		assertFalse(a.hasAnnotation(XmlElement.class));
		assertTrue(a.hasAnnotation(Ignore.class));

		Optional<Ignore> annotation = a.getAnnotation(Ignore.class);

		assertTrue(annotation.isPresent());
	}

	@Test
	public void testMethodAnnotation() throws NoSuchMethodException {
		Method method = AnnotationsTestClass.class.getMethod("getField");

		Annotations a = Annotations.fromMethod(method);

		assertTrue(a.hasAnnotation(Transient.class));
		assertFalse(a.hasAnnotation(Ignore.class));

		Optional<Transient> annotation = a.getAnnotation(Transient.class);

		assertTrue(annotation.isPresent());
	}

	@Test
	public void testFieldAnnotation() throws NoSuchFieldException {
		Field field = AnnotationsTestClass.class.getDeclaredField("field");

		Annotations a = Annotations.fromField(field);

		assertTrue(a.hasAnnotation(XmlElement.class));
		assertFalse(a.hasAnnotation(Ignore.class));

		Optional<XmlElement> annotation = a.getAnnotation(XmlElement.class);

		assertTrue(annotation.isPresent());
	}

	@Ignore
	private class AnnotationsTestClass {
		@XmlElement
		private String field;

		@Transient
		public String getField() {
			return field;
		}
	}

}
