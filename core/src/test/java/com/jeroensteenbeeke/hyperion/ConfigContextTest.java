package com.jeroensteenbeeke.hyperion;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class ConfigContextTest {
	@Test
	public void testDirectoryReturned() {
		File file = new File("i_am_a_directory");

		File firstInvocation = ConfigContext.Instance.getConfigDirectory();
		File secondInvocation = ConfigContext.Instance.getConfigDirectory();

		assertEquals(firstInvocation, secondInvocation);

		ConfigContext.Instance.setConfigDirectory(file);

		File thirdInvocation = ConfigContext.Instance.getConfigDirectory();

		assertEquals(file, thirdInvocation);
		assertNotEquals(thirdInvocation, secondInvocation);
	}

	@Test
	public void invokeEnumMethods() {
		// For test coverage
		assertNotNull(ConfigContext.valueOf("Instance"));
		assertEquals(1, ConfigContext.values().length);
	}
}
