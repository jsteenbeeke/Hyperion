package com.jeroensteenbeeke.hyperion.listrefgen;

import com.google.common.base.Joiner;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import io.swagger.annotations.ApiModel;
import org.junit.Test;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.tools.JavaFileObject;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;

public class ListReferenceGeneratorTest {
	@Test
	public void testQueryObjectGenerator() {
		JavaFileObject file =
				JavaFileObjects.forSourceLines("com.jeroensteenbeeke.hyperion.listrefgen.TestObject",
											   "package com.jeroensteenbeeke.hyperion.listrefgen;",
											   "",
											   "import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;",
											   "import io.swagger.annotations.ApiModel;",
											   "import io.swagger.annotations.ApiModelProperty;",
											   "",
											   "@ApiModel",
											   "public class TestObject {",
											   "\t@Queryable",
											   "\t@ApiModelProperty(value=\"A value for testing purposes\")",
											   "\tprivate String testValue;",
											   "}");


		ClassLoader loader = ApiModel.class.getClassLoader();
		System.out.println(getClasspathFromClassloader(loader));

		Compilation compilation = javac().withOptions("-verbose", "-cp", System.getProperty("java" +
																									".class.path"))
										 .withProcessors(new TestListReferenceGenerator())
										 .compile
												 (file);
		assertThat(compilation).succeeded();
		assertThat(compilation)
				.generatedSourceFile(
						"com.jeroensteenbeeke.hyperion.listrefgen.ListOf")
				.hasSourceEquivalentTo(JavaFileObjects
											   .forSourceLines(
													   "com.jeroensteenbeeke.hyperion.listrefgen.ListOf",
													   "package com.jeroensteenbeeke.hyperion.listrefgen;",
													   "",
													   "import java.util.List;",
													   "import javax.annotation.Generated;",
													   "import javax.ws.rs.core.GenericType;",
													   "",
													   "@Generated(value = \"com.jeroensteenbeeke.hyperion.listrefgen" +
															   ".ListReferenceGenerator\",",
													   "\tdate= \"now\")",
													   "public class ListOf {",
													   "\tpublic static final GenericType<List<TestObject>> TESTOBJECT = new GenericType<List<TestObject>>() {};",

													   "}"

											   ));

	}

	@SupportedAnnotationTypes("com.jeroensteenbeeke.hyperion.rest.annotations.Queryable")
	@SupportedSourceVersion(SourceVersion.RELEASE_8)
	private static class TestListReferenceGenerator extends ListReferenceGenerator {
		@Override
		protected String getDateString() {
			return "now";
		}
	}

	private static String getClasspathFromClassloader(ClassLoader currentClassloader) {
		ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

		for (ArrayList<ClassLoader> classloaders = new ArrayList<>(); currentClassloader != null;
			 currentClassloader = currentClassloader.getParent()) {
			classloaders.add(currentClassloader);
			if (currentClassloader == systemClassLoader) {
				Set<String> classpaths = new LinkedHashSet<>();
				Iterator var4 = classloaders.iterator();

				while (var4.hasNext()) {
					ClassLoader classLoader = (ClassLoader) var4.next();
					if (classLoader instanceof URLClassLoader) {
						URLClassLoader ucl = (URLClassLoader) classLoader;
						URL[] var6 = ucl.getURLs();
						int var7 = var6.length;

						for (int var8 = 0; var8 < var7; ++var8) {
							URL url = var6[var8];
							if (!url.getProtocol().equals("file")) {
								throw new IllegalArgumentException(
										"Given classloader consists of classpaths which are " +
												"unsupported for compilation.");
							}

							classpaths.add(url.getPath());
						}
					}
				}

				return Joiner.on("\n").join(classpaths);
			}
		}

		throw new IllegalArgumentException(
				"Classpath for compilation could not be extracted since given classloader is not " +
						"an instance of URLClassloader");
	}
}
