/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. <p> This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details. <p> You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import com.jeroensteenbeeke.andalite.java.analyzer.AnalyzedClass;
import com.jeroensteenbeeke.andalite.java.analyzer.AnalyzedMethod;
import com.jeroensteenbeeke.andalite.java.analyzer.AnalyzedSourceFile;
import com.jeroensteenbeeke.andalite.java.analyzer.ClassAnalyzer;
import java.util.Locale;

import com.jeroensteenbeeke.lux.TypedResult;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.plugin.testing.stubs.MavenProjectStub;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.MalformedURLException;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import static org.hamcrest.CoreMatchers.*;

public class GeneratorMojoTest extends AbstractMojoTestCase {

	@Test
	public void testGenerateInterfaces() throws MojoExecutionException,
			MojoFailureException, MalformedURLException {
		File pom = getTestFile("src/test/resources/unit/testproject/pom.xml");

		File resourceFile = getTestFile(
				"src/test/resources/unit/testproject/target/generated-sources/retrofit/com/jeroensteenbeeke/hyperion"
						+
						"/retrofitgen/RFFakeResource.java");
		File modelFile = getTestFile(
				"src/test/resources/unit/testproject/target/generated-sources/retrofit/com/jeroensteenbeeke/hyperion"
						+
						"/retrofitgen/RFFake.java");
		File queryFile = getTestFile(
				"src/test/resources/unit/testproject/target/generated-sources/retrofit/com/jeroensteenbeeke/hyperion"
						+
						"/retrofitgen/RFFake_Query.java");

		if (resourceFile.exists()) {
			assertTrue(resourceFile.delete());
		}

		if (modelFile.exists()) {
			assertTrue(modelFile.delete());
		}

		if (queryFile.exists()) {
			assertTrue(queryFile.delete());
		}


		GeneratorMojo mojo = null;
		try {
			mojo = (GeneratorMojo) lookupMojo("generate", pom);
		} catch (Exception e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}

		File testRoot = pom.getParentFile();

		mojo.project = new MavenProjectStub() {
			@Override
			public File getBasedir() {
				return testRoot;
			}
		};
		mojo.basePackage = "com.jeroensteenbeeke.hyperion.retrofitgen";
		mojo.resourceClassPrefix = "RF";
		mojo.queryClassPrefix = "RF";
		mojo.modelClassPrefix = "RF";
		mojo.generatedSourcesSubdir = "retrofit";
		mojo.serviceMethodOutputType = ServiceMethodOutputType.OBSERVABLE;

		mojo.execute();


		try {
			assertTrue("RFFakeResource was generated", resourceFile.exists());
			assertTrue("RFFake was generated", modelFile.exists());
			assertTrue("RFFake_Query was generated", queryFile.exists());

			TypedResult<AnalyzedSourceFile> result = new ClassAnalyzer(
					resourceFile).analyze();
			assertTrue("RFFakeResource parsed successfully", result.isOk());
			AnalyzedSourceFile source = result.getObject();

			boolean methodsProperlyAnnotated = source.getInterfaces()
					.stream()
					.filter(i -> i.getDenominationName().equals("RFFakeResource"))
					.flatMap(i -> i.getMethods().stream())
					.allMatch(
							m -> m.hasAnnotation("GET")
									|| m.hasAnnotation("POST")
									|| m.hasAnnotation("PUT")
									|| m.hasAnnotation("DELETE")
					);

			assertTrue("All methods annotated with one of: @GET, @POST, @PUT, @DELETE",
					methodsProperlyAnnotated);

			assertTrue("Method updateFake does not have parameters 'id' and 'payload'",
					source.getInterfaces()
							.stream()
							.filter(i -> i.getDenominationName().equals("RFFakeResource"))
							.flatMap(i -> i.getMethods().stream())
							.filter(m -> m.getName().equals("updateFake"))
							.allMatch(m ->
									m.getParameters().stream().filter(p -> p.getName().equals("id"))
											.allMatch(p -> p.hasAnnotation("Path"))
											&&
											m.getParameters().stream()
													.filter(p -> p.getName().equals("payload"))
													.allMatch(p -> p.hasAnnotation("Body")
													) &&
											m.getParameters().stream()
													.filter(p -> p.getName().equals("xAuthToken"))
													.allMatch(p -> p.hasAnnotation("Header")
													)));

			result = new ClassAnalyzer(
					modelFile).analyze();
			assertTrue("RFFake parsed successfully", result.isOk());
			source = result.getObject();

			Assert.assertThat(source.getClasses().size(), CoreMatchers.equalTo(1));
			AnalyzedClass rffake = source.getClasses().get(0);

			AnalyzedMethod getBars = rffake.getMethod().withReturnType("List<Bar>").named("getBars");
			Assert.assertThat(getBars, notNullValue());
			Assert.assertThat(getBars.getAnnotation("Nonnull"), notNullValue());

			result = new ClassAnalyzer(
					queryFile).analyze();
			assertTrue("RFFake_Query parsed successfully", result.isOk());
			source = result.getObject();

			assertTrue("RFFake_Query has a toMap() method", source.getInterfaces().stream()
					.filter(i -> i.getDenominationName().equals("RFFake_Query"))
					.flatMap(i -> i.getMethods().stream())
					.filter(m -> m.getName().equals("toMap")).allMatch(m ->
							m.getParameters().isEmpty() &&
									"Map<String,String>".equals(m.getReturnType().toJavaString())
					));

			Compilation compilation = javac().withOptions("-classpath", System.getProperty("java.class.path"))
					.compile(JavaFileObjects.forResource(modelFile.toURI().toURL()),
							JavaFileObjects.forResource(queryFile.toURI().toURL()),
							JavaFileObjects.forResource(resourceFile.toURI().toURL())
					);

			compilation.diagnostics().asList()
					.forEach(d -> System.out.println(d.getMessage(Locale.ENGLISH)));

			assertThat(
					compilation)
					.succeededWithoutWarnings();
		} finally {
			if (resourceFile.exists()) {
				// expectedFile.deleteOnExit();
			}
		}
	}

}
