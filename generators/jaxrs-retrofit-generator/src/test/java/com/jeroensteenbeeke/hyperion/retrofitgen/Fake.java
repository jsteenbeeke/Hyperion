/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.util.List;

@ApiModel(description = "object containing fake news")
public class Fake {
	@Queryable
	@ApiModelProperty(value = "The headline of the fake news")
	private String title;

	@ApiModelProperty(required = true)
	private List<Bar> bars;

	public String getTitle() {
		return title;
	}

	public Fake setTitle(String title) {
		this.title = title;
		return this;
	}

	@Nonnull
	public List<Bar> getBars() {
		return bars;
	}

	public void setBars(@Nonnull List<Bar> bars) {
		this.bars = bars;
	}
}
