/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. <p> This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details. <p> You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.retrofitgen;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Api(value = "Endpoint to work with fake news")
@Path("/fake")
public interface FakeResource {

	@GET
	@Path("/{id}")
	@ApiOperation(response = Fake.class, value = "Get a Fake object by ID")
	Response getFake(
			@ApiParam(value = "The identifier of the fake", required = true) @PathParam("id") Long id);

	@GET
	@ApiOperation(responseContainer = "List", response = Fake.class, value = "Get a list of Fake objects")
	Response listFakes(@BeanParam Fake_Query query);

	@PUT
	@Path("/{id}")
	@ApiOperation(value = "Update the fake", response = Fake.class)
	@ApiImplicitParams(
			@ApiImplicitParam(name = "X-Auth-Token", dataType = "string", paramType = "header", example = "dfsdf32r324")
	)
	Response updateFake(
			@ApiParam(value = "The identifier of the fake", required = true) @PathParam("id") Long id,
			Fake fake);

	@POST
	@ApiOperation(response = Fake.class, value = "Create a new fake object")
	@ApiImplicitParams(
			@ApiImplicitParam(name = "X-Auth-Token", dataType = "string", paramType = "header", example = "dfsdf32r324")
	)
	Response createFake(Fake fake);

	@DELETE
	@Path("/{id}")
	@ApiOperation(response = Fake.class, value = "Delete the fake object by the given ID")
	Response deleteFake(
			@ApiParam(value = "The identifier of the fake", required = true) @PathParam("id") Long id);

	@GET
	@Path("/simplesearch")
	@ApiOperation(value = "Search for fakes matching the given query", responseContainer = "List", response = Fake.class)
	Response doSimpleSearch(
			@ApiParam(value = "The text to search for", required = true) @QueryParam("query") String query);
}
