package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.jeroensteenbeeke.hyperion.rest.query.StringProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import io.swagger.annotations.ApiModelProperty;

import javax.ws.rs.QueryParam;

public class Fake_Query implements QueryObject<Fake> {
	private static final long serialVersionUID = -7393044415236528122L;

	private int nextSortIndex;

	@QueryParam("title")
	@ApiModelProperty
	private StringProperty<Fake_Query> title = new StringProperty<>(this, "title");

	public StringProperty<Fake_Query> title() {
		return title;
	}

	@Override
	public int getNextSortIndex() {
		return nextSortIndex++;
	}

	@Override
	public boolean matches(Fake object) {
		return title.appliesTo(object);
	}
}
