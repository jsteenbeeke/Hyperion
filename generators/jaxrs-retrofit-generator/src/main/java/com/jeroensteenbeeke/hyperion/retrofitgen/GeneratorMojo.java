/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. <p> This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details. <p> You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.google.common.collect.ImmutableMap;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;

import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IBooleanProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IComparableProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IStringProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import com.jeroensteenbeeke.hyperion.util.StringUtil;
import io.swagger.annotations.*;
import javassist.Modifier;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.sonatype.plexus.build.incremental.BuildContext;

/**
 * Maven plugin for generating retrofit classes for JAX-RS services
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES,
	requiresDependencyResolution = ResolutionScope.COMPILE)
public class GeneratorMojo extends AbstractMojo {
	private static final ImmutableMap<String, String> BACKPORTS = ImmutableMap.<String, String>builder()
		.put("java.time", "org.threeten.bp")
		.build();

	@Parameter(required = true)
	public String basePackage;

	@Parameter(required = false, defaultValue = "generated-sources")
	public String generatedSourcesDir;

	@Parameter(required = false, defaultValue = "retrofit")
	public String generatedSourcesSubdir;

	@Parameter(required = false, defaultValue = "CALL")
	public ServiceMethodOutputType serviceMethodOutputType;

	@Parameter(required = false, defaultValue = "GSON")
	public JsonOutputType jsonOutputType;

	@Parameter(required = false)
	public String resourceClassPrefix;

	@Parameter(required = false)
	public String modelClassPrefix;

	@Parameter(required = false)
	public String queryClassPrefix;

	/**
	 * @component
	 */
	@Component
	public BuildContext buildContext;

	@Parameter(required = true, property = "project")
	public MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		generatedSourcesDir = Optional.ofNullable(generatedSourcesDir).orElse("generated-sources");
		generatedSourcesSubdir = Optional.ofNullable(generatedSourcesSubdir).orElse("retrofit");
		resourceClassPrefix = Optional.ofNullable(resourceClassPrefix).orElse("");
		modelClassPrefix = Optional.ofNullable(modelClassPrefix).orElse("");
		queryClassPrefix = Optional.ofNullable(queryClassPrefix).orElse("");
		jsonOutputType = Optional.ofNullable(jsonOutputType).orElse(JsonOutputType.GSON);
		basePackage = Optional.ofNullable(basePackage).orElse("");

		Reflections ref = new Reflections(new ConfigurationBuilder()
											  .setUrls(ClasspathHelper.forPackage(basePackage))
											  .setScanners(new SubTypesScanner(), new TypeAnnotationsScanner(),
														   new FieldAnnotationsScanner(), new MethodAnnotationsScanner())
		);

		Map<String, ClassMapping> mappings = new HashMap<>();
		getLog().info("Converting model classes");
		for (Class<?> modelClass : ref.getTypesAnnotatedWith(ApiModel.class)) {
			if (!modelClass.isInterface()) {
				mappings.put(modelClass.getName(), processModelClass(modelClass));
			} else {
				getLog().error(
					String.format("Class %s is an interface, ignoring", modelClass.getName()));
			}
		}

		getLog().info("Converting generated querysupport classes");
		for (Class<?> queryClass : ref.getSubTypesOf(QueryObject.class)) {
			if (queryClass.isInterface()) {
				getLog().error(
					String.format("Class %s is an interface, ignoring", queryClass.getName()));
			} else {
				processQueryClass(queryClass, mappings);
			}
		}

		getLog().info("Finding JAX-RS interfaces");
		Set<Class<?>> pathClasses = ref.getTypesAnnotatedWith(Path.class);
		getLog().info(String.format("Found %d classes", pathClasses.size()));
		for (Class<?> c : pathClasses) {
			if (!c.isInterface()) {
				getLog().error(String.format(
					"Class %s is not an interface, ignoring",
					c.getName()));

				continue;
			}

			processInterface(c, mappings);
		}
	}

	private void processQueryClass(Class<?> queryClass,
								   Map<String, ClassMapping> mappings) throws MojoExecutionException {
		String pkg = queryClass.getPackage().getName();
		String newName = modelClassPrefix.concat(queryClass.getSimpleName());

		getLog().info(String.format("Processing model class %s.%s", pkg,
									queryClass.getSimpleName()));

		Optional<String> genericClass = Optional.empty();

		for (Type type : queryClass.getGenericInterfaces()) {
			if (type.getTypeName()
					.startsWith("com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject")) {
				if (type instanceof ParameterizedType) {
					ParameterizedType pt = (ParameterizedType) type;

					if (pt.getActualTypeArguments().length != 1) {
						throw new MojoExecutionException(
							"Invalid number of type parameters for query object " + queryClass
								.getName() + ": " +
								pt.getActualTypeArguments().length);
					} else {
						genericClass = Optional.of(pt.getActualTypeArguments()[0].getTypeName());
					}
				}
			}
		}

		Set<String> imports = new TreeSet<>();
		imports.add("com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject");

		if (genericClass.isEmpty()) {
			throw new MojoExecutionException(
				"Could not determine generic regular object type for " + queryClass.getName());
		} else {
			String e = genericClass.get();

			if (!e.startsWith(pkg) || e.substring(pkg.length() + 1).indexOf('.') != -1) {
				imports.add(mappings.computeIfAbsent(e, ClassMapping::fromFQDN).getFqdn());
			}
		}

		File basedir = project != null && project.getBasedir() != null ? project.getBasedir() :
			new File(System.getProperty("user.dir"));
		File target = create(new File(basedir, "target"));
		File generatedSources = create(new File(target, generatedSourcesDir));
		File retrofit = create(new File(generatedSources, generatedSourcesSubdir));
		File packageDir = createPackage(retrofit, pkg);

		File java = new File(packageDir, newName.concat(".java"));
		try {
			if (!java.exists() && !java.createNewFile()) {
				throw new MojoExecutionException(String
													 .format("Could not create file %s", java.getName()));
			}
		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage());
		}

		imports.add("javax.annotation.Generated");
		imports.add("java.util.Map");
		imports.add("java.util.HashMap");
		imports.add("com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject");

		for (Field f : queryClass.getDeclaredFields()) {
			if (!IQueryProperty.class.isAssignableFrom(f.getType())) {
				continue;
			}
			imports.add(mappings.computeIfAbsent(f.getType().getName(), ClassMapping::fromFQDN)
								.getFqdn());
		}

		try (FileWriter fw = new FileWriter(java);
			 PrintWriter pw = new PrintWriter(fw)) {
			pw.printf("package %s;", pkg);
			pw.println();
			pw.println();
			imports.forEach(i -> {
				pw.print("import ");
				pw.print(i);
				pw.println(";");
			});
			pw.println();
			pw.printf("@Generated(value=\"%s\", date=\"%s\")", getClass().getName(),
					  ZonedDateTime.now().format(
						  DateTimeFormatter.ISO_OFFSET_DATE_TIME));
			pw.println();
			String genericType = mappings.computeIfAbsent(genericClass.get(), ClassMapping::fromFQDN).newName;
			pw.printf("public class %s implements QueryObject<%s> {", newName,
					  genericType);
			pw.println();
			pw.println("\tprivate int nextOrderByIndex;");
			pw.println();

			for (Field f : queryClass.getDeclaredFields()) {
				if (!IQueryProperty.class.isAssignableFrom(f.getType())) {
					continue;
				}
				pw.printf("\tprivate %1$s<%2$s> %3$s = new %1$s<>(this, \"%3$s\");",
						  f.getType().getSimpleName(), newName,
						  f.getName());
				pw.println();
				pw.println();
			}

			for (Field f : queryClass.getDeclaredFields()) {
				if (!IQueryProperty.class.isAssignableFrom(f.getType())) {
					continue;
				}
				pw.printf("\tpublic %1$s<%2$s> %3$s() {", f.getType().getSimpleName(), newName,
						  f.getName());
				pw.println();
				pw.printf("\t\treturn this.%s;", f.getName());
				pw.println();
				pw.println("\t}");
				pw.println();
			}

			pw.println("\tpublic Map<String,String> toMap() {");
			pw.println("\t\tMap<String,String> result = new HashMap<>();");
			for (Field f : queryClass.getDeclaredFields()) {
				if (!IQueryProperty.class.isAssignableFrom(f.getType())) {
					continue;
				}
				pw.printf("\t\tString %1$sRep = this.%1$s.toString();", f.getName());
				pw.println();
				pw.printf("\t\tif (!%1$sRep.isEmpty()) {", f.getName());
				pw.println();
				pw.printf("\t\t\tresult.put(\"%1$s\", %1$sRep);", f.getName());
				pw.println();
				pw.println("\t\t}");
			}
			pw.println("\t\treturn result;");
			pw.println("\t}");
			pw.println();
			pw.println("\t@Override");
			pw.println("\tpublic int getNextSortIndex() {");
			pw.println("\t\treturn nextOrderByIndex++;");
			pw.println("\t}");
			pw.println();
			pw.println("\t@Override");
			pw.printf("\tpublic boolean matches(%s object) {", genericType).println();
			if (queryClass.getDeclaredFields().length == 0) {
				pw.println("\t\treturn true;");
			} else {
				pw.printf("\t\treturn %s;", Arrays.stream(queryClass.getDeclaredFields())
												  .filter(f -> IQueryProperty.class.isAssignableFrom(f.getType()))
												  .map(Field::getName)
												  .map(n -> n + ".appliesTo(object)")
												  .collect(Collectors.joining(" && "))
				).println();
			}
			pw.println("\t}");
			pw.println();


			pw.println("}");

		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage(), e);
		} finally {
			if (buildContext != null) {
				buildContext.refresh(target);
			}
			if (project != null) {
				project.addCompileSourceRoot(
					"target/generated-sources/retrofit");
			}
		}
	}

	private ClassMapping processModelClass(Class<?> modelClass)
		throws MojoExecutionException, MojoFailureException {
		String pkg = modelClass.getPackage().getName();
		String newName = modelClassPrefix.concat(modelClass.getSimpleName());
		ClassMapping mapping = new ClassMapping(pkg, newName);

		getLog().info(String.format("Processing model class %s.%s", pkg,
									modelClass.getSimpleName()));

		File basedir = project != null && project.getBasedir() != null ? project.getBasedir() :
			new File(System.getProperty("user.dir"));
		File target = create(new File(basedir, "target"));
		File generatedSources = create(new File(target, generatedSourcesDir));
		File retrofit = create(new File(generatedSources, generatedSourcesSubdir));
		File packageDir = createPackage(retrofit, pkg);

		File java = new File(packageDir, newName.concat(".java"));
		try {
			if (!java.exists() && !java.createNewFile()) {
				throw new MojoExecutionException(String
													 .format("Could not create file %s", java.getName()));
			}
		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage());
		}

		ApiModel model = Optional.ofNullable(modelClass.getAnnotation(ApiModel.class))
								 .orElseThrow(
									 () -> new MojoFailureException(
										 String.format("Model class %s does not have an ApiModel " +
														   "annotation", modelClass.getName())));

		List<Field> fields = new LinkedList<>();
		Set<String> imports = new HashSet<>();
		imports.add("javax.annotation.Generated");
		imports.addAll(jsonOutputType.getAdditionalImports());

		for (Field f : modelClass.getDeclaredFields()) {
			if (Modifier.isStatic(f.getModifiers())) {
				continue;
			}

			if ("$jacocoData".equals(f.getName())) {
				continue;
			}

			ApiModelProperty prop = f.getAnnotation(ApiModelProperty.class);
			if (prop == null) {
				throw new MojoFailureException(String.format(
					"Model class %s has a field %s without an ApiModelProperty annotation",
					modelClass.getName(), f.getName()));
			}

			Class<?> fieldType = f.getType();
			if (!fieldType.isPrimitive()) {
				String fieldTypePackage = fieldType.getPackage().getName();

				for (Map.Entry<String, String> entry : BACKPORTS.entrySet()) {
					if (fieldTypePackage.startsWith(entry.getKey())) {
						fieldTypePackage = fieldTypePackage.replaceFirst(entry.getKey(), entry.getValue());
					}
				}

				if (f.getType().isAssignableFrom(List.class)) {
					imports.add("java.util.List");
				} else {

					if (!fieldTypePackage.equals(pkg) && !"java.lang".equals(fieldTypePackage)) {
						imports.add(fieldTypePackage + "." + fieldType.getSimpleName());
					}
				}

				if (prop.required()) {
					imports.add("javax.annotation.Nonnull");
				} else {
					imports.add("javax.annotation.CheckForNull");
					imports.add("javax.annotation.Nullable");

				}
			}

			fields.add(f);
		}

		try (FileWriter fw = new FileWriter(java);
			 PrintWriter pw = new PrintWriter(fw)) {
			pw.printf("package %s;", pkg);
			pw.println();
			pw.println();
			imports.forEach(i -> {
				pw.print("import ");
				pw.print(i);
				pw.println(";");
			});
			pw.println();

			pw.println("/**");
			pw.print(" * ");
			pw.println(model.description());
			pw.println(" */");
			pw.printf("@Generated(value=\"%s\", date=\"%s\")", getClass().getName(),
					  ZonedDateTime.now().format(
						  DateTimeFormatter.ISO_OFFSET_DATE_TIME));
			pw.println();
			pw.printf("public class %s {", newName);
			pw.println();

			for (Field f : fields) {
				jsonOutputType.annotateField(f, pw);

				pw.print("\tprivate ");
				pw.print(getType(f));
				pw.print(" ");
				pw.print(f.getName());
				pw.println(";");
				pw.println();
			}

			for (Field f : fields) {
				ApiModelProperty prop = f.getAnnotation(ApiModelProperty.class);
				Class<?> fieldType = f.getType();

				if (prop != null) {
					pw.println("\t/**");
					pw.printf("\t * %s", prop.value());
					pw.println();
					pw.println("\t */");
					if (!fieldType.isPrimitive()) {
						if (prop.required()) {
							pw.println("\t@Nonnull");
						} else {
							pw.println("\t@CheckForNull");
						}
					}
				}

				pw.print("\tpublic ");
				pw.print(getType(f));
				if (f.getType().equals(boolean.class)) {
					pw.print(" is");
				} else {
					pw.print(" get");
				}
				pw.print(StringUtil.capitalizeFirst(f.getName()));
				pw.println("() {");
				pw.print("\t\treturn this.");
				pw.print(f.getName());
				pw.println(";");
				pw.println("\t}");
				pw.println();

				pw.print("\tpublic void set");
				pw.print(StringUtil.capitalizeFirst(f.getName()));
				pw.print("(");
				if (prop != null && !fieldType.isPrimitive()) {
					if (prop.required()) {
						pw.print("@Nonnull ");
					} else {
						pw.print("@Nullable ");
					}
				}
				pw.print(getType(f));
				pw.print(" ");
				pw.print(f.getName());
				pw.println(") {");
				pw.printf("\t\tthis.%1$s = %1$s;", f.getName());
				pw.println();
				pw.println("\t}");
				pw.println();
			}

			pw.println("}");

		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage(), e);
		} finally {
			if (buildContext != null) {
				buildContext.refresh(target);
			}
			if (project != null) {
				project.addCompileSourceRoot(
					"target/generated-sources/retrofit");
			}
		}

		return mapping;
	}

	private String getType(Field f) {
		if (List.class.isAssignableFrom(f.getType())) {
			ParameterizedType genericType = (ParameterizedType) f.getGenericType();

			Class<?> listEntryType = (Class<?>) genericType.getActualTypeArguments()[0];

			return "List<" + listEntryType.getSimpleName() + ">";

		}

		return f.getType().getSimpleName();
	}


	private void processInterface(Class<?> c,
								  Map<String, ClassMapping> mappings)
		throws MojoExecutionException, MojoFailureException {
		String pkg = c.getPackage().getName();

		getLog().info(String.format("Processing resource %s.%s", pkg,
									c.getSimpleName()));

		File basedir = project != null && project.getBasedir() != null ? project.getBasedir() :
			new File(System.getProperty("user.dir"));
		File target = create(new File(basedir, "target"));
		File generatedSources = create(new File(target, generatedSourcesDir));
		File retrofit = create(new File(generatedSources, generatedSourcesSubdir));

		File packageDir = createPackage(retrofit, pkg);

		Path path = c.getAnnotation(Path.class);
		String prefix = path != null ? path.value() : "";

		// Retrofit URLs starting with / are considered absolute rather than relative to base URL
		while (prefix.startsWith("/")) {
			prefix = prefix.substring(1);
		}

		File java = new File(packageDir,
							 resourceClassPrefix.concat(c.getSimpleName()).concat(".java"));
		try {
			if (!java.exists() && !java.createNewFile()) {
				throw new MojoExecutionException(String
													 .format("Could not create file %s", java.getName()));
			}
		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage());
		}

		try (FileWriter fw = new FileWriter(java);
			 PrintWriter pw = new PrintWriter(fw)) {
			pw.printf("package %s;", pkg);
			pw.println();
			pw.println();

			Set<String> imports = new TreeSet<>();

			imports.add("retrofit2.http.*");
			imports.add("javax.annotation.Generated");

			for (Method method : c.getMethods()) {
				ApiOperation apiOperation = method
					.getAnnotation(ApiOperation.class);

				Class<?> returnType = method.getReturnType();
				checkImportsForType(imports, null, returnType);

				if (apiOperation != null) {
					Class<?> responseType = apiOperation.response();
					checkImportsForType(imports, null, responseType);

					String container = apiOperation.responseContainer();
					switch (container.toLowerCase()) {
						case "list":
							imports.add("java.util.List");
							break;
						case "set":
							imports.add("java.util.Set");
							break;
					}
				}

				for (java.lang.reflect.Parameter param : method
					.getParameters()) {
					if (param.isAnnotationPresent(BeanParam.class)) {
						imports.add("java.util.Map");
					}
					checkImportsForType(imports, apiOperation, param.getType());
				}


			}

			imports.stream().map(
				s -> mappings.computeIfAbsent(s, ClassMapping::fromFQDN))
				   .map(ClassMapping::getFqdn)
				   .map(s -> String.format("import %s;", s))
				   .forEach(pw::println);

			pw.println();

			if (c.isAnnotationPresent(Api.class)) {
				Api api = c.getAnnotation(Api.class);
				if (!api.value().isEmpty()) {
					pw.println("/**");
					pw.print(" * ");
					pw.println(api.value());
					pw.println(" */");
				}
			}

			pw.printf("@Generated(value=\"%s\", date=\"%s\")", getClass().getName(),
					  ZonedDateTime.now().format(
						  DateTimeFormatter.ISO_OFFSET_DATE_TIME));
			pw.println();
			pw.printf("public interface %s%s {", resourceClassPrefix, c.getSimpleName());
			pw.println();

			for (Method method : c.getDeclaredMethods()) {
				String methodSignature = method.getReturnType().getSimpleName() + " " + method
					.getName() + "(" + Arrays.stream(method.getParameterTypes())
											 .map(Class::getSimpleName).collect(Collectors.joining(",")) + ")";

				if (method.isBridge()) {
					getLog().warn(
						"\t\tSKIP " + methodSignature
					);
					continue;
				}

				GET get = method.getAnnotation(GET.class);
				POST post = method.getAnnotation(POST.class);
				PUT put = method.getAnnotation(PUT.class);
				DELETE delete = method.getAnnotation(DELETE.class);
				Path methodPath = method.getAnnotation(Path.class);
				ApiOperation apiOperation = method
					.getAnnotation(ApiOperation.class);

				if (get == null && post == null && put == null && delete == null) {
					// Probably a parent interface, ignore
					getLog().warn(
						"\t\tSKIP " + methodSignature);
					continue;
				} else {
					getLog().info("\t\t" + methodSignature);
				}

				List<String> paramDecls = new ArrayList<>();

				ApiImplicitParams implicitParams = method
					.getAnnotation(ApiImplicitParams.class);
				if (implicitParams != null) {
					for (ApiImplicitParam param : implicitParams.value()) {
						if ("header".equals(param.paramType())
							&& "string".equals(param.dataType())) {
							paramDecls.add(String.format(
								"@Header(\"%s\") String %s", param.name(),
								extractIdentifier(param.name())));
						} else {
							getLog().warn("Unsupport implicit parameter: "
											  + param.toString());
						}
					}
				}

				List<String> javadoc = new LinkedList<>();
				if (apiOperation != null) {
					javadoc.add(apiOperation.value());
				}

				for (java.lang.reflect.Parameter param : method
					.getParameters()) {
					HeaderParam hp = param.getAnnotation(HeaderParam.class);
					QueryParam qp = param.getAnnotation(QueryParam.class);
					PathParam pp = param.getAnnotation(PathParam.class);
					FormParam fp = param.getAnnotation(FormParam.class);
					BeanParam bp = param.getAnnotation(BeanParam.class);
					Context ctx = param.getAnnotation(Context.class);

					if (ctx != null) {
						// Skip context parameters, they have no equivalent in Retrofit
						continue;
					}

					String paramPrefix = "";

					String paramType =
						mappings.computeIfAbsent(param.getType().getName(),
												 ClassMapping::fromFQDN).getNewName();

					String name = param.getName();
					boolean lookForReplacementName = name.matches("arg\\d+");

					if (hp != null) {
						paramPrefix = String.format("@Header(\"%s\")",
													hp.value());
						if (lookForReplacementName) {
							name = sanitize(hp.value());
						}
					} else if (qp != null) {
						paramPrefix = String.format("@Query(\"%s\")",
													qp.value());
						if (lookForReplacementName) {
							name = sanitize(qp.value());
						}
					} else if (pp != null) {
						paramPrefix = String.format("@Path(\"%s\")",
													pp.value());
						if (lookForReplacementName) {
							name = sanitize(pp.value());
						}
					} else if (fp != null) {
						paramPrefix = String.format("@Field(\"%s\")",
													fp.value());
						if (lookForReplacementName) {
							name = sanitize(fp.value());
						}
					} else if (bp != null) {
						if (QueryObject.class.isAssignableFrom(param.getType())) {
							paramPrefix = "@QueryMap";
							paramType = "Map<String,String>";
							if (lookForReplacementName) {
								name = "query";
							}

							javadoc.add(String.format(
								"@param %1$s A map of query parameters: \n\t * \t%2$s\n\t * \t\tPlease use "
									+
									"{@link %3$s#toMap} rather than constructing this map yourself",
								name,
								describeQueryParameters(param.getType()).stream()
																		.collect(Collectors.joining("\n\t * \t")),
								param.getType().getName()));

						} else {
							// TODO: Parse object for other field types and treat as individual parameters
						}
					} else {
						paramPrefix = "@Body";
						if (lookForReplacementName) {
							name = "payload";
						}
					}

					if (!QueryObject.class.isAssignableFrom(param.getType())) {
						if (param.isAnnotationPresent(ApiParam.class)) {
							ApiParam paramDoc = param.getAnnotation(ApiParam.class);
							javadoc.add(String.format("@param %s (%s) %s", name,
													  paramDoc.required() ? "required" : "optional",
													  paramDoc.value()));
						} else if ("@Body".equals(paramPrefix)) {
							if (!param.getType().isAnnotationPresent(ApiModel.class)) {
								throw new MojoFailureException(
									"Parameter " + name + " of " + method.getDeclaringClass()
																		 .getName() + "#" +
										method.getName() + "(" +
										Arrays.stream(method
														  .getParameters())
											  .map(p -> p.getType().getName())
											  .collect(Collectors.joining(","))
										+
										") represents the request body, but the given type has no ApiModel "
										+
										"annotation");
							} else {
								ApiModel model = param.getType().getAnnotation(ApiModel.class);
								javadoc.add(String.format("@param %s (required) %s", name,
														  model.description()));
							}
						} else {
							throw new MojoFailureException(
								"Parameter " + name + " of " + method.getDeclaringClass()
																	 .getName() + "#" +
									method.getName() + "(" +
									Arrays.stream(method
													  .getParameters())
										  .map(p -> p.getType().getName())
										  .collect(Collectors.joining(","))
									+ ") does not have an ApiParam annotation");
						}
					}

					paramDecls.add(String.format("%s %s %s", paramPrefix,
												 paramType, name));

				}

				if (implicitParams != null) {
					for (ApiImplicitParam param : implicitParams.value()) {
						if ("header".equals(param.paramType())
							&& "string".equals(param.dataType())) {
							if (param.required()) {
								javadoc.add(String.format("@param %s (required) %s",
														  extractIdentifier(param.name()), param.value()));
							} else {
								javadoc.add(String.format("@param %s (optional) %s",
														  extractIdentifier(param.name()), param.value()));
							}
						}
					}
				}

				if (paramDecls.isEmpty()) {
					if (post != null) {
						getLog().warn("POST " + prefix + path.value() + " has no convertible parameters");
						continue;
					} else if (put != null) {
						getLog().warn("POST " + prefix + path.value() + " has no convertible parameters");
						continue;
					}

				}

				Class<?> returnType = method.getReturnType();
				String returnTypeName = mappings
					.computeIfAbsent(returnType.getName(), ClassMapping::fromFQDN).newName;

				if (returnTypeName.equals("Response")) {
					if (apiOperation == null) {
						throw new MojoFailureException(
							"Missing @ApiOperation annotation on " + method.getDeclaringClass()
																		   .getName() + "#" +
								method.getName() + "(" +
								Arrays.stream(method
												  .getParameters()).map(p -> p.getType().getName())
									  .collect(Collectors.joining(","))
								+ ")");
					}
					returnTypeName =
						mappings.computeIfAbsent(apiOperation.response().getName(),
												 ClassMapping::fromFQDN)
							.newName;
					switch (apiOperation.responseContainer().toLowerCase()) {
						case "list":
							returnTypeName = String.format("List<%s>", returnTypeName);
							break;
						case "set":
							returnTypeName = String.format("Set<%s>", returnTypeName);
							break;
					}

					javadoc.add(
						serviceMethodOutputType.createJavadoc(apiOperation, returnTypeName));
					returnTypeName = String
						.format("%s<%s>", serviceMethodOutputType.getReturnType(),
								returnTypeName);

				} else {
					javadoc.add("@return An object of type " + returnTypeName);
				}

				String postFix = methodPath != null ? methodPath.value() : "";

				String exactPath = prefix.concat(postFix);

				if (!javadoc.isEmpty()) {
					pw.println("\t/**");
					javadoc.forEach(jd -> {
						pw.print("\t * ");
						pw.print(jd);
						pw.println();
					});
					pw.println("\t */");

				}

				if (get != null) {
					pw.print("\t@GET(\"");
					pw.print(exactPath);
					pw.println("\")");
				}

				if (post != null) {
					pw.print("\t@POST(\"");
					pw.print(exactPath);
					pw.println("\")");
				}

				if (put != null) {
					pw.print("\t@PUT(\"");
					pw.print(exactPath);
					pw.println("\")");
				}

				if (delete != null) {
					pw.print("\t@DELETE(\"");
					pw.print(exactPath);
					pw.println("\")");
				}

				if (Arrays.stream(method.getParameters()).anyMatch(
					p -> p.isAnnotationPresent(FormParam.class))) {
					pw.println("\t@FormUrlEncoded");
				}

				pw.print("\t");
				pw.print(returnTypeName);
				pw.print(" ");
				pw.print(method.getName());
				pw.print("(");

				pw.print(paramDecls.stream().collect(Collectors.joining(", ")));

				pw.println(");");
				pw.println();
			}

			pw.println("}");

			pw.flush();
		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage(), e);
		} finally {
			if (buildContext != null) {
				buildContext.refresh(target);
			}
			if (project != null) {
				project.addCompileSourceRoot(
					"target/generated-sources/retrofit");
			}
		}

	}

	private List<String> describeQueryParameters(Class<?> type) {
		final String delimiter = "<br />\n\t * \t\t\t\t\t";

		List<String> result = new LinkedList<>();
		result.add("\t<table>");
		result.add("\t\t<tr><th>Field</th><th>Accepted expressions</th></tr>");

		for (Field f : type.getDeclaredFields()) {
			if (f.isAnnotationPresent(ApiParam.class)) {
				String acceptableValues = "";
				if (IBooleanProperty.class.isAssignableFrom(f.getType())) {
					acceptableValues = Stream.of(
						"{@code true}", "{@code false}", "{@code !true}", "{@code !false}",
						"{@code #}(equal to null)", "{@code !#} (not equal to null)")
											 .collect(Collectors.joining("" + Character.LINE_SEPARATOR));
				} else if (IComparableProperty.class.isAssignableFrom(f.getType())) {
					acceptableValues = Stream.of(
						"{@code =value} (equal to)", "{@code !=value} (not equal to value)",
						"{@code >value} (greater than)",
						"{@code !>value} (not greater than)",
						"{@code >=value} (greater than or equal to)",
						"{@code !>=value} (not greater than or equal to)",
						"{@code <value} (less than)",
						"{@code !<value} (not less than)",
						"{@code <=value} (less than or equal to)",
						"{@code !<=value} (not less than or equal to)",
						"{@code [value,value]} (between)",
						"{@code ![value,value]} (not between)",
						"{@code #}(equal to null)", "{@code !#} (not equal to null)"
					)
											 .collect(Collectors.joining(delimiter));
				} else if (IStringProperty.class.isAssignableFrom(f.getType())) {
					acceptableValues = Stream.of(
						"{@code =value} (equal to value)",
						"{@code !=value} (not equal to value)",
						"{@code _=value} (equal to value, ignoring case)",
						"{@code !_=value} (not equal value, ignoring case)",
						"{@code ~value} (like value)",
						"{@code !~value} (not like value)",
						"{@code _~value} (like value, ignoring case)",
						"{@code !_~value} (not like value, ignoring case)",
						"{@code #} (equal to null)",
						"{@code !#} (not equal to null)")
											 .collect(Collectors.joining(delimiter));
				}

				result.add(String.format("\t\t<tr><td>%1$s</td><td>%2$s</td></tr>", f.getName(),
										 acceptableValues));
			}
		}

		result.add("\t</table>");

		return result;
	}

	private String extractIdentifier(String name) {
		StringBuilder builder = new StringBuilder(name.length());
		for (char c : name.toCharArray()) {
			if (builder.length() == 0) {
				if (Character.isJavaIdentifierStart(c)) {
					builder.append(Character.toLowerCase(c));
				}
			} else {
				if (Character.isJavaIdentifierPart(c)) {
					builder.append(c);
				}
			}
		}

		return builder.toString();
	}

	private String sanitize(String value) {
		StringBuilder result = new StringBuilder();
		for (char c : value.toCharArray()) {
			if (Character.isJavaIdentifierPart(c)) {
				if (result.length() == 0) {
					result.append(Character.toLowerCase(c));
				} else {
					result.append(c);
				}
			}
		}

		return result.toString();
	}

	private void checkImportsForType(Set<String> imports,
									 ApiOperation operation, Class<?> targetType) {
		String returnTypeName = targetType.getSimpleName();

		if (!targetType.isPrimitive()) {
			if (returnTypeName.equals("Response")) {
				imports.add(serviceMethodOutputType.getImport());
				if (operation != null && operation.response() != null) {
					imports.add(operation.response().getName());
				}
			} else {
				if (targetType.getName().startsWith("javax.")) {
					return;
				}

				if (targetType.getName().startsWith("java.lang")) {
					return;
				}

				imports.add(targetType.getName());
			}
		}
	}

	private File createPackage(File parent, String packageName)
		throws MojoExecutionException {
		File base = parent;

		String[] packages = packageName.split("\\.");

		for (String pkg : packages) {
			base = create(new File(base, pkg));
		}

		return base;
	}

	private File create(File file) throws MojoExecutionException {
		if (file.exists() && !file.isDirectory()) {
			throw new MojoExecutionException(String
												 .format("File %s is not a directory", file.getName()));
		}

		if (!file.exists() && !file.mkdir()) {
			throw new MojoExecutionException(String
												 .format("Could not create directory %s", file.getName()));
		}

		return file;
	}

	private static class ClassMapping {

		private final String newPackage;

		private final String newName;

		private final String fqdn;

		private ClassMapping(String newPackage, String newName) {
			this.newPackage = newPackage;
			this.newName = newName;
			this.fqdn = newPackage.isEmpty() ? newName : newPackage.concat(".").concat(newName);
		}

		public String getNewPackage() {
			return newPackage;
		}

		public String getNewName() {
			return newName;
		}

		public String getFqdn() {
			return fqdn;
		}

		public static ClassMapping fromFQDN(String fqdn) {
			int sep = fqdn.lastIndexOf('.');

			if (sep == -1) {
				return new ClassMapping("", fqdn);
			} else {
				String pkg = fqdn.substring(0, sep);
				String name = fqdn.substring(sep + 1);

				return new ClassMapping(pkg, name);
			}
		}
	}
}
