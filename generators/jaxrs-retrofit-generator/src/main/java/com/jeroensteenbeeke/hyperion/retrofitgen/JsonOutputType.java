package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.google.common.collect.ImmutableSet;
import io.swagger.annotations.ApiModelProperty;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Set;

/**
 * Enum for the type of annotations to use for annotating model classes
 */
public enum JsonOutputType {
	/**
	 * Use GSON annotations
	 */
	GSON {
		@Override
		public Set<String> getAdditionalImports() {
			return ImmutableSet.of("com.google.gson.annotations.SerializedName");
		}

		@Override
		public void annotateField(Field field, PrintWriter printWriter) {
			String name = field.getName();

			ApiModelProperty prop = field.getAnnotation(ApiModelProperty.class);

			if (prop != null) {
				if (!"".equals(prop.name())) {
					name = prop.name();
				}
 			}
			printWriter.printf("\t@SerializedName(\"%s\")", name);
			printWriter.println();

		}
	},
	/**
	 * Use Jackson annotations
	 */
	JACKSON {
		@Override
		public Set<String> getAdditionalImports() {
			return ImmutableSet.of("com.fasterxml.jackson.annotation.JsonProperty");
		}

		@Override
		public void annotateField(Field field, PrintWriter printWriter) {

		}
	};

	/**
	 * Returns a set of additional import statements that should be added to the generated file
	 * @return A set of fully qualified domain names
	 */
	public abstract Set<String> getAdditionalImports();

	/**
	 * Adds annotations to the given field
	 * @param field The field to annotate
	 * @param printWriter The printwriter being used to generate the class
	 */
	public abstract void annotateField(Field field, PrintWriter printWriter);
}
