package com.jeroensteenbeeke.hyperion.retrofitgen;

import io.swagger.annotations.ApiOperation;

/**
 * The type of object returned by service methods. Allows project to choose between regular Retrofit/OkHttp or RXJava
 */
public enum ServiceMethodOutputType {
	/**
	 * Retrofit Call
	 */
	CALL {
		@Override
		public String getReturnType() {
			return "Call";
		}

		@Override
		public String getImport() {
			return "import retrofit2.Call";
		}

		@Override
		public String createJavadoc(ApiOperation apiOperation, String returnTypeName) {
			return String.format("@return A Call object encapsulating the %s return type", returnTypeName);
		}
	},
	/**
	 * RXJava observable
	 */
	OBSERVABLE {
		@Override
		public String getReturnType() {
			return "Observable";
		}

		@Override
		public String getImport() {
			return "io.reactivex.Observable";
		}


		@Override
		public String createJavadoc(ApiOperation apiOperation, String returnTypeName) {
			return String.format("@return An RxJava Observable that will eventually yield a %s", returnTypeName);
		}

	};

	/**
	 * The return type of the method
	 * @return An identifier
	 */
	public abstract String getReturnType();

	/**
	 * The import required for this output type
	 * @return A fully qualified domain name
	 */
	public abstract String getImport();

	/**
	 * Generates JavaDoc for the given return type
	 * @param apiOperation The ApiOperation annotation
	 * @param returnTypeName The type returned by the current implementation
	 * @return A snippet of Javadoc
	 */
	public abstract String createJavadoc(ApiOperation apiOperation, String returnTypeName);
}
