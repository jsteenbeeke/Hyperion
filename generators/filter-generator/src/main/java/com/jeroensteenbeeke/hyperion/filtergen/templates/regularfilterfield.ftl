    private ${fieldType}<${entityType},${propertyType},${filterType}> ${propertyName}
        = new ${fieldType}<>(${entityType}_.${fieldName}, this);
