    private ${fieldType}<${entityType},${propertyType},${filterType}, ${propertyType}Filter> ${propertyName}
        = new ${fieldType}<>(${entityType}_.${fieldName}, this, ${propertyType}_.id);
