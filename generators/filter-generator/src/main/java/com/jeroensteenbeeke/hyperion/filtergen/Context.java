package com.jeroensteenbeeke.hyperion.filtergen;

import javax.annotation.processing.ProcessingEnvironment;

class Context {
	private final ProcessingEnvironment environment;

	public Context(ProcessingEnvironment environment) {
		super();
		this.environment = environment;
	}
	
	public ProcessingEnvironment getEnvironment() {
		return environment;
	}
	
	
}
