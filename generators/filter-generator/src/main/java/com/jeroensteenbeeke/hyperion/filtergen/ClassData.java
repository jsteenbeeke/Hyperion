package com.jeroensteenbeeke.hyperion.filtergen;

import java.util.List;

class ClassData {
	private final String packageName;
	
	private final String className;
	
	private final List<FieldData> fields;

	

	public ClassData(String packageName, String className, List<FieldData> fields) {
		super();
		this.packageName = packageName;
		this.className = className;
		this.fields = fields;
	}

	public String getPackageName() {
		return packageName;
	}

	public String getClassName() {
		return className;
	}
	
	public List<FieldData> getFields() {
		return fields;
	}
	
	public String getFQDN() {
		return String.format("%s.%s", packageName, className);
	}
	
	
}
