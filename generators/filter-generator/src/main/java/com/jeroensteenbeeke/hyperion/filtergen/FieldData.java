package com.jeroensteenbeeke.hyperion.filtergen;

import java.util.Set;

class FieldData {
	private final String filterType;
	
	private final String propertyType;
	
	private final String classType;
	
	private final String attributeName;

	private final boolean generified;

	private final boolean entity;
	
	private final String fieldName;
	
	private final Set<String> imports;

	public FieldData(boolean generified,
					 boolean entity,
					 String filterType,
					 String propertyType,
					 String classType,
					 String attributeName,
					 String fieldName,
					 Set<String> imports) {
		this.generified = generified;
		this.entity = entity;
		this.filterType = filterType;
		this.propertyType = propertyType;
		this.classType = classType;
		this.fieldName = fieldName;
		this.attributeName = attributeName;
		this.imports = imports;
	}
	
	public Set<String> getImports() {
		return imports;
	}

	public String getFilterType() {
		return filterType;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public String getClassType() {
		return classType;
	}

	public String getFieldName() {
		return fieldName;
	}
	
	public String getAttributeName() {
		return attributeName;
	}
	
	public boolean isGenerified() {
		return generified;
	}

	public boolean isEntity() {
		return entity;
	}
}
