
    public ${fieldType}<${entityType},${propertyType},${filterType}> ${propertyName}() {
        return setLastUserFilterField(this.${fieldName});
    }

    public ${filterType} ${propertyName}(${propertyType} value) {
        return setLastUserFilterField(this.${fieldName}).equalTo(value);
    }

    public ${fieldType}<${entityType},${propertyType},${filterType}> or${propertyName?cap_first}() {
        return disjunction(this.${fieldName}.createNew());
    }

    public ${filterType} or${propertyName?cap_first}(${propertyType} value) {
        return disjunction(this.${fieldName}.createNew()).equalTo(value);
    }

    public ${fieldType}<${entityType},${propertyType},${filterType}> and${propertyName?cap_first}() {
        return conjunction(this.${fieldName}.createNew());
    }

    public ${filterType} and${propertyName?cap_first}(${propertyType} value) {
        return conjunction(this.${fieldName}.createNew()).equalTo(value);
    }

