
    public ${fieldType}<${entityType},${propertyType},${filterType}, ${propertyType}Filter> ${propertyName}() {
        return setLastUserFilterField(this.${fieldName});
    }

    public ${filterType} ${propertyName}(${propertyType} value) {
        return setLastUserFilterField(this.${fieldName}).equalTo(value);
    }

    public ${filterType} ${propertyName}(${propertyType}Filter filter) {
        return setLastUserFilterField(this.${fieldName}).byFilter(filter);
    }

    public ${fieldType}<${entityType},${propertyType},${filterType}, ${propertyType}Filter> or${propertyName?cap_first}() {
        return disjunction(this.${fieldName}.createNew());
    }

    public ${filterType} or${propertyName?cap_first}(${propertyType} value) {
        return disjunction(this.${fieldName}.createNew()).equalTo(value);
    }

    public ${filterType} or${propertyName?cap_first}(${propertyType}Filter filter) {
        return disjunction(this.${fieldName}.createNew()).byFilter(filter);
    }

    public ${fieldType}<${entityType},${propertyType},${filterType}, ${propertyType}Filter> and${propertyName?cap_first}() {
        return conjunction(this.${fieldName}.createNew());
    }

    public ${filterType} and${propertyName?cap_first}(${propertyType} value) {
        return conjunction(this.${fieldName}.createNew()).equalTo(value);
    }

    public ${filterType} and${propertyName?cap_first}(${propertyType}Filter filter) {
        return conjunction(this.${fieldName}.createNew()).byFilter(filter);
    }
