package com.jeroensteenbeeke.hyperion.filtergen;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.persistence.Entity;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.apt.core.HyperionProcessor;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Annotation processor for generating search filter classes for entities
 */
@SupportedAnnotationTypes("javax.persistence.Entity")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class SearchFilterGenerator extends HyperionProcessor {
	private static final String ENTITY = "javax.persistence.Entity";

	private static final String MAPPED_SUPERCLASS = "javax.persistence.MappedSuperclass";

	private static final String ONE_TO_ONE = "javax.persistence.OneToOne";

	private static final String MANY_TO_ONE = "javax.persistence.ManyToOne";

	private static final String COLUMN = "javax.persistence.Column";

	private static final String CUSTOM_FILTER_ANNOTATION =
			"com.jeroensteenbeeke.hyperion.meld.filter.CustomFilter";

	private static final String PROPERTY_NAME = "name";

	private static final String PROPERTY_CLASS = "type";

	private static final String CUSTOM_FILTERS_ANNOTATION =
			"com.jeroensteenbeeke.hyperion.meld.filter.CustomFilters";

	private static final String ID = "javax.persistence.Id";

	private static final String STRING_TYPE = "String";

	private static final Set<String> COMPARABLES = ImmutableSet
			.of("BigDecimal", "BigInteger", "Date", "Integer", "Long", "Short", "LocalDate", "LocalDateTime", "ZonedDateTime");

	private static final Set<String> BLACKLISTED = ImmutableSet.of("Blob");

	private Context context;

	private Messager messager;

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		if (context == null) {
			this.context = new Context(processingEnv);
		}

		this.messager = processingEnv.getMessager();

		processingEnv.getMessager().printMessage(Kind.NOTE, "Initialized SearchFilterGenerator");

		super.init(processingEnv);
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		roundEnv.getElementsAnnotatedWith(Entity.class).stream().map(this::analyzeElement)
				.filter(Optional::isPresent)
				.map(Optional::get).forEach(this::generateClass);

		return false;
	}

	private void generateClass(ClassData data) {


		try {
			final String targetPackage = data.getPackageName().concat(".filter");
			final String targetClassName = data.getClassName().concat("Filter");
			final String fqdn = String.format("%s.%s", targetPackage, targetClassName);

			messager.printMessage(Kind.NOTE, "Generating "+ fqdn);

			JavaFileObject classFile = context.getEnvironment().getFiler().createSourceFile(fqdn);
			Set<String> imports = Sets.newHashSet();
			imports.add("com.jeroensteenbeeke.hyperion.meld.SearchFilter");
			imports.add("javax.annotation.Generated");
			imports.add(data.getFQDN());
			imports.add(String.format("%s_", data.getFQDN()));
			data.getFields().stream().map(FieldData::getImports).flatMap(Set::stream)
					.filter(s -> !s.startsWith("java.lang.") || s.substring(10).contains("."))
					.forEach(imports::add);

			Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
			configuration.setClassForTemplateLoading(SearchFilterGenerator.class, "templates");
			configuration.setDefaultEncoding("UTF-8");

			try (PrintWriter pw = new PrintWriter(classFile.openWriter())) {
				pw.print("package ");
				pw.print(targetPackage);
				pw.println(";");
				pw.println();
				imports.forEach(i -> {
					if (i.indexOf('.') == -1) {
						throw new IllegalArgumentException("Default package imports not allowed");
					}
					pw.print("import ");
					pw.print(i);
					pw.println(";");
				});
				pw.println();
				pw.printf(
						"@Generated(value = \"com.jeroensteenbeeke.hyperion.filtergen" +
								".SearchFilterGenerator\",\n date= \"%s\")",
						new Date().toString());
				pw.println();
				pw.print("public class ");
				pw.print(targetClassName);
				pw.print(" extends SearchFilter <");
				pw.print(data.getClassName());
				pw.print(",");
				pw.print(targetClassName);
				pw.println("> {");
				pw.println();
				pw.println("\tprivate static final long serialVersionUID = 1L;");
				pw.println();

				// Fields
				data.getFields().forEach(field -> {
					final String propertyType = field.getPropertyType();
					final String filterType = field.getFilterType();
					final String classType = field.getClassType();
					final String fieldName = field.getFieldName();
					final String attributeName = field.getAttributeName();

					Map<String, Object> model = new HashMap<>();
					model.put("propertyType", propertyType);
					model.put("propertyName", attributeName);
					model.put("entityType", classType);
					model.put("fieldType", filterType);
					model.put("filterType", targetClassName);
					model.put("fieldName", fieldName);
					model.put("isEntity", field.isEntity());


					if (field.isEntity()) {
						try {
							Template template = configuration.getTemplate("entityfilterfield.ftl");

							template.process(model, pw);

						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					} else {
						try {
							Template template = configuration.getTemplate("regularfilterfield.ftl");

							template.process(model, pw);

						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					}

				});

				// Setters
				data.getFields().forEach(field -> {
					final String propertyType = field.getPropertyType();
					final String filterType = field.getFilterType();
					final String classType = field.getClassType();
					final boolean generified = field.isGenerified();
					final String attributeName = field.getAttributeName();
					final String fieldName = field.getFieldName();

					Map<String, Object> model = new HashMap<>();
					model.put("propertyType", propertyType);
					model.put("propertyName", attributeName);
					model.put("entityType", classType);
					model.put("fieldType", filterType);
					model.put("filterType", targetClassName);
					model.put("fieldName", fieldName);
					model.put("isEntity", field.isEntity());

					if (field.isEntity()) {
						try {
							Template template = configuration.getTemplate("entityaccessors.ftl");

							template.process(model, pw);

						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					} else {
						try {
							Template template = configuration.getTemplate("regularaccessors.ftl");

							template.process(model, pw);

						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					}


				});

				pw.println("}");
				pw.flush();

			}

			context.getEnvironment().getMessager()
					.printMessage(Kind.NOTE, "Generated " + classFile.getName());

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Optional<ClassData> analyzeElement(Element e) {
		if (e.getKind() != ElementKind.CLASS) {
			return Optional.empty();
		}

		final String className = e.getSimpleName().toString();

		TypeElement type = ((TypeElement) e);

		final String packageName = extractPackageName(type.getQualifiedName().toString());

		ImmutableList.Builder<FieldData> fields = ImmutableList.builder();

		List<Element> toExplore = Lists.newArrayList(e.getEnclosedElements());

		TypeMirror superType = type.getSuperclass();

		while (superType != null) {
			if (superType instanceof DeclaredType) {
				DeclaredType dt = (DeclaredType) superType;

				Element element = dt.asElement();

				if (element instanceof TypeElement &&
						containsAnnotation(element, MAPPED_SUPERCLASS, ENTITY)) {
					TypeElement t = (TypeElement) element;

					toExplore.addAll(t.getEnclosedElements());

					superType = t.getSuperclass();
				} else {
					break;
				}
			} else {
				break;
			}

		}

		analyzeFields(className, fields, toExplore);

		return Optional.of(new ClassData(packageName, className, fields.build()));

	}

	private void analyzeFields(final String className, ImmutableList.Builder<FieldData> fields,
							   List<Element> toExplore) {
		for (Element child : toExplore) {
			if (child.getKind() == ElementKind.FIELD
					&& containsAnnotation(child, CUSTOM_FILTER_ANNOTATION,
					CUSTOM_FILTERS_ANNOTATION)) {
				VariableElement field = (VariableElement) child;

				List<AnnotationMirror> mirror = new LinkedList<>();

				getAnnotationMirror(field, CUSTOM_FILTER_ANNOTATION).ifPresent(mirror::add);
				getAnnotationMirror(field, CUSTOM_FILTERS_ANNOTATION).ifPresent(am -> {
					context.getEnvironment().getMessager().printMessage(Kind.NOTE,
							"Multi filter annotation: " + field.getSimpleName().toString());
					am.getElementValues().forEach((k, v) -> {
						context.getEnvironment().getMessager().printMessage(Kind.NOTE,
								"\tsub annotation: " + k.getSimpleName().toString());

						if (k.getSimpleName().toString().equals("value")) {
							@SuppressWarnings("unchecked")
							List<AnnotationMirror> mirrors = (List<AnnotationMirror>) v.getValue();
							mirror.addAll(mirrors);
						}
					});
				});

				for (AnnotationMirror custom : mirror) {
					AtomicReference<String> name = new AtomicReference<String>(null);
					AtomicReference<String> filterClass = new AtomicReference<>(null);

					custom.getElementValues().forEach((k, v) -> {
						switch (k.getSimpleName().toString()) {
							case PROPERTY_NAME:
								name.set((String) v.getValue());
								break;
							case PROPERTY_CLASS:
								filterClass.set(v.getValue().toString());
								break;
						}
					});

					if (name.get() != null && filterClass.get() != null) {
						fields.add(
								new FieldData(false, false, filterClass.get(), null, className,
										name
												.get(),
										field.getSimpleName().toString(), ImmutableSet.of()));
					}
				}

			}
			if (child.getKind() == ElementKind.FIELD
					&& containsAnnotation(child, COLUMN, MANY_TO_ONE, ONE_TO_ONE, ID)) {

				ImmutableSet.Builder<String> imports = ImmutableSet.builder();
				VariableElement field = (VariableElement) child;

				TypeMirror declaredType = field.asType();

				String filterType = "SimpleFilterField";
				String propertyType = null;
				if (declaredType instanceof ArrayType) {
					// Array of something, most likely bytes. These are not
					// included in filters
					continue;
				} else if (declaredType instanceof DeclaredType) {
					// Not an array, could be an entity, enum, string or boxed
					// primitive
					DeclaredType ref = (DeclaredType) declaredType;

					TypeElement t = (TypeElement) ref.asElement();

					propertyType = t.getSimpleName().toString();
					imports.add(t.getQualifiedName().toString());

					if (BLACKLISTED.contains(propertyType)) {
						// This is a type that is by definition unqueryable
						continue;
					} else if (STRING_TYPE.equals(propertyType)) {
						filterType = "StringFilterField";
					} else if (COMPARABLES.contains(propertyType)) {
						filterType = "ComparableFilterField";
					}

				} else if (declaredType instanceof PrimitiveType) {
					// Primitive type
					PrimitiveType prim = (PrimitiveType) declaredType;
					filterType = "ComparableFilterField";
					switch (prim.getKind()) {
						case BOOLEAN:
							filterType = "SimpleFilterField";
							propertyType = "Boolean";
							break;
						case BYTE:
							propertyType = "Byte";
							break;
						case CHAR:
							propertyType = "Character";
							break;
						case DOUBLE:
							propertyType = "Double";
							break;
						case FLOAT:
							propertyType = "Float";
							break;
						case INT:
							propertyType = "Integer";
							break;
						case LONG:
							propertyType = "Long";
							break;
						case SHORT:
							propertyType = "Short";
							break;
						default:
							// Should not occur, go for Object
							propertyType = "Object";
					}
				}

				final String fieldName = field.getSimpleName().toString();

				Optional<? extends AnnotationMirror> manyToOne =
						getAnnotationMirror(field, MANY_TO_ONE);
				Optional<? extends AnnotationMirror> oneToOne =
						getAnnotationMirror(field, ONE_TO_ONE);
				Optional<? extends AnnotationMirror> column = getAnnotationMirror(field, COLUMN);
				Optional<? extends AnnotationMirror> id = getAnnotationMirror(field, ID);

				boolean entity = false;

				if (manyToOne.isPresent()) {

					entity = true;
				} else if (oneToOne.isPresent()) {

					entity = true;
				} else if (!column.isPresent() && !id.isPresent()) {
					continue;
				}

				if (entity) {
					filterType = "EntityFilterField";
					if (declaredType instanceof DeclaredType) {
						DeclaredType ref = (DeclaredType) declaredType;

						TypeElement t = (TypeElement) ref.asElement();

						imports.add(t.getQualifiedName().toString().concat("_"));
						determineEntityFilter(t).ifPresent(imports::add);
					}
				}

				imports.add(
						String.format("com.jeroensteenbeeke.hyperion.meld.filter.%s", filterType));

				fields.add(new FieldData(true, entity, filterType, propertyType, className,
						fieldName,
						fieldName,
						imports.build()));

			}
		}
	}

	private Optional<String> determineEntityFilter(TypeElement t) {
		String fqdn = t.getQualifiedName().toString();

		int lastDot = fqdn.lastIndexOf('.');
		if (lastDot != -1) {
			String pkg = fqdn.substring(0, lastDot);
			String className = fqdn.substring(lastDot + 1);

			return Optional.of(String.format("%s.filter.%sFilter", pkg, className));
		} else {
			return Optional.empty();
		}

	}


}
