package com.jeroensteenbeeke.hyperion.filtergen.testdata;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class BarBar extends Bar {
	private static final long serialVersionUID = 1L;

	@Column
	private boolean booleanVar;

	@Column
	private byte byteVal;

	@Column
	private char charVal;

	@Column
	private double doubleVal;

	@Column
	private float floatVal;

	@Column
	private int intVal;

	@Column
	private short shortVal;

	public boolean isBooleanVar() {
		return booleanVar;
	}

	public void setBooleanVar(boolean booleanVar) {
		this.booleanVar = booleanVar;
	}

	public byte getByteVal() {
		return byteVal;
	}

	public void setByteVal(byte byteVal) {
		this.byteVal = byteVal;
	}

	public char getCharVal() {
		return charVal;
	}

	public void setCharVal(char charVal) {
		this.charVal = charVal;
	}

	public double getDoubleVal() {
		return doubleVal;
	}

	public void setDoubleVal(double doubleVal) {
		this.doubleVal = doubleVal;
	}

	public float getFloatVal() {
		return floatVal;
	}

	public void setFloatVal(float floatVal) {
		this.floatVal = floatVal;
	}

	public int getIntVal() {
		return intVal;
	}

	public void setIntVal(int intVal) {
		this.intVal = intVal;
	}

	public short getShortVal() {
		return shortVal;
	}

	public void setShortVal(short shortVal) {
		this.shortVal = shortVal;
	}
}
