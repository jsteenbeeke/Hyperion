package com.jeroensteenbeeke.hyperion.filtergen.testdata;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.meld.filter.CustomFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.CustomFilters;

@Entity
public class Foo extends BaseDomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Serializable getDomainObjectId() {
		return getId();
	}


}
