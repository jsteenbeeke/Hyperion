package com.jeroensteenbeeke.hyperion.filtergen.testdata;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.meld.filter.CustomFilter;

@Entity
public class Bar extends BaseDomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private long countables;

	@Column
	private byte[] picture;

	@Column
	@Lob
	private Blob bigPicture;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public Serializable getDomainObjectId() {
		return getId();
	}
	
	public long getCountables() {
		return countables;
	}
	
	public void setCountables(long countables) {
		this.countables = countables;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public Blob getBigPicture() {
		return bigPicture;
	}

	public void setBigPicture(Blob bigPicture) {
		this.bigPicture = bigPicture;
	}
}
