package com.jeroensteenbeeke.hyperion.querygen;

import com.google.common.base.Joiner;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import io.swagger.annotations.ApiModel;
import org.junit.Test;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.tools.JavaFileObject;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;

public class QueryObjectGeneratorTest {
	@Test
	public void testQueryObjectGenerator() {
		JavaFileObject file =
				JavaFileObjects.forSourceLines("com.jeroensteenbeeke.hyperion.querygen.TestObject",
						"package com.jeroensteenbeeke.hyperion.querygen;",
						"",
						"import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;",
						"import io.swagger.annotations.ApiModel;",
						"import io.swagger.annotations.ApiModelProperty;",
						"",
						"@ApiModel",
						"public class TestObject {",
						"\t@Queryable",
						"\t@ApiModelProperty(value=\"A value for testing purposes\")",
						"\tprivate String testValue;",
						"}");


		ClassLoader loader = ApiModel.class.getClassLoader();
		System.out.println(getClasspathFromClassloader(loader));

		Compilation compilation = javac().withOptions("-verbose", "-cp", System.getProperty("java" +
				".class.path"))
										 .withProcessors(new TestQueryObjectGenerator())
										 .compile
												 (file);
		assertThat(compilation).succeeded();
		assertThat(compilation)
				.generatedSourceFile(
						"com.jeroensteenbeeke.hyperion.querygen.query.TestObject_Query")
				.hasSourceEquivalentTo(JavaFileObjects
						.forSourceLines(
								"com.jeroensteenbeeke.hyperion.querygen.query.TestObject_Query",
								"package com.jeroensteenbeeke.hyperion.querygen.query;",
								"",
								"import com.jeroensteenbeeke.hyperion.querygen.TestObject;",
								"import com.jeroensteenbeeke.hyperion.rest.query.StringProperty;",
								"import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;",
								"import io.swagger.annotations.*;",
								"import java.io.Serializable;",
								"import java.util.LinkedList;",
								"import java.util.List;",
								"import javax.annotation.Generated;",
								"import javax.ws.rs.QueryParam;",
								"",
								"@Generated(value = \"com.jeroensteenbeeke.hyperion.querygen" +
										".QueryObjectGenerator\",",
								"\tdate= \"now\")",
								"public class TestObject_Query implements " +
										"QueryObject<TestObject>," +
										" Serializable {",
								"\tprivate static final long serialVersionUID = 1L;",
								"",
								"\tprivate int nextSortIndex;",
								"",
								"\t@ApiParam(name=\"testValue\", value=\"A value for testing " +
										"purposes\", examples=@Example({",
								"\t\t@ExampleProperty(value=\"=value\"),",
								"\t\t@ExampleProperty(value=\"!=value\"),",
								"\t\t@ExampleProperty(value=\"_=value\"),",
								"\t\t@ExampleProperty(value=\"!_=value\"),",
								"\t\t@ExampleProperty(value=\"~value\"),",
								"\t\t@ExampleProperty(value=\"!~value\"),",
								"\t\t@ExampleProperty(value=\"_~value\"),",
								"\t\t@ExampleProperty(value=\"!_~value\"),",
								"\t\t@ExampleProperty(value=\"#\"),",
								"\t\t@ExampleProperty(value=\"!#\")",
								"\t}), example=\"=value, !=value, _=value, !_=value, ~value, " +
										"!~value, _~value, !_~value, #, !#\")",
								"\t@QueryParam(\"testValue\")",
								"\tprivate StringProperty<TestObject_Query> testValue = new " +
										"StringProperty<>(this, \"testValue\");",
								"",
								"\tpublic StringProperty<TestObject_Query> testValue() {",
								"\t\treturn this.testValue;",
								"\t}",
								"",
								"\t@Override",
								"\tpublic int getNextSortIndex() {",
								"\t\treturn nextSortIndex++;",
								"\t}",
								"",
								"\t@Override",
								"\tpublic boolean matches(TestObject object) {",
								"\t\treturn testValue.appliesTo(object);",
								"\t}",

								"}"

						));

	}

	@SupportedAnnotationTypes("com.jeroensteenbeeke.hyperion.rest.annotations.Queryable")
	@SupportedSourceVersion(SourceVersion.RELEASE_8)
	private static class TestQueryObjectGenerator extends QueryObjectGenerator {
		@Override
		protected String getDateString() {
			return "now";
		}
	}

	private static String getClasspathFromClassloader(ClassLoader currentClassloader) {
		ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

		for (ArrayList<ClassLoader> classloaders = new ArrayList<>(); currentClassloader != null;
				currentClassloader = currentClassloader.getParent()) {
			classloaders.add(currentClassloader);
			if (currentClassloader == systemClassLoader) {
				Set<String> classpaths = new LinkedHashSet<>();
				Iterator var4 = classloaders.iterator();

				while (var4.hasNext()) {
					ClassLoader classLoader = (ClassLoader) var4.next();
					if (classLoader instanceof URLClassLoader) {
						URLClassLoader ucl = (URLClassLoader) classLoader;
						URL[] var6 = ucl.getURLs();
						int var7 = var6.length;

						for (int var8 = 0; var8 < var7; ++var8) {
							URL url = var6[var8];
							if (!url.getProtocol().equals("file")) {
								throw new IllegalArgumentException(
										"Given classloader consists of classpaths which are " +
												"unsupported for compilation.");
							}

							classpaths.add(url.getPath());
						}
					}
				}

				return Joiner.on("\n").join(classpaths);
			}
		}

		throw new IllegalArgumentException(
				"Classpath for compilation could not be extracted since given classloader is not " +
						"an instance of URLClassloader");
	}
}
