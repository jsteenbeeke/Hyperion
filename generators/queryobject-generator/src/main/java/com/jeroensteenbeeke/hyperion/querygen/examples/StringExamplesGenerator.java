package com.jeroensteenbeeke.hyperion.querygen.examples;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Example generator for StringProperty
 */
public class StringExamplesGenerator implements ExamplesGenerator {
	@Override
	public List<String> generateExamples() {
		return ImmutableList.of("=value", "!=value", "_=value", "!_=value", "~value", "!~value", "_~value", "!_~value", "#", "!#");
	}
}
