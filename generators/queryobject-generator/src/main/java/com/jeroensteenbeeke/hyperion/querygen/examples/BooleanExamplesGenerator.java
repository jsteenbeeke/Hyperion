package com.jeroensteenbeeke.hyperion.querygen.examples;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Example generator for BooleanProperty
 */
public class BooleanExamplesGenerator implements ExamplesGenerator {
	@Override
	public List<String> generateExamples() {
		return ImmutableList.of("true", "false", "!true", "!false", "#", "!#");
	}
}
