package com.jeroensteenbeeke.hyperion.querygen.examples;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Example generator for subclasses of ComparableProperty
 */
public class ComparableExamplesGenerator implements ExamplesGenerator {
	private final String primary;

	private final String secondary;

	/**
	 * Create a new examples generator for comparable properties, with the given example primary and secondary values
	 * @param primary String representation of the primary value to use in examples
	 * @param secondary String representation of the secondary value to use in examples
	 */
	public ComparableExamplesGenerator(String primary, String secondary) {
		this.primary = primary;
		this.secondary = secondary;
	}

	@Override
	public List<String> generateExamples() {
		return Stream.of("=$1","!=$1",">$1","!>$1",">=$1","!>=$1","<$1","!<$1","<=$1","!<=$1","[$1,$2]","![$1,$2]","#","!#"
				
		).map(s -> s.replace("$1", primary)).map(s -> s.replace("$2", secondary)).collect(Collectors.toList());


	}
}
