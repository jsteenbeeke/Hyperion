package com.jeroensteenbeeke.hyperion.querygen;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.jeroensteenbeeke.hyperion.apt.core.HyperionProcessor;
import com.jeroensteenbeeke.hyperion.querygen.examples.BooleanExamplesGenerator;
import com.jeroensteenbeeke.hyperion.querygen.examples.ComparableExamplesGenerator;
import com.jeroensteenbeeke.hyperion.querygen.examples.ExamplesGenerator;
import com.jeroensteenbeeke.hyperion.querygen.examples.StringExamplesGenerator;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Annotation processor that creates query objects
 */
@SupportedAnnotationTypes("com.jeroensteenbeeke.hyperion.rest.annotations.Queryable")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class QueryObjectGenerator extends HyperionProcessor {

	private static final String QUERYABLE = "com.jeroensteenbeeke.hyperion.rest.annotations.Queryable";

	private Set<String> classes = new HashSet<>();

	@Override
	public void initEnvironment(ProcessingEnvironment environment) {
		super.initEnvironment(environment);
		environment.getMessager()
				.printMessage(Diagnostic.Kind.NOTE, "QueryObjectGenerator initialized");
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		Set<? extends Element> annotatedElements = roundEnv
				.getElementsAnnotatedWith(Queryable.class);
		processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
				"QueryObjectGenerator found " + annotatedElements.size() + " eligible classes");
		annotatedElements.stream().map(this::analyzeElement).flatMap(List::stream)
				.forEach(this::generateClass);

		return false;
	}

	private List<QueryObjectDescriptor> analyzeElement(Element e) {
		processingEnv.getMessager()
				.printMessage(Diagnostic.Kind.NOTE, "Considering " + e.getSimpleName().toString());

		if (e.getKind() != ElementKind.CLASS) {
			Element enclosingElement = e.getEnclosingElement();
			if (enclosingElement != null) {
				return analyzeElement(enclosingElement);
			} else {
				return ImmutableList.of();
			}
		}

		final String className = e.getSimpleName().toString();

		TypeElement type = ((TypeElement) e);

		if (classes.contains(type.getQualifiedName().toString())) {
			return ImmutableList.of();
		}
		classes.add(type.getQualifiedName().toString());

		final String packageName = extractPackageName(type.getQualifiedName().toString());

		ImmutableList.Builder<QueryObjectField> fields = ImmutableList.builder();

		List<Element> toExplore = Lists.newArrayList(e.getEnclosedElements());

		TypeMirror superType = type.getSuperclass();

		while (superType != null) {
			if (superType instanceof DeclaredType) {
				DeclaredType dt = (DeclaredType) superType;

				Element element = dt.asElement();

				if (element instanceof TypeElement && containsAnnotation(element, QUERYABLE)) {
					TypeElement t = (TypeElement) element;

					toExplore.addAll(t.getEnclosedElements());

					superType = t.getSuperclass();
				} else {
					break;
				}
			} else {
				break;
			}

		}

		analyzeFields(className, fields, toExplore);

		return ImmutableList.of(new QueryObjectDescriptor(className, packageName, fields.build()));

	}

	private void analyzeFields(String className, ImmutableList.Builder<QueryObjectField> fields,
			List<Element> toExplore) {
		processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "Fields of " + className);
		for (Element child : toExplore) {
			processingEnv.getMessager()
					.printMessage(Diagnostic.Kind.NOTE, "\t" + child.getSimpleName().toString());
			if (child.getKind() == ElementKind.FIELD
					&& containsAnnotation(child, QUERYABLE)) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "\t\tIs a field");

				ImmutableSet.Builder<String> imports = ImmutableSet.builder();
				VariableElement field = (VariableElement) child;

				TypeMirror declaredType = field.asType();

				final String fieldName = field.getSimpleName().toString();

				processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
						"Considering " + className + "." + fieldName);

				Map<? extends ExecutableElement, ? extends AnnotationValue> map = getAnnotationMirror(
						field, "io.swagger.annotations.ApiModelProperty").
						map(AnnotationMirror::getElementValues).orElseGet(HashMap::new);

				AtomicReference<String> description = new AtomicReference<>("");

				map.forEach((k, v) -> {
					if (k.getSimpleName().toString().equals("value")) {
						description.set((String) v.getValue());
					}
				});

				if (declaredType instanceof ArrayType) {
					// Array of something, most likely bytes. These are not
					// considered queryable
					processingEnv.getMessager()
							.printMessage(Diagnostic.Kind.NOTE, "\tArrayType -> SKIP");
				} else if (declaredType instanceof DeclaredType) {
					// Not an array, could be an entity, enum, string or boxed
					// primitive
					DeclaredType ref = (DeclaredType) declaredType;

					Element element = ref.asElement();

					if (element instanceof TypeElement) {
						TypeElement t = (TypeElement) element;

						imports.add(t.getQualifiedName().toString());

						if (isInstanceOf(String.class, t)) {
							processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
									"\tString -> create property");

							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.StringProperty",
									fieldName).setGenerator(new StringExamplesGenerator())
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(LocalDate.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.LocalDateProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator("2017-10-08",
											"2017-10-12"))
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(LocalDateTime.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query" +
											".LocalDateTimeProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator(
											"2017-10-08T14:45:33.000", "2017-10-08T16:45:33.000"))
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(ZonedDateTime.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.DateTimeProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator(
											"2017-10-08T14:45:33.000Z",
											"2017-10-08T16:45:33.000-01:00"))
									.setSwaggerDescription(description.get()));

						} else if (isInstanceOf(Boolean.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.BooleanProperty",
									fieldName).setGenerator(new BooleanExamplesGenerator())
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(Double.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.DoubleProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator("4.0", "12.0"))
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(BigDecimal.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.BigDecimalProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator("4.0", "12.0"))
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(Float.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.FloatProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator("4.0", "12.0"))
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(Integer.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.IntegerProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator("4", "12"))
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(Long.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.LongProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator("4", "12"))
									.setSwaggerDescription(description.get()));
						} else if (isInstanceOf(Short.class, t)) {
							fields.add(new QueryObjectField(
									"com.jeroensteenbeeke.hyperion.rest.query.ShortProperty",
									fieldName)
									.setGenerator(new ComparableExamplesGenerator("4", "12"))
									.setSwaggerDescription(description.get()));
						} else {
							processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
									"\tUnrecognized TypeElement " + t.toString() + " -> SKIP");

						}
					} else {
						processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
								"\tUnrecognized DeclaredType -> SKIP");
					}

				} else if (declaredType instanceof PrimitiveType) {
					// Primitive type
					PrimitiveType prim = (PrimitiveType) declaredType;
					String propertyType;
					ExamplesGenerator generator = null;
					switch (prim.getKind()) {
						case BOOLEAN:
							propertyType = "com.jeroensteenbeeke.hyperion.rest.query.BooleanProperty";
							generator = new BooleanExamplesGenerator();
							break;
						case DOUBLE:
							propertyType = "com.jeroensteenbeeke.hyperion.rest.query.DoubleProperty";
							generator = new ComparableExamplesGenerator("4.0", "12.0");
							break;
						case FLOAT:
							propertyType = "com.jeroensteenbeeke.hyperion.rest.query.FloatProperty";
							generator = new ComparableExamplesGenerator("4.0", "12.0");
							break;
						case INT:
							propertyType = "com.jeroensteenbeeke.hyperion.rest.query.IntegerProperty";
							generator = new ComparableExamplesGenerator("4", "12");
							break;
						case LONG:
							propertyType = "com.jeroensteenbeeke.hyperion.rest.query.LongProperty";
							generator = new ComparableExamplesGenerator("4", "12");
							break;
						case SHORT:
							propertyType = "com.jeroensteenbeeke.hyperion.rest.query.ShortProperty";
							generator = new ComparableExamplesGenerator("4", "12");
							break;
						default:
							// No idea, skip
							processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
									"\tUnrecognized PrimitiveType -> SKIP");
							continue;
					}

					fields.add(new QueryObjectField(propertyType, fieldName).setGenerator(generator)
							.setSwaggerDescription(description.get()));
				} else {
					processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
							"\tUnrecognized other type -> SKIP");
				}
			}
		}
	}

	private void generateClass(QueryObjectDescriptor data) {

		try {
			final String targetPackage = data.getPackageName().concat(".query");
			final String targetClassName = data.getQueryClassName().concat("_Query");
			final String fqdn = String.format("%s.%s", targetPackage, targetClassName);

			processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "Writing to " + fqdn);

			Filer filer = processingEnv.getFiler();
			FileObject classFile;
			try {
				classFile = filer.createSourceFile(fqdn);
			} catch (FilerException f) {
				// File already exists, get resource instead to overwrite
				classFile = filer.getResource(StandardLocation.SOURCE_OUTPUT, targetPackage,
						targetClassName.concat(".java"));
			}
			Set<String> imports = Sets.newTreeSet();
			imports.add("javax.annotation.Generated");
			imports.add("java.io.Serializable");
			imports.add("java.util.List");
			imports.add("java.util.LinkedList");
			imports.add("com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject");
			imports.add("javax.ws.rs.QueryParam");
			imports.add(data.getFQDN());

			data.getFields().stream().map(QueryObjectField::getType).forEach(type -> {
				imports.add(type);
				imports.add("io.swagger.annotations.*");
			});

			try (Writer fw = classFile.openWriter(); PrintWriter pw = new PrintWriter(fw)) {
				pw.print("package ");
				pw.print(targetPackage);
				pw.println(";");
				pw.println();
				imports.forEach(i -> {
					if (i.indexOf('.') == -1) {
						throw new IllegalArgumentException("Default package imports not allowed");
					}
					pw.print("import ");
					pw.print(i);
					pw.println(";");
				});
				pw.println();
				pw.printf(
						"@Generated(value = \"com.jeroensteenbeeke.hyperion.querygen.QueryObjectGenerator\",\n date="
								+
								" " +
								"\"%s\")",
						getDateString());
				pw.println();
				pw.print("public class ");
				pw.print(targetClassName);
				pw.printf(" implements QueryObject<%s>, Serializable {", data.getQueryClassName());
				pw.println();
				pw.println("\tprivate static final long serialVersionUID = 1L;");
				pw.println();
				pw.println("\tprivate int nextSortIndex;");
				pw.println();

				// Fields
				data.getFields().forEach(field -> {
					final String propertyFQDN = field.getType();
					final String propertyType = extractClass(propertyFQDN);
					final String fieldName = field.getName();

					if (field.getGenerator() != null) {
						List<String> examples = field.getGenerator().generateExamples();
						String exampleAnnotations = examples
								.stream()
								.map(s -> String.format("@ExampleProperty(value=\"%s\")", s))
								.collect(Collectors.joining(",\n\t\t"));
						String exampleValues = examples.stream().collect(Collectors.joining(", "));
						String exampleAnnotation = String
								.format("\t@ApiParam(name=\"%1$s\", value=\"%2$s\", examples=@Example({\n\t\t%3$s\n\t\t}), example=\"%4$s\")",
										fieldName,
										field.getSwaggerDescription(),
										exampleAnnotations,
										exampleValues);
						pw.println(exampleAnnotation);
					}
					pw.printf("\t@QueryParam(\"%s\")", fieldName);
					pw.println();
					pw.printf("\tprivate %1$s<%2$s> %3$s = new %1$s<>(this, \"%3$s\");", propertyType,
							targetClassName,
							fieldName);
					pw.println();
					pw.println();
				});

				// Accessors
				data.getFields().forEach(field -> {
					final String propertyFQDN = field.getType();
					final String propertyType = extractClass(propertyFQDN);

					pw.printf("\tpublic %1$s<%2$s> %3$s() {", propertyType, targetClassName,
							field.getName());
					pw.println();
					pw.printf("\t\treturn this.%s;", field.getName());
					pw.println();
					pw.println("\t}");
					pw.println();
				});

				pw.println("\t@Override");
				pw.println("\tpublic int getNextSortIndex() {");
				pw.println("\t\treturn nextSortIndex++;");
				pw.println("\t}");
				pw.println();

				pw.println("\t@Override");
				pw.printf("\tpublic boolean matches(%s object) {", data.getQueryClassName()).println();
				if (data.getFields().isEmpty()) {
					pw.println("\t\treturn true;");
				} else {
					pw.printf("\t\treturn %s;", data.getFields().stream()
													  .map(QueryObjectField::getName)
													  .map(n -> n + ".appliesTo(object)")
													  .collect(Collectors.joining(" && "))
					).println();
				}
				pw.println("\t}");
				pw.println();
				pw.println("}");


				pw.flush();


			}

			processingEnv.getMessager()
					.printMessage(Diagnostic.Kind.NOTE, "Generated " + classFile.getName());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the generation date as String
	 * @return The generation date
	 */
	protected String getDateString() {
		return new Date().toString();
	}

}
