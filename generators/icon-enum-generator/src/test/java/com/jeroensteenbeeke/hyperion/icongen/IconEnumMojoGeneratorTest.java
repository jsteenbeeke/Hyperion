package com.jeroensteenbeeke.hyperion.icongen;

import com.google.common.collect.ImmutableMap;
import com.jeroensteenbeeke.lux.TypedResult;
import com.jeroensteenbeeke.andalite.java.analyzer.*;
import com.jeroensteenbeeke.andalite.java.analyzer.annotation.StringValue;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.plugin.testing.stubs.MavenProjectStub;
import org.junit.Test;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import java.util.Map;

public class IconEnumMojoGeneratorTest extends AbstractMojoTestCase{
	@Test
	public void testIconGeneration() throws MojoExecutionException, MojoFailureException {
		File resourceFile = getTestFile(
				"src/test/resources/unit/testproject/target/generated-sources/icon/com" +
						"/jeroensteenbeeke/hyperion/icongen/TestIcon.java");
		File pom = getTestFile("src/test/resources/unit/testproject/pom.xml");

		if (resourceFile.exists()) {
			assertTrue(resourceFile.delete());
		}

		IconEnumGeneratorMojo mojo;
		try {
			mojo = (IconEnumGeneratorMojo) lookupMojo("generate", pom);
		} catch (Exception e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}

		assertNotNull(mojo);

		File testRoot = pom.getParentFile();
		mojo.project = new MavenProjectStub() {
			@Override public File getBasedir() {
				return testRoot;
			}
		};
		mojo.pseudo = "before";
		mojo.cssPrefixToStrip = "";
		mojo.cssPath = "src/main/css/icon.css";
		mojo.execute();

		assertTrue(resourceFile.exists());
		TypedResult<AnalyzedSourceFile> result = new ClassAnalyzer(
				resourceFile).analyze();
		assertTrue(result.isOk());

		AnalyzedSourceFile source = result.getObject();

		verifySource(source, ImmutableMap.<String, String> builder()
				.put("fire_everything", "fire-everything")
				.build());
	}

	@Test
	public void testPrefixedGeneration() throws MojoExecutionException, MojoFailureException {
		File resourceFile = getTestFile(
				"src/test/resources/unit/testproject/target/generated-sources/icon/com" +
						"/jeroensteenbeeke/hyperion/icongen/TestIcon.java");
		File pom = getTestFile("src/test/resources/unit/testproject/pom.xml");

		if (resourceFile.exists()) {
			assertTrue(resourceFile.delete());
		}

		IconEnumGeneratorMojo mojo;
		try {
			mojo = (IconEnumGeneratorMojo) lookupMojo("generate", pom);
		} catch (Exception e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}

		assertNotNull(mojo);

		File testRoot = pom.getParentFile();
		mojo.project = new MavenProjectStub() {
			@Override public File getBasedir() {
				return testRoot;
			}
		};
		mojo.pseudo = "before";
		mojo.cssPrefixToStrip = "";
		mojo.cssPath = "src/main/css/icon.css";
		mojo.impliedCssClasses = "test";
		mojo.execute();

		assertTrue(resourceFile.exists());
		TypedResult<AnalyzedSourceFile> result = new ClassAnalyzer(
				resourceFile).analyze();
		assertTrue(result.isOk());

		AnalyzedSourceFile source = result.getObject();

		verifySource(source, ImmutableMap.<String, String> builder()
								.put("fire_everything", "test fire-everything")
								.build());
	}

	@Test
	public void testMultiIconWithNumericStart() throws MojoExecutionException,
			MojoFailureException {
		File resourceFile = getTestFile(
				"src/test/resources/unit/testproject/target/generated-sources/icon/com" +
						"/jeroensteenbeeke/hyperion/icongen/TestIcon.java");
		File pom = getTestFile("src/test/resources/unit/testproject/pom.xml");

		if (resourceFile.exists()) {
			assertTrue(resourceFile.delete());
		}

		IconEnumGeneratorMojo mojo;
		try {
			mojo = (IconEnumGeneratorMojo) lookupMojo("generate", pom);
		} catch (Exception e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}

		assertNotNull(mojo);

		File testRoot = pom.getParentFile();
		mojo.project = new MavenProjectStub() {
			@Override public File getBasedir() {
				return testRoot;
			}
		};
		mojo.pseudo = "before";
		mojo.cssPrefixToStrip = "icon-";
		mojo.cssPath = "src/main/css/icon2.css";
		mojo.impliedCssClasses = "test";
		mojo.execute();

		assertTrue(resourceFile.exists());
		TypedResult<AnalyzedSourceFile> result = new ClassAnalyzer(
				resourceFile).analyze();
		assertTrue(result.isOk());

		AnalyzedSourceFile source = result.getObject();

		verifySource(source, ImmutableMap.<String, String> builder()
				.put("fire_everything", "test icon-fire-everything")
				.put("_666", "test icon-666")
				.build());
	}

	private void verifySource(AnalyzedSourceFile source, Map<String,String>
			iconExpectedConstParam) {
		// 1. Check for proper imports
		assertTrue(source.hasImport("com.jeroensteenbeeke.hyperion.icons.Icon"));
		assertTrue(source.hasImport("javax.annotation.Generated"));

		// 2. Check if generated file contains an enum named TestIcon
		assertEquals(1, source.getEnums().size());

		AnalyzedEnum firstEnum = source.getEnums().get(0);
		assertEquals("TestIcon", firstEnum.getEnumName());
		assertTrue(firstEnum.implementsInterface("Icon"));

		// 2a. Check for proper javax.annotation.Generated annotation
		assertTrue(firstEnum.hasAnnotation("Generated"));
		AnalyzedAnnotation generated = firstEnum.getAnnotation("Generated");
		assertNotNull(generated);
		assertTrue(generated.hasValueNamed("value"));
		assertTrue(generated.hasValueNamed("date"));
		StringValue valueValue = generated.getValue(StringValue.class, "value");
		StringValue dateValue = generated.getValue(StringValue.class, "date");

		assertNotNull(valueValue);
		assertNotNull(dateValue);
		String dateStr = dateValue.getValue();

		assertNotNull(dateStr);

		TemporalAccessor temporal = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(dateStr);

		assertEquals("com.jeroensteenbeeke.hyperion.icongen.IconEnumGeneratorMojo",
				valueValue.getValue());
		assertFalse(ZonedDateTime.from(temporal).isAfter(ZonedDateTime.now()));

		// 3. Check if enum has proper structure
		AnalyzedField cssClassesField = firstEnum.getField("cssClasses");
		List<AnalyzedConstructor> constructors = firstEnum.getConstructors();
		AnalyzedMethod getter =
				firstEnum.getMethod().withReturnType("String").named("getCssClasses");

		assertNotNull(getter);
		assertNotNull(cssClassesField);
		assertEquals(1, constructors.size());

		// 3a. Check constructor
		AnalyzedConstructor constructor = constructors.get(0);
		assertEquals(AccessModifier.DEFAULT, constructor.getAccessModifier());
		List<AnalyzedParameter> constructorParameters = constructor.getParameters();
		assertEquals(1, constructorParameters.size());
		AnalyzedParameter firstConstructorParameter = constructorParameters.get(0);

		assertEquals("String", firstConstructorParameter.getType());

		List<AnalyzedStatement<?,?>> constructorStatements = constructor.getStatements();
		assertEquals(1, constructorStatements.size());
		AnalyzedStatement statement = constructorStatements.get(0);
		assertEquals("this.cssClasses = cssClasses", statement.toJavaString());

		// 3b. Check field
		assertEquals("String", cssClassesField.getType().toJavaString());
		assertEquals(AccessModifier.PRIVATE, cssClassesField.getAccessModifier());

		// 3c. Check getter
		assertTrue(getter.hasAnnotation("Override"));

		// 4. Check enum constants
		List<AnalyzedEnumConstant> constants = firstEnum.getConstants();
		assertEquals(iconExpectedConstParam.size(), constants.size());

		for (AnalyzedEnumConstant constant: constants) {
			assertTrue(iconExpectedConstParam.containsKey(constant.getDenominationName()));
			String expectedValue = iconExpectedConstParam.get(constant.getDenominationName());

			List<AnalyzedExpression> parameters = constant.getParameters();
			assertEquals(1, parameters.size());

			assertEquals(String.format("\"%s\"", expectedValue), parameters.get(0).toJavaString());
		}
	}
}
