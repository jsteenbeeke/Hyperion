package com.jeroensteenbeeke.hyperion.icongen;

import com.jeroensteenbeeke.hyperion.icongen.cssparser.CSS3BaseVisitor;
import com.jeroensteenbeeke.hyperion.icongen.cssparser.CSS3Lexer;
import com.jeroensteenbeeke.hyperion.icongen.cssparser.CSS3Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.sonatype.plexus.build.incremental.BuildContext;

import java.io.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Maven plugin that generates icon enums
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES,
	  requiresDependencyResolution = ResolutionScope.COMPILE)
public class IconEnumGeneratorMojo extends AbstractMojo {
	@Parameter(required = true)
	public String cssPath;

	@Parameter(required = true, defaultValue = "before")
	public String pseudo;

	@Parameter(required = true, defaultValue = "")
	public String cssPrefixToStrip;

	@Parameter(required=false)
	public String impliedCssClasses;

	@Parameter(required = true)
	public String targetPackage;

	@Parameter(required = true)
	public String enumName;

	/**
	 * @component
	 */
	@Component
	public BuildContext buildContext;

	@Parameter(required = true, property = "project")
	public MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		File css = new File(project.getBasedir(), cssPath);

		getLog().info(String.format("Detecting icons from %s", css.getAbsolutePath()));

		try (InputStream in = new FileInputStream(css)) {
			CSS3Lexer lexer = new CSS3Lexer(new ANTLRInputStream(in));
			CSS3Parser parser = new CSS3Parser(new BufferedTokenStream(lexer));

			CSS3Parser.StylesheetContext stylesheet = parser.stylesheet();

			Set<String> iconClasses = new TreeSet<>();

			AtomicReference<String> containingClass = new AtomicReference<>();

			stylesheet.accept(new CSS3BaseVisitor<Void>() {
				@Override
				public Void visitSimpleSelectorSequence(CSS3Parser.SimpleSelectorSequenceContext
																ctx) {
					List<ParseTree> children = ctx.children;

					for (int i = 0; i < children.size(); i++) {
						ParseTree currentChild = children.get(i);
						if (currentChild instanceof CSS3Parser.ClassNameContext) {
							if ((i+1) < children.size()) {
								ParseTree nextChild = children.get(i+1);
								if (nextChild instanceof CSS3Parser.PseudoContext) {
									CSS3Parser.ClassNameContext className =
											(CSS3Parser.ClassNameContext) currentChild;
									CSS3Parser.PseudoContext pseudo =
											(CSS3Parser.PseudoContext) nextChild;

									String cssClass = className.getText();

									if (IconEnumGeneratorMojo.this.pseudo.equals(pseudo.ident()
																					   .Ident().getText())) {
										containingClass.set(cssClass);
									}
								}
							}
						}
					}

					return super.visitSimpleSelectorSequence(ctx);
				}

				@Override
				public Void visitDeclarationList(CSS3Parser.DeclarationListContext ctx) {
					if (containingClass.get() == null) {
						return super.visitDeclarationList(ctx);
					}

					List<CSS3Parser.DeclarationContext> declaration = ctx.declaration();

					if (declaration.size() == 1) {
						CSS3Parser.DeclarationContext declarationContext = declaration.get(0);
						CSS3Parser.PropertyContext prop = null;

						if (declarationContext instanceof CSS3Parser.UnknownDeclarationContext) {
							CSS3Parser.UnknownDeclarationContext unknown =
									(CSS3Parser.UnknownDeclarationContext) declarationContext;
							prop = unknown.property();
						} else if (declarationContext instanceof CSS3Parser
								.KnownDeclarationContext) {
							CSS3Parser.KnownDeclarationContext known =
									(CSS3Parser.KnownDeclarationContext) declarationContext;

							prop = known.property();
						}

						if (prop != null && "content".equals(prop.getText())) {
							// Highly likely to be an icon
							iconClasses.add(containingClass.get());
						}
					}

					return super.visitDeclarationList(ctx);
				}
			});

			File base = new File(project.getBasedir(), "target/generated-sources/icon");

			if (!base.exists()) {
				if (!base.mkdirs()) {
					throw new MojoFailureException("Could not create target directory "+
							base.getAbsolutePath()
							+" as user "+
							System.getProperty("user.name"));
				}
			}

			File pkg = new File(base, targetPackage.replace('.', File.separatorChar));

			if (!pkg.exists()) {
				if (!pkg.mkdirs()) {
					throw new MojoFailureException("Could not create target directory "+
							pkg.getAbsolutePath()
							+" as user "+
							System.getProperty("user.name"));
				}
			}

			File enumFile = new File(pkg, enumName.concat(".java"));

			try (FileWriter fw = new FileWriter(enumFile); PrintWriter pw = new PrintWriter(fw)) {
				pw.printf("package %s;", targetPackage).println();
				pw.println();
				pw.println("import com.jeroensteenbeeke.hyperion.icons.Icon;");
				pw.println("import javax.annotation.Generated;");
				pw.println();
				pw.println("/**");
				pw.printf(" * Automatically generated enum for %s icons", enumName).println();
				pw.println(" */");
				pw.printf("@Generated(value=\"%s\", date=\"%s\")", getClass().getName(),
						ZonedDateTime.now().format(
								DateTimeFormatter.ISO_OFFSET_DATE_TIME)).println();
				pw.printf("public enum %s implements Icon {", enumName).println();
				AtomicBoolean first = new AtomicBoolean(true);

				iconClasses.forEach(c -> {
					if (!first.getAndSet(false)) {
						pw.println(",");
					}

					String name = c;

					if (name.startsWith(".")) {
						name = name.substring(1);
					}

					if (name.startsWith(cssPrefixToStrip)) {
						name = name.substring(cssPrefixToStrip.length());
					}

					if (!Character.isJavaIdentifierStart(name.charAt(0))) {
						name = "_".concat(name);
					}

					StringBuilder sanitized = new StringBuilder();
					for (char n: name.toCharArray()) {
						if (n == '-') {
							sanitized.append('_');
						} else if (Character.isJavaIdentifierPart(n)) {
							sanitized.append(n);
						}
					}
					name = sanitized.toString();


					pw.print("\t");
					pw.print(name);
					pw.print("(\"");
					if (impliedCssClasses != null) {
						pw.print(impliedCssClasses);
						pw.print(" ");
					}
					pw.print(c.substring(1));
					pw.print("\")");

				});

				pw.println("\t;");
				pw.println();
				pw.println("\tprivate final String cssClasses;");
				pw.println();
				pw.printf("\t%s(String cssClasses) {", enumName).println();
				pw.println("\t\tthis.cssClasses = cssClasses;");
				pw.println("\t}");
				pw.println();
				pw.println("\t@Override");
				pw.println("\tpublic String getCssClasses() {");
				pw.println("\t\treturn this.cssClasses;");
				pw.println("\t}");
				pw.println("}");
				pw.flush();
			}

			project.addCompileSourceRoot(base.getPath());
		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage());
		}
	}
}
