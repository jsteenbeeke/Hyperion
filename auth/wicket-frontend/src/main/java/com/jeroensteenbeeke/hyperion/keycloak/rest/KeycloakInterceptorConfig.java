package com.jeroensteenbeeke.hyperion.keycloak.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Spring configuration class for auto-configuring Keycloak token passing to REST backends that
 * use Hyperion's proxy mechanism
 */
@Configuration
public class KeycloakInterceptorConfig {
	/**
	 * Creates a KeycloakTokenProvider bean
	 * @return A KeycloakTokenProvider
	 */
	@Bean
	@Scope("singleton")
	public KeycloakTokenProvider tokenProvider() {
		return new KeycloakTokenProvider();
	}

}
