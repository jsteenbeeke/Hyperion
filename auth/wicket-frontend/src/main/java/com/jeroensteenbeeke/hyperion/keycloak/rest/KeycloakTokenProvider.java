package com.jeroensteenbeeke.hyperion.keycloak.rest;

import com.jeroensteenbeeke.hyperion.keycloak.wicket.KeycloakWebSession;
import com.jeroensteenbeeke.hyperion.solstice.spring.resteasy.CustomHeaderProvider;
import com.jeroensteenbeeke.hyperion.solstice.spring.resteasy.CustomHeaderProviderRegistry;
import org.springframework.beans.factory.InitializingBean;

import javax.ws.rs.core.MultivaluedMap;

/**
 * CustomHeaderProvider implementation that adds authorization headers with the known Keycloak token
 */
public class KeycloakTokenProvider implements CustomHeaderProvider, InitializingBean {
	@Override
	public void afterPropertiesSet() {
		CustomHeaderProviderRegistry.instance.registerProvider(this);
	}

	@Override
	public void addHeaders(MultivaluedMap<String, Object> headers) {
		String token = KeycloakWebSession.get().getKeycloakToken();
		if (token != null) {
			headers.add("Authorization", "Bearer ".concat(token));
		}
	}
}
