package com.jeroensteenbeeke.hyperion.keycloak.wicket;

import org.apache.wicket.Component;
import org.apache.wicket.Session;
import org.apache.wicket.authorization.IUnauthorizedComponentInstantiationListener;
import org.apache.wicket.authorization.UnauthorizedInstantiationException;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authorization.strategies.role.RoleAuthorizationStrategy;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;

/**
 * Specialization of Wicket's WebApplication that automatically creates sessions
 * that hold Keycloak's token
 */
public abstract class KeycloakSecuredApplication extends WebApplication implements IUnauthorizedComponentInstantiationListener {
	@Override
	public Session newSession(Request request, Response response) {
		return new KeycloakWebSession(getKeycloakApplicationResourceId(), request);
	}

	/**
	 * Returns the application resource ID as defined in keycloak.json
	 * @return The application resource ID
	 */
	protected abstract String getKeycloakApplicationResourceId();

	/**
	 * @see org.apache.wicket.protocol.http.WebApplication#init()
	 */
	@Override
	protected void init()
	{
		super.init();

		// Set authorization strategy and unauthorized instantiation listener
		getSecuritySettings().setAuthorizationStrategy(new RoleAuthorizationStrategy(new KeycloakRoleCheckingStrategy()));
		getSecuritySettings().setUnauthorizedComponentInstantiationListener(this);
	}

	@Override
	public void onUnauthorizedInstantiation(Component component) {
		throw new UnauthorizedInstantiationException(component.getClass());
	}
}
