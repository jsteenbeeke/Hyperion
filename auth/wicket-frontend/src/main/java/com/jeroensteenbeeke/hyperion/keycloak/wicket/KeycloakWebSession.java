package com.jeroensteenbeeke.hyperion.keycloak.wicket;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.cycle.RequestCycle;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;

import javax.servlet.http.HttpServletRequest;

/**
 * Wicket session implementation that contains information about the current keycloak session. Requires
 * KeycloakOIDCFilter to be filtering the current request
 */
public class KeycloakWebSession extends AbstractAuthenticatedWebSession {
	private static final long serialVersionUID = 7802297771186768506L;

	private final String applicationResourceId;

	/**
	 * Constructor. Note that {@link RequestCycle} is not available until this constructor returns.
	 *
	 * @param applicationResourceId The resource ID of the current application
	 * @param request The current request
	 */
	public KeycloakWebSession(String applicationResourceId, Request request) {
		super(request);
		this.applicationResourceId = applicationResourceId;
	}

	/**
	 * Returns the current security token as returned by Keycloak
	 * @return A valid Keycloak token
	 */
	public String getKeycloakToken() {
		return getSecurityContext().getTokenString();
	}

	/**
	 * Returns the Keycloak security context for the current request
	 * @return The security context
	 */
	protected KeycloakSecurityContext getSecurityContext() {
		ServletWebRequest request = (ServletWebRequest) RequestCycle.get().getRequest();
		HttpServletRequest containerRequest = request.getContainerRequest();

		return (KeycloakSecurityContext) containerRequest.getAttribute(KeycloakSecurityContext.class
																		   .getName());
	}

	@Override
	public Roles getRoles() {
		return new Roles(getKeycloakRoles().toJavaSet());
	}

	@Override
	public boolean isSignedIn() {
		return getKeycloakToken() != null;
	}

	/**
	 * Determines the set of roles the current user has
	 * @return A set of roles the current user has
	 */
	public Set<String> getKeycloakRoles() {

		return Option.of(getSecurityContext())
					 .map(KeycloakSecurityContext::getToken)
					 .map(AccessToken::getResourceAccess)
					 .filter(resourcesAccess -> resourcesAccess.containsKey(applicationResourceId))
					 .map(resourceAccess -> resourceAccess.get(applicationResourceId))
					 .map(AccessToken.Access::getRoles)
					 .map(roles -> HashSet.ofAll(roles).toSet())
					 .getOrElse(HashSet::empty);
	}

	/**
	 * Gets the current Keycloak WebSession
	 * @return A KeycloakWebSession
	 */
	public static KeycloakWebSession get() {
		return (KeycloakWebSession) Session.get();
	}
}
