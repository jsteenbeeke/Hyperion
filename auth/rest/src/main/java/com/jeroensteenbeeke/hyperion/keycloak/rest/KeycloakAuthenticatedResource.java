package com.jeroensteenbeeke.hyperion.keycloak.rest;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.EntityFilterField;
import com.jeroensteenbeeke.hyperion.meld.rest.RESTQueryBinding;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import com.jeroensteenbeeke.hyperion.solstice.backend.HttpStatusException;
import com.jeroensteenbeeke.lux.TypedResult;
import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.function.Function;

import static com.jeroensteenbeeke.hyperion.keycloak.HyperionKeycloak.PROPERTY_TESTMODE;
import static com.jeroensteenbeeke.hyperion.keycloak.HyperionKeycloak.TESTMODE_USER_UUID;

/**
 * Base resource class for use with keycloak-secured resources
 *
 * @param <U> The user type
 */
public abstract class KeycloakAuthenticatedResource<U extends DomainObject> {
	@Context
	private HttpServletRequest servletRequest;

	/**
	 * Gets the current user
	 *
	 * @return A TypedResult containing either the current user with a list of permissions or an error message explaining why it couldn't be found
	 */
	protected final TypedResult<Tuple2<U, Set<String>>> getCurrentUser() {
		return TypedResult
			.attempt(() -> Option
				.of(System.getProperty(PROPERTY_TESTMODE))
			).mapUnless(HttpStatusException.class, option ->
				option.map(__ -> new Tuple2<>(TESTMODE_USER_UUID, HashSet.<String>empty().toSet()))
					  .orElse(() -> {
						  HttpServletRequest httpRequest = servletRequest;

						  KeycloakSecurityContext context = (KeycloakSecurityContext) httpRequest.getAttribute(KeycloakSecurityContext.class
																												   .getName());

						  return Option.of(context)
									   .map(ctx -> new Tuple2<>(ctx.getToken(),
																Optional
																	.ofNullable(ctx.getToken())
																	.map(AccessToken::getResourceAccess)
																	.filter(resourcesAccess -> resourcesAccess.containsKey(getKeycloakApplicationId()))
																	.map(resourceAccess -> resourceAccess.get(getKeycloakApplicationId()))
																	.map(AccessToken.Access::getRoles)
																	.map(roles -> HashSet
																		.ofAll(roles)
																		.toSet())
																	.orElseGet(HashSet::empty)
									   ))
									   .map(tuple -> tuple.map1(AccessToken::getSubject));

					  })
					  .getOrElseThrow(() -> HttpStatusException
						  .forStatus(Response.Status.UNAUTHORIZED)
						  .withMessage("Invalid or missing user token")
						  .asPlainText()))
			.map(tuple -> tuple.map1(userId -> findUserById(userId).getOrElse(() -> createUser(tuple._1))));
	}

	/**
	 * Checks if a user has the given role
	 *
	 * @param role The role to check
	 * @return A TypedResult containing either the current user, or an error message explaining the problem
	 */
	protected final TypedResult<U> requireRole(@Nonnull String role) {
		return getCurrentUser()
			.filter(userAndRoles -> userAndRoles._2.contains(role), userAndRoles -> String.format("User %s does not have role %s", userAndRoles._1, role))
			.map(Tuple2::_1);
	}

	/**
	 * Checks if a user has one of the given roles
	 *
	 * @param firstRole  The first role to check, this ensures the caller passes at least 1 role
	 * @param otherRoles Other roles to check
	 * @return A TypedResult containing either the current user, or an error message explaining the problem
	 */
	protected final TypedResult<U> requireAnyRole(@Nonnull String firstRole, @Nonnull String... otherRoles) {
		HashSet<String> requestedRoles = HashSet.of(firstRole);
		for (String role : otherRoles) {
			requestedRoles = requestedRoles.add(role);
		}

		final Set<String> requested = requestedRoles;

		return getCurrentUser()
			.filter(userAndRoles -> !userAndRoles._2
				.intersect(requested)
				.isEmpty(), userAndRoles -> String.format("User %s does not have one of roles: [%s]", userAndRoles._1, requested
				.reduce((a, b) -> a + ", " + b)))
			.map(Tuple2::_1);
	}


	/**
	 * Gets the application ID used in Keycloak. This is used to fetch user roles from the access token
	 *
	 * @return The application ID as String
	 */
	protected abstract String getKeycloakApplicationId();

	/**
	 * Retrieve the current user based on the given Keycloak ID
	 *
	 * @param id The ID from Keycloak
	 * @return Optionally the user belonging to this ID
	 */
	protected abstract Option<U> findUserById(@Nonnull String id);

	/**
	 * Create a new user for the given ID
	 *
	 * @param id The ID from Keycloak
	 * @return A new user object
	 */
	protected abstract U createUser(@Nonnull String id);

	/**
	 * Start constructing a count query using query objects
	 * @param query The query object
	 * @param <Q> The query object type
	 * @return A builder
	 */
	protected <Q extends QueryObject<?>> CountFilterOperationBuilder<Q> countBy(Q query) {
		return new CountFilterOperationBuilder<>(query);
	}

	/**
	 * Builder class for constructing a count query, adds a binding
	 * @param <Q> The query object type
	 */
	protected class CountFilterOperationBuilder<Q extends QueryObject<?>> {
		private final Q queryObject;

		private CountFilterOperationBuilder(Q queryObject) {
			this.queryObject = queryObject;
		}

		/**
		 * Set the REST binding
		 * @param binding The binding that converts the query
		 * @param <T> The type of entity
		 * @param <F> The type of search filter
		 * @return A builder
		 */
		public <T extends DomainObject, F extends SearchFilter<T, F>> CountDAOSelectionStage<T, F> withBinding(RESTQueryBinding<Q, F> binding) {
			return new CountDAOSelectionStage<>(binding.convert(queryObject));
		}
	}

	/**
	 * Builder class for constructing a count query, adds a DAO
	 * @param <T> The type of entity counted
	 * @param <F> The type of filter used
	 */
	protected class CountDAOSelectionStage<T extends DomainObject, F extends SearchFilter<T, F>> {
		private final F filter;

		private CountDAOSelectionStage(F filter) {
			this.filter = filter;
		}

		/**
		 * Sets the DAO
		 * @param dao The DAO to use
		 * @return A builder
		 */
		public CountLimitationStage<T, F> using(DAO<T, F> dao) {
			return new CountLimitationStage<>(filter, dao);
		}

	}

	/**
	 * Finalizer for creating a count query
	 * @param <T> The type of entity counted
	 * @param <F> The type of filter used
	 */
	protected class CountLimitationStage<T extends DomainObject, F extends SearchFilter<T, F>> {
		private final F filter;

		private final DAO<T, F> dao;

		private CountLimitationStage(F filter, DAO<T, F> dao) {
			this.filter = filter;
			this.dao = dao;
		}

		/**
		 * Sets accessors for setting the owner of the query
		 * @param ownerFieldGetter The primary field, if the owner is not yet set
		 * @param alternativeFieldGetter The secondary field, if the owner is already set
		 * @return The count
		 */
		public long withOwnerSpecifiedBy(
			Function<F, EntityFilterField<T, U, F, ?>> ownerFieldGetter,
			Function<F, EntityFilterField<T, U, F, ?>> alternativeFieldGetter
		) {
			EntityFilterField<T, U, F, ?> field = ownerFieldGetter.apply(filter);
			if (field.isSet()) {
				field = alternativeFieldGetter.apply(filter);
			}

			field.set(getCurrentUser().throwIfNotOk(IllegalStateException::new)._1());

			return dao.countByFilter(filter);
		}
	}

	/**
	 * Starts creating a List selection based on the given filter
	 * @param query The query
	 * @param <R> The type of REST object returned
	 * @param <Q> The query type
	 * @return A builder
	 */
	protected <R, Q extends QueryObject<R>> FilterOperationBuilder<R, Q> find(Q query) {
		return new FilterOperationBuilder<>(query);
	}

	/**
	 * Builder class for constructing a list query, adds a binding
	 * @param <R> The type of REST object returned
	 * @param <Q> The query object type
	 */
	protected class FilterOperationBuilder<R, Q extends QueryObject<R>> {
		private final Q queryObject;

		private FilterOperationBuilder(Q queryObject) {
			this.queryObject = queryObject;
		}

		/**
		 * Set the REST binding
		 * @param binding The binding that converts the query
		 * @param <T> The type of entity
		 * @param <F> The type of search filter
		 * @return A builder
		 */
		public <T extends DomainObject, F extends SearchFilter<T, F>> DAOSelectionStage<R, T, F> withBinding(RESTQueryBinding<Q, F> binding) {
			return new DAOSelectionStage<>(binding.convert(queryObject));
		}
	}

	/**
	 * Builder class for constructing a list query, adds a DAO
	 * @param <R> The type of REST object returned
	 * @param <T> The type of entity counted
	 * @param <F> The type of filter used
	 */
	protected class DAOSelectionStage<R, T extends DomainObject, F extends SearchFilter<T, F>> {
		private final F filter;

		private DAOSelectionStage(F filter) {
			this.filter = filter;
		}

		/**
		 * Sets the DAO
		 * @param dao The DAO to use
		 * @return A builder
		 */
		public LimitationStage<R, T, F> using(DAO<T, F> dao) {
			return new LimitationStage<>(filter, dao);
		}

	}

	/**
	 * Builder for creating a list query, splits between the limitation, and setting the logic for determing an owner
	 * @param <T> The type of entity counted
	 * @param <F> The type of filter used
	 */
	protected class LimitationStage<R, T extends DomainObject, F extends SearchFilter<T, F>> {
		private final F filter;

		private final DAO<T, F> dao;

		private LimitationStage(F filter, DAO<T, F> dao) {
			this.filter = filter;
			this.dao = dao;
		}

		/**
		 * Sets the logic for setting the owner
		 * @param ownerFieldGetter Primary getter for setting an owner, if not already set on the filter
		 * @param alternativeFieldGetter Secondary getter for setting an owner
		 * @return The current builder
		 */
		public LimitationStage<R, T, F> withOwnerSpecifiedBy(
			Function<F, EntityFilterField<T, U, F, ?>> ownerFieldGetter,
			Function<F, EntityFilterField<T, U, F, ?>> alternativeFieldGetter
		) {
			EntityFilterField<T, U, F, ?> field = ownerFieldGetter.apply(filter);
			if (field.isSet()) {
				field = alternativeFieldGetter.apply(filter);
			}

			field.set(getCurrentUser().throwIfNotOk(IllegalStateException::new)._1());
			return this;
		}

		/**
		 * Limits the query to the given offset and count. Executes the query
		 * @param offset The offset for the results, may be {@code null}, in which case it defaults to 0
		 * @param count The max number of results, may be {@code null}
		 * @return The finalizer to convert the results to the desired REST object
		 */
		public MappingFinalizer<T, R> limitedTo(Long offset, Long count) {
			if (count == null) {
				return new MappingFinalizer<>(dao.findByFilter(filter));
			} else {
				long o = Optional.ofNullable(offset).orElse(0L);

				return new MappingFinalizer<>(dao.findByFilter(filter, o, count));
			}

		}
	}

	/**
	 * Finalizer class, allows caller to specify how to convert to target REST object
	 * @param <T> The entity type
	 * @param <R> The REST object type
	 */
	protected static class MappingFinalizer<T extends DomainObject, R> {
		private final Seq<T> entities;

		private MappingFinalizer(Seq<T> entities) {
			this.entities = entities;
		}

		/**
		 * Specifies the mapper to use, and return the result
		 * @param mapper The mapper to convert entities to REST objects
		 * @return A list of REST objects
		 */
		public java.util.List<R> mappedBy(Function1<T, R> mapper) {
			return entities.map(mapper).toJavaList();
		}
	}
}
