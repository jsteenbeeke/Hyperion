package com.jeroensteenbeeke.hyperion.keycloak.filter;

import org.keycloak.adapters.servlet.KeycloakOIDCFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.jeroensteenbeeke.hyperion.keycloak.HyperionKeycloak.PROPERTY_TESTMODE;

/**
 * Servlet filter class that enabled Keycloak authentication through OIDC, except when
 * the system property {@code hyperion.testmode} is set OR the request is an OPTIONS request.
 * <p>
 * Also adds {@code Access-Control-Allow-Origin: *} and {@code Access-Control-Allow-Headers: authorization content-type} headers
 */
public class HyperionKeycloakFilter extends KeycloakOIDCFilter {
	private static final String ACCESS_CONTROL_ALLOW_HEADERS = "accessControlAllowHeaders";

	private static final String DEFAULT_ACCESS_CONTROL_ALLOW_HEADERS = "authorization, content-type";

	private String accessControlAllowHeaders;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);

		String headers = filterConfig.getInitParameter(ACCESS_CONTROL_ALLOW_HEADERS);

		if (headers != null) {
			accessControlAllowHeaders = headers;
		} else {
			accessControlAllowHeaders = DEFAULT_ACCESS_CONTROL_ALLOW_HEADERS;
		}

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		Optional<HttpServletRequest> httpServletRequest = Optional.ofNullable(req)
				.filter(r -> r instanceof HttpServletRequest)
				.map(r -> (HttpServletRequest) r);
		Optional.ofNullable(res)
				.filter(r -> r instanceof HttpServletResponse)
				.map(r -> (HttpServletResponse) r)
				.ifPresent(http -> {
					http.addHeader("Access-Control-Allow-Origin", "*");
					http.addHeader("Access-Control-Allow-Headers", accessControlAllowHeaders);
				});

		if (System.getProperty(PROPERTY_TESTMODE) != null || httpServletRequest.filter(r -> r.getMethod().equals("OPTIONS")).isPresent()) {
			chain.doFilter(req, res);
		} else {
			super.doFilter(req, res, chain);
		}
	}
}
