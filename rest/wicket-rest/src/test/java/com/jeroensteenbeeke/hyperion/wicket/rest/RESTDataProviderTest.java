package com.jeroensteenbeeke.hyperion.wicket.rest;

import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.junit.Test;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RESTDataProviderTest {
	private static final GenericType<List<String>> LIST_OF_STRING =
			new GenericType<List<String>>() {
			};

	interface TestResource extends ListableResource<String, TestObject> {

	}

	@Test
	public void testProviderCount() {
		TestResource resource = mock(TestResource.class);
		RESTDataProvider<String, TestObject, TestResource> provider = RESTDataProvider.query(new TestObject())
																					  .onService(resource)
																					  .as(() -> LIST_OF_STRING);

		Map<Response, Long> testCases = new HashMap<>();
		testCases.put(mockResponse(500), 0L);
		testCases.put(mockResponse(101), 0L);
		testCases.put(mockResponse(403), 0L);
		testCases.put(mockResponse(666), 0L);
		testCases.put(mockResponse(303), 0L);

		Response.StatusType okStatusInfo = mock(Response.StatusType.class);
		when(okStatusInfo.getFamily()).thenReturn(Response.Status.Family.SUCCESSFUL);

		Response okResponse = mock(Response.class);
		when(okResponse.readEntity(String.class)).thenReturn("5");
		when(okResponse.getStatus()).thenReturn(200);
		when(okResponse.getStatusInfo()).thenReturn(okStatusInfo);

		testCases.put(okResponse, 5L);

		Response invalidOkResponse = mock(Response.class);
		when(invalidOkResponse.readEntity(String.class)).thenReturn("five");
		when(invalidOkResponse.getStatus()).thenReturn(200);
		when(invalidOkResponse.getStatusInfo()).thenReturn(okStatusInfo);

		testCases.put(invalidOkResponse, 0L);

		testCases.forEach((response, expected) -> {
			when(resource.count(any(TestObject.class))).thenReturn(response);

			assertEquals((long) expected, provider.size());

		});


	}

	@Test
	public void testProviderIterator() {
		@SuppressWarnings("unchecked")
		ListableResource<String, TestObject> resource = mock(ListableResource.class);
		RESTDataProvider<String, TestObject, ?> provider = RESTDataProvider.query(new TestObject())
																		   .onService(resource)
																		   .as(() -> LIST_OF_STRING);

		Map<Response, List<String>> testCases = new HashMap<>();
		testCases.put(mockResponse(500), new LinkedList<>());
		testCases.put(mockResponse(101), new LinkedList<>());
		testCases.put(mockResponse(403), new LinkedList<>());
		testCases.put(mockResponse(666), new LinkedList<>());
		testCases.put(mockResponse(303), new LinkedList<>());
		List<String> actualResult = new LinkedList<>();
		actualResult.add("Result");

		Response.StatusType okStatusInfo = mock(Response.StatusType.class);
		when(okStatusInfo.getFamily()).thenReturn(Response.Status.Family.SUCCESSFUL);
		Response okResponse = mock(Response.class);
		when(okResponse.getStatusInfo()).thenReturn(okStatusInfo);
		@SuppressWarnings("unchecked")
		GenericType<List<String>> anyStringList = any(GenericType.class);
		when(okResponse.readEntity(anyStringList)).thenReturn(actualResult);

		testCases.put(okResponse, actualResult);

		testCases.forEach((response, expected) -> {
			when(resource.list(any(TestObject.class), anyLong(), anyLong())).thenReturn(response);

			List<String> result = new LinkedList<>();
			provider.iterator(0, 20).forEachRemaining(result::add);
			assertEquals(expected, result);

		});
	}

	@Test
	public void testModelProvider() {
		@SuppressWarnings("unchecked")
		ListableResource<String, TestObject> resource = mock(ListableResource.class);
		RESTDataProvider<String, TestObject, ?> provider = RESTDataProvider.query(new TestObject())
																		   .onService(resource)
																		   .as(() -> LIST_OF_STRING);

		IModel<String> model = provider.model("Test");
		assertTrue(model instanceof Model);
		assertEquals("Test", model.getObject());

	}

	private Response mockResponse(int status) {
		Response response = mock(Response.class);
		when(response.getStatus()).thenReturn(status);
		when(response.readEntity(String.class)).thenReturn(Integer.toString(status));

		when(response.getStatusInfo()).thenReturn(Optional
														  .ofNullable(Response.Status
																			  .fromStatusCode(status))
														  .orElse(Response.Status.INTERNAL_SERVER_ERROR));

		return response;

	}
}
