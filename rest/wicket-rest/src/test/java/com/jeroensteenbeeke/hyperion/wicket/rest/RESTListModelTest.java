package com.jeroensteenbeeke.hyperion.wicket.rest;

import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;
import org.apache.wicket.model.IModel;
import org.junit.Test;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RESTListModelTest {
	private static final GenericType<List<String>> LIST_OF_STRING =
		new GenericType<List<String>>() {
		};

	interface TestResource extends ListableResource<String, TestObject> {

	}


	@Test
	public void testBatchedFetch() {
		TestObject t = new TestObject();
		@SuppressWarnings("unchecked")
		ListableResource<String, TestObject> resource = mock(ListableResource.class);
		List<String> allStrings = new ArrayList<>(150);
		for (int i = 0; i < 150; i++) {
			allStrings.add(Integer.toBinaryString(i));
		}

		Response sizeResponse = mockResponse(Long.class, Long.valueOf(allStrings.size()));
		Response batch1Response = mockResponse(LIST_OF_STRING, allStrings.subList(0, 20));
		Response batch2Response = mockResponse(LIST_OF_STRING, allStrings.subList(20, 40));
		Response batch3Response = mockResponse(LIST_OF_STRING, allStrings.subList(40, 60));
		Response batch4Response = mockResponse(LIST_OF_STRING, allStrings.subList(60, 80));
		Response batch5Response = mockResponse(LIST_OF_STRING, allStrings.subList(80, 100));
		Response batch6Response = mockResponse(LIST_OF_STRING, allStrings.subList(100, 120));
		Response batch7Response = mockResponse(LIST_OF_STRING, allStrings.subList(120, 140));
		Response batch8Response = mockResponse(LIST_OF_STRING, allStrings.subList(140, 150));

		when(resource.count(t)).thenReturn(sizeResponse);

		when(resource.list(t, 0L, 20L)).thenReturn(batch1Response);
		when(resource.list(t, 20L, 20L)).thenReturn(batch2Response);
		when(resource.list(t, 40L, 20L)).thenReturn(batch3Response);
		when(resource.list(t, 60L, 20L)).thenReturn(batch4Response);
		when(resource.list(t, 80L, 20L)).thenReturn(batch5Response);
		when(resource.list(t, 100L, 20L)).thenReturn(batch6Response);
		when(resource.list(t, 120L, 20L)).thenReturn(batch7Response);
		when(resource.list(t, 140L, 10L)).thenReturn(batch8Response);

		IModel<List<String>> model = RESTListModel.query(t)
												  .withResource(resource)
												  .yielding(() -> LIST_OF_STRING)
												  .withErrorHandler((code, msg) -> assertEquals(msg, (Integer) 200, code));

		assertEquals(allStrings, model.getObject());
	}

	private <T> Response mockResponse(Class<T> responseType, T body) {
		Response response = mock(Response.class);
		when(response.getStatus()).thenReturn(200);
		when(response.readEntity(responseType)).thenReturn(body);

		when(response.getStatusInfo()).thenReturn(Optional
													  .ofNullable(Response.Status
																	  .fromStatusCode(200))
													  .orElse(Response.Status.INTERNAL_SERVER_ERROR));

		return response;

	}

	private <T> Response mockResponse(GenericType<T> responseType, T body) {
		Response response = mock(Response.class);
		when(response.getStatus()).thenReturn(200);
		when(response.readEntity(responseType)).thenReturn(body);

		when(response.getStatusInfo()).thenReturn(Optional
													  .ofNullable(Response.Status
																	  .fromStatusCode(200))
													  .orElse(Response.Status.INTERNAL_SERVER_ERROR));

		return response;

	}
}
