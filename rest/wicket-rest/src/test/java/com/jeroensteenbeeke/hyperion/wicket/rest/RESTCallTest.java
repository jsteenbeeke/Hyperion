package com.jeroensteenbeeke.hyperion.wicket.rest;

import org.junit.Test;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

import static com.jeroensteenbeeke.hyperion.wicket.rest.RESTCall.call;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class RESTCallTest {
	@Test(expected = UnsupportedOperationException.class)
	public void invokeConstructor() {
		new RESTCall();
	}

	@Test
	public void testHappyFlow() {
		String happy = "It works!";

		Response response = mock(Response.class);
		when(response.hasEntity()).thenReturn(true);
		when(response.bufferEntity()).thenReturn(true);
		when(response.getStatus()).thenReturn(200);
		when(response.readEntity(String.class)).thenReturn(happy);

		call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertNull("This statement should not be reached",e))
				.andThen(() -> assertTrue(true));


		call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertNull("This statement should not be reached",e))
				.yielding(String.class)
				.andThen(s -> assertEquals(happy, s));

		assertTrue(call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertNull("This statement should not be reached",e))
				.yielding(String.class)
				.asOptional().isPresent());

		GenericType<List<String>> expectedType = new GenericType<List<String>>() {
		};

		List<String> happyList = Collections.singletonList(happy);
		when(response.readEntity(expectedType)).thenReturn(happyList);

		call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertNull("This statement should not be reached",e))
				.yielding(expectedType)
				.andThen(l -> assertEquals(happyList, l));

		assertTrue(call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertNull("This statement should not be reached",e))
				.yielding(expectedType)
				.asOptional().isPresent());
	}

	@Test
	public void testReadableErrorFlow() {
		String error = "Shit hit the fan";

		Response response = mock(Response.class);
		when(response.hasEntity()).thenReturn(true);
		when(response.bufferEntity()).thenReturn(true);
		when(response.getStatus()).thenReturn(500);
		when(response.readEntity(String.class)).thenReturn(error);

		testErrorFlow(error, response);
		;
	}

	@Test
	public void testUnbufferedEntityFlow() {
		String error = "Remote returned error 500";

		Response response = mock(Response.class);
		when(response.hasEntity()).thenReturn(true);
		when(response.bufferEntity()).thenReturn(false);
		when(response.getStatus()).thenReturn(500);
		when(response.readEntity(String.class)).thenReturn(error);

		testErrorFlow(error, response);
		;
	}

	@Test
	public void testMissingEntityFlow() {
		String error = "Remote returned error 500";

		Response response = mock(Response.class);
		when(response.hasEntity()).thenReturn(false);
		when(response.bufferEntity()).thenReturn(true);
		when(response.getStatus()).thenReturn(500);
		when(response.readEntity(String.class)).thenReturn(error);

		testErrorFlow(error, response);
		;
	}

	@Test
	public void testMissingBothFlow() {
		String error = "Remote returned error 500";

		Response response = mock(Response.class);
		when(response.hasEntity()).thenReturn(false);
		when(response.bufferEntity()).thenReturn(false);
		when(response.getStatus()).thenReturn(500);
		when(response.readEntity(String.class)).thenReturn(error);

		testErrorFlow(error, response);
		;
	}

	private void testErrorFlow(String error, Response response) {
		call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertEquals(error, e))
				.andThen(() -> assertFalse("This statement should not be reached", true));


		call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertEquals(error, e))
				.yielding(String.class)
				.andThen(s -> assertNull("This statement should not be reached", s));

		assertFalse(call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertEquals(error, e))
				.yielding(String.class)
				.asOptional().isPresent());

		call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertEquals(error, e))
				.yielding(new GenericType<List<String>>() {})
				.andThen(s -> assertNull("This statement should not be reached", s));

		assertFalse(call(response)
				.expecting(Response.Status.OK)
				.onError((c,e) -> assertEquals(error, e))
				.yielding(new GenericType<List<String>>() {})
				.asOptional().isPresent());
	}
}
