package com.jeroensteenbeeke.hyperion.wicket.rest;

import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.apache.wicket.model.LoadableDetachableModel;
import org.danekja.java.util.function.serializable.SerializableBiConsumer;
import org.danekja.java.util.function.serializable.SerializableSupplier;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.jeroensteenbeeke.hyperion.wicket.rest.RESTCall.call;

/**
 * ListModel that wraps a REST query. Useful for dropdown choices. For repeaters it is generally advisable to use RESTDataProvider instead
 * @param <T> The type of object returned by the REST service
 * @param <Q> The type of query to execute on the service
 */
public class RESTListModel<T extends Serializable, Q extends QueryObject<T>> extends LoadableDetachableModel<List<T>> {
	private static final int BATCH_SIZE = 20;

	private static final long serialVersionUID = -6925777078479456337L;

	private final ListableResource<T, Q> resource;

	private final Q query;

	private final SerializableBiConsumer<Integer, String> errorHandler;

	private final SerializableSupplier<GenericType<List<T>>> listSupplier;

	private RESTListModel(ListableResource<T, Q> resource,
						 Q query,
						 SerializableBiConsumer<Integer, String> errorHandler,
						 SerializableSupplier<GenericType<List<T>>> listSupplier) {
		this.resource = resource;
		this.query = query;
		this.errorHandler = errorHandler;
		this.listSupplier = listSupplier;
	}

	@Override
	protected List<T> load() {
		return call(resource.count(query))
			.expecting(Response.Status.OK)
			.onError(errorHandler)
			.yielding(Long.class)
			.asOptional()
			.map(Long::intValue)
			.map(count -> {
				List<T> result = new ArrayList<>(count);

				for (long offset = 0L; offset < count; offset += BATCH_SIZE) {
					long slice = Math.min(BATCH_SIZE, count - offset);

					call(resource.list(query, offset, slice)).expecting(Response.Status.OK)
															 .onError(errorHandler)
															 .yielding(listSupplier.get())
															 .andThen(result::addAll);
				}

				return result;
			}).orElseGet(List::of);
	}

	/**
	 * Start creating a model for the given query
	 * @param query The query to execute
	 * @param <T> The type of object to get a list of
	 * @param <Q> The type of the query
	 * @return A builder
	 */
	public static <T extends Serializable, Q extends QueryObject<T>> WithResource<T, Q> query(Q query) {
		return resource -> listTypeSupplier -> errorHandler -> new RESTListModel<>(resource, query, errorHandler, listTypeSupplier);
	}

	/**
	 * Builder step
	 * @param <T> The type of object to get a list of
	 * @param <Q> The type of the query
	 */
	@FunctionalInterface
	public interface WithResource<T extends Serializable, Q extends QueryObject<T>> {
		/**
		 * Set the resource to perform the query on
		 * @param resource A REST resource (generally a proxy)
		 * @return The next builder step
		 */
		Yielding<T, Q> withResource(ListableResource<T, Q> resource);
	}

	/**
	 * Builder step
	 * @param <T> The type of object to get a list of
	 * @param <Q> The type of the query
	 */
	@FunctionalInterface
	public interface Yielding<T extends Serializable, Q extends QueryObject<T>> {
		/**
		 * Set the type of list to return
		 * @param listTypeSupplier A supplier that returns the list type
		 * @return The next builder step
		 */
		WithErrorHandler<T, Q> yielding(SerializableSupplier<GenericType<List<T>>> listTypeSupplier);
	}

	/**
	 * Builder step
	 * @param <T> The type of object to get a list of
	 * @param <Q> The type of the query
	 */
	@FunctionalInterface
	public interface WithErrorHandler<T extends Serializable, Q extends QueryObject<T>> {
		/**
		 * Sets the error handler to invoke when one of the REST calls fails
		 * @param errorHandler The error handler, that accepts the HTTP status code and message
		 * @return The constructed ListModel
		 */
		RESTListModel<T, Q> withErrorHandler(SerializableBiConsumer<Integer, String> errorHandler);
	}
}
