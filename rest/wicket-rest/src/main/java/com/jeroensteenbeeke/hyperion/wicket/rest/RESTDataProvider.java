package com.jeroensteenbeeke.hyperion.wicket.rest;

import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.danekja.java.util.function.serializable.SerializableFunction;
import org.danekja.java.util.function.serializable.SerializableSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * IDataProvider implementation that calls a ListableResource to fetch data
 *
 * @param <RESOURCE>
 * @param <QUERY>
 * @param <ENDPOINT> The JAX-RS interface type that returns the resource
 */
public class RESTDataProvider<RESOURCE extends Serializable, QUERY extends QueryObject<RESOURCE>,
		ENDPOINT extends ListableResource<RESOURCE, QUERY>>
		implements
		IDataProvider<RESOURCE> {
	private static final Logger log = LoggerFactory.getLogger(RESTDataProvider.class);

	private final ENDPOINT resource;

	private final QUERY query;

	private final SerializableSupplier<GenericType<List<RESOURCE>>> listType;

	private RESTDataProvider(
			ENDPOINT resource, QUERY query,
			SerializableSupplier<GenericType<List<RESOURCE>>>
					listType) {
		this.resource = resource;
		this.query = query;
		this.listType = listType;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Iterator<? extends RESOURCE> iterator(long first, long count) {
		Response response = resource.list(query, first, count);

		switch (response.getStatusInfo().getFamily()) {
			case OTHER:
			case REDIRECTION:
			case SERVER_ERROR:
			case CLIENT_ERROR:
			case INFORMATIONAL:
				logError(String.format("GET %s/", resource.getName()), response);
				return Collections.emptyListIterator();
			default:
				return response.readEntity(listType.get()).iterator();

		}

	}

	@Override
	public long size() {
		Response response = resource.count(query);

		switch (response.getStatusInfo().getFamily()) {
			case OTHER:
			case REDIRECTION:
			case SERVER_ERROR:
			case CLIENT_ERROR:
			case INFORMATIONAL:
				logError(String.format("GET %s/count", resource.getName()), response);
				return 0L;
			default:
				try {
					return Long.parseLong(response.readEntity(String.class));
				} catch (NumberFormatException nfe) {
					log.error(nfe.getMessage(), nfe);
					return 0L;
				}

		}
	}

	private void logError(@Nonnull String call, @Nonnull Response response) {
		log.error("Call {} yielded unexpected REST response {}: {}", call, response.getStatus(), response.readEntity
				(String.class));
	}

	@Override
	public IModel<RESOURCE> model(RESOURCE object) {
		return Model.of(object);
	}

	/**
	 * Build a DataProvider for the given query
	 *
	 * @param query The query to list the results of
	 * @param <R> The type of resource to return
	 * @param <Q>   The type of query
	 * @return A Builder that allows the caller to provide a serivce
	 */
	public static <R extends Serializable, Q extends QueryObject<R>> Builder<R, Q> query(Q query) {
		return new Builder<>(query);
	}

	/**
	 * Builder for specifying the resource to use with the RESTDataProvider
	 *
	 * @param <R> The type of resource to return
	 * @param <Q> The type of query
	 */
	public static class Builder<R extends Serializable, Q extends QueryObject<R>> {
		private final Q query;

		private Builder(Q query) {
			this.query = query;
		}

		/**
		 * Specifies the service to fire the query at
		 *
		 * @param service The service to query
		 * @param <LR>    The interface that returns the resource
		 * @return A finalizer to specify the return type
		 */
		public <LR extends ListableResource<R, Q>> Finalizer<R, Q, LR> onService(LR service) {
			return new Finalizer<>(query, service);
		}
	}

	/**
	 * Finalizer for creating a RESTDataProvider, specifies the JAX-RS GenericType to use for
	 * unmarshalling the response
	 *
	 * @param <R>  The type of resource to return
	 * @param <Q>  The type of query
	 * @param <LR> The interface that returns the resource
	 */
	public static class Finalizer<R extends Serializable, Q extends QueryObject<R>, LR extends ListableResource<R, Q>> {
		private final Q query;

		private final LR service;

		private Finalizer(Q query, LR service) {
			this.query = query;
			this.service = service;
		}

		/**
		 * Creates the dataprovider by providing the expected return type. Generally speaking
		 * you define this type along with the data object, as a {@code static final} anonymous
		 * inner class. Due to the way generic type info is retained at runtime (only at the
		 * class-level), we need to specify this manually
		 *
		 * @param listType The type of list we expect to receive.
		 * @return A DataProvider that submits the query to the given service
		 */
		public RESTDataProvider<R, Q, LR> as(SerializableSupplier<GenericType<List<R>>> listType) {
			return new RESTDataProvider<>(service, query, listType);
		}
	}
}
