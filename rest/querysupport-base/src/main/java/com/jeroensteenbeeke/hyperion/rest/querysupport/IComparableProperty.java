package com.jeroensteenbeeke.hyperion.rest.querysupport;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import java.lang.reflect.ParameterizedType;
import java.util.Optional;

/**
 * Representation of a comparable property: i.e. a property containing a value that is comparable
 *
 * @param <C> The type of comparable contained
 * @param <Q> The type of object containing the current property
 */
public interface IComparableProperty<C extends Comparable<? super C>, Q extends QueryObject<?>> extends IQueryProperty<Q> {
	/**
	 * Returns the type of comparison to perform on this property
	 *
	 * @return The type of comparison, {@link ComparableComparisonType}
	 */
	@Nonnull
	ComparableComparisonType getComparisonType();

	/**
	 * Gets the primary value of the current comparison. Should always be non-null if an operation was set, but may
	 * be null if the current property has no operation
	 *
	 * @return The primary value, or {@code null} if none is present
	 */
	@CheckForNull
	C getPrimary();

	/**
	 * Gets the secondary value of the current comparison. Generally speaking this value is only used
	 * with BETWEEN queries
	 *
	 * @return The secondary value, or {@code null} if none is present
	 */
	@CheckForNull
	C getSecondary();


	@Override
	@SuppressWarnings("unchecked")
	default boolean appliesTo(Object object) {
		if (!isSet()) {
			return true;
		}

		C primary = getPrimary();
		C actual = (C) getField(object, getFieldName(), primary != null ? primary.getClass() : null);

		return isNegated() != getComparisonType().test(primary, getSecondary(), actual);
	}
}
