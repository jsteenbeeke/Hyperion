package com.jeroensteenbeeke.hyperion.rest.querysupport;

import javax.annotation.Nonnull;

/**
 * Query property representing String operations
 * @param <Q> The type of object containing the current property
 */
public interface IStringProperty<Q extends QueryObject<?>> extends IQueryProperty<Q> {
	/**
	 * Returns the type of comparison done by this property.
	 * @return The {@link StringComparisonType} denoting the type of comparison done by this property
	 */
	@Nonnull
	StringComparisonType getType();

	/**
	 * Returns the value of the current property
	 * @return A String containing the value of this field
	 */
	@Nonnull
	String getValue();

	@Override
	default boolean appliesTo(Object object) {
		if (!isSet()) {
			return true;
		}

		return isNegated() != getType().test(getValue(), getField(object, getFieldName(), String.class));
	}
}
