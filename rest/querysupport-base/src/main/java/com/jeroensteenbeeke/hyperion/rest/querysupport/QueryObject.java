package com.jeroensteenbeeke.hyperion.rest.querysupport;

import java.io.Serializable;
import java.util.List;

/**
 * Marker interface to indicate the given object is used to describe a query
 * @param <T> The type of object that can be queried
 */
public interface QueryObject<T> extends Serializable {
	/**
	 * Get the next index for field ordering
	 * @return An integer representing the next index for field sorting
	 */
	int getNextSortIndex();

	/**
	 * Applies query logic to the given object (rather than being translated to, for instance, JPA)
	 * @param object The object to test
	 * @return {@code} true if this query would have returned this result. {@code false otherwise}
	 */
	boolean matches(T object);
}
