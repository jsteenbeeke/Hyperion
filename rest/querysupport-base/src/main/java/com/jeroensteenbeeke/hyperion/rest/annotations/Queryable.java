package com.jeroensteenbeeke.hyperion.rest.annotations;

import java.lang.annotation.*;

/**
 * Marker annotation that indicates the given type or field can be converted to query object by the QueryObjectGenerator
 * from hyperion-queryobject-generator
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.CLASS)
@Inherited
public @interface Queryable {
}
