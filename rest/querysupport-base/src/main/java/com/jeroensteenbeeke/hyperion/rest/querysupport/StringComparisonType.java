package com.jeroensteenbeeke.hyperion.rest.querysupport;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Comparison type between String values, defines behavior of a String property
 */
public enum StringComparisonType {
	/**
	 * No comparison
	 */
	NONE {
		@Override
		public boolean test(@Nullable String expectedValue, @Nullable String actualValue) {
			return true;
		}
	},
	/**
	 * Tests if two Strings are equal
	 */
	EQUALS {
		@Override
		public boolean test(@Nullable String expectedValue, @Nullable String actualValue) {
			return Objects.equals(expectedValue, actualValue);
		}
	},
	/**
	 * Tests if two Strings are equal when ignoring case
	 */
	EQUALS_IGNORECASE {
		@Override
		public boolean test(@Nullable String expectedValue, @Nullable String actualValue) {
			if (expectedValue == null || actualValue == null) {
				return false;
			}

			return expectedValue.equalsIgnoreCase(actualValue);
		}
	},
	/**
	 * Tests if two Strings match according to the SQL LIKE operation
	 */
	LIKE {
		@Override
		public boolean test(@Nullable String expectedValue, @Nullable String actualValue) {
			if (expectedValue == null || actualValue == null) {
				return false;
			}

			return actualValue.matches(expectedValue.replace("%", ".*"));
		}
	},
	/**
	 * Tests if two Strings match according to the SQL LIKE operation, ignoring case
	 */
	ILIKE {
		@Override
		public boolean test(@Nullable String expectedValue, @Nullable String actualValue) {
			if (expectedValue == null || actualValue == null) {
				return false;
			}

			return LIKE.test(expectedValue.toLowerCase(), actualValue.toLowerCase());
		}
	},
	/**
	 * Tests if the given String is explicitly null
	 */
	NULL {
		@Override
		public boolean test(@Nullable String expectedValue, @Nullable String actualValue) {
			return actualValue == null;
		}
	};

	/**
	 * Tests whether the given value matches the query's value
	 * @param expectedValue The value part of the query
	 * @param actualValue The value in the object
	 * @return {@code true} in case of a match, {@code false} otherwise
	 */
	public abstract boolean test(@Nullable String expectedValue, @Nullable String actualValue);
}
