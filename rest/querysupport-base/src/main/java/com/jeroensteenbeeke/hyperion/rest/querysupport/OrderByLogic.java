package com.jeroensteenbeeke.hyperion.rest.querysupport;

import javax.annotation.Nonnull;

/**
 * Utility class for parsing and formatting order-by clauses
 */
public class OrderByLogic {
	/**
	 * Parses the given String representation for order-by expressions, and reports them back to the given BiConsumer
	 *
	 * @param representation The String representation that might contain an order-by clause
	 * @param property       The property to set the orderby expression on
	 * @return The remainder of the expression
	 * @param <P> The type of queryproperty passed to this method
	 */
	@Nonnull
	public static <P extends IQueryProperty<?>> String extractOrderBy(@Nonnull String representation, @Nonnull P property) {
		String input = representation;

		if (input.startsWith("-") || input.startsWith("+")) {
			int orderIndex = 0;
			boolean ascending = input.startsWith("+");
			input = input.substring(1);

			while (input.startsWith("-") || input.startsWith("+")) {
				orderIndex++;
				input = input.substring(1);
			}

			property.setPropertyOrder(new OrderBy(orderIndex, ascending));
		}


		return input;
	}

	/**
	 * Writes the orderby expression to the given output, based on the given OrderBy description
	 *
	 * @param orderBy The description of how this property should be ordered
	 * @param output  The output to write the expression to
	 */
	public static void applyOrderBy(@Nonnull OrderBy orderBy, @Nonnull StringBuilder output) {
		if (orderBy.getIndex() != OrderBy.IGNORE.getIndex()) {
			for (int i = 0; i <= orderBy.getIndex(); i++) {
				if (orderBy.isAscending()) {
					output.append("+");
				} else {
					output.append("-");
				}
			}
		}
	}
}
