package com.jeroensteenbeeke.hyperion.rest.querysupport;

/**
 * Marker interface for Comparable properties of type Long
 * @param <Q> The type of queryobject this property is part of
 */
public interface ILongProperty<Q extends QueryObject<?>> extends IComparableProperty<Long,Q> {
}
