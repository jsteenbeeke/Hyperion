package com.jeroensteenbeeke.hyperion.rest.querysupport;

import java.util.Objects;

/**
 * Query property representing boolean operations
 * @param <Q> The type of object containing the current property
 */
public interface IBooleanProperty<Q extends QueryObject<?>> extends IQueryProperty<Q> {
	/**
	 * Returns the value that was set, if one exists
	 *
	 * @return The value set, or {@code null} if none was set
	 */
	Boolean getUnwrappedValue();

	/**
	 * Return whether or not the property should be checked for an explicit null value
	 *
	 * @return {@code true} if the property should be null (or not-null if {@link #isNegated()} returns true), or
	 * {@code false} if a normal equality check should be performed
	 */
	boolean isExplicitNull();

	@Override
	default boolean appliesTo(Object object) {
		if (!isSet()) {
			return true;
		}

		Boolean f = getField(object, getFieldName(), Boolean.class);
		if (f == null) {
			f = getField(object, getFieldName(), boolean.class);
		}

		if (isExplicitNull()) {
			return f == null;
		} else {
			boolean equals = Objects.equals(f, getUnwrappedValue());

			return isNegated() != equals;
		}
	}

}
