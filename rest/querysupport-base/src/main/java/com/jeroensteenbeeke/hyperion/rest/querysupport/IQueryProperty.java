package com.jeroensteenbeeke.hyperion.rest.querysupport;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Basic interface for REST query properties
 *
 * @param <Q> The type of object containing the current property
 */
public interface IQueryProperty<Q extends QueryObject<?>> extends Serializable {
	/**
	 * Determines whether or not the property is actually set
	 *
	 * @return {@code true} if the property is set and should be taken into account, {@code false} otherwise
	 */
	boolean isSet();

	/**
	 * Returns whether or not the current property's condition should be negated
	 *
	 * @return {@code true} if the operation should be negated, {@code false} otherwise
	 */
	boolean isNegated();

	/**
	 * Adds an order-by clause for the current property.
	 *
	 * @param ascending {@code true} if the results should be sorted by the current property in ascending order, {@code false} if they should be sorted in descending order
	 * @return The Query object containing the current property
	 */
	Q orderBy(boolean ascending);

	/**
	 * Determines how the given property should be ordered.
	 *
	 * @return An OrderBy class describing how this property should be ordered, or OrderBy.IGNORE if it does not count toward ordering
	 */
	@Nonnull
	OrderBy getPropertyOrder();

	/**
	 * FOR INTERNAL USE ONLY
	 *
	 * @param orderBy Sets the OrderBy clause of this property
	 */
	void setPropertyOrder(@Nonnull OrderBy orderBy);

	/**
	 * Returns the field name of this property. NON-PUBLIC API. FOR INTERNAL USE ONLY
	 * @return The name of the field for this property
	 */
	String getFieldName();

	/**
	 * Tests to see if this property has a valid match for the given object
	 *
	 * @param object The object to test
	 * @return {@code true} if this property is present on the given object, and matches what is set for this property
	 */
	boolean appliesTo(Object object);

	/**
	 * Gets the given field from the given object, provided it is of the given type
	 * @param object The object
	 * @param fieldName The field to get
	 * @param expectedType The field's type
	 * @param <T> The type of field
	 * @return Either the value of the field, or null if it's not determinable or has no value
	 */
	@CheckForNull
	@SuppressWarnings("deprecation")
	default <T> T getField(Object object, String fieldName, Class<T> expectedType) {
		Class<?> objectClass = object.getClass();

		try {
			Field f = objectClass.getDeclaredField(fieldName);
			boolean accessible = f.isAccessible();
			if (!accessible) {
				f.setAccessible(true);
			}
			Object fieldValue = f.get(object);
			if (!accessible) {
				f.setAccessible(false);
			}

			if (fieldValue != null && (expectedType == null || expectedType.isAssignableFrom(fieldValue.getClass()))) {
				@SuppressWarnings("unchecked")
				T castValue = (T) fieldValue;

				return castValue;
			}
		} catch (NoSuchFieldException | IllegalAccessException e) {
			// Silent ignore
		}

		return null;
	}


}
