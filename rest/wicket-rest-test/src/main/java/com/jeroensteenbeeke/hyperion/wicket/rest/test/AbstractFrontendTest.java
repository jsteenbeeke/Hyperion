package com.jeroensteenbeeke.hyperion.wicket.rest.test;

import com.google.common.collect.ImmutableList;
import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;
import com.jeroensteenbeeke.hyperion.rest.query.MutableResource;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.OrderBy;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import com.jeroensteenbeeke.hyperion.solitary.InMemory;
import com.jeroensteenbeeke.hyperion.solstice.spring.resteasy.TestMode;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.apache.wicket.Application;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.After;
import org.junit.Before;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Basic test class for testing a frontend. This means that the frontend is started, but the associated backend is mocked in a manner specified
 * in each test case. Provides abstractions for working with ListableResource
 */
public abstract class AbstractFrontendTest implements TestMode.IProxyProvider {
	private static final Logger log = LoggerFactory.getLogger(AbstractFrontendTest.class);

	private WicketTester tester;

	private InMemory.Handler handler;

	private Map<Class<?>, Object> proxies;

	/**
	 * Creates the callback handler (i.e. starts the application)
	 *
	 * @return The handler to use to stop the application
	 * @throws Exception If anything goes wrong
	 */
	protected abstract InMemory.Handler createApplicationHandler() throws Exception;

	/**
	 * Returns the key under which the Wicket application is registered in web.xml
	 *
	 * @return The application key
	 */
	protected abstract String getApplicationKey();

	/**
	 * Starts the application, figures out which Wicket application we're running, and sets up the proxy provider to produce mocks
	 *
	 * @throws Exception If something happens
	 */
	@Before
	public void startApplicationAndInitializeProxyHandler() throws Exception {
		this.handler = createApplicationHandler();

		WebApplication app = (WebApplication) Application.get(getApplicationKey());

		assertNotNull(app);

		tester = new WicketTester(app, false);

		proxies = new HashMap<>();

		TestMode.init(this);
	}

	/**
	 * Returns the current WicketTester instance
	 *
	 * @return The WicketTester
	 */
	protected WicketTester wicket() {
		return tester;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T proxy(Class<T> targetClass) {
		if (!proxies.containsKey(targetClass)) {
			log.warn("No proxy for {}, defaulting to noop", targetClass.getName());

			proxies.put(targetClass, mock(targetClass));
		}


		return (T) proxies.get(targetClass);
	}

	/**
	 * Create a proxy for the given class, allowing further details to be specified in the subsequent builder
	 *
	 * @param targetClass The class to proxy
	 * @param <T>         The type of ListableResource
	 * @param <R>         The type of data object returned by the resource
	 * @return A builder
	 */
	protected <T extends ListableResource<R, ?>, R> WithReadableDataset<T, R> readable(Class<T> targetClass) {
		return dataSet -> {
			T resource = mock(targetClass);

			when(resource.list(any(), any(), any())).thenAnswer((Answer<Response>) invocation -> respond(Response.Status.OK)
				.withEntity(dataSet));
			when(resource.count(any())).thenAnswer((Answer<Response>) invocation -> respond(Response.Status.OK).withEntity(Integer
																															   .toString(dataSet
																																			 .size())));

			proxies.put(targetClass, resource);
		};
	}

	/**
	 * Create a proxy for the given class, allowing further details to be specified in the subsequent builder
	 *
	 * @param targetClass The class to proxy
	 * @param <T>         The type of ListableResource
	 * @param <R>         The type of data object returned by the resource
	 * @param <ID>        The type of ID for the data object
	 * @param <Q>         The type of query object used for this type
	 * @return A builder
	 */
	protected <T extends ListableResource<R, Q> & MutableResource<R, ID>, R, ID extends Serializable, Q extends QueryObject<R>> WithMutableDataset<T, R, ID> mutable(Class<T> targetClass) {
		return initialData -> identifierFunction -> newIdGenerator -> newIdAssigner -> {
			T resource = mock(targetClass);

			Map<ID, R> repository = new LinkedHashMap<>();
			initialData.forEach(r -> repository.put(identifierFunction.apply(r), r));

			when(resource.get(any())).thenAnswer((Answer<Response>) invocation -> {
				ID id = invocation.getArgument(0);

				if (!repository.containsKey(id)) {
					return respond(Response.Status.NOT_FOUND).withEntity("No object with ID " + id);
				}

				return respond(Response.Status.OK).withEntity(repository.get(id));
			});
			when(resource.list(any(), any(), any())).thenAnswer((Answer<Response>) invocation -> {
				Q query = invocation.getArgument(0);
				Long offset = invocation.getArgument(1);
				Long count = invocation.getArgument(2);

				return respond(Response.Status.OK).withEntity(applyFilter(repository.values(), query, offset, count));
			});

			when(resource.count(any())).thenAnswer((Answer<Response>) invocation -> respond(Response.Status.OK).withEntity(Integer
																															   .toString(repository
																																			 .size())));
			when(resource.create(any())).thenAnswer((Answer<Response>) invocation -> {
				R argument = invocation.getArgument(0);
				newIdAssigner.accept(argument, newIdGenerator.apply(repository.keySet().stream()));
				repository.put(identifierFunction.apply(argument), argument);

				return respond(Response.Status.CREATED).withEntity(argument);
			});
			when(resource.update(any(), any())).thenAnswer((Answer<Response>) invocation -> {
				R argument = invocation.getArgument(1);
				ID id = invocation.getArgument(0);

				if (!repository.containsKey(id)) {
					return respond(Response.Status.NOT_FOUND).withEntity("No object with ID " + id);
				}

				repository.put(id, argument);

				return respond(Response.Status.OK).withEntity(argument);
			});
			when(resource.delete(any())).thenAnswer(
				(Answer<Response>) invocation -> {
					ID id = invocation.getArgument(0);

					if (!repository.containsKey(id)) {
						return respond(Response.Status.NOT_FOUND).withEntity("No object with ID " + id);
					}

					repository.remove(id);

					return respond(Response.Status.NO_CONTENT).withEntity(null);
				}
			);

			proxies.put(targetClass, resource);
		};
	}

	private <R, Q extends QueryObject<R>> List<R> applyFilter(Collection<R> values, Q query, Long offset, Long count) {
		final Stream<R> stream = values.stream().filter(query::matches);

		List<R> filtered = createComparator(query).map(stream::sorted).orElse(stream).collect(Collectors.toList());

		if (offset != null && count != null) {
			if (filtered.size() <= offset) {
				return ImmutableList.of();
			}

			return filtered.subList(offset.intValue(), Math.min(filtered.size(), offset.intValue() + count.intValue()));
		}

		return filtered;
	}

	private <R, Q extends QueryObject<R>> Optional<Comparator<R>> createComparator(Q query) {
		Class<? extends QueryObject> queryClass = query.getClass();

		List<Comparator<R>> comparators = new ArrayList<>(queryClass.getDeclaredFields().length);

		Map<Integer, Comparator<R>> sortProperties = new TreeMap<>();

		for (Field declaredField : queryClass.getDeclaredFields()) {
			if (IQueryProperty.class.isAssignableFrom(declaredField.getType())) {
				Option<IQueryProperty<Q>> optionalProp = extractField(query, declaredField.getName());
				optionalProp.forEach(prop -> {
					OrderBy propertyOrder = prop.getPropertyOrder();

					if (propertyOrder != OrderBy.IGNORE) {
						Comparator<R> propertyComparator = createComparator(prop);

						sortProperties.put(propertyOrder.getIndex(), propertyComparator);
					}
				});
			}
		}

		sortProperties.forEach((i, c) -> comparators.add(c));

		return comparators.stream().reduce(Comparator::thenComparing);
	}

	private <T extends Comparable<T>, R, Q extends QueryObject<R>> Comparator<R> createComparator(IQueryProperty<Q> prop) {
		return (a,b) -> {
			@SuppressWarnings("unchecked")
			T fieldA = (T) extractField(a, prop.getFieldName()).getOrNull();
			@SuppressWarnings("unchecked")
			T fieldB = (T) extractField(b, prop.getFieldName()).getOrNull();

			return fieldA.compareTo(fieldB);
		};
	}

	@SuppressWarnings({"deprecation", "unchecked"})
	private <T> Option<T> extractField(@Nonnull Object o, String field) {
		Class<?> objectClass = o.getClass();

		Field declaredField;

		try {
			declaredField = objectClass.getDeclaredField(field);
		} catch (NoSuchFieldException e) {
			return Option.none();
		}

		boolean accessible = declaredField.isAccessible();

		if (!accessible) {
			declaredField.setAccessible(true);
		}

		T value;

		try {
			value = (T) declaredField.get(o);
		} catch (IllegalAccessException e) {
			return Option.none();
		}

		return Option.some(value);
	}

	/**
	 * Starts building a response
	 *
	 * @param status The response status
	 * @param <T>    The type of response entity
	 * @return A builder
	 */
	@Nonnull
	protected <T> WithEntity<T> respond(@Nonnull Response.Status status) {
		return entity -> {
			Response response = mock(Response.class);
			when(response.getStatus()).thenReturn(status.getStatusCode());
			when(response.getStatusInfo()).thenReturn(status);
			when(response.hasEntity()).thenReturn(true);
			when(response.readEntity(any(Class.class))).thenReturn(entity);
			when(response.readEntity(any(GenericType.class))).thenReturn(entity);

			return response;

		};
	}

	/**
	 * Builder
	 *
	 * @param <T> The type of entity returned
	 */
	@FunctionalInterface
	protected interface WithEntity<T> {
		/**
		 * Builds the response, returning the given entity
		 *
		 * @param entity The entity to return
		 * @return A JAX-RS response
		 */
		Response withEntity(T entity);
	}

	/**
	 * Builder step that allows specifying the dataset for the proxy
	 *
	 * @param <T>  The type of resource
	 * @param <R>  The type of data object
	 * @param <ID> The type of identifier
	 */
	@FunctionalInterface
	protected interface WithMutableDataset<T extends ListableResource<R, ?> & MutableResource<R, ID>, R, ID extends Serializable> {
		/**
		 * Specify the data used by the proxy
		 *
		 * @param dataSet The data
		 * @return The next builder step
		 */
		WithIdentifier<T, R, ID> withDataSet(List<R> dataSet);
	}

	/**
	 * Builder step that instructs the proxy on how to get the ID of the data object
	 *
	 * @param <T>  The type of resource
	 * @param <R>  The type of data object
	 * @param <ID> The type of identifier
	 */
	protected interface WithIdentifier<T extends ListableResource<R, ?> & MutableResource<R, ID>, R, ID extends Serializable> {
		/**
		 * Sets the identifier function to get the ID from the data object and creates the proxy
		 *
		 * @param identifierGetter The identifier function
		 * @return The next builder step
		 */
		WithIdentifierGenerator<T, R, ID> identifiedBy(Function<R, ID> identifierGetter);
	}

	/**
	 * Builder step to set how new identifiers are determined
	 *
	 * @param <T>  The type of resource
	 * @param <R>  The type of data object
	 * @param <ID> The type of identifier
	 */
	protected interface WithIdentifierGenerator<T extends ListableResource<R, ?> & MutableResource<R, ID>, R, ID extends Serializable> {
		/**
		 * Sets the generator function for new IDs.  Takes a stream of all existing IDs and outputs a new one
		 *
		 * @param generatorFunction The generator function
		 * @return The next builder step
		 */
		WithIdentifierSetter<T, R, ID> withIdentifierGenerator(Function<Stream<ID>, ID> generatorFunction);
	}

	/**
	 * Default generator for long-based IDs
	 *
	 * @return A generator
	 */
	protected Function<Stream<Long>, Long> longIdGenerator() {
		return stream -> stream.max(Comparator.naturalOrder()).map(i -> i + 1).orElse(1L);
	}

	/**
	 * Default generator for UUIDs as IDs
	 *
	 * @return A generator
	 */
	protected Function<Stream<String>, String> uuidGenerator() {
		return stream -> {
			UUID next = UUID.randomUUID();

			if (stream.noneMatch(next.toString()::equals)) {
				return next.toString();
			} else {
				return uuidGenerator().apply(stream);
			}
		};
	}

	/**
	 * Builder step for defining the identifier setter
	 *
	 * @param <T>  The type of resource
	 * @param <R>  The type of data object
	 * @param <ID> The type of identifier
	 */
	protected interface WithIdentifierSetter<T extends ListableResource<R, ?> & MutableResource<R, ID>, R, ID extends Serializable> {
		/**
		 * Sets the setter for the data object's ID
		 *
		 * @param setter The setter
		 */
		void withIdentifierSetter(@Nonnull BiConsumer<R, ID> setter);
	}

	/**
	 * Builder step that allows specifying the dataset for the proxy
	 *
	 * @param <T> The type of resource
	 * @param <R> The type of data object
	 */
	@FunctionalInterface
	protected interface WithReadableDataset<T extends ListableResource<R, ?>, R> {
		/**
		 * Specify the data used by the proxy and creates it
		 *
		 * @param dataSet The data
		 */
		void withDataSet(List<R> dataSet);
	}

	/**
	 * Shuts down the in-memory application
	 */
	@After
	public void shutdownApplication() {
		this.handler.terminate();

		proxies = new HashMap<>();
	}
}
