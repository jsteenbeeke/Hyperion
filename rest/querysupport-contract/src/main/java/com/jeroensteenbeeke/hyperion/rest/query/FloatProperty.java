package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.annotation.Nonnull;
import java.math.BigInteger;

/**
 * Query object property representing a Float value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class FloatProperty<T extends QueryObject<?>> extends ComparableProperty<T, Float> {
	/**
	 * Create a new Float property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public FloatProperty(@Nonnull T target, @Nonnull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@Nonnull Float aFloat) {
		return aFloat.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a FloatProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of FloatProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressFBWarnings(value = "NP_NONNULL_PARAM_VIOLATION", justification = "Method primarily invoked by JAX-RS to " +
			"be used in a RESTQueryBinding. Additional operations on the query object make no sense at this point")
	public static <T extends QueryObject<?>> FloatProperty<T> fromString(@Nonnull String input) {
		return fromString(new FloatProperty<T>(null, null), input, Float::parseFloat);
	}
}
