package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

/**
 * Query object property representing a BigDecimal value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class BigDecimalProperty<T extends QueryObject<?>> extends ComparableProperty<T, BigDecimal> {
	/**
	 * Create a new BigDecimal property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public BigDecimalProperty(@Nonnull T target, @Nonnull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@Nonnull BigDecimal bigDecimal) {
		return bigDecimal.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a BigDecimalProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of BigDecimalProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressFBWarnings(value = "NP_NONNULL_PARAM_VIOLATION", justification = "Method primarily invoked by JAX-RS to " +
			"be used in a RESTQueryBinding. Additional operations on the query object make no sense at this point")
	public static <T extends QueryObject<?>> BigDecimalProperty<T> fromString(@Nonnull String input) {
		return fromString(new BigDecimalProperty<T>(null, null), input, BigDecimal::new);
	}
}
