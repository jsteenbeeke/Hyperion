package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.*;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Query property that resembles a boolean. Boolean properties are serialized to JSON as follows:
 * <p>
 * {@code
 * <p>
 * {
 * 'value': 'true',
 * 'another_value': 'false',
 * 'not_value': '!true',
 * 'not_another_value': '!false',
 * 'explicit_null': '#',
 * "explicit_not_null': '!#'
 * <p>
 * }
 * <p>
 * }
 *
 * @param <T> The type of object this Boolean property is tied to
 */
public class BooleanProperty<T extends QueryObject<?>> implements IBooleanProperty<T> {
	private static final long serialVersionUID = 5373733539239628113L;

	private final T target;

	private final String fieldName;

	private Boolean value = null;

	private boolean negated = false;

	private boolean shouldNegateNext = false;

	private boolean explicitNull = false;

	private OrderBy orderBy = OrderBy.IGNORE;

	/**
	 * Creates a new BooleanProperty
	 *
	 * @param target    The containing object
	 * @param fieldName The name of the field this property is assigned to
	 */
	public BooleanProperty(@Nonnull T target, @Nonnull String fieldName) {
		this.target = target;
		this.fieldName = fieldName;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Nonnull
	@Override
	public OrderBy getPropertyOrder() {
		return orderBy;
	}

	@Override
	public void setPropertyOrder(@Nonnull OrderBy orderBy) {
		this.orderBy = orderBy;
	}


	/**
	 * Specifies that this property should be (not) equal to the given value
	 *
	 * @param value The value to compare against
	 * @return The query object
	 */
	@Nonnull
	public T equalTo(boolean value) {
		this.value = value;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property's query should be negated. Usually this method is called
	 * prior to calling one of the other methods to negate this, for example: {@code property.not().equalTo("value")}
	 *
	 * @return This property object
	 */
	@Nonnull
	public BooleanProperty<T> not() {
		this.shouldNegateNext = true;
		return this;
	}

	/**
	 * Specifies that the current property should not have a value
	 *
	 * @return The query object
	 */
	@Nonnull
	public T isNull() {
		this.explicitNull = true;
		this.value = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Returns the value that was set, if one exists
	 *
	 * @return An Optional that may contain a boolean value
	 */
	public Optional<Boolean> getValue() {
		return Optional.ofNullable(value);
	}

	@Override
	public Boolean getUnwrappedValue() {
		return value;
	}

	/**
	 * Return whether or not the current operation should be negated
	 *
	 * @return {@code true} if the operation should be negated, {@code false} otherwise
	 */
	public boolean isNegated() {
		return negated;
	}

	/**
	 * Return whether or not the property should be checked for an explicit null value
	 *
	 * @return {@code true} if the property should be null (or not-null if {@link #isNegated()} returns true), or
	 * {@code false} if a normal equality check should be performed
	 */
	public boolean isExplicitNull() {
		return explicitNull;
	}

	@Override
	public boolean isSet() {
		return value != null || explicitNull;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BooleanProperty)) return false;

		BooleanProperty<?> that = (BooleanProperty<?>) o;

		if (fieldName != null ? !fieldName.equals(that.fieldName) : that.fieldName != null)
			return false;
		if (negated != that.negated) return false;
		if (shouldNegateNext != that.shouldNegateNext) return false;
		if (explicitNull != that.explicitNull) return false;
		return value != null ? value.equals(that.value) : that.value == null;
	}

	@Override
	public int hashCode() {
		int result = (negated ? 1 : 0);
		result = 31 * result + (fieldName != null ? fieldName.hashCode() : 0);
		result = 31 * result + (value != null ? value.hashCode() : 0);
		result = 31 * result + (shouldNegateNext ? 1 : 0);
		result = 31 * result + (explicitNull ? 1 : 0);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder r = new StringBuilder();

		OrderByLogic.applyOrderBy(orderBy, r);

		if (!isSet()) {
			return r.toString();
		}

		if (isNegated()) {
			r.append('!');
		}

		if (isExplicitNull()) {
			r.append("#");
		} else {
			getValue().ifPresent(r::append);
		}

		return r.toString();
	}

	/**
	 * Creates a BooleanProperty from the given input String
	 *
	 * @param representation The String representation of the given property
	 * @param <T>            The type of containing object
	 * @return A BooleanProperty expressing the given String query. Please keep in mind that the object
	 * returned here will violate the not-null constraint of the target, so calling additional query methods
	 * on objects returned by the JAX-RS implementation will cause NullPointerExceptions. Seeing as they are not meant
	 * to be used this way, this is acceptable
	 */
	@SuppressFBWarnings(value = "NP_NONNULL_PARAM_VIOLATION", justification = "Method primarily invoked by JAX-RS to " +
			"be used in a RESTQueryBinding. Additional operations on the query object make no sense at this point")
	public static <T extends QueryObject<?>> BooleanProperty<T> fromString(@Nonnull String representation) {
		BooleanProperty<T> property = new BooleanProperty<>(null, null);
		String input = OrderByLogic.extractOrderBy(representation, property);

		if (input.isEmpty()) {
			return property;
		}

		if (input.startsWith("!")) {
			// Negation
			input = input.substring(1);
			property.not();
		}

		if (input.equals(Boolean.TRUE.toString())) {
			property.equalTo(true);
		} else if (input.equals(Boolean.FALSE.toString())) {
			property.equalTo(false);
		} else if (input.equals("#")) {
			property.isNull();
		}

		return property;
	}

	@Override
	public T orderBy(boolean ascending) {
		if (shouldNegateNext) {
			throw new IllegalStateException("OrderBy cannot be negated");
		}

		orderBy = new OrderBy(target.getNextSortIndex(), ascending);

		return target;
	}
}
