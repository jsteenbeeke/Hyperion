package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.*;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.annotation.Nonnull;

import static com.jeroensteenbeeke.hyperion.rest.query.Assertions.notNull;

/**
 * Query property that resembles a string comparison. Values of this String are serialized as follows:
 * <p>
 * {@code
 * <p>
 * {
 * 'equal': '=value',
 * 'not_equal': '!=value',
 * 'equal_ignorecase': '_=value',
 * 'not_equal_ignorecase': '!_=value',
 * 'like': '~value',
 * 'not_like': '!~value',
 * 'like_ignorecase': '_~value',
 * 'not_like_ignorecase': '!_~value',
 * 'null': '#',
 * 'not_null': '!#'
 * }
 * <p>
 * }
 *
 * @param <T> The type of object this String property is tied to
 */
public class StringProperty<T extends QueryObject<?>> implements IStringProperty<T> {
	private final T target;

	private final String fieldName;

	private StringComparisonType type = StringComparisonType.NONE;

	private String value = "";

	private boolean negated = false;

	private boolean shouldNegateNext = false;

	private OrderBy orderBy = OrderBy.IGNORE;

	/**
	 * Creates a new StringProperty
	 *
	 * @param target    The containing object
	 * @param fieldName The name of the field this property is assigned to
	 */
	public StringProperty(@Nonnull T target, @Nonnull String fieldName) {
		this.target = target;
		this.fieldName = fieldName;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Nonnull
	@Override
	public OrderBy getPropertyOrder() {
		return orderBy;
	}

	@Override
	public void setPropertyOrder(@Nonnull OrderBy orderBy) {
		this.orderBy = orderBy;
	}


	/**
	 * Specifies that this property should be (not) equal to the given value
	 *
	 * @param value The value to compare against
	 * @return The query object
	 */
	@Nonnull
	public T equalTo(@Nonnull String value) {
		this.type = StringComparisonType.EQUALS;
		this.value = notNull("value", value);
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that this property should be (not) equal to the given value, using
	 * a case-insensitive comparison
	 *
	 * @param value The value to compare against
	 * @return The query object
	 */
	@Nonnull
	public T equalToIgnoreCase(@Nonnull String value) {
		this.type = StringComparisonType.EQUALS_IGNORECASE;
		this.value = notNull("value", value);
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property should be a case sensitive LIKE comparison.
	 *
	 * @param value The value to compare against (where {@code %} counts as a wildcard)
	 * @return The query object
	 */
	@Nonnull
	public T like(@Nonnull String value) {
		this.type = StringComparisonType.LIKE;
		this.value = notNull("value", value);
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property should be a case insensitive LIKE comparison.
	 *
	 * @param value The value to compare against (where {@code %} counts as a wildcard)
	 * @return The query object
	 */
	@Nonnull
	public T ilike(@Nonnull String value) {
		this.type = StringComparisonType.ILIKE;
		this.value = notNull("value", value);
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property should be unset
	 *
	 * @return The query object
	 */
	@Nonnull
	public T isNull() {
		this.type = StringComparisonType.NULL;
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		this.value = "";
		return target;
	}

	/**
	 * Specifies that the current property's query should be negated. Usually this method is called
	 * prior to calling one of the other methods to negate this, for example: {@code property.not().equalTo("value")}
	 *
	 * @return This property object
	 */
	@Nonnull
	public StringProperty<T> not() {
		this.shouldNegateNext = true;
		return this;
	}

	/**
	 * Returns the type of comparison done by this property.
	 *
	 * @return The {@link StringComparisonType} denoting the type of comparison done by this property
	 */
	@Nonnull
	public StringComparisonType getType() {
		return type;
	}

	/**
	 * Returns the value of the current property
	 *
	 * @return A String containing the value of this field
	 */
	@Nonnull
	public String getValue() {
		return value;
	}

	/**
	 * Whether or not the current comparison should be negated
	 *
	 * @return {@code true} if the comparison should be negated, {@code false} otherwise
	 */
	public boolean isNegated() {
		return negated;
	}

	@Override
	public boolean isSet() {
		return getType() != StringComparisonType.NONE;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StringProperty<?> that = (StringProperty<?>) o;

		if (negated != that.negated) return false;
		if (shouldNegateNext != that.shouldNegateNext) return false;
		if (fieldName != null ? !fieldName.equals(that.fieldName) : that.fieldName != null)
			return false;
		if (type != that.type) return false;
		return value.equals(that.value);
	}


	@Override public int hashCode() {
		int result = fieldName != null ? fieldName.hashCode() : 0;
		result = 31 * result + type.hashCode();
		result = 31 * result + value.hashCode();
		result = 31 * result + (negated ? 1 : 0);
		result = 31 * result + (shouldNegateNext ? 1 : 0);
		return result;
	}

	/**
	 * Creates a StringProperty from the given input String
	 *
	 * @param representation The String representation of the given property
	 *
	 * @param <T>            The type of containing object
	 * @return A StringProperty expressing the given String query. Please keep in mind that the object
	 * returned here will violate the not-null constraint of the target, so calling additional query methods
	 * on objects returned by the JAX-RS implementation will cause NullPointerExceptions. Seeing as they are not meant
	 * to be used this way, this is acceptable
	 */
	@SuppressFBWarnings(value = "NP_NONNULL_PARAM_VIOLATION", justification = "Method primarily invoked by JAX-RS to " +
			"be used in a RESTQueryBinding. Additional operations on the query object make no sense at this point")
	public static <T extends QueryObject<?>> StringProperty<T> fromString(@Nonnull String representation) {
		StringProperty<T> property = new StringProperty<>(null, null);
		String input = OrderByLogic.extractOrderBy(representation, property);

		if (input.isEmpty()) {
			return property;
		}

		if (input.startsWith("!")) {
			// Negation
			input = input.substring(1);
			property.not();
		}

		if (input.startsWith("=")) {
			// EQUALS
			property.equalTo(input.substring(1));
		} else if (input.startsWith("_=")) {
			// EQUALS, IGNORE CASE
			property.equalToIgnoreCase(input.substring(2));
		} else if (input.startsWith("~")) {
			// LIKE
			property.like(input.substring(1));
		} else if (input.startsWith("_~")) {
			// ILIKE
			property.ilike(input.substring(2));
		} else if (input.equals("#")) {
			property.isNull();
		} else if (!input.isEmpty()) {
			// Unspecified, default to EQUALS
			property.equalTo(input);
		}

		return property;
	}

	@Override
	public String toString() {
		StringBuilder r = new StringBuilder();

		OrderByLogic.applyOrderBy(orderBy, r);

		if (!isSet()) {
			return r.toString();
		}

		if (isNegated()) {
			r.append('!');
		}

		switch (getType()) {
			case NONE:
				return "";
			case NULL:
				r.append("#");
				return r.toString();
			case LIKE:
				r.append("~");
				break;
			case ILIKE:
				r.append("_~");
				break;
			case EQUALS:
				r.append("=");
				break;
			case EQUALS_IGNORECASE:
				r.append("_=");
				break;
		}

		r.append(getValue());

		return r.toString();
	}

	@Override
	public T orderBy(boolean ascending) {
		if (shouldNegateNext) {
			throw new IllegalStateException("OrderBy cannot be negated");
		}

		orderBy = new OrderBy(target.getNextSortIndex(), ascending);

		return target;
	}
}
