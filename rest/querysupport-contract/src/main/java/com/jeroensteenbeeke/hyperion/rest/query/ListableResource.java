package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * Parent interface for JAX-RS interfaces that represent basic query-backed list behavior
 *
 * @param <TYPE>  The type of object returned in the list
 * @param <QUERY> The type of query object returned by this list resource
 */
public interface ListableResource<TYPE, QUERY extends QueryObject<TYPE>> {
	/**
	 * Returns a list of type TYPE that corresponds to the given query
	 *
	 * @param query  The query object
	 * @param offset The optional starting index of the results
	 * @param count  The maximum number of results
	 * @return A Response either containing a list of TYPE objects, or an error code explaining
	 * why the list could not be obtained
	 */
	Response list(QUERY query, Long offset, Long count);

	/**
	 * Counts the number of results to the given query
	 * @param query The query object
	 * @return A Response either containing a count of the number of TYPE objects, or an error code
	 * explaining why the count could not be determined
	 */
	Response count(QUERY query);

	/**
	 * Returns the name of the current service, defaulting to {@code getClass().getSimpleName()}
	 * @return The name of this resource
	 */
	@GET
	@Path("/meta/name")
	@Produces(MediaType.TEXT_PLAIN)
	default String getName() {
		return getClass().getSimpleName();
	}
}
