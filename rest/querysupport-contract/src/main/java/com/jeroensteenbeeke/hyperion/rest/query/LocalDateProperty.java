package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 * Query object property representing a LocalDate value, that can be queried as any comparable
 * @param <T> The type of query object that contains this property
 */
public class LocalDateProperty<T extends QueryObject<?>> extends ComparableProperty<T,LocalDate> {

	/**
	 * Create a new LocalDate property for the given query object
	 * @param target The query object containing this property
	 *               @param fieldName The name of the field this property is assigned to
	 */
	public LocalDateProperty(@Nonnull T target, @Nonnull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@Nonnull LocalDate localDate) {
		return DateTimeFormatter.ISO_DATE.format(localDate);
	}

	/**
	 * Parses the given String and attempts to turn it into a LocalDateProperty
	 * @param input The input to parse
	 * @param <T> The type of containing query object
	 * @return An instance of LocalDateProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressFBWarnings(value = "NP_NONNULL_PARAM_VIOLATION", justification = "Method primarily invoked by JAX-RS to " +
			"be used in a RESTQueryBinding. Additional operations on the query object make no sense at this point")
	public static <T extends QueryObject<?>> LocalDateProperty<T> fromString(@Nonnull String input) {
		return fromString(new LocalDateProperty<T>(null, null), input, i -> LocalDate.parse(i, DateTimeFormatter.ISO_DATE));
	}
}
