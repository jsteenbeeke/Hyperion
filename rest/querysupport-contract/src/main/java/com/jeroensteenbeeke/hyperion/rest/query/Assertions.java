package com.jeroensteenbeeke.hyperion.rest.query;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Convenience class for parameter assertions. Essentially duplicates functionality
 * from Hyperion Core but we do not want to add that as a dependency
 */
class Assertions {
	/**
	 * Ensures the given parameter is non-null
	 * @param name The name of the parameter
	 * @param input The parameter
	 * @param <T> The type of the parameter
	 * @return The input
	 * @throws IllegalArgumentException If the input is {@code null}
	 */
	static <T> T notNull(@Nonnull String name, @Nullable T input) {
		if (input == null) {
			throw new IllegalArgumentException(String.format("Parameter %s should not be null",
					name));
		}

		return input;
	}
}
