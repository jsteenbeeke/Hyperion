package com.jeroensteenbeeke.hyperion.rest.query;

import javax.ws.rs.core.Response;
import java.io.Serializable;

/**
 * Parent interface for JAX-RS interfaces that represent basic create, update and delete behavior
 *
 * @param <TYPE>  The type of object mutated by these operations
 * @param <ID> The type of identifier for mutable objects
 */
public interface MutableResource<TYPE,ID extends Serializable> {
	/**
	 * Get a single instance by ID
	 * @param id The ID of the instance
	 * @return A response with status 200 (OK) and the found object, or status 404 if the ID is invalid
	 */
	Response get(ID id);
	/**
	 * Create a new instance of the given type
	 * @param input The type to create
	 * @return A response with status 201 (Created), a URI and the created object
	 */
	Response create(TYPE input);

	/**
	 * Updates a given instance, identified by the given ID
	 * @param identifier The ID of the instance to update
	 * @param input The updated data
	 * @return A response with status 200 (OK) and the updated object
	 */
	Response update(ID identifier, TYPE input);

	/**
	 * Removes the instance with the given identifier
	 * @param identifier The identifier of the instance to remove
	 * @return A response with status 204 (No Content)
	 */
	Response delete(ID identifier);
}
