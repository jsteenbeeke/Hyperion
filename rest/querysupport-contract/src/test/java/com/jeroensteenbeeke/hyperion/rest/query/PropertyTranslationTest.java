package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.ComparableComparisonType;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import com.jeroensteenbeeke.hyperion.rest.querysupport.StringComparisonType;
import com.jeroensteenbeeke.hyperion.test.Equivalence;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static org.junit.Assert.*;

/**
 * Basic test for serializing queries to/from JSON
 */
public class PropertyTranslationTest {
	/**
	 * Tests if query properties of type BigDecimal are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicBigDecimalConversion() throws IOException {
		final BigDecimal FOUR = new BigDecimal(4);
		final BigDecimal FIVE = new BigDecimal(5);

		assertConversion("=4", new TestObject().bigdecimal().equalTo(FOUR),
				TestObject::bigdecimal);
		assertConversion("=4.0", new TestObject().bigdecimal().equalTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("<4", new TestObject().bigdecimal().lessThan(FOUR),
				TestObject::bigdecimal);
		assertConversion("<4.0", new TestObject().bigdecimal().lessThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("<=4", new TestObject().bigdecimal().lessThanOrEqualTo(FOUR),
				TestObject::bigdecimal);
		assertConversion("<=4.0", new TestObject().bigdecimal().lessThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion(">4", new TestObject().bigdecimal().greaterThan(FOUR),
				TestObject::bigdecimal);
		assertConversion(">4.0", new TestObject().bigdecimal().greaterThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion(">=4", new TestObject().bigdecimal().greaterThanOrEqualTo(FOUR),
				TestObject::bigdecimal);
		assertConversion(">=4.0", new TestObject().bigdecimal().greaterThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("[4,5]", new TestObject().bigdecimal().between(FOUR, FIVE),
				TestObject::bigdecimal);
		assertConversion("[4.0,5]", new TestObject().bigdecimal().between(FOUR.setScale(1), FIVE),
				TestObject::bigdecimal);
		assertConversion("#", new TestObject().bigdecimal().isNull(), TestObject::bigdecimal);
		assertConversion("!=4", new TestObject().bigdecimal().not().equalTo(FOUR),
				TestObject::bigdecimal);
		assertConversion("!=4.0", new TestObject().bigdecimal().not().equalTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!<4", new TestObject().bigdecimal().not().lessThan(FOUR),
				TestObject::bigdecimal);
		assertConversion("!<4.0", new TestObject().bigdecimal().not().lessThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!<=4", new TestObject().bigdecimal().not().lessThanOrEqualTo(FOUR),
				TestObject::bigdecimal);
		assertConversion("!<=4.0", new TestObject().bigdecimal().not().lessThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!>4", new TestObject().bigdecimal().not().greaterThan(FOUR),
				TestObject::bigdecimal);
		assertConversion("!>4.0", new TestObject().bigdecimal().not().greaterThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!>=4", new TestObject().bigdecimal().not().greaterThanOrEqualTo(FOUR),
				TestObject::bigdecimal);
		assertConversion("!>=4.0", new TestObject().bigdecimal().not().greaterThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("![4,5]", new TestObject().bigdecimal().not().between(FOUR, FIVE),
				TestObject::bigdecimal);
		assertConversion("![4.0,5]",
				new TestObject().bigdecimal().not().between(FOUR.setScale(1), FIVE),
				TestObject::bigdecimal);
		assertConversion("!#", new TestObject().bigdecimal().not().isNull(),
				TestObject::bigdecimal);

		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						false),
				"=4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.EQUALS, false),
				"=4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						false),
				"<4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.LESS_THAN, false),
				"<4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false),
				"<=4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false), "<=4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, false),
				">4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.GREATER_THAN,
						false),
				">4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO, false), ">=4.0");
		assertObject(BigDecimalProperty.class, p -> assertBetween(FOUR, FIVE, p, false), "[4,5]");
		assertObject(BigDecimalProperty.class, p -> assertBetween(FOUR.setScale(1), FIVE, p,
				false),
				"[4.0,5]");
		assertObject(BigDecimalProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						true),
				"!=4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.EQUALS, true),
				"!=4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						true),
				"!<4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.LESS_THAN, true),
				"!<4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true), "!<=4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.GREATER_THAN,
						true),
				"!>4.0");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=4");
		assertObject(BigDecimalProperty.class,
				p -> assertComparableSingularOperation(FOUR.setScale(1), p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO, true), "!>=4.0");
		assertObject(BigDecimalProperty.class, p -> assertBetween(FOUR, FIVE, p, true), "![4,5]");
		assertObject(BigDecimalProperty.class, p -> assertBetween(FOUR.setScale(1), FIVE, p, true),
				"![4.0,5]");
		assertObject(BigDecimalProperty.class, p -> assertNull(p, true), "!#");
		assertConversion("![4.0,5]",
				new TestObject().bigdecimal().not().between(FOUR.setScale(1), FIVE),
				TestObject::bigdecimal);
		

	}


	/**
	 * Tests if query properties of type BigInteger are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicBigIntegerConversion() throws IOException {
		final BigInteger FOUR = BigInteger.valueOf(4L);
		final BigInteger FIVE = BigInteger.valueOf(5L);

		assertConversion("=4", new TestObject().biginteger().equalTo(FOUR),
				TestObject::biginteger);
		assertConversion("<4", new TestObject().biginteger().lessThan(FOUR),
				TestObject::biginteger);
		assertConversion("<=4", new TestObject().biginteger().lessThanOrEqualTo(FOUR),
				TestObject::biginteger);
		assertConversion(">4", new TestObject().biginteger().greaterThan(FOUR),
				TestObject::biginteger);
		assertConversion(">=4", new TestObject().biginteger().greaterThanOrEqualTo(FOUR),
				TestObject::biginteger);
		assertConversion("[4,5]", new TestObject().biginteger().between(FOUR, FIVE),
				TestObject::biginteger);
		assertConversion("#", new TestObject().biginteger().isNull(), TestObject::biginteger);
		assertConversion("!=4", new TestObject().biginteger().not().equalTo(FOUR),
				TestObject::biginteger);
		assertConversion("!<4", new TestObject().biginteger().not().lessThan(FOUR),
				TestObject::biginteger);
		assertConversion("!<=4", new TestObject().biginteger().not().lessThanOrEqualTo(FOUR),
				TestObject::biginteger);
		assertConversion("!>4", new TestObject().biginteger().not().greaterThan(FOUR),
				TestObject::biginteger);
		assertConversion("!>=4", new TestObject().biginteger().not().greaterThanOrEqualTo(FOUR),
				TestObject::biginteger);
		assertConversion("![4,5]", new TestObject().biginteger().not().between(FOUR, FIVE),
				TestObject::biginteger);
		assertConversion("!#", new TestObject().biginteger().not().isNull(),
				TestObject::biginteger);

		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						false),
				"=4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						false),
				"<4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false),
				"<=4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, false),
				">4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=4");
		assertObject(BigIntegerProperty.class, p -> assertBetween(FOUR, FIVE, p, false), "[4,5]");
		assertObject(BigIntegerProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						true),
				"!=4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						true),
				"!<4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>4");
		assertObject(BigIntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=4");
		assertObject(BigIntegerProperty.class, p -> assertBetween(FOUR, FIVE, p, true), "![4,5]");
		assertObject(BigIntegerProperty.class, p -> assertNull(p, true), "!#");

		

	}

	/**
	 * Tests if query properties of type Boolean are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicBooleanConversion() throws IOException {
		assertConversion("false", new TestObject().booleanVal().equalTo(Boolean.FALSE),
				TestObject::booleanVal);
		assertConversion("true", new TestObject().booleanVal().equalTo(Boolean.TRUE),
				TestObject::booleanVal);
		assertConversion("!false", new TestObject().booleanVal().not().equalTo(Boolean.FALSE),
				TestObject::booleanVal);
		assertConversion("!true", new TestObject().booleanVal().not().equalTo(Boolean.TRUE),
				TestObject::booleanVal);
		assertConversion("#", new TestObject().booleanVal().isNull(), TestObject::booleanVal);
		assertConversion("!#", new TestObject().booleanVal().not().isNull(),
				TestObject::booleanVal);

		

		Assert.assertNull(new TestObject().booleanVal().getUnwrappedValue());

		testEquivalence(TestObject::booleanVal);
		testEquivalence(t -> t.booleanVal().equalTo(true).booleanVal());
		testEquivalence(t -> t.booleanVal().equalTo(false).booleanVal());
		testEquivalence(t -> t.booleanVal().not().equalTo(true).booleanVal());
		testEquivalence(t -> t.booleanVal().not().equalTo(true).booleanVal());
		testEquivalence(t -> t.booleanVal().isNull().booleanVal());
		testEquivalence(t -> t.booleanVal().not().isNull().booleanVal());
	}


	/**
	 * Tests if query properties of type Double are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicDoubleConversion() throws IOException {
		final double FOUR = 4.0;
		final double FIVE = 5.0;

		assertConversion("=4.0", new TestObject().doubleVal().equalTo(FOUR),
				TestObject::doubleVal);
		assertConversion("<4.0", new TestObject().doubleVal().lessThan(FOUR),
				TestObject::doubleVal);
		assertConversion("<=4.0", new TestObject().doubleVal().lessThanOrEqualTo(FOUR),
				TestObject::doubleVal);
		assertConversion(">4.0", new TestObject().doubleVal().greaterThan(FOUR),
				TestObject::doubleVal);
		assertConversion(">=4.0", new TestObject().doubleVal().greaterThanOrEqualTo(FOUR),
				TestObject::doubleVal);
		assertConversion("[4.0,5.0]", new TestObject().doubleVal().between(FOUR, FIVE),
				TestObject::doubleVal);
		assertConversion("#", new TestObject().doubleVal().isNull(), TestObject::doubleVal);
		assertConversion("!=4.0", new TestObject().doubleVal().not().equalTo(FOUR),
				TestObject::doubleVal);
		assertConversion("!<4.0", new TestObject().doubleVal().not().lessThan(FOUR),
				TestObject::doubleVal);
		assertConversion("!<=4.0", new TestObject().doubleVal().not().lessThanOrEqualTo(FOUR),
				TestObject::doubleVal);
		assertConversion("!>4.0", new TestObject().doubleVal().not().greaterThan(FOUR),
				TestObject::doubleVal);
		assertConversion("!>=4.0", new TestObject().doubleVal().not().greaterThanOrEqualTo(FOUR),
				TestObject::doubleVal);
		assertConversion("![4.0,5.0]", new TestObject().doubleVal().not().between(FOUR, FIVE),
				TestObject::doubleVal);
		assertConversion("!#", new TestObject().doubleVal().not().isNull(), TestObject::doubleVal);

		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						false),
				"=4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						false),
				"<4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false),
				"<=4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, false),
				">4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=4.0");
		assertObject(DoubleProperty.class, p -> assertBetween(FOUR, FIVE, p, false), "[4.0,5.0]");
		assertObject(DoubleProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						true),
				"!=4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						true),
				"!<4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>4.0");
		assertObject(DoubleProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=4.0");
		assertObject(DoubleProperty.class, p -> assertBetween(FOUR, FIVE, p, true), "![4.0,5.0]");
		assertObject(DoubleProperty.class, p -> assertNull(p, true), "!#");

		

	}

	/**
	 * Tests if query properties of type Float are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicFloatConversion() throws IOException {
		final float FOUR = 4.0f;
		final float FIVE = 5.0f;

		assertConversion("=4.0", new TestObject().floatVal().equalTo(FOUR), TestObject::floatVal);
		assertConversion("<4.0", new TestObject().floatVal().lessThan(FOUR), TestObject::floatVal);
		assertConversion("<=4.0", new TestObject().floatVal().lessThanOrEqualTo(FOUR),
				TestObject::floatVal);
		assertConversion(">4.0", new TestObject().floatVal().greaterThan(FOUR),
				TestObject::floatVal);
		assertConversion(">=4.0", new TestObject().floatVal().greaterThanOrEqualTo(FOUR),
				TestObject::floatVal);
		assertConversion("[4.0,5.0]", new TestObject().floatVal().between(FOUR, FIVE),
				TestObject::floatVal);
		assertConversion("#", new TestObject().floatVal().isNull(), TestObject::floatVal);
		assertConversion("!=4.0", new TestObject().floatVal().not().equalTo(FOUR),
				TestObject::floatVal);
		assertConversion("!<4.0", new TestObject().floatVal().not().lessThan(FOUR),
				TestObject::floatVal);
		assertConversion("!<=4.0", new TestObject().floatVal().not().lessThanOrEqualTo(FOUR),
				TestObject::floatVal);
		assertConversion("!>4.0", new TestObject().floatVal().not().greaterThan(FOUR),
				TestObject::floatVal);
		assertConversion("!>=4.0", new TestObject().floatVal().not().greaterThanOrEqualTo(FOUR),
				TestObject::floatVal);
		assertConversion("![4.0,5.0]", new TestObject().floatVal().not().between(FOUR, FIVE),
				TestObject::floatVal);
		assertConversion("!#", new TestObject().floatVal().not().isNull(), TestObject::floatVal);

		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						false),
				"=4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						false),
				"<4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false),
				"<=4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, false),
				">4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=4.0");
		assertObject(FloatProperty.class, p -> assertBetween(FOUR, FIVE, p, false), "[4.0,5.0]");
		assertObject(FloatProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						true),
				"!=4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						true),
				"!<4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>4.0");
		assertObject(FloatProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=4.0");
		assertObject(FloatProperty.class, p -> assertBetween(FOUR, FIVE, p, true), "![4.0,5.0]");
		assertObject(FloatProperty.class, p -> assertNull(p, true), "!#");

		

	}

	/**
	 * Tests if query properties of type Integer are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicIntegerConversion() throws IOException {
		final int FOUR = 4;
		final int FIVE = 5;

		assertConversion("=4", new TestObject().integer().equalTo(FOUR), TestObject::integer);
		assertConversion("<4", new TestObject().integer().lessThan(FOUR), TestObject::integer);
		assertConversion("<=4", new TestObject().integer().lessThanOrEqualTo(FOUR),
				TestObject::integer);
		assertConversion(">4", new TestObject().integer().greaterThan(FOUR), TestObject::integer);
		assertConversion(">=4", new TestObject().integer().greaterThanOrEqualTo(FOUR),
				TestObject::integer);
		assertConversion("[4,5]", new TestObject().integer().between(FOUR, FIVE),
				TestObject::integer);
		assertConversion("#", new TestObject().integer().isNull(), TestObject::integer);
		assertConversion("!=4", new TestObject().integer().not().equalTo(FOUR),
				TestObject::integer);
		assertConversion("!<4", new TestObject().integer().not().lessThan(FOUR),
				TestObject::integer);
		assertConversion("!<=4", new TestObject().integer().not().lessThanOrEqualTo(FOUR),
				TestObject::integer);
		assertConversion("!>4", new TestObject().integer().not().greaterThan(FOUR),
				TestObject::integer);
		assertConversion("!>=4", new TestObject().integer().not().greaterThanOrEqualTo(FOUR),
				TestObject::integer);
		assertConversion("![4,5]", new TestObject().integer().not().between(FOUR, FIVE),
				TestObject::integer);
		assertConversion("!#", new TestObject().integer().not().isNull(), TestObject::integer);

		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						false),
				"=4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						false),
				"<4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false),
				"<=4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, false),
				">4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=4");
		assertObject(IntegerProperty.class, p -> assertBetween(FOUR, FIVE, p, false), "[4,5]");
		assertObject(IntegerProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						true),
				"!=4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						true),
				"!<4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>4");
		assertObject(IntegerProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=4");
		assertObject(IntegerProperty.class, p -> assertBetween(FOUR, FIVE, p, true), "![4,5]");
		assertObject(IntegerProperty.class, p -> assertNull(p, true), "!#");
	}

	/**
	 * Tests if query properties of type LocalDate are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicLocalDateConversion() throws IOException {
		final LocalDate today = LocalDate.of(2017, 10, 8);
		final LocalDate tomorrow = today.plusDays(1);

		assertConversion("=2017-10-08", new TestObject().localDate().equalTo(today),
				TestObject::localDate);
		assertConversion("<2017-10-08", new TestObject().localDate().lessThan(today),
				TestObject::localDate);
		assertConversion("<=2017-10-08", new TestObject().localDate().lessThanOrEqualTo(today),
				TestObject::localDate);
		assertConversion(">2017-10-08", new TestObject().localDate().greaterThan(today),
				TestObject::localDate);
		assertConversion(">=2017-10-08", new TestObject().localDate().greaterThanOrEqualTo(today),
				TestObject::localDate);
		assertConversion("[2017-10-08,2017-10-09]",
				new TestObject().localDate().between(today, tomorrow), TestObject::localDate);
		assertConversion("#", new TestObject().localDate().isNull(), TestObject::localDate);
		assertConversion("!=2017-10-08", new TestObject().localDate().not().equalTo(today),
				TestObject::localDate);
		assertConversion("!<2017-10-08", new TestObject().localDate().not().lessThan(today),
				TestObject::localDate);
		assertConversion("!<=2017-10-08",
				new TestObject().localDate().not().lessThanOrEqualTo(today),
				TestObject::localDate);
		assertConversion("!>2017-10-08", new TestObject().localDate().not().greaterThan(today),
				TestObject::localDate);
		assertConversion("!>=2017-10-08", new TestObject().localDate().not().greaterThanOrEqualTo
				(today), TestObject::localDate);
		assertConversion("![2017-10-08,2017-10-09]",
				new TestObject().localDate().not().between(today, tomorrow),
				TestObject::localDate);
		assertConversion("!#", new TestObject().localDate().not().isNull(), TestObject::localDate);

		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType.EQUALS,
						false),
				"=2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType
								.LESS_THAN,
						false),
				"<2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO,
						false),
				"<=2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN, false),
				">2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=2017-10-08");
		assertObject(LocalDateProperty.class, p -> assertBetween(today, tomorrow, p, false),
				"[2017-10-08,2017-10-09]");
		assertObject(LocalDateProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType.EQUALS,
						true),
				"!=2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType
								.LESS_THAN,
						true),
				"!<2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>2017-10-08");
		assertObject(LocalDateProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=2017-10-08");
		assertObject(LocalDateProperty.class, p -> assertBetween(today, tomorrow, p, true),
				"![2017-10-08,2017-10-09]");
		assertObject(LocalDateProperty.class, p -> assertNull(p, true), "!#");

		

	}

	/**
	 * Tests if query properties of type LocalDateTime are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicLocalDateTimeConversion() throws IOException {
		final LocalDateTime today = LocalDateTime.of(2017, 10, 8, 14, 45, 33);
		final LocalDateTime tomorrow = today.plusDays(1);

		assertConversion("=2017-10-08T14:45:33.000", new TestObject().localDateTime().equalTo
				(today), TestObject::localDateTime);
		assertConversion("<2017-10-08T14:45:33.000",
				new TestObject().localDateTime().lessThan(today), TestObject::localDateTime);
		assertConversion("<=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().lessThanOrEqualTo(today),
				TestObject::localDateTime);
		assertConversion(">2017-10-08T14:45:33.000", new TestObject().localDateTime().greaterThan
				(today), TestObject::localDateTime);
		assertConversion(">=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().greaterThanOrEqualTo(today),
				TestObject::localDateTime);
		assertConversion("[2017-10-08T14:45:33.000,2017-10-09T14:45:33.000]",
				new TestObject().localDateTime().between(today, tomorrow),
				TestObject::localDateTime);
		assertConversion("#", new TestObject().localDateTime().isNull(),
				TestObject::localDateTime);
		assertConversion("!=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().equalTo(today), TestObject::localDateTime);
		assertConversion("!<2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().lessThan(today), TestObject::localDateTime);
		assertConversion("!<=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().lessThanOrEqualTo(today),
				TestObject::localDateTime);
		assertConversion("!>2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().greaterThan(today),
				TestObject::localDateTime);
		assertConversion("!>=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().greaterThanOrEqualTo(today),
				TestObject::localDateTime);
		assertConversion("![2017-10-08T14:45:33.000,2017-10-09T14:45:33.000]",
				new TestObject().localDateTime().not().between(today, tomorrow),
				TestObject::localDateTime);
		assertConversion("!#", new TestObject().localDateTime().not().isNull(),
				TestObject::localDateTime);

		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType.EQUALS,
						false),
				"=2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType
								.LESS_THAN,
						false),
				"<2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO,
						false),
				"<=2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN, false),
				">2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class, p -> assertBetween(today, tomorrow, p, false),
				"[2017-10-08T14:45:33.000,2017-10-09T14:45:33.000]");
		assertObject(LocalDateTimeProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType.EQUALS,
						true),
				"!=2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType
								.LESS_THAN,
						true),
				"!<2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=2017-10-08T14:45:33.000");
		assertObject(LocalDateTimeProperty.class, p -> assertBetween(today, tomorrow, p, true),
				"![2017-10-08T14:45:33.000,2017-10-09T14:45:33.000]");
		assertObject(LocalDateTimeProperty.class, p -> assertNull(p, true), "!#");

			

	}

	/**
	 * Tests if query properties of type Long are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicLongConversion() throws IOException {
		final long FOUR = 4;
		final long FIVE = 5;

		assertConversion("=4", new TestObject().longVal().equalTo(FOUR), TestObject::longVal);
		assertConversion("<4", new TestObject().longVal().lessThan(FOUR), TestObject::longVal);
		assertConversion("<=4", new TestObject().longVal().lessThanOrEqualTo(FOUR),
				TestObject::longVal);
		assertConversion(">4", new TestObject().longVal().greaterThan(FOUR), TestObject::longVal);
		assertConversion(">=4", new TestObject().longVal().greaterThanOrEqualTo(FOUR),
				TestObject::longVal);
		assertConversion("[4,5]", new TestObject().longVal().between(FOUR, FIVE),
				TestObject::longVal);
		assertConversion("#", new TestObject().longVal().isNull(), TestObject::longVal);
		assertConversion("!=4", new TestObject().longVal().not().equalTo(FOUR),
				TestObject::longVal);
		assertConversion("!<4", new TestObject().longVal().not().lessThan(FOUR),
				TestObject::longVal);
		assertConversion("!<=4", new TestObject().longVal().not().lessThanOrEqualTo(FOUR),
				TestObject::longVal);
		assertConversion("!>4", new TestObject().longVal().not().greaterThan(FOUR),
				TestObject::longVal);
		assertConversion("!>=4", new TestObject().longVal().not().greaterThanOrEqualTo(FOUR),
				TestObject::longVal);
		assertConversion("![4,5]", new TestObject().longVal().not().between(FOUR, FIVE),
				TestObject::longVal);
		assertConversion("!#", new TestObject().longVal().not().isNull(), TestObject::longVal);

		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						false),
				"=4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						false),
				"<4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false),
				"<=4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, false),
				">4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=4");
		assertObject(LongProperty.class, p -> assertBetween(FOUR, FIVE, p, false), "[4,5]");
		assertObject(LongProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						true),
				"!=4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						true),
				"!<4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>4");
		assertObject(LongProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=4");
		assertObject(LongProperty.class, p -> assertBetween(FOUR, FIVE, p, true), "![4,5]");
		assertObject(LongProperty.class, p -> assertNull(p, true), "!#");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidBetweenExpression() {
		LongProperty.fromString("[4,5,6]");
	}

	@Test(expected = IllegalArgumentException.class)
	public void unParseableExpression() {
		LongProperty.fromString("[4,5a]");
	}

	@Test
	public void testNullUnsafeExpression() {
		LongProperty<QueryObject<?>> prop = LongProperty.fromString("[4,5]");

		assertEquals(Long.valueOf(4L), prop.getPrimary());
		assertEquals(Long.valueOf(5L), prop.getSecondary());
	}

	/**
	 * Tests if query properties of type Short are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicShortConversion() throws IOException {
		final short FOUR = 4;
		final short FIVE = 5;

		assertConversion("=4", new TestObject().shortVal().equalTo(FOUR), TestObject::shortVal);
		assertConversion("<4", new TestObject().shortVal().lessThan(FOUR), TestObject::shortVal);
		assertConversion("<=4", new TestObject().shortVal().lessThanOrEqualTo(FOUR),
				TestObject::shortVal);
		assertConversion(">4", new TestObject().shortVal().greaterThan(FOUR),
				TestObject::shortVal);
		assertConversion(">=4", new TestObject().shortVal().greaterThanOrEqualTo(FOUR),
				TestObject::shortVal);
		assertConversion("[4,5]", new TestObject().shortVal().between(FOUR, FIVE),
				TestObject::shortVal);
		assertConversion("#", new TestObject().shortVal().isNull(), TestObject::shortVal);
		assertConversion("!=4", new TestObject().shortVal().not().equalTo(FOUR),
				TestObject::shortVal);
		assertConversion("!<4", new TestObject().shortVal().not().lessThan(FOUR),
				TestObject::shortVal);
		assertConversion("!<=4", new TestObject().shortVal().not().lessThanOrEqualTo(FOUR),
				TestObject::shortVal);
		assertConversion("!>4", new TestObject().shortVal().not().greaterThan(FOUR),
				TestObject::shortVal);
		assertConversion("!>=4", new TestObject().shortVal().not().greaterThanOrEqualTo(FOUR),
				TestObject::shortVal);
		assertConversion("![4,5]", new TestObject().shortVal().not().between(FOUR, FIVE),
				TestObject::shortVal);
		assertConversion("!#", new TestObject().shortVal().not().isNull(), TestObject::shortVal);

		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						false),
				"=4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						false),
				"<4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, false),
				"<=4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, false),
				">4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=4");
		assertObject(ShortProperty.class, p -> assertBetween(FOUR, FIVE, p, false), "[4,5]");
		assertObject(ShortProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.EQUALS,
						true),
				"!=4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p, ComparableComparisonType.LESS_THAN,
						true),
				"!<4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>4");
		assertObject(ShortProperty.class,
				p -> assertComparableSingularOperation(FOUR, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=4");
		assertObject(ShortProperty.class, p -> assertBetween(FOUR, FIVE, p, true), "![4,5]");
		assertObject(ShortProperty.class, p -> assertNull(p, true), "!#");

		

	}

	/**
	 * Tests if query properties of type String are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicStringConversion() throws IOException {
		assertConversion("=test", new TestObject().name().equalTo("test"), TestObject::name);
		assertConversion("!=test", new TestObject().name().not().equalTo("test"),
				TestObject::name);
		assertConversion("~test", new TestObject().name().like("test"), TestObject::name);
		assertConversion("!~test", new TestObject().name().not().like("test"), TestObject::name);
		assertConversion("_=test", new TestObject().name().equalToIgnoreCase("test"),
				TestObject::name);
		assertConversion("!_=test", new TestObject().name().not().equalToIgnoreCase("test"),
				TestObject::name);
		assertConversion("_~test", new TestObject().name().ilike("test"), TestObject::name);
		assertConversion("!_~test", new TestObject().name().not().ilike("test"), TestObject::name);
		assertConversion("#", new TestObject().name().isNull(), TestObject::name);
		assertConversion("!#", new TestObject().name().not().isNull(), TestObject::name);

		

		StringProperty<QueryObject<?>> unqualifiedSelector = StringProperty.fromString("test");
		assertTrue(unqualifiedSelector.isSet());
		assertEquals("test", unqualifiedSelector.getValue());
		assertEquals(StringComparisonType.EQUALS, unqualifiedSelector.getType());
		assertFalse(unqualifiedSelector.isNegated());

	}


	/**
	 * Tests if query properties of type LocalDateTime are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testBasicZonedDateTimeConversion() throws IOException {
		final ZonedDateTime today =
				ZonedDateTime.of(2017, 10, 8, 12, 45, 33, 0, ZoneId.of("UTC"))
						.withFixedOffsetZone();
		final ZonedDateTime tomorrow = today.plusDays(1);

		assertConversion("=2017-10-08T12:45:33.000Z", new TestObject().zonedDateTime().equalTo
				(today), TestObject::zonedDateTime);
		assertConversion("<2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().lessThan(today), TestObject::zonedDateTime);
		assertConversion("<=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().lessThanOrEqualTo(today),
				TestObject::zonedDateTime);
		assertConversion(">2017-10-08T12:45:33.000Z", new TestObject().zonedDateTime().greaterThan
				(today), TestObject::zonedDateTime);
		assertConversion(">=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().greaterThanOrEqualTo(today),
				TestObject::zonedDateTime);
		assertConversion("[2017-10-08T12:45:33.000Z,2017-10-09T12:45:33.000Z]",
				new TestObject().zonedDateTime().between(today, tomorrow),
				TestObject::zonedDateTime);
		assertConversion("#", new TestObject().zonedDateTime().isNull(),
				TestObject::zonedDateTime);
		assertConversion("!=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().equalTo(today), TestObject::zonedDateTime);
		assertConversion("!<2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().lessThan(today), TestObject::zonedDateTime);
		assertConversion("!<=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().lessThanOrEqualTo(today),
				TestObject::zonedDateTime);
		assertConversion("!>2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().greaterThan(today),
				TestObject::zonedDateTime);
		assertConversion("!>=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().greaterThanOrEqualTo(today),
				TestObject::zonedDateTime);
		assertConversion("![2017-10-08T12:45:33.000Z,2017-10-09T12:45:33.000Z]",
				new TestObject().zonedDateTime().not().between(today, tomorrow),
				TestObject::zonedDateTime);
		assertConversion("!#", new TestObject().zonedDateTime().not().isNull(),
				TestObject::zonedDateTime);

		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType.EQUALS,
						false),
				"=2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType
								.LESS_THAN,
						false),
				"<2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO,
						false),
				"<=2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN, false),
				">2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						false),
				">=2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class, p -> assertBetween(today, tomorrow, p, false),
				"[2017-10-08T12:45:33.000Z,2017-10-09T12:45:33.000Z]");
		assertObject(DateTimeProperty.class, p -> assertNull(p, false), "#");
		// Negations
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType.EQUALS,
						true),
				"!=2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p, ComparableComparisonType
								.LESS_THAN,
						true),
				"!<2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.LESS_THAN_OR_EQUAL_TO, true),
				"!<=2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN, true),
				"!>2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class,
				p -> assertComparableSingularOperation(today, p,
						ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO,
						true),
				"!>=2017-10-08T12:45:33.000Z");
		assertObject(DateTimeProperty.class, p -> assertBetween(today, tomorrow, p, true),
				"![2017-10-08T12:45:33.000Z,2017-10-09T12:45:33.000Z]");
		assertObject(DateTimeProperty.class, p -> assertNull(p, true), "!#");

		

	}


	/**
	 * Converts the given query object to a String representation, and asserts that this
	 * representation equals
	 * the given String
	 *
	 * @param expectedData The expected resulting data
	 * @param query        The query to convert
	 * @throws IOException If the conversion fails
	 */
	protected <P extends IQueryProperty> void assertConversion(
			@Nonnull
					String expectedData,
			@Nonnull
					TestObject
					query,
			@Nonnull
					Function<TestObject, P> getProperty) throws IOException {
		assertEquals(expectedData, getProperty.apply(query).toString());
	}

	/**
	 * Enforces the given constraint on the given JSON converted to an object
	 *
	 * @param propertyClass        The type of property to test
	 * @param assertion            The constraint to assert
	 * @param stringRepresentation The JSON to convert to an object
	 * @param <Q>                  The type of property to run the assertion on
	 * @throws IOException If the conversion failed for any reason
	 */
	@SuppressWarnings("unchecked")
	protected <Q extends IQueryProperty> void assertObject(
			@Nonnull
					Class<Q> propertyClass,
			@Nonnull
					AssertionSet<Q> assertion,
			@Nonnull
					String stringRepresentation) throws IOException {

	}

	/**
	 * Converts the TestObject to JSON, and then back again, ensuring the resulting object equals
	 * the given object
	 *
	 * @param query The query to test
	 * @throws IOException If the conversion failed for any reason
	 */
	private void assertTwoWayConversion(
			@Nonnull
					TestObject query) throws IOException {
		Map<String, String> properties = new HashMap<>();
		Set<Object> props = new HashSet<>();

		for (Field f : TestObject.class.getDeclaredFields()) {
			if (IQueryProperty.class.isAssignableFrom(f.getType())) {
				boolean accessible = f.isAccessible();

				try {
					if (!accessible) {
						f.setAccessible(true);
					}
					Object prop = f.get(query);
					props.add(prop);
					properties.put(f.getName(), prop.toString());

				} catch (IllegalAccessException e) {
					throw new IOException(e);
				} finally {
					f.setAccessible(accessible);
				}
			}
		}

		assertEquals(properties.size(), props.size());

		TestObject result = new TestObject();

		for (Field f : TestObject.class.getDeclaredFields()) {
			if (IQueryProperty.class.isAssignableFrom(f.getType())) {
				assertTrue(properties.containsKey(f.getName()));
				boolean accessible = f.isAccessible();

				try {
					if (!accessible) {
						f.setAccessible(true);
					}

					Method fromString = f.getType().getMethod("fromString", String.class);

					f.set(result, fromString.invoke(null, properties.get(f.getName())));

				} catch (IllegalAccessException | NoSuchMethodException |
						InvocationTargetException e) {
					throw new IOException(e);
				} finally {
					f.setAccessible(accessible);
				}
			}
		}

		assertEquals(query, result);

	}

	/**
	 * Consumer, accepts a query property to allow the user to run assertions on them
	 *
	 * @param <Q> The type of property passed to the consumer
	 */
	protected interface AssertionSet<Q extends IQueryProperty> {
		/**
		 * Apply logic to given object
		 *
		 * @param object The object to run assertions on
		 */
		void apply(Q object);
	}

	private <P extends ComparableProperty<?, C>, C extends Comparable<? super C>> void
	assertComparableSingularOperation(
			C expected, P
			property, ComparableComparisonType comparisonType, boolean negated) {
		assertTrue(property.isSet());
		assertEquals(negated, property.isNegated());
		assertTrue(property.getPrimaryValue().isPresent());
		assertFalse(property.getSecondaryValue().isPresent());
		assertEquals(expected, property.getPrimaryValue().get());
		assertEquals(comparisonType, property.getComparisonType());
	}

	private <P extends ComparableProperty<?, C>, C extends Comparable<? super C>> void assertNull(
			P property,
			boolean negated) {
		assertTrue(property.isSet());
		assertEquals(negated, property.isNegated());
		assertFalse(property.getPrimaryValue().isPresent());
		assertFalse(property.getSecondaryValue().isPresent());
		assertEquals(ComparableComparisonType.NULL, property.getComparisonType());
	}

	private <P extends ComparableProperty<?, C>, C extends Comparable<? super C>> void
	assertBetween(
			C expectedLower, C expectedUpper, P
			property, boolean negated) {
		assertTrue(property.isSet());
		assertEquals(negated, property.isNegated());
		assertTrue(property.getPrimaryValue().isPresent());
		assertTrue(property.getSecondaryValue().isPresent());
		assertEquals(expectedLower, property.getPrimaryValue().get());
		assertEquals(expectedUpper, property.getSecondaryValue().get());
		assertEquals(ComparableComparisonType.BETWEEN, property.getComparisonType());
	}

	private void testEquivalence(Function<TestObject, Object> targetFunction) {
		Equivalence.equivalent(targetFunction.apply(new TestObject()), targetFunction.apply(new
				TestObject()), targetFunction.apply(new TestObject()));
	}

}
