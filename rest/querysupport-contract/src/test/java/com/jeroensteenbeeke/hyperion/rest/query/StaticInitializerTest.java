package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class StaticInitializerTest {
	@Test
	public void testPropertiesHaveStaticInitializers() {
		Reflections ref = new Reflections("com.jeroensteenbeeke.hyperion.rest.query", new SubTypesScanner(false));

		@SuppressWarnings("unchecked")
		Set<Class<? extends IQueryProperty>> properties = ref.getAllTypes().stream().map(t -> {
			try {
				return Class.forName(t);
			} catch (ClassNotFoundException e) {
				return null;
			}
		}).filter(Objects::nonNull).filter(IQueryProperty.class::isAssignableFrom).map(c -> (Class<? extends IQueryProperty>) c).collect(Collectors.toSet());
		assertFalse("No implementations of IQueryProperty found", properties.isEmpty());

		for (Class<? extends IQueryProperty> prop : properties) {
			if (Modifier.isAbstract(prop.getModifiers())) {
				continue;
			}

			Method method;
			try {
				method = prop.getMethod("fromString", String.class);
			} catch (NoSuchMethodException e) {
				method = null;
			}

			assertNotNull("Query property class " + prop.getSimpleName() + " does not declare a static method fromString(String)", method);
			assertTrue("Query property class " + prop.getSimpleName() + " declares a method fromString(String), but it is not static", Modifier.isStatic(method.getModifiers()));
			assertTrue("Query property class " + prop.getSimpleName() + " declares a method fromString(String), but it is not public", Modifier.isPublic(method.getModifiers()));
			assertTrue("Query property class " + prop.getSimpleName() + " declares a static method fromString(String), but it does not return " + prop.getSimpleName() + ", but " + method.getReturnType().getName(), prop.equals(method.getReturnType()));
		}

	}
}
