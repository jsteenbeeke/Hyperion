package com.jeroensteenbeeke.hyperion.rest.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.solitary.InMemory.Handler;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTask;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.ServiceProvider;
import com.jeroensteenbeeke.hyperion.util.Datasets;
import okhttp3.*;
import okhttp3.Request.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Base class for executing REST calls against the current project
 */
public abstract class AbstractBackendTest {

	protected static final Logger log = LoggerFactory.getLogger(AbstractBackendTest.class);

	private Handler handler;

	private TransactionStatus transaction;

	private ObjectMapper mapper;

	/**
	 * Constructor
	 */
	protected AbstractBackendTest() {
		this.mapper = new ObjectMapper();

		this.mapper.registerModule(new JavaTimeModule());
		this.mapper.registerModule(new Jdk8Module());
	}

	/**
	 * Starts the application for testing
	 * @throws Exception If startup fails
	 */
	@Before
	public void startApplication() throws Exception {
		Datasets.readFromObject(this).ifPresent(Datasets.INSTANCE::set);
		handler = createApplicationHandler();
	}


	/**
	 * Closes the application
	 * @throws Exception If closing the application fails
	 */
	@After
	public void closeApplication() throws Exception {
		handler.terminate();
		Datasets.INSTANCE.unset();
	}

	/**
	 * Prints out the class under test
	 */
	@Before
	public void dumpClass() {
		log.warn("+++ CLASS {} +++", getClass().getName());
	}

	/**
	 * Creates the callback handler (i.e. starts the application)
	 * @return The handler to use to stop the application
	 * @throws Exception If anything goes wrong
	 */
	protected abstract Handler createApplicationHandler() throws Exception;

	/**
	 * Determines the port to which to connect during tests. Defaults to 8081, but
	 * can be overriden by the user
	 * @return The port to which to connect
	 */
	protected int getApplicationPort() {
		return 8081;
	}

	/**
	 * Determines the header key used to identify the application
	 * @return The name of the HTTP header (for example: {@code X-Application-Token})
	 */
	protected abstract String getApplicationHeaderKey();

	/**
	 * The default value to use for the application header
	 * @return The default value for the application header
	 */
	protected abstract String getDefaultApplicationHeader();

	/**
	 * Returns a serviceProvider implementation to use to get access to Spring beans
	 * @return A ServiceProvider instance
	 */
	protected abstract ServiceProvider getServiceProvider();

	/**
	 * Define a prefix for calls under test
	 * @return The prefix for any REST paths. Defaults to empty String
	 */
	protected String getPrefix() {
		return "";
	}

	/**
	 * Do a GET request to the given path
	 * @param path The path in question
	 * @return A CallFinalizer to finish configuring the request
	 */
	protected CallFinalizer get(String path) {
		return new CallFinalizer(new Request.Builder().get(), getPrefix(), path);
	}

	/**
	 * Do a DELETE request to the given path
	 * @param path The path in question
	 * @return A CallFinalizer to finish configuring the request
	 */
	protected CallFinalizer delete(String path) {
		return new CallFinalizer(new Request.Builder().delete(), getPrefix(), path);
	}

	/**
	 * Do a POST request to the given path
	 * @param path The path in question
	 * @param payload The object to send as body
	 * @param <T> The type of payload
	 * @return A CallFinalizer to finish configuring the request
	 * @throws IOException If the payload cannot be converted to JSON
	 */
	protected <T> CallFinalizer post(String path, T payload)
			throws IOException {
		return post(path, "application/json", mapper.writer().forType(payload.getClass())
													.writeValueAsString(payload));
	}

	/**
	 * Do a POST request to the given path
	 * @param path The path in question
	 * @param contentType The content type of the payload
	 * @param payload The String to send as body
	 * @return A CallFinalizer to finish configuring the request
	 * @throws IOException If the payload cannot be converted to JSON
	 */
	protected CallFinalizer post(String path, String contentType, String payload)
		throws IOException {
		RequestBody body = RequestBody.create(
			MediaType.parse(contentType),
			payload);

		return new CallFinalizer(new Request.Builder().post(body), getPrefix(), path).withContentType(contentType);
	}

	/**
	 * Do a PUT request to the given path
	 * @param path The path in question
	 * @param payload The object to send as body
	 * @param <T> The type of payload
	 * @return A CallFinalizer to finish configuring the request
	 * @throws IOException If the payload cannot be converted to JSON
	 */
	protected <T> CallFinalizer put(String path, T payload) throws IOException {
		return put(path, "application/json", mapper.writer().forType(payload.getClass())
												   .writeValueAsString(payload));
	}

	/**
	 * Do a PUT request to the given path
	 * @param path The path in question
	 * @param contentType The content type of the payload
	 * @param payload The literal String to send as body
	 * @return A CallFinalizer to finish configuring the request
	 */
	protected CallFinalizer put(String path, String contentType, String payload) {
		RequestBody body = RequestBody.create(
				MediaType.parse(contentType),
				payload);

		return new CallFinalizer(new Request.Builder().put(body), getPrefix(), path).withContentType(contentType);
	}

	private <T> T parseJson(Class<T> targetClass, Response response)
			throws IOException {
		assertTrue(response.isSuccessful());

		return mapper
				   .readerFor(targetClass)
				   .readValue(response.body().bytes());

	}

	private <T> List<T> parseJsonList(Class<T> targetClass, Response response)
			throws IOException {
		assertTrue(response.isSuccessful());

		CollectionType collectionType =
				mapper.getTypeFactory().constructCollectionType(List.class, targetClass);

		return mapper
				.readerFor(collectionType)
				.readValue(response.body().bytes());

	}

	/**
	 * Builder-type object for configuring an HTTP call to the service
	 */
	public final class CallFinalizer {
		private final Builder builder;

		private String prefix;

		private final String path;

		private String url = "http://localhost:"+ getApplicationPort();

		private String includeHeader = getDefaultApplicationHeader();

		private String userToken = null;

		private String customAuthorization = null;

		private String contentType = "application/json";

		private String accept = "application/json";

		/**
		 * Constructor
		 * @param builder The builder to finalize
		 * @param prefix The prefix for the URL
		 * @param path The path of the URL
		 */
		public CallFinalizer(Builder builder, String prefix, String path) {
			this.builder = builder;
			this.path = path;
			this.prefix = prefix;
		}


		/**
		 * Override the default application header with the given value
		 * @param header The new header value
		 * @return The current finalizer
		 */
		public CallFinalizer withAlternativeApplicationHeader(@Nonnull String header) {
			this.includeHeader = header;
			return this;
		}

		/**
		 * Omit the default application header
		 * @return The current finalizer
		 */
		public CallFinalizer withoutApplicationHeader() {
			this.includeHeader = null;
			return this;
		}

		/**
		 * Set the HTTP Accept header
		 * @param accept The value of the accept header
		 * @return The current finalizer
		 */
		public CallFinalizer accepting(String accept) {
			this.accept = accept;
			return this;
		}

		/**
		 * Set the HTTP Content-Type header
		 * @param contentType The value of the content-type header
		 * @return The current finalizer
		 */
		public CallFinalizer withContentType(String contentType) {
			this.contentType = contentType;
			return this;
		}

		/**
		 * Override the default service URL and resets the prefix
		 * @param url The alternative base URL to use
		 * @return The current finalizer
		 */
		public CallFinalizer atUrl(@Nonnull String url) {
			this.url = url;
			this.prefix = "";
			return this;
		}

		/**
		 * Set the token to authenticate with
		 * @param token the token to use
		 * @return The current finalizer
		 */
		public CallFinalizer identifiedBy(@Nonnull String token) {
			this.userToken = token;
			return this;
		}

		/**
		 * Override the authorization header with the given value
		 * @param authorization The value of the authorization header
		 * @return The current finalizer
		 */
		public CallFinalizer withCustomAuthorization(@Nonnull String authorization) {
			this.customAuthorization = authorization;
			return this;
		}

		/**
		 * Execute the call, indicating that an HTTP Status 200 is expected as result
		 * @return A ResultHandler object to inspect the response
		 * @throws IOException If the communication fails
		 */
		public ResultHandler expectingOK() throws IOException {
			return expecting(HttpStatus.OK.value());
		}

		/**
		 * Execute the call, indicating that an HTTP Status 201 is expected as result
		 * @return A ResultHandler object to inspect the response
		 * @throws IOException If the communication fails
		 */
		public ResultHandler expectingCreated() throws IOException {
			return expecting(HttpStatus.CREATED.value());
		}

		/**
		 * Execute the call, indicating that an HTTP Status 204 is expected as result
		 * @return A ResultHandler object to inspect the response
		 * @throws IOException If the communication fails
		 */
		public ResultHandler expectingNoContent() throws IOException {
			return expecting(HttpStatus.NO_CONTENT.value());
		}


		/**
		 * Execute the call, indicating that the given HTTP status code is expected as result
		 * @param expectedCode The expected status code
		 * @return A ResultHandler object to inspect the response
		 * @throws IOException If the communication fails
		 */
		public ResultHandler expecting(int expectedCode) throws IOException {
			builder.url(url + prefix + path);

			builder.header("Accept", accept);
			builder.header("Content-Type", contentType);

			if (includeHeader != null) {
				builder.header(getApplicationHeaderKey(),
						includeHeader);

				if (userToken != null) {
					builder.header("Authorization",
							String.format("Bearer %s", userToken));
				} else if (customAuthorization != null) {
					builder.header("Authorization", customAuthorization);
				}
			}

			Request request = builder.build();

			OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
					.connectTimeout(120, TimeUnit.SECONDS);

			// If we're explicitly looking for a redirect then instruct client not map follow
			// redirect
			if (expectedCode >= 300 && expectedCode < 400) {
				clientBuilder.followRedirects(false);
			}

			HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(log::debug);
			interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

			clientBuilder.addInterceptor(interceptor);
			clientBuilder.addNetworkInterceptor(interceptor);
			clientBuilder.connectTimeout(10, TimeUnit.SECONDS);
			clientBuilder.readTimeout(5, TimeUnit.MINUTES);
			clientBuilder.writeTimeout(5, TimeUnit.MINUTES);

			OkHttpClient client = clientBuilder.build();

			Call call = client.newCall(request);
			Response response = call.execute();

			try {
				assertEquals(
						String.format(
								"Expected %s call to %s to result in %d, but received %d: %s",
								request.method(),
								path,
								expectedCode, response.code(), response.message()), expectedCode,
						response.code());
			} catch (AssertionError e) {
				System.err.println("=== HTTP MESSAGE BODY ===");
				System.err.println(response.body().string());
				System.err.println("=== END HTTP MESSAGE BODY ===");
				throw e;
			}

			return new ResultHandler(response);
		}

	}

	/**
	 * Wrapper for the Response object, allowing sanitized access
	 */
	public class ResultHandler {
		private final Response response;

		private ResultHandler(Response response) {
			super();
			this.response = response;
		}

		/**
		 * Return the response body as an instance of the given type
		 * @param type The class object representing the type
		 * @param <T> The type
		 * @return An instance of the given type, parsed from the response body
		 * @throws IOException If parsing fails, for whatever reason
		 */
		@SuppressWarnings("unchecked")
		@Nonnull
		public <T> T getAs(@Nonnull Class<T> type) throws IOException {
			if (String.class.isAssignableFrom(type)) {
				return (T) response.body().string();
			}
			return parseJson(type, response);
		}

		/**
		 * Return the response body as an list of the given type
		 * @param type The class object representing the type
		 * @param <T> The type
		 * @return A list of instances of the given type, parsed from the response body
		 * @throws IOException If parsing fails, for whatever reason
		 */
		@Nonnull
		public <T> List<T> getAsListOf(@Nonnull Class<T> type) throws IOException {
			return parseJsonList(type, response);
		}

		/**
		 * Returns the header with the given key, if it exists
		 * @param key The key to look for
		 * @return Optionally the header in question
		 */
		@Nonnull
		public Optional<String> getHeader(@Nonnull String key) {
			return Optional.ofNullable(response.header(key));
		}
	}

	/**
	 * Run an instance of the given task
	 * @param jobClass The class of the task to execute
	 * @param <T> The type of task
	 */
	protected <T extends HyperionTask> void runJob(Class<T> jobClass) {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestAttributes attr = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attr);

		PlatformTransactionManager txManager =
				getServiceProvider().getService(PlatformTransactionManager.class);
		transaction = txManager.getTransaction(new DefaultTransactionDefinition());

		Exception ex = null;

		try {
			T job = jobClass.newInstance();

			job.run(getServiceProvider());
		} catch (Exception e) {
			e.printStackTrace();
			ex = e;
		} finally {
			txManager.commit(transaction);

			RequestContextHolder.resetRequestAttributes();
		}

		assertNull("Unexpected job exception: " + Optional.ofNullable(ex).map
				(Exception::getMessage).orElse(""), ex);
	}

	/**
	 * Create an instance of the given class, and perform a piece of logic with said service
	 * @param serviceClass The class of service to instantiate
	 * @param <D> The type of service
	 * @return A ServiceActionBuilder to specify the logic to execute
	 */
	protected <D> ServiceActionBuilder<D> withService(Class<D> serviceClass) {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestAttributes attr = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attr);

		PlatformTransactionManager txManager =
				getServiceProvider().getService(PlatformTransactionManager.class);

		transaction = txManager.getTransaction(new DefaultTransactionDefinition());

		return new ServiceActionBuilder<D>(getServiceProvider().getService(serviceClass));
	}

	/**
	 * Finalizer class for an action to run on the given service
	 * @param <SERVICE> The type of service
	 */
	protected class ServiceActionBuilder<SERVICE> {
		private final SERVICE service;

		private ServiceActionBuilder(SERVICE service) {
			this.service = service;
		}

		/**
		 * Apply the given consumer to the service contained in this builder
		 * @param serviceConsumer The consumer to give the service to
		 */
		public void perform(Consumer<SERVICE> serviceConsumer) {
			serviceConsumer.accept(service);

			PlatformTransactionManager txManager =
					getServiceProvider().getService(PlatformTransactionManager.class);
			txManager.commit(transaction);

			RequestContextHolder.resetRequestAttributes();
		}
	}

	/**
	 * Create an instance of the given DAO, and perform a piece of logic with said DAO
	 * @param daoClass The class of DAO to instantiate
	 * @param <D> The type of DAO
	 * @return A DAOActionBuilder to specify the logic to execute
	 */
	protected <D extends DAO<?,?>> DAOActionBuilder<D> withDAO(Class<D> daoClass) {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestAttributes attr = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attr);

		PlatformTransactionManager txManager =
				getServiceProvider().getService(PlatformTransactionManager.class);
		transaction = txManager.getTransaction(new DefaultTransactionDefinition());

		return new DAOActionBuilder<>(getServiceProvider().getService(daoClass));
	}

	/**
	 * Finalizer class to specify action to run on the created DAO
	 * @param <D> The type of DAO
	 */
	protected class DAOActionBuilder<D extends DAO<?,?>> {
		private final D dao;

		private DAOActionBuilder(D dao) {
			this.dao = dao;
		}

		/**
		 * Perform an action using the given DAO
		 * @param daoConsumer The consumer that accepts the DAO
		 */
		public void perform(Consumer<D> daoConsumer) {
			daoConsumer.accept(dao);

			PlatformTransactionManager txManager =
					getServiceProvider().getService(PlatformTransactionManager.class);
			txManager.commit(transaction);

			RequestContextHolder.resetRequestAttributes();
		}
	}

	/**
	 * Create an instance of the given DAOs, and perform a piece of logic with said DAOs
	 * 	 *
	 * @param firstDaoClass The first class of DAO to instantiate
	 * @param secondDaoClass The second class of DAO to instantiate
	 * @param <D> The first type of DAO
	 * @param <DD> The second type of DAO
	 * @return A MultiDAOActionBuilder to specify the logic to execute
	 */
	protected <D extends DAO<?,?>, DD extends DAO<?,?>> MultiDAOActionBuilder<D, DD> withDAOS(
			Class<D> firstDaoClass,
			Class<DD> secondDaoClass) {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestAttributes attr = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attr);

		PlatformTransactionManager txManager =
				getServiceProvider().getService(PlatformTransactionManager.class);
		transaction = txManager.getTransaction(new DefaultTransactionDefinition());

		return new MultiDAOActionBuilder<D, DD>(getServiceProvider().getService(firstDaoClass),
				getServiceProvider().getService(secondDaoClass));
	}

	/**
	 * Finalizer class to specify action to run on the created DAOs
	 * @param <D> The first DAO type
	 * @param <DD> The second DAO type
	 */
	protected class MultiDAOActionBuilder<D extends DAO<?,?>, DD extends DAO<?,?>> {
		private final D firstDao;

		private final DD secondDao;

		private MultiDAOActionBuilder(D firstDao, DD secondDao) {
			this.firstDao = firstDao;
			this.secondDao = secondDao;
		}

		/**
		 * Perform an action using the given DAOs
		 * @param daoConsumer A BiConsumer that accepts both DAOs and performs actions on them
		 */
		public void perform(BiConsumer<D, DD> daoConsumer) {
			daoConsumer.accept(firstDao, secondDao);

			PlatformTransactionManager txManager =
					getServiceProvider().getService(PlatformTransactionManager.class);
			txManager.commit(transaction);

			RequestContextHolder.resetRequestAttributes();
		}
	}
}
