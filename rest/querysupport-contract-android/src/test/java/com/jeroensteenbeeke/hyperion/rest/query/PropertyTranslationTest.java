package com.jeroensteenbeeke.hyperion.rest.query;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import com.jeroensteenbeeke.hyperion.test.Equivalence;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNull;

/**
 * Basic test for serializing queries to/from JSON
 */
public class PropertyTranslationTest {
	/**
	 * Tests if query properties of type BigDecimal are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicBigDecimalConversion() throws IOException {
		final BigDecimal FOUR = new BigDecimal(4);
		final BigDecimal FIVE = new BigDecimal(5);

		assertConversion("", new TestObject(), TestObject::bigdecimal);
		assertConversion("=4", new TestObject().bigdecimal().equalTo(FOUR), TestObject::bigdecimal);
		assertConversion("=4.0", new TestObject().bigdecimal().equalTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("<4", new TestObject().bigdecimal().lessThan(FOUR), TestObject::bigdecimal);
		assertConversion("<4.0", new TestObject().bigdecimal().lessThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("<=4", new TestObject().bigdecimal().lessThanOrEqualTo(FOUR), TestObject::bigdecimal);
		assertConversion("<=4.0", new TestObject().bigdecimal().lessThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion(">4", new TestObject().bigdecimal().greaterThan(FOUR), TestObject::bigdecimal);
		assertConversion(">4.0", new TestObject().bigdecimal().greaterThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion(">=4", new TestObject().bigdecimal().greaterThanOrEqualTo(FOUR), TestObject::bigdecimal);
		assertConversion(">=4.0", new TestObject().bigdecimal().greaterThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("[4,5]", new TestObject().bigdecimal().between(FOUR, FIVE), TestObject::bigdecimal);
		assertConversion("[4.0,5]", new TestObject().bigdecimal().between(FOUR.setScale(1), FIVE),
				TestObject::bigdecimal);
		assertConversion("#", new TestObject().bigdecimal().isNull(), TestObject::bigdecimal);
		assertConversion("!=4", new TestObject().bigdecimal().not().equalTo(FOUR), TestObject::bigdecimal);
		assertConversion("!=4.0", new TestObject().bigdecimal().not().equalTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!<4", new TestObject().bigdecimal().not().lessThan(FOUR), TestObject::bigdecimal);
		assertConversion("!<4.0", new TestObject().bigdecimal().not().lessThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!<=4", new TestObject().bigdecimal().not().lessThanOrEqualTo(FOUR), TestObject::bigdecimal);
		assertConversion("!<=4.0", new TestObject().bigdecimal().not().lessThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!>4", new TestObject().bigdecimal().not().greaterThan(FOUR), TestObject::bigdecimal);
		assertConversion("!>4.0", new TestObject().bigdecimal().not().greaterThan(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("!>=4", new TestObject().bigdecimal().not().greaterThanOrEqualTo(FOUR),
				TestObject::bigdecimal);
		assertConversion("!>=4.0", new TestObject().bigdecimal().not().greaterThanOrEqualTo(FOUR
				.setScale(1)), TestObject::bigdecimal);
		assertConversion("![4,5]", new TestObject().bigdecimal().not().between(FOUR, FIVE), TestObject::bigdecimal);
		assertConversion("![4.0,5]",
				new TestObject().bigdecimal().not().between(FOUR.setScale(1), FIVE), TestObject::bigdecimal);
		assertConversion("!#", new TestObject().bigdecimal().not().isNull(), TestObject::bigdecimal);

		// Negations

		assertConversion("![4.0,5]",
				new TestObject().bigdecimal().not().between(FOUR.setScale(1), FIVE), TestObject::bigdecimal);

	}


	/**
	 * Tests if query properties of type BigInteger are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicBigIntegerConversion() throws IOException {
		final BigInteger FOUR = BigInteger.valueOf(4L);
		final BigInteger FIVE = BigInteger.valueOf(5L);

		assertConversion("", new TestObject(), TestObject::biginteger);
		assertConversion("=4", new TestObject().biginteger().equalTo(FOUR), TestObject::biginteger);
		assertConversion("<4", new TestObject().biginteger().lessThan(FOUR), TestObject::biginteger);
		assertConversion("<=4", new TestObject().biginteger().lessThanOrEqualTo(FOUR), TestObject::biginteger);
		assertConversion(">4", new TestObject().biginteger().greaterThan(FOUR), TestObject::biginteger);
		assertConversion(">=4", new TestObject().biginteger().greaterThanOrEqualTo(FOUR), TestObject::biginteger);
		assertConversion("[4,5]", new TestObject().biginteger().between(FOUR, FIVE), TestObject::biginteger);
		assertConversion("#", new TestObject().biginteger().isNull(), TestObject::biginteger);
		assertConversion("!=4", new TestObject().biginteger().not().equalTo(FOUR), TestObject::biginteger);
		assertConversion("!<4", new TestObject().biginteger().not().lessThan(FOUR), TestObject::biginteger);
		assertConversion("!<=4", new TestObject().biginteger().not().lessThanOrEqualTo(FOUR), TestObject::biginteger);
		assertConversion("!>4", new TestObject().biginteger().not().greaterThan(FOUR), TestObject::biginteger);
		assertConversion("!>=4", new TestObject().biginteger().not().greaterThanOrEqualTo(FOUR),
				TestObject::biginteger);
		assertConversion("![4,5]", new TestObject().biginteger().not().between(FOUR, FIVE), TestObject::biginteger);
		assertConversion("!#", new TestObject().biginteger().not().isNull(), TestObject::biginteger);

		// Negations

	}

	/**
	 * Tests if query properties of type Boolean are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicBooleanConversion() throws IOException {
		assertConversion("", new TestObject(), TestObject::booleanVal);
		assertConversion("false", new TestObject().booleanVal().equalTo(Boolean.FALSE), TestObject::booleanVal);
		assertConversion("true", new TestObject().booleanVal().equalTo(Boolean.TRUE), TestObject::booleanVal);
		assertConversion("!false", new TestObject().booleanVal().not().equalTo(Boolean.FALSE), TestObject::booleanVal);
		assertConversion("!true", new TestObject().booleanVal().not().equalTo(Boolean.TRUE), TestObject::booleanVal);
		assertConversion("#", new TestObject().booleanVal().isNull(), TestObject::booleanVal);
		assertConversion("!#", new TestObject().booleanVal().not().isNull(), TestObject::booleanVal);

		assertEquivalent(() -> new TestObject().booleanVal().equalTo(Boolean.FALSE));
		assertEquivalent(() -> new TestObject().booleanVal().equalTo(Boolean.TRUE));
		assertEquivalent(() -> new TestObject().booleanVal().not().equalTo(Boolean.FALSE));
		assertEquivalent(() -> new TestObject().booleanVal().not().equalTo(Boolean.TRUE));
		assertEquivalent(() -> new TestObject().booleanVal().isNull());
		assertEquivalent(() -> new TestObject().booleanVal().not().isNull());

		Assert.assertNull(new TestObject().booleanVal().getUnwrappedValue());
	}

	private void assertEquivalent(Supplier<TestObject> creator) {
		assertEquals(creator.get(), creator.get());
	}

	/**
	 * Tests if query properties of type Double are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicDoubleConversion() throws IOException {
		final double FOUR = 4.0;
		final double FIVE = 5.0;

		assertConversion("", new TestObject(), TestObject::doubleVal);
		assertConversion("=4.0", new TestObject().doubleVal().equalTo(FOUR), TestObject::doubleVal);
		assertConversion("<4.0", new TestObject().doubleVal().lessThan(FOUR), TestObject::doubleVal);
		assertConversion("<=4.0", new TestObject().doubleVal().lessThanOrEqualTo(FOUR), TestObject::doubleVal);
		assertConversion(">4.0", new TestObject().doubleVal().greaterThan(FOUR), TestObject::doubleVal);
		assertConversion(">=4.0", new TestObject().doubleVal().greaterThanOrEqualTo(FOUR), TestObject::doubleVal);
		assertConversion("[4.0,5.0]", new TestObject().doubleVal().between(FOUR, FIVE), TestObject::doubleVal);
		assertConversion("#", new TestObject().doubleVal().isNull(), TestObject::doubleVal);
		assertConversion("!=4.0", new TestObject().doubleVal().not().equalTo(FOUR), TestObject::doubleVal);
		assertConversion("!<4.0", new TestObject().doubleVal().not().lessThan(FOUR), TestObject::doubleVal);
		assertConversion("!<=4.0", new TestObject().doubleVal().not().lessThanOrEqualTo(FOUR), TestObject::doubleVal);
		assertConversion("!>4.0", new TestObject().doubleVal().not().greaterThan(FOUR), TestObject::doubleVal);
		assertConversion("!>=4.0", new TestObject().doubleVal().not().greaterThanOrEqualTo(FOUR),
				TestObject::doubleVal);
		assertConversion("![4.0,5.0]", new TestObject().doubleVal().not().between(FOUR, FIVE), TestObject::doubleVal);
		assertConversion("!#", new TestObject().doubleVal().not().isNull(), TestObject::doubleVal);

		// Negations

	}

	/**
	 * Tests if query properties of type Float are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicFloatConversion() throws IOException {
		final float FOUR = 4.0f;
		final float FIVE = 5.0f;

		assertConversion("", new TestObject(), TestObject::floatVal);
		assertConversion("=4.0", new TestObject().floatVal().equalTo(FOUR), TestObject::floatVal);
		assertConversion("<4.0", new TestObject().floatVal().lessThan(FOUR), TestObject::floatVal);
		assertConversion("<=4.0", new TestObject().floatVal().lessThanOrEqualTo(FOUR), TestObject::floatVal);
		assertConversion(">4.0", new TestObject().floatVal().greaterThan(FOUR), TestObject::floatVal);
		assertConversion(">=4.0", new TestObject().floatVal().greaterThanOrEqualTo(FOUR), TestObject::floatVal);
		assertConversion("[4.0,5.0]", new TestObject().floatVal().between(FOUR, FIVE), TestObject::floatVal);
		assertConversion("#", new TestObject().floatVal().isNull(), TestObject::floatVal);
		assertConversion("!=4.0", new TestObject().floatVal().not().equalTo(FOUR), TestObject::floatVal);
		assertConversion("!<4.0", new TestObject().floatVal().not().lessThan(FOUR), TestObject::floatVal);
		assertConversion("!<=4.0", new TestObject().floatVal().not().lessThanOrEqualTo(FOUR), TestObject::floatVal);
		assertConversion("!>4.0", new TestObject().floatVal().not().greaterThan(FOUR), TestObject::floatVal);
		assertConversion("!>=4.0", new TestObject().floatVal().not().greaterThanOrEqualTo(FOUR), TestObject::floatVal);
		assertConversion("![4.0,5.0]", new TestObject().floatVal().not().between(FOUR, FIVE), TestObject::floatVal);
		assertConversion("!#", new TestObject().floatVal().not().isNull(), TestObject::floatVal);

		// Negations

	}

	/**
	 * Tests if query properties of type Integer are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicIntegerConversion() throws IOException {
		final int FOUR = 4;
		final int FIVE = 5;

		assertConversion("", new TestObject(), TestObject::integer);
		assertConversion("=4", new TestObject().integer().equalTo(FOUR), TestObject::integer);
		assertConversion("<4", new TestObject().integer().lessThan(FOUR), TestObject::integer);
		assertConversion("<=4", new TestObject().integer().lessThanOrEqualTo(FOUR), TestObject::integer);
		assertConversion(">4", new TestObject().integer().greaterThan(FOUR), TestObject::integer);
		assertConversion(">=4", new TestObject().integer().greaterThanOrEqualTo(FOUR), TestObject::integer);
		assertConversion("[4,5]", new TestObject().integer().between(FOUR, FIVE), TestObject::integer);
		assertConversion("#", new TestObject().integer().isNull(), TestObject::integer);
		assertConversion("!=4", new TestObject().integer().not().equalTo(FOUR), TestObject::integer);
		assertConversion("!<4", new TestObject().integer().not().lessThan(FOUR), TestObject::integer);
		assertConversion("!<=4", new TestObject().integer().not().lessThanOrEqualTo(FOUR), TestObject::integer);
		assertConversion("!>4", new TestObject().integer().not().greaterThan(FOUR), TestObject::integer);
		assertConversion("!>=4", new TestObject().integer().not().greaterThanOrEqualTo(FOUR), TestObject::integer);
		assertConversion("![4,5]", new TestObject().integer().not().between(FOUR, FIVE), TestObject::integer);
		assertConversion("!#", new TestObject().integer().not().isNull(), TestObject::integer);
	}

	/**
	 * Tests if query properties of type LocalDate are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicLocalDateConversion() throws IOException {
		final LocalDate today = new LocalDate(2017, 10, 8);
		final LocalDate tomorrow = today.plusDays(1);

		assertConversion("", new TestObject(), TestObject::localDate);
		assertConversion("=2017-10-08", new TestObject().localDate().equalTo(today), TestObject::localDate);
		assertConversion("<2017-10-08", new TestObject().localDate().lessThan(today), TestObject::localDate);
		assertConversion("<=2017-10-08", new TestObject().localDate().lessThanOrEqualTo(today), TestObject::localDate);
		assertConversion(">2017-10-08", new TestObject().localDate().greaterThan(today), TestObject::localDate);
		assertConversion(">=2017-10-08", new TestObject().localDate().greaterThanOrEqualTo(today),
				TestObject::localDate);
		assertConversion("[2017-10-08,2017-10-09]",
				new TestObject().localDate().between(today, tomorrow), TestObject::localDate);
		assertConversion("#", new TestObject().localDate().isNull(), TestObject::localDate);
		assertConversion("!=2017-10-08", new TestObject().localDate().not().equalTo(today), TestObject::localDate);
		assertConversion("!<2017-10-08", new TestObject().localDate().not().lessThan(today), TestObject::localDate);
		assertConversion("!<=2017-10-08", new TestObject().localDate().not().lessThanOrEqualTo(today),
				TestObject::localDate);
		assertConversion("!>2017-10-08", new TestObject().localDate().not().greaterThan(today), TestObject::localDate);
		assertConversion("!>=2017-10-08", new TestObject().localDate().not().greaterThanOrEqualTo
				(today), TestObject::localDate);
		assertConversion("![2017-10-08,2017-10-09]",
				new TestObject().localDate().not().between(today, tomorrow), TestObject::localDate);
		assertConversion("!#", new TestObject().localDate().not().isNull(), TestObject::localDate);
	}

	/**
	 * Tests if query properties of type LocalDateTime are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicLocalDateTimeConversion() throws IOException {
		final LocalDateTime today = new LocalDateTime(2017, 10, 8, 14, 45, 33);
		final LocalDateTime tomorrow = today.plusDays(1);

		assertConversion("", new TestObject(), TestObject::localDateTime);
		assertConversion("=2017-10-08T14:45:33.000", new TestObject().localDateTime().equalTo
				(today), TestObject::localDateTime);
		assertConversion("<2017-10-08T14:45:33.000",
				new TestObject().localDateTime().lessThan(today), TestObject::localDateTime);
		assertConversion("<=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().lessThanOrEqualTo(today), TestObject::localDateTime);
		assertConversion(">2017-10-08T14:45:33.000", new TestObject().localDateTime().greaterThan
				(today), TestObject::localDateTime);
		assertConversion(">=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().greaterThanOrEqualTo(today), TestObject::localDateTime);
		assertConversion("[2017-10-08T14:45:33.000,2017-10-09T14:45:33.000]",
				new TestObject().localDateTime().between(today, tomorrow), TestObject::localDateTime);
		assertConversion("#", new TestObject().localDateTime().isNull(), TestObject::localDateTime);
		assertConversion("!=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().equalTo(today), TestObject::localDateTime);
		assertConversion("!<2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().lessThan(today), TestObject::localDateTime);
		assertConversion("!<=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().lessThanOrEqualTo(today), TestObject::localDateTime);
		assertConversion("!>2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().greaterThan(today), TestObject::localDateTime);
		assertConversion("!>=2017-10-08T14:45:33.000",
				new TestObject().localDateTime().not().greaterThanOrEqualTo(today), TestObject::localDateTime);
		assertConversion("![2017-10-08T14:45:33.000,2017-10-09T14:45:33.000]",
				new TestObject().localDateTime().not().between(today, tomorrow), TestObject::localDateTime);
		assertConversion("!#", new TestObject().localDateTime().not().isNull(), TestObject::localDateTime);
	}

	/**
	 * Tests if query properties of type Long are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicLongConversion() throws IOException {
		final long FOUR = 4;
		final long FIVE = 5;

		assertConversion("", new TestObject(), TestObject::longVal);
		assertConversion("=4", new TestObject().longVal().equalTo(FOUR), TestObject::longVal);
		assertConversion("<4", new TestObject().longVal().lessThan(FOUR), TestObject::longVal);
		assertConversion("<=4", new TestObject().longVal().lessThanOrEqualTo(FOUR), TestObject::longVal);
		assertConversion(">4", new TestObject().longVal().greaterThan(FOUR), TestObject::longVal);
		assertConversion(">=4", new TestObject().longVal().greaterThanOrEqualTo(FOUR), TestObject::longVal);
		assertConversion("[4,5]", new TestObject().longVal().between(FOUR, FIVE), TestObject::longVal);
		assertConversion("#", new TestObject().longVal().isNull(), TestObject::longVal);
		assertConversion("!=4", new TestObject().longVal().not().equalTo(FOUR), TestObject::longVal);
		assertConversion("!<4", new TestObject().longVal().not().lessThan(FOUR), TestObject::longVal);
		assertConversion("!<=4", new TestObject().longVal().not().lessThanOrEqualTo(FOUR), TestObject::longVal);
		assertConversion("!>4", new TestObject().longVal().not().greaterThan(FOUR), TestObject::longVal);
		assertConversion("!>=4", new TestObject().longVal().not().greaterThanOrEqualTo(FOUR), TestObject::longVal);
		assertConversion("![4,5]", new TestObject().longVal().not().between(FOUR, FIVE), TestObject::longVal);
		assertConversion("!#", new TestObject().longVal().not().isNull(), TestObject::longVal);
	}

	/**
	 * Tests if query properties of type Short are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicShortConversion() throws IOException {
		final short FOUR = 4;
		final short FIVE = 5;

		assertConversion("", new TestObject(), TestObject::shortVal);
		assertConversion("=4", new TestObject().shortVal().equalTo(FOUR), TestObject::shortVal);
		assertConversion("<4", new TestObject().shortVal().lessThan(FOUR), TestObject::shortVal);
		assertConversion("<=4", new TestObject().shortVal().lessThanOrEqualTo(FOUR), TestObject::shortVal);
		assertConversion(">4", new TestObject().shortVal().greaterThan(FOUR), TestObject::shortVal);
		assertConversion(">=4", new TestObject().shortVal().greaterThanOrEqualTo(FOUR), TestObject::shortVal);
		assertConversion("[4,5]", new TestObject().shortVal().between(FOUR, FIVE), TestObject::shortVal);
		assertConversion("#", new TestObject().shortVal().isNull(), TestObject::shortVal);
		assertConversion("!=4", new TestObject().shortVal().not().equalTo(FOUR), TestObject::shortVal);
		assertConversion("!<4", new TestObject().shortVal().not().lessThan(FOUR), TestObject::shortVal);
		assertConversion("!<=4", new TestObject().shortVal().not().lessThanOrEqualTo(FOUR), TestObject::shortVal);
		assertConversion("!>4", new TestObject().shortVal().not().greaterThan(FOUR), TestObject::shortVal);
		assertConversion("!>=4", new TestObject().shortVal().not().greaterThanOrEqualTo(FOUR), TestObject::shortVal);
		assertConversion("![4,5]", new TestObject().shortVal().not().between(FOUR, FIVE), TestObject::shortVal);
		assertConversion("!#", new TestObject().shortVal().not().isNull(), TestObject::shortVal);
	}

	/**
	 * Tests if query properties of type String are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	public void testBasicStringConversion() throws IOException {
		assertConversion("", new TestObject(), TestObject::name);
		assertConversion("=test", new TestObject().name().equalTo("test"), TestObject::name);
		assertConversion("!=test", new TestObject().name().not().equalTo("test"), TestObject::name);
		assertConversion("~test", new TestObject().name().like("test"), TestObject::name);
		assertConversion("!~test", new TestObject().name().not().like("test"), TestObject::name);
		assertConversion("_=test", new TestObject().name().equalToIgnoreCase("test"), TestObject::name);
		assertConversion("!_=test", new TestObject().name().not().equalToIgnoreCase("test"), TestObject::name);
		assertConversion("_~test", new TestObject().name().ilike("test"), TestObject::name);
		assertConversion("!_~test", new TestObject().name().not().ilike("test"), TestObject::name);
		assertConversion("#", new TestObject().name().isNull(), TestObject::name);
		assertConversion("!#", new TestObject().name().not().isNull(), TestObject::name);
	}

	/**
	 * Tests if query properties of type LocalDateTime are correctly converted to JSON
	 *
	 * @throws IOException On test failure
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testBasicZonedDateTimeConversion() throws IOException {
		final DateTime today = new DateTime(2017, 10, 8, 12, 45, 33, DateTimeZone.UTC);
		final DateTime tomorrow = today.plusDays(1);

		assertConversion("", new TestObject(), TestObject::zonedDateTime);
		assertConversion("=2017-10-08T12:45:33.000Z", new TestObject().zonedDateTime().equalTo
				(today), TestObject::zonedDateTime);
		assertConversion("<2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().lessThan(today), TestObject::zonedDateTime);
		assertConversion("<=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().lessThanOrEqualTo(today), TestObject::zonedDateTime);
		assertConversion(">2017-10-08T12:45:33.000Z", new TestObject().zonedDateTime().greaterThan
				(today), TestObject::zonedDateTime);
		assertConversion(">=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().greaterThanOrEqualTo(today), TestObject::zonedDateTime);
		assertConversion("[2017-10-08T12:45:33.000Z,2017-10-09T12:45:33.000Z]",
				new TestObject().zonedDateTime().between(today, tomorrow), TestObject::zonedDateTime);
		assertConversion("#", new TestObject().zonedDateTime().isNull(), TestObject::zonedDateTime);
		assertConversion("!=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().equalTo(today), TestObject::zonedDateTime);
		assertConversion("!<2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().lessThan(today), TestObject::zonedDateTime);
		assertConversion("!<=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().lessThanOrEqualTo(today), TestObject::zonedDateTime);
		assertConversion("!>2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().greaterThan(today), TestObject::zonedDateTime);
		assertConversion("!>=2017-10-08T12:45:33.000Z",
				new TestObject().zonedDateTime().not().greaterThanOrEqualTo(today), TestObject::zonedDateTime);
		assertConversion("![2017-10-08T12:45:33.000Z,2017-10-09T12:45:33.000Z]",
				new TestObject().zonedDateTime().not().between(today, tomorrow), TestObject::zonedDateTime);
		assertConversion("!#", new TestObject().zonedDateTime().not().isNull(), TestObject::zonedDateTime);
	}

	@Test
	public void testNullUnsafeExpression() {
		LongProperty<TestObject> prop = new TestObject().longVal().between(4L,5L).longVal();

		assertEquals(Long.valueOf(4L), prop.getPrimary());
		assertEquals(Long.valueOf(5L), prop.getSecondary());
	}

	/**
	 * Converts the given query object to a String representation, and asserts that this representation equals
	 * the given String
	 *
	 * @param expectedData The expected resulting data
	 * @param query        The query to convert
	 * @throws IOException If the conversion fails
	 */
	protected <P extends IQueryProperty> void assertConversion(@Nonnull String expectedData, @Nonnull TestObject
			query, @Nonnull Function<TestObject, P> getProperty) throws IOException {
		assertEquals(expectedData, getProperty.apply(query).toString());

		Set<Object> props = new HashSet<>();
		int pc = 0;

		for (Field f : TestObject.class.getDeclaredFields()) {
			if (IQueryProperty.class.isAssignableFrom(f.getType())) {
				boolean accessible = f.isAccessible();

				try {
					if (!accessible) {
						f.setAccessible(true);
					}
					Object prop = f.get(query);
					props.add(prop);
					pc++;
				} catch (IllegalAccessException e) {
					throw new IOException(e);
				} finally {
					f.setAccessible(accessible);
				}
			}
		}

		assertEquals(pc, props.size());
	}


}
