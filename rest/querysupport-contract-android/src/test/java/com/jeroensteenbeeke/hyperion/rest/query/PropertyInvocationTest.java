package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.ComparableComparisonType;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import com.jeroensteenbeeke.hyperion.rest.querysupport.StringComparisonType;
import org.junit.Test;

import static org.junit.Assert.*;

public class PropertyInvocationTest {
	private TestObject obj = new TestObject();

	@Test
	public void invokeBooleanProperty() {
		assertSame(obj, obj.booleanVal().equalTo(true));
		assertSame(obj, obj.booleanVal().not().equalTo(true));
		assertSame(obj, obj.booleanVal().isNull());

		assertTrue(obj.booleanVal().isSet());
		assertTrue(obj.booleanVal().isExplicitNull());
		assertFalse(obj.booleanVal().isNegated());
		assertFalse(obj.booleanVal().getValue().isPresent());
	}

	@Test
	public void invokeComparableProperty() {
		assertFalse(obj.integer().isSet());
		assertFalse(obj.integer().isNegated());

		assertSame(obj, obj.integer().between(0, 5));
		assertSame(obj, obj.integer().equalTo(3));
		assertSame(obj, obj.integer().greaterThan(3));
		assertSame(obj, obj.integer().greaterThanOrEqualTo(3));
		assertSame(obj, obj.integer().lessThan(3));
		assertSame(obj, obj.integer().lessThanOrEqualTo(3));
		assertSame(obj, obj.integer().isNull());

		assertTrue(obj.integer().isSet());
		assertEquals(ComparableComparisonType.NULL, obj.integer().getComparisonType());
		assertFalse(obj.integer().isNegated());
		assertFalse(obj.integer().getPrimaryValue().isPresent());
		assertFalse(obj.integer().getSecondaryValue().isPresent());

		assertSame(obj, obj.integer().not().between(0, 5));
		assertSame(obj, obj.integer().not().equalTo(3));
		assertSame(obj, obj.integer().not().greaterThan(3));
		assertSame(obj, obj.integer().not().greaterThanOrEqualTo(3));
		assertSame(obj, obj.integer().not().lessThan(3));
		assertSame(obj, obj.integer().not().lessThanOrEqualTo(3));
		assertSame(obj, obj.integer().not().isNull());

		assertTrue(obj.integer().isSet());
		assertEquals(ComparableComparisonType.NULL, obj.integer().getComparisonType());
		assertTrue(obj.integer().isNegated());
		assertFalse(obj.integer().getPrimaryValue().isPresent());
		assertFalse(obj.integer().getSecondaryValue().isPresent());
	}

	@Test
	public void invokeStringProperty() {
		assertSame(obj, obj.name().equalTo("Test"));
		assertSame(obj, obj.name().equalToIgnoreCase("Test"));
		assertSame(obj, obj.name().like("Test%"));
		assertSame(obj, obj.name().ilike("Test%"));
		assertSame(obj, obj.name().isNull());
		assertSame(obj, obj.name().not().equalTo("Test"));
		assertSame(obj, obj.name().not().equalToIgnoreCase("Test"));
		assertSame(obj, obj.name().not().like("Test%"));
		assertSame(obj, obj.name().not().ilike("Test%"));
		assertSame(obj, obj.name().not().isNull());

		assertTrue(obj.name().isSet());
		assertEquals(StringComparisonType.NULL, obj.name().getType());
		assertTrue(obj.name().isNegated());
		assertEquals("", obj.name().getValue());
	}



}
