package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class StaticInitializerTest {
	@Test
	public void testPropertiesDoNotHaveStaticInitializers() {
		Reflections ref = new Reflections("com.jeroensteenbeeke.hyperion.rest.query", new SubTypesScanner(false));

		@SuppressWarnings("unchecked")
		Set<Class<? extends IQueryProperty>> properties = ref.getAllTypes().stream().map(t -> {
			try {
				return Class.forName(t);
			} catch (ClassNotFoundException e) {
				return null;
			}
		}).filter(Objects::nonNull).filter(IQueryProperty.class::isAssignableFrom).map(c -> (Class<? extends IQueryProperty>) c).collect(Collectors.toSet());
		assertFalse("No implementations of IQueryProperty found", properties.isEmpty());

		for (Class<? extends IQueryProperty> prop : properties) {
			Method method;
			try {
				method = prop.getMethod("valueOf", String.class);
			} catch (NoSuchMethodException e) {
				method = null;
			}

			assertNull("Query property class " + prop.getSimpleName() + " does declares a static method valueOf(String)", method);
		}

	}
}
