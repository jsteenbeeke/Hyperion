package com.jeroensteenbeeke.hyperion.rest.query;

import com.google.common.base.Optional;
import com.jeroensteenbeeke.hyperion.rest.querysupport.*;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.Objects;

/**
 * Query property that resembles a boolean. Boolean properties are serialized to JSON as follows:
 * <p>
 * {@code
 * <p>
 * {
 * 'value': 'true',
 * 'another_value': 'false',
 * 'not_value': '!true',
 * 'not_another_value': '!false',
 * 'explicit_null': '#',
 * "explicit_not_null': '!#'
 * <p>
 * }
 * <p>
 * }
 *
 * @param <T> The type of object this Boolean property is tied to
 */
public class BooleanProperty<T extends QueryObject<?>> implements IQueryProperty<T>, IBooleanProperty<T> {
	private final T target;
	private final String fieldName;

	private Boolean value = null;

	private boolean negated = false;

	private boolean shouldNegateNext = false;

	private boolean explicitNull = false;

	private OrderBy orderBy = OrderBy.IGNORE;

	/**
	 * Creates a new BooleanProperty
	 *
	 * @param target    The containing object
	 * @param fieldName The name of the field this property is assigned to
	 */
	public BooleanProperty(@Nonnull T target, @Nonnull String fieldName) {
		this.target = target;
		this.fieldName = fieldName;
	}

	@Nonnull
	@Override
	public OrderBy getPropertyOrder() {
		return orderBy;
	}

	@Override
	public void setPropertyOrder(@Nonnull OrderBy orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * Specifies that this property should be (not) equal to the given value
	 *
	 * @param value The value to compare against
	 * @return The query object
	 */
	@Nonnull
	public T equalTo(@Nonnull boolean value) {
		this.value = value;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property's query should be negated. Usually this method is called
	 * prior to calling one of the other methods to negate this, for example: {@code property.not().equalTo("value")}
	 *
	 * @return This property object
	 */
	@Nonnull
	public BooleanProperty<T> not() {
		this.shouldNegateNext = true;
		return this;
	}

	/**
	 * Specifies that the current property should not have a value
	 *
	 * @return The query object
	 */
	@Nonnull
	public T isNull() {
		this.explicitNull = true;
		this.value = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Returns the value that was set, if one exists
	 *
	 * @return An Optional that may contain a boolean value
	 */
	public Optional<Boolean> getValue() {
		return Optional.fromNullable(value);
	}

	@Override
	public Boolean getUnwrappedValue() {
		return value;
	}


	/**
	 * Return whether or not the current operation should be negated
	 *
	 * @return {@code true} if the operation should be negated, {@code false} otherwise
	 */
	public boolean isNegated() {
		return negated;
	}

	/**
	 * Return whether or not the property should be checked for an explicit null value
	 *
	 * @return {@code true} if the property should be null (or not-null if {@link #isNegated()} returns true), or
	 * {@code false} if a normal equality check should be performed
	 */
	public boolean isExplicitNull() {
		return explicitNull;
	}

	@Override
	public boolean isSet() {
		return value != null || explicitNull;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BooleanProperty)) return false;

		BooleanProperty<?> that = (BooleanProperty<?>) o;

		if (negated != that.negated) return false;
		if (shouldNegateNext != that.shouldNegateNext) return false;
		if (explicitNull != that.explicitNull) return false;
		return value != null ? value.equals(that.value) : that.value == null;
	}

	@Override
	public int hashCode() {
		int result = (negated ? 1 : 0);
		result = 31 * result + (value != null ? value.hashCode() : 0);
		result = 31 * result + (shouldNegateNext ? 1 : 0);
		result = 31 * result + (explicitNull ? 1 : 0);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder r = new StringBuilder();

		OrderByLogic.applyOrderBy(orderBy, r);

		if (!isSet()) {
			return r.toString();
		}

		if (isNegated()) {
			r.append('!');
		}

		if (isExplicitNull()) {
			r.append("#");
		} else {
			if (value != null) {
				r.append(value.toString());
			}
		}

		return r.toString();
	}

	@Override
	public T orderBy(boolean ascending) {
		if (shouldNegateNext) {
			throw new IllegalStateException("OrderBy cannot be negated");
		}

		orderBy = new OrderBy(target.getNextSortIndex(), ascending);

		return target;
	}
}
