package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

/**
 * Query object property representing a BigDecimal value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class BigDecimalProperty<T extends QueryObject<?>> extends ComparableProperty<T, BigDecimal> {
	/**
	 * Create a new BigDecimal property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public BigDecimalProperty(@Nonnull T target, @Nonnull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@Nonnull BigDecimal bigDecimal) {
		return bigDecimal.toString();
	}
}
