package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import javax.annotation.Nonnull;

/**
 * Query object property representing a Float value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class FloatProperty<T extends QueryObject<?>> extends ComparableProperty<T, Float> {
	/**
	 * Create a new Float property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public FloatProperty(@Nonnull T target, @Nonnull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@Nonnull Float aFloat) {
		return aFloat.toString();
	}
}
