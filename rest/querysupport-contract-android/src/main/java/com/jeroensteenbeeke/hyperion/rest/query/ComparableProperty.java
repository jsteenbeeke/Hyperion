package com.jeroensteenbeeke.hyperion.rest.query;

import com.google.common.base.Optional;
import com.jeroensteenbeeke.hyperion.rest.querysupport.*;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

/**
 * Query object property base class that can be used for any class that extends Comparable. These
 * properties are represented in JSON as follows (using numbers as example):
 * <p>
 * {@code
 * <p>
 * {
 * 'equal_to': '=4',
 * 'not_equal_to': '!=4',
 * 'greater_than': '>4',
 * 'not_greater_than': '!>4',
 * 'greater_than_or_equal_to': '>=4',
 * 'not_greater_than_or_equal_to': '!>=4',
 * 'less_than': '<4',
 * 'not_less_than': '!<4',
 * 'less_than_or_equal_to': '<=4',
 * 'not_less_than_or_equal_to': '!<=4',
 * 'between': '[4,6]',
 * 'not_between': '![4,6]',
 * 'null': '#',
 * 'not_null': '!#'
 * }
 * <p>
 * }
 * <p>
 * So supposing you have two integer properties, {@code foo} and {@code bar}, the following would be a valid query
 * object:
 * <p>
 * {@code
 * <p>
 * {
 * 'foo': '<4',
 * 'bar': '![0,25]'
 * }
 * <p>
 * }
 * <p>
 * In other words: return all objects that have a {@code foo} of less than 4, and a {@code bar} that has any value
 * not in
 * the range of 0 to 25.
 *
 * @param <T> The type of Query object that contains this property
 * @param <C> The type of comparable represented by this property
 */
public abstract class ComparableProperty<T extends QueryObject<?>, C extends Comparable<? super C>> implements
		IComparableProperty<C, T> {
	private final T target;

	private final String fieldName;

	private ComparableComparisonType comparisonType = ComparableComparisonType.NONE;

	private C primaryValue = null;

	private C secondaryValue = null;

	private boolean negated = false;

	private boolean shouldNegateNext = false;

	private OrderBy orderBy = OrderBy.IGNORE;

	/**
	 * Creates a new ComparableProperty for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	protected ComparableProperty(@Nonnull T target, @Nonnull String fieldName) {
		this.target = target;
		this.fieldName = fieldName;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Nonnull
	@Override
	public OrderBy getPropertyOrder() {
		return orderBy;
	}

	@Override
	public void setPropertyOrder(@Nonnull OrderBy orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * Negates the query for this property. Generally speaking you want to call this
	 * method before one of the other ones for readability, i.e. {@code property.not().equalTo(5); }
	 *
	 * @return The current property
	 */
	@Nonnull
	public ComparableProperty<T, C> not() {
		this.shouldNegateNext = true;
		return this;
	}

	/**
	 * Sets the query to a between command, requiring the values to be greater than or equal to {@code lower}, and
	 * less than or equal to {@code upper}
	 *
	 * @param lower The lower bound of the range
	 * @param upper The upper bound of the range
	 * @return The query object
	 */
	@Nonnull
	public T between(@Nonnull C lower, @Nonnull C upper) {
		comparisonType = ComparableComparisonType.BETWEEN;
		primaryValue = lower;
		secondaryValue = upper;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Sets the query to an equal-to comparison, requiring the value to be equal to the given value
	 *
	 * @param value The desired value of this property
	 * @return The query object
	 */
	@Nonnull
	public T equalTo(@Nonnull C value) {
		comparisonType = ComparableComparisonType.EQUALS;
		primaryValue = value;
		secondaryValue = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Sets the query to an greater-than-or-equal-to comparison, requiring the value to be greater than or equal to
	 * the given value
	 *
	 * @param value The desired lower bound of this property
	 * @return The query object
	 */
	@Nonnull
	public T greaterThanOrEqualTo(@Nonnull C value) {
		comparisonType = ComparableComparisonType.GREATER_THAN_OR_EQUAL_TO;
		primaryValue = value;
		secondaryValue = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Sets the query to an greater-than comparison, requiring the value to be greater than to the given value
	 *
	 * @param value The desired non-inclusive lower bound of this property
	 * @return The query object
	 */
	@Nonnull
	public T greaterThan(@Nonnull C value) {
		comparisonType = ComparableComparisonType.GREATER_THAN;
		primaryValue = value;
		secondaryValue = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Sets the query to an less-than-or-equal-to comparison, requiring the value to be less than or equal to the
	 * given value
	 *
	 * @param value The desired upper bound of this property
	 * @return The query object
	 */
	@Nonnull
	public T lessThanOrEqualTo(@Nonnull C value) {
		comparisonType = ComparableComparisonType.LESS_THAN_OR_EQUAL_TO;
		primaryValue = value;
		secondaryValue = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Sets the query to an less-than comparison, requiring the value to be less than the given value
	 *
	 * @param value The desired non-inclusive upper bound of this property
	 * @return The query object
	 */
	@Nonnull
	public T lessThan(@Nonnull C value) {
		comparisonType = ComparableComparisonType.LESS_THAN;
		primaryValue = value;
		secondaryValue = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property should be unset
	 *
	 * @return The query object
	 */
	@Nonnull
	public T isNull() {
		this.comparisonType = ComparableComparisonType.NULL;
		primaryValue = null;
		secondaryValue = null;
		this.negated = shouldNegateNext;
		this.shouldNegateNext = false;
		return target;
	}

	/**
	 * Returns the type of comparison to perform on this property
	 *
	 * @return The type of comparison, {@see ComparableComparisonType}
	 */
	@Nonnull
	public ComparableComparisonType getComparisonType() {
		return comparisonType;
	}

	/**
	 * Gets the primary value of the current comparison. Should always be filled if an operation was set, but may
	 * be empty if the current property has no operation
	 *
	 * @return An Optional that may contain the primary value
	 */
	@Nonnull
	public Optional<C> getPrimaryValue() {
		return Optional.fromNullable(primaryValue);
	}

	/**
	 * Gets the secondary value of the current comparison. Generally speaking this value is only used
	 * with BETWEEN queries
	 *
	 * @return An Optional that may contain a secondary value
	 */
	@Nonnull
	public Optional<C> getSecondaryValue() {
		return Optional.fromNullable(secondaryValue);
	}

	@CheckForNull
	@Override
	public C getPrimary() {
		return primaryValue;
	}

	@CheckForNull
	@Override
	public C getSecondary() {
		return secondaryValue;
	}

	/**
	 * Returns whether or not the current property's condition should be negated
	 *
	 * @return {@code true} if the operation should be negated, {@code false} otherwise
	 */
	public boolean isNegated() {
		return negated;
	}

	@Override
	public boolean isSet() {
		return getComparisonType() != ComparableComparisonType.NONE;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ComparableProperty)) return false;

		ComparableProperty<?, ?> that = (ComparableProperty<?, ?>) o;

		if (negated != that.negated) return false;
		if (shouldNegateNext != that.shouldNegateNext) return false;
		if (comparisonType != that.comparisonType) return false;
		if (primaryValue != null ? !primaryValue.equals(that.primaryValue) : that.primaryValue != null) return false;
		return secondaryValue != null ? secondaryValue.equals(that.secondaryValue) : that.secondaryValue == null;
	}

	@Override
	public int hashCode() {
		int result = comparisonType.hashCode();
		result = 31 * result + getClass().hashCode();
		result = 31 * result + (primaryValue != null ? primaryValue.hashCode() : 0);
		result = 31 * result + (secondaryValue != null ? secondaryValue.hashCode() : 0);
		result = 31 * result + (negated ? 1 : 0);
		result = 31 * result + (shouldNegateNext ? 1 : 0);
		return result;
	}

	<TT> void ifPresent(Optional<TT> value, Consumer<TT> operation) {
		if (value.isPresent()) {
			operation.accept(value.get());
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		OrderByLogic.applyOrderBy(orderBy, sb);

		if (!isSet()) {
			return sb.toString();
		}

		if (isNegated()) {
			sb.append("!");
		}

		switch (getComparisonType()) {
			case NULL:
				sb.append("#");
				return sb.toString();
			case NONE:
				return "";
			case BETWEEN:
				sb.append("[");
				ifPresent(getPrimaryValue().transform(this::getStringRepresentation), sb::append);
				sb.append(",");
				ifPresent(getSecondaryValue().transform(this::getStringRepresentation), sb::append);
				sb.append("]");
				return sb.toString();
			case EQUALS:
				sb.append("=");
				break;
			case LESS_THAN_OR_EQUAL_TO:
				sb.append("<=");
				break;
			case LESS_THAN:
				sb.append("<");
				break;
			case GREATER_THAN:
				sb.append(">");
				break;
			case GREATER_THAN_OR_EQUAL_TO:
				sb.append(">=");
				break;

		}
		ifPresent(getPrimaryValue().transform(this::getStringRepresentation), sb::append);

		return sb.toString();
	}

	/**
	 * Converts the given value to a String representation
	 *
	 * @param c The value to convert to String
	 * @return A string representing the given value
	 */
	protected abstract String getStringRepresentation(@Nonnull C c);

	private interface Consumer<T> {
		void accept(T value);
	}

	@Override
	public T orderBy(boolean ascending) {
		if (shouldNegateNext) {
			throw new IllegalStateException("OrderBy cannot be negated");
		}

		orderBy = new OrderBy(target.getNextSortIndex(), ascending);

		return target;
	}
}
