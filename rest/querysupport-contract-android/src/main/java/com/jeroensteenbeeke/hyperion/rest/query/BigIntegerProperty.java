package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import javax.annotation.Nonnull;
import java.math.BigInteger;

/**
 * Query object property representing a BigInteger value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class BigIntegerProperty<T extends QueryObject<?>> extends ComparableProperty<T, BigInteger> {
	/**
	 * Create a new BigInteger property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public BigIntegerProperty(@Nonnull T target, @Nonnull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@Nonnull BigInteger bigInteger) {
		return bigInteger.toString();
	}
}
