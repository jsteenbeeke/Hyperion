package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.*;

import javax.annotation.Nonnull;

/**
 * Query property that resembles a string comparison. Values of this String are serialized as follows:
 * <p>
 * {@code
 * <p>
 * {
 * 'equal': '=value',
 * 'not_equal': '!=value',
 * 'equal_ignorecase': '_=value',
 * 'not_equal_ignorecase': '!_=value',
 * 'like': '~value',
 * 'not_like': '!~value',
 * 'like_ignorecase': '_~value',
 * 'not_like_ignorecase': '!_~value',
 * 'null': '#',
 * 'not_null': '!#'
 * }
 * <p>
 * }
 *
 * @param <T> The type of object this String property is tied to
 */
public class StringProperty<T extends QueryObject<?>> implements IStringProperty {
	private final T target;

	private final String fieldName;

	private StringComparisonType type = StringComparisonType.NONE;

	private String value = "";

	private boolean negated = false;

	private boolean shouldNegateNext = false;

	private OrderBy orderBy = OrderBy.IGNORE;

	/**
	 * Creates a new StringProperty
	 *
	 * @param target    The containing object.
	 * @param fieldName The name of the field this property is assigned to
	 */
	public StringProperty(@Nonnull T target, @Nonnull String fieldName) {
		this.target = target;
		this.fieldName = fieldName;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Nonnull
	@Override
	public OrderBy getPropertyOrder() {
		return orderBy;
	}

	@Override
	public void setPropertyOrder(@Nonnull OrderBy orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * Specifies that this property should be (not) equal to the given value
	 *
	 * @param value The value to compare against
	 * @return The query object
	 */
	@Nonnull
	public T equalTo(@Nonnull String value) {
		this.type = StringComparisonType.EQUALS;
		this.value = value;
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that this property should be (not) equal to the given value, using
	 * a case-insensitive comparison
	 *
	 * @param value The value to compare against
	 * @return The query object
	 */
	@Nonnull
	public T equalToIgnoreCase(@Nonnull String value) {
		this.type = StringComparisonType.EQUALS_IGNORECASE;
		this.value = value;
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property should be a case sensitive LIKE comparison.
	 *
	 * @param value The value to compare against (where {@code %} counts as a wildcard)
	 * @return The query object
	 */
	@Nonnull
	public T like(@Nonnull String value) {
		this.type = StringComparisonType.LIKE;
		this.value = value;
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property should be a case insensitive LIKE comparison.
	 *
	 * @param value The value to compare against (where {@code %} counts as a wildcard)
	 * @return The query object
	 */
	@Nonnull
	public T ilike(@Nonnull String value) {
		this.type = StringComparisonType.ILIKE;
		this.value = value;
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		return target;
	}

	/**
	 * Specifies that the current property should be unset
	 *
	 * @return The query object
	 */
	@Nonnull
	public T isNull() {
		this.type = StringComparisonType.NULL;
		this.negated = shouldNegateNext;
		shouldNegateNext = false;
		this.value = "";
		return target;
	}

	/**
	 * Specifies that the current property's query should be negated. Usually this method is called
	 * prior to calling one of the other methods to negate this, for example: {@code property.not().equalTo("value")}
	 *
	 * @return This property object
	 */
	@Nonnull
	public StringProperty<T> not() {
		this.shouldNegateNext = true;
		return this;
	}

	/**
	 * Returns the type of comparison done by this property.
	 *
	 * @return The {@link StringComparisonType} denoting the type of comparison done by this property
	 */
	@Nonnull
	public StringComparisonType getType() {
		return type;
	}

	/**
	 * Returns the value of the current property
	 *
	 * @return A String containing the value of this field
	 */
	@Nonnull
	public String getValue() {
		return value;
	}

	/**
	 * Whether or not the current comparison should be negated
	 *
	 * @return {@code true} if the comparison should be negated, {@code false} otherwise
	 */
	public boolean isNegated() {
		return negated;
	}

	@Override
	public boolean isSet() {
		return getType() != StringComparisonType.NONE;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof StringProperty)) return false;

		StringProperty<?> that = (StringProperty<?>) o;

		if (negated != that.negated) return false;
		if (shouldNegateNext != that.shouldNegateNext) return false;
		if (type != that.type) return false;
		return value != null ? value.equals(that.value) : that.value == null;
	}

	@Override
	public int hashCode() {
		int result = type.hashCode();
		result = 31 * result + (value != null ? value.hashCode() : 0);
		result = 31 * result + (negated ? 1 : 0);
		result = 31 * result + (shouldNegateNext ? 1 : 0);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder r = new StringBuilder();

		OrderByLogic.applyOrderBy(orderBy, r);

		if (!isSet()) {
			return r.toString();
		}

		if (isNegated()) {
			r.append('!');
		}

		switch (getType()) {
			case NONE:
				return "";
			case NULL:
				r.append("#");
				return r.toString();
			case LIKE:
				r.append("~");
				break;
			case ILIKE:
				r.append("_~");
				break;
			case EQUALS:
				r.append("=");
				break;
			case EQUALS_IGNORECASE:
				r.append("_=");
				break;
		}

		r.append(getValue());

		return r.toString();
	}

	@Override
	public T orderBy(boolean ascending) {
		if (shouldNegateNext) {
			throw new IllegalStateException("OrderBy cannot be negated");
		}

		orderBy = new OrderBy(target.getNextSortIndex(), ascending);

		return target;
	}
}
