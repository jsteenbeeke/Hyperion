package com.jeroensteenbeeke.hyperion.tardis.scheduler;

import javax.annotation.Nonnull;

/**
 * Representation of a running task
 */
public class HyperionTaskView {
	private final String name;
	
	private final TaskGroup group;
	
	private final HyperionTaskProgress progress;

	/**
	 * Constructor
	 * @param task The task for which to create this view
	 */
	public HyperionTaskView(@Nonnull HyperionTask task) {
		this.name = task.getName();
		this.group = task.getGroup();
		this.progress = task.getProgress();
	}

	/**
	 * Returns the name of the task
	 * @return The name
	 */
	@Nonnull 
	public String getName() {
		return name;
	}

	/**
	 * Returns the group of the task
	 * @return The group
	 */
	@Nonnull 
	public TaskGroup getGroup() {
		return group;
	}

	/**
	 * Returnss the progress of the task
	 * @return The progress
	 */
	@Nonnull
	public HyperionTaskProgress getProgress() {
		return progress;
	}
	
	
}
