package com.jeroensteenbeeke.hyperion.tardis.scheduler.progress;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskProgress;

import javax.annotation.Nonnull;

/**
 * Default implementation that does not know how far along a task is
 */
public class UnknownTaskProgress implements HyperionTaskProgress {

	@Override
	public int getPercentageComplete() {
		return 0;
	}

	@Nonnull
	@Override
	public String getProgressDescription() {
		// TODO: I18N support
		return "Unknown";
	}
}
