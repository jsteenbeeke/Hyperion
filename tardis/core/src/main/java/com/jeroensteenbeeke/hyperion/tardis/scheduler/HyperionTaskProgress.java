package com.jeroensteenbeeke.hyperion.tardis.scheduler;

import javax.annotation.Nonnull;

/**
 * Representation of the progress of a background task
 * 
 * @author Jeroen Steenbeeke
 *
 */
public interface HyperionTaskProgress {
	/**
	 * Determine the progress of the task as a percentage
	 * @return An integer between 0 and 100
	 */
	int getPercentageComplete();
	
	/**
	 * Describes the progress in human-readable terms
	 * @return A String describing this progress
	 */
	@Nonnull
	String getProgressDescription();	
}
