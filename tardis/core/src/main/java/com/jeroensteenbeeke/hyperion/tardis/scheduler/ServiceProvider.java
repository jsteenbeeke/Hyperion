/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler;

/**
 * Dependency lookup mechanism. Tasks generally can't have services injected directly, so they are provided with this
 * mechanism to look up services and DAOs
 */
public interface ServiceProvider {
	/**
	 * Get a service of the specified type
	 * @param serviceClass The class of the desired type. When using Solstice (Spring) this is expected to be an interface
	 * @param <T> The type of service
	 * @return An instance of the service
	 */
	<T> T getService(Class<T> serviceClass);
}
