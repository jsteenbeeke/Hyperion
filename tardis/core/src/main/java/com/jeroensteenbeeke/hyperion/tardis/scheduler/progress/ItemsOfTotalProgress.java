package com.jeroensteenbeeke.hyperion.tardis.scheduler.progress;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskProgress;

import javax.annotation.Nonnull;

/**
 * Progress indicator that specifies completion as a fraction: items completed divided by items total,
 * and converts this to a percentage
 */
public class ItemsOfTotalProgress implements HyperionTaskProgress {
	private final long current;
	
	private final long total;

	/**
	 * Constructor
	 * @param current The current number of items
 	 * @param total The total number of items
	 */
	public ItemsOfTotalProgress(long current, long total) {
		this.current = current;
		this.total = total;
	}

	@Override
	public int getPercentageComplete() {
		return (int) ((100 * current) / total);
	}

	@Nonnull
	@Override
	public String getProgressDescription() {
		return String.format("%d / %d", current, total);
	}

}
