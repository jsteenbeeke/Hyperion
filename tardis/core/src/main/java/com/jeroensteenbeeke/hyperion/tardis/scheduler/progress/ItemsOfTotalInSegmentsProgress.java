package com.jeroensteenbeeke.hyperion.tardis.scheduler.progress;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskProgress;
import com.jeroensteenbeeke.hyperion.util.Asserts;

import javax.annotation.Nonnull;

/**
 * Progress indicator that divides a task into a number of equal-sized segments, each having its own current/total pair
 * 
 * @author Jeroen Steenbeeke
 *
 */
public class ItemsOfTotalInSegmentsProgress implements HyperionTaskProgress {
	private final long current;

	private final long total;

	private final long currentSegment;

	private final long totalSegments;

	/**
	 * Create a new progress object
	 * @param current The current item in this segment
	 * @param total The total number of items in this segment
	 * @param currentSegment The current segment in range [1, totalSegments]
	 * @param totalSegments The total number of segments. Should be at least 1
	 */
	public ItemsOfTotalInSegmentsProgress(long current, long total,
			long currentSegment, long totalSegments) {
		Asserts.numberParam("total").withValue(total).atLeast(1L);
		Asserts.numberParam("current").withValue(current).between(0L, total);
		Asserts.numberParam("totalSegmenets").withValue(totalSegments).atLeast(1L);
		Asserts.numberParam("currentSegment").withValue(currentSegment).between(1L, totalSegments);
		
		this.current = current;
		this.total = total;
		this.currentSegment = currentSegment;
		this.totalSegments = totalSegments;
	}

	@Override
	public int getPercentageComplete() {
		final long segmentSize = 100 / totalSegments;
		final long offset = (currentSegment-1)*segmentSize;
		
		return (int) (offset + (segmentSize * current) / total);
	}

	@Nonnull
	@Override
	public String getProgressDescription() {
		// TODO: I18N support
		return String.format("Step %d of %d, item %d of %d", currentSegment, totalSegments, current, total);
	}

}
