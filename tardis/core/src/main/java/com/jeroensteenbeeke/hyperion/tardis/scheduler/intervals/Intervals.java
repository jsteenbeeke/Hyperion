/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler.intervals;

/**
 * Factory method for creating common intervals
 */
public final class Intervals {
	private Intervals() {
	}

	/**
	 * An interval of a given number of seconds
	 * @param seconds The number of seconds between executions. Should be at least 1, and at most 59
	 * @return An interval representing the repetition of a task every {@code seconds} seconds
	 */
	public static Interval seconds(int seconds) {
		if (seconds < 1 || seconds > 59)
			throw new IllegalArgumentException(
				"seconds should be at least 1 and at most 59");

		return new DefaultInterval("*/" + seconds + " * * * * ?");
	}

	/**
	 * An interval of a given number of minutes
	 * @param minutes The number of minutes between executions. Should be at least 1, and at most 59
	 * @return An interval representing the repetition of a task every {@code minutes} minutes
	 */
	public static Interval minutes(int minutes) {
		if (minutes < 1 || minutes > 59)
			throw new IllegalArgumentException(
				"minutes should be at least 1 and at most 59");

		return new DefaultInterval("0 */" + minutes + " * * * ?");
	}

	/**
	 * An interval of a given number of hours. Executes on the exact hour (keeping in mind scheduling limitations)
	 * @param hours The number of hours between executions. Should be at least 1, and at most 24
	 * @return An interval representing the repetition of a task every {@code hours} hours
	 */
	public static Interval hours(int hours) {
		if (hours < 1 || hours > 24)
			throw new IllegalArgumentException(
				"hours should be at least 1 and at most 24");

		return new DefaultInterval("0 0 */" + hours + " * * ?");
	}

	/**
	 * An interval of a given number of days. Executes a task on 6 in the morning of every matching day
	 *
	 * @param days The number of days between executions. Should be at least 1
	 * @return An interval representing the repetition of a task every {@code days} days
	 */
	public static Interval days(int days) {
		if (days < 1)
			throw new IllegalArgumentException("days should be at least 1");

		return new DefaultInterval("0 0 6 */" + days + " * ?");
	}
}
