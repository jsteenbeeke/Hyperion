/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.progress.ItemsOfTotalInSegmentsProgress;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.progress.ItemsOfTotalProgress;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.progress.PercentageTaskProgress;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.progress.UnknownTaskProgress;
import com.jeroensteenbeeke.hyperion.util.Asserts;

import javax.annotation.Nonnull;

/**
 * A task to be executed by the scheduler. Tasks should be <b>stateless</b>
 *
 */
public abstract class HyperionTask {
	private final String name;

	private final TaskGroup group;
	
	private HyperionTaskProgress progress = new UnknownTaskProgress();

	/**
	 * Constructor
	 * @param name The name of the task
	 * @param group The group of the task
	 */
	protected HyperionTask(String name, TaskGroup group) {
		this.name = name;
		this.group = group;
	}

	/**
	 * Gets the name of the task
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Gets the group of the task
	 * @return the group
	 */
	public final TaskGroup getGroup() {
		return group;
	}

	/**
	 * Gets the progress of the task
	 * @return The progress of the task
	 */
	@Nonnull
	public final HyperionTaskProgress getProgress() {
		return progress;
	}

	/**
	 * Sets the progress of the task to the given percentage
	 * @param percentage The percentage expressed as an integer, valid numbers 0 to 100 (inclusive)
	 */
	public final void setProgress(int percentage) {
		Asserts.numberParam("percentage").withValue(percentage).between(0, 100);
		
		this.progress = new PercentageTaskProgress(percentage);
	}

	/**
	 * Sets the progress of the task to the given amount of total
	 * @param current The current item being processed (should be between 0 and total, inclusive)
	 * @param total The total number of items to be processed (should be 1 or more)
	 */
	public final void setProgress(long current, long total) {
		Asserts.numberParam("total").withValue(total).atLeast(1L);
		Asserts.numberParam("current").withValue(current).between(0L, total);
		
		this.progress = new ItemsOfTotalProgress(current, total);
	}

	/**
	 * Sets the progress of the task to the given amount of total within the current segment, with
	 * the specified current and total segments
	 *
	 * @param current The current item being processed, within the current segment (should be between 0 and total, inclusive)
	 * @param total The total number of items to be processed in the current segment (should be 1 or more)
	 * @param currentSegment The current segment. Should be between 1 and totalSegments
	 * @param totalSegments The total number of segments. Should be at least 1
	 */
	public final void setProgress(long current, long total, long currentSegment, long totalSegments) {
		Asserts.numberParam("total").withValue(total).atLeast(1L);
		Asserts.numberParam("current").withValue(current).between(0L, total);
		Asserts.numberParam("totalSegmenets").withValue(totalSegments).atLeast(1L);
		Asserts.numberParam("currentSegment").withValue(currentSegment).between(1L, totalSegments);
		
		this.progress = new ItemsOfTotalInSegmentsProgress(current, total, currentSegment, totalSegments);
	}

	
	/**
	 * Executes the task
	 * @param provider The callback interface for looking up Spring services
	 */
	public abstract void run(ServiceProvider provider);
}
