package com.jeroensteenbeeke.hyperion.tardis.scheduler.wicket;

import org.apache.wicket.Application;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.AbstractHyperionScheduler;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTask;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskView;

/**
 * Wicket-backed implementation of the scheduler.
 */
public class HyperionScheduler extends AbstractHyperionScheduler {

	private static HyperionScheduler instance;

	private Application application;

	private HyperionScheduler() throws SchedulerException {
		super();
	}

	/**
	 * Gets an instance of the scheduler, creating a new one if it doesn't exist (singleton pattern)
	 * @return The scheduler
	 */
	public static HyperionScheduler getScheduler() {
		if (instance == null) {
			try {
				instance = new HyperionScheduler();
			} catch (SchedulerException e) {
				logger.error(e.getMessage(), e);
			}
		}

		return instance;
	}

	/**
	 * Sets the application running the scheduler
	 * @param application
	 *            the application to set
	 */
	public void setApplication(Application application) {
		this.application = application;
	}

	/**
	 * Get the application running the scheduler
	 * @return the application
	 */
	public Application getApplication() {
		return application;
	}

	@Override
	protected JobDetail createJobDetail(HyperionTask task) {
		JobDetail detail = JobBuilder
				.newJob(WicketBackedHyperionTaskExecutor.class)
				.withIdentity(task.getName().concat(".").concat(Long.toString(System.currentTimeMillis())))
				.withDescription(task.getGroup().getDescriptor()).build();

		detail.getJobDataMap().put(WicketBackedHyperionTaskExecutor.TASK_KEY,
				task);
		detail.getJobDataMap().put(WicketBackedHyperionTaskExecutor.APP_KEY,
				getApplication());
		return detail;
	}

	@Override
	protected HyperionTaskView contextToView(JobExecutionContext context) {
		return new HyperionTaskView(
				(HyperionTask) context.getJobDetail().getJobDataMap()
						.get(WicketBackedHyperionTaskExecutor.TASK_KEY));
	}

}
