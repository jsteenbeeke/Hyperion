package com.jeroensteenbeeke.hyperion.tardis.scheduler.wicket;

import java.util.List;

import javax.annotation.Nonnull;

import org.apache.wicket.model.LoadableDetachableModel;
import org.danekja.java.util.function.serializable.SerializablePredicate;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskView;

/**
 * Wicket Model that contains an up-to-date list of all running tasks
 * 
 * @author Jeroen Steenbeeke
 */
public class TaskModel extends LoadableDetachableModel<List<HyperionTaskView>> {
	private static final long serialVersionUID = 1L;

	private final SerializablePredicate<HyperionTaskView> predicate;

	/**
	 * Create a model that lists all tasks
	 */
	public TaskModel() {
		this(t -> true);
	}

	/**
	 * Create a model that lists all tasks matching the given predicate
	 * 
	 * @param predicate
	 *            The predicate to filter tasks by
	 */
	public TaskModel(
			@Nonnull SerializablePredicate<HyperionTaskView> predicate) {
		this.predicate = predicate;
	}

	@Override
	protected List<HyperionTaskView> load() {

		return HyperionScheduler.getScheduler().getRunningTasks(predicate);
	}
}
