/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler.wicket;

import org.apache.wicket.Application;
import org.apache.wicket.ThreadContext;
import org.apache.wicket.protocol.http.mock.MockHttpServletRequest;
import org.apache.wicket.protocol.http.mock.MockHttpSession;
import org.apache.wicket.protocol.http.mock.MockServletContext;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationContextProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.ApplicationContextServiceProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTask;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.NullServiceProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.ServiceProvider;

/**
 * Wicket-backed task executor. Uses the Wicket application to retrieve
 * the Spring applicationContext to use as service provider
 */
public class WicketBackedHyperionTaskExecutor implements Job {
	static final String TASK_KEY = "HyperionTask";

	static final String APP_KEY = "HyperionApplication";

	private static final Logger logger = LoggerFactory
			.getLogger(WicketBackedHyperionTaskExecutor.class);

	/**
	 * Constructor
	 */
	public WicketBackedHyperionTaskExecutor() {
	}

	@Override
	public void execute(JobExecutionContext context) {
		HyperionTask task = (HyperionTask) context.getMergedJobDataMap().get(
				TASK_KEY);

		Application application = (Application) context.getMergedJobDataMap()
													   .get(APP_KEY);
		ThreadContext.setApplication(application);

		ServiceProvider provider;

		if (application instanceof ApplicationContextProvider) {
			ApplicationContextProvider appCtxProv = (ApplicationContextProvider) application;
			provider = new ApplicationContextServiceProvider(
					appCtxProv.getApplicationContext());
		} else {
			provider = new NullServiceProvider();
		}

		if (task != null) {
			if (HyperionScheduler.getScheduler().isLogTaskLifecycle()) {
				logger.info("Starting background task {}:{}", task.getGroup().getDescriptor(), task.getName());
			}

			MockServletContext sctx = new MockServletContext(application,
															 "src/main/webapp/");
			MockHttpServletRequest request = new MockHttpServletRequest(
					application, new MockHttpSession(sctx), sctx);
			RequestAttributes attr = new ServletRequestAttributes(request);

			RequestContextHolder.setRequestAttributes(attr);

			try {
				task.run(provider);
			} catch (Exception e) {
				if (HyperionScheduler.getScheduler().isLogTaskLifecycle()) {
					logger.error("Task {}:{} resulted in error", task.getGroup().getDescriptor(), task.getName());
					logger.error(e.getMessage(), e);
				}
				HyperionScheduler.getScheduler().onException(e);
				throw e;
			}

			RequestContextHolder.resetRequestAttributes();

			if (HyperionScheduler.getScheduler().isLogTaskLifecycle()) {
				logger.info("Completed background task {}:{}", task.getGroup().getDescriptor(), task.getName());
			}
		} else {
			logger.error("No task passed to HyperionTaskExecutor!");
		}
	}
}
