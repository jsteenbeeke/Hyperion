package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import static org.junit.Assert.*;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringConfigTest {
	public static final AtomicBoolean hasBeenCalled = new AtomicBoolean(false);
	
	@Test
	public void testSpringConfig() {
		DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
		
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
				factory)) {
			ctx.register(TestConfig.class);
			ctx.refresh();
			
			assertTrue(hasBeenCalled.get());
		}
	}
}
