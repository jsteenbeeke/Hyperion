package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.springframework.stereotype.Component;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.OnSchedulerInitializedCallback;

@Component
class TestInitializer implements OnSchedulerInitializedCallback {
	

	@Override
	public void onSchedulerInitialized() {
		SpringConfigTest.hasBeenCalled.set(true);
	}

}
