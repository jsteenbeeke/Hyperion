/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.ApplicationContextServiceProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTask;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.NullServiceProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.ServiceProvider;

/**
 * Quartz job that executes a Hyperion task
 */
public class SpringBackedHyperionTaskExecutor implements Job {
	public static final String TASK_KEY = "HyperionTask";

	public static final String CONTEXT_KEY = "SpringApplicationContext";

	private static final Logger logger = LoggerFactory
			.getLogger(SpringBackedHyperionTaskExecutor.class);

	/**
	 * Constructor
	 */
	public SpringBackedHyperionTaskExecutor() {
	}

	@Override
	public void execute(JobExecutionContext context) {
		HyperionTask task = (HyperionTask) context.getMergedJobDataMap().get(
				TASK_KEY);

		ApplicationContext applicationContext = (ApplicationContext) context
				.getMergedJobDataMap().get(CONTEXT_KEY);

		ServiceProvider provider;

		if (applicationContext != null) {
			provider = new ApplicationContextServiceProvider(applicationContext);
		} else {
			provider = new NullServiceProvider();
		}

		if (task != null) {
			if (HyperionScheduler.getScheduler().isLogTaskLifecycle()) {
				logger.info("Starting background task {}:{}", task.getGroup().getDescriptor(), task.getName());
			}
			
			MockHttpServletRequest request = new MockHttpServletRequest();
			RequestAttributes attr = new ServletRequestAttributes(request);

			RequestContextHolder.setRequestAttributes(attr);

			try {
				task.run(provider);
			} catch (Exception e) {
				if (HyperionScheduler.getScheduler().isLogTaskLifecycle()) {
					logger.error("Task {}:{} resulted in error", task.getGroup().getDescriptor(), task.getName());
					logger.error(e.getMessage(), e);
				}
				HyperionScheduler.getScheduler().onException(e);
				throw e;
			}

			RequestContextHolder.resetRequestAttributes();

			if (HyperionScheduler.getScheduler().isLogTaskLifecycle()) {
				logger.info("Completed background task {}:{}", task.getGroup().getDescriptor(), task.getName());
			}
		} else {
			logger.error("No task passed to HyperionTaskExecutor!");
		}
	}
}
