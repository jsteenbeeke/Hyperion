package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.AbstractHyperionScheduler;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTask;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskView;

/**
 * Spring-backed implementation of the scheduler
 */
public class HyperionScheduler extends AbstractHyperionScheduler {
	private static HyperionScheduler instance;

	private ApplicationContext context;

	private HyperionScheduler() throws SchedulerException {
		super();
	}

	/**
	 * Gets the current scheduler, creating a new one if none exists
	 * @return The scheduler
	 */
	public static HyperionScheduler getScheduler() {
		if (instance == null) {
			try {
				instance = new HyperionScheduler();
			} catch (SchedulerException e) {
				logger.error(e.getMessage(), e);
			}
		}

		return instance;
	}

	/**
	 * Sets the Spring ApplicationContext
	 * @param context The context to set
	 */
	void setApplicationContext(ApplicationContext context) {
		this.context = context;
	}

	@Override
	protected JobDetail createJobDetail(HyperionTask task) {
		JobDetail detail = JobBuilder
				.newJob(SpringBackedHyperionTaskExecutor.class)
				.withIdentity(task.getName().concat(".").concat(Long.toString(System.currentTimeMillis())))
				.withDescription(task.getGroup().getDescriptor()).build();

		detail.getJobDataMap().put(SpringBackedHyperionTaskExecutor.TASK_KEY,
				task);
		detail.getJobDataMap().put(SpringBackedHyperionTaskExecutor.CONTEXT_KEY,
				context);
		return detail;
	}

	@Override
	protected HyperionTaskView contextToView(JobExecutionContext context) {
		return new HyperionTaskView(
				(HyperionTask) context.getJobDetail().getJobDataMap()
						.get(SpringBackedHyperionTaskExecutor.TASK_KEY));
	}

}
