/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.OnSchedulerInitializedCallback;

import java.util.List;

/**
 * Spring event listener that launches the scheduler
 */
public class SpringBasedInitializer
		implements ApplicationListener<ContextRefreshedEvent> {
	private static final Logger log = LoggerFactory
			.getLogger(SpringBasedInitializer.class);

	@Autowired(required = false)
	private List<OnSchedulerInitializedCallback> callbacks;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.info("Initializing Tardis scheduler");
		HyperionScheduler.getScheduler()
				.setApplicationContext(event.getApplicationContext());
		if (HyperionScheduler.getScheduler().isShutDown()) {
			return;
		}

		log.info("Scheduler callbacks exist: {}", callbacks != null && !callbacks.isEmpty());

		if (callbacks != null) {
			callbacks.forEach(c -> log.info("- {}", c.getClass().getName()));
			callbacks.forEach(
					OnSchedulerInitializedCallback::onSchedulerInitialized);
		}

	}
}
