package com.jeroensteenbeeke.hyperion.cmdline;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertEquals;

public final class Util {

	static <T, E extends Exception> void assertOutput(AtomicReference<ByteArrayOutputStream> out, String expected, T input, Invocation<T, E>
			invocation)
			throws
			E {
		try {
			invocation.accept(input);
		} finally {
			String actual = new String(out.get().toByteArray());
			out.set(new ByteArrayOutputStream());
			System.setOut(new PrintStream(out.get()));
			assertEquals(expected, actual);
		}
	}

	public interface Invocation<T, E extends Exception> {
		void accept(T input) throws E;
	}
}
