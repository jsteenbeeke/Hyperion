package com.jeroensteenbeeke.hyperion.cmdline;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AnsiTest {
	static final String JACOCO_DATA = "$jacocoData";

	@Test
	public void testAnsiValues() throws IllegalAccessException {
		for (Field field : Ansi.class.getDeclaredFields()) {
			final int modifiers = field.getModifiers();

			final String name = field.getName();
			
			if (JACOCO_DATA.equals(name)) {
				continue;
			}
			
			assertTrue(String.format("Ansi.%s is either not static or not final", name), Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers));
			assertEquals(String.format("Ansi.%s is not a String", name), String.class, field.getType());
			String fieldValue = (String) field.get(null);

			assertNotNull(String.format("Ansi.%s is not set", name));
			assertTrue(String.format("Ansi.%s is not a valid value (%s)", name, fieldValue), fieldValue.matches("^\\u001b\\[[0-9a-fA-F]+(;[0-9a-fA-F]+)?m?$"));

			System.out.println(fieldValue);
		}

	}
}
