package com.jeroensteenbeeke.hyperion.cmdline;

import com.google.common.io.Files;
import com.jeroensteenbeeke.lux.TypedResult;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class ArgumentsTest {


	@Test
	public void testArgumentSpecification() throws IOException {
		File tempDir = Files.createTempDir();
		File file = new File(tempDir, "file.txt");
		File subDir = new File(tempDir, "sub");
		File subDirFile = new File(subDir, "another.txt");
		File subSubDir = new File(subDir, "subsub");


		try {
			assertTrue(file.createNewFile());
			String abspath = tempDir.getAbsolutePath();
			assertTrue(subDir.mkdir());
			assertTrue(subDirFile.createNewFile());

			String destination = abspath.concat(System.getProperty("file.separator")).concat("output");

			Arguments arguments = createArguments();


			String[][] validInputs = new String[][]{
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath()
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath()
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath()
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					}

			};

			for (String[] input : validInputs) {
				TypedResult<Map<String, Object>> validity = arguments.checkValidity(input);

				arguments.run(input, Assert::assertNotNull, Assert::assertNull);
			}

			String[][] missingInputs = new String[][]{
					{},
					{
							"true"
					},
					{
							"true", abspath
					},
					{
							"true", abspath, file.getAbsolutePath()
					},
					{
							"true", abspath, file.getAbsolutePath(), destination
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom"
					}
			};

			for (String[] input : missingInputs) {
				Consumer<Map<String, Object>> program = r -> {
					throw new AssertionError(
							String.format("Input [%s] unexpectedly considered valid", Arrays.stream(input).collect(
									Collectors.joining(", "))));
				};
				arguments.run(input, program, Assert::assertNotNull);
			}

			String[][] invalidInputs = {
					{
							"maybe", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", file.getAbsolutePath(), file.getAbsolutePath(), destination, "5",
							"2489999344023433",
							"Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, abspath, destination, "5", "2489999344023433",
							"Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", "/tmp" + abspath, file.getAbsolutePath(), destination, "5", "2489999344023433",
							"Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, "/tmp" + file.getAbsolutePath(), destination, "5", "2489999344023433",
							"Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), "/tmp" + destination, "5a", "2489999344023433",
							"Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433a", null,
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"I_am_an_URL", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "maybe", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", "/tmp" + subDir.getAbsolutePath(),
							subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(),
							"/tmp" + subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							"/tmp" + subSubDir.getAbsolutePath(), "5b", "800000", "John", "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000b", null, "http://www.wikipedia.org"
					},
					{
							"true", abspath, file.getAbsolutePath(), destination, "5", "2489999344023433", "Your mom",
							"http://www.google.com/", "false", subDir.getAbsolutePath(), subDirFile.getAbsolutePath(),
							subSubDir.getAbsolutePath(), "5", "800000", "John", "I_am_an_URL"
					}
			};

			for (String[] input : invalidInputs) {
				Consumer<Map<String, Object>> program = r -> {
					throw new AssertionError(
							String.format("Input [%s] unexpectedly considered valid", Arrays.stream(input).collect(
									Collectors.joining(", "))));
				};
				arguments.run(input, program, Assert::assertNotNull);
			}
		} finally {
			file.delete();
			subDirFile.delete();
			subDir.delete();
			tempDir.delete();
		}
	}

	@Test
	public void testUsage() throws IOException {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); PrintStream ps = new PrintStream(bos)) {
			Arguments arg = createArguments();

			arg.printUsage(ps);

			ps.flush();

			String output = new String(bos.toByteArray());

			assertEquals("Usage:\n" +
					"\tJenkins truth location file destination iterations seed label source [falseness] " +
					"[sublocation]" +
					" " +
					"[subfile] [subsublocation] [maxFailCount] [maxTime] [john] [target]\n" +
					"Parameters:\n" +
					"\ttruth           (boolean)\n" +
					"\t\tWhether or not Jenkins should tell the truth\n" +
					"\tlocation        (directory)\n" +
					"\t\tIn which folder we can find the truth\n" +
					"\tfile            (file)\n" +
					"\t\tIn which file the truth lies\n" +
					"\tdestination     (directory)\n" +
					"\t\tWhere the truth should be written\n" +
					"\titerations      (integer)\n" +
					"\t\tHow long should we try to see the truth?\n" +
					"\tseed            (long)\n" +
					"\t\tHow to seed the randomizer in our search for truth\n" +
					"\tlabel           (string)\n" +
					"\t\tHow we should label any untruths\n" +
					"\tsource          (url)\n" +
					"\t\tWhich remote location we should use for fact-checking\n" +
					"\tfalseness       (boolean, optional)\n" +
					"\t\tIf the answer may be false (default: false)\n" +
					"\tsublocation     (directory, optional)\n" +
					"\t\tA folder within a folder\n" +
					"\tsubfile         (file, optional)\n" +
					"\t\tA file within a folder within a folder\n" +
					"\tsubsublocation  (directory, optional)\n" +
					"\t\tA folder within a folder within a folder\n" +
					"\tmaxFailCount    (integer, optional)\n" +
					"\t\tHow often we may fail\n" +
					"\tmaxTime         (long, optional)\n" +
					"\t\tHow long the calculation may last in milliseconds\n" +
					"\tjohn            (string, optional)\n" +
					"\t\tBibilical revelations\n" +
					"\ttarget          (url, optional)\n" +
					"\t\tTo which URL we should post the results\n", output);
		}
	}

	@Test
	public void testArgumentLists() {
		assertTrue(Arguments.builder().getOptional().isEmpty());
		assertTrue(Arguments.builder().getRequired().isEmpty());
		assertFalse(Arguments.builder().withBooleanArgument("test", "test").getRequired().isEmpty());
		assertTrue(Arguments.builder().withBooleanArgument("test", "test").getOptional().isEmpty());
		assertFalse(Arguments.builder().withBooleanArgument("test", "test").andOptionally().getRequired().isEmpty());
		assertTrue(Arguments.builder().withBooleanArgument("test", "test").andOptionally().getOptional().isEmpty());
		assertFalse(Arguments.builder().withBooleanArgument("test", "test").andOptionally()
				.withBooleanArgument("test2", "test2").getRequired().isEmpty());
		assertFalse(Arguments.builder().withBooleanArgument("test", "test").andOptionally()
				.withBooleanArgument("test2", "test2").getOptional().isEmpty());
	}

	@Test
	public void testStringJoins() {
		Arguments arguments = Arguments.builder().withBooleanArgument("truth", "A boolean parameter")
				.withStringArgument("parameter", "The String to use").forProgram("Stringify");

		arguments.run(new String[]{"true", "\"I", "am", "a", "Stegosaurus!\""},
				args -> assertEquals("I am a Stegosaurus!", args.get("parameter")), Assert::assertNull);

		arguments.run(new String[]{"true", "\"I", "am", "a", "\\\"", "mark!\""},
				args -> assertEquals("I am a \" mark!", args.get("parameter")), Assert::assertNull);

		arguments.run(new String[]{"true", "\"quotes"},
				args -> assertEquals("\"quotes", args.get("parameter")), Assert::assertNull);
	}

	private Arguments createArguments() {
		return Arguments.builder().withBooleanArgument("truth", "Whether or not Jenkins should tell the truth")
				.withExistingFolderArgument("location", "In which folder we can find the truth")
				.withFileArgument("file", "In which file the truth lies")
				.withFolderArgument("destination", "Where the truth should be written")
				.withIntArgument("iterations", "How long should we try to see the truth?")
				.withLongArgument("seed", "How to seed the randomizer in our search for truth")
				.withStringArgument("label", "How we should label any untruths")
				.withUrlArgument("source", "Which remote location we should use for fact-checking")
				.andOptionally()
				.withBooleanArgument("falseness", "If the answer may be false (default: false)")
				.withExistingFolderArgument("sublocation", "A folder within a folder")
				.withFileArgument("subfile", "A file within a folder within a folder")
				.withFolderArgument("subsublocation", "A folder within a folder within a folder")
				.withIntArgument("maxFailCount", "How often we may fail")
				.withLongArgument("maxTime", "How long the calculation may last in milliseconds")
				.withStringArgument("john", "Bibilical revelations")
				.withUrlArgument("target", "To which URL we should post the results")
				.forProgram("Jenkins");
	}
}
