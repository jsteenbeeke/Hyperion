package com.jeroensteenbeeke.hyperion.cmdline;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import static org.junit.Assert.*;

public class OutputTest {
	private static PrintStream oldSysout;

	private static AtomicReference<ByteArrayOutputStream> byteOut;

	private static RatpackServer ratpack;

	@BeforeClass
	public static void setUp() throws Exception {
		oldSysout = System.out;
		byteOut = new AtomicReference<>(new ByteArrayOutputStream());
		System.setOut(new PrintStream(byteOut.get()));
		ratpack =
				RatpackServer.start(server -> server.serverConfig(ServerConfig.builder().port(8765).build())
						.handlers(chain -> {
							chain.get("good", ctx -> ctx.render("I am a successful response!"));
							chain.get("bad", ctx -> ctx.clientError(405));
						}));
	}

	@Test
	public void testColorOutput() {
		final String input = "I am test-text";

		testInput(input, Ansi.BLACK, Output::black);
		testInput(input, Ansi.RED, Output::red);
		testInput(input, Ansi.GREEN, Output::green);
		testInput(input, Ansi.BROWN, Output::brown);
		testInput(input, Ansi.BLUE, Output::blue);
		testInput(input, Ansi.PURPLE, Output::purple);
		testInput(input, Ansi.CYAN, Output::cyan);
		testInput(input, Ansi.GREY, Output::grey);
		testInput(input, Ansi.YELLOW, Output::yellow);
		testInput(input, Ansi.LIGHT_BLUE, Output::lightBlue);
		testInput(input, Ansi.WHITE, Output::white);
	}

	@Test
	public void testOutputFunction() {
		final String newline = System.getProperty("line.separator");

		Util.assertOutput(byteOut, "[" + Ansi.CYAN + "++++" + Ansi.RESET + "] Test" + newline, "Test", Output::header);
		Util.assertOutput(byteOut, "[" + Ansi.CYAN + "info" + Ansi.RESET + "] Test" + newline, "Test", Output::info);
		Util.assertOutput(byteOut, "[" + Ansi.GREEN + " ok " + Ansi.RESET + "] Test" + newline, "Test", Output::ok);
		Util.assertOutput(byteOut, "[" + Ansi.YELLOW + " q? " + Ansi.RESET + "] Test" + newline, "Test", Output::question);
		Util.assertOutput(byteOut, "[" + Ansi.BROWN + "warn" + Ansi.RESET + "] Test" + newline, "Test", Output::warn);
		Util.assertOutput(byteOut, "[" + Ansi.RED + "fail" + Ansi.RESET + "] Test" + newline, "Test", Output::fail);
		Util.assertOutput(byteOut, "[" + Ansi.PURPLE + "done" + Ansi.RESET + "] Test" + newline, "Test", Output::done);
		Util.assertOutput(byteOut, "[" + Ansi.LIGHT_BLUE + "1" + Ansi.RESET + "] Test" + newline, "Test", i -> Output.option(1, 1,
				i));
	}

	@Test
	public void testSuccessfulDownload() throws Exception {

		final String newline = System.getProperty("line.separator");

		File target = File.createTempFile("target", ".txt");

		URL url = new URL("http://localhost:8765/good");
		Util.assertOutput(byteOut,
				Ansi.RESET + String.format("Downloading %-160s", url.toString()) + " [" +
						Ansi.GREEN + "++++++++++++++++++++++++++++++" + Ansi.RESET + "]" + newline + "[" +
						Ansi.GREEN + " ok " + Ansi.RESET +
						"] Downloaded " + url.toString() + newline, url,
				u -> assertTrue(FileFetcher.bar(u, target).isOk()));
		Util.assertOutput(byteOut,
				Ansi.RESET + String.format("Downloading %-160s", url.toString()) + " [" +
						Ansi.GREEN + rotatify("-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|") + Ansi.RESET + "]" + newline +
						"[" +
						Ansi.GREEN + " ok " + Ansi.RESET +
						"] Downloaded " + url.toString() + newline, url,
				u -> assertTrue(FileFetcher.rotator(u, target).isOk()));

		assertTrue(FileFetcher.invisible(url, target).isOk());

		target.deleteOnExit();
	}

	private String rotatify(String input) {
		StringBuilder sb = new StringBuilder();

		for (char c : input.toCharArray()) {
			if (sb.length() > 0) {
				sb.append(Ansi.KEY_LEFT);
			}
			sb.append(c);
		}

		return sb.toString();
	}

	@Test
	public void testFailedDownload() throws Exception {
		final String newline = System.getProperty("line.separator");

		File target = File.createTempFile("target", ".txt");

		URL url = new URL("http://localhost:8765/bad");
		Util.assertOutput(byteOut,
				Ansi.RESET + String.format("Downloading %-160s", url.toString()) + " [" +
						Ansi.GREEN + Ansi.RED + "!" + Ansi.RESET + "]" + newline + "[" +
						Ansi.RED + "fail" + Ansi.RESET +
						"] Failed to download http://localhost:8765/bad" + newline, url,
				u -> assertFalse(FileFetcher.bar(u, target).isOk()));
		Util.assertOutput(byteOut,
				Ansi.RESET + String.format("Downloading %-160s", url.toString()) + " [" +
						Ansi.GREEN + "-"+ Ansi.RED + "!" + Ansi.RESET + "]" + newline + "[" +
						Ansi.RED + "fail" + Ansi.RESET +
						"] Failed to download http://localhost:8765/bad" + newline, url,
				u -> assertFalse(FileFetcher.rotator(u, target).isOk()));
		assertFalse(FileFetcher.invisible(url, target).isOk());

	}



	private void testInput(String input, String colorConstant, Function<String, String> colorizeFunction) {
		assertEquals(colorConstant + input + Ansi.RESET, colorizeFunction.apply(input));
	}

	@AfterClass
	public static void tearDown() throws Exception {
		System.setOut(oldSysout);
		ratpack.stop();
	}


}
