package com.jeroensteenbeeke.hyperion.cmdline;

import com.google.common.io.Files;
import com.jeroensteenbeeke.hyperion.util.FileUtil;
import com.jeroensteenbeeke.hyperion.util.Randomizer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UnzipperTest {
	private static PrintStream oldSysout;

	private static AtomicReference<ByteArrayOutputStream> byteOut;

	@BeforeClass
	public static void setUp() throws Exception {
		oldSysout = System.out;
		byteOut = new AtomicReference<>(new ByteArrayOutputStream());
		System.setOut(new PrintStream(byteOut.get()));
	}

	@Test
	public void testUnzip() throws IOException {
		final String newline = System.getProperty("line.separator");

		File tempDir = Files.createTempDir();

		try {
			File zip = new File(tempDir, "random.zip");
			File target = new File(tempDir, "target");
			assertTrue(target.mkdir());

			ZipOutputStream stream = new ZipOutputStream(new FileOutputStream(zip));
			stream.putNextEntry(new ZipEntry("random.txt"));

			byte[] data = Randomizer.randomBytes(1337);

			stream.write(data, 0, data.length);
			stream.closeEntry();
			stream.close();


			Util.assertOutput(byteOut,
					Ansi.RESET + "Extracting " + String.format("%-40s [", zip.getName()) + Ansi.GREEN +
							"++++++++++++++++++++++++++++++" + Ansi.RESET + "]" + newline, zip,
					z -> VisualUnzipper.bar(z, target));

			Util.assertOutput(byteOut,
					Ansi.RESET + "Extracting " + String.format("%-40s [", target.getName()) + Ansi.GREEN , target, z -> VisualUnzipper.bar(z, target));

		} finally {
			assertTrue(FileUtil.deleteTree(tempDir));
		}


	}

	@AfterClass
	public static void tearDown() throws Exception {
		System.setOut(oldSysout);
	}


}
