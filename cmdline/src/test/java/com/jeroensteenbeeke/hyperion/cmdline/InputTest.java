package com.jeroensteenbeeke.hyperion.cmdline;

import com.google.common.collect.ImmutableList;
import org.junit.*;

import java.io.*;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class InputTest {
	private static InputStream oldIn;

	private static PrintStream oldOut;

	private static ByteArrayOutputStream byteOut;


	@BeforeClass
	public static void setUp() throws Exception {
		oldIn = System.in;
		oldOut = System.out;
		System.setOut(new PrintStream(byteOut = new ByteArrayOutputStream()));

	}

	@Test(timeout = 3000L)
	public void testYesNoQuestion() {
		println("0", "3", "horse", "1");
		assertTrue(Input.yesOrNo("Yes or no?"));
		println("2");
		assertFalse(Input.yesOrNo("Yes or no?"));
	}

	@Test
	public void testMultipleChoiceQuestion() {
		Exception except = null;
		try {
			Input.multipleChoice("Are you an elephant?", ImmutableList.of());
		} catch (IllegalArgumentException e) {
			except = e;
		}
		assertNotNull(except);

		assertTrue(Input.multipleChoice("Are you an elephant", ImmutableList.of(true)));
	}

	@Test
	public void testOpenQuestion() {
		println("rabbit");
		assertEquals("rabbit", Input.openQuestion("What animal are you?"));
	}

	private void print(String input) {
		System.setIn(new ByteArrayInputStream(input.getBytes()));
	}

	private void println(String... input) {
		String newline = System.getProperty("line.separator");
		print(Arrays.stream(input).collect(
				Collectors.joining(newline, "", newline)));
	}

	@AfterClass
	public static void tearDown() throws Exception {
		System.setIn(oldIn);
		System.setOut(oldOut);
	}
}
