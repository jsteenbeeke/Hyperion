/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline.arguments;

import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;

/**
 * Representation of a command-line argument
 * @param <T> The type of the argument
 */
public interface Argument<T> {
	/**
	 * Converts the given String argument to the required type
	 * @param argument The String argument to convert
	 * @return A TypedResult containing the converted value, or an error message
	 */
	@Nonnull
	TypedResult<T> parse(@Nonnull String argument);

	/**
	 * The name of this argument
	 * @return A String containing the name
	 */
	@Nonnull
	String getName();

	/**
	 * A description of the type of this argument
	 * @return A String describing the type of this argument
	 */
	@Nonnull
	String getType();

	/**
	 * A description of the purpose of this argument
	 * @return A String containing a description
	 */
	@Nonnull
	String getDescription();
}
