/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline.arguments;

import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;

/**
 * String argument, basically foregoes parsing the argument aside from what was already done by the Arguments class itself
 */
public class StringArgument extends AbstractArgument<String> {

	/**
	 * Create a new StringArgument
	 * @param name The name of the argument
	 * @param description The description of the argument
	 */
	public StringArgument(String name, String description) {
		super(name, "string", description);
	}

	@Nonnull
	@Override
	public TypedResult<String> parse(@Nonnull String argument) {
		return TypedResult.ok(argument);
	}
}
