/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import com.jeroensteenbeeke.lux.ActionResult;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;

/**
 * Utility class for unzipping files with a progress indicator
 */
public class VisualUnzipper {
	/**
	 *
	 * Extracts the given archive to the target folder
	 * @param archive The archive to extract
	 * @param targetFolder The folder to extract the archive to
	 * @return An ActionResult indicating either success or the reason for failure
	 */
	public static ActionResult bar(File archive, File targetFolder) {
		if (!archive.exists()) {
			return ActionResult.error("Invalid archive: %s", archive);
		}
		
		System.out.print(Ansi.RESET);
		System.out.printf("Extracting %-40s [", archive.getName());
		System.out.print(Ansi.GREEN);
		
		try (ZipFile zip = new ZipFile(archive)) {
			Enumeration<ZipArchiveEntry> entries = zip.getEntries();
			
			long total = 0L;
			while (entries.hasMoreElements()) {
				ZipArchiveEntry entry = entries.nextElement();
				
				total = total + entry.getSize();
			}
			
			entries = zip.getEntries();
			while (entries.hasMoreElements()) {
				ZipArchiveEntry entry = entries.nextElement();

				String entryName = entry.getName();
				int slashIndex = entryName.indexOf('/');
				File folder = targetFolder;
				
				while (slashIndex != -1) {
					String path = entryName.substring(0, slashIndex);
					folder = new File(folder, path);
					entryName = entryName.substring(slashIndex+1);
					slashIndex = entryName.indexOf('/');
				}

				if (!folder.exists()) {
					if (!folder.mkdirs()) {
						return ActionResult.error("Target directory %s does not exist, and failed to create", folder.getAbsolutePath());
					}
				}
				
				File targetFile = new File(folder,
						entryName);

				try (InputStream in = zip.getInputStream(entry);
						OutputStream out = new FileOutputStream(
								targetFile)) {
					ProgressReporter.copyStream(in, out, total);
				}
				
				
				
			}
			System.out.print(Ansi.RESET);
			System.out.println("]");
			
			return ActionResult.ok();
		} catch (IOException ioe) {
			return ActionResult.error("Failed to extract %s: %s", archive.getName(), ioe.getMessage());
		}
	}
}
