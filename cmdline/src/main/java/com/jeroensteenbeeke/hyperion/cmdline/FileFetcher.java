/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline;

import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utility class to download files, optionally with visual feedback
 */
public class FileFetcher {
	private static final String ROTATOR_CHARS = "-\\|/";

	/**
	 * Download a file without visual feedback
	 * @param target The URL to download
	 * @param targetFile The file to save the data at the URL to
	 * @return An ActionResult indicating success or the reason for failure
	 */
	@Nonnull
	public static ActionResult invisible(@Nonnull URL target, @Nonnull File targetFile) {
		try {
			URLConnection connection = target.openConnection();

			try (InputStream in = connection.getInputStream();
				 OutputStream out = new FileOutputStream(targetFile)) {
				int data;

				while ((data = in.read()) != -1) {
					out.write(data);
				}
				out.flush();

				return ActionResult.ok();
			}
		} catch (IOException e) {
			return ActionResult.error(e.getMessage());
		}
	}

	/**
	 * Download a file with a rotator feedback (a single character that gets replaced and resembles a rotating dash)
	 * @param target The URL to download
	 * @param targetFile The file to save the data at the URL to
	 * @return An ActionResult indicating success or the reason for failure
	 */
	@Nonnull
	public static ActionResult rotator(@Nonnull URL target, @Nonnull File targetFile) {
		try {

			URLConnection connection = target.openConnection();

			int idx = 0;

			long count = 0L;

			long perc = 0;

			System.out.print(Ansi.RESET);
			System.out.printf("Downloading %-160s [", target.toString());
			System.out.print(Ansi.GREEN);

			System.out.print(ROTATOR_CHARS.charAt(idx));

			try (InputStream in = connection.getInputStream();
				 OutputStream out = new FileOutputStream(targetFile)) {
				int data;

				final long total = Math.max(connection.getContentLengthLong(),
						(long) in.available());

				while ((data = in.read()) != -1) {
					out.write(data);

					count++;

					long np = ProgressReporter.SEGMENTSIZE * count / total;

					while (perc < np) {
						idx = ++idx % ROTATOR_CHARS.length();

						System.out.print(Ansi.KEY_LEFT);
						System.out.print(ROTATOR_CHARS.charAt(idx));
						perc++;
					}


				}

				out.flush();

				System.out.print(Ansi.RESET);
				System.out.println("]");
			}
		} catch (IOException ioe) {
			return reportFailure(target, ioe);
		}

		Output.ok("Downloaded %s", target.toString());


		return ActionResult.ok();
	}

	@Nonnull
	private static ActionResult reportFailure(@Nonnull URL target, @Nonnull IOException ioe) {
		System.out.print(Ansi.RED);
		System.out.print("!");
		System.out.print(Ansi.RESET);
		System.out.println("]");
		Output.fail("Failed to download %s", target.toString());
		return ActionResult.error(ioe.getMessage());
	}

	/**
	 * Download the file at the given URL, displaying a progress bar next to it
	 * @param target The URL to download
	 * @param targetFile The file to save the download to
	 * @return An ActionResult indicating success or the reason for failure
	 */
	@Nonnull
	public static ActionResult bar(@Nonnull URL target, @Nonnull File targetFile) {
		try {
			URLConnection connection = target.openConnection();


			System.out.print(Ansi.RESET);
			System.out.printf("Downloading %-160s [", target.toString());
			System.out.print(Ansi.GREEN);



			try (InputStream in = connection.getInputStream();
				 OutputStream out = new FileOutputStream(targetFile)) {
				final long total = Math.max(connection.getContentLengthLong(),
						(long) in.available());
				ProgressReporter.copyStream(in, out, total);

				System.out.print(Ansi.RESET);
				System.out.println("]");
			}
		} catch (IOException ioe) {
			return reportFailure(target, ioe);
		}

		Output.ok("Downloaded %s", target.toString());

		return ActionResult.ok();
	}


}
