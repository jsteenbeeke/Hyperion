/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline.arguments;

import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;
import java.net.URL;

/**
 * URL argument, i.e. anything that would be well-formed according to {@code java.net.URL}
 * @see java.net.URL
 */
public class UrlArgument extends AbstractArgument<URL> {

	/**
	 * Create a new UrlArgument
	 * @param name The name of the argument
	 * @param description The description of the argument
	 */
	public UrlArgument(String name, String description) {
		super(name, "url", description);
	}

	@Nonnull
	@Override
	public TypedResult<URL> parse(@Nonnull String argument) {
		return TypedResult.attempt(() -> new URL(argument));
	}
}
