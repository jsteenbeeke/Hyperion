/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline.arguments;

import javax.annotation.Nonnull;

/**
 * Basic implementation of a command-line argument
 * @param <T> The type of argument
 */
public abstract class AbstractArgument<T> implements Argument<T> {
	private final String name;
	
	private final String type;
	
	private final String description;

	/**
	 * Create a new AbstractArgument
	 * @param name The name of the argument
	 * @param type A description of the type of the argument
	 * @param description A description of the argument
	 */
	AbstractArgument(@Nonnull String name, @Nonnull String type, @Nonnull String description) {
		super();
		this.name = name;
		this.type = type;
		this.description = description;
	}


	@Nonnull
	@Override
	public String getName() {
		return name;
	}
	
	@Nonnull
	@Override
	public String getType() {
		return type;
	}
	
	@Nonnull
	@Override
	public String getDescription() {
		return description;
	}
	
}
