/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Functionality for reading user input
 */
public final class Input {
	private Input() {

	}

	/**
	 * Asks an open question, i.e. a question that can be answered with any non-empty String
	 *
	 * @param question The question to ask
	 * @return The answer given by the user
	 */
	@Nonnull
	public static String openQuestion(@Nonnull String question) {
		String input = null;
		while (input == null) {
			Output.question(question);
			input = readInput();
		}

		return input;
	}

	/**
	 * Asks a multiple choice question. The user is presented the given options as a list, and can only select
	 * one of these options. If only a single option is presented, this option is selected automatically.
	 *
	 * @param question The question to ask
	 * @param options  A non-empty list of options to present to the user. List should not contain null entries
	 * @param <T>      The type of the options. Each option is rendered using the object's toString method
	 * @return The option selected by the user.
	 */
	@Nonnull
	public static <T> T multipleChoice(@Nonnull String question, @Nonnull List<T> options) {
		return multipleChoice(question, options, Object::toString);
	}

	/**
	 * Asks a multiple choice question. The user is presented the given options as a list, and can only select
	 * one of these options. If only a single option is presented, this option is selected automatically.
	 *
	 * @param question The question to ask
	 * @param options  A non-empty list of options to present to the user. List should not contain null entries
	 * @param renderer A function that converts each option to a String
	 * @param <T>      The type of the options. Each option is rendered using the given renderer function
	 * @return The option selected by the user.
	 */
	@Nonnull
	public static <T> T multipleChoice(@Nonnull String question, @Nonnull List<T> options,
									   @Nonnull Function<T, String> renderer) {
		if (options.size() == 0) {
			throw new IllegalArgumentException("At least 1 option must be provided");
		}

		if (options.size() == 1) {
			return options.get(0);
		}

		Output.question(question);
		int i = 1;
		for (T t : options) {
			Output.option(i++, options.size(), renderer.apply(t));
		}

		String input = readInput();
		try {
			Integer selected = Integer.parseInt(input);
			if (selected < 1 || selected > options.size()) {
				Output.fail("Option out of range: %d (valid: 1-%d)", selected,
						options.size());
				return multipleChoice(question, options, renderer);
			} else {
				return options.get(selected - 1);
			}

		} catch (NumberFormatException nfe) {
			Output.fail("Invalid input: %s (%s)", input, nfe.getMessage());
			return multipleChoice(question, options, renderer);
		}
	}

	@Nonnull
	private static String readInput() {
		return Optional.ofNullable(System.console()).map(Console::readLine).orElse(getInputFromSystemIn());
	}

	@Nonnull
	private static String getInputFromSystemIn() {
		final String newline = System.getProperty("line.separator");

		String read = "";

		try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
			int in;
			while ((in = System.in.read()) != -1) {
				bout.write(in);
				bout.flush();

				read = new String(bout.toByteArray());

				if (read.endsWith(newline)) {
					break;
				}
			}

		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return read.endsWith(newline) ? read.substring(0, read.length() - newline.length()) : read;
	}

	/**
	 * Asks a question that can be answered with either yes or no. Rendered as a multiple choice question with
	 * values {@code true} and {@code false}, rendered as "Yes" and "No", respectively
	 * @param question The question to ask
	 * @return {@code true} or {@code false}, depending on user input
	 */
	@Nonnull
	public static boolean yesOrNo(@Nonnull String question) {
		return multipleChoice(question, ImmutableList.of(true, false), b -> b ? "Yes" : "No");
	}
}
