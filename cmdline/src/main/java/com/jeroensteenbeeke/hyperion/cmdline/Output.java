/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline;

import javax.annotation.Nonnull;

/**
 * Utility class for outputting colored text. This class contains two types of methods: status methods and translator
 * methods. A status method outputs a given text status (formatable like String.format) with a predefined 4 character
 * prefix
 * between brackets. A translator method simply takes the given input and gives it the expected color
 */
public final class Output {
	private Output() {

	}

	/**
	 * Status method that prefixes the text with 4 cyan-colored + symbols
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void header(@Nonnull String string, @Nonnull Object... objects) {
		reportAction(Ansi.CYAN, "++++", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "info" in cyan
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void info(@Nonnull String string, @Nonnull Object... objects) {
		reportAction(Ansi.CYAN, "info", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "ok" in green, pre- and postfixed with a single space
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void ok(@Nonnull String string, @Nonnull Object... objects) {
		reportAction(Ansi.GREEN, " ok ", string, objects);
	}

	/**
	 * Status method that prefixes the text with the text "q?" in yellow, pre- and postfixed with a single space
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void question(@Nonnull String string, @Nonnull Object... objects) {
		reportAction(Ansi.YELLOW, " q? ", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "warn" in brown
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void warn(@Nonnull String string, @Nonnull Object... objects) {
		reportAction(Ansi.BROWN, "warn", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "fail" in red
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void fail(@Nonnull String string, @Nonnull Object... objects) {
		reportAction(Ansi.RED, "fail", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "done" in purple
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void done(@Nonnull String string, @Nonnull Object... objects) {
		reportAction(Ansi.PURPLE, "done", string, objects);
	}

	/**
	 * Status method that outputs the given text as an option, prefixed with an option
	 * number and a number of leading zeroes to make the number of digits match the maximum value
	 *
	 * @param index  The number to display with this option
	 * @param max    The maximum number possible for all options
	 * @param option The text belonging to this option
	 */
	public static void option(int index, int max, @Nonnull String option) {
		String format = "%0".concat(Integer.toString(Integer.toString(max).length())).concat("d");
		reportAction(Ansi.LIGHT_BLUE, String.format(format, index), option);

	}

	/**
	 * Colors the given String black
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String black(String input) {
		return color(Ansi.BLACK, input);
	}

	/**
	 * Colors the given String red
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String red(String input) {
		return color(Ansi.RED, input);
	}

	/**
	 * Colors the given String green
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String green(String input) {
		return color(Ansi.GREEN, input);
	}

	/**
	 * Colors the given String brown
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String brown(String input) {
		return color(Ansi.BROWN, input);
	}

	/**
	 * Colors the given String yellow
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String yellow(String input) {
		return color(Ansi.YELLOW, input);
	}

	/**
	 * Colors the given String blue
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String blue(String input) {
		return color(Ansi.BLUE, input);
	}

	/**
	 * Colors the given String light blue
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String lightBlue(String input) {
		return color(Ansi.LIGHT_BLUE, input);
	}

	/**
	 * Colors the given String grey
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String grey(String input) {
		return color(Ansi.GREY, input);
	}

	/**
	 * Colors the given String purple
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String purple(String input) {
		return color(Ansi.PURPLE, input);
	}

	/**
	 * Colors the given String cyan
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String cyan(String input) {
		return color(Ansi.CYAN, input);
	}

	/**
	 * Colors the given String white
	 * @param input The text to color
	 * @return The given input, wrapped with Ansi control characters
	 */
	public static String white(String input) {
		return color(Ansi.WHITE, input);
	}

	private static String color(String ansiColor, String input) {
		StringBuilder builder = new StringBuilder();
		builder.append(ansiColor);
		builder.append(input);
		builder.append(Ansi.RESET);

		return builder.toString();
	}

	private static void reportAction(String ansi, String status,
									 String message, Object... objects) {
		System.out.print("[");
		System.out.print(ansi);
		System.out.print(status);
		System.out.print(Ansi.RESET);
		System.out.print("] ");

		System.out.printf(message, objects);
		System.out.println();
	}


}
