/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline.arguments;

import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * A File argument, used to pass an existing file to a program
 */
public class FileArgument extends AbstractArgument<File> {
	/**
	 * Create a new FileArgument
	 * @param name The name of the argument
	 * @param description The description of the argument
	 */
	public FileArgument(String name, String description) {
		super(name, "file", description);
	}

	@Nonnull
	@Override
	public TypedResult<File> parse(@Nonnull String argument) {
		File file = new File(argument);

		if (!file.exists()) {
			return TypedResult.fail(
					"Invalid file %s", argument);
		}

		if (file.isDirectory()) {
			return TypedResult.fail(
					"File %s is a directory", argument);
		}

		return TypedResult.ok(file);
	}
}
