/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline.arguments;

import com.jeroensteenbeeke.lux.TypedResult;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * Argument representing a folder/directory in the filesystem
 */
public class FolderArgument extends AbstractArgument<File> {
	private final boolean mustExist;

	/**
	 * Create a new folder argument
	 * @param name The name of the argument
	 * @param description A description of the argument
	 * @param mustExist Whether or not the created directory should already exist
	 */
	public FolderArgument(String name, String description, boolean mustExist) {
		super(name, "directory", description);
		this.mustExist = mustExist;
	}

	@Nonnull
	@Override
	public TypedResult<File> parse(@Nonnull String argument) {
		File file = new File(argument);

		if (mustExist && !file.exists()) {
			return TypedResult.fail(
					"Invalid directory %s", argument);
		}

		if (file.exists() && !file.isDirectory()) {
			return TypedResult.fail(
					"File %s is not a directory", argument);
		}

		return TypedResult.ok(file);
	}
}
