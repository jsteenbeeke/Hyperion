package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.lux.ActionResult;
import org.danekja.java.util.function.serializable.SerializableConsumer;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

class JettyRunner implements Runner {
	private static final String[] WEB_XML_LOCATIONS = {"/webapp/WEB-INF",
		"/WEB-INF", "/src/main/webapp/WEB-INF", "/src/main/resources/WEB-INF", "/src/test/resource/WEB-INF"};

	private static final Logger log = LoggerFactory.getLogger(JettyRunner.class);

	private final Server server;

	private final List<SerializableConsumer<Server>> onServerStarted;

	private final List<SerializableConsumer<Server>> onServerStopped;

	JettyRunner(String contextPath, int port, List<SerializableConsumer<WebAppContext>> contextConsumers, List<SerializableConsumer<Server>> onServerStarted, List<SerializableConsumer<Server>> onServerStopped) throws MalformedURLException {
		this.onServerStarted = onServerStarted;
		this.onServerStopped = onServerStopped;

		log.info("SLF4J initialized");


		final int timeout = 1000 * 60 * 60;

		this.server = new Server();

		HttpConfiguration http_config = new HttpConfiguration();

		ServerConnector http = new ServerConnector(server,
												   new HttpConnectionFactory(http_config));
		http.setPort(port);
		http.setIdleTimeout(timeout);
		server.addConnector(http);

		WebAppContext context = new WebAppContext();
		context.setContextPath(contextPath);

		URL web_xml = null;
		URL webinf = null;
		for (String location : WEB_XML_LOCATIONS) {
			if (web_xml == null || webinf == null) {
				ContextHandler.Context servletContext = context
					.getServletContext();

				web_xml = servletContext
					.getResource(location + "/web.xml");
				if (web_xml != null) {
					log.info("web.xml found at {}", location);
					webinf = InMemory.class
						.getResource(location);
				}

			}
		}

		if (web_xml != null) {
			context.setDefaultsDescriptor(web_xml.toExternalForm());
			context.setBaseResource(Resource.newResource(webinf));
		} else {
			log.info(
				"/webapp resource is missing from jar, defaulting to src/main/webapp");
			context.setResourceBase("src/main/webapp");
		}

		context.setConfigurationClasses(new String[]{
			"org.eclipse.jetty.webapp.WebInfConfiguration",
			"org.eclipse.jetty.webapp.WebXmlConfiguration",
			"org.eclipse.jetty.plus.webapp.EnvConfiguration",
			"org.eclipse.jetty.plus.webapp.PlusConfiguration"});

		server.setHandler(context);

		contextConsumers.forEach(c -> c.accept(context));
	}

	@Override
	public ActionResult start(boolean consoleAvailable) {
		return ActionResult.attempt(() -> {
			if (consoleAvailable) {
				System.out.println("Starting embedded Jetty server");
			}

			server.start();
			onServerStarted.forEach(l -> l.accept(server));
		});
	}

	@Override
	public ActionResult stop(boolean consoleAvailable)  {
		return ActionResult.attempt(() -> {
			if (consoleAvailable) {
				System.out.println("Stopping embedded Jetty server");
			}
			server.stop();

			onServerStopped.forEach(l -> l.accept(server));

			server.join();
		});
	}
}
