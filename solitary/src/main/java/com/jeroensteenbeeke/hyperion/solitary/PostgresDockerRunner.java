package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;
import io.vavr.Tuple2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

class PostgresDockerRunner implements Runner {
	private final String username;

	private final String password;

	private final String database;

	private final int port;

	private Process databaseProcess;

	PostgresDockerRunner(String username, String password, String database, int port) {
		this.username = username;
		this.password = password;
		this.database = database;
		this.port = port;

		execute("docker info").throwIfNotOk(msg -> new IllegalStateException("Could not launch Docker: "+ msg));
	}

	@Override
	public ActionResult start(boolean consoleAvailable) {
		if (consoleAvailable) {
			System.out.println("Starting Postgres Docker Container");
		}

		execute("docker stop hyperion-inmemory");
		execute("docker rm hyperion-inmemory");

		return ActionResult.attempt(() -> {
			databaseProcess = Runtime.getRuntime().exec("docker run -p " + port + ":5432" +
															" --name hyperion-inmemory" +
															" -e " + "POSTGRES_DB=" + database +
															" -e POSTGRES_USER=" + username +
															" -e POSTGRES_PASSWORD=" + password +
															" postgres:alpine");

			Thread.sleep(5000L);

			if (consoleAvailable) {
				System.out.println("\tConnection command:");
				System.out
					.printf("\t\tpsql -h localhost -p %d -U %s -W %s", port, username, database)
					.println();
				System.out.println();
				System.out.println("\tPassword:");
				System.out.printf("\t\t%s", password).println();
				System.out.println();
			}
		});
	}

	@Override
	public ActionResult stop(boolean consoleAvailable) {
		if (consoleAvailable) {
			System.out.println("Shutting down Postgres Docker Container");
		}

		return ActionResult.attempt(() -> databaseProcess.destroyForcibly().waitFor());

	}

	@Override
	public Map<String, String> getApplicationConfiguration() {
		Map<String, String> config = new HashMap<>();

		config.put("database.username", username);
		config.put("database.password", password);
		config.put("database.driver", "org.postgresql.Driver");
		config.put("database.dialect", "org.hibernate.dialect.PostgreSQL10Dialect");
		config.put("database.sourceClass", "org.postgresql.xa.PGXADataSource");
		config.put("database.type", "postgres");
		config.put("database.url", String.format("jdbc:postgresql://localhost:%d/%s", port,
												 database));

		return config;
	}

	private static TypedResult<Integer> execute(String command) {
		return TypedResult.attempt(() -> {
			Process process = Runtime.getRuntime().exec(command);

			return new Tuple2<>(process, process.waitFor());
		}).filter(t -> t._2 == 0, t -> toError(t._1.getErrorStream())).map(Tuple2::_2);
	}

	private static String toError(InputStream stream) {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			int i;
			while ((i = stream.read()) != -1) {
				bos.write(i);
			}
			bos.flush();
			bos.close();

			return bos.toString();
		} catch (IOException e) {
			return "Failed to write error output to String: " + e.getMessage();
		}
	}
}
