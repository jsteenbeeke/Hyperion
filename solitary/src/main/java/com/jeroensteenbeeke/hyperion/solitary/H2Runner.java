package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.lux.ActionResult;
import io.vavr.control.Option;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

class H2Runner implements Runner {
	private static final Logger log = LoggerFactory.getLogger(H2Runner.class);

	private static final String BASEDIR_ARGUMENT = "-baseDir";

	private static final String DATABASE_PASSWORD_ARGUMENT = "-tcpPassword";

	private static final String DATABASE_PORT_ARGUMENT = "-tcpPort";

	private final String tcpPassword;

	private final String applicationName;

	private final Server database;

	H2Runner(int port, @Nonnull String tcpPassword, @Nonnull String applicationName, @Nonnull Option<InputStream> databasePrototype) throws IOException, SQLException {
		this.tcpPassword = tcpPassword;
		this.applicationName = applicationName;

		Path tempDirectory = Files.createTempDirectory("h2-database");

		if (databasePrototype.isDefined()) {
			InputStream prototype = databasePrototype.get();

			Path target = tempDirectory.resolve(applicationName + ".mv.db");

			Files.copy(prototype, target);
		}

		database = Server.createTcpServer
			(DATABASE_PASSWORD_ARGUMENT, tcpPassword, DATABASE_PORT_ARGUMENT,
			 Integer.toString(port), BASEDIR_ARGUMENT, tempDirectory
				 .toAbsolutePath().toString());
	}

	@Override
	public ActionResult start(boolean consoleAvailable) {
		if (consoleAvailable) {
			System.out.println("Starting in-memory H2 server");
		} else {
			log.info("Starting in-memory H2 server");
		}

		return ActionResult.attempt(database::start);
	}

	@Override
	public ActionResult stop(boolean consoleAvailable) {
		if (consoleAvailable) {
			System.out.println("Stopping in-memory H2 server");
		} else {
			log.info("Stopping in-memory H2 server");
		}

		return ActionResult.attempt(() -> Server.shutdownTcpServer(String.format("tcp://localhost:%d", database.getPort()),
																   tcpPassword,
																   true,
																   true));
	}

	@Override
	public Map<String, String> getApplicationConfiguration() {
		Map<String, String> config = new HashMap<>();

		config.put("database.username", "sa");
		config.put("database.password", "-");
		config.put("database.driver", "org.h2.Driver");
		config.put("database.dialect", "org.hibernate.dialect.H2Dialect");
		config.put("database.sourceClass", "org.h2.jdbcx.JdbcDataSource");
		config.put("database.type", "h2");
		config.put("database.url", String.format("jdbc:h2:tcp://localhost:%d/%s", database.getPort(),
												 applicationName));

		return config;
	}
}
