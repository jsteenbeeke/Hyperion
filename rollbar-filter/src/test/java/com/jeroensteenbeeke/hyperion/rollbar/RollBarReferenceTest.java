package com.jeroensteenbeeke.hyperion.rollbar;

import com.rollbar.api.payload.data.Level;
import com.rollbar.notifier.Rollbar;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class RollBarReferenceTest {
	private static Rollbar rollbar = mock(Rollbar.class);

	@BeforeClass
	public static void setup() {
		RollBarReference.instance.setRollbar(rollbar);
	}

	@Test
	public void testExceptionFilters() {
		Throwable e = new IllegalStateException();

		RollBarReference.instance.errorCaught(e);
		RollBarReference.instance.errorUncaught(e);

		RollBarReference.instance.excludeException("java.lang.IllegalStateException");

		RollBarReference.instance.errorCaught(e);
		RollBarReference.instance.errorUncaught(e);

		verify(rollbar, times(1)).log(eq(e), eq(null),
									  eq(null), eq(Level.ERROR), eq(false));
		verify(rollbar, times(1)).log(eq(e), eq(null),
									  eq(null), eq(Level.ERROR), eq(true));

		e = new IllegalArgumentException();

		RollBarReference.instance.errorCaught(e);
		RollBarReference.instance.errorUncaught(e);

		RollBarReference.instance.excludeException(IllegalArgumentException.class);

		RollBarReference.instance.errorCaught(e);
		RollBarReference.instance.errorUncaught(e);

		verify(rollbar, times(1)).log(eq(e), eq(null),
									  eq(null), eq(Level.ERROR), eq(false));
		verify(rollbar, times(1)).log(eq(e), eq(null),
									  eq(null), eq(Level.ERROR), eq(true));
	}
}
