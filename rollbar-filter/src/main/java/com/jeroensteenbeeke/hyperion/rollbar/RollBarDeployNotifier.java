package com.jeroensteenbeeke.hyperion.rollbar;

import com.jeroensteenbeeke.lux.ActionResult;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.function.Function;

/**
 * Notifier class that registers an application deploy with Rollbar
 */
public class RollBarDeployNotifier implements IRollBarDeployNotifier {
	private final String apiKey;

	private final String environment;

	private final String deployingUsername;

	private RollBarDeployNotifier(String apiKey, String environment, String deployingUsername) {
		this.apiKey = apiKey;
		this.environment = environment;
		this.deployingUsername = deployingUsername;
	}

	@Override
	public ActionResult notifyDeploy(@Nonnull String revision, @Nullable String comment) {
		OkHttpClient client = new OkHttpClient();
		MultipartBody.Builder builder = new MultipartBody.Builder()
				.addFormDataPart("access_token", apiKey)
				.addFormDataPart("environment", environment)
				.addFormDataPart("revision", revision)
				.addFormDataPart("local_username", deployingUsername);

		if (comment != null) {
			builder.addFormDataPart("comment", comment);
		}

		MultipartBody body = builder
				.setType(MultipartBody.FORM)
				.build();
		Request request = new Request.Builder().url("https://api.rollbar.com/api/1/deploy/")
											   .post(body

											   ).build();
		try (Response response = client.newCall(request).execute()) {
			if (response.isSuccessful()) {
				return ActionResult.ok();
			} else {
				return ActionResult.error("Rollbar deploy notification failed: %d %s", response.code(), response
						.body()
						.string());
			}
		} catch (IOException e) {
			return ActionResult.error(e.getMessage());
		}
	}

	/**
	 * Start creating a new notifier
	 * @return A builder
	 */
	public static WithApiKey createNotifier() {
		return apiKey -> environment -> deployingUser -> new RollBarDeployNotifier(apiKey, environment, deployingUser);
	}

	/**
	 * Builder step that registers the API key
	 */
	public interface WithApiKey extends Function<String, WithEnvironment> {
		/**
		 * Sets the API key
		 * @param apiKey The RollBar API key
		 * @return A builder
		 */
		default WithEnvironment withApiKey(@Nonnull String apiKey) {
			return apply(apiKey);
		}
	}

	/**
	 * Builder step that registers the environment
	 */
	public interface WithEnvironment extends Function<String, AndDeployingUser> {
		/**
		 * Sets the environment we're running on
		 * @param environment The environment
		 * @return A builder
		 */
		default AndDeployingUser withEnvironment(@Nonnull String environment) {
			return apply(environment);
		}
	}

	/**
	 * Builder step that registers the deploying user and creates the notifier
	 */
	public interface AndDeployingUser extends Function<String, RollBarDeployNotifier> {
		/**
		 * Sets the deploying user
		 * @param deployingUser The user triggering the deploy
		 * @return A RollBarDeployNotifier object
		 */
		default RollBarDeployNotifier andDeployingUser(@Nonnull String deployingUser) {
			return apply(deployingUser);
		}
	}
}
