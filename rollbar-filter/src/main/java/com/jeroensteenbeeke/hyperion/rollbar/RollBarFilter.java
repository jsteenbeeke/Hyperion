package com.jeroensteenbeeke.hyperion.rollbar;

import java.io.IOException;

import javax.servlet.*;

/**
 * Servlet filter class for logging exceptions to Rollbar
 */
public class RollBarFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
		} catch (Exception e) {
			RollBarReference.instance.errorUncaught(e);
			throw e;
		}
	}

	@Override
	public void destroy() {

	}

}
