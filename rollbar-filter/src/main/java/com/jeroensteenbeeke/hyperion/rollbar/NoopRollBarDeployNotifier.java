package com.jeroensteenbeeke.hyperion.rollbar;

import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Stub that does not actually communicate with Rollbar
 */
public class NoopRollBarDeployNotifier implements IRollBarDeployNotifier {
	@Override
	public ActionResult notifyDeploy(@Nonnull String revision, @Nullable String comment) {
		return ActionResult.ok();
	}
}
