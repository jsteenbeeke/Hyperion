package com.jeroensteenbeeke.hyperion.rollbar;

import com.jeroensteenbeeke.lux.ActionResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Interface for rollbar deploy notification logic
 */
public interface IRollBarDeployNotifier {
	/**
	 * Notify Rollbar that we have deployed the given revision to this server
	 *
	 * @param revision The git hash that was deployed
	 * @return An ActionResult indicating either success or the reason for failure
	 */
	default ActionResult notifyDeploy(@Nonnull String revision) {
		return notifyDeploy(revision, null);
	}

	/**
	 * Notify Rollbar that we have deployed the given revision to this server
	 *
	 * @param revision The git hash that was deployed
	 * @param comment  An additional comment to send along to Rollbar
	 * @return An ActionResult indicating either success or the reason for failure
	 */
	ActionResult notifyDeploy(@Nonnull String revision, @Nullable String comment);
	}
