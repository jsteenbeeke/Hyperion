package com.jeroensteenbeeke.hyperion.rollbar;

import com.rollbar.api.payload.data.Level;
import com.rollbar.notifier.Rollbar;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Objects;

/**
 * Holder class for storing the Rollbar reference
 */
public enum RollBarReference {
	instance;
	
	private Rollbar rollbar;

	private Set<FQDN> exceptionsNotToLog = HashSet.empty();

	/**
	 * Sets the global Rollbar instance
	 * @param rollbar The rollbar instance to use
	 */
	public void setRollbar(@Nonnull Rollbar rollbar) {
		this.rollbar = rollbar;
	}

	/**
	 * Exclude a given Exception class from being sent to Rollbar
	 * @param exclude The exception class to exclude
	 */
	public void excludeException(@Nonnull Class<? extends Throwable> exclude) {
		exceptionsNotToLog = exceptionsNotToLog.add(new FQDN(exclude));
	}

	/**
	 * Exclude an exception from being sent to Rollbar by FQDN
	 * @param fqdn The fully qualified domain name of the exception class to exclude
	 */
	public void excludeException(@Nonnull String fqdn) {
		exceptionsNotToLog = exceptionsNotToLog.add(new FQDN(fqdn));
	}

	/**
	 * Register the given exception to Rollbar
	 * @param e The exception to log
	 */
	public void errorUncaught(@Nonnull Throwable e) {
		if (rollbar != null && !exceptionsNotToLog.contains(new FQDN(e.getClass()))) {
			rollbar.log(e, null, null, Level.ERROR, true);
		}
	}

	/**
	 * Register the given exception to Rollbar
	 * @param e The exception to log
	 */
	public void errorCaught(@Nonnull Throwable e) {
		errorCaught(e, null);
	}

	/**
	 * Register the given exception to Rollbar
	 * @param e The exception to log
	 * @param customData Any custom data to pass along to Rollbar
	 */
	public void errorCaught(@Nonnull Throwable e, @Nullable Map<String,Object> customData) {
		if (rollbar != null && !exceptionsNotToLog.contains(new FQDN(e.getClass()))) {
			rollbar.log(e, customData, null, Level.ERROR, false);
		}
	}

	private static class FQDN {
		private final String fqdn;

		private FQDN(@Nonnull String fqdn) {
			this.fqdn = fqdn;
		}

		private FQDN(@Nonnull Class<?> input) {
			this.fqdn = input.getName();
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			FQDN fqdn1 = (FQDN) o;
			return fqdn.equals(fqdn1.fqdn);
		}

		@Override
		public int hashCode() {
			return Objects.hash(fqdn);
		}
	}
}
