package com.jeroensteenbeeke.hyperion.webcomponents.passwordpolicy;

import com.jeroensteenbeeke.hyperion.passwordpolicy.PasswordPolicy;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

/**
 * Wicket Validator for enforcing a developer-defined password policy, using the
 * various checks built into hyperion-password-policy.
 */
public class PasswordPolicyValidator implements IValidator<String> {
	private final boolean checkCommonPasswords;

	private final boolean checkPasswordTopology;

	private final Integer minimumLength;

	private PasswordPolicyValidator(Builder builder) {
		this.checkCommonPasswords = builder.checkCommonPasswords;
		this.checkPasswordTopology = builder.checkPasswordTopology;
		this.minimumLength = builder.minimumLength;
	}

	@Override
	public void validate(IValidatable<String> validatable) {
		String password = validatable.getValue();

		if (minimumLength != null && password.length() < minimumLength) {
			validatable.error(new ValidationError(
					String.format("Password should be at least %d characters", minimumLength)));
		}

		if (checkCommonPasswords && PasswordPolicy.isCommonPassword(password)) {
			validatable.error(new ValidationError("Your password is common and therefore easy to hack, pick another password"));
		}

		if (checkPasswordTopology && PasswordPolicy.isCommonTopology(password)) {
			validatable.error(new ValidationError("Your password has a common pattern, and is relatively easy to hack, pick another password"));
		}

	}

	/**
	 * Create a builder for defining the password policy to check
	 * @return A builder
	 */
	public static Builder passwords() {
		return new Builder();
	}

	/**
	 * Builder class for defining password policies
	 */
	public static class Builder {
		private static final int OWASP_RECOMMENDED_MINIMUM = 10;

		private boolean checkCommonPasswords = false;

		private boolean checkPasswordTopology = false;

		private Integer minimumLength = null;

		/**
		 * Set up the validator to check against the top common passwords. This is a relatively slow
		 * check, as the list of passwords is not fully loaded into memory but streamed upon each call
		 * @return This builder
		 */
		public Builder shouldNotBeCommon() {
			this.checkCommonPasswords = true;
			return this;
		}

		/**
		 * Set up the validator to check against common password topologies. This is a relatively slow
		 * check (though 2 orders of magnitude faster than the common password check), as the list of topologies is not
		 * fully loaded into memory but streamed upon each call
		 * @return This builder
		 */
		public Builder shouldNotHaveACommonTopology() {
			this.checkPasswordTopology = true;
			return this;
		}

		/**
		 * Sets up the validator to enforce a minimum length. Checks the given length against OWASP best practices.
		 *
		 * PLEASE KEEP IN MIND THAT A MAXIMUM LENGTH FOR PASSWORDS IS CONSIDERED A SECURITY WEAKNESS
		 *
		 * @param minimumLength The minimum length to enforce
		 * @return This builder
		 */
		public Builder shouldHaveAMinimumLengthOf(int minimumLength) {
			if (minimumLength < OWASP_RECOMMENDED_MINIMUM) {
				throw new IllegalArgumentException("Passwords shorter than "+ OWASP_RECOMMENDED_MINIMUM
						+ " are considered weak by definition: https://www.owasp.org/index.php/Authentication_Cheat_Sheet#Password_Length");
			}

			this.minimumLength = minimumLength;
			return this;
		}

		/**
		 * Creates the validator with the defined rules
		 * @return The password validator
		 */
		public PasswordPolicyValidator andBeValidated() {
			if (!checkPasswordTopology && !checkCommonPasswords && minimumLength == null) {
				throw new IllegalStateException("No rules defined");
			}

			return new PasswordPolicyValidator(this);
		}
	}
}
