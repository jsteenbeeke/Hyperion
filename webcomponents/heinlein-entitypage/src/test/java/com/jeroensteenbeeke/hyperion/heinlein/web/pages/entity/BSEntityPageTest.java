package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.google.common.collect.ImmutableList;
import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;
import com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice.LambdaRenderer;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.DefaultFieldType;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.EntityFormField;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Maximum;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Minimum;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BSEntityPageTest {
	@Test
	public void testSettings() {
			FooDAO fooDAO = new FooDAO();
			Foo entity = new Foo();
			BSEntityPageSettings.<Foo> titled("Add")
					.withCancelText("Cease")
					.withCancelCss("cancel")
					.withDeleteText(
							"Exterminate")
					.withDeleteCss("delete")
					.withSubmitText("Engage")
					.withSubmitCss("submit")
					.withNavigator(id -> null)
					.readOnly()
					.build(entity, fooDAO);
	}

	@Test
	public void testPageFlow() {
		WicketTester tester = new WicketTester();

		EntityEncapsulator.setFactory(new TestEntityEncapsulator());

		FooDAO fooDAO = new FooDAO();
		Foo entity = new Foo();
		BSEntityPageSettings<Foo> settings = BSEntityPageSettings.<Foo> titled("Add")
																	   .allowDelete()
																	   .withCancelText("Cease")
																	   .withDeleteText(
																			   "Exterminate")
																	   .withSubmitText("Engage")
																	   .build(entity, fooDAO);

		TestEntityPage<Foo> entityPage;
		tester.startPage(entityPage = new TestEntityPage<>(settings));
		tester.assertRenderedPage(BSEntityFormPage.class);

		tester.assertLabel("pagetitle", "Add");
		tester.assertComponent("back", Link.class);
		Link<?> link = (Link<?>) tester.getComponentFromLastRenderedPage("back");
		assertEquals("Cease", link.getBody().getObject());

		entityPage.setTitle("Title");

		tester.assertLabel("title", "Title");
		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);
		tester.assertComponent("entityForm", Form.class);
		tester.assertComponent("entityForm:fields", ListView.class);
		tester.assertComponent("entityForm:fields:0:componentPanel:text", TextField.class);

		tester.clickLink("submit");

		entityPage.assertSaved(entity);
		fooDAO.assertSaved(entity);

		fooDAO = new FooDAO();

		entity.setId(1L);

		settings = BSEntityPageSettings.<Foo> titled("Edit")
				.allowDelete()
				.withCancelText("Cease")
				.withDeleteText(
						"Exterminate")
				.withSubmitText("Engage")
				.build(entity, fooDAO);

		tester.startPage(entityPage = new TestEntityPage<>(settings));
		tester.assertRenderedPage(BSEntityFormPage.class);

		tester.assertLabel("pagetitle", "Edit");
		tester.assertComponent("back", Link.class);
		link = (Link<?>) tester.getComponentFromLastRenderedPage("back");
		assertEquals("Cease", link.getBody().getObject());

		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);
		tester.assertComponent("entityForm", Form.class);
		tester.assertComponent("entityForm:fields", ListView.class);

		tester.clickLink("submit");

		entityPage.assertSaved(entity);
		fooDAO.assertUpdated(entity);

		fooDAO = new FooDAO();

		settings = BSEntityPageSettings.<Foo> titled("Edit")
				.allowDelete()
				.withCancelText("Cease")
				.withDeleteText(
						"Exterminate")
				.withSubmitText("Engage")
				.build(entity, fooDAO);

		tester.startPage(entityPage = new TestEntityPage<>(settings));
		tester.assertRenderedPage(BSEntityFormPage.class);

		tester.assertLabel("pagetitle", "Edit");
		tester.assertComponent("back", Link.class);
		link = (Link<?>) tester.getComponentFromLastRenderedPage("back");
		assertEquals("Cease", link.getBody().getObject());

		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);

		tester.clickLink("back");

		entityPage.assertCancelled(entity);

		fooDAO = new FooDAO();

		settings = BSEntityPageSettings.<Foo> titled("Edit")
				.allowDelete()
				.withCancelText("Cease")
				.withDeleteText(
						"Exterminate")
				.withSubmitText("Engage")
				.build(entity, fooDAO);

		tester.startPage(entityPage = new TestEntityPage<>(settings));
		tester.assertRenderedPage(BSEntityFormPage.class);

		tester.assertLabel("pagetitle", "Edit");
		tester.assertComponent("back", Link.class);
		link = (Link<?>) tester.getComponentFromLastRenderedPage("back");
		assertEquals("Cease", link.getBody().getObject());

		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);

		tester.clickLink("delete");

		entityPage.assertDeleted(entity);
		fooDAO.assertDeleted(entity);

		Foo bar = new Foo();
		settings = BSEntityPageSettings.<Foo> titled("Add")
				.allowDelete()
				.withCancelText("Cease")
				.withDeleteText(
						"Exterminate")
				.withSubmitText("Engage")
				.build(bar, fooDAO);

		tester.startPage(entityPage = new TestEntityPage<>(settings));
		entityPage.disableDeletion("Unit test");

		tester.assertRenderedPage(BSEntityFormPage.class);

		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);

		tester.clickLink("delete");

		entityPage.assertNotDeleted(bar);
		tester.assertErrorMessages("Unit test");
	}

	@Test
	public void testInitialValues() {
		WicketTester tester = new WicketTester();

		EntityEncapsulator.setFactory(new TestEntityEncapsulator());

		FooDAO fooDAO = new FooDAO();
		Foo entity = new Foo();
		BSEntityPageSettings<Foo> settings = BSEntityPageSettings.<Foo> titled("Add")
				.allowDelete()
				.withCancelText("Cease")
				.withDeleteText(
						"Exterminate")
				.withSubmitText("Engage")
				.build(entity, fooDAO);

		TestEntityPage<Foo> entityPage = new TestEntityPage<>(settings);
		entityPage.addInitialValue(Foo_.name, "Test");

		tester.startPage(entityPage);
		tester.assertRenderedPage(TestEntityPage.class);

		tester.assertComponent("entityForm:fields:0:componentPanel:text", TextField.class);
		TextField tf = (TextField) tester.getComponentFromLastRenderedPage
				("entityForm:fields:0:componentPanel:text");
		assertEquals("Test", tf.getModelObject());

	}

	@Test

	public void testDropDown() {
		WicketTester tester = new WicketTester();

		EntityEncapsulator.setFactory(new TestEntityEncapsulator());

		BarDAO barDAO = new BarDAO();
		Bar entity = new Bar();

		BSEntityPageSettings<Bar> settings = BSEntityPageSettings.<Bar> titled("Create Bar")
				.allowDelete()
				.withCancelText("Barred")
				.withDeleteText("Bar Fight")
				.withSubmitText("Drinks on the house")
				.build(entity, barDAO);

		TestEntityPage<Bar> entityPage = new TestEntityPage<>(settings);
		Foo foo1 = new Foo();
		foo1.setName("First");

		Foo foo2 = new Foo();
		foo2.setName("Second");

		entityPage.setChoices(Bar_.foo, ImmutableList.of(foo1, foo2));

		tester.startPage(entityPage);
		tester.assertRenderedPage(TestEntityPage.class);

		tester.assertLabel("title", "Create Bar");
		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);
		tester.assertComponent("entityForm", Form.class);
		tester.assertComponent("entityForm:fields", ListView.class);
		tester.assertComponent("entityForm:fields:0:componentPanel:text", TextField.class);
		tester.assertComponent("entityForm:fields:1:componentPanel:dropdown", DropDownChoice
				.class);

		@SuppressWarnings("unchecked")
		DropDownChoice<Foo> fooChoice = (DropDownChoice<Foo>) tester.getComponentFromLastRenderedPage
				("entityForm:fields:1:componentPanel:dropdown");
		assertEquals(2, fooChoice.getChoices().size());
		assertNull(fooChoice.getModelObject());

		entity = new Bar();
		settings = BSEntityPageSettings.<Bar> titled("Create Bar")
				.allowDelete()
				.withCancelText("Barred")
				.withDeleteText("Bar Fight")
				.withSubmitText("Drinks on the house")
				.build(entity, barDAO);

		entityPage = new TestEntityPage<>(settings);

		entityPage.setChoices(Bar_.foo, ImmutableList.of(foo1, foo2));
		entityPage.addInitialValue(Bar_.foo, foo1);

		tester.startPage(entityPage);
		tester.assertRenderedPage(TestEntityPage.class);

		tester.assertLabel("title", "Create Bar");
		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);
		tester.assertComponent("entityForm", Form.class);
		tester.assertComponent("entityForm:fields", ListView.class);
		tester.assertComponent("entityForm:fields:0:componentPanel:text", TextField.class);
		tester.assertComponent("entityForm:fields:1:componentPanel:dropdown", DropDownChoice
				.class);

		@SuppressWarnings("unchecked")
		DropDownChoice<Foo> fooChoice2 = (DropDownChoice<Foo>) tester
				.getComponentFromLastRenderedPage
				("entityForm:fields:1:componentPanel:dropdown");
		assertEquals(2, fooChoice2.getChoices().size());
		assertEquals(foo1, fooChoice2.getModelObject());

		entity = new Bar();
		entity.setFoo(foo1);
		entity.setName("NAME");

		settings = BSEntityPageSettings.<Bar> titled("Create Bar")
				.allowDelete()
				.withCancelText("Barred")
				.withDeleteText("Bar Fight")
				.withSubmitText("Drinks on the house")
				.build(entity, barDAO);

		entityPage = new TestEntityPage<>(settings);
		entityPage.setRenderer(Bar_.foo, LambdaRenderer.of(Foo::getName));
		entityPage.setChoices(Bar_.foo, ImmutableList.of(foo1, foo2));


		tester.startPage(entityPage);
		tester.assertRenderedPage(TestEntityPage.class);

		tester.assertLabel("title", "Create Bar");
		tester.assertComponent("delete", Link.class);
		tester.assertComponent("submit", SubmitLink.class);
		tester.assertComponent("entityForm", Form.class);
		tester.assertComponent("entityForm:fields", ListView.class);

		tester.assertComponent("entityForm:fields:0:componentPanel:text", TextField.class);

		tester.assertComponent("entityForm:fields:1:componentPanel:dropdown", DropDownChoice
				.class);
		
		@SuppressWarnings("unchecked")
		DropDownChoice<Foo> fooChoice3 = (DropDownChoice<Foo>) tester
				.getComponentFromLastRenderedPage
						("entityForm:fields:1:componentPanel:dropdown");
		assertEquals(2, fooChoice3.getChoices().size());
		assertEquals(foo1, fooChoice3.getModelObject());
	}



	public static class FooDAO extends TestDAO<Foo,FooFilter> {

	}

	public static class BarDAO extends TestDAO<Bar,BarFilter> {

	}

	public static class FooFilter extends SearchFilter<Foo,FooFilter> {

	}

	public static class BarFilter extends SearchFilter<Bar,BarFilter> {

	}

	@Entity
	public static class Bar extends BaseDomainObject {
		@Id
		private Long id;

		@Column
		@EntityFormField(label = "Name")
		private String name;

		@ManyToOne
		@EntityFormField(label = "Parent", type = DefaultFieldType.DropDownChoice.class)
		private Foo foo;

		@EntityFormField(label = "A", type = DefaultFieldType.Number.class)
		@Minimum(100)
		private Integer a;

		@EntityFormField(label = "B", type = DefaultFieldType.Number.class)
		@Maximum(1000)
		private Integer b;

		@EntityFormField(label = "C", type = DefaultFieldType.Number.class)
		@Minimum(100)
		@Maximum(1000)
		private Integer c;


		public Long getId() {
			return id;
		}

		public Bar setId(Long id) {
			this.id = id;
			return this;
		}

		public String getName() {
			return name;
		}

		public Bar setName(String name) {
			this.name = name;
			return this;
		}

		public Foo getFoo() {
			return foo;
		}

		public Bar setFoo(Foo foo) {
			this.foo = foo;
			return this;
		}

		public Integer getA() {
			return a;
		}

		public Bar setA(Integer a) {
			this.a = a;
			return this;
		}

		public Integer getB() {
			return b;
		}

		public Bar setB(Integer b) {
			this.b = b;
			return this;
		}

		public Integer getC() {
			return c;
		}

		public Bar setC(Integer c) {
			this.c = c;
			return this;
		}

		@Override
		public Serializable getDomainObjectId() {
			return getId();
		}
	}

	@Entity
	public static class Foo extends BaseDomainObject {
		@Id
		private Long id;

		@Column
		@EntityFormField(label = "Name")
		private String name;

		public Long getId() {
			return id;
		}

		public Foo setId(Long id) {
			this.id = id;
			return this;
		}

		public String getName() {
			return name;
		}

		public Foo setName(String name) {
			this.name = name;
			return this;
		}

		@Override
		public Serializable getDomainObjectId() {
			return getId();
		}
	}

	public static class Foo_ {
		public static volatile SingularAttribute<Foo,String> name = singular("name");
	}

	public static class Bar_ {
		public static volatile SingularAttribute<Bar,String> name = singular("name");
		public static volatile SingularAttribute<Bar,Foo> foo = singular("foo");

	}

	@SuppressWarnings("unchecked")
	private static <T extends DomainObject, V extends Serializable> SingularAttribute<T,V>
	singular(String attributeName) {
		SingularAttribute<T,V> attribute = (SingularAttribute<T,V>) Mockito.mock(SingularAttribute.class);
		Mockito.when(attribute.getName()).thenReturn(attributeName);
		return attribute;
	}
}
