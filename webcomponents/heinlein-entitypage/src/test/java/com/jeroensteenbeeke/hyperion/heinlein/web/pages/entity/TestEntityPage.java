package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.lux.ActionResult;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestEntityPage<T extends BaseDomainObject> extends BSEntityFormPage<T> {
	private final Set<T> saved = new HashSet<>();

	private final Set<T> cancelled = new HashSet<>();

	private final Set<T> deleted = new HashSet<>();

	private String deleteRefusalReason = null;

	public TestEntityPage(BSEntityPageSettings<T> settings) {
		super(settings);
	}

	public void disableDeletion(String reason) {
		this.deleteRefusalReason = reason;
	}

	@Override
	protected void onSaved(T entity) {
		saved.add(entity);
	}

	@Override
	protected void onCancel(T entity) {
		cancelled.add(entity);
	}

	@Override
	protected ActionResult onBeforeDelete(T entity) {
		if (deleteRefusalReason != null) {
			return ActionResult.error(deleteRefusalReason);
		}

		deleted.add(entity);

		return ActionResult.ok();
	}

	public void assertSaved(T object) {
		assertTrue(
				String.format("%s %s was saved", object.getClass(), object.getDomainObjectId()),
				saved.contains(object));
	}

	public void assertNotSaved(T object) {
		assertFalse(
				String.format("%s %s was not saved", object.getClass(), object.getDomainObjectId()),
				saved.contains(object));
	}

	public void assertCancelled(T object) {
		assertTrue(
				String.format("%s %s was cancelled", object.getClass(), object.getDomainObjectId()),
				cancelled.contains(object));
	}

	public void assertNotCancelled(T object) {
		assertFalse(
				String.format("%s %s was not cancelled", object.getClass(), object.getDomainObjectId()),
				cancelled.contains(object));
	}

	public void assertDeleted(T object) {
		assertTrue(
				String.format("%s %s was deleted", object.getClass(), object.getDomainObjectId()),
				deleted.contains(object));
	}

	public void assertNotDeleted(T object) {
		assertFalse(
				String.format("%s %s was not deleted", object.getClass(), object.getDomainObjectId()),
				deleted.contains(object));
	}
}
