package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;

import javax.annotation.Nonnull;

/**
 * Drop-in support for entity forms. Implement this interface in an abstract base page and it will give
 * you a simplified version of the normal BSEntityPageSettings builder:
 *
 * {@code edit(new MyEntity()).onPage("Edit Entity").using(myEntityDAO)}
 * {@code create(new MyEntity()).onPage("Create Entity").using(myEntityDAO)}
 */
public interface EntityFormSupport {
	/**
	 * First step in creating an edit page
	 * @param entity The entity to edit
	 * @param <T> The type of the entity to edit
	 * @return An instance of EditContextCreator1
	 */
	@Nonnull
	default <T extends BaseDomainObject> EditContextCreator1<T> edit(@Nonnull T entity) {
		return new EditContextCreator1<>(entity);
	}

	/**
	 * First step in creating a create page
	 * @param entity The entity to create
	 * @param <T> The type of the entity to create
	 * @return An instance of AddContextCreator1
	 */
	@Nonnull
	default <T extends BaseDomainObject> AddContextCreator1<T> create(@Nonnull
			T entity) {
		return new AddContextCreator1<>(entity);
	}

	/**
	 * First builder step, allows you to specify a page title
	 * @param <T> The type of entity to edit.
	 */
	class EditContextCreator1<T extends BaseDomainObject> {
		private final T entity;

		private EditContextCreator1(@Nonnull T entity) {
			this.entity = entity;
		}

		/**
		 * Specifies the page title
		 * @param title The title  of the edit page
		 * @return An instance of EditContextCreator2
		 */
		@Nonnull
		public EditContextCreator2<T> onPage(@Nonnull String title) {
			return new EditContextCreator2<T>(entity, title);
		}
	}

	/**
	 * Second builder step, allows you to tweak page settings and finalize the settings
	 * @param <T> The type of entity to edit.
	 */
	class EditContextCreator2<T extends BaseDomainObject> {
		private final T entity;

		private final String title;

		private boolean allowDelete = true;

		private boolean readOnly = false;

		private EditContextCreator2(@Nonnull T entity, @Nonnull String title) {
			this.entity = entity;
			this.title = title;
		}

		/**
		 * Disable the delete function on the edit page
		 * @return The current builder
		 */
		public EditContextCreator2<T> withoutDelete() {
			this.allowDelete = false;
			return this;
		}


		/**
		 * Marks the edit page as being read-only, disabling all edit capabilities
		 * @return The current builder
		 */
		public EditContextCreator2<T> readOnly() {
			this.readOnly = true;
			return this;
		}

		/**
		 * Finalizes the settings by specifying the DAO to use for saving the entity
		 * @param dao The DAO for the given entity
		 * @return A BSEntityPageSettings instance
		 */
		public BSEntityPageSettings<T> using(DAO<T,?> dao) {
			BSEntityPageSettings.Builder<T> builder = new BSEntityPageSettings.Builder<T>(
					title).withCancelCss("btn btn-info").withCancelText("Back")
					.withDeleteCss("btn btn-warning")
					.withDeleteText("Delete")
					.withSubmitCss("btn btn-success")
					.withSubmitText("Save");

			if (this.allowDelete) {
				builder.allowDelete();
			}
			if (this.readOnly) {
				builder.readOnly();
			}

			return builder.build(entity, dao);

		}

	}

	/**
	 * First builder step, allows you to specify a page title
	 * @param <T> The type of entity to add.
	 */
	class AddContextCreator1<T extends BaseDomainObject> {
		private final T entity;

		private AddContextCreator1(T entity) {
			this.entity = entity;
		}

		/**
		 * Specifies the page title
		 * @param title The title  of the create page
		 * @return An instance of AddContextCreator2
		 */
		public AddContextCreator2<T> onPage(String title) {
			return new AddContextCreator2<>(entity, title);
		}
	}

	/**
	 * Second builder step, allows you to specify the DAO
	 * @param <T> The type of entity to add.
	 */
	class AddContextCreator2<T extends BaseDomainObject> {
		private final T entity;

		private final String title;

		private AddContextCreator2(T entity, String title) {
			this.entity = entity;
			this.title = title;
		}

		/**
		 * Finalizes the settings by specifying the DAO to use for saving the entity
		 * @param dao The DAO for the given entity
		 * @return A BSEntityPageSettings instance
		 */
		public BSEntityPageSettings<T> using(DAO<T,?> dao) {
			BSEntityPageSettings.Builder<T> builder = new BSEntityPageSettings.Builder<T>(
					title).withCancelCss("btn btn-info").withCancelText("Back")
							.withDeleteCss("btn btn-warning")
							.withDeleteText("Delete")
							.withSubmitCss("btn btn-success")
							.withSubmitText("Save");

			return builder.build(entity, dao);

		}
	}
}
