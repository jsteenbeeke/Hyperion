/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.EmptyNavComponentProvider;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.INavComponentProvider;
import org.apache.wicket.Component;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;

import javax.annotation.Nonnull;

/**
 * Settings class for BSEntityFormPage, contains all the settings needed to construct an entity page
 * @param <T> The type of entity being edited
 */
public class BSEntityPageSettings<T extends DomainObject> {
	/**
	 * Builder class for creating settings
	 * @param <T> The type of entity being edited
	 */
	public static class Builder<T extends DomainObject> {
		private final String title;

		private String submitCssClasses = "btn btn-default btn-block";

		private String cancelCssClasses = "btn btn-default btn-block";

		private String deleteCssClasses = "btn btn-default btn-block";

		private String cancelText = "Back";

		private String deleteText = "Delete";

		private String submitText = "Submit";

		private INavComponentProvider navProvider = new EmptyNavComponentProvider();

		private boolean allowDelete = false;

		private boolean readOnly = false;

		Builder(String title) {
			this.title = title;
		}

		/**
		 * Add a navigation provider to the builder
		 * @param provider A class that yields a navigator component
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> withNavigator(@Nonnull INavComponentProvider provider) {
			this.navProvider = provider;
			return this;
		}

		/**
		 * Replaces the css for the submit button with the given classes
		 * @param classes The replacement classes
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> withSubmitCss(@Nonnull String classes) {
			this.submitCssClasses = classes;
			return this;
		}

		/**
		 * Replaces the css for the cancel button with the given classes
		 * @param classes The replacement classes
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> withCancelCss(@Nonnull String classes) {
			this.cancelCssClasses = classes;
			return this;
		}

		/**
		 * Replaces the css for the delete button with the given classes
		 * @param classes The replacement classes
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> withDeleteCss(@Nonnull String classes) {
			this.deleteCssClasses = classes;
			return this;
		}

		/**
		 * Replaces the text on the cancel button with the given text
		 * @param text The replacement text
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> withCancelText(@Nonnull String text) {
			this.cancelText = text;
			return this;
		}

		/**
		 * Replaces the text on the delete button with the given text
		 * @param text The replacement text
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> withDeleteText(@Nonnull String text) {
			this.deleteText = text;
			return this;
		}

		/**
		 * Replaces the text on the submit button with the given text
		 * @param text The replacement text
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> withSubmitText(@Nonnull String text) {
			this.submitText = text;
			return this;
		}

		/**
		 * Enables the option to delete the entity being edited
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> allowDelete() {
			this.allowDelete = true;
			return this;
		}

		/**
		 * Sets the page to readOnly mode
		 * @return The current builder
		 */
		@Nonnull
		public Builder<T> readOnly() {
			this.readOnly = true;
			return this;
		}

		/**
		 * Create the settings object, passing the entity that should be edited, and the DAO that should be
		 * used to save it
		 * @param entity The entity to edit
		 * @param dao The DAO to persist the entity with
		 * @return An instance of BSEntityPageSettings
		 */
		@Nonnull
		public BSEntityPageSettings<T> build(@Nonnull T entity, @Nonnull DAO<? super T,?> dao) {
			BSEntityPageSettings<T> settings = new BSEntityPageSettings<>();
			settings.title = title;
			settings.navigationProvider = navProvider;
			settings.submitCssClasses = submitCssClasses;
			settings.cancelCssClasses = cancelCssClasses;
			settings.cancelText = cancelText;
			settings.submitText = submitText;
			settings.deleteText = deleteText;
			settings.deleteCssClasses = deleteCssClasses;
			settings.allowDelete = allowDelete;
			settings.readOnly = readOnly;
			settings.dao = dao;
			settings.entity = entity;

			return settings;
		}


	}

	/**
	 * Create a new builder for generating entity page settings
	 * @param title The title of the page to generate
	 * @param <T> The type of entity edited on the page
	 * @return A Builder
	 */
	public static <T extends DomainObject> Builder<T> titled(String title) {
		return new Builder<>(title);
	}

	private String title;

	private boolean allowDelete;

	private boolean readOnly;

	private INavComponentProvider navigationProvider;

	private String submitCssClasses;

	private String cancelCssClasses;

	private String deleteCssClasses;

	private String submitText;

	private String cancelText;

	private String deleteText;

	private DAO<? super T,?> dao;

	private T entity;

	private BSEntityPageSettings() {
		super();
	}

	boolean isAllowDelete() {
		return allowDelete;
	}

	String getDeleteCssClasses() {
		return deleteCssClasses;
	}

	String getDeleteText() {
		return deleteText;
	}

	DAO<? super T,?> getDAO() {
		return dao;
	}

	/**
	 * Returns the title of the entity page
	 * @return A String containing the page's title
	 */
	public String getTitle() {
		return title;
	}

	boolean isReadOnly() {
		return readOnly;
	}

	T getEntity() {
		return entity;
	}

	Component createNavigation(String id) {
		return navigationProvider.createNavComponent(id);
	}

	String getSubmitCssClasses() {
		return submitCssClasses;
	}

	String getCancelCssClasses() {
		return cancelCssClasses;
	}

	String getSubmitText() {
		return submitText;
	}

	String getCancelText() {
		return cancelText;
	}
}
