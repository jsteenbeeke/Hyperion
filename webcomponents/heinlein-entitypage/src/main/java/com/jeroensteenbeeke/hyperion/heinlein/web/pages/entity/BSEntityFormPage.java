/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.google.common.collect.ImmutableList;
import com.jeroensteenbeeke.hyperion.annotation.Annotations;
import com.jeroensteenbeeke.hyperion.data.DomainObject;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.BootstrapFeedbackPanel;
import com.jeroensteenbeeke.hyperion.heinlein.web.pages.BootstrapBasePage;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.*;
import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.hyperion.webcomponents.core.Components;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.*;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.metamodel.Attribute;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Page that takes an annotated entity and displays a fully-fledged edit page for it
 *
 * @param <T> The type of entity to edit
 */
public abstract class BSEntityFormPage<T extends DomainObject> extends
	BootstrapBasePage implements IFieldMetadataProvider, IComponentPostProcessor {
	private static final long serialVersionUID = 1L;
	public static final String COMPONENT_PANEL_ID = "componentPanel";

	private Set<String> readOnly;

	private Map<String, IModel<? extends List<?>>> providers;

	private Map<String, IChoiceRenderer<?>> renderers;

	private Form<T> entityForm;

	private ListView<FieldReference> fields;

	private Map<String, IModel<?>> initialValues;

	private DAO<? super T, ?> dao;

	/**
	 * Create a new entity edit page with the given settings
	 *
	 * @param settings The settings for the edit page
	 */
	public BSEntityFormPage(BSEntityPageSettings<T> settings) {
		super(settings.getTitle());
		this.providers = new HashMap<>();
		this.initialValues = new HashMap<>();
		this.renderers = new HashMap<>();
		this.readOnly = new HashSet<>();

		this.dao = settings.getDAO();

		add(new Label("pagetitle", settings.getTitle()));

		add(settings.createNavigation("nav"));

		add(createFeedbackPanel("feedback"));

		Link<T> cancelLink = new Link<T>("back") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				onCancel(getEntity());
			}
		};
		cancelLink.setBody(Model.of(settings.getCancelText()));
		cancelLink.add(AttributeModifier.replace("class",
												 settings.getCancelCssClasses()));

		add(cancelLink);

		entityForm = new Form<T>("entityForm") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				T entity = getEntity();
				List<ListItem<FieldReference>> items = Components.extract(fields);
				for (ListItem<FieldReference> item : items) {
					FieldReference reference = item.getModelObject();

					FormComponentPanel<?, ?> component =
						(FormComponentPanel<?, ?>) item.get(COMPONENT_PANEL_ID);

					reference.setValue(entity, component.getFormComponent().getModelObject());
				}

				ActionResult result = validateEntity(entity);

				if (result.isOk()) {
					onBeforeSave(entity);
					if (entity.isSaved()) {
						result = updateEntity(entity);
					} else {
						result = saveEntity(entity);
					}

					if (result.isOk()) {
						dao.flush();
						onSaved(entity);
					} else {
						error(result.getMessage());
					}
				} else {
					error(result.getMessage());
				}
			}
		};

		entityForm
			.setModel(EntityEncapsulator.createModel(settings.getEntity()));

		add(entityForm);

		Link<T> deleteLink = new Link<T>("delete") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				T entity = getEntity();
				ActionResult result = onBeforeDelete(entity);
				if (result.isOk()) {
					dao.delete(entity);
					onDeleted();
				} else {
					error(result.getMessage());
				}
			}
		};
		deleteLink.setBody(Model.of(settings.getDeleteText()));
		deleteLink.add(AttributeModifier.replace("class",
												 settings.getDeleteCssClasses()));
		deleteLink.setEnabled(settings.isAllowDelete()
								  && !settings.isReadOnly());
		deleteLink.setVisible(settings.isAllowDelete()
								  && !settings.isReadOnly());
		add(deleteLink);

		SubmitLink submit = new SubmitLink("submit", entityForm);
		submit.setBody(Model.of(settings.getSubmitText()));
		submit.add(AttributeModifier.replace("class",
											 settings.getSubmitCssClasses()));
		submit.setEnabled(!settings.isReadOnly());
		submit.setVisible(!settings.isReadOnly());
		add(submit);

		entityForm.setDefaultButton(submit);

		entityFormInit(settings.isReadOnly());
	}

	private ActionResult saveEntity(T entity) {
		dao.save(entity);
		return ActionResult.ok();
	}

	private ActionResult updateEntity(T entity) {
		dao.update(entity);
		return ActionResult.ok();
	}

	/**
	 * Creates a FeedbackPanel for use on the page
	 *
	 * @param id The id of the panel to create
	 * @return An instance of FeedbackPanel
	 */
	protected FeedbackPanel createFeedbackPanel(String id) {
		return new BootstrapFeedbackPanel(id);
	}

	private T getEntity() {
		return entityForm.getModelObject();
	}

	/**
	 * Validates the given entity, allowing an implementing page to add extra integrity check
	 *
	 * @param entity The entity being edited
	 * @return An ActionResult either specifying success or the reason for failure
	 */
	@Nonnull
	protected ActionResult validateEntity(@Nonnull T entity) {
		return ActionResult.ok();
	}

	/**
	 * Callback method called just before an entity is saved, allowing extra properties to be set that do
	 * not depend on user input
	 *
	 * @param entity The entity being edited
	 */
	protected void onBeforeSave(T entity) {

	}

	/**
	 * Method called after the entity under edit has been saved or updated
	 *
	 * @param entity The entity being edited
	 */
	protected abstract void onSaved(T entity);

	/**
	 * Method called after the user presses cancel
	 *
	 * @param entity The entity being edited
	 */
	protected abstract void onCancel(T entity);

	/**
	 * Method called prior to deleting an entity
	 *
	 * @param entity The entity about to be deleted
	 * @return An ActionResult indicating either that the delete can proceed, or the reason why it can't
	 */
	protected ActionResult onBeforeDelete(T entity) {
		return ActionResult.ok();
	}

	/**
	 * Method called after the entity under edit has been deleted
	 */
	protected void onDeleted() {

	}

	private void entityFormInit(final boolean readOnly) {
		fields = new ListView<FieldReference>("fields", scan(getEntity())) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<FieldReference> item) {
				FieldReference ref = item.getModelObject();
				Annotations annotations = ref.getAnnotations();
				IModel<?> initialValue = getInitialValue(ref);

				Class<? extends IFieldType> type = ref.getFieldType();

				FormComponentPanel<?, ?> component = FieldTypeRepository.instance.get(type).createComponent(
					COMPONENT_PANEL_ID,
					ref,
					BSEntityFormPage.this,
					initialValue
				);

				component.setLabel(ref.isLocalized() ? new StringResourceModel(ref.getLabel()) : Model.of(ref.getLabel()));

				postProcessComponent(component, annotations);

				component.setEnabled(!readOnly && !BSEntityFormPage.this.readOnly.contains(ref.getFieldName()));
				component.getFormComponent().setRequired(ref.isRequired());

				item.add(component);
			}

			@Nonnull
			private IModel<?> getInitialValue(FieldReference ref) {
				Serializable value = ref.getValue(getEntity());

				if (value != null) {
					if (value instanceof DomainObject) {
						return EntityEncapsulator
							.createModel((DomainObject) value);
					}

					return Model.of(value);
				}

				IModel<?> initial = initialValues.get(ref.getFieldName());
				if (initial != null) {
					return initial;
				}

				if (DomainObject.class.isAssignableFrom(ref.getPropertyClass())) {
					@SuppressWarnings("unchecked")
					Class<? extends DomainObject> type = (Class<? extends DomainObject>) ref.getPropertyClass();
					return EntityEncapsulator.createNullModel(type);
				}

				return Model.of((Serializable) null);
			}

		};
		fields.setReuseItems(true);

		entityForm.add(fields);
	}

	private List<FieldReference> scan(@Nonnull T entity) {
		return scan(entity.getClass());
	}

	private List<FieldReference> scan(@Nullable Class<?> targetClass) {
		if (targetClass == null) {
			return new ArrayList<>(0);
		}

		List<FieldReference> result = new LinkedList<>();
		result.addAll(scan(targetClass.getSuperclass()));

		for (Field f : targetClass.getDeclaredFields()) {
			EntityFormField ff = f.getAnnotation(EntityFormField.class);

			if (ff != null) {
				FieldReference ref = new FieldReference(
					Annotations.fromField(f), targetClass, f.getType(),
					f.getName());
				result.add(ref);
			}
		}

		return result;
	}

	/**
	 * Method to set the available choices for a given field
	 *
	 * @param attribute The JPA Metamodel attribute pointing to the field
	 * @param choices   The valid choices for this field
	 * @param <C>       The type of the valid choices for this field
	 * @return The current page
	 */
	public final <C> BSEntityFormPage<T> setChoices(@Nonnull Attribute<T, C> attribute, @Nonnull
		List<C> choices) {
		return setChoicesModel(attribute, Model.ofList(choices));
	}

	/**
	 * Method to set the available choices for a given field
	 *
	 * @param attribute The JPA Metamodel attribute pointing to the field
	 * @param choices   An IModel containing the valid choices for this field
	 * @param <C>       The type of the valid choices for this field
	 * @return The current page
	 */
	public final <C> BSEntityFormPage<T> setChoicesModel(@Nonnull Attribute<T, C> attribute, @Nonnull
		IModel<List<C>> choices) {

		providers.put(attribute.getName(), choices);

		return this;
	}

	/**
	 * Sets the initial value for the given field
	 *
	 * @param attribute The JPA Metamodel attribute pointing to the field
	 * @param value     The value to assign to the field
	 * @param <V>       The type of the value
	 * @return The current page
	 */
	@Nonnull
	public final <V extends Serializable> BSEntityFormPage<T> addInitialValue(@Nonnull Attribute<T, V> attribute,
																			  @Nonnull V value) {
		return addInitialValue(attribute, Model.of(value));
	}

	/**
	 * Sets the initial value for the given field
	 *
	 * @param attribute The JPA Metamodel attribute pointing to the field
	 * @param model     A model containing the initial value for the given field
	 * @param <V>       The type of the value
	 * @return The current page
	 */
	@Nonnull
	public final <V> BSEntityFormPage<T> addInitialValue(@Nonnull Attribute<T, V> attribute,
														 @Nonnull IModel<V> model) {
		initialValues.put(attribute.getName(), model);
		return this;
	}

	/**
	 * Sets a renderer for the given field
	 *
	 * @param attribute The attribute pointing to the field to set the renderer for
	 * @param renderer  The renderer to set
	 * @param <C>       The type of value rendered
	 * @return The current page
	 */
	@Nonnull
	public final <C> BSEntityFormPage<T> setRenderer(@Nonnull Attribute<T, C> attribute, @Nonnull
		IChoiceRenderer<C> renderer) {
		renderers.put(attribute.getName(), renderer);
		return this;
	}

	/**
	 * Sets the given field to read only status
	 *
	 * @param attribute The attribute pointing to the field to set the renderer for
	 * @param <C>       The type of value rendered
	 * @return The current page
	 */
	@Nonnull
	public final <C> BSEntityFormPage<T> setReadOnly(@Nonnull Attribute<T, C> attribute) {
		readOnly.add(attribute.getName());
		return this;
	}

	@Nonnull
	@SuppressWarnings("unchecked")
	@Override
	public <U> List<U> getSelectableValues(@Nonnull String fieldName) {

		if (providers.containsKey(fieldName)) {
			IModel<? extends List<?>> model = providers.get(fieldName);

			if (model != null) {
				return (List<U>) model.getObject();
			}
		}

		return ImmutableList.of();
	}

	@Nonnull
	@SuppressWarnings("unchecked")
	@Override
	public <U> IChoiceRenderer<U> getRenderer(@Nonnull String attributeName) {
		if (renderers.containsKey(attributeName)) {
			return (IChoiceRenderer<U>) renderers.get(attributeName);
		}

		return new ChoiceRenderer<>();
	}

	@Override
	protected void onDetach() {
		super.onDetach();

		for (IModel<? extends List<?>> model : providers.values()) {
			model.detach();
		}
	}
}
