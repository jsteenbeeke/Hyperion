package com.jeroensteenbeeke.hyperion.vavr.wicket.model;

import io.vavr.collection.Seq;
import org.apache.wicket.model.util.GenericBaseModel;

/**
 * Model that wraps vavr's {@code Seq} abstraction
 * @param <T> The type of element inside the Seq
 */
public class SeqModel<T> extends GenericBaseModel<Seq<T>> {
	/**
	 * Constructor
	 * @param seq The contained seq
	 */
	private SeqModel(Seq<T> seq) {
		setObject(seq);
	}

	@Override
	protected Seq<T> createSerializableVersionOf(Seq<T> object) {
		return object;
	}

	/**
	 * Create a new model for the given Seq
	 * @param seq The Seq to wrap
	 * @param <O> The type of element inside the Seq
	 * @return A new SeqModel
	 */
	public static <O> SeqModel<O> of(Seq<O> seq) {
		return new SeqModel<>(seq);
	}
}
