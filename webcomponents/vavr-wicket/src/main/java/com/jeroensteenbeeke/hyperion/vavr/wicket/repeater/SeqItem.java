package com.jeroensteenbeeke.hyperion.vavr.wicket.repeater;

import org.apache.wicket.IGenericComponent;
import org.apache.wicket.markup.html.list.LoopItem;
import org.apache.wicket.model.IModel;

/**
 * Container that holds components in a SeqView.
 *
 * @see SeqView
 * @author Jonathan Locke
 *
 * @param <T>
 *            Model object type
 */
public class SeqItem<T> extends LoopItem
		implements IGenericComponent<T, SeqItem<T>>
{
	private static final long serialVersionUID = 1L;

	/**
	 * A constructor which uses the index and the list provided to create a SeqItem. This
	 * constructor is the default one.
	 *
	 * @param index
	 *            The index of the item
	 * @param model
	 *            The model object of the item
	 */
	public SeqItem(final int index, final IModel<T> model)
	{
		super(index, model);
	}
}
