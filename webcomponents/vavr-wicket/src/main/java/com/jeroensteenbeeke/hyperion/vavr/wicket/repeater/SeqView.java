package com.jeroensteenbeeke.hyperion.vavr.wicket.repeater;

import com.jeroensteenbeeke.hyperion.vavr.wicket.model.SeqModel;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.repeater.AbstractRepeater;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.collections.ReadOnlyIterator;

import java.util.Iterator;

/**
 * Repeater for displaying a VAVR sequence
 * @param <T> The type of object contained in the sequence
 */
public abstract class SeqView<T> extends AbstractRepeater {
	/** Index of the first item to show */
	private int firstIndex = 0;

	/**
	 * If true, re-rendering the seq view is more efficient if the window doesn't get changed at
	 * all or if it gets scrolled (compared to paging). But if you modify the seqView model object,
	 * than you must manually call seqView.removeAll() in order to rebuild the ListItems. If you
	 * nest a ListView in a Form, ALWAYS set this property to true, as otherwise validation will not
	 * work properly.
	 */
	private boolean reuseItems = false;

	/** Max number (not index) of items to show */
	private int viewSize = Integer.MAX_VALUE;

	/**
	 * Constructor
	 * @param id The wicket:id
	 */
	public SeqView(String id) {
		super(id);
	}

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model containing the sequence
	 */
	public SeqView(String id, IModel<? extends Seq<T>> model) {
		super(id, model);
	}

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param seq The sequence
	 */
	public SeqView(String id, Seq<T> seq) {
		super(id, SeqModel.of(seq));
	}

	/**
	 * @see org.apache.wicket.markup.repeater.AbstractRepeater#renderIterator()
	 */
	@Override
	protected Iterator<Component> renderIterator()
	{
		final int size = size();
		return new ReadOnlyIterator<Component>()
		{
			private int index = 0;

			@Override
			public boolean hasNext()
			{
				return index < size;
			}

			@Override
			public Component next()
			{
				final String id = Integer.toString(firstIndex + index);
				index++;
				return get(id);
			}
		};
	}

	/**
	 * Gets the seq of items in the seqView. This method is final because it is not designed to be
	 * overridden. If it were allowed to be overridden, the values returned by getModelObject() and
	 * getList() might not coincide.
	 *
	 * @return The seq of items in this seq view.
	 */
	@SuppressWarnings("unchecked")
	public final Seq<T> getList()
	{
		final Seq<T> seq = (Seq<T>)getDefaultModelObject();
		if (seq == null)
		{
			return List.empty();
		}
		return seq;
	}

	/**
	 * If true re-rendering the seq view is more efficient if the windows doesn't get changed at
	 * all or if it gets scrolled (compared to paging). But if you modify the seqView model object,
	 * then you must manually call seqView.removeAll() in order to rebuild the ListItems. If you
	 * nest a ListView in a Form, ALLWAYS set this property to true, as otherwise validation will
	 * not work properly.
	 *
	 * @return Whether to reuse items
	 */
	public boolean getReuseItems()
	{
		return reuseItems;
	}

	/**
	 * Get index of first cell in page. Default is: 0.
	 *
	 * @return Index of first cell in page. Default is: 0
	 */
	public final int getStartIndex()
	{
		return firstIndex;
	}

	/**
	 * Based on the model object's seq size, firstIndex and view size, determine what the view size
	 * really will be. E.g. default for viewSize is Integer.MAX_VALUE, if not set via setViewSize().
	 * If the underlying seq has 10 elements, the value returned by getViewSize() will be 10 if
	 * startIndex = 0.
	 *
	 * @return The number of items to be populated and rendered.
	 */
	public int getViewSize()
	{
		int size = viewSize;

		final Object modelObject = getDefaultModelObject();
		if (modelObject == null)
		{
			return 0;
		}

		// Adjust view size to model object's seq size
		final int modelSize = getList().size();
		if (firstIndex > modelSize)
		{
			return 0;
		}

		if ((size == Integer.MAX_VALUE) || ((firstIndex + size) > modelSize))
		{
			size = modelSize - firstIndex;
		}

		// firstIndex + size must be smaller than Integer.MAX_VALUE
		if ((Integer.MAX_VALUE - size) < firstIndex)
		{
			throw new IllegalStateException(
					"firstIndex + size must be smaller than Integer.MAX_VALUE");
		}

		return size;
	}

	/**
	 * If true re-rendering the seq view is more efficient if the windows doesn't get changed at
	 * all or if it gets scrolled (compared to paging). But if you modify the seqView model object,
	 * than you must manually call seqView.removeAll() in order to rebuild the ListItems. If you
	 * nest a ListView in a Form, <strong>always</strong> set this property to true,
	 * as otherwise validation will not work properly.
	 *
	 * @param reuseItems whether to reuse the child items.
	 * @return this
	 */
	public SeqView<T> setReuseItems(boolean reuseItems)
	{
		this.reuseItems = reuseItems;
		return this;
	}

	/**
	 * Set the index of the first item to render
	 *
	 * @param startIndex first index of model object's seq to display
	 * @return This
	 */
	public SeqView<T> setStartIndex(final int startIndex)
	{
		firstIndex = startIndex;

		if (firstIndex < 0)
		{
			firstIndex = 0;
		}
		else if (firstIndex > getModelObject().size())
		{
			firstIndex = 0;
		}

		return this;
	}

	/**
	 * Define the maximum number of items to render. Default: render all.
	 *
	 * @param size number of items to display
	 * @return This
	 */
	public SeqView<T> setViewSize(final int size)
	{
		viewSize = size;

		if (viewSize < 0)
		{
			viewSize = Integer.MAX_VALUE;
		}

		return this;
	}

	@Override
	protected void onPopulate() {
		// Get number of items to be displayed
		final int size = getViewSize();
		if (size > 0)
		{
			if (getReuseItems())
			{
				// Remove all ListItems no longer required
				final int maxIndex = firstIndex + size;
				for (final Iterator<Component> iterator = iterator(); iterator.hasNext();)
				{
					// Get next child component
					final SeqItem<?> child = (SeqItem<?>)iterator.next();
					if (child != null)
					{
						final int index = child.getIndex();
						if (index < firstIndex || index >= maxIndex)
						{
							iterator.remove();
						}
					}
				}
			}
			else
			{
				// Automatically rebuild all ListItems before rendering the
				// seq view
				removeAll();
			}

			boolean hasChildren = size() != 0;
			// Loop through the markup in this container for each item
			for (int i = 0; i < size; i++)
			{
				// Get index
				final int index = firstIndex + i;

				@SuppressWarnings("unchecked")
				SeqItem<T> item = null;
				if (hasChildren)
				{
					// If this component does not already exist, populate it
					item = (SeqItem<T>)get(Integer.toString(index));
				}
				if (item == null)
				{
					// Create item for index
					item = newItem(index, getSeqItemModel(getModel(), index));

					// Add seq item
					add(item);

					// Populate the seq item
					onBeginPopulateItem(item);
					populateItem(item);
				}
			}
		}
		else
		{
			removeAll();
		}
	}

	/**
	 * Subclasses may provide their own SeqItemModel with extended functionality. The default
	 * SeqItemModel works fine with mostly static seqs where index remains valid. In cases where
	 * the underlying seq changes a lot (many users using the application), it may not longer be
	 * appropriate. In that case your own SeqItemModel implementation should use an id (e.g. the
	 * database' record id) to identify and load the seq item model object.
	 *
	 * @param seqViewModel the SeqView's model
	 * @param index the seq item index
	 * @return The SeqItemModel created
	 */
	protected IModel<T> getSeqItemModel(final IModel<? extends Seq<T>> seqViewModel,
										 final int index)
	{
		return new SeqItemModel<>(this, index);
	}

	/**
	 * Create a new SeqItem for seq item at index.
	 *
	 * @param index The index of the item
	 * @param itemModel object in the seq that the item represents
	 * @return SeqItem
	 */
	protected SeqItem<T> newItem(final int index, IModel<T> itemModel)
	{
		return new SeqItem<>(index, itemModel);
	}

	/**
	 * Comes handy for ready made ListView based components which must implement populateItem() but
	 * you don't want to lose compile time error checking reminding the user to implement abstract
	 * populateItem().
	 *
	 * @param item the item being populated
	 */
	protected void onBeginPopulateItem(final SeqItem<T> item)
	{
	}

	/**
	 * Populate a given item.
	 *
	 * @param item the item to populate
	 */
	protected abstract void populateItem(final SeqItem<T> item);

	/**
	 * Gets model
	 *
	 * @return model
	 */
	@SuppressWarnings("unchecked")
	public final IModel<? extends Seq<T>> getModel()
	{
		return (IModel<? extends Seq<T>>)getDefaultModel();
	}

	/**
	 * Sets model
	 *
	 * @param model The model to set
	 */
	public final void setModel(IModel<? extends Seq<T>> model)
	{
		setDefaultModel(model);
	}

	/**
	 * Gets model object
	 *
	 * @return model object
	 */
	@SuppressWarnings("unchecked")
	public final Seq<T> getModelObject()
	{
		return (Seq<T>)getDefaultModelObject();
	}

	/**
	 * Sets model object
	 *
	 * @param object The object to set
	 */
	public final void setModelObject(Seq<T> object)
	{
		setDefaultModelObject(object);
	}
}
