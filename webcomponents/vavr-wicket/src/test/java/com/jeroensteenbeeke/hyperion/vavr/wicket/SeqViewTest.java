package com.jeroensteenbeeke.hyperion.vavr.wicket;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class SeqViewTest {
	@Test
	public void testSeqView() {
		WicketTester tester = new WicketTester();

		tester.startPage(new SeqViewTestPage());
		tester.assertRenderedPage(SeqViewTestPage.class);

		tester.assertContains("<h1[^>]*>1</h1>");
		tester.assertContains("<h1[^>]*>2</h1>");
		tester.assertContains("<h1[^>]*>3</h1>");
		tester.assertContainsNot("<h1[^>]*>4</h1>");
		tester.assertContains("<h1[^>]*>5</h1>");
		tester.assertContains("<h1[^>]*>6</h1>");
		tester.assertContains("<h1[^>]*>7</h1>");

		tester.clickLink("refresh");

		tester.assertContains("<h1[^>]*>1</h1>");
		tester.assertContains("<h1[^>]*>2</h1>");
		tester.assertContains("<h1[^>]*>3</h1>");
		tester.assertContainsNot("<h1[^>]*>4</h1>");
		tester.assertContains("<h1[^>]*>5</h1>");
		tester.assertContains("<h1[^>]*>6</h1>");
		tester.assertContains("<h1[^>]*>7</h1>");
	}

}
