package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class IconLinksTest {
	@Test
	public void testIconLinks() {
		WicketTester tester = new WicketTester();

		tester.startPage(IconLinksTestPage.class);
		tester.assertRenderedPage(IconLinksTestPage.class);
		tester.assertLabel("text", "Text");
		tester.assertComponent("basic", IconLink.class);
		tester.assertComponent("ajax", AjaxIconLink.class);
		tester.clickLink("basic:link");

		tester.assertRenderedPage(IconLinksTestPage.class);
		tester.assertLabel("text", "Basic Link Clicked");

		tester.clickLink("ajax:link", true);
		tester.assertRenderedPage(IconLinksTestPage.class);
		tester.assertLabel("text", "Ajax Link Clicked");

		tester.clickLink("icontext:link");
		tester.assertRenderedPage(IconLinksTestPage.class);
		tester.assertLabel("text", "Icon Text Link Clicked");

		tester.clickLink("icontext2:link");
		tester.assertRenderedPage(IconLinksTestPage.class);
		tester.assertLabel("text", "Icon Text Link 2 Clicked");
	}
}
