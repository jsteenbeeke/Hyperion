package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.Component;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ModalWindowTest {
	@Rule
	public ExpectedException except = ExpectedException.none();

	@Test
	public void testModalWindow() {
		WicketTester tester = new WicketTester();

		tester.startPage(ModalWindowTestPage.class);
		tester.assertRenderedPage(ModalWindowTestPage.class);
		tester.assertContains("Refresh Time");
		tester.assertContains("Your mom");
		tester.assertContains("FooBar");
		tester.assertContains("Other page");
		tester.assertContains("class=\"btn\\s*btn-dark\"");
		tester.assertContains("class=\"btn\\s*btn-primary\"");
		tester.assertContains("class=\"btn\\s*btn-info\"");
		tester.assertContains("class=\"btn\\s*btn-danger\"");

		tester.assertContains("Submit A");
		tester.assertContains("Submit B");
		tester.assertContains("Submit C");

		Component component = tester.getComponentFromLastRenderedPage("a");
		assertTrue(component instanceof BootstrapModalWindow);

		BootstrapModalWindow window = (BootstrapModalWindow) component;

		assertNotNull(window.getDialog());
		assertNotNull(window.getDocument());
		assertNotNull(window.getContent());
		assertNotNull(window.getHeader());
		assertNotNull(window.getBody());
		assertNotNull(window.getFooter());



	}

	@Test
	public void testIncompleteButtons() {
		except.expect(IllegalStateException.class);
		except.expectMessage("There are 7 unfinished button creation chains.");
		WicketTester tester = new WicketTester();

		tester.startPage(ModalWindowTestInvalidPage.class);
	}
}
