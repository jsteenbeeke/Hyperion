package com.jeroensteenbeeke.hyperion.heinlein.web.pages;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

public class ConfirmationTriggerPage extends WebPage {
	public ConfirmationTriggerPage(ConfirmationPage.ColorScheme scheme) {
		add(new Link<Void>("confirm") {
			@Override
			public void onClick() {
				setResponsePage(new ConfirmationPage("Proceed?", "Yes or no?", scheme, answer ->
						setResponsePage(answer ? new YesAnswerPage
						() : new NoAnswerPage())));
			}
		});
	}
}
