package com.jeroensteenbeeke.hyperion.heinlein.web.pages;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class ConfirmationPageTest {
	@Test
	public void testYesOption() {
		WicketTester tester = new WicketTester();

		tester.startPage(new ConfirmationTriggerPage(ConfirmationPage.ColorScheme.NORMAL));
		tester.assertRenderedPage(ConfirmationTriggerPage.class);

		tester.clickLink("confirm");

		tester.assertRenderedPage(ConfirmationPage.class);

		tester.clickLink("yes");

		tester.assertRenderedPage(YesAnswerPage.class);

	}

	@Test
	public void testNoOption() {
		WicketTester tester = new WicketTester();

		tester.startPage(new ConfirmationTriggerPage(ConfirmationPage.ColorScheme.INVERTED));
		tester.assertRenderedPage(ConfirmationTriggerPage.class);

		tester.clickLink("confirm");

		tester.assertRenderedPage(ConfirmationPage.class);

		tester.clickLink("no");

		tester.assertRenderedPage(NoAnswerPage.class);

	}
}
