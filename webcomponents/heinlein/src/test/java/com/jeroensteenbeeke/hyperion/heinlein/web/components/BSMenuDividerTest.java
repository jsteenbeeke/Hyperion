package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class BSMenuDividerTest {
	@Test
	public void testNavigator() {
		WicketTester tester = new WicketTester();
		tester.startPage(new BSMenuDividerTestPage(false));

		tester.assertRenderedPage(BSMenuDividerTestPage.class);
		tester.assertInvisible("trigger");
		tester.assertInvisible("divider");

		tester.startPage(new BSMenuDividerTestPage(true));

		tester.assertRenderedPage(BSMenuDividerTestPage.class);
		tester.assertVisible("trigger");
		tester.assertVisible("divider");
	}
}
