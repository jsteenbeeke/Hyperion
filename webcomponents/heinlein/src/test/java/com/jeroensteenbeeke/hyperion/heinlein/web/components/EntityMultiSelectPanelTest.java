package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.heinlein.web.TestEntityEncapsulator;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class EntityMultiSelectPanelTest {
	@Test
	public void testSelectPanel() {
		WicketTester tester = new WicketTester();

		EntityEncapsulator.setFactory(new TestEntityEncapsulator());
		tester.startPage(EntityMultiSelectPanelTestPage.class);
		tester.assertRenderedPage(EntityMultiSelectPanelTestPage.class);

		tester.assertComponent("entity", EntityMultiSelectPanel.class);
		tester.assertComponent("entity:heading", Label.class);
		tester.assertComponent("entity:choices", ListView.class);
		tester.assertComponent("entity:choices:0:choice", CheckBox.class);
		tester.assertComponent("entity:choices:1:choice", CheckBox.class);
		tester.assertComponent("entity:choices:2:choice", CheckBox.class);
		tester.assertComponent("entity:choices:3:choice", CheckBox.class);
		tester.assertComponent("entity:choices:4:choice", CheckBox.class);
	}
}
