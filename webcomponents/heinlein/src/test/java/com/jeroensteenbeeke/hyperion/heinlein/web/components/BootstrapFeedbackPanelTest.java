package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class BootstrapFeedbackPanelTest {
	@Test
	public void testFeedback() {
		WicketTester tester = new WicketTester();
		tester.startPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);

		tester.assertFeedback("feedback");

		tester.clickLink("success");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertFeedback("feedback", "Success");

		tester.clickLink("info");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertFeedback("feedback", "Info");

		tester.clickLink("warn");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertFeedback("feedback", "Warn");

		tester.clickLink("error");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertFeedback("feedback", "Error");

		tester.clickLink("fatal");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertFeedback("feedback", "Fatal");

		tester.clickLink("undefined");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertFeedback("feedback", "Undefined");
	}
}
