package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.util.tester.WicketTester;

import java.util.List;

import static com.jeroensteenbeeke.hyperion.heinlein.web.components.AbstractPagingNavigatorTestPage.LIST_SIZE;

public abstract class AbstractPagingNavigatorTest {
	protected void runTest(Class<? extends AbstractPagingNavigatorTestPage> pageClass, Class<? extends Component> linkClass) {
		WicketTester tester = new WicketTester();
		tester.startPage(pageClass);

		tester.assertRenderedPage(pageClass);

		AbstractPagingNavigatorTestPage page = (AbstractPagingNavigatorTestPage) tester.getLastRenderedPage();
		List<String> list = page.getList();

		tester.assertComponent(getListComponentId(), PageableListView.class);
		tester.assertModelValue(getListComponentId(), list);

		assertOnlySegmentAppears(tester, list, 0, 5);

		assertListItem(tester, "<a .* title=\"Go to first page\">.*</a>");
		assertListItem(tester, "<a .*disabled=\"disabled\".*>.*<span>1</span>.*</a>");
		assertListItem(tester, "<a.*>.*<span>2</span>.*</a>");
		assertListItem(tester, "<a.*>.*<span>3</span>.*</a>");
		assertListItem(tester, "<a.*>.*<span>4</span>.*</a>");
		assertListItem(tester, "<a.*>.*<span>5</span>.*</a>");
		assertListItem(tester, "<a .* title=\"Go to last page\">.*</a>");
		assertListItem(tester, "<a .*class=\".*page-link\".*</a>");

		tester.assertComponent("nav:navigation:1:pageLink", linkClass);
		tester.clickLink("nav:navigation:1:pageLink");
		tester.assertRenderedPage(pageClass);

		tester.assertComponent(getListComponentId(), PageableListView.class);
		tester.assertModelValue(getListComponentId(), list);

		assertOnlySegmentAppears(tester, list, 5, 10);

		tester.assertComponent("nav:navigation:2:pageLink", linkClass);
		tester.clickLink("nav:navigation:2:pageLink");
		tester.assertRenderedPage(pageClass);

		tester.assertComponent(getListComponentId(), PageableListView.class);
		tester.assertModelValue(getListComponentId(), list);

		assertOnlySegmentAppears(tester, list, 10, 15);

		tester.assertComponent("nav:navigation:3:pageLink", linkClass);
		tester.clickLink("nav:navigation:3:pageLink");
		tester.assertRenderedPage(pageClass);

		tester.assertComponent(getListComponentId(), PageableListView.class);
		tester.assertModelValue(getListComponentId(), list);

		assertOnlySegmentAppears(tester, list, 15, 20);

		tester.assertComponent("nav:navigation:4:pageLink", linkClass);
		tester.clickLink("nav:navigation:4:pageLink");
		tester.assertRenderedPage(pageClass);

		tester.assertComponent(getListComponentId(), PageableListView.class);
		tester.assertModelValue(getListComponentId(), list);

		assertOnlySegmentAppears(tester, list, 20, 25);
	}
	
	public String getListComponentId() {
		return "strings";
	}

	protected void assertListItem(WicketTester tester, String regex) {
		String pattern = String.format("<ul class=\"pagination\">.*<li.*>.*%s.*</li>.*</ul>", regex);

		tester.assertContains(pattern);
	}


	protected void assertOnlySegmentAppears(WicketTester tester, List<String> values, int start, int end) {
		for (int i = 0; i < start; i++) {
			tester.assertContainsNot(values.get(i));
		}

		for (int i = start; i < end; i++) {
			tester.assertContains(values.get(i));
		}

		for (int i = end; i < LIST_SIZE; i++) {
			tester.assertContainsNot(values.get(i));

		}
	}

}
