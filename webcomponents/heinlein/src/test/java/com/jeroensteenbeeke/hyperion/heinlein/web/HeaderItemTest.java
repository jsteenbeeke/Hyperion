package com.jeroensteenbeeke.hyperion.heinlein.web;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class HeaderItemTest {
	@Test
	public void testReferences() {
		WicketTester tester = new WicketTester();

		IllegalStateException ex = null;
		CssHeaderItem item = null;
		try {
			item = Heinlein.createCssHeaderItem();
		} catch (IllegalStateException e) {
			ex = e;
		}

		assertNull(item);
		assertNotNull(ex);

		Heinlein.init(tester.getApplication(), "test.css");

		tester.startPage(ResourceReferenceTestPage.class);
		tester.assertRenderedPage(ResourceReferenceTestPage.class);


		tester.assertContains("test\\.css");
		tester.assertContains("jquery\\.ui\\.touch-punch\\.js");
		tester.assertContains("bootstrap-toggle\\.js");
		tester.assertContains("bootstrap-datepicker\\.js");
		tester.assertContains("bootstrap\\.min\\.js");
		tester.assertContains("bootstrap-datepicker3\\.css");
		tester.assertContains("bootstrap-toggle\\.css");
	}
}
