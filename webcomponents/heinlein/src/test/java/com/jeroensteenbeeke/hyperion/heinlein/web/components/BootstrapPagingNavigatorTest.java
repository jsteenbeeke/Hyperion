package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.navigation.paging.PagingNavigationLink;
import org.junit.Test;

public class BootstrapPagingNavigatorTest extends AbstractPagingNavigatorTest {
	@Test
	public void testNavigator() {
		runTest(BootstrapPagingNavigatorTestPage.class, PagingNavigationLink.class);
	}

}
