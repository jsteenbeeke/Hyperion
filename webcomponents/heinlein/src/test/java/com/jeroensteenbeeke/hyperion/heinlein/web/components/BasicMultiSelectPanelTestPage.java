package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.google.common.collect.Lists;
import com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice.LambdaRenderer;
import org.apache.wicket.markup.html.WebPage;

public class BasicMultiSelectPanelTestPage extends WebPage {
	public BasicMultiSelectPanelTestPage() {
		add(new BasicMultiSelectPanel<>("basic",
				"Basic Multiselect",
				Lists.newArrayList("A", "B", "E"),
				Lists.newArrayList("A", "B", "C", "D", "E"),
				LambdaRenderer.of(a -> a)
		));
	}
}
