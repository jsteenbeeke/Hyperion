package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;

public class TestWindowB extends BootstrapModalWindow {
	public TestWindowB(String id) {
		super(id, "Window A");

		Label label = new Label("label", () -> Long.toString(System.currentTimeMillis()));
		label.setOutputMarkupId(true);
		add(label);

		addAjaxButton(target -> target.add(label)).ofType(ButtonType.Dark);
		this.<String> addAjaxButton((target, str) -> {})
				.withModel(Model.of("Your mom"))
				.ofType(ButtonType.Danger);

		addButton(() -> setResponsePage(IconLinksTestPage.class));
		this.<String> addButton(foo -> {}).withModel(Model.of("FooBar"));

		Form<String> form = new Form<>("form");

		addSubmitButton();
		addAjaxSubmitButton().forForm(form);
		addAjaxSubmitButton(target -> {}).forForm(form).ofType(ButtonType.Info);
	}

	@Override
	public boolean isCloseEnabled() {
		return false;
	}
}
