package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.google.common.collect.Lists;
import com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice.LambdaRenderer;
import org.apache.wicket.markup.html.WebPage;

public class IncorrectBasicMultiSelectTestPage extends WebPage {
	public IncorrectBasicMultiSelectTestPage() {
		add(new BasicMultiSelectPanel<>("basic",
				"Basic Multiselect",
				Lists.newArrayList("A", "B", "E", "F"),
				Lists.newArrayList("A", "B", "C", "D", "E"),
				LambdaRenderer.of(a -> a)
		));
	}
}
