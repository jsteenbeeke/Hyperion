package com.jeroensteenbeeke.hyperion.heinlein.web;

import com.jeroensteenbeeke.hyperion.heinlein.web.pages.BootstrapBasePage;
import com.jeroensteenbeeke.hyperion.heinlein.web.resources.*;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebPage;

public class ResourceReferenceTestPage extends BootstrapBasePage {
	public ResourceReferenceTestPage() {
		super("ResourceReferenceTestPage");
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		response.render(JavaScriptHeaderItem.forReference(BSCheckBoxToggleJavaScriptReference.get()));
		response.render(CssHeaderItem.forReference(BSCheckBoxToggleCSSResourceReference.get()));

		response.render(JavaScriptHeaderItem.forReference(BSDatePickerJavaScriptReference.get()));
		response.render(CssHeaderItem.forReference(BSDatePickerCSSResourceReference.get()));

		response.render(JavaScriptHeaderItem.forReference(TouchPunchJavaScriptReference.get()));
	}
}
