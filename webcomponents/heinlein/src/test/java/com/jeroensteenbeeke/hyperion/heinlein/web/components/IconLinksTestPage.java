package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.icons.fontawesome.FontAwesome;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;

public class IconLinksTestPage extends WebPage {
	private final Label label;

	public IconLinksTestPage() {
		this("Text");
	}

	public IconLinksTestPage(String text) {
		add(label = new Label("text", text));
		label.setOutputMarkupId(true);

		add(new IconLink<String>("basic", () -> "Basic Link Clicked", FontAwesome.baseball_ball) {
			@Override
			public void onClick() {
				setResponsePage(new IconLinksTestPage(getModelObject()));
			}
		}.setTitle("Basic"));

		add(new AjaxIconLink<String>("ajax", () -> "Ajax Link Clicked", FontAwesome.volleyball_ball) {
			@Override
			public void onClick(AjaxRequestTarget target) {
				label.setDefaultModelObject(getModelObject());
				target.add(label);
			}
		}.setTitle("Ajax"));

		add(new IconTextLink<String>("icontext", () -> "Icon Text Link Clicked", FontAwesome.basketball_ball) {
			@Override
			public void onClick() {
				setResponsePage(new IconLinksTestPage(getModelObject()));
			}
		});

		add(new IconTextLink<String>("icontext2", () -> "Icon Text Link 2 Clicked", FontAwesome.football_ball, m -> "Peaches") {
			@Override
			public void onClick() {
				setResponsePage(new IconLinksTestPage(getModelObject()));
			}
		});
	}
}
