package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigationLink;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class AjaxBootstrapPagingNavigatorTest extends AbstractPagingNavigatorTest {
	@Test
	public void testNavigator() {
		runTest( AjaxBootstrapPagingNavigatorTestPage.class, AjaxPagingNavigationLink.class);
	}

	@Override
	public String getListComponentId() {
		return "container:strings";
	}
}
