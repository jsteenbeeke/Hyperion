package com.jeroensteenbeeke.hyperion.heinlein.web;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.web.IEntityEncapsulatorFactory;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.util.ListModel;

import javax.annotation.Nonnull;
import java.util.List;

public class TestEntityEncapsulator implements IEntityEncapsulatorFactory {
	@Nonnull
	@Override
	public <T extends DomainObject, F extends SearchFilter<T, F>> IDataProvider<T> createDataProvider(@Nonnull F filter, @Nonnull DAO<T, F> dao) {
		return new ListDataProvider<>(dao.findByFilter(filter).toJavaList());
	}

	@Nonnull @Override
	public <T extends DomainObject> IModel<T> createModel(@Nonnull T object) {
		return Model.of(object);
	}

	@Nonnull @Override
	public <T extends DomainObject> IModel<List<T>> createListModel(
			@Nonnull List<T> list) {
		return new ListModel<>(list);
	}

	@Override
	public <T extends DomainObject> IModel<T> createModel(Class<T> modelClass) {
		return Model.of();
	}
}
