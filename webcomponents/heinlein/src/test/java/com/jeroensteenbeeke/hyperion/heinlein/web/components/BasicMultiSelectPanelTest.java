package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BasicMultiSelectPanelTest {
	@Test
	public void testSelectPanel() {
		WicketTester tester = new WicketTester();

		tester.startPage(BasicMultiSelectPanelTestPage.class);
		tester.assertRenderedPage(BasicMultiSelectPanelTestPage.class);

		tester.assertComponent("basic", BasicMultiSelectPanel.class);
		tester.assertComponent("basic:heading", Label.class);
		tester.assertComponent("basic:choices", ListView.class);
		tester.assertComponent("basic:choices:0:choice", CheckBox.class);
		tester.assertComponent("basic:choices:1:choice", CheckBox.class);
		tester.assertComponent("basic:choices:2:choice", CheckBox.class);
		tester.assertComponent("basic:choices:3:choice", CheckBox.class);
		tester.assertComponent("basic:choices:4:choice", CheckBox.class);
	}

	@Test
	public void testSelectPanelWithInvalidChoice() {
		WicketTester tester = new WicketTester();
		Throwable thrown = null;

		try {
			tester.startPage(IncorrectBasicMultiSelectTestPage.class);
		} catch (WicketRuntimeException e) {
			thrown = e;
			while (thrown.getCause() != null) {
				thrown = thrown.getCause();
			}
		}

		assertNotNull(thrown);
		assertEquals("Selected list contains elements not in choices list: F", thrown.getMessage());
	}
}
