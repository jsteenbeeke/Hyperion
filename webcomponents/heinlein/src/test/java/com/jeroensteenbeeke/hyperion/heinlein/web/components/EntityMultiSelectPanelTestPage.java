package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.google.common.collect.Lists;
import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice.LambdaRenderer;
import org.apache.wicket.markup.html.WebPage;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

public class EntityMultiSelectPanelTestPage extends WebPage {
	private static long c = 1L;

	private static final Map<String,Entity> entities = new TreeMap<>();

	public EntityMultiSelectPanelTestPage() {
		add(new EntityMultiSelectPanel<>("entity",
				"Entity Multiselect",
				Lists.newArrayList(e("A"), e("B"), e("E")),
				Lists.newArrayList(e("A"), e("B"), e("C"), e("D"), e("E")),
				LambdaRenderer.of(Entity::getName)
		));
	}

	public static Entity e(String name) {
		return entities.put(name, entities.computeIfAbsent(name, n -> new Entity(c++, n)));
	}
}

class Entity implements DomainObject {
	private Long id;

	private String name;

	public Entity(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Entity() {
	}

	@Override
	public Serializable getDomainObjectId() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public Entity setId(Long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public Entity setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Entity entity = (Entity) o;

		if (!id.equals(entity.id)) return false;
		return name.equals(entity.name);
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + name.hashCode();
		return result;
	}
}
