package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.google.common.collect.Lists;
import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import com.jeroensteenbeeke.hyperion.webcomponents.core.Components;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.IFormModelUpdateListener;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A composition of form elements that allow a user to select a number of items from a given list using checkboxes
 *
 * @param <T> The type of list element displayed by the panel
 */
public abstract class AbstractMultiSelectPanel<T> extends TypedPanel<List<T>>
		implements IFormModelUpdateListener {
	private static final String CHOICE_ID = "choice";

	private class ChoiceModel extends LoadableDetachableModel<Boolean> {
		private static final long serialVersionUID = 1L;

		private final IModel<T> targetModel;

		private ChoiceModel(IModel<T> targetModel) {
			super();
			this.targetModel = targetModel;
		}

		@Override
		protected Boolean load() {
			T object = targetModel.getObject();

			return AbstractMultiSelectPanel.this.getModelObject().contains(
					object);
		}
	}

	private static final long serialVersionUID = 1L;

	private IModel<List<T>> choices;

	private ListView<T> choiceView;

	/**
	 * Creates a new multi-select panel
	 *
	 * @param id       The Wicket ID of the panel
	 * @param title    The title of the panel: i.e. what it is that the user is selecting
	 * @param selected The list of items initially checked. This should be a subset of the next parameter
	 * @param choices  The list of items that can be checked.
	 * @param renderer An IChoiceRenderer for converting the items from the list to valid display values
	 */
	protected AbstractMultiSelectPanel(@Nonnull String id, @Nonnull String title,
									   @Nonnull List<T> selected, @Nonnull List<T> choices, @Nonnull IChoiceRenderer<T> renderer) {
		super(id);

		if (!choices.containsAll(selected)) {
			List<T> sel = Lists.newArrayList(selected);
			sel.removeAll(choices);

			throw new IllegalArgumentException(String.format(
					"Selected list contains elements not in choices list: %s",
					sel.stream()
							.map(renderer::getDisplayValue)
					   		.map(Object::toString)
							.collect(Collectors.joining(", "))));
		}

		setModel(modelFor(selected, true));
		this.choices = modelFor(choices, false);

		add(new Label("heading", title));
		choiceView = new ListView<T>("choices", this.choices) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<T> item) {
				T object = item.getModelObject();

				CheckBox box = new CheckBox(CHOICE_ID, new ChoiceModel(
						item.getModel()));
				Object displayValue = renderer.getDisplayValue(object);
				if (displayValue instanceof String) {
					box.setLabel(Model.of((String) displayValue));
				}
				item.add(box);

			}
		};
		add(choiceView);

	}

	@Override
	protected void onDetach() {
		super.onDetach();

		choices.detach();
	}

	/**
	 * Returns an IModel for the given list of choices
	 * @param list The list of values to create a model for
	 * @param mutable Whether or not the model should be mutable: i.e. if the values can be changed
	 * @return A model wrapping the given list
	 */
	protected abstract IModel<List<T>> modelFor(List<T> list, boolean mutable);

	@Override
	public void updateModel() {
		List<T> newChoices = Lists.newArrayList();

		Components.forEach(choiceView, li -> {
			T object = li.getModelObject();
			CheckBox box = (CheckBox) li.get(CHOICE_ID);

			if (box.getModelObject()) {
				newChoices.add(object);
			}
		});

		setModelObject(newChoices);
	}

}
