package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.ListModel;

/**
 * Multi-select panel that displays regular (non-entity) serializable values
 * @param <T> The type displayed by the panel
 */
public class BasicMultiSelectPanel<T extends Serializable> extends
		AbstractMultiSelectPanel<T> {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new multi-select panel with the given properties
	 * @param id The id of the component
	 * @param title The title of the panel
	 * @param selected The list of currently selected values
	 * @param choices The list of available values
	 * @param renderer A renderer to create labels for the given values
	 */
	public BasicMultiSelectPanel(String id, String title, List<T> selected,
			List<T> choices, IChoiceRenderer<T> renderer) {
		super(id, title, selected, choices, renderer);
	}

	@Override
	protected IModel<List<T>> modelFor(List<T> list, boolean writeable) {
		return new ListModel<>(list);
	}

}
