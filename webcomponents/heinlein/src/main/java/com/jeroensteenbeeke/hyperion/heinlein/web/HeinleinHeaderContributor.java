/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.IHeaderContributor;

import javax.annotation.Nonnull;

/**
 * HeaderContributor that ensures all Wicket pages have the required CSS and JS file includes
 */
class HeinleinHeaderContributor implements IHeaderContributor {
	private static final long serialVersionUID = 1L;

	private final String contextRelativeCSSFile;

	HeinleinHeaderContributor(@Nonnull String contextRelativeCSSFile) {
		this.contextRelativeCSSFile = contextRelativeCSSFile;
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		response.render(CssHeaderItem.forUrl(contextRelativeCSSFile));
	}


}
