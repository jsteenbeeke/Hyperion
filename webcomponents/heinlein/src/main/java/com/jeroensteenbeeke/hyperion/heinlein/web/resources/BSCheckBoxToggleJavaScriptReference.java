package com.jeroensteenbeeke.hyperion.heinlein.web.resources;

import java.util.List;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.resource.JQueryPluginResourceReference;

import com.google.common.collect.ImmutableList;

/**
 * Resource reference for the bootstrap-toggle.js file
 */
public class BSCheckBoxToggleJavaScriptReference
		extends JQueryPluginResourceReference {

	private static final long serialVersionUID = 1L;
	private static final BSCheckBoxToggleJavaScriptReference instance = new BSCheckBoxToggleJavaScriptReference();

	private BSCheckBoxToggleJavaScriptReference() {
		super(BSCheckBoxToggleJavaScriptReference.class,
				"js/bootstrap-toggle.js");
	}

	@Override
	public List<HeaderItem> getDependencies() {
		ImmutableList.Builder<HeaderItem> deps = ImmutableList.builder();

		deps.addAll(super.getDependencies());
		deps.add(CssHeaderItem.forReference(BSCheckBoxToggleCSSResourceReference.get()));

		return deps.build();
	}

	/**
	 * Get a singleton instance of this resource
	 * @return A ResourceReference
	 */
	public static BSCheckBoxToggleJavaScriptReference get() {
		return instance;
	}
}
