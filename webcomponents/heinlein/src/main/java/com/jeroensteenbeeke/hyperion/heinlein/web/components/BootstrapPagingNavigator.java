package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.webcomponents.core.HTMLWrapper;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.navigation.paging.*;

import javax.annotation.Nonnull;

/**
 * Modified version of the default PagingNavigator that adds bootstrap-specific markup
 * to the component
 */
public class BootstrapPagingNavigator extends PagingNavigator implements HTMLWrapper {
	private static final long serialVersionUID = -4267416173536448060L;

	/**
	 * Creates a new BootstrapPagingNavigator
	 *
	 * @param id The id corresponding to the wicket:id in the HTML markup
	 * @param pageable The pageable component the page links are referring to
	 */
	public BootstrapPagingNavigator(@Nonnull String id, @Nonnull IPageable pageable) {
		super(id, pageable);
		setRenderBodyOnly(true);
	}

	/**
	 * Creates a new BootstrapPagingNavigator
	 *
	 * @param id The id corresponding to the wicket:id in the HTML markup
	 * @param pageable he pageable component the page links are referring to
	 * @param labelProvider The label provider for the link text
	 */
	public BootstrapPagingNavigator(@Nonnull String id, @Nonnull IPageable pageable, @Nonnull IPagingLabelProvider labelProvider) {
		super(id, pageable, labelProvider);
		setRenderBodyOnly(true);
	}

	@Override
	protected void onRender() {
		wrapRenderedHTML("<nav><ul class=\"pagination\">", () -> super.onRender(), "</ul></nav>");
	}

	@Override
	protected AbstractLink newPagingNavigationIncrementLink(String id, IPageable pageable, int increment) {
		PagingNavigationIncrementLink<Void> link = new PagingNavigationIncrementLink<Void>(id, pageable, increment) {
			private static final long serialVersionUID = 3662819653820610962L;

			@Override
			protected void onRender() {
				wrapRenderedHTML("<li class=\"page-item\">", () -> super.onRender(), "</li>");
			}
		};
		link.add(AttributeModifier.append("class", "page-link"));
		return link;
	}

	@Override
	protected AbstractLink newPagingNavigationLink(String id, IPageable pageable, int pageNumber) {
		PagingNavigationLink<Void> link = new PagingNavigationLink<Void>(id, pageable, pageNumber) {
			private static final long serialVersionUID = -3264724599299394089L;

			@Override
			protected void onRender() {
				wrapRenderedHTML("<li class=\"page-item\">", () -> super.onRender(), "</li>");
			}
		};
		link.add(AttributeModifier.append("class", "page-link"));
		return link;
	}

	@Override
	protected PagingNavigation newNavigation(String id, IPageable pageable, IPagingLabelProvider labelProvider) {
		return new BootstrapPagingNavigation(id, pageable, labelProvider);
	}
}
