package com.jeroensteenbeeke.hyperion.heinlein.web.pages;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.danekja.java.util.function.serializable.SerializableConsumer;

import javax.annotation.Nonnull;

/**
 * A simple bootstrap-based page for confirming a certain action
 * 
 * @author Jeroen Steenbeeke
 *
 */
public class ConfirmationPage extends BootstrapBasePage {
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new confirmation page
	 * @param title The title of the page
	 * @param message The message to display on the page
	 * @param scheme The color scheme to apply to the buttons.
	 * @param onAnswer A Consumer that is called once the user has given an anwer
	 */
	public ConfirmationPage(@Nonnull String title, @Nonnull String message, @Nonnull ColorScheme scheme,
							@Nonnull SerializableConsumer<Boolean> onAnswer) {
		super(title);

		add(new Label("ptitle", title));
		add(new Label("message", message));
		
		Link<Boolean> yes = new Link<Boolean>("yes") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				onAnswer.accept(true);
			}
			
		};
		yes.add(AttributeModifier.replace("class", scheme.getYesButtonCSS()));
		yes.setBody(getYesBody());
		add(yes);
		
		Link<Boolean> no = new Link<Boolean>("no") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				onAnswer.accept(false);
			}
			
		};
		no.add(AttributeModifier.replace("class", scheme.getNoButtonCSS()));
		no.setBody(getNoBody());
		add(no);

	}

	/**
	 * Determine the text to display in the positive response button (default: yes)
	 * @return An IModel that returns the appropriate response
	 */
	protected IModel<?> getYesBody() {
		return Model.of("Yes");
	}

	/**
	 * Determine the text to display in the negative response button (default: no)
	 * @return An IModel that returns the appropriate response
	 */
	protected IModel<?> getNoBody() {
		return Model.of("No");
	}


	/**
	 * Display options for the positive and negative buttons
	 */
	public enum ColorScheme {
		/**
		 * Regular color scheme, with the Yes button styled as success, and No styled as danger
		 */
		NORMAL {
			@Override
			public String getYesButtonCSS() {
				return "btn btn-success";
			}

			@Override
			public String getNoButtonCSS() {
				return "btn btn-danger";
			}
		}, 
		/**
		 * Inverted color scheme, with the Yes button styled as danger, and No styled as success
		 */
		INVERTED {
			@Override
			public String getYesButtonCSS() {
				return "btn btn-danger";
			}

			@Override
			public String getNoButtonCSS() {
				return "btn btn-success";
			}
		};

		/**
		 * Returns the CSS for the positive button
		 * @return A String of CSS classes
		 */
		public abstract String getYesButtonCSS();

		/**
		 * Returns the CSS for the negative button
		 * @return A String of CSS classes
		 */
		public abstract String getNoButtonCSS();
	}
}
