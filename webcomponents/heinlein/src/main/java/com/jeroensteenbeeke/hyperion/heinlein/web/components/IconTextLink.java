/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import com.jeroensteenbeeke.hyperion.icons.Icon;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableFunction;

import java.io.Serializable;

/**
 * Link that uses a Bootstrap Glyphicon, and has a descriptive text
 * @param <T> The type of the model's object
 */
public abstract class IconTextLink<T extends Serializable> extends TypedPanel<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new IconTextLink that uses the toString function to figure out the descriptive text
	 * @param id The id of the icon
	 * @param model The model containing the link's object
	 * @param icon The icon to use
	 */
	public IconTextLink(String id, IModel<T> model, Icon icon) {
		this(id, model, icon, Object::toString);
	}

	/**
	 * Creates a new IconTextLink
	 * @param id The id of the icon
	 * @param model The model containing the link's object
	 * @param icon The icon to use
	 * @param modelToLabel A function that determines the descriptive text based on the link's model
	 */
	public IconTextLink(String id, IModel<T> model, Icon icon, SerializableFunction<T, String>
			modelToLabel) {
		super(id, model);

		Link<T> iconLink = new Link<T>("link", model) {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				IconTextLink.this.onClick();
			}
		};

		iconLink.add(new WebMarkupContainer("icon").add(AttributeModifier
				.replace("class", icon.getCssClasses())));
		iconLink.add(new Label("text", new LabelModel(modelToLabel)));

		add(iconLink);
	}

	/**
	 * Listener method invoked when the user clicks the link
	 */
	public abstract void onClick();

	private class LabelModel implements IModel<String> {
		private static final long serialVersionUID = 1L;

		private final SerializableFunction<T, String> modelToString;

		private LabelModel(SerializableFunction<T, String> modelToString) {
			this.modelToString = modelToString;
		}

		@Override
		public String getObject() {
			return modelToString.apply(IconTextLink.this.getModelObject());
		}
	}

}
