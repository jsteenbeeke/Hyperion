/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.icons.Icon;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;

import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;

/**
 * Link that uses a Bootstrap Glyphicon
 * @param <T> The type of the model's object
 */
public abstract class IconLink<T> extends TypedPanel<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new IconLink
	 * @param id The id of the icon
	 * @param model The model containing the link's object
	 * @param icon The icon to use
	 */
	public IconLink(String id, IModel<T> model, Icon icon) {
		super(id, model);

		Link<T> iconLink = new Link<T>("link", model) {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				IconLink.this.onClick();
			}
		};

		iconLink.add(new WebMarkupContainer("icon").add(AttributeModifier
				.replace("class", icon.getCssClasses())));

		add(iconLink);
	}

	/**
	 * Sets the title (hover-text) of the icon
	 * @param title The title of the icon
	 * @return The current link
	 */
	public IconLink<T> setTitle(String title) {
		get("link").get("icon").add(AttributeModifier.replace("title", title));
		return this;
	}

	/**
	 * Listener method invoked when the user clicks the link
	 */
	public abstract void onClick();
}
