package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import java.util.List;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;

/**
 * Multi-select panel that displays entity values
 * @param <T> The type displayed by the panel
 */
public class EntityMultiSelectPanel<T extends DomainObject>
		extends AbstractMultiSelectPanel<T> {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new multi-select panel with the given properties
	 * @param id The id of the component
	 * @param title The title of the panel
	 * @param selected The list of currently selected values
	 * @param choices The list of available values
	 * @param renderer A renderer to create labels for the given values
	 */
	public EntityMultiSelectPanel(String id, String title, List<T> selected,
			List<T> choices, IChoiceRenderer<T> renderer) {
		super(id, title, selected, choices, renderer);
	}

	@Override
	protected IModel<List<T>> modelFor(List<T> list, boolean writeable) {
		return EntityEncapsulator.createListModel(list);
	}

}
