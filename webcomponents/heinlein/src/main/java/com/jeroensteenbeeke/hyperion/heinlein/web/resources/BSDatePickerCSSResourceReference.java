/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.resources;

import java.util.List;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.request.resource.CssResourceReference;

import com.google.common.collect.ImmutableList;
import com.jeroensteenbeeke.hyperion.heinlein.web.Heinlein;

/**
 * Resource reference for the bootstrap-datepicker3.css file
 */
public class BSDatePickerCSSResourceReference extends CssResourceReference {
	private static final long serialVersionUID = 1L;

	private static final BSDatePickerCSSResourceReference instance = new BSDatePickerCSSResourceReference();

	private BSDatePickerCSSResourceReference() {
		super(BSDatePickerCSSResourceReference.class,
				"css/bootstrap-datepicker3.css");
	}

	@Override
	public List<HeaderItem> getDependencies() {
		return ImmutableList.of(Heinlein.createCssHeaderItem());
	}

	/**
	 * Get a singleton instance of this resource
	 * @return A ResourceReference
	 */
	public static BSDatePickerCSSResourceReference get() {
		return instance;
	}
}
