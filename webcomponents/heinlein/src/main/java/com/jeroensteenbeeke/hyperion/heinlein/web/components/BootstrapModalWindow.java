package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.danekja.java.misc.serializable.SerializableRunnable;
import org.danekja.java.util.function.serializable.SerializableBiConsumer;
import org.danekja.java.util.function.serializable.SerializableConsumer;
import org.danekja.java.util.function.serializable.SerializableFunction;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Bootstrap 4 Modal window. Content determined through markup inheritance
 */
public class BootstrapModalWindow extends Panel {
	private final List<ModalWindowLinkReference> linkSuppliers = new ArrayList<>();

	private TransparentWebMarkupContainer dialog;
	private TransparentWebMarkupContainer document;
	private TransparentWebMarkupContainer content;
	private TransparentWebMarkupContainer header;
	private TransparentWebMarkupContainer body;
	private TransparentWebMarkupContainer footer;

	private AtomicInteger unfinishedButtons = new AtomicInteger(0);

	/**
	 * Creates a new ModalWindow
	 * @param id The Wicket ID
	 * @param title The window title
	 */
	protected BootstrapModalWindow(String id, String title) {
		super(id);
		initComponents(title);
	}

	/**
	 * Creates a new ModalWindow
	 * @param id The Wicket ID
	 * @param model The model of the window
	 * @param title The window title
	 */
	protected BootstrapModalWindow(String id, IModel<?> model, String title) {
		super(id, model);
		initComponents(title);
	}


	private void initComponents(String title) {
		add(new Label("title", title));
		add(dialog = new TransparentWebMarkupContainer("dialog"));
		dialog.add(document = new TransparentWebMarkupContainer("document"));
		document.add(content = new TransparentWebMarkupContainer("content"));
		content.add(header = new TransparentWebMarkupContainer("header"));
		content.add(body = new TransparentWebMarkupContainer("body"));
		content.add(footer = new TransparentWebMarkupContainer("footer"));
	}

	/**
	 * Get the HTML element with the dialog role
	 * @return A WebMarkupContainer
	 */
	protected final WebMarkupContainer getDialog() {
		return dialog;
	}

	/**
	 * Get the HTML element with the document role
	 * @return A WebMarkupContainer
	 */
	protected final WebMarkupContainer getDocument() {
		return document;
	}

	/**
	 * Get the HTML element with the content role
	 * @return A WebMarkupContainer
	 */
	protected final WebMarkupContainer getContent() {
		return content;
	}

	/**
	 * Get the modal header component
	 * @return A WebMarkupContainer
	 */
	protected TransparentWebMarkupContainer getHeader() {
		return header;
	}

	/**
	 * Get the modal body component
	 * @return A WebMarkupContainer
	 */
	protected TransparentWebMarkupContainer getBody() {
		return body;
	}

	/**
	 * Get the modal footer component
	 * @return A WebMarkupContainer
	 */
	protected TransparentWebMarkupContainer getFooter() {
		return footer;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		if (this.unfinishedButtons.get() != 0) {
			throw new IllegalStateException("There are "+ unfinishedButtons.get() +" unfinished button creation chains.");
		}

		footer.add(new ListView<ModalWindowLinkReference>("buttons", linkSuppliers) {

			@Override
			protected void populateItem(ListItem<ModalWindowLinkReference> item) {
				ModalWindowLinkReference ref = item.getModelObject();

				AbstractLink l = ref.supplier.apply("button");
				l.add(AttributeModifier.append("class", " btn-".concat(ref.buttonType.name().toLowerCase())));
				item.add(l);
			}
		});

		footer.add(new WebMarkupContainer("close").setVisibilityAllowed(isCloseEnabled()));
	}

	/**
	 * Determines whether or not the bottom close button should be shown
	 * @return {@code true} if it should be shown, {@code false} otherwise
	 */
	public boolean isCloseEnabled() {
		return true;
	}

	/**
	 * Creates a builder for an Ajax submit button
	 * @return A builder for the form
	 */
	public final SetButtonForm addSubmitButton() {
		unfinishedButtons.incrementAndGet();

		return form -> internalAddButton(id -> new SubmitLink(id, form));
	}

	/**
	 * Creates a builder for an Ajax submit button
	 * @return A builder for the form
	 */
	public final SetButtonForm addAjaxSubmitButton() {
		unfinishedButtons.incrementAndGet();

		return form -> internalAddButton(id -> new AjaxSubmitLink(id, form) {
		});
	}

	/**
	 * Creates a builder for an Ajax submit button
	 * @param onSubmit The callback to call when the form is submitted
	 * @return A builder for the form
	 */
	public final SetButtonForm addAjaxSubmitButton(@Nonnull SerializableConsumer<AjaxRequestTarget> onSubmit) {
		unfinishedButtons.incrementAndGet();

		return form -> internalAddButton(id -> new AjaxSubmitLink(id, form) {
			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				onSubmit.accept(target);
			}
		});
	}

	/**
	 * Creates a builder for an Ajax button
	 * @param onClick Handler for when the button is clicked
	 * @param <T> The type of model for the ajax button
	 * @return A builder to set the button model
	 */
	public final <T> SetButtonModel<T> addAjaxButton(@Nonnull SerializableBiConsumer<AjaxRequestTarget,T> onClick) {
		unfinishedButtons.incrementAndGet();

		return model -> internalAddButton(id -> new AjaxLink<T>(id, model) {

			@Override
			public void onClick(AjaxRequestTarget target) {
				onClick.accept(target, getModelObject());
			}
		});
	}

	/**
	 * Creates a builder for an Ajax button without a model
	 * @param onClick Handler for when the button is clicked
	 * @param <T> The type of model for the ajax button
	 * @return A builder to set the button type
	 */
	public final <T> SetButtonType addAjaxButton(@Nonnull SerializableConsumer<AjaxRequestTarget> onClick) {
		unfinishedButtons.incrementAndGet();

		return internalAddButton(id -> new AjaxLink<T>(id) {

			@Override
			public void onClick(AjaxRequestTarget target) {
				onClick.accept(target);
			}
		});
	}

	/**
	 * Creates a builder for a regular button
	 * @param onClick Handler for when the button is clicked
	 * @param <T> The type of model for the ajax button
	 * @return A builder to set the button model
	 */
	public final <T> SetButtonModel<T> addButton(@Nonnull SerializableConsumer<T> onClick) {
		unfinishedButtons.incrementAndGet();

		return model -> internalAddButton(id -> new Link<T>(id, model) {

			@Override
			public void onClick() {
				onClick.accept(getModelObject());
			}
		});
	}

	/**
	 * Creates a builder for an Ajax button without a model
	 * @param onClick Handler for when the button is clicked
	 * @param <T> The type of model for the ajax button
	 * @return A builder to set the button type
	 */
	public final <T> SetButtonType addButton(@Nonnull SerializableRunnable onClick) {
		unfinishedButtons.incrementAndGet();

		return internalAddButton(id -> new Link<T>(id) {

			@Override
			public void onClick() {
				onClick.run();
			}
		});
	}

	/**
	 * Creates a builder for a new button
	 * @param linkSupplier Function that maps a Wicket ID to a link component
	 * @return A builder to specify the button type
	 */
	public final SetButtonType addCustomButton(@Nonnull SerializableFunction<String,AbstractLink> linkSupplier) {
		unfinishedButtons.incrementAndGet();

		return internalAddButton(linkSupplier);
	}

	private SetButtonType internalAddButton(@Nonnull SerializableFunction<String, AbstractLink> linkSupplier) {
		return type -> label -> {
			SerializableFunction<AbstractLink,AbstractLink> setLabel = (AbstractLink link) -> {
				link.setBody(Model.of(label));
				return link;
			};

			linkSuppliers.add(new ModalWindowLinkReference(setLabel.compose(linkSupplier), type));

			unfinishedButtons.decrementAndGet();
		};
	}

	/**
	 * Builder stage for adding a form to a submit link
	 */
	protected interface SetButtonForm extends SerializableFunction<Form<?>, SetButtonType> {
		/**
		 * Sets the form for the button
		 * @param form The form to set
		 * @return A builder for setting the button type
		 */
		@Nonnull
		default SetButtonType forForm(@Nonnull Form<?> form) {
			return apply(form);
		}
	}

	/**
	 * Builder stage for creating a button that has a model (i.e. most of them)
	 * @param <T> The type of the model object
	 */
	protected interface SetButtonModel<T> extends SerializableFunction<IModel<T>, SetButtonType> {
		/**
		 * Sets the model of the button
		 * @param model The model of the button
		 * @return The next button stage
		 */
		@Nonnull
		default SetButtonType withModel(@Nonnull IModel<T> model) {
			return apply(model);
		}
	}

	/**
	 * Builder stage for linking a button type to link
	 */
	protected interface SetButtonType extends SerializableFunction<ButtonType, SetButtonLabel> {
		/**
		 * Sets the type of the button
		 * @param type The type of the button
		 * @return The next button stage
		 */
		@Nonnull
		default SetButtonLabel ofType(@Nonnull ButtonType type) {
			return apply(type);
		}
	}

	/**
	 * Builder stage for setting button label
	 */
	protected interface SetButtonLabel extends SerializableConsumer<String> {
		/**
		 * Complete the building process, setting the link label
		 * @param label The label to set
		 */
		default void withLabel(@Nonnull String label) {
			accept(label);
		}
	}

	private static class ModalWindowLinkReference implements Serializable {
		private final SerializableFunction<String,AbstractLink>  supplier;

		private final ButtonType buttonType;

		private ModalWindowLinkReference(SerializableFunction<String,AbstractLink>  supplier, ButtonType buttonType) {
			this.supplier = supplier;
			this.buttonType = buttonType;
		}
	}
}
