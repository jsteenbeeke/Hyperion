/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

/**
 * FeedbackPanel implementation that uses Bootstrap-specific styling
 */
public class BootstrapFeedbackPanel extends FeedbackPanel {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new feedback panel with the given ID
	 *
	 * @param id the ID of the component, corresponding to the HTML element
	 */
	public BootstrapFeedbackPanel(String id) {
		super(id);
	}

	/**
	 * Creates a new feedback panel with the given ID
	 *
	 * @param id     the ID of the component, corresponding to the HTML element
	 * @param filter A filter to use to limit the number of messages shown
	 */
	public BootstrapFeedbackPanel(String id, IFeedbackMessageFilter filter) {
		super(id, filter);
	}

	@Override
	protected String getCSSClass(FeedbackMessage message) {
		switch (message.getLevel()) {
			case FeedbackMessage.FATAL:
			case FeedbackMessage.ERROR:
				return "alert alert-danger";
			case FeedbackMessage.WARNING:
				return "alert alert-warning";
			case FeedbackMessage.INFO:
				return "alert alert-info";
			case FeedbackMessage.SUCCESS:
				return "alert alert-success";
		}

		return super.getCSSClass(message);

	}
}
