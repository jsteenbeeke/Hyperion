/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web;

import com.jeroensteenbeeke.hyperion.icons.IconInitializer;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.resource.JQueryResourceReference;

import javax.annotation.Nonnull;
import java.util.Arrays;

/**
 * Initializer class for Wicket applications. Sets all necessary settings to properly make a Bootstrap-based page
 */
public class Heinlein {
	private static String mainCssFile = null;
	
	private Heinlein() {
		
	}

	/**
	 * Initializes the given application to use Heinlein-powered Bootstrap support
	 * @param application The wicket application to configure
	 * @param mainCssFile The context-relative URL of the CSS file to use. This should probably be compiled using LESS
	 * @param iconInitializers Zero or more icon initializer classes to
	 *                                               configure webfonts and such
	 */
	public static void init(@Nonnull WebApplication application, @Nonnull String mainCssFile,
							IconInitializer... iconInitializers) {
		Heinlein.mainCssFile = mainCssFile;
		application.getHeaderContributorListeners().add(new HeinleinHeaderContributor(mainCssFile));
		application.getMarkupSettings().setStripWicketTags(true);

		application.getJavaScriptLibrarySettings().setJQueryReference(JQueryResourceReference.getV3());

		Arrays.stream(iconInitializers).forEach(i -> i.initialize(application));
	}

	/**
	 * Create a CssHeaderItem for the configured main CSS file. This method assumes that the {@code init()} method has already
	 * been called
	 * @return A CssHeaderItem for the main CSS file
	 * @throws IllegalStateException If {@code init()} has not yet been called
	 */
	public static CssHeaderItem createCssHeaderItem() {
		if (mainCssFile == null) {
			throw new IllegalStateException("Heinlein not initialized!");
		}
		
		return CssHeaderItem.forUrl(mainCssFile);
	}
}
