/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import java.text.DateFormat;
import java.util.Date;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.joda.time.LocalDate;

import com.jeroensteenbeeke.hyperion.heinlein.web.resources.BSDatePickerJavaScriptReference;

/**
 * Bootstrap-backed DatePicker component that uses a TextField as base
 */
public class BSDatePickerField extends TextField<Date> {
	private static class NormalizedDateModel extends
			LoadableDetachableModel<Date> {

		private static final long serialVersionUID = 1L;

		private final IModel<Date> refModel;

		private NormalizedDateModel(IModel<Date> refModel) {
			super();
			this.refModel = refModel;
		}

		@Override
		protected Date load() {
			Date original = refModel.getObject();

			if (original != null) {
				LocalDate date = LocalDate.fromDateFields(original);

				return date.toDate();
			}

			return null;
		}

		@Override
		protected void onDetach() {
			super.onDetach();
			refModel.detach();
		}
	}

	private static final long serialVersionUID = 1L;

	/**
	 * Create a new BSDatePickerField
	 * @param id The id of the textfield
	 */
	public BSDatePickerField(String id) {
		super(id, Date.class);
	}

	/**
	 * Create a new BSDatePickerField
	 * @param id The id of the textfield
	 * @param model The model containing the date to display
	 */
	public BSDatePickerField(String id, IModel<Date> model) {
		super(id, normalize(model), Date.class);
	}

	private static IModel<Date> normalize(IModel<Date> model) {
		return new NormalizedDateModel(model);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		DateFormat localeFormat = DateFormat.getDateInstance(DateFormat.SHORT,
				getLocale());

		String intermediate = localeFormat.format(new LocalDate(1983, 12, 31)
				.toDate());
		String format = intermediate.replace("1983", "yyyy")
				.replace("83", "yy").replace("12", "mm").replace("31", "dd");

		add(AttributeModifier.append("data-date-format", format));
		add(AttributeModifier.append("data-provide", "datepicker-inline"));
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		response.render(JavaScriptHeaderItem
				.forReference(BSDatePickerJavaScriptReference.get()));
	}
}
