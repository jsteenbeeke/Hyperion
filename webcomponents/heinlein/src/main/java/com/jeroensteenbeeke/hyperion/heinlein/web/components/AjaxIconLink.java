/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.icons.Icon;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;

import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;

import javax.annotation.Nonnull;

/**
 * Ajax-powered Link that uses a Bootstrap Glyphicon
 * @param <T> The type of the model's object
 */
public abstract class AjaxIconLink<T> extends TypedPanel<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new AjaxIconLink
	 * @param id The id of the icon
	 * @param model The model containing the link's object
	 * @param icon The icon to use
	 */
	public AjaxIconLink(@Nonnull String id, @Nonnull IModel<T> model, @Nonnull Icon icon) {
		super(id, model);

		AjaxLink<T> iconLink = new AjaxLink<T>("link", model) {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				AjaxIconLink.this.onClick(target);
			}
		};

		iconLink.add(new WebMarkupContainer("icon").add(AttributeModifier
				.replace("class", icon.getCssClasses())));

		add(iconLink);
	}

	/**
	 * Sets the title (hover-text) of the icon
	 * @param title The title of the icon
	 * @return The current link
	 */
	public AjaxIconLink<T> setTitle(String title) {
		get("link").get("icon").add(AttributeModifier.replace("title", title));
		return this;
	}

	/**
	 * Listener method invoked on the ajax request generated when the user clicks the link
	 *
	 * @param target the request target of the click action
	 */
	public abstract void onClick(AjaxRequestTarget target);
}
