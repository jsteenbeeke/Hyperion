package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.webcomponents.core.HTMLWrapper;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigationLink;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigationLink;

/**
 * Bootstrap-specific implementation of PagingNavigation to properly render item links
 */
public class BootstrapPagingNavigation extends PagingNavigation implements HTMLWrapper {
	private static final long serialVersionUID = 2636778689231051644L;

	/**
	 * Create a new PagingNavigation with the given ID, for the given pageable
	 * @param id The wicket ID
	 * @param pageable The pageable
	 */
	public BootstrapPagingNavigation(String id, IPageable pageable) {
		super(id, pageable);
	}

	/**
	 * Create a new PagingNavigation with the given ID, for the given pageable
	 * @param id The wicket ID
	 * @param pageable The pageable
	 * @param labelProvider Used to generate labels
	 */
	public BootstrapPagingNavigation(String id, IPageable pageable, IPagingLabelProvider labelProvider) {
		super(id, pageable, labelProvider);
	}

	@Override
	protected AbstractLink newPagingNavigationLink(String id, IPageable pageable, long pageIndex) {
		PagingNavigationLink<Void> link = new PagingNavigationLink<Void>(id, pageable, pageIndex) {
			private static final long serialVersionUID = -334212951286841345L;

			@Override
			protected void onRender() {
				wrapRenderedHTML("<li class=\"page-item\">", () -> super.onRender(), "</li>");
			}
		};
		link.add(AttributeModifier.append("class", "page-link"));
		return link;
	}
}
