package com.jeroensteenbeeke.hyperion.cousteau.web;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.icons.Icon;
import com.jeroensteenbeeke.hyperion.icons.fontawesome.FontAwesome;
import org.apache.wicket.Page;

import javax.annotation.Nonnull;

public class BaseAuthenticatedTestPage<T extends DomainObject & UserOwned<U>, U extends IUser>
		extends AbstractAuthenticatedPage<T,U> {
	protected BaseAuthenticatedTestPage(String title, U user) {
		super(title, user);
	}

	protected BaseAuthenticatedTestPage(String title, T object) {
		super(title, object);
	}

	@Override
	protected OverviewContext getOverviewContext() {
		return new TestOverviewContext();
	}

	@Override
	public Class<? extends Page> getLogoutPage() {
		return TestLogoutPage.class;
	}

	protected class TestOverviewContext implements OverviewContext {

		@Nonnull
		@Override
		public Icon getIcon() {
			return FontAwesome.home;
		}

		@Nonnull
		@Override
		public String getLabel() {
			return "Overview";
		}

		@Nonnull
		@Override
		public Page getPage() {
			return BaseAuthenticatedTestPage.this;
		}

		@Nonnull
		@Override
		public OverviewContext setIcon(Icon icon) {
			return this;
		}

		@Nonnull
		@Override
		public OverviewContext setLabel(String label) {
			return this;
		}

		@Override
		public void detach() {

		}
	}
}
