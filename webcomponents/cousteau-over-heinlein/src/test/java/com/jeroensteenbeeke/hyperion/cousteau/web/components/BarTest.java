package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.util.tester.WicketTester;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.IsEqual.equalTo;

public class BarTest {
	@Rule
	public ExpectedException except = ExpectedException.none();

	@Test
	public void testBars() {
		WicketTester tester = new WicketTester();

		tester.startPage(BarTestPage.class);
		tester.assertRenderedPage(BarTestPage.class);

		tester.assertContains(">!!TOP!!</a>");
		tester.assertContainsNot(">TOP</a>");
		tester.assertContains("BottomButton");
	}

	@Test
	public void testFailure() {
		final String expectedMessage = "initComponents not called on com.jeroensteenbeeke.hyperion.cousteau.web.components.HeaderBar";

		except.expectCause(
				new TypeSafeMatcher<Throwable>() {
					@Override
					public void describeTo(Description description) {
						description.appendText("WicketRuntimeException caused by IllegalStateException with message ");
						description.appendText(expectedMessage);
					}

					@Override
					protected boolean matchesSafely(Throwable item) {
						return item instanceof IllegalStateException
								&& item.getMessage().equals(expectedMessage);
					}
				}
		);

		WicketTester tester = new WicketTester();

		tester.startPage(FailingBarTestPage.class);
	}
}
