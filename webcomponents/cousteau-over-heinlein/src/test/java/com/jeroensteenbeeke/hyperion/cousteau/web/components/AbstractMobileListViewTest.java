package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class AbstractMobileListViewTest {
	@Test
	public void testListView() {
		WicketTester tester = new WicketTester();

		tester.startPage(new AbstractMobileListViewTestPage());
		tester.assertRenderedPage(AbstractMobileListViewTestPage.class);

		tester.assertComponent("view", AbstractMobileListView.class);
		tester.assertComponent("view:container", WebMarkupContainer.class);
		tester.assertComponent("view:container:title", Label.class);
		tester.assertComponent("view:container:list", ListView.class);
		tester.assertComponent("view:container:list:0:border", Border.class);
		tester.assertComponent("view:container:list:0:border:border_body",
				Border.BorderBodyContainer.class);
		tester.assertInvisible("view:container:list:0:border:border_body:count");
		tester.assertComponent("view:container:list:0:border:border_body:label",
				Label.class);
		tester.assertInvisible("view:container:list:0:border:border_body:subtitle");
		// count, label, subtitle

		tester.assertComponent("view:container:list:1:border", Border.class);
		tester.assertComponent("view:container:list:1:border:border_body",
				Border.BorderBodyContainer.class);
		tester.assertComponent("view:container:list:2:border", Border.class);
		tester.assertComponent("view:container:list:2:border:border_body",
				Border.BorderBodyContainer.class);


	}
}
