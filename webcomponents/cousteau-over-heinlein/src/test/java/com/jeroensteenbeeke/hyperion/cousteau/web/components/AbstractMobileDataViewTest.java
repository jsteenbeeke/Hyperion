package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class AbstractMobileDataViewTest {
	@Test
	public void testDataView() {
		WicketTester tester = new WicketTester();

		tester.startPage(new AbstractMobileDataViewTestPage());
		tester.assertRenderedPage(AbstractMobileDataViewTestPage.class);

		tester.assertComponent("view", AbstractMobileDataView.class);
		tester.assertComponent("view:container", WebMarkupContainer.class);
		tester.assertComponent("view:container:title", Label.class);
		tester.assertComponent("view:container:list", DataView.class);
		tester.assertComponent("view:container:list:1:border", Border.class);
		tester.assertComponent("view:container:list:1:border:border_body",
				Border.BorderBodyContainer.class);
		tester.assertInvisible("view:container:list:1:border:border_body:count");
		tester.assertComponent("view:container:list:1:border:border_body:label",
				Label.class);
		tester.assertInvisible("view:container:list:1:border:border_body:subtitle");
		// count, label, subtitle

		tester.assertComponent("view:container:list:2:border", Border.class);
		tester.assertComponent("view:container:list:2:border:border_body",
				Border.BorderBodyContainer.class);
		tester.assertComponent("view:container:list:3:border", Border.class);
		tester.assertComponent("view:container:list:3:border:border_body",
				Border.BorderBodyContainer.class);


	}
}
