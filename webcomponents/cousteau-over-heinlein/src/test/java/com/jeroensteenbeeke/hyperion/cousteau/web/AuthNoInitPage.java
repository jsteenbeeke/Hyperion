package com.jeroensteenbeeke.hyperion.cousteau.web;

public class AuthNoInitPage extends BaseAuthenticatedTestPage {
	public AuthNoInitPage(Unit unit) {
		super("Unit View", unit);
	}
}
