package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class FactDisplayTest {
	@Test
	public void testFactDisplay() {
		WicketTester tester = new WicketTester();

		tester.startPage(new FactDisplayPage());
		tester.assertRenderedPage(FactDisplayPage.class);
	}
}
