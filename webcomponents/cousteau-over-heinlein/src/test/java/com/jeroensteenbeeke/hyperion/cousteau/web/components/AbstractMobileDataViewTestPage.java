package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import com.google.common.collect.ImmutableList;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.repeater.data.ListDataProvider;

import javax.annotation.Nonnull;
import java.util.List;

public class AbstractMobileDataViewTestPage extends WebPage {
	public AbstractMobileDataViewTestPage() {
		List<String> testList = ImmutableList.of("A", "B", "C");

		add(new AbstractMobileDataView<String>("view", "Letters", new ListDataProvider<>
				(testList)) {
			@Nonnull
			@Override
			public String getLabel(String object) {
				return object;
			}
		});
	}
}
