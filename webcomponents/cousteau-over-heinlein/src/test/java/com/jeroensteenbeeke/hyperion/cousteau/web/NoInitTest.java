package com.jeroensteenbeeke.hyperion.cousteau.web;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class NoInitTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testPageWithoutInitComponents() {
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("You must call initComponents() at the end of your constructor");

		WicketTester tester = new WicketTester();
		tester.startPage(NoInitPage.class);

	}

	@Test
	public void testAuthenticatedPageWithoutInitComponents() {
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("You must call initComponents() at the end of your constructor");

		WicketTester tester = new WicketTester();
		tester.startPage(new AuthNoInitPage(new Unit()));

	}
}
