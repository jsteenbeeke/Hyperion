package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import com.google.common.collect.ImmutableList;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.util.ListModel;

import java.util.List;

public class AbstractMobileListViewTestPage extends WebPage {
	public AbstractMobileListViewTestPage() {
		List<String> testList = ImmutableList.of("A", "B", "C");

		add(new AbstractMobileListView<String>("view", "Letters",
				new ListModel<>(testList)) {
			@Override
			public String getLabel(String object) {
				return object;
			}
		});
	}
}
