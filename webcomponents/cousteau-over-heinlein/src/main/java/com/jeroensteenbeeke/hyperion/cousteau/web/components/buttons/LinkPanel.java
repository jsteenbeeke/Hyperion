/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons;

import com.jeroensteenbeeke.hyperion.icons.Icon;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.panel.Panel;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Panel that contains a button/link, possibly with an icon
 */
public class LinkPanel extends Panel {
	private static final long serialVersionUID = 1L;
	
	static final String LINK_ID = "link";

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param link The link to place
	 * @param button The button we're rendering
	 */
	LinkPanel(@Nonnull String id, @Nonnull AbstractLink link, @Nonnull AbstractButton button) {
		super(id);
		setRenderBodyOnly(true);
		add(link);
		
		link.add(new Label("label", button.getLabel()));
		
		WebMarkupContainer iconHolder = new WebMarkupContainer("icon");
		Icon icon = button.getIcon();
		if (icon != null) {
			iconHolder.add(AttributeModifier.replace("class", icon.getCssClasses()));
		} else {
			iconHolder.setVisible(false);
		}
		
		link.add(iconHolder);
		
	}
}
