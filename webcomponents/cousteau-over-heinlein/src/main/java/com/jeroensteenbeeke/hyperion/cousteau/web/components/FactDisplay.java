/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import java.io.Serializable;
import java.util.Arrays;

import io.vavr.control.Option;
import org.apache.wicket.model.util.ListModel;

import com.jeroensteenbeeke.hyperion.cousteau.web.components.FactDisplay.Fact;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * A list of facts
 */
public class FactDisplay extends AbstractMobileListView<Fact> {
	private static final long serialVersionUID = 1L;

	/**
	 * A fact. Essentially a pair of a label and count
	 */
	public static final class Fact implements Serializable {
		private static final long serialVersionUID = 1L;

		private final String label;

		private final Serializable count;

		/**
		 * Constructor
		 * @param label The label
		 * @param count The count
		 */
		public Fact(@Nonnull String label, @Nullable Serializable count) {
			this.label = label;
			this.count = count;
		}
	}

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param title The title of the panel
	 * @param facts The facts to display
	 */
	public FactDisplay(String id, String title, Fact... facts) {
		super(id, title, new ListModel<>(Arrays.asList(facts)));
	}

	@Nonnull
	@Override
	public String getLabel(Fact object) {
		return object.label;
	}

	@Nonnull
	@Override
	public Option<Serializable> getCount(Fact object) {
		return Option.of(object.count);
	}
}
