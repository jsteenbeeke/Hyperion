/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import java.io.Serializable;
import java.util.List;

import io.vavr.control.Option;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.jeroensteenbeeke.hyperion.cousteau.web.MobileThemeManager;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.listview.EmptyBorder;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.listview.LinkBorder;

import javax.annotation.Nonnull;

/**
 * ListView panel with default mobile styling
 * @param <T> The type of object displayed within the listview
 */
public abstract class AbstractMobileListView<T> extends AbstractMobileRepeater {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
 	 * @param id The wicket:id
	 * @param title The title of the panel
	 * @param listModel The model containing the list
	 */
	public AbstractMobileListView(String id, String title,
			IModel<List<T>> listModel) {
		super(id);

		WebMarkupContainer container = new RepeaterContainer("container");
		add(container);

		Label titleLabel = new TitleLabel("title", title);
		titleLabel.setEscapeModelStrings(false);

		if (MobileThemeManager.getTheme() != null) {
			container.add(AttributeModifier.replace("data-dividertheme",
					MobileThemeManager.getTheme()));
		}

		container.add(titleLabel);

		container.add(new ListView<>("list", listModel) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<T> item) {
				T object = item.getModelObject();
				Option<Serializable> count = getCount(object);

				Border border = getBorderFor("border", object);

				if (border instanceof LinkBorder) {
					item.setRenderBodyOnly(true);
					setContainsLinks(true);
				}

				border.getBodyContainer().add(
					new Label("label", getLabel(object)).setRenderBodyOnly(
						true).setEscapeModelStrings(
						shouldEscapeModelStrings()));
				Option<String> subtitle = getSubtitle(object);
				border.getBodyContainer().add(
					new Label("subtitle", subtitle.getOrElse("-")).setVisible(
						subtitle.isDefined()).setEscapeModelStrings(false));
				border.getBodyContainer().add(
					new Label("count", Model.of(count.getOrElse("-")))
						.setVisible(count.isDefined())
						.setEscapeModelStrings(false));
				item.add(border);
				item.setVisible(isRowVisible(object));

			}
		});
	}

	/**
	 * Whether or not model String should be escaped (to avoid HTML injection). Defaults to {@code true}
	 * @return {@code true} if the model Strings should be escaped, {@code false} otherwise
	 */
	protected boolean shouldEscapeModelStrings() {
		return true;
	}
	/**
	 * Obtains a label for the given object
	 * @param object The object
	 * @return A label
	 */
	@Nonnull
	public abstract String getLabel(T object);
	/**
	 * Returns the count for the given object, which is displayed as a badge. Defaults to empty
	 * @param object The object
	 * @return Optionally a "count"
	 */
	@Nonnull
	public Option<Serializable> getCount(T object) {
		return Option.none();
	}

	/**
	 * Returns a subtitle for the given object. Defaults to empty
	 * @param object The object
	 * @return Optionally a subtitle
	 */
	@Nonnull
	public Option<String> getSubtitle(T object) {
		return Option.none();
	}

	/**
	 * Returns a border for the given object
	 * @param id The wicket:id for the border
	 * @param object The object
	 * @return A Border
	 */
	@Nonnull
	public Border getBorderFor(String id, T object) {
		return new EmptyBorder(id);
	}

	/**
	 * Determines if the row for the given object should be visible
	 * @param object The object
	 * @return {@code true} if visible. {@code false} otherwise
	 */
	public boolean isRowVisible(T object) {
		return true;
	}
}
