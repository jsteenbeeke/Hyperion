/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import java.util.List;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;

/**
 * ListView for entities. Essentially an AbstractMobileListView with convenience constructor
 * @param <T> The type of entity
 */
public abstract class EntityMobileListView<T extends DomainObject> extends
		AbstractMobileListView<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param title The title of the panel
	 * @param list The list of entities to render
	 */
	public EntityMobileListView(String id, String title, List<T> list) {
		super(id, title, EntityEncapsulator.createListModel(list));
	}
}
