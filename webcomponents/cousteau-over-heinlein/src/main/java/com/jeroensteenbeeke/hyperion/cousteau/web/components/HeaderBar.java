/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons.BarButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Bar displayed at the top of the page
 */
public class HeaderBar extends AbstractBar {
	private static final long serialVersionUID = 1L;

	private static final String TOPLEFT_ID = "topleft";

	private static final String TOPRIGHT_ID = "topright";

	private String title;

	private BarButton topLeft = null;

	private BarButton topRight = null;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param title The title displayed in the center of the bar
	 */
	public HeaderBar(String id, String title) {
		super(id);

		this.title = title;
	}

	/**
	 * Updates the title
	 * @param title The title
	 */
	public void setTitle(@Nonnull String title) {
		this.title = title;
	}

	/**
	 * Sets the button to display on the top left
	 * @param topLeft The button
	 */
	public void setTopLeft(@Nullable BarButton topLeft) {
		this.topLeft = topLeft;
	}

	/**
	 * Sets the button to display on the top right
	 * @param topRight The button
	 */
	public void setTopRight(@Nullable BarButton topRight) {
		this.topRight = topRight;
	}

	@Override
	protected void onDetach() {
		super.onDetach();
		if (topLeft != null)
			topLeft.detach();
		if (topRight != null)
			topRight.detach();
	}

	@Override
	public final void initComponents() {
		super.initComponents();

		add(new Label("title", title).setVisible(title != null));

		add(topLeft != null ? render(TOPLEFT_ID, topLeft)
				: new WebMarkupContainer(TOPLEFT_ID).setVisible(false));
		add(topRight != null ? render(TOPRIGHT_ID, topRight)
				: new WebMarkupContainer(TOPRIGHT_ID).setVisible(false));
	}
}
