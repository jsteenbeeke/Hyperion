/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons;

import com.jeroensteenbeeke.hyperion.icons.Icon;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.model.IModel;

/**
 * Base class for a button
 * @param <T> The implementing button type
 */
public abstract class AbstractButton<T extends AbstractButton<T>> implements BarButton {

	private static final long serialVersionUID = 1L;

	private IModel<String> labelModel;

	private Icon icon = null;

	/**
	 * Constructor
	 * @param labelModel Model containing the button's label
	 */
	protected AbstractButton(IModel<String> labelModel) {
		this.labelModel = labelModel;
	}

	@Override
	public final void detach() {
		labelModel.detach();
		onDetach();
	}

	/**
	 * Method called when the button is detached
	 */
	protected void onDetach() {

	}

	/**
	 * Set the icon for this button
	 * @param icon The icon
	 * @return This button
	 */
	@SuppressWarnings("unchecked")
	public final T withIcon(Icon icon) {
		this.icon = icon;
		return (T) this;
	}

	@Override
	public final IModel<String> getLabel() {
		return labelModel;
	}

	/**
	 * Get this button's icon
	 * @return The icon
	 */
	public final Icon getIcon() {
		return icon;
	}

	@Override
	public LinkPanel createLinkPanel(String id) {
		AbstractLink link = makeLink(LinkPanel.LINK_ID);
		
		return new LinkPanel(id, link, this);
	}

	/**
	 * Create a link for this button
	 * @param id The wicket:id
	 * @return A subclass of AbstractLink
	 */
	public abstract AbstractLink makeLink(String id);

}
