/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import java.io.Serializable;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Base class for repeaters in mobile style
 */
public abstract class AbstractMobileRepeater extends Panel {
	private static final long serialVersionUID = 1L;

	private static class TagType {
		public static final String UL = "ul";
		
		public static final String LI = "li";
		
		public static final String DIV = "div";
		
		public static final String A = "a";
	}

	private boolean containsLinks = false;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model for the repeater
	 */
	protected AbstractMobileRepeater(String id, IModel<?> model) {
		super(id, model);
	}

	/**
	 * Constructor
	 * @param id the wicket:id
	 */
	protected AbstractMobileRepeater(String id) {
		super(id);
	}

	/**
	 * Indicates that the repeater contains links
	 * @param containsLinks {@code true} if it contains links. {@code false} otherwise
	 */
	protected void setContainsLinks(boolean containsLinks) {
		this.containsLinks = containsLinks;
	}

	/**
	 * Label specialization for panel title
	 */
	protected class TitleLabel extends Label {
		private static final long serialVersionUID = 1L;

		/**
		 * Constructor
		 * @param id The wicket:id
		 * @param label The label
		 */
		public TitleLabel(String id, Serializable label) {
			super(id, label);
		}

		@Override
		protected void onComponentTag(ComponentTag tag) {
			super.onComponentTag(tag);

			tag.setName(containsLinks ? TagType.A : TagType.LI);
			tag.setNamespace(null);
		}
	}

	/**
	 * Container for the repeater
	 */
	protected class RepeaterContainer extends WebMarkupContainer {
		private static final long serialVersionUID = 1L;

		/**
		 * Constructor
		 * @param id The wicket:id
		 */
		public RepeaterContainer(String id) {
			super(id);
		}

		@Override
		protected void onComponentTag(ComponentTag tag) {
			super.onComponentTag(tag);

			tag.setName(containsLinks ? TagType.DIV : TagType.UL);
			tag.setNamespace(null);
		}
	}
}
