/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import java.io.Serializable;

import io.vavr.control.Option;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.jeroensteenbeeke.hyperion.cousteau.web.MobileThemeManager;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.listview.EmptyBorder;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.listview.LinkBorder;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

/**
 * DataView panel with default mobile styling
 * @param <T> The type of object displayed within the dataview
 */
public abstract class AbstractMobileDataView<T> extends AbstractMobileRepeater {

	private static final long serialVersionUID = 1L;

	private long entryCount;

	private DataView<T> dataview;

	private Link<T> left;

	private Link<T> right;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param title The title of the repeater
	 * @param provider The dataprovider
	 */
	public AbstractMobileDataView(String id, String title,
			IDataProvider<T> provider) {
		super(id);

		this.entryCount = provider.size();

		WebMarkupContainer container = new RepeaterContainer("container");
		add(container);

		Label titleLabel = new Label("title", title);
		titleLabel.setEscapeModelStrings(false);

		if (MobileThemeManager.getTheme() != null) {
			container.add(AttributeModifier.replace("data-dividertheme",
					MobileThemeManager.getTheme()));
		}

		container.add(titleLabel);

		dataview = new DataView<>("list", provider) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<T> item) {
				T object = item.getModelObject();
				Option<Serializable> count = getCount(object);

				Border border = getBorderFor("border", object);
				
				if (border instanceof LinkBorder) {
					item.setRenderBodyOnly(true);
					setContainsLinks(true);
				}

				border.getBodyContainer().add(
						new Label("label", getLabel(object))
								.setRenderBodyOnly(true));
				Option<String> subtitle = getSubtitle(object);
				border.getBodyContainer().add(
						new Label("subtitle", subtitle.getOrElse("-")).setEscapeModelStrings(
								shouldEscapeModelStrings()).setVisible(
								subtitle.isDefined()));
				border.getBodyContainer().add(
						new Label("count", new Model<>(count.getOrElse("-")))
								.setVisible(count.isDefined()));
				item.add(border);
				item.setVisible(isRowVisible(object));
			}
		};

		container.add(dataview);

		left = new Link<>("left") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				dataview.setCurrentPage(dataview.getCurrentPage() - 1);

			}
		};
		left.setBody(new PageNumberModel(0));
		right = new Link<>("right") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				dataview.setCurrentPage(dataview.getCurrentPage() + 1);

			}
		};
		right.setBody(new PageNumberModel(2));

		add(left);
		add(right);
	}

	/**
	 * Whether or not model String should be escaped (to avoid HTML injection). Defaults to {@code true}
	 * @return {@code true} if the model Strings should be escaped, {@code false} otherwise
	 */
	protected boolean shouldEscapeModelStrings() {
		return true;
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();

		boolean hasMultiplePages = dataview.getItemsPerPage() < entryCount;

		left.setVisible(hasMultiplePages && dataview.getCurrentPage() > 0L);
		right.setVisible(hasMultiplePages
				&& dataview.getCurrentPage() < (dataview.getPageCount() - 1));
	}

	/**
	 * Sets the number of items that may be displayed on a single page
	 * @param items The number of items
	 * @return The current dataview
	 */
	public final AbstractMobileDataView<T> setItemsPerPage(long items) {
		dataview.setItemsPerPage(items);
		return this;
	}

	/**
	 * Obtains a label for the given object
	 * @param object The object
	 * @return A label
	 */
	@Nonnull
	public abstract String getLabel(T object);

	/**
	 * Returns the count for the given object, which is displayed as a badge. Defaults to empty
	 * @param object The object
	 * @return Optionally a "count"
	 */
	@Nonnull
	public Option<Serializable> getCount(T object) {
		return Option.none();
	}

	/**
	 * Returns a subtitle for the given object. Defaults to empty
	 * @param object The object
	 * @return Optionally a subtitle
	 */
	@Nonnull
	public Option<String> getSubtitle(T object) {
		return Option.none();
	}

	/**
	 * Returns a border for the given object
	 * @param id The wicket:id for the border
	 * @param object The object
	 * @return A Border
	 */
	@Nonnull
	public Border getBorderFor(String id, T object) {
		return new EmptyBorder(id);
	}

	/**
	 * Determines if the row for the given object should be visible
	 * @param object The object
	 * @return {@code true} if visible. {@code false} otherwise
	 */
	public boolean isRowVisible(T object) {
		return true;
	}

	/**
	 * Counts the number of entries
	 * @return The number of entries
	 */
	public final long getEntryCount() {
		return entryCount;
	}

	@Override
	protected void onDetach() {
		super.onDetach();

		dataview.detach();
	}

	private class PageNumberModel implements IModel<Long> {
		private static final long serialVersionUID = 1L;

		private final int modifier;

		public PageNumberModel(int modifier) {
			this.modifier = modifier;
		}

		@Override
		public void detach() {

		}

		@Override
		public Long getObject() {
			return dataview.getCurrentPage() + modifier;
		}

		@Override
		public void setObject(Long object) {

		}

	}
}
