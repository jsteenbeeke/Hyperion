/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons.BarButton;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons.LinkPanel;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Base class representing a bar (header or footer)
 */
public abstract class AbstractBar extends Panel {
	private static final long serialVersionUID = 1L;

	private boolean initialized = false;

	/**
	 * Constructor
	 * @param id The wicket:id
	 */
	public AbstractBar(String id) {
		super(id);
	}

	/**
	 * Initialization method that must be called at the end of the constructor inserting this bar
	 */
	protected void initComponents() {
		if (initialized)
			return;

		initialized = true;
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();

		if (!initialized)
			throw new IllegalStateException("initComponents not called on "
					+ getClass().getName());
	}

	/**
	 * Renders a button
	 * @param id The wicket:id of the button
	 * @param button The button to render
	 * @return A LinkPanel
	 */
	protected final LinkPanel render(String id, BarButton button) {
		return button.createLinkPanel(id);
	}
}
