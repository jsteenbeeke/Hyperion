/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web;

import com.jeroensteenbeeke.hyperion.cousteau.web.components.BottomBar;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.HeaderBar;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons.LinkButton;
import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.icons.Icon;
import com.jeroensteenbeeke.hyperion.icons.fontawesome.FontAwesome;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;
import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;

import javax.annotation.Nonnull;

/**
 * Basic page implementation for a Bootstrap-based page for authenticated users
 * @param <T> The type of object displayed on this page
 * @param <U> The type of user that owns this object
 */
public abstract class AbstractAuthenticatedPage<T extends DomainObject & UserOwned<U>, U extends IUser>
		extends BasePage {
	/**
	 * Internal class representing the behavior of the overview/back button in the top left
	 */
	protected interface OverviewContext extends IDetachable {
		/**
		 * The icon to display in the top-left
		 * @return An icon
		 */
		@Nonnull
		Icon getIcon();

		/**
		 * The label next to the icon
		 * @return A label
		 */
		@Nonnull
		String getLabel();

		/**
		 * The page the context applies to
		 * @return The page
		 */
		@Nonnull
		Page getPage();

		/**
		 * Sets the icon
		 * @param icon The icon
		 * @return This context
		 */
		@Nonnull
		OverviewContext setIcon(Icon icon);

		/**
		 * Sets the label
		 * @param label The label
		 * @return This context
		 */
		@Nonnull
		OverviewContext setLabel(String label);
	}

	private static final long serialVersionUID = 1L;

	private IModel<U> userModel;

	private OverviewContext overviewContext;

	/**
	 * Constructor
	 * @param title The title of the page
	 * @param user The user currently logged in
	 */
	public AbstractAuthenticatedPage(String title, U user) {
		super(title);

		this.userModel = EntityEncapsulator.createModel(user);
		setDefaultModel(EntityEncapsulator.createModel((T) null));

	}

	/**
	 * Constructor
	 * @param title The title of the page
	 * @param object The object that is the focus of the page
	 */
	public AbstractAuthenticatedPage(String title, T object) {
		super(title);

		this.userModel = EntityEncapsulator.createModel(object.getOwningUser());
		setDefaultModel(EntityEncapsulator.createModel(object));
	}

	@Override
	public void onInitComponents() {
		super.onInitComponents();

		overviewContext = getOverviewContext();
	}

	/**
	 * Gets the current user
	 * @return The user
	 */
	public final U getUser() {
		userModel.detach();

		return userModel.getObject();
	}

	/**
	 * Gets the object contained in the page's model
	 * @return The object of the model
	 */
	@SuppressWarnings("unchecked")
	public final T getModelObject() {
		getDefaultModel().detach(); // Experimental lazy-init hack fix

		return (T) getDefaultModelObject();
	}

	/**
	 * Return the page's model, respecting the page's generic type
	 * @return The model
	 */
	@SuppressWarnings("unchecked")
	public final IModel<T> getModel() {
		getDefaultModel().detach(); // Experimental lazy-init hack fix

		return (IModel<T>) getDefaultModel();
	}

	@Override
	public void configureBottomBar(BottomBar bar) {
		super.configureBottomBar(bar);

	}

	@Override
	public void configureHeaderBar(HeaderBar bar) {
		super.configureHeaderBar(bar);

		bar.setTopLeft(new LinkButton(getOverviewContext().getLabel()) {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(getOverviewContext().getPage());

			}
		}.withIcon(overviewContext.getIcon()));

		bar.setTopRight(new LinkButton("Logout") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				Session.get().invalidateNow();

				setResponsePage(getLogoutPage());

			}
		}.withIcon(FontAwesome.sign_out_alt));

	}

	/**
	 * Gets the overview context of this page
	 * @return The context
	 */
	protected abstract OverviewContext getOverviewContext();

	/**
	 * Gets the page class to redirect to on logout
	 * @return The logout page class
	 */
	public abstract Class<? extends Page> getLogoutPage();

	@Override
	protected void onDetach() {
		super.onDetach();

		if (userModel != null)
			userModel.detach();
		if (overviewContext != null)
			overviewContext.detach();
	}
}
