/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components.listview;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;

/**
 * Border that wraps contents in a link
 * @param <T> The type of object contained in the link
 */
public abstract class LinkBorder<T> extends Border {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model wrapping the object
	 */
	public LinkBorder(String id, IModel<T> model) {
		this(id, model, null);
	}

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model wrapping the object
	 * @param icon The icon to display
	 */
	public LinkBorder(String id, IModel<T> model, String icon) {
		super(id);

		Link<T> link = new Link<>("link", model) {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				LinkBorder.this.onClick(getModelObject());
			}
		};

		if (icon != null) {
			link.add(AttributeModifier.replace("data-icon", icon));
		}
		link.add(AttributeModifier.replace("rel", "external"));

		addToBorder(link);

	}

	/**
	 * Method called when the link is clicked
	 * @param object The object contained by the link
	 */
	public abstract void onClick(T object);

}
