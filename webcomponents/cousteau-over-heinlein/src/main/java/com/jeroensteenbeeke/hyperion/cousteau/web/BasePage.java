/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web;

import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

import com.jeroensteenbeeke.hyperion.cousteau.web.components.BottomBar;
import com.jeroensteenbeeke.hyperion.cousteau.web.components.HeaderBar;
import com.jeroensteenbeeke.hyperion.heinlein.web.pages.BootstrapBasePage;

/**
 * Basic JQuery-mobile like page. You really shouldn't be using this for new development
 */
public abstract class BasePage extends BootstrapBasePage {
	private static final long serialVersionUID = 1L;

	private HeaderBar header;

	private BottomBar bottom;

	private final String title;

	private boolean initialized = false;

	private WebMarkupContainer container;

	/**
	 * Constructor
	 * @param title The title of the page
	 */
	public BasePage(String title) {
		super(title);
		this.title = title;

		container = new TransparentWebMarkupContainer("parent");

		add(container);

		container.add(new FeedbackPanel("feedback"));
	}

	/**
	 * Sets (overrides) the page title
	 * @param title The page title
	 */
	public final void setPageTitle(String title) {
		super.setTitle(title);
	}

	/**
	 * Method called when initComponents is called
	 */
	public void onInitComponents() {

	}

	/**
	 * Method that should be called at the end of the constructor. Inits basic components such as header and bottom bar
	 */
	public final void initComponents() {
		if (initialized)
			return;

		initialized = true;

		onInitComponents();

		header = new HeaderBar("header", title);
		configureHeaderBar(header);
		container.add(header);
		header.initComponents();

		bottom = new BottomBar("bottom");
		configureBottomBar(bottom);
		container.add(bottom);
		bottom.initComponents();

	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();

		if (!initialized)
			throw new IllegalStateException(
				"You must call initComponents() at the end of your constructor");
	}

	/**
	 * Method that can be overridden to customize the header bar
	 * @param bar The header bar
	 */
	public void configureHeaderBar(HeaderBar bar) {

	}

	/**
	 * Method that can be overridden to customize the bottom bar
	 * @param bar The bottom bar
	 */
	public void configureBottomBar(BottomBar bar) {

	}
}
