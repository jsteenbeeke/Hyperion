package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.annotation.Annotations;
import com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice.NaiveRenderer;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.*;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class FieldTypeTestPage<FT extends IFieldType> extends WebPage implements IFieldMetadataProvider, IComponentPostProcessor {
	private static final long serialVersionUID = -1070859621193720168L;

	private final FT fieldTypeInstance;

	public FieldTypeTestPage(Class<FT> fieldTypeClass) {
		try {
			fieldTypeInstance = fieldTypeClass.getConstructor().newInstance();
		} catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
			throw new WicketRuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> ForField<T> createComponentForType(Class<T> type) {
		return fieldName -> entityClass -> initialValueModel -> label -> id -> {
			try {
				Annotations annotations = Annotations
					.fromField(entityClass
								   .getDeclaredField(fieldName));
				return (FormComponentPanel<?, T>) postProcessComponent(fieldTypeInstance.createComponent(id,
																										 new FieldReference(annotations,
																								 entityClass,
																								 type, label
														 ),
																										 this, initialValueModel
				), annotations);
			} catch (NoSuchFieldException e) {
				throw new WicketRuntimeException(e);
			}

		};
	}

	@Nonnull
	@Override
	public <T> List<T> getSelectableValues(@Nonnull String fieldName) {
		return new ArrayList<>(0);
	}

	@Nonnull
	@Override
	public <T> IChoiceRenderer<T> getRenderer(@Nonnull String fieldName) {
		return new NaiveRenderer<>() {
			private static final long serialVersionUID = 7190383914627994541L;

			@Override
			public Object getDisplayValue(T object) {
				return object.toString();
			}

			@Override
			public String getIdValue(T object, int index) {
				return Integer.toString(index);
			}
		};
	}

	@FunctionalInterface
	interface ForField<T> {
		InClass<T> forField(String fieldName);
	}

	@FunctionalInterface
	interface InClass<T> {
		WithInitialValue<T> inClass(Class<?> entityClass);
	}

	@FunctionalInterface
	interface WithInitialValue<T> {
		WithLabel<T> withInitialValue(IModel<T> initialValueModel);

	}

	@FunctionalInterface
	interface WithLabel<T> {
		WithId<T> withLabel(String label);
	}

	@FunctionalInterface
	interface WithId<T> {
		FormComponentPanel<?, T> withId(String id);
	}
}
