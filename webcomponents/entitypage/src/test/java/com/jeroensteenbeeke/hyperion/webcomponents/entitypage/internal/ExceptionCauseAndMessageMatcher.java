package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class ExceptionCauseAndMessageMatcher extends TypeSafeDiagnosingMatcher<Throwable> {
	private final Class<? extends Throwable> expectedCause;

	private final String expectedMessage;

	public ExceptionCauseAndMessageMatcher(Class<? extends Throwable> expectedCause, String expectedMessage) {
		this.expectedCause = expectedCause;
		this.expectedMessage = expectedMessage;
	}

	@Override
	protected boolean matchesSafely(Throwable item, Description mismatchDescription) {
		Throwable cause = item;

		while (cause.getCause() != null) {
			cause = cause.getCause();
		}

		boolean match = expectedCause.isAssignableFrom(cause.getClass()) && cause
			.getMessage()
			.equals(expectedMessage);

		if (!match) {
			mismatchDescription
				.appendText("cause of exception is of type ")
				.appendValue(cause.getClass().getName())
				.appendText(" and has message: ")
				.appendValue(cause.getMessage());
		}


		return match;
	}

	@Override
	public void describeTo(Description description) {
		description
			.appendText("cause of exception is of type ")
			.appendValue(expectedCause.getName())
			.appendText(" and has message: ")
			.appendValue(expectedMessage);
	}
}
