package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class CheckBoxPanelTest {
	@Test
	public void test_panel() {
		WicketTester tester = new WicketTester();
		tester.startPage(CheckBoxPanelTestPage.class);
		tester.assertRenderedPage(CheckBoxPanelTestPage.class);

		tester.assertContains("This is a test");
	}
}
