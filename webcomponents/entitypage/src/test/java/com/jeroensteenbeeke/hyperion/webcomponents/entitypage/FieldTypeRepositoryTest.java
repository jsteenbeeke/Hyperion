package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

public class FieldTypeRepositoryTest {
	@Test
	public void testRepeatInvocation() {
		DefaultFieldType.Number number = FieldTypeRepository.instance.get(DefaultFieldType.Number.class);
		assertThat(number, not(nullValue()));

		DefaultFieldType.Number number2 = FieldTypeRepository.instance.get(DefaultFieldType.Number.class);
		assertThat(number2, not(nullValue()));
		assertSame(number, number2);
	}
}
