package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import io.vavr.control.Option;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class NumberPanelTest {
	@Test
	public void testBigDecimalInput() {
		testInput("bigdecimal:number", Option.none());
	}

	@Test
	public void testBigIntegerInput() {
		testInput("biginteger:number", Option.some("BigInteger"));
	}

	@Test
	public void testByte() {
		testInput("byte:number", Option.some("Byte"));
	}

	@Test
	public void testDoubleInput() {
		testInput("double:number", Option.none());
	}

	@Test
	public void testFloatInput() {
		testInput("float:number", Option.none());
	}

	@Test
	public void testIntegerInput() {
		testInput("integer:number", Option.some("Integer"));
	}

	@Test
	public void testLongInput() {
		testInput("long:number", Option.some("Long"));
	}

	@Test
	public void testShortInput() {
		testInput("short:number", Option.some("Short"));
	}

	private void testInput(String formField, Option<String> nonDecimalType) {
		WicketTester tester = new WicketTester();
		tester.startPage(NumberPanelTestPage.class);
		tester.assertRenderedPage(NumberPanelTestPage.class);

		FormTester form = tester.newFormTester("form");
		form.setValue(formField, "50");
		form.submit();

		tester.assertNoErrorMessage();

		form = tester.newFormTester("form");
		form.setValue(formField, "5.5");
		form.submit();

		if (nonDecimalType.isEmpty()) {
			tester.assertNoErrorMessage();
		} else {
			nonDecimalType.peek(type -> tester.assertErrorMessages(String.format("The value of 'number' is not a valid %s.", type)));
		}
	}
}
