package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.DefaultFieldType;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.EntityFormField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class DatePanelTestPage extends FieldTypeTestPage<DefaultFieldType.Date> {
	private static final long serialVersionUID = 4563550121678984025L;

	public DatePanelTestPage() {
		super(DefaultFieldType.Date.class);

		Form<LocalDate> form = new Form<>("form");

		form.add(createComponentForType(LocalDate.class)
					 .forField("bookDate")
					 .inClass(AccountingEntry.class)
					 .withInitialValue(Model
										   .of(LocalDate.now()))
					 .withLabel("LocalDate")
					 .withId("localdate"));

		form.add(createComponentForType(Date.class).forField("legacyBookDate").inClass(AccountingEntry.class).withInitialValue(Model.of(new Date())).withLabel("Date").withId("date"));

		add(form);
	}

	private static class AccountingEntry {
		@EntityFormField(label = "Book date")
		private LocalDate bookDate;

		@EntityFormField(label = "Old style date", required = false)
		private Date legacyBookDate;

		@EntityFormField(label = "Amount")
		private BigDecimal amount;

		public LocalDate getBookDate() {
			return bookDate;
		}

		public void setBookDate(LocalDate bookDate) {
			this.bookDate = bookDate;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public Date getLegacyBookDate() {
			return legacyBookDate;
		}

		public void setLegacyBookDate(Date legacyBookDate) {
			this.legacyBookDate = legacyBookDate;
		}
	}
}
