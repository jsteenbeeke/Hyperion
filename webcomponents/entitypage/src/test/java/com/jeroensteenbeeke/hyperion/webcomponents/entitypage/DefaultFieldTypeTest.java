package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import com.jeroensteenbeeke.hyperion.annotation.Annotations;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.EntityFormField;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DefaultFieldTypeTest implements IFieldMetadataProvider {
	@Test
	public void testDefaultFieldTypeInstantiation() throws Exception {
		new WicketTester();

		FooBar fb = new FooBar();

		Field foo = FooBar.class.getDeclaredField("foo");
		Field bar = FooBar.class.getDeclaredField("bar");
		Field baz = FooBar.class.getDeclaredField("baz");
		Field truth = FooBar.class.getDeclaredField("truth");
		Field time = FooBar.class.getDeclaredField("truth");


		FieldReference fooRef = new FieldReference(
				Annotations.fromField(foo),
				FooBar.class,
				BigDecimal.class,
				"foo"
		);

		FieldReference barRef = new FieldReference(
				Annotations.fromField(bar),
				FooBar.class,
				String.class,
				"bar"
		);


		FieldReference bazRef = new FieldReference(
				Annotations.fromField(baz),
				FooBar.class,
				Date.class,
				"baz"
		);

		FieldReference truthRef = new FieldReference(
				Annotations.fromField(truth),
				FooBar.class,
				boolean.class,
				"truth"
		);

		FieldReference timeRef = new FieldReference(
				Annotations.fromField(time),
				FooBar.class,
				java.time.LocalTime.class,
				"time"
		);

		Map<Class<?>, FieldReference> inputs = new HashMap<>();
		inputs.put(DefaultFieldType.Currency.class, fooRef);
		inputs.put(DefaultFieldType.Url.class, barRef);
		inputs.put(DefaultFieldType.Email.class, barRef);
		inputs.put(DefaultFieldType.DateTimeLocal.class, bazRef);
		inputs.put(DefaultFieldType.Time.class, timeRef);
		inputs.put(DefaultFieldType.Date.class, bazRef);
		inputs.put(DefaultFieldType.Number.class, fooRef);
		inputs.put(DefaultFieldType.Password.class, barRef);
		inputs.put(DefaultFieldType.TextArea.class, barRef);
		inputs.put(DefaultFieldType.TextField.class, barRef);
		inputs.put(DefaultFieldType.CheckBox.class, truthRef);
		inputs.put(DefaultFieldType.DropDownChoice.class, barRef);

		for (Class<?> innerClass : DefaultFieldType.class.getClasses()) {
			if (IFieldType.class.isAssignableFrom(innerClass)) {
				assertTrue(inputs.containsKey(innerClass));

				IFieldType fieldType;
				assertNotNull(fieldType = (IFieldType) innerClass.newInstance());


				FieldReference ref = inputs.get(innerClass);
				assertNotNull(fieldType.createComponent("id", ref, this, Model.of(ref.getValue
						(fb))));
			}
		}
	}

	@Nonnull
	@Override
	public <T> List<T> getSelectableValues(@Nonnull String fieldName) {
		return new LinkedList<>();
	}

	@Nonnull
	@Override
	public <T> IChoiceRenderer<T> getRenderer(@Nonnull String fieldName) {
		return new ChoiceRenderer<>();
	}
}

class FooBar {
	@EntityFormField(label = "Foo", type = DefaultFieldType.Number.class)
	private BigDecimal foo;

	@EntityFormField(label = "Bar")
	private String bar;

	@EntityFormField(label = "Baz", type = DefaultFieldType.Date.class)
	private Date baz;

	@EntityFormField(label = "Truth", type = DefaultFieldType.CheckBox.class)
	private boolean truth;

	@EntityFormField(label = "Time", type = DefaultFieldType.Time.class)
	private java.time.LocalTime time;

	public BigDecimal getFoo() {
		return foo;
	}

	public void setFoo(BigDecimal foo) {
		this.foo = foo;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public Date getBaz() {
		return baz;
	}

	public void setBaz(Date baz) {
		this.baz = baz;
	}

	public boolean isTruth() {
		return truth;
	}

	public void setTruth(boolean truth) {
		this.truth = truth;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}
}
