package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal.NumberPanel;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.tester.WicketTester;
import org.apache.wicket.validation.validator.RangeValidator;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NumberPanelRangeTest {
	@Test
	public void testNumberPanel() throws NoSuchFieldException {
		new WicketTester();

		NumberPanel<Integer> panel = new NumberPanel<>("id",
				Model.of(5), Integer.class, Double::intValue);
		panel.applyMaximum(5);

		assertTrue(panel.getFormComponent().getValidators().stream().allMatch(RangeValidator
				.class::isInstance));

		panel = new NumberPanel<>("id",
				Model.of(5), Integer.class, Double::intValue);
		panel.applyRange(0, 5);

		assertTrue(panel.getFormComponent().getValidators().stream().allMatch(RangeValidator
				.class::isInstance));

		panel = new NumberPanel<>("id",
				Model.of(5), Integer.class, Double::intValue);
		panel.applyMinimum(0);

		assertTrue(panel.getFormComponent().getValidators().stream().allMatch(RangeValidator
				.class::isInstance));
	}


}
