package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class DatePanelTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testValidDateTypes() {
		WicketTester tester = new WicketTester();
		tester.startPage(DatePanelTestPage.class);
		tester.assertRenderedPage(DatePanelTestPage.class);

		FormTester form = tester.newFormTester("form");
		form.setValue("localdate:date", "2018-01-01");
		form.setValue("date:date", "2018-01-01");

		form.submit();

		tester.assertNoErrorMessage();
		tester.assertRenderedPage(DatePanelTestPage.class);

		form = tester.newFormTester("form");
		form.setValue("localdate:date", "2018/01/01");
		form.setValue("date:date", "2018-01-01");

		form.submit();

		tester.assertErrorMessages("The value of 'date' is not a valid LocalDate.");
		tester.assertRenderedPage(DatePanelTestPage.class);

		form = tester.newFormTester("form");
		form.setValue("localdate:date", "2018-01-01");
		form.setValue("date:date", "2018/01/01");
		form.submit();

		tester.assertErrorMessages("The value of 'date' is not a valid Date.");
	}

	@Test
	public void testInvalidDateType() {
		WicketTester tester = new WicketTester();

		expectedException.expect(new ExceptionCauseAndMessageMatcher(IllegalArgumentException.class, "Cannot display type java.time.ZonedDateTime as date"));

		tester.startPage(InvalidDatePanelTestPage.class);

	}
}
