package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.DefaultFieldType;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.EntityFormField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;

public class InvalidDatePanelTestPage extends FieldTypeTestPage<DefaultFieldType.Date> {
	private static final long serialVersionUID = 4563550121678984025L;

	public InvalidDatePanelTestPage() {
		super(DefaultFieldType.Date.class);

		Form<LocalDate> form = new Form<>("form");

		form.add(createComponentForType(ZonedDateTime.class)
					 .forField("bookDate")
					 .inClass(AccountingEntry.class)
					 .withInitialValue(Model
										   .of(ZonedDateTime.now()))
					 .withLabel("LocalDate")
					 .withId("localdate"));


		add(form);
	}

	private static class AccountingEntry {
		@EntityFormField(label = "Book date")
		private LocalDate bookDate;

		public LocalDate getBookDate() {
			return bookDate;
		}

		public void setBookDate(LocalDate bookDate) {
			this.bookDate = bookDate;
		}

	}
}
