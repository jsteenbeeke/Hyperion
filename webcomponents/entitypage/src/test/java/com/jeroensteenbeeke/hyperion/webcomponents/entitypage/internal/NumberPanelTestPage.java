package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.DefaultFieldType;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.EntityFormField;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Maximum;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Minimum;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Step;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;

public class NumberPanelTestPage extends FieldTypeTestPage<DefaultFieldType.Number> {
	private static final long serialVersionUID = 7471829354850034861L;

	public NumberPanelTestPage() {
		super(DefaultFieldType.Number.class);
		Form<Void> form = new Form<>("form");
		form.add(createComponentForType(BigDecimal.class).forField("bd")
														 .inClass(AllTheNumbers.class)
														 .withInitialValue(Model.of(BigDecimal.valueOf(8.0)))
														 .withLabel("BigDecimal")
														 .withId("bigdecimal"));
		form.add(createComponentForType(BigInteger.class).forField("bi")
														 .inClass(AllTheNumbers.class)
														 .withInitialValue(Model.of(BigInteger.valueOf(300L)))
														 .withLabel("BigInteger")
														 .withId("biginteger"));
		form.add(createComponentForType(Byte.class)
					 .forField("b")
					 .inClass(AllTheNumbers.class)
					 .withInitialValue(Model.of((byte) 5))
					 .withLabel("Byte")
					 .withId("byte"));
		form.add(createComponentForType(Double.class).forField("d")
													 .inClass(AllTheNumbers.class)
													 .withInitialValue(Model.of(9.0))
													 .withLabel("Double")
													 .withId("double"));
		form.add(createComponentForType(Float.class).forField("f")
													.inClass(AllTheNumbers.class)
													.withInitialValue(Model.of(12.0f))
													.withLabel("Float")
													.withId("float"));
		form.add(createComponentForType(Integer.class).forField("i")
													  .inClass(AllTheNumbers.class)
													  .withInitialValue(Model.of(15))
													  .withLabel("Integer")
													  .withId("integer"));
		form.add(createComponentForType(Long.class).forField("l")
												   .inClass(AllTheNumbers.class)
												   .withInitialValue(Model.of(12l))
												   .withLabel("Long")
												   .withId("long"));
		form.add(createComponentForType(Short.class).forField("s")
													.inClass(AllTheNumbers.class)
													.withInitialValue(Model.of((short) 5))
													.withLabel("Short")
													.withId("short"));

		add(form);
	}

	private static class AllTheNumbers {

		@EntityFormField(label = "Byte")
		private Byte b;

		@EntityFormField(label = "BigDecimal")
		@Step(0.5)
		private BigDecimal bd;

		@EntityFormField(label = "BigInteger")
		private BigInteger bi;

		@EntityFormField(label = "Double")
		private Double d;

		@EntityFormField(label = "Float")
		private Float f;

		@EntityFormField(label = "Integer")
		@Minimum(0)
		private Integer i;

		@EntityFormField(label = "Long")
		@Maximum(50)
		private Long l;

		@EntityFormField(label = "Short")
		@Minimum(0)
		@Maximum(50)
		private Short s;

		public Byte getB() {
			return b;
		}

		public void setB(Byte b) {
			this.b = b;
		}

		public BigDecimal getBd() {
			return bd;
		}

		public void setBd(BigDecimal bd) {
			this.bd = bd;
		}

		public BigInteger getBi() {
			return bi;
		}

		public void setBi(BigInteger bi) {
			this.bi = bi;
		}

		public Double getD() {
			return d;
		}

		public void setD(Double d) {
			this.d = d;
		}

		public Float getF() {
			return f;
		}

		public void setF(Float f) {
			this.f = f;
		}

		public Integer getI() {
			return i;
		}

		public void setI(Integer i) {
			this.i = i;
		}

		public Long getL() {
			return l;
		}

		public void setL(Long l) {
			this.l = l;
		}

		public Short getS() {
			return s;
		}

		public void setS(Short s) {
			this.s = s;
		}
	}
}
