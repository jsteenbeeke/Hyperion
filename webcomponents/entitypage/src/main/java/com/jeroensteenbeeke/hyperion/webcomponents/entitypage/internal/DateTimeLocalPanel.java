package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableBiFunction;

import javax.annotation.Nonnull;

/**
 * FormComponentPanel that renders a given FormComponent to a {@code <input type="datetime-local">}
 * @param <F> The type of FormComponent
 * @param <T> The type of data rendered by the FormComponent
 */
public class DateTimeLocalPanel<F extends FormComponent<T>,T> extends FormComponentPanel<F,T> {
	private static final long serialVersionUID = 3349574978178048083L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param dateModel The model containing the date type
	 * @param componentCreator Function that takes a wicket ID and the model and turns it into a FormComponent
	 */
	public DateTimeLocalPanel(@Nonnull String id, IModel<T> dateModel, SerializableBiFunction<String,
			IModel<T>,F> componentCreator) {
		super(id, componentCreator.apply("datetimelocal", dateModel));
	}
}
