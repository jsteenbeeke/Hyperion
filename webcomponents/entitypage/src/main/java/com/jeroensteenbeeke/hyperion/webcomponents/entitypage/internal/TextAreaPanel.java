package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.IModel;

/**
 * FormComponentPanel that renders a text area
 *
 * @param <T> The type of data to display in the text area
 */
public class TextAreaPanel<T> extends FormComponentPanel<TextArea<T>, T> {
	private static final long serialVersionUID = -2701726887788225548L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The model of the text area
	 */
	public TextAreaPanel(String id, IModel<T> initialValue) {
		super(id, new TextArea<>("textarea", initialValue));
	}
}
