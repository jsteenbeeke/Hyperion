package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;

/**
 * FormComponent that renders a default textfield
 * @param <T> The type of data to show in the textfield
 */
public class TextFieldPanel<T> extends FormComponentPanel<TextField<T>,T> {
	private static final long serialVersionUID = 7534230215249861530L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The value of the field
	 * @param propertyClass The type of field
	 */
	public TextFieldPanel(String id, IModel<T> initialValue, Class<T> propertyClass) {
		super(id, new TextField<>("text", initialValue, propertyClass));
	}
}
