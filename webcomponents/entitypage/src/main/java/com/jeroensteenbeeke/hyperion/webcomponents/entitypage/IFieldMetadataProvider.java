/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.markup.html.form.IChoiceRenderer;

import javax.annotation.Nonnull;

/**
 * Facade for entity edit page, to provide contextual information for creating form components
 */
public interface IFieldMetadataProvider extends Serializable {

	/**
	 * Get all objects that are considered valid values for the given field
	 * @param fieldName The name of the field
	 * @param <T> The type of return values
	 * @return A list of valid objects
	 */
	@Nonnull
	<T> List<T> getSelectableValues(@Nonnull String fieldName);

	/**
	 * Get a renderer for the given field
	 * @param fieldName The name of the field to get a renderer for
	 * @param <T> The type of the choices for this field
	 * @return An IChoiceRenderer for the given type
	 */
	@Nonnull
	<T> IChoiceRenderer<T> getRenderer(@Nonnull String fieldName);
}
