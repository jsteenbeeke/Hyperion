package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableBiFunction;

import javax.annotation.Nonnull;

/**
 * FormComponentPanel that renders a time component
 * @param <F> The type of time component
 * @param <T> The type of time value
 */
public class TimePanel<F extends FormComponent<T>,T> extends FormComponentPanel<F,T> {
	private static final long serialVersionUID = 4382268300201708034L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param timeModel The model containing the time value
	 * @param componentCreator Function that uses a wicket ID and time model to create a FormComponent
	 */
	public TimePanel(@Nonnull String id, IModel<T> timeModel, SerializableBiFunction<String,
			IModel<T>,F> componentCreator) {
		super(id, componentCreator.apply(id, timeModel));
	}
}
