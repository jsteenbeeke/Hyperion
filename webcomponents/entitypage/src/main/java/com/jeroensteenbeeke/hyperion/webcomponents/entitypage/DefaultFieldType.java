/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import com.jeroensteenbeeke.hyperion.webcomponents.core.form.TimeTextField;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal.*;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.markup.html.form.datetime.LocalDateTextField;
import org.apache.wicket.extensions.markup.html.form.datetime.LocalDateTimeTextField;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableFunction;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.FormatStyle;
import java.util.function.Function;

/**
 * Default implementations of IFieldType, covering the most common types of form components required
 */
public class DefaultFieldType {

	/**
	 * Dropdown choice (combobox)
	 */
	public static class DropDownChoice implements IFieldType {
		private static final long serialVersionUID = -7243186926698766732L;

		@Nonnull
		@Override
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			DropDownChoicePanel<T> dropDownChoice = new DropDownChoicePanel<>(id, initialValue,
																			  new ProviderModel<>(provider, ref.getFieldName()),
																			  provider.getRenderer(ref.getFieldName()));
			dropDownChoice.getFormComponent().setNullValid(!ref.isRequired());
			return dropDownChoice;
		}
	}

	/**
	 * Checkbox
	 */
	public static class CheckBox implements IFieldType {

		private static final long serialVersionUID = -9113630490633957327L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			return (FormComponentPanel<?, T>) new CheckBoxPanel(id, (IModel<Boolean>) initialValue);
		}
	}

	/**
	 * TextField (the default value)
	 */
	public static class TextField implements IFieldType {

		private static final long serialVersionUID = 4631399515825757661L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			return (FormComponentPanel<?, T>) new TextFieldPanel(id, initialValue, ref.getPropertyClass());
		}
	}

	/**
	 * TextArea
	 */
	public static class TextArea implements IFieldType {

		private static final long serialVersionUID = 2423928018915723723L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			return (FormComponentPanel<?, T>) new TextAreaPanel(id, initialValue);
		}
	}

	/**
	 * Password. {@code <input type="password">}
	 */
	public static class Password implements IFieldType {

		private static final long serialVersionUID = -1432046682071860410L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			if (!String.class.isAssignableFrom(ref.getPropertyClass())) {
				throw new IllegalArgumentException("Field " + ref.getFieldName() + " cannot be " +
													   "rendered as password, because it is not a String, but " + ref
					.getPropertyClass().getName());
			}

			return (FormComponentPanel<?, T>) new PasswordPanel(id, (IModel<String>) initialValue);
		}
	}

	/**
	 * Number. {@code <input type="number">}
	 */
	public static class Number implements IFieldType {

		private static final long serialVersionUID = 1057144461669915283L;

		@Override
		@SuppressWarnings({"unchecked", "rawtypes"})
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {

			if (Double.class.isAssignableFrom(ref.getPropertyClass()) || double.class.isAssignableFrom(ref.getPropertyClass())) {
				SerializableFunction<Double, Double> doubleToDouble = SerializableFunction.identity();

				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(doubleToDouble));
			} else if (Float.class.isAssignableFrom(ref.getPropertyClass()) || float.class.isAssignableFrom(ref.getPropertyClass())) {
				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(Double::floatValue));
			} else if (Integer.class.isAssignableFrom(ref.getPropertyClass()) || int.class.isAssignableFrom(ref.getPropertyClass())) {
				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(Double::intValue));
			} else if (Long.class.isAssignableFrom(ref.getPropertyClass()) || long.class.isAssignableFrom(ref.getPropertyClass())) {
				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(Math::round));
			} else if (Short.class.isAssignableFrom(ref.getPropertyClass()) || short.class.isAssignableFrom(ref.getPropertyClass())) {
				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(Double::shortValue));
			} else if (Byte.class.isAssignableFrom(ref.getPropertyClass()) || byte.class.isAssignableFrom(ref.getPropertyClass())) {
				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(Double::byteValue));
			} else if (BigDecimal.class.isAssignableFrom(ref.getPropertyClass())) {
				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(BigDecimal::valueOf));
			} else if (BigInteger.class.isAssignableFrom(ref.getPropertyClass())) {
				return new NumberPanel(id, initialValue, ref.getPropertyClass(), fudgeGenerics(d -> BigInteger.valueOf(d.longValue())));
			}


			throw new IllegalStateException("Unsupported number type for NumberPanel");
		}

		@SuppressWarnings("unchecked")
		private <T, N extends java.lang.Number> SerializableFunction<Double, T> fudgeGenerics(SerializableFunction<Double, N> doubleToNumber) {
			return (SerializableFunction<Double, T>) doubleToNumber;
		}
	}

	/**
	 * Date. Depending on the type of Date used, creates either a DateTextField or LocalDateTextField
	 */
	public static class Date implements IFieldType {

		private static final long serialVersionUID = -4343298364921389565L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			if (java.util.Date.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new DatePanel<>(id, (IModel<java.util.Date>)
					initialValue,
																  (i, m) -> new DateTextField(i, m, "yyyy-MM-dd"));
			} else if (LocalDate.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new DatePanel<>(id, (IModel<LocalDate>)
					initialValue,
																  (i, m) -> new LocalDateTextField(i, m, "yyyy-MM-dd", "yyyy-MM-dd") {
																	  private static final long serialVersionUID = -5110265277667254511L;

																	  @Override
																	  protected String[] getInputTypes() {
																		  return new String[]{"date"};
																	  }
																  });
			} else {
				throw new IllegalArgumentException("Cannot display type " + ref.getPropertyClass()
																			   .getName() + " as date");
			}
		}
	}

	/**
	 * Time (represented by LocalTime)
	 */
	public static class Time implements IFieldType {

		private static final long serialVersionUID = 3863105581942811301L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {


			if (LocalTime.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new TimePanel<>(id, (IModel<LocalTime>)
					initialValue,
																  (i, m) -> new TimeTextField<>(i, m, LocalTime.class));
			} else {
				throw new IllegalArgumentException("Cannot display type " + ref.getPropertyClass().getName
					() + " as time");
			}
		}
	}

	/**
	 * DateTimeLocal. {@code <input type="datetime-local">}
	 */
	public static class DateTimeLocal implements IFieldType {

		private static final long serialVersionUID = 7287802980939497496L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			if (java.util.Date.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new DateTimeLocalPanel<>(id, (IModel<java.util.Date>)
					initialValue,
																		   (i, m) -> new DateTextField(i, m, "yyyy-MM-dd hh:mm"));
			} else if (LocalDateTime.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new DateTimeLocalPanel<>(id, (IModel<LocalDateTime>)
					initialValue,
																		   (i, m) -> new LocalDateTimeTextField(i, m, "yyyy-MM-dd hh:mm") {
																			   private static final long serialVersionUID = -238869224299457246L;

																			   @Override
																			   protected String[] getInputTypes() {
																				   return new String[]{"datetime-local"};
																			   }
																		   });
			} else {
				throw new IllegalArgumentException("Cannot display type " + ref.getPropertyClass()
																			   .getName() + " as date");
			}
		}
	}

	/**
	 * E-mail. {@code <input type="email">}
	 */
	public static class Email implements IFieldType {

		private static final long serialVersionUID = -4707249294542465329L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			if (!String.class.isAssignableFrom(ref.getPropertyClass())) {
				throw new IllegalArgumentException("Field " + ref.getFieldName() + " cannot be " +
													   "rendered as e-mail, because it is not a String, but " + ref
					.getPropertyClass().getName());
			}

			return (FormComponentPanel<?, T>) new EmailPanel(id, (IModel<String>) initialValue);
		}
	}

	/**
	 * URL. {@code <input type="url">}
	 */
	public static class Url implements IFieldType {

		private static final long serialVersionUID = -1779038482277583358L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			if (!String.class.isAssignableFrom(ref.getPropertyClass())) {
				throw new IllegalArgumentException("Field " + ref.getFieldName() + " cannot be " +
													   "rendered as url, because it is not a String, but " + ref
					.getPropertyClass().getName());
			}

			return (FormComponentPanel<?, T>) new UrlPanel(id, (IModel<String>) initialValue);
		}
	}

	/**
	 * Currency. Regular text field with special logic to enforce numbers
	 */
	public static class Currency implements IFieldType {

		private static final long serialVersionUID = 2269559906875057265L;

		@Override
		@SuppressWarnings("unchecked")
		public <T> FormComponentPanel<?, T> createComponent(@Nonnull String id,
															@Nonnull FieldReference ref,
															@Nonnull IFieldMetadataProvider provider,
															@Nonnull IModel<T> initialValue) {
			if (Double.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new CurrencyPanel<>(id,
																	  (IModel<Double>) initialValue,
																	  SerializableFunction
																		  .identity());
			} else if (BigDecimal.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new CurrencyPanel<>(id,
																	  (IModel<BigDecimal>) initialValue,
																	  BigDecimal::valueOf);
			} else if (Float.class.isAssignableFrom(ref.getPropertyClass())) {
				return (FormComponentPanel<?, T>) new CurrencyPanel<>(id,
																	  (IModel<Float>) initialValue,
																	  Double::floatValue);
			}

			throw new IllegalArgumentException("Cannot display type " + ref.getPropertyClass()
																		   .getName() + " as " +
												   "currency");
		}
	}


}
