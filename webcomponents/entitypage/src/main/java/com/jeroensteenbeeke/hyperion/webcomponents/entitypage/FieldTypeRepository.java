package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Repository of field types: stores all factories for generating FormComponentPanels, creating new instances if a field
 * type has not been encountered before
 */
public enum FieldTypeRepository {
	/**
	 * Singleton instance
	 */
	instance;

	private final Map<Class<? extends IFieldType>,IFieldType> fieldTypes = new HashMap<>();

	/**
	 * Gets an instance of the given field type class, creating a new instance if it does not exist
	 * @param type The class representing the field type
	 * @param <T> The field type
	 * @return An instance of the field type
	 */
	@SuppressWarnings("unchecked")
	public <T extends IFieldType> T get(Class<T> type) {
		if (fieldTypes.containsKey(type)) {
			return (T) fieldTypes.get(type);
		} else {
			try {
				T fieldType = type.getConstructor().newInstance();
				fieldTypes.put(type, fieldType);
				return fieldType;
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
				throw new IllegalStateException("Invalid fieldType implementation", e);
			}

		}
	}
}
