package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.UrlTextField;
import org.apache.wicket.model.IModel;

/**
 * FormComponentPanel that renders an URL as a UrlTextField
 */
public class UrlPanel extends FormComponentPanel<UrlTextField,String> {
	private static final long serialVersionUID = -8691992490403602008L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The model containing the URL
	 */
	public UrlPanel(String id, IModel<String> initialValue) {
		super(id, new UrlTextField("url", initialValue));
	}
}
