package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableBiFunction;

import javax.annotation.Nonnull;

/**
 * FormComponentPanel that renders a specified component to a {@code <input type="date">}
 * @param <F> The type of FormComponent
 * @param <T> The type of value to be displayed by the FormComponent
 */
public class DatePanel<F extends FormComponent<T>,T> extends FormComponentPanel<F,T> {
	private static final long serialVersionUID = -4305057817030728646L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param dateModel The model containing the date type
	 * @param componentCreator Function that takes an ID and the model and turns it into a FormComponent
	 */
	public DatePanel(@Nonnull String id, IModel<T> dateModel, SerializableBiFunction<String,
			IModel<T>,F> componentCreator) {
		super(id, componentCreator.apply("date", dateModel));
	}
}
