/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.EntityFormField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeroensteenbeeke.hyperion.annotation.Annotations;

/**
 * A FieldReference is a collection of metadata about a field inside an entity, along with information about present
 * annotations (with those directly relevant to Entity Forms already read)
 */
public class FieldReference implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Annotations annotations;

	private final String fieldName;

	private final Class<?> propertyClass;

	private final String label;

	private final boolean required;

	private final boolean localized;

	private final Class<?> containingClass;

	private final Class<? extends IFieldType> fieldType;

	/**
	 * Constructor
	 *
	 * @param annotations     The annotations applied to the field
	 * @param containingClass The entity class
	 * @param propertyClass   The type of the field
	 * @param name            The name of the field
	 */
	public FieldReference(Annotations annotations, Class<?> containingClass,
						  Class<?> propertyClass, String name) {
		this.annotations = annotations;
		this.fieldName = name;
		this.propertyClass = propertyClass;
		this.containingClass = containingClass;

		this.label = annotations.getAnnotation(EntityFormField.class).map(EntityFormField::label)
								.orElseThrow(() -> new IllegalArgumentException("FieldReference " +
																				 "without EntityFormField annotation"));
		this.required = annotations.getAnnotation(EntityFormField.class).map(EntityFormField::required)
								   .orElseThrow(() -> new IllegalArgumentException("FieldReference " +
																					"without EntityFormField annotation"));
		this.localized = annotations
			.getAnnotation(EntityFormField.class)
			.map(EntityFormField::localized)
			.orElseThrow(() -> new IllegalArgumentException("FieldReference " +
															 "without EntityFormField annotation"));

		this.fieldType = annotations
			.getAnnotation(EntityFormField.class)
			.map(EntityFormField::type)
			.orElseThrow(() -> new IllegalArgumentException("FieldReference " +
															 "without EntityFormField annotation"));
	}

	/**
	 * Returns the type of the field (i.e. the column in the entity)
	 *
	 * @return A Class
	 */
	public Class<?> getPropertyClass() {
		return propertyClass;
	}

	/**
	 * Get the annotations applied to the field
	 *
	 * @return An Annotations object
	 */
	public Annotations getAnnotations() {
		return annotations;
	}

	/**
	 * Get the name of the field
	 *
	 * @return A String representing the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Get the label that should be used. If the {@code @EntityFormField} annotation states that the label should be
	 * localized, this field is interpreted as being the I18N key
	 *
	 * @return The label/key
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Whether or not the field is required
	 * @return {@code true} if the field is required. {@code false} otherwise
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * Whether or not the field label should be localized
	 * @return {@code true} if the field should be localized. {@code false} otherwise
	 */
	public boolean isLocalized() {
		return localized;
	}

	/**
	 * Returns the factory class to be used for creating a panel for this field
	 * @return A Class object representing an IFieldType
	 */
	public Class<? extends IFieldType> getFieldType() {
		return fieldType;
	}

	/**
	 * Gets the value of this field from the given entity
	 * @param entity The entity in question
	 * @return The value
	 */
	public Serializable getValue(Object entity) {
		try {
			Method method = containingClass.getMethod(getGetterName());

			return (Serializable) method.invoke(entity);
		} catch (SecurityException | NoSuchMethodException | IllegalArgumentException | IllegalAccessException |
			InvocationTargetException e) {
			throw new IllegalStateException("Could not invoke getter", e);
		}
	}

	/**
	 * Sets the value of this field to the given entity
	 * @param entity The entity
	 * @param value The value to set
	 */
	public void setValue(Object entity, Object value) {
		try {
			Method method = containingClass.getMethod(getSetterName(),
													  propertyClass);

			method.invoke(entity, value);
		} catch (SecurityException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException |
			InvocationTargetException e) {
			throw new IllegalStateException("Could not invoke setter", e);
		}
	}

	private String getGetterName() {
		final String prefix = isPrimitiveBoolean() ? "is" : "get";

		return prefix.concat(fieldName.substring(0, 1).toUpperCase()
									  .concat(fieldName.substring(1)));
	}

	private boolean isPrimitiveBoolean() {
		if (boolean.class.equals(propertyClass)) {
			return true;
		}

		return false;
	}

	private String getSetterName() {
		return "set".concat(fieldName.substring(0, 1).toUpperCase()
									 .concat(fieldName.substring(1)));
	}

	/**
	 * Create a new FieldReference for the given field
	 * @param f The field
	 * @return A FieldReference for the given field
	 * @throws IllegalArgumentException If the given field is not annotated with {@code @EntityFormField}
	 */
	public static FieldReference of(Field f) {
		return new FieldReference(
			Annotations.fromField(f),
			f.getDeclaringClass(),
			f.getType(),
			f.getName()
		);
	}
}
