package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.model.IModel;

import javax.annotation.Nonnull;

/**
 * Panel representing a form component and label
 * @param <F> The type of form component
 * @param <T> The type of value contained by the form component
 */
public class FormComponentPanel<F extends FormComponent<?>,T> extends TypedPanel<T> {
	private static final long serialVersionUID = -8640475426394702583L;

	protected final F formComponent;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param formComponent The form component
	 */
	public FormComponentPanel(@Nonnull String id, @Nonnull F formComponent) {
		super(id);
		add(this.formComponent = formComponent);
	}

	/**
	 * Sets the model for the form component's label
	 * @param labelModel The model for the label
	 */
	public void setLabel(@Nonnull IModel<String> labelModel) {
		formComponent.setLabel(labelModel);
	}

	/**
	 * Gets the contained FormComponent
	 * @return A FormComponent
	 */
	public F getFormComponent() {
		return formComponent;
	}
}
