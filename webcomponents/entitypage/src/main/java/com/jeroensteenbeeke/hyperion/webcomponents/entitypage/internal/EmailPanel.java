package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.EmailTextField;
import org.apache.wicket.model.IModel;

/**
 * FormComponentPanel that renders an EmailTextField
 */
public class EmailPanel extends FormComponentPanel<EmailTextField,String> {
	private static final long serialVersionUID = -4342649649732618979L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue Model containing the value of the field
	 */
	public EmailPanel(String id, IModel<String> initialValue) {
		super(id, new EmailTextField("email", initialValue));
	}
}
