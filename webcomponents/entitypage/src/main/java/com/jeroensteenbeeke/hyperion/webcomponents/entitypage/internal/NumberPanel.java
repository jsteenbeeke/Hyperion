package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IRangeSupportingComponent;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IStepSupportingComponent;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.RangeValidator;
import org.danekja.java.util.function.serializable.SerializableFunction;

import javax.annotation.Nonnull;

/**
 * FormComponentPanel that renders a {@code <input type="number">} using a NumberTextField
 * @param <N> The type of number
 */
public class NumberPanel<N extends Number & Comparable<N>> extends
		FormComponentPanel<NumberTextField<N>,N>
		implements IRangeSupportingComponent, IStepSupportingComponent {
	private static final long serialVersionUID = -3778713032515489223L;

	private final SerializableFunction<Double, N> numberFromDouble;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The model of the field
	 * @param numberClass The type of number
	 * @param numberFromDouble Conversion function that turns a double into type N
	 */
	public NumberPanel(@Nonnull String id,
					   IModel<N> initialValue,
					   Class<N> numberClass,
					   SerializableFunction<Double,N> numberFromDouble) {
		super(id, new NumberTextField<>("number", initialValue, numberClass));
		this.numberFromDouble = numberFromDouble;
	}

	@Override
	public void applyRange(double min, double max) {
		getFormComponent().add(new RangeValidator<>(numberFromDouble.apply(min),numberFromDouble.apply(max)));
	}

	@Override
	public void applyMinimum(double min) {
		getFormComponent().add(RangeValidator.minimum(numberFromDouble.apply(min)));
	}

	@Override
	public void applyMaximum(double max) {
		getFormComponent().add(RangeValidator.maximum(numberFromDouble.apply(max)));
	}

	@Override
	public void setStep(double step) {
		getFormComponent().setStep(numberFromDouble.apply(step));
	}
}
