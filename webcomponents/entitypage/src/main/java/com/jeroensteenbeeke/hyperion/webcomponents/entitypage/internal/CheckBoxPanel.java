package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.model.IModel;

import javax.annotation.Nonnull;

/**
 * Panel that contains a CheckBox
 */
public class CheckBoxPanel extends FormComponentPanel<CheckBox,Boolean> {
	private static final long serialVersionUID = 7038710937333691054L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model
	 */
	public CheckBoxPanel(@Nonnull String id, @Nonnull IModel<Boolean> model) {
		super(id, new CheckBox("checkbox", model));
	}

}
