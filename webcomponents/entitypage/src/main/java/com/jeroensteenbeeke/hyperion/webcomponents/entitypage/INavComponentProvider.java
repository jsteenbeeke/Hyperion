package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import org.apache.wicket.Component;

/**
 * Provider for navigation components
 */
public interface INavComponentProvider {
	/**
	 * Creates a new navigation component
	 * @param id Th of the component to create
	 * @return The newly created component
	 */
	Component createNavComponent(String id);
}
