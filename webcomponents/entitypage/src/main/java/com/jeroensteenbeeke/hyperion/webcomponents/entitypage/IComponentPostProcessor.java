package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import com.jeroensteenbeeke.hyperion.annotation.Annotations;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Maximum;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Minimum;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation.Step;

/**
 * Component post-processor. Allows the configuration of additional details for certain components, such
 * as minimum, maximum and steps
 */
public interface IComponentPostProcessor {
	/**
	 * Post-process the given component based on the given annotations
	 * @param component The component under consideration
	 * @param annotations Annotations defined for the field
	 * @return The component, possibly decorated
	 */
	default FormComponentPanel<?,?> postProcessComponent(FormComponentPanel<?,?> component,
													 Annotations annotations
													 ) {
		if (component instanceof IRangeSupportingComponent) {
			IRangeSupportingComponent rangeComponent =
				(IRangeSupportingComponent) component;

			Minimum min = annotations.getAnnotation(Minimum.class).orElse(null);
			Maximum max = annotations.getAnnotation(Maximum.class).orElse(null);

			if (min != null && max != null) {
				rangeComponent.applyRange(min.value(),
										  max.value());
			} else if (min != null) {
				rangeComponent.applyMinimum(min.value());
			} else if (max != null) {
				rangeComponent.applyMaximum(max.value());
			}
		}

		if (component instanceof IStepSupportingComponent) {
			IStepSupportingComponent stepSupportingComponent = (IStepSupportingComponent) component;

			annotations
				.getAnnotation(Step.class)
				.map(Step::value)
				.ifPresent(stepSupportingComponent::setStep);

		}

		return component;
	}
}
