package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import org.apache.wicket.model.IModel;

import javax.annotation.Nonnull;
import java.io.Serializable;

/**
 * A field type is a factory for form components
 */
public interface IFieldType extends Serializable {
	/**
	 * Create a form component
	 * @param id The wicket:id
	 * @param ref The reference to the field the component represents
	 * @param provider Metadata provider for the field
	 * @param initialValue The initial value for the form component
	 * @param <T> The type of entity in the form
	 * @return A FormComponentPanel
	 */
	<T> FormComponentPanel<?,T> createComponent(@Nonnull String id,
												@Nonnull FieldReference ref,
									  @Nonnull IFieldMetadataProvider provider,
									  @Nonnull IModel<T> initialValue);
}
