package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.model.IModel;

/**
 * FormComponentPanel that renders a {@code <input type="password">} using a PasswordTextField
 */
public class PasswordPanel extends FormComponentPanel<PasswordTextField,String> {
	private static final long serialVersionUID = -5022949530980216476L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The model containing the password value
	 */
	public PasswordPanel(String id, IModel<String> initialValue) {
		super(id, new PasswordTextField("password", initialValue));
	}
}
