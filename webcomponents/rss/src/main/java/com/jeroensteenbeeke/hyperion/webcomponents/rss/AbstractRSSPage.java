/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.rss;

import org.apache.wicket.markup.MarkupType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * RSS feed
 */
public abstract class AbstractRSSPage extends WebPage {
	private static final long serialVersionUID = 1L;

	private static final DateFormat FORMAT = new SimpleDateFormat(
		"EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

	/**
	 * Constructor
	 * @param params The parameters passed to the page
	 */
	public AbstractRSSPage(PageParameters params) {
		onBeforeAddRSSComponents(params);

		add(new NonNullLabel("title", getTitle()));
		add(new NonNullLabel("description", getDescription()));
		add(new NonNullLabel("link", getLink()));
		add(new NonNullLabel("buildDate", FORMAT.format(getBuildDate())));
		add(new NonNullLabel("pubDate", FORMAT.format(getPubDate())));
		add(new NonNullLabel("ttl", Model.of(getTTL()).map(Object::toString)));

		add(new ListView<>("items", getItems()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<RSSItem> item) {
				RSSItem i = item.getModelObject();

				item.add(new NonNullLabel("title", i.getTitle())
							 .setEscapeModelStrings(false));
				item.add(new NonNullLabel("desc", i.getDescription())
							 .setEscapeModelStrings(false));
				item.add(new NonNullLabel("link", i.getLink()));
				item.add(new NonNullLabel("guid", i.getGuid()));
				item.add(new NonNullLabel("pubDate", FORMAT.format(i
																	   .getPubDate())));
				item.add(new NonNullLabel("category", i.getCategory()));
				item.add(new NonNullLabel("author", i.getAuthor()));
			}
		});

	}

	/**
	 * Returns the time to live in seconds, i.e. how often the feed should be refreshed
	 * @return The TTL in seconds. Defaults to 30 minutes
	 */
	protected int getTTL() {
		return 1800;
	}

	/**
	 * Method called before the RSS components are added. Can be used for extra information gathering
	 * prior to calls to {@link #getTitle()}, {@link #getDescription()}, {@link #getLink()}, {@link #getBuildDate()},
	 * {@link #getPubDate()}, {@link #getTTL} and {@link #getItems()}
	 *
	 * @param params The parameters passed to the page
	 */
	public void onBeforeAddRSSComponents(PageParameters params) {

	}

	/**
	 * Gets the feed title
	 * @return The feed title
	 */
	protected abstract String getTitle();

	/**
	 * Gets the description of the feed
	 * @return The feed description
	 */
	protected abstract String getDescription();

	/**
	 * Gets a permanent link to the RSS feed
	 * @return The link
	 */
	protected abstract String getLink();

	/**
	 * Gets a list of RSS items representing the feed
	 * @return The items
	 */
	protected abstract List<RSSItem> getItems();

	/**
	 * Returns the date this feed version was built
	 * @return A date
	 */
	protected abstract Date getBuildDate();

	/**
	 * Returns the date this feed version was published
	 * @return A date
	 */
	protected abstract Date getPubDate();

	@Override
	public MarkupType getMarkupType() {
		return new MarkupType("rss", "application/rss+xml");
	}

	/**
	 * Feed item, an entry in the RSS feed
	 */
	protected static class RSSItem implements Serializable {
		private static final long serialVersionUID = 1L;

		private String title;

		private String description;

		private String link;

		private String guid;

		private Date pubDate;

		private String author;

		private String category;

		/**
		 * Constructor
		 */
		public RSSItem() {
		}

		/**
		 * Get the title of the item
		 * @return The title
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * Sets the title of the item
		 * @param title The title
		 */
		public void setTitle(@Nonnull String title) {
			this.title = title;
		}

		/**
		 * Gets the description of the item
		 * @return The description
		 */
		public String getDescription() {
			return description;
		}

		/**
		 * Sets the description of the item
		 * @param description The description
		 */
		public void setDescription(@Nonnull String description) {
			this.description = description;
		}

		/**
		 * Gets the link of the item
		 * @return The link
		 */
		public String getLink() {
			return link;
		}

		/**
		 * Sets the link of item
		 * @param link The link
		 */
		public void setLink(@Nonnull String link) {
			this.link = link;
		}

		/**
		 * Gets the GUID of the item
		 * @return The guid
		 */
		public String getGuid() {
			return guid;
		}

		/**
		 * Sets the GUID of the item
		 * @param guid The GUID
		 */
		public void setGuid(@Nonnull String guid) {
			this.guid = guid;
		}

		/**
		 * Gets the publication date of the item
		 * @return The publication date
		 */
		public Date getPubDate() {
			return pubDate;
		}

		/**
		 * Sets the publication date of the item
		 * @param pubDate The publication date
		 */
		public void setPubDate(@Nonnull Date pubDate) {
			this.pubDate = pubDate;
		}

		/**
		 * Gets the author of the item
		 * @return The author
		 */
		public String getAuthor() {
			return author;
		}

		/**
		 * Sets the author of the item
		 * @param author The item
		 */
		public void setAuthor(@Nonnull String author) {
			this.author = author;
		}

		/**
		 * Gets the category of the item
		 * @return The category
		 */
		public String getCategory() {
			return category;
		}

		/**
		 * Sets the category of the item
		 * @param category The category
		 */
		public void setCategory(@Nonnull String category) {
			this.category = category;
		}

		/**
		 * Create a builder for a new RSS item
		 * @param title The title of the item
		 * @return A builder
		 */
		@Nonnull
		public static WithDescription newItem(@Nonnull String title) {
			return description -> author -> category -> link -> guid -> pubDate -> {
				RSSItem item = new RSSItem();
				item.setTitle(title);
				item.setDescription(description);
				item.setAuthor(author);
				item.setCategory(category);
				item.setGuid(guid);
				item.setLink(link);
				item.setPubDate(pubDate);

				return item;
			};
		}

		/**
		 * Builder
		 */
		@FunctionalInterface
		public interface WithDescription {
			/**
			 * Sets the description of the item
			 * @param description The description
			 * @return A builder
			 */
			@Nonnull
			WithAuthor withDescription(@Nonnull String description);
		}

		/**
		 * Builder
		 */
		@FunctionalInterface
		public interface WithAuthor {
			/**
			 * Sets the author of the item
			 * @param author The author
			 * @return A builder
			 */
			@Nonnull
			WithCategory withAuthor(@Nonnull String author);
		}

		/**
		 * Builder
		 */
		@FunctionalInterface
		public interface WithCategory {
			/**
			 * Sets the category of the item
			 * @param category The category
			 * @return A builder
			 */
			@Nonnull
			WithLink withCategory(@Nonnull String category);
		}

		/**
		 * Builder
		 */
		@FunctionalInterface
		public interface WithLink {
			/**
			 * Sets the link of the item
			 * @param link The description
			 * @return A builder
			 */
			@Nonnull
			WithGuid withLink(@Nonnull String link);
		}

		/**
		 * Builder
		 */
		@FunctionalInterface
		public interface WithGuid {
			/**
			 * Sets the guid of the item
			 * @param guid The guid
			 * @return A builder
			 */
			@Nonnull
			WithPubDate withGuid(@Nonnull String guid);
		}

		/**
		 * Builder
		 */
		@FunctionalInterface
		public interface WithPubDate {
			/**
			 * Sets the publication date of the item
			 * @param pubDate The publication date
			 * @return A newly constructed RSS item
			 */
			@Nonnull
			RSSItem withPubDate(@Nonnull Date pubDate);
		}
	}

	private static class NonNullLabel extends Label {
		private static final long serialVersionUID = 6168260732608248559L;

		NonNullLabel(String id, String contents) {
			this(id, Model.of(contents));
		}

		NonNullLabel(String id, IModel<String> contents) {
			super(id, contents);
		}

		@Override
		protected void onConfigure() {
			super.onConfigure();

			setVisibilityAllowed(getDefaultModelObject() != null);
		}
	}
}
