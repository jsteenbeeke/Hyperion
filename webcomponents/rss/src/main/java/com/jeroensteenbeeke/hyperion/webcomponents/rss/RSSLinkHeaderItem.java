/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.rss;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.google.common.collect.Lists;

/**
 * Header item that includes a {@code <link rel="alternative">} tag that links to an
 * abstract RSS page
 */
public class RSSLinkHeaderItem extends HeaderItem {
	private static final long serialVersionUID = 1L;

	private final Class<? extends AbstractRSSPage> rssPageClass;

	private final PageParameters params;

	private final String uniqueIdentifier;

	/**
	 * Constructor
	 * @param rssPageClass The RSS page subclass to link to
	 * @param params Parameters to use to construct the URL
	 * @param uniqueIdentifier A unique identifier for this item
	 */
	public RSSLinkHeaderItem(Class<? extends AbstractRSSPage> rssPageClass,
			PageParameters params, String uniqueIdentifier) {
		this.rssPageClass = rssPageClass;
		this.params = params;
		this.uniqueIdentifier = uniqueIdentifier;
	}

	@Override
	public Iterable<?> getRenderTokens() {
		return Lists.newArrayList(uniqueIdentifier);
	}

	@Override
	public void render(Response response) {
		response.write("<link href=");
		response.write(RequestCycle.get().urlFor(rssPageClass, params));
		response.write(" rel=\"alternate\" type=\"application/rss+xml\" />");
	}

}
