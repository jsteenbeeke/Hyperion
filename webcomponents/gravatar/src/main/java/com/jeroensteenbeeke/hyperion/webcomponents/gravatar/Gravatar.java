/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.gravatar;

import java.util.List;
import java.util.Optional;

import com.jeroensteenbeeke.hyperion.util.Asserts;
import com.jeroensteenbeeke.lux.TypedResult;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebComponent;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.jeroensteenbeeke.hyperion.util.HashUtil;

import javax.annotation.Nonnull;

/**
 * Displays a globally recognized avatar ( https://en.gravatar.com/ )
 */
public class Gravatar extends WebComponent {
	/**
	 * Which Gravatar alternative to use if no gravatar exists for a user
	 */
	public enum Alternative {
		/**
		 * The default image for when a user has no gravatar
		 */
		FileNotFound("404"),
		/**
		 * A silhouette image, which does not vary
		 */
		MysteryMan("mm"),
		/**
		 * A geometric pattern based on an email hash
		 */
		Identicon,
		/**
		 * A generated 'monster' with different colors, faces, etc
		 */
		MonsterID,
		/**
		 * Generated faces with differing features and backgrounds
		 */
		Wavatar,
		/**
		 * A generated robot with different colors, faces, etc
		 */
		Robohash,
		/**
		 * Awesome generated, 8-bit arcade-style pixelated faces
		 */
		Retro,
		/**
		 * A transparent PNG image
		 */
		Blank;

		private final String overrideToString;

		Alternative() {
			this.overrideToString = null;
		}

		Alternative(String overrideToString) {
			this.overrideToString = overrideToString;
		}

		@Override
		public String toString() {

			return overrideToString != null ? overrideToString : super
					.toString().toLowerCase();
		}
	}

	/**
	 * The rating of gravatars. Can be used to expand the range of images displayed
	 */
	public enum Rating {
		/**
		 * Suitable for display on all websites with any audience type
		 */
		g,
		/**
		 * May contain rude gestures, provocatively dressed individuals, the lesser swear words, or mild violence.
		 */
		pg,
		/**
		 * May contain such things as harsh profanity, intense violence, nudity, or hard drug use.
		 */
		r,
		/**
		 * May contain hardcore sexual imagery or extremely disturbing violence
		 */
		x;
	}

	private static final long serialVersionUID = 1L;

	private final String email;

	private Integer size = null;

	private Alternative alternative = null;

	private boolean forceAlternative = false;

	private boolean secure = true;

	private Rating rating = Rating.g;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param email The e-mail address for which we want to request a Gravatar
	 */
	public Gravatar(String id, String email) {
		super(id);
		this.email = email;
	}

	/**
	 * Sets the alternative to use when no gravatar is configured for the user
	 * @param alternative The alternative to use
	 * @return The current gravatar
	 */
	public Gravatar setAlternative(@Nonnull Alternative alternative) {
		this.alternative = alternative;
		return this;
	}

	/**
	 * Sets the alternative to use, regardless of whether or not the user has an actual gravatar configured
	 * @param alternative The alternative to use
	 * @return The current gravatar
	 */
	public Gravatar setForcedAlternative(@Nonnull Alternative alternative) {
		this.alternative = alternative;
		this.forceAlternative = true;
		return this;
	}

	/**
	 * Sets the desired size of the image
	 * @param size The size in pixels, may be any positive integer
	 * @return The current gravatar
	 */
	public Gravatar setSize(int size) {
		this.size = Asserts.numberParam("size").withValue(size).atLeast(1);
		return this;
	}

	/**
	 * Sets the maximum rating we're willing to accept. If not specified, defaults to g
	 * @param rating The maximum rating
	 * @return The current gravatar
	 */
	public Gravatar setRating(Rating rating) {
		this.rating = rating;
		return this;
	}

	/**
	 * Enables or disables HTTPS. Is enabled by default
	 * @param secure {@code true} if HTTPS should be used for the gravatar. {@code false} otherwise
	 * @return The current gravatar
	 */
	public Gravatar setSecure(boolean secure) {
		this.secure = secure;
		return this;
	}

	@Override
	protected void onComponentTag(ComponentTag tag) {
		checkComponentTag(tag, "img");
		super.onComponentTag(tag);

		StringBuilder url = new StringBuilder(
				secure ? "https://secure.gravatar.com/avatar/"
						: "http://www.gravatar.com/avatar/");
		url.append(createHash(email));

		List<String> extraParams = getExtraParams();
		if (!extraParams.isEmpty()) {
			url.append("?");
			Joiner.on("&").appendTo(url, extraParams);
		}

		tag.put("src", url.toString());
	}

	private List<String> getExtraParams() {
		ImmutableList.Builder<String> builder = ImmutableList.builder();
		if (size != null) {
			builder.add(String.format("s=%d", size));
		}
		if (alternative != null) {
			builder.add(String.format("d=%s", alternative));
			if (forceAlternative) {
				builder.add("f=y");
			}
		}
		if (rating != Rating.g) {
			builder.add(String.format("r=%s", rating));
		}

		return builder.build();
	}

	// Gravatar uses MD5, not much to be done about it
	@SuppressWarnings("deprecation")
	private String createHash(String email) {
		return Optional.ofNullable(email).map(String::trim).map(String::toLowerCase).map(HashUtil::md5Hash).filter(
				TypedResult::isOk).map(TypedResult::getObject).orElse("");
	}

}
