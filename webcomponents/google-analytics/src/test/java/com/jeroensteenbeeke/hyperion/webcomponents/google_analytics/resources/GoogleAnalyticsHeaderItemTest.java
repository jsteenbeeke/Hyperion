package com.jeroensteenbeeke.hyperion.webcomponents.google_analytics.resources;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class GoogleAnalyticsHeaderItemTest {
	@Test
	public void testJavaScriptRendered() {
		WicketTester tester = new WicketTester();
		tester.startPage(new GoogleAnalyticsHeaderItemPage(false));
		tester.assertRenderedPage(GoogleAnalyticsHeaderItemPage.class);
		tester.assertContains("function\\(i,s,o,g,r,a,m\\)");
		tester.assertContainsNot("displayfeatures");

		tester.startPage(new GoogleAnalyticsHeaderItemPage(true));
		tester.assertRenderedPage(GoogleAnalyticsHeaderItemPage.class);
		tester.assertContains("function\\(i,s,o,g,r,a,m\\)");
		tester.assertContains("displayfeatures");

	}
}
