package com.jeroensteenbeeke.hyperion.webcomponents.google_analytics;

import org.apache.wicket.Component;
import org.apache.wicket.application.IComponentInstantiationListener;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class TrackedGALinkTest implements IComponentInstantiationListener,
		GoogleAnalyticsTrackingIdProvider {
	@Test
	public void testTrackedLink() {
		WicketTester tester = new WicketTester();
		tester.getApplication().getComponentInstantiationListeners()
			  .add(this);

		tester.startPage(new TrackedGALinkPage());
		tester.assertRenderedPage(TrackedGALinkPage.class);
		tester.assertContains("trackOutboundLink\\(&#039;http://www.a.com&#039;\\); return " +
				"false\">a");
		tester.assertContains("trackOutboundLink\\(&#039;http://www.b.com&#039;\\); return " +
				"false\">b");
		tester.assertContains("trackOutboundLink\\(&#039;http://www.c.com&#039;\\); return " +
				"false\">C");
		tester.assertContains("trackOutboundLink\\(&#039;http://www.d.com&#039;\\); return " +
				"false\">D");
		tester.assertContains("var trackOutboundLink = function\\(url\\)");
	}

	@Override
	public void onInstantiation(Component component) {
		if (component instanceof TrackedGALink) {
			TrackedGALink link = (TrackedGALink) component;
			link.trackingIdProvider = this;
		}
	}

	@Override
	public String getGoogleAnalyticsTrackingId() {
		return "1337";
	}
}
