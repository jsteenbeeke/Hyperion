/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.google_analytics.resources;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.request.Response;

import com.google.common.collect.Sets;

/**
 * Google analytics functionality. Includes a script block that initializes
 * GA for the current page
 */
public class GoogleAnalyticsHeaderItem extends HeaderItem {
	private static final long serialVersionUID = 1L;

	private final String trackingId;

	private final boolean displayFeatures;

	/**
	 * Creates a new Google Analytics header item
	 * @param trackingId The tracking ID as defined by Google Analytics
	 */
	public GoogleAnalyticsHeaderItem(String trackingId) {
		this(trackingId, false);
	}

	/**
	 * Creates a new Google Analytics header item
	 * @param trackingId The tracking ID as defined by Google Analytics
	 * @param displayFeatures Whether or not to enable the displayFeatures plugin
	 */
	public GoogleAnalyticsHeaderItem(String trackingId, boolean displayFeatures) {
		this.trackingId = trackingId;
		this.displayFeatures = displayFeatures;
	}

	@Override
	public Iterable<?> getRenderTokens() {
		return Sets.newHashSet("google_analytics");
	}

	@Override
	public void render(Response response) {
		response.write("<script>\n");
		response.write("(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n");
		response.write("(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n");
		response.write("m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n");
		response.write("})(window,document,'script','//www.google_analytics.com/analytics.js','ga');\n\n");
		response.write("ga('create', '");
		response.write(trackingId);

		response.write("', 'auto');\n");
		if (displayFeatures) {
			response.write("ga('require', 'displayfeatures');\n");
		}
		response.write("ga('send', 'pageview');\n\n");
		response.write("</script>\n");
	}

}
