/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.google_analytics;

import javax.inject.Inject;

import com.jeroensteenbeeke.hyperion.webcomponents.google_analytics.resources
		.GoogleAnalyticsTrackOutgoingHeaderItem;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.model.IModel;

/**
 * External link whose onclick triggers a registration with Google Analytics
 */
public class TrackedGALink extends ExternalLink {
	private static final long serialVersionUID = 1L;

	@Inject
	GoogleAnalyticsTrackingIdProvider trackingIdProvider;

	/**
	 * Constructor.
	 *
	 * @param id
	 *            See Component
	 * @param href
	 *            the href attribute to set
	 * @param label
	 *            the label (body)
	 */
	public TrackedGALink(String id, IModel<String> href, IModel<?> label) {
		super(id, href, label);

		addOnClickAttribute(href.getObject());
	}

	/**
	 * Constructor.
	 *
	 * @param id
	 *            See Component
	 * @param href
	 *            the href attribute to set
	 */
	public TrackedGALink(String id, IModel<String> href) {
		super(id, href);

		addOnClickAttribute(href.getObject());
	}

	/**
	 * Constructor.
	 *
	 * @param id
	 *            See Component
	 * @param href
	 *            the href attribute to set
	 * @param label
	 *            the label (body)
	 */
	public TrackedGALink(String id, String href, String label) {
		super(id, href, label);

		addOnClickAttribute(href);
	}

	/**
	 * Constructor.
	 *
	 * @param id
	 *            See Component
	 * @param href
	 *            the href attribute to set
	 */
	public TrackedGALink(String id, String href) {
		super(id, href);

		addOnClickAttribute(href);
	}

	@Override
	protected boolean getStatelessHint() {
		return true;
	}

	private void addOnClickAttribute(String href) {
		add(new GoogleAnalyticsOnClickBehavior(href));
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		response.render(new GoogleAnalyticsTrackOutgoingHeaderItem(
				trackingIdProvider.getGoogleAnalyticsTrackingId()));
	}

	private static class GoogleAnalyticsOnClickBehavior extends Behavior {
		private static final long serialVersionUID = 1L;

		private final String originalUrl;

		private GoogleAnalyticsOnClickBehavior(String originalUrl) {
			this.originalUrl = originalUrl;
		}

		@Override
		public void bind(Component component) {
			super.bind(component);

			component.add(AttributeModifier.replace("onclick", String.format(
					"trackOutboundLink('%s'); return false", originalUrl)));
		}
	}
}
