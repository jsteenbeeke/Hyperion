/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.google_analytics.resources;

import java.util.List;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.request.Response;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

/**
 * Google analytics functionality for tracking outgoing links. These are normally
 * not covered by Google Analytics
 */
public class GoogleAnalyticsTrackOutgoingHeaderItem extends HeaderItem {
	private static final long serialVersionUID = 1L;

	private final String trackingId;

	/**
	 * Creates a new outgoing items header item with the given tracking ID
	 * @param trackingId The tracking ID as defined by Google Analytics
	 */
	public GoogleAnalyticsTrackOutgoingHeaderItem(String trackingId) {
		this.trackingId = trackingId;

	}

	@Override
	public List<HeaderItem> getDependencies() {
		return ImmutableList.<HeaderItem>of(new GoogleAnalyticsHeaderItem(trackingId));
	}

	@Override
	public Iterable<?> getRenderTokens() {
		return Sets.newHashSet("google_analytics-track-outgoing");
	}

	@Override
	public void render(Response response) {
		response.write("<script>\n");
		response.write("\tvar trackOutboundLink = function(url) {\n");
		response.write("\t\tif (ga.hasOwnProperty('loaded') && ga.loaded === true) {\n");
		response.write("\t\t\tga('send', 'event', 'outbound', 'click', url, {'hitCallback':\n");
		response.write("\t\t\t\tfunction () {\n");
		response.write("\t\t\t\t\tdocument.location = url;\n");
		response.write("\t\t\t\t}\n");
		response.write("\t\t\t});\n");
		response.write("\t\t} else {\n");
		response.write("\t\t\t// If Google Analytics are blocked, redirect without tracking\n");
		response.write("\t\t\tdocument.location = url;\n");
		response.write("\t\t}\n");
		response.write("\t}\n");
		response.write("</script>\n");
	}

}
