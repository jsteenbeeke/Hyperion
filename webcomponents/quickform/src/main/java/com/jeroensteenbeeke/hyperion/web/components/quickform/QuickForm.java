/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.web.components.quickform;

import java.lang.reflect.Field;
import java.util.*;
import java.util.Map.Entry;

import com.jeroensteenbeeke.hyperion.web.components.quickform.annotation.FormField;
import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import org.apache.wicket.Page;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeroensteenbeeke.hyperion.annotation.Annotations;

/**
 * Implementation of a Wicket form for people who hate editing HTML. Provides basic layout for common controls and
 * allows the user to create a form simply by making an anonymous inner class extending this class, and adding the
 * desired controls as fields with @FormField annotation
 *
 * @param <T> The type of object being edited
 */
public abstract class QuickForm<T> extends TypedPanel<T> {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(QuickForm.class);

	private final List<ComponentProvider> componentProviders = new LinkedList<ComponentProvider>();

	private final Map<String, String> requiredFields = new HashMap<String, String>();

	protected static final String FIELD_DROPDOWN = "dropdown";

	protected static final String FIELD_CHECKBOX = "checkbox";

	protected static final String FIELD_TEXT = "textfield";

	protected static final String FIELD_TEXTAREA = "textarea";

	protected static final String FIELD_PASSWORD = "password";

	protected static final String FIELD_EMAIL = "email";

	protected static final String FIELD_URL = "url";

	protected static final String FIELD_NUMBER = "number";
	
	protected static final String FIELD_DATE = "date";
	
	protected static final String FIELD_DATETIME = "datetime";

	private Form<T> internalForm;

	/**
	 * Constructor
	 * @param id The wicket:id
	 */
	public QuickForm(String id) {
		super(id);
		scanComponents();
		addComponents();
	}

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model containing the form's main object
	 */
	public QuickForm(String id, IModel<T> model) {
		super(id, model);
		scanComponents();
		addComponents();
		onFormComponentsAdded();
	}

	private static Map<String, Object> getMessageKeys(String fieldName) {
		Map<String, Object> work = new HashMap<String, Object>();

		work.put("field", fieldName);

		return Collections.unmodifiableMap(work);
	}

	/**
	 * Gets the Wicket form used internally. Generally required to link submit links
	 * @return A form
	 */
	public Form<T> getInternalForm() {
		return internalForm;
	}

	private void addComponents() {
		internalForm = new Form<T>("quickForm") {
			private static final long serialVersionUID = 1L;

			protected void onSubmit() {
				boolean proceed = true;

				Page p = getPage();

				for (Entry<String, String> e : requiredFields.entrySet()) {
					String path = e.getKey();
					String label = e.getValue();
					FormComponent<?> c = (FormComponent<?>) p.get(path);

					if (c instanceof DropDownChoice) {
						DropDownChoice<?> dd = (DropDownChoice<?>) c;
						if (dd.isNullValid())
							continue;
					}

					if (c.getModelObject() == null) {

						error(getLocalizer().getString(
								"com.jeroensteenbeeke.hyperion.web.components.quickform.field.required", QuickForm.this),
								getMessageKeys(label));
						proceed = false;
					}
				}

				if (proceed)
					QuickForm.this.onSubmit();
			}
		};

		internalForm.add(new ListView<ComponentProvider>("elements",
				componentProviders) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<ComponentProvider> item) {

				boolean match = false;

				ComponentData data = item.getModelObject().getComponent();

				Border border = createFormFieldBorder("wrap", data);

				match |= checkFormField(FIELD_CHECKBOX, data.getComponent(),
						border);
				match |= checkFormField(FIELD_DROPDOWN, data.getComponent(),
						border);
				match |= checkFormField(FIELD_PASSWORD, data.getComponent(),
						border);
				match |= checkFormField(FIELD_TEXT, data.getComponent(), border);
				match |= checkFormField(FIELD_TEXTAREA, data.getComponent(),
						border);
				match |= checkFormField(FIELD_EMAIL, data.getComponent(),
						border);
				match |= checkFormField(FIELD_URL, data.getComponent(), border);
				match |= checkFormField(FIELD_NUMBER, data.getComponent(),
						border);
				match |= checkFormField(FIELD_DATE, data.getComponent(),
						border);
				match |= checkFormField(FIELD_DATETIME, data.getComponent(),
						border);

				if (!match) {
					throw new IllegalStateException("Invalid component id "
							+ data.getComponent().getId() + " in QuickForm");
				}

				border.getBodyContainer().add(
						decorateFormComponent(data.getComponent()));
				item.add(border);

				FormField formField = data.getAnnotations().getAnnotation(
						FormField.class).orElseThrow(IllegalStateException::new);

				if (formField.required()) {
					requiredFields.put(data.getComponent()
							.getPageRelativePath(), formField.label());
				}

			}

			private boolean checkFormField(String id,
					FormComponent<?> component, Border border) {
				boolean match = false;

				if (id.equals(component.getId())) {
					match = true;
				} else {
					border.getBodyContainer().add(
							new WebMarkupContainer(id).setVisible(false));
				}
				return match;
			}
		});

		add(internalForm);
	}

	/**
	 * Callback method to add extra logic to components
	 * @param component The component to decorate
	 * @return The component, with extra decoration
	 */
	protected FormComponent<?> decorateFormComponent(FormComponent<?> component) {
		return component;
	}
	
	private void scanComponents() {
		scanComponents(getClass());
		
		componentProviders.addAll(getCustomProviders());
	}

	private void scanComponents(Class<?> target) {
		Field[] fields = target.getDeclaredFields();

		for (Field field : fields) {
			checkAnnotation(target, field);
		}
		if (target.getSuperclass() != QuickForm.class) {
			scanComponents(target.getSuperclass());
		}
	}

	private void checkAnnotation(Class<?> targetClass, Field field) {
		if (field.isAnnotationPresent(FormField.class)) {
			checkFormComponent(targetClass, field);
		}
	}

	private void checkFormComponent(Class<?> targetClass, Field field) {
		if (FormComponent.class.isAssignableFrom(field.getType())) {
			componentProviders.add(new ReflectionComponentProvider(targetClass, field
					.getName()));
		}
	}

	/**
	 * Plugin method that allows for extra component providers to be added
	 * @return A list of ComponentProvider instances. Defaults to an empty list
	 */
	protected List<ComponentProvider> getCustomProviders() {
		return new ArrayList<>();
	}

	/**
	 * Callback. Called when all form components have been added
	 */
	public void onFormComponentsAdded() {

	}

	/**
	 * Callback for form submit
	 */
	protected abstract void onSubmit();

	@Override
	protected void onDetach() {
		super.onDetach();

		for (ComponentProvider provider : componentProviders) {
			provider.detach();
		}
	}

	/**
	 * Creates a border for the given field
	 * @param id The wicket:id of the border
	 * @param data The data about the component being bordered
	 * @return A Border
	 */
	protected Border createFormFieldBorder(String id, ComponentData data) {
		return new FormFieldBorder(id, data.getComponent(),
				data.getAnnotations());
	}

	private class ReflectionComponentProvider implements ComponentProvider {
		private static final long serialVersionUID = 1L;
		
		private final Class<?> targetClass;
		
		private final String field;

		public ReflectionComponentProvider(Class<?> targetClass, String field) {
			this.targetClass = targetClass;
			this.field = field;
		}

		@Override
		public ComponentData getComponent() {
			try {
				Field f = targetClass.getDeclaredField(field);
				f.setAccessible(true);

				FormComponent<?> component = (FormComponent<?>) f
						.get(QuickForm.this);
				f.setAccessible(false);

				return new ComponentData(component, Annotations.fromField(f));
			} catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
				log.error(e.getMessage(), e);
				throw new WicketRuntimeException(e);
			}
		}

		@Override
		public void detach() {

		}
	}

	/**
	 * Provider of components
	 */
	public interface ComponentProvider extends IDetachable {
		/**
		 * Gets the component, including relevant annotations
		 * @return An instance of ComponentData
		 */
		ComponentData getComponent();
	}

	/**
	 * Data about a component in a QuickForm
	 */
	public static class ComponentData {
		private final FormComponent<?> component;

		private final Annotations annotations;

		private ComponentData(FormComponent<?> component,
				Annotations annotations) {
			super();
			this.component = component;
			this.annotations = annotations;
		}

		/**
		 * Gets the form component
		 * @return The component
		 */
		public FormComponent<?> getComponent() {
			return component;
		}

		/**
		 * Gets any annotations decorating the component
		 * @return Annotations
		 */
		public Annotations getAnnotations() {
			return annotations;
		}
	}
}
