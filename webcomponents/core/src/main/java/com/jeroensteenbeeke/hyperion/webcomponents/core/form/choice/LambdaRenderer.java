/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableFunction;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * IChoiceRenderer implementation that retrieves display values based on a Function
 * @param <T> The type of entity being rendered
 */
public class LambdaRenderer<T> implements IChoiceRenderer<T>, NaiveRenderer<T> {

	private static final long serialVersionUID = 1L;

	private final SerializableFunction<T, String> function;

	private LambdaRenderer(SerializableFunction<T, String> function) {
		super();
		this.function = function;
	}

	/**
	 * Create a new renderer using the given function
	 * @param function The function that transforms the entity to a unique String
	 * @param <T> The type of entity
	 * @return A renderer
	 */
	public static <T> IChoiceRenderer<T> of(
			SerializableFunction<T, String> function) {
		return new LambdaRenderer<>(function);
	}

	/**
	 * Creates a specialized renderer for the given choices, that caches the renderer result
	 * @param choices The available choices
	 * @param function The function to determine the display value, should yield a unique String for each value
	 * @param <T> The type of entity we're rendering. This should be as Serializable non-entity type
	 * @return A renderer
	 */
	public static <T> IChoiceRenderer<T> forChoices(List<T> choices,
			SerializableFunction<T, String> function) {
		return new LambdaChoiceRenderer<>(choices, function);
	}

	/**
	 * Creates a renderer for a given Enum class, caching the results
	 * @param enumClass The class of enum we're rendering
	 * @param function The function to get a display value, this should yield a unique String
	 * @param <T> The type of enum. The enum should follow the rules of serializability
	 * @return A renderer
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Enum<T>> IChoiceRenderer<T> forEnum(
			Class<T> enumClass, SerializableFunction<T, String> function) {
		T[] values;
		try {
			values = (T[]) enumClass.getMethod("values").invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			return of(function);
		}

		return new LambdaChoiceRenderer<>(Lists.newArrayList(values),
				function);
	}

	@Override
	public Object getDisplayValue(T object) {
		return function.apply(object);
	}

	@Override
	public String getIdValue(T object, int index) {
		return function.apply(object);
	}

}

class LambdaChoiceRenderer<T> implements IChoiceRenderer<T> {
	private static final long serialVersionUID = 1L;

	private final Map<T, String> values;

	private final Map<String, T> ids;

	LambdaChoiceRenderer(List<T> choices,
			SerializableFunction<T, String> function) {
		values = Maps.newHashMapWithExpectedSize(choices.size());
		ids = Maps.newHashMapWithExpectedSize(choices.size());

		choices.forEach(t -> {
			String id = function.apply(t);
			values.put(t, id);
			ids.put(id, t);
		});

	}

	@Override
	public Object getDisplayValue(T object) {
		return values.get(object);
	}

	@Override
	public String getIdValue(T object, int index) {
		return values.get(object);
	}

	@Override
	public T getObject(String id, IModel<? extends List<? extends T>> choices) {
		return ids.get(id);
	}

	@Override
	public void detach() {
		values.forEach((k,v) -> checkDetach(k));
		ids.forEach((k,v) -> checkDetach(v));
	}

	private void checkDetach(T t) {
		if (t instanceof IDetachable) {
			IDetachable d = (IDetachable) t;
			d.detach();
		}
	}
}
