package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.cycle.RequestCycle;
import org.danekja.java.misc.serializable.SerializableRunnable;

import javax.annotation.Nonnull;

/**
 * Convenience interface that allows a Wicket render operation
 * to be extended with additional HTML tags
 */
public interface HTMLWrapper {
	/**
	 * Output HTML to the Response before and after the given operation
	 *
	 * @param prefix     The HTML to place before the current component
	 * @param runBetween The actual render command. Supposing you call this method from
	 * {@link MarkupContainer#onRender()} this
	 *                   should be equal to {@code () -> super.onRender()}
	 * @param suffix     The HTML to place after the current component
	 */
	default void wrapRenderedHTML(@Nonnull String prefix, @Nonnull SerializableRunnable runBetween,
								  @Nonnull String suffix) {
		Response response = RequestCycle.get().getResponse();
		response.write(prefix);
		runBetween.run();
		response.write(suffix);
	}
}
