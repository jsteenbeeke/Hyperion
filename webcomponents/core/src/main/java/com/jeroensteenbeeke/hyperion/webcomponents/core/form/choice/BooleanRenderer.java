/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * IChoiceRenderer for displaying boolean options as textual representations such as Yes or No
 */
public class BooleanRenderer implements NaiveRenderer<Boolean> {
	private static final long serialVersionUID = 1L;

	private final String trueOption;

	private final String falseOption;

	/**
	 * Create a new BooleanRenderer for the given true and false options
	 * @param trueOption The text to display when the value is {@code true}
	 * @param falseOption The text to display when the value is {@code false}
	 */
	public BooleanRenderer(@Nonnull String trueOption, @Nonnull String falseOption) {
		super();

		if (Objects.equals(trueOption, falseOption)) {
			throw new IllegalArgumentException(
					"True and false options may not be equal!");
		}

		this.trueOption = trueOption;
		this.falseOption = falseOption;
	}

	@Override
	public Object getDisplayValue(Boolean object) {
		return object != null && object ? trueOption
				: falseOption;
	}

	@Override
	public String getIdValue(Boolean object, int index) {
		return object != null && object ? trueOption
				: falseOption;
	}

}
