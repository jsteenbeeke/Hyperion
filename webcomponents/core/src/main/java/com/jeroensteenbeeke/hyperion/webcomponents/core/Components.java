/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.core;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.danekja.java.util.function.serializable.SerializableConsumer;

/**
 * Utility class for Wicket components
 */
public final class Components {
	/**
	 * Iterates over the ListItem objects within a listView, and applies the given consumer to them
	 * @param listview The listview
	 * @param consumer The consumer
	 * @param <T> The type of object contained in the ListView
	 */
	public static <T> void forEach(ListView<T> listview,
			SerializableConsumer<ListItem<T>> consumer) {
		extractItems(listview).forEach(consumer);
	}

	/**
	 * Extracts all items from a ListView and returns them as a list
	 * @param listview The listview
	 * @param <T> The type of object contained in the ListView
	 * @return A list of ListItems
	 */
	public static <T> List<ListItem<T>> extract(ListView<T> listview) {
		return extract(listview, Function.identity());
	}

	/**
	 * Extracts all items from a ListView, applies a transformation, and returns the transformed objects as a list
	 * @param listview The listview
	 * @param conversion The transformation
	 * @param <T> The ListView's list element type
	 * @param <N> The target type
	 * @return A list of N
	 */
	public static <T, N> List<N> extract(ListView<T> listview,
			Function<ListItem<T>, N> conversion) {
		return extractItems(listview).map(conversion).filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	@SuppressWarnings("unchecked")
	private static <T> Stream<ListItem<T>> extractItems(ListView<T> listview) {
		Iterator<Component> ci = listview.iterator();

		return StreamSupport
				.stream(Spliterators.spliteratorUnknownSize(ci,
						Spliterator.ORDERED), false)
				.filter(i -> i instanceof ListItem).map(i -> (ListItem<T>) i);
	}

}
