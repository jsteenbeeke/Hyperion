/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Optional;

import org.apache.wicket.Session;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.danekja.java.util.function.serializable.SerializableFunction;

/**
 * TextField implementation for displaying currencies
 * @param <N> The type of number to display
 */
public class CurrencyTextField<N extends Number> extends TextField<String> {
	private static final long serialVersionUID = 1L;

	private IModel<N> numberModel;

	private NumberFormat format;

	private SerializableFunction<Double,N> doubleToNumber;

	/**
	 * Create a new currency field
	 * @param id The wicket ID
	 * @param value The value of the currency
	 * @param doubleToNumber A function to convert a Double to the given type. NumberFormat
	 *                          converts input to double
	 */
	public CurrencyTextField(String id, N value, SerializableFunction<Double,N> doubleToNumber) {
		this(id, Model.of(value), doubleToNumber);
	}

	/**
	 * Create a new currency field
	 * @param id The wicket ID
	 * @param model The model containing the value of the currency
	 * @param doubleToNumber A function to convert a Double to the given type. NumberFormat
	 *                          converts input to double
	 */
	public CurrencyTextField(String id, IModel<N> model, SerializableFunction<Double,N> doubleToNumber) {
		super(id);

		this.numberModel = model;
		this.doubleToNumber = doubleToNumber;

		Locale locale = Session.get().getLocale();

		format = NumberFormat.getNumberInstance(locale);
		format.setMinimumFractionDigits(2);
		format.setMaximumFractionDigits(2);
		format.setRoundingMode(RoundingMode.HALF_UP);

		setModel(new IModel<>() {

			private static final long serialVersionUID = 865156231409568331L;

			@Override
			public String getObject() {
				return format.format(numberModel.getObject());
			}

			@Override
			public void setObject(String object) {
				numberModel.setObject(
					Optional.ofNullable(object)
							.map(CurrencyTextField.this::parseNumber)
							.map(Number::doubleValue)
							.map(doubleToNumber)
							.orElse(null));
			}
		});
	}

	private Number parseNumber(String number) {
		try {
			return format.parse(number);
		} catch (ParseException e) {
			throw new WicketRuntimeException(e);
		}
	}

	/**
	 * Return the modelObject converted to the number type of this field
	 * @return The parsed number
	 */
	public N getModelObjectAsNumber() {
		return numberModel.getObject();
	}

	@Override
	protected void onDetach() {
		super.onDetach();
		numberModel.detach();
	}
}
