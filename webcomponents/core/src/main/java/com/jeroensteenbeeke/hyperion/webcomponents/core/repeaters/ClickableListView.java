/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import java.util.List;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * Listview implementation that makes each item clickable
 *
 * @param <T> The type of object contained in the listview
 */
public abstract class ClickableListView<T> extends ListView<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new ClickableListView
	 * @param id The wicket component ID
	 * @param model The model of the listview
	 */
	public ClickableListView(String id,
			IModel<? extends List<T>> model) {
		super(id, model);
	}

	/**
	 * Creates a new ClickableListView
	 * @param id The wicket component ID
	 * @param list The list to put in the listview
	 */
	public ClickableListView(String id, List<T> list) {
		this(id, Model.ofList(list));
	}

	/**
	 * Callback method that is called when an item is clicked
	 * @param item The item that was clicked
	 */
	protected abstract void onClick(ListItem<T> item);

	@Override
	protected final ListItem<T> newItem(int index, IModel<T> itemModel) {

		return new LinkItem<>(this, index, itemModel);
	}

}
