package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LambdaModel;
import org.danekja.java.util.function.serializable.SerializableBiConsumer;
import org.danekja.java.util.function.serializable.SerializableFunction;

import javax.annotation.Nonnull;

/**
 * Wicket panel base class that can be used to construct forms for a single entity or DTO, where
 * each form field represents a field on the base object.
 * @param <T> The base object, generally an entity, REST object or DTO
 */
public abstract class EditorPanel<T> extends TypedPanel<T> {
	private static final long serialVersionUID = -7358785407809141199L;

	/**
	 * Constructor
	 * @param id The component ID corresponding to the HTML wicket:id
	 * @param model The model for the base object
	 */
	public EditorPanel(String id, IModel<T> model) {
		super(id, model);
	}

	/**
	 * Starts constructing a model for a field that has a simple getter/setter pair
	 * @param fieldClass The class of the field
	 * @param <R> The type of field
	 * @return A builder
	 */
	@Nonnull
	public <R> WithGetter<T, R> field(@Nonnull Class<R> fieldClass) {
		return getter -> setter -> LambdaModel.of(getModel(), getter, setter);
	}

	/**
	 * Builder
	 * @param <T> Base object type
	 * @param <R> Field type
	 */
	@FunctionalInterface
	protected interface WithGetter<T, R> {
		/**
		 * Indicate that the given function is the getter for this field
		 * @param getter The getter (generally a method reference)
		 * @return A builder
		 */
		@Nonnull
		WithSetter<T, R> withGetter(@Nonnull SerializableFunction<T, R> getter);
	}

	/**
	 * Builder
	 * @param <T> Base object type
	 * @param <R> Field type
	 */
	@FunctionalInterface
	protected interface WithSetter<T, R> {
		/**
		 * Indicate that the given function is a setter for this field
		 * @param setter The setter (generally a method reference)
		 * @return The resulting model
		 */
		@Nonnull
		IModel<R> withSetter(@Nonnull SerializableBiConsumer<T, R> setter);
	}

	/**
	 * Starts constructing a model for a field with getter/setter logic, but a different display format. Useful for entity
	 * relations
	 *
	 * @param targetClass The class as displayed in the user interface
	 * @param fieldClass The class the field is stored as
	 * @param <UI_FIELD_TYPE> The type as shown in the user interface
	 * @param <OBJECT_FIELD_TYPE> The type as stored
	 * @return A builder
	 */
	public <UI_FIELD_TYPE, OBJECT_FIELD_TYPE> WithComputedGetter<T, UI_FIELD_TYPE, OBJECT_FIELD_TYPE> computed(@Nonnull Class<UI_FIELD_TYPE> targetClass,
																											   @Nonnull Class<OBJECT_FIELD_TYPE> fieldClass) {
		return getter -> outputToTarget -> setter -> targetToInput -> LambdaModel.of(getModel(), getter.andThen(v -> v != null ? outputToTarget
				.apply(v) : null), (t, ui) -> setter
				.accept(t, targetToInput.apply(ui)));
	}

	/**
	 * Builder
	 * @param <T> The base object type
	 * @param <UI_FIELD_TYPE> The type as shown in the user interface
	 * @param <OBJECT_FIELD_TYPE> The type as stored
	 */
	@FunctionalInterface
	protected interface WithComputedGetter<T, UI_FIELD_TYPE, OBJECT_FIELD_TYPE> {
		/**
		 * Defines the getter from the base object
		 * @param getter The getter (generally a method reference)
		 * @return A builder
		 */
		WithGetterTransformation<T, UI_FIELD_TYPE, OBJECT_FIELD_TYPE> withGetter(@Nonnull SerializableFunction<T, OBJECT_FIELD_TYPE> getter);
	}

	/**
	 * Builder
	 * @param <T> The base object type
	 * @param <UI_FIELD_TYPE> The type as shown in the user interface
	 * @param <OBJECT_FIELD_TYPE> The type as stored
	 */
	@FunctionalInterface
	protected interface WithGetterTransformation<T, UI_FIELD_TYPE, OBJECT_FIELD_TYPE> {
		/**
		 * Sets the transformation from base object field type to ui type
		 * @param toTargetType The transformation to convert from OBJECT_FIELD_TYPE to UI_FIELDTYPE
		 * @return A builder
		 */
		WithComputedSetter<T, UI_FIELD_TYPE, OBJECT_FIELD_TYPE> transformedBy(@Nonnull SerializableFunction<OBJECT_FIELD_TYPE, UI_FIELD_TYPE> toTargetType);
	}

	/**
	 * Builder
	 * @param <T> The base object type
	 * @param <OBJECT_FIELD_TYPE> The type as stored
	 * @param <UI_FIELDTYPE> The type as shown in the user interface
	 */
	@FunctionalInterface
	protected interface WithComputedSetter<T, UI_FIELDTYPE, OBJECT_FIELD_TYPE> {
		/**
		 * Sets the setter for setting the object field on the base object
		 * @param setter The setter (generally a method reference)
		 * @return A builder
		 */
		WithSetterTransformation<T, UI_FIELDTYPE, OBJECT_FIELD_TYPE> withSetter(@Nonnull SerializableBiConsumer<T, OBJECT_FIELD_TYPE> setter);
	}

	/**
	 * Builder
	 * @param <T> The base object type
	 * @param <UI_FIELD_TYPE>
	 * @param <OBJECT_FIELD_TYPE>
	 */
	@FunctionalInterface
	protected interface WithSetterTransformation<T, UI_FIELD_TYPE, OBJECT_FIELD_TYPE> {
		/**
		 * Sets the transformation to convert from UI_FIELD_TYPE back to OBJECT_FIELD_TYPE
		 * @param targetToInput The transformation
		 * @return The resulting model
		 */
		IModel<UI_FIELD_TYPE> transformedBy(@Nonnull SerializableFunction<UI_FIELD_TYPE, OBJECT_FIELD_TYPE> targetToInput);
	}
}
