package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class ClickableListViewTest {
	@Test
	public void testClickableListView() {
		WicketTester tester = new WicketTester();

		tester.startPage(new ClickableListViewTestPage());
		tester.assertRenderedPage(ClickableListViewTestPage.class);

		// Simulate item click
		@SuppressWarnings("unchecked")
		LinkItem<String> item = (LinkItem<String>) tester.getComponentFromLastRenderedPage("items:0");
		tester.executeListener(item);
		tester.assertRenderedPage(LinkTargetPage.class);
		tester.assertLabel("label", "A");

	}
}
