package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EditorPanelTest {
	@Test
	public void testEditorPanelModelAndFormSubmit() {
		WicketTester tester = new WicketTester();
		tester.startPage(new EditorPanelTestPage());
		tester.assertRenderedPage(EditorPanelTestPage.class);

		tester.assertComponent("editor", TestEditorPanel.class);
		tester.assertComponent("editor:name", TextField.class);
		tester.assertComponent("editor:cardinality", NumberTextField.class);

		@SuppressWarnings("unchecked")
		TextField<String> nameField = (TextField<String>) tester.getComponentFromLastRenderedPage("editor:name");
		assertEquals("", nameField.getModelObject());
		nameField.setModelObject("naam");
		assertEquals("naam", nameField.getModelObject());

		@SuppressWarnings("unchecked")
		TextField<String> factorField = (TextField<String>) tester.getComponentFromLastRenderedPage("editor:factor");
		assertNull(factorField.getModelObject());
		factorField.setModelObject("HIGH");
		assertEquals("HIGH", factorField.getModelObject());

		@SuppressWarnings("unchecked")
		NumberTextField<Integer> cardinalityField = (NumberTextField<Integer>) tester.getComponentFromLastRenderedPage("editor:cardinality");
		assertEquals(Integer.valueOf(0), cardinalityField.getModelObject());
		cardinalityField.setModelObject(5);
		assertEquals(Integer.valueOf(5), cardinalityField.getModelObject());
	}
}
