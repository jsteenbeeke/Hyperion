package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

public class TimeTextFieldTest {
	@Test
	public void testTimeTextField() {
		WicketTester tester = new WicketTester();

		tester.startPage(new TimeTextFieldTestPage());
		tester.assertRenderedPage(TimeTextFieldTestPage.class);

		tester.assertContains("input type=\"time\"");
	}
}
