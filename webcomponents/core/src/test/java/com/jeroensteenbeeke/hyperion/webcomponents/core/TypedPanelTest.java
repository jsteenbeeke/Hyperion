package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.model.Model;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TypedPanelTest {
	@Test
	public void createTypedPanel() {
		WicketTester tester = new WicketTester();
		tester.startPage(new TypedPanelTestPage());
		tester.assertRenderedPage(TypedPanelTestPage.class);

		@SuppressWarnings("unchecked")
		TestTypedPanel<FooBar> panel = (TestTypedPanel<FooBar>)
				tester.getComponentFromLastRenderedPage("test2");

		assertNotNull(panel);

		assertEquals(FooBar.BAR, panel.getModelObject());
		panel.setModelObject(FooBar.FOO);
		assertEquals(FooBar.FOO, panel.getModelObject());
		panel.setModel(Model.of(FooBar.BAR));
		assertEquals(FooBar.BAR, panel.getModelObject());
		assertNotNull(panel.getModel());
		assertEquals(FooBar.BAR, panel.getModel().getObject());

	}
}
