package com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice;

import com.google.common.collect.Lists;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.ListModel;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BooleanRendererTest {
	@Test
	public void testBooleanRenderer() {
		BooleanRenderer br = new BooleanRenderer("Yes", "No");

		assertEquals("Yes", br.getDisplayValue(true));
		assertEquals("No", br.getDisplayValue(null));
		assertEquals("No", br.getDisplayValue(false));

		assertEquals("Yes", br.getIdValue(true, 0));
		assertEquals("No", br.getIdValue(null, 0));
		assertEquals("No", br.getIdValue(false, 0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgument() {
		new BooleanRenderer("Yes", "Yes");
	}

	@Test
	public void testValueFromList() {
		BooleanRenderer br = new BooleanRenderer("Yes", "No");
		IModel<List<Boolean>> choices = new ListModel<>(Lists.newArrayList(true, false));

		assertEquals(true, br.getObject("Yes", choices));
		assertEquals(false, br.getObject("No", choices));
		assertNull(br.getObject("Foo", choices));
	}
}
