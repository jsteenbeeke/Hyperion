package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.Session;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class CurrencyTextFieldTest {
	static final BigDecimal FIVE_POINT_FIVE = new BigDecimal(55)
			.divide(BigDecimal.TEN, 1, RoundingMode.HALF_UP);
	static final BigDecimal TWELVE_POINT_FIVE = new BigDecimal(125)
			.divide(BigDecimal.TEN, 1, RoundingMode.HALF_UP);


	@Test
	public void testDutchLocale() {
		WicketTester tester = new WicketTester();
		tester.startPage(new CurrencyTextFieldTestPage());
		tester.assertRenderedPage(CurrencyTextFieldTestPage.class);

		Session.get().setLocale(Locale.forLanguageTag("nl-NL"));
		tester.startPage(new CurrencyTextFieldTestPage());
		tester.assertRenderedPage(CurrencyTextFieldTestPage.class);

		tester.assertComponent("currency", CurrencyTextField.class);
		@SuppressWarnings("unchecked")
		CurrencyTextField<BigDecimal> field =
				(CurrencyTextField<BigDecimal>) tester.getComponentFromLastRenderedPage("currency");


		assertEquals(FIVE_POINT_FIVE, field.getModelObjectAsNumber());

		field.setModelObject("12,5");

		assertEquals(TWELVE_POINT_FIVE, field.getModelObjectAsNumber());
	}

	@Test(expected = WicketRuntimeException.class)
	public void testUnparseable() {
		WicketTester tester = new WicketTester();
		Session.get().setLocale(Locale.forLanguageTag("nl-NL"));
		tester.startPage(new CurrencyTextFieldTestPage());
		tester.assertRenderedPage(CurrencyTextFieldTestPage.class);

		tester.assertComponent("currency", CurrencyTextField.class);
		@SuppressWarnings("unchecked")
		CurrencyTextField<BigDecimal> field =
				(CurrencyTextField<BigDecimal>) tester.getComponentFromLastRenderedPage("currency");


		assertEquals(FIVE_POINT_FIVE, field.getModelObjectAsNumber());

		field.setModelObject("!!12~5");
	}
}
