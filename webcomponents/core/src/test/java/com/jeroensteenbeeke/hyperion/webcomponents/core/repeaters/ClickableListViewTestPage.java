package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import com.google.common.collect.Lists;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;

public class ClickableListViewTestPage extends WebPage {
	public ClickableListViewTestPage() {
		add(new ClickableListView<String>("items", Lists.newArrayList("A", "B", "C")) {
			@Override
			protected void populateItem(ListItem<String> item) {
				item.add(new Label("label", item.getModelObject()));
			}

			@Override
			protected void onClick(ListItem<String> item) {
				ClickableListViewTestPage.this.setResponsePage(new LinkTargetPage(item.getModelObject()));
			}
		});
	}
}
