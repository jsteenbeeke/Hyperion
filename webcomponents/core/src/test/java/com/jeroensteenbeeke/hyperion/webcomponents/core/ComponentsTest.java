package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ComponentsTest {
	@Test
	public void testComponents() {
		WicketTester tester = new WicketTester();

		ComponentsTestPage page;
		tester.startPage(page = new ComponentsTestPage());
		tester.assertRenderedPage(ComponentsTestPage.class);

		assertEquals(4, Components.extract(page.items).size());
		assertEquals(4, Components.extract(page.items, ListItem::getModelObject).size());

		Components.forEach(page.items, i -> assertTrue(i.getModelObject() instanceof String));
	}
}
