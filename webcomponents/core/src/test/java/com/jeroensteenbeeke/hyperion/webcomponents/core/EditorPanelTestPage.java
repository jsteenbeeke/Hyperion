package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.markup.html.WebPage;

public class EditorPanelTestPage extends WebPage {

	private static final long serialVersionUID = 3708050100333798914L;

	public EditorPanelTestPage() {
		BarFoo barfoo = new BarFoo();
		barfoo.setCardinality(0);
		barfoo.setName("");

		add(new TestEditorPanel("editor", barfoo));
	}
}
