package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.markup.html.WebPage;

import java.math.BigDecimal;


public class CurrencyTextFieldTestPage extends WebPage {
	public CurrencyTextFieldTestPage() {
		add(new CurrencyTextField<>("currency",
				CurrencyTextFieldTest.FIVE_POINT_FIVE, BigDecimal::valueOf));
	}
}
