package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;

public class TypedPanelTestPage extends WebPage {
	public TypedPanelTestPage() {
		add(new TestTypedPanel<FooBar>("test"));
		add(new TestTypedPanel<>("test2", Model.of(FooBar.BAR)));
	}
}

enum FooBar {
	FOO, BAR
}
