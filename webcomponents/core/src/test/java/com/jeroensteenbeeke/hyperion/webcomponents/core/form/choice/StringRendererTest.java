package com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringRendererTest {
	@Test
	public void testStringRenderer() {
		StringChoiceRenderer r = new StringChoiceRenderer();

		assertEquals("Foo", r.getDisplayValue("Foo"));
		assertEquals("Bar", r.getDisplayValue("Bar"));
		assertEquals("Foo", r.getIdValue("Foo", 0));
		assertEquals("Bar", r.getIdValue("Bar", 1));
	}
}
