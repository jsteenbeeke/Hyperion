package com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice;

import com.google.common.collect.Lists;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.ListModel;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class LambdaRendererTest {
	@Test
	public void testLambdaRenderer() {
		IChoiceRenderer<Object> r = LambdaRenderer.of(Object::toString);

		assertEquals("5", r.getDisplayValue(5));
		assertEquals("5", r.getIdValue(5, 0));

		r.detach();
	}

	@Test
	public void testEnumRenderer() {
		IChoiceRenderer<FooBar> r = LambdaRenderer.forEnum(FooBar.class, FooBar::name);

		assertEquals("FOO", r.getDisplayValue(FooBar.FOO));
		assertEquals("BAR", r.getDisplayValue(FooBar.BAR));
		assertEquals("FOO", r.getIdValue(FooBar.FOO, 0));
		assertEquals("BAR", r.getIdValue(FooBar.BAR, 1));


		r = LambdaRenderer.forEnum(FooBar.class, f -> Integer.toString(f.ordinal()));

		assertEquals("0", r.getDisplayValue(FooBar.FOO));
		assertEquals("1", r.getDisplayValue(FooBar.BAR));
		assertEquals("0", r.getIdValue(FooBar.FOO, 0));
		assertEquals("1", r.getIdValue(FooBar.BAR, 1));

		r.detach();
	}

	@Test
	public void testValueFromList() {
		List<FooBar> c = Lists.newArrayList(FooBar.FOO, FooBar.BAR);

		IChoiceRenderer<FooBar> r = LambdaRenderer.forChoices(c, FooBar::name);
		IModel<List<FooBar>> choices = new ListModel<>(c);

		assertEquals(FooBar.FOO, r.getObject("FOO", choices));
		assertEquals(FooBar.BAR, r.getObject("BAR", choices));
		assertNull(r.getObject("!!", choices));

		r.detach();
	}

}

enum FooBar implements IDetachable {
	FOO, BAR;


	@Override
	public void detach() {

	}
}
