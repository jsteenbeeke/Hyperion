/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.cycler;

import org.apache.wicket.resource.JQueryPluginResourceReference;

/**
 * Wicket reference to the Content Cycler JavaScript
 */
public class ContentCyclerScriptReference extends JQueryPluginResourceReference {
	private static final long serialVersionUID = 1L;

	private static final ContentCyclerScriptReference REFERENCE = new ContentCyclerScriptReference();

	private ContentCyclerScriptReference() {
		super(ContentCyclerScriptReference.class, "js/contentcycler.js");
	}

	/**
	 * Get the reference
	 * @return The reference
	 */
	public static ContentCyclerScriptReference get() {
		return REFERENCE;
	}

}
