/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.cycler;

import java.util.List;

import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;

import com.jeroensteenbeeke.hyperion.data.DomainObject;

/**
 * Widget that displays a list of content items in sequence, showing
 * each element for a set time and then fading to the next.
 *
 * @param <T> The type of entity to show
 */
public abstract class ContentCycler<T extends DomainObject> extends
		TypedPanel<List<T>> {

	private static final long serialVersionUID = 1L;
	private final long delay;

	private final long fade;

	private final int minHeight;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model containing the entities
	 * @param minHeight The minimum height of the content
	 * @param delay How long to display each item, in milliseconds
	 * @param fade How long to make the fadeout animation last, in milliseconds
	 */
	public ContentCycler(String id, IModel<List<T>> model, int minHeight,
						 long delay, long fade) {
		super(id, model);
		setOutputMarkupId(true);

		setVisible(!model.getObject().isEmpty());

		this.delay = delay;
		this.fade = fade;
		this.minHeight = minHeight;

		add(AttributeModifier.append("class", "hyperion-cycler"));

		add(new ListView<T>("items", model) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<T> item) {
				T object = item.getModelObject();

				TypedPanel<T> contentPanel = createContentPanel("item", object);
				contentPanel.add(AttributeModifier.append("class",
						"hyperion-cycler-item"));
				if (item.getIndex() == 0) {
					contentPanel.add(AttributeModifier.append("class",
							"hyperion-cycler-item-active"));
				}

				item.add(contentPanel);

			}
		});
	}

	/**
	 * Whether or not the include the default CSS
	 * @return {@code true} if the default CSS should be included. {@code false} otherwise
	 */
	public boolean includeDefaultCss() {
		return true;
	}

	/**
	 * Creates a panel to display the given object
	 * @param id The wicket:id for the panel
	 * @param object The object to create the panel for
	 * @return A panel that displays the object
	 */
	public abstract TypedPanel<T> createContentPanel(String id, T object);

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		response.render(JavaScriptHeaderItem
				.forReference(ContentCyclerScriptReference.get()));
		if (includeDefaultCss()) {
			response.render(CssHeaderItem
					.forReference(ContentCyclerStyleReference.get()));
		}

		StringBuilder script = new StringBuilder();
		script.append("$('#").append(getMarkupId()).append("').contentCycler(")
				.append(minHeight).append(", ").append(delay).append(", ")
				.append(fade).append(");\n");

		response.render(OnDomReadyHeaderItem.forScript(script));
	}

}
