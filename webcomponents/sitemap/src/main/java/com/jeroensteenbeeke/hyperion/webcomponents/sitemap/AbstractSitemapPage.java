/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.sitemap;

import com.jeroensteenbeeke.hyperion.util.TimeUtil;
import com.jeroensteenbeeke.hyperion.util.XMLable;
import org.apache.wicket.Component;
import org.apache.wicket.markup.MarkupType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.List;

/**
 * Page that contains a sitemap
 */
public abstract class AbstractSitemapPage extends WebPage {
	/**
	 * Model that contains an XMLable
	 *
	 * @param <T> The type of XMLable
	 */
	public static class XMLableModel<T extends XMLable> implements
		IModel<String> {
		private static final long serialVersionUID = 1L;

		private final T xmlable;

		/**
		 * Constructor
		 *
		 * @param xmlable The model content
		 */
		public XMLableModel(T xmlable) {
			this.xmlable = xmlable;
		}

		@Override
		public String getObject() {

			return xmlable != null ? xmlable.toXmlValue() : null;
		}
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(new ListView<>("entries", getEntries()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<SitemapEntry> item) {
				SitemapEntry entry = item.getModelObject();

				item.add(new Label("url", entry.getUrl()));
				item.add(new NonNullLabel("lastmod",
										  entry.getLastModification().flatMap(TimeUtil::formatYearMonthDay)
											   .orElse(null)));

				item.add(new NonNullLabel("changefreq",
										  entry.getChangeFrequency().<IModel<String>>map(XMLableModel::new)
											  .orElseGet(Model::of)
				));
				item.add(new NonNullLabel("priority",

										  entry.getPriority().<IModel<String>>map(XMLableModel::new)
											  .orElseGet(Model::of)));
			}
		});
	}

	/**
	 * Gets the list of Sitemap entries
	 *
	 * @return A list of entries
	 */
	public abstract List<SitemapEntry> getEntries();

	@Override
	public final MarkupType getMarkupType() {
		return new MarkupType("xml", "application/xml");
	}

	private static class NonNullLabel extends Label {
		private static final long serialVersionUID = 7340260927632296729L;

		NonNullLabel(String id, String contents) {
			this(id, Model.of(contents));
		}

		NonNullLabel(String id, IModel<String> contents) {
			super(id, contents);
		}

		@Override
		public boolean isVisible() {
			return super.isVisible() && getDefaultModelObject() != null;
		}
	}
}
