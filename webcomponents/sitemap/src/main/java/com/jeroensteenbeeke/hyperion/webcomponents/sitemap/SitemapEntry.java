/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.sitemap;

import java.io.Serializable;
import java.util.Optional;

import org.joda.time.DateTime;

import javax.annotation.Nonnull;

/**
 * Entry within a sitemap
 */
public class SitemapEntry implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String url;

	private DateTime lastModification = null;

	private UpdateFrequency changeFrequency = null;

	private Priority priority = null;

	/**
	 * Creates a new entry
	 * @param url The URL
	 */
	public SitemapEntry(@Nonnull String url) {
		this.url = url;
	}

	/**
	 * Sets the last modified time
	 * @param dateTime The last changed time
	 * @return This entry
	 */
	public SitemapEntry lastModifiedOn(@Nonnull DateTime dateTime) {
		this.lastModification = dateTime;
		return this;
	}

	/**
	 * Sets the change frequency
	 * @param freq The frequency with which this entry is updated
	 * @return This entry
	 */
	public SitemapEntry withChangeFrequency(@Nonnull UpdateFrequency freq) {
		this.changeFrequency = freq;
		return this;
	}

	/**
	 * Sets the priority
	 * @param priority The priority
	 * @return This entry
	 */
	public SitemapEntry withPriority(@Nonnull Priority priority) {
		this.priority = priority;
		return this;
	}

	@Nonnull
	String getUrl() {
		return url;
	}

	@Nonnull
	Optional<DateTime> getLastModification() {
		return Optional.ofNullable(lastModification);
	}

	@Nonnull
	Optional<UpdateFrequency> getChangeFrequency() {
		return Optional.ofNullable(changeFrequency);
	}

	@Nonnull
	Optional<Priority> getPriority() {
		return Optional.ofNullable(priority);
	}

}
