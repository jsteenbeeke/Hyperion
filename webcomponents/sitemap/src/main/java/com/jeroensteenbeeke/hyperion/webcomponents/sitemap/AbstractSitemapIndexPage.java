/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.sitemap;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.wicket.markup.MarkupType;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.joda.time.ReadableInstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Page that represents a sitemap index ( https://www.sitemaps.org/protocol.html#index )
 */
public abstract class AbstractSitemapIndexPage extends WebPage {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory
			.getLogger(AbstractSitemapIndexPage.class);

	/**
	 * Constructor
	 */
	public AbstractSitemapIndexPage() {
		add(new ListView<>("maps", getEntries()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<SitemapIndexEntry> item) {
				SitemapIndexEntry entry = item.getModelObject();

				item.add(new Label("loc", entry.getUrl()));
				ReadableInstant lastModification = entry.getLastModification();

				item.add(new WebMarkupContainer("lastmod").setVisible(false));

				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(new Date(lastModification.getMillis()));

				try {
					XMLGregorianCalendar xmlCal = DatatypeFactory
							.newInstance().newXMLGregorianCalendar(cal);

					item.addOrReplace(new Label("lastmod", xmlCal
							.toXMLFormat()));
				} catch (DatatypeConfigurationException e) {
					log.error(e.getMessage(), e);
				}

			}
		});
	}

	/**
	 * Gets a list of index entries
	 * @return A list of sitemap index entries
	 */
	protected abstract List<SitemapIndexEntry> getEntries();

	@Override
	public final MarkupType getMarkupType() {
		return new MarkupType("xml", "application/xml");
	}
}
