package com.jeroensteenbeeke.hyperion.social.beans.linkedin;

import com.github.scribejava.apis.LinkedInApi20;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import io.vavr.control.Option;

import javax.annotation.Nonnull;

/**
 * Handler class for LinkedIn integration. Applications that use LinkedIn integration
 * are required to implement this class
 */
public abstract class LinkedInHandler {
	private String urlPrefix;

	/**
	 * Returns the OAuth client ID of the LinkedIn application
	 * @return The client ID
	 */
	@Nonnull
	public abstract String getClientId();

	/**
	 * Returns the OAuth client secret of the LinkedIn application
	 * @return The client secret
	 */
	@Nonnull
	public abstract String getClientSecret();

	/**
	 * Returns the base URL of the application
	 * @return The base URL
	 */
	@Nonnull
	public abstract String getApplicationBaseUrl();

	/**
	 * Callback method for errors
	 * @param message The error that just occurred
	 */
	public abstract void onError(@Nonnull String message);

	/**
	 * Returns the URL prefix as set by the integration
	 * @return the url prefix
	 */
	public String getUrlPrefix() {
		return urlPrefix;
	}

	/**
	 * Sets the URL prefix required for the integration
	 * @param urlPrefix the url prefix
	 */
	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix.startsWith("/") ? urlPrefix : "/".concat(urlPrefix);
	}

	/**
	 * Returns the required OAuth scopes (space separated). Defaults to {@code r_basicprofile}
	 * @return The scopes
	 */
	@Nonnull
	public String getScopes() {
		return "r_basicprofile";
	}

	/**
	 * Creates a new scribe service for communicating with LinkedIn
	 * @param state The state string to send along (should be randomized) to prevent replay attacks
	 * @return The service object
	 */
	@Nonnull
	public final OAuth20Service createService(String state) {
		return new ServiceBuilder()
				.apiKey(getClientId())
				.apiSecret(getClientSecret())
				.scope(getScopes())
				.state(state)
				.callback(
						String.format("%s%s/linkedin/callback",
								getApplicationBaseUrl(), getUrlPrefix()))
				.build(LinkedInApi20.instance());
	}

	/**
	 * Callback method for when an access token has been acquired
	 * @param service The service to use for follow-up requests
	 * @param accessToken The access token to use for these requests
	 */
	public abstract void onAccessTokenReceived(@Nonnull OAuth20Service service, @Nonnull OAuth2AccessToken accessToken);

	/**
	 * Returns the user state (the randomized state String provided to prevent replay attacks).
	 * This should be session/user-specific. Responsibility for making the data session/user-specific
	 * lies with the implementor.
	 *
	 * @return Optionally the state
	 */
	@Nonnull
	public abstract Option<String> getUserState();

	/**
	 * Sets the user state for the current user. This should be session/user-spcific. Responsibility for making the data session/user-specific
	 * lies with the implementor.
	 * @param state The state to set
	 */
	public abstract void setUserState(@Nonnull String state);

}
