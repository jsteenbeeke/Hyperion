package com.jeroensteenbeeke.hyperion.social.web.pages.linkedin;

import java.io.IOException;

import javax.inject.Inject;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.util.string.StringValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.scribejava.core.oauth.OAuth20Service;
import com.jeroensteenbeeke.hyperion.social.beans.linkedin.LinkedInHandler;

/**
 * Wicket callback page for LinkedIn OAuth flow
 */
public class LinkedInCallbackPage extends WebPage {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory
		.getLogger(LinkedInCallbackPage.class);

	@Inject
	private LinkedInHandler linkedInHandler;

	/**
	 * Constructor
	 */
	public LinkedInCallbackPage() {
		StringValue stateParam = getRequest().getQueryParameters()
											 .getParameterValue("state");
		StringValue codeParam = getRequest().getQueryParameters()
											.getParameterValue("code");

		if (stateParam.isEmpty() || codeParam.isEmpty()) {
			onError("Expected both state and code query parameters");
			return;
		}

		String state = stateParam
			.toOptionalString();

		linkedInHandler.getUserState().peek(expectedState -> {
			if (!expectedState.equals(state)) {
				onError("Provided state parameter does not match expected state");
				return;
			}

			OAuth20Service service = linkedInHandler.createService(state);

			try {
				linkedInHandler.onAccessTokenReceived(service, service.getAccessToken(codeParam.toOptionalString()));
			} catch (IOException e) {
				onError(e.getMessage());
			}
		}).onEmpty(() -> onError("Provided state parameter does not match expected state"));


	}

	private void onError(String message) {
		log.error(String.format("Twitter login failed: %s", message));

		linkedInHandler.onError(message);

	}

}
