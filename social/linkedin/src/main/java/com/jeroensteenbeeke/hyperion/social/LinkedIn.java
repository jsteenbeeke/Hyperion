package com.jeroensteenbeeke.hyperion.social;

import org.apache.wicket.protocol.http.WebApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.jeroensteenbeeke.hyperion.social.beans.linkedin.LinkedInHandler;
import com.jeroensteenbeeke.hyperion.social.web.pages.linkedin.LinkedInCallbackPage;
import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationContextProvider;

/**
 * Initializer for LinkedIn integration
 */
public enum LinkedIn implements SocialMediumInitializer {
	/**
	 * Singleton instance
	 */
	integration;
	
	private static final Logger log = LoggerFactory.getLogger(LinkedIn.class);

	@Override
	public <T extends WebApplication & ApplicationContextProvider> void initialize(
			T application, String prefix) {
		log.info("Initializing LinkedIn login support");
		
		ApplicationContext context = application.getApplicationContext();
		LinkedInHandler linkedInHandler = context.getBean(LinkedInHandler.class);
		
		linkedInHandler.setUrlPrefix(prefix);
		
		String contextRelativeUrl = prefix.concat("/linkedin/callback");
		application.mountPage(contextRelativeUrl, LinkedInCallbackPage.class);
		
		log.info("Mounted LinkedIn callback at {}", contextRelativeUrl);
		
	}
	
	
}
