package com.jeroensteenbeeke.hyperion.social.web.pages.twitter;

import javax.inject.Inject;

import org.apache.wicket.markup.html.WebPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeroensteenbeeke.hyperion.social.beans.twitter.TwitterHandler;

import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * Wicket implementation of a Twitter callback
 */
public class TwitterCallbackPage extends WebPage {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory
		.getLogger(TwitterCallbackPage.class);

	@Inject
	private TwitterHandler twitterHandler;

	/**
	 * Constructor
	 */
	public TwitterCallbackPage() {
		twitterHandler.getRequestToken().peek(
			requestToken -> {
				if (requestToken.getToken() == null
					|| requestToken.getTokenSecret() == null) {
					onError("Session reset during Twitter auth cycle");
					return;
				}

				log.info(String.format("Twitter callback for request token %s||%s",
									   requestToken.getToken(), requestToken.getTokenSecret()));

				String verifier = getRequest().getQueryParameters()
											  .getParameterValue("oauth_verifier").toString();

				if (verifier == null) {
					onError("No verifier present");
					return;
				}

				try {
					AccessToken token = twitterHandler.getTwitter()
													  .getOAuthAccessToken(requestToken, verifier);

					if (token != null) {
						twitterHandler.setRequestToken(null);

						twitterHandler.onAccessTokenAcquired(token);
					}

					onError("Could not retrieve access token");
				} catch (TwitterException e) {
					twitterHandler.handleError(e);
				}
			}).onEmpty(() -> onError("Request Token not set"));


	}

	private void onError(String message) {
		twitterHandler.onError(message);
	}

}
