package com.jeroensteenbeeke.hyperion.social.beans.twitter;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import io.vavr.control.Option;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * Controller base implementation for Twitter functionality. Implementing applications are required to
 * provide an implementation of this class for complete Twitter functionality
 */
public abstract class TwitterHandler {
	private TwitterFactory twitterFactory;
	
	private String pathPrefix = "";

	/**
	 * Sets the factory for creating Twitter objects
	 * @param twitterFactory The factory
	 */
	public void setTwitterFactory(TwitterFactory twitterFactory) {
		this.twitterFactory = twitterFactory;
	}

	/**
	 * Returns an instance of the Twitter class, which is a facade for the
	 * Twitter API
	 * @return A Twitter object
	 */
	public Twitter getTwitter() {
		return twitterFactory.getInstance();
	}

	/**
	 * Returns an instance of the Twitter class, with the given access token provider.
	 * @param provider The provider
	 * @return A Twitter object
	 */
	public Twitter getTwitter(IAccessTokenProvider provider) {
		Twitter instance = twitterFactory.getInstance();
		
		instance.setOAuthAccessToken(provider.get());
		
		return instance;
	}

	/**
	 * Returns the request token for the current session. Implementing classes
	 * should also provide the logic for determining the current session
	 *
	 * @return The request token
	 */
	@Nonnull
	public abstract Option<RequestToken> getRequestToken();

	/**
	 * Sets the request token for the current session. Implementing classes should provide logic
	 * for per-session storage.
	 * @param token The request otken
	 */
	public abstract void setRequestToken(@Nonnull RequestToken token);

	/**
	 * Callback method for actions to take upon acquiring an AccessToken
	 * @param token The token that was acquired
	 */
	public abstract void onAccessTokenAcquired(@Nonnull AccessToken token);

	/**
	 * Callback method for actions to take upon encountering an error
	 * @param message The error message(s) encountered
	 */
	public abstract void onError(@Nonnull String message);

	/**
	 * Returns the Twitter API consumer key
	 * @return The key
	 */
	@Nonnull
	public abstract String getConsumerKey();

	/**
	 * Returns the Twitter API consumer secret
	 * @return The secret
	 */
	@Nonnull
	public abstract String getConsumerSecret();

	/**
	 * Returns the OAuth callback URL for the current application
	 * @return The callback URL
	 */
	@Nonnull
	public abstract String getCallbackUrl();

	/**
	 * Method called upon encountering a Twitter exception
	 * @param e The exception
	 */
	public final void handleError(@Nonnull TwitterException e) {
		Stream.Builder<String> msg = Stream.builder();
		msg.add(String.format("[%d]", e.getAccessLevel()));
		msg.add(e.getErrorMessage());
		msg.add(e.getExceptionCode());
		msg.add(e.getMessage());
		
		Throwable ex = e;
		while (ex.getCause() != null) {
			msg.add(ex.getMessage());
			
			ex = ex.getCause();
		}
			
		onError(msg.build().filter(Objects::nonNull).collect(Collectors.joining(" ")));
	}

	/**
	 * Convenience method for constructing a callback URL from a base URL. Useful when also
	 * using the Wicket logic
	 * @param baseUrl The base URL of the application
	 * @return A complete callback URL
	 */
	public final String buildCallbackUrl(String baseUrl) {
		String prefix = baseUrl.endsWith("/") ? baseUrl : baseUrl.concat("/");
		
		return prefix.concat(pathPrefix).concat("twitter/callback");
	}

	/**
	 * Sets the current path prefix, a context-specific prefix that should always
	 * be prefixed to relative URLs
	 * @param prefix The prefix
	 */
	public final void setPathPrefix(String prefix) {
		String sanitized = prefix;
		
		if (!sanitized.isEmpty() && !sanitized.endsWith("/")) {
			sanitized = sanitized.concat("/");
		}
		while (prefix.startsWith("/")) {
			sanitized = sanitized.substring(1);
		}
		
		this.pathPrefix = sanitized;
		
	}
}
