package com.jeroensteenbeeke.hyperion.social.web.components.twitter;

import javax.inject.Inject;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.flow.RedirectToUrlException;

import com.jeroensteenbeeke.hyperion.social.beans.twitter.TwitterHandler;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.RequestToken;

/**
 * Wicket Link for initiating a Twitter login
 */
public class TwitterLink extends Link<Void> {

	private static final long serialVersionUID = 1L;

	@Inject
	private TwitterHandler twitterHandler;

	/**
	 * Constructor
	 * @param id The wicket:id
	 */
	public TwitterLink(String id) {
		super(id);
	}

	@Override
	public void onClick() {
		Twitter twitter = twitterHandler.getTwitter();

		try {

			RequestToken requestToken = twitter
						.getOAuthRequestToken(twitterHandler.getCallbackUrl());

			twitterHandler.setRequestToken(requestToken);

			throw new RedirectToUrlException(
					requestToken.getAuthorizationURL());
		} catch (TwitterException e) {
			twitterHandler.handleError(e);
			
			
		}
	}

}
