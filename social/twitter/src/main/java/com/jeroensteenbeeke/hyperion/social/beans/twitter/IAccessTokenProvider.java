package com.jeroensteenbeeke.hyperion.social.beans.twitter;

import org.danekja.java.util.function.serializable.SerializableSupplier;
import twitter4j.auth.AccessToken;

/**
 * Specialized supplier that supplies an access token
 */
public interface IAccessTokenProvider extends SerializableSupplier<AccessToken> {
	
}
