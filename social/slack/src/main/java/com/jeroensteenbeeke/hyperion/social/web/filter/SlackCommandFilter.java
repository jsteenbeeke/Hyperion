package com.jeroensteenbeeke.hyperion.social.web.filter;

import com.jeroensteenbeeke.hyperion.util.HashUtil;
import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.TreeMap;
import io.vavr.control.Option;
import io.vavr.control.Try;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.function.Function;

/**
 * Abstract servlet filter for responding to custom Slack commands
 */
public abstract class SlackCommandFilter implements Filter {
	private static final String VERSION = "v0";

	private static final Logger log = LoggerFactory.getLogger(SlackCommandFilter.class);

	private static final OkHttpClient client = new OkHttpClient();

	private static boolean requireCorrectSignature = true;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		String body = readBody(httpServletRequest);

		Map<String, String> data = parseBody(body);

		Option<String> command = data.get("command");
		Option<String> response_url = data.get("response_url");

		String requestTimestamp = httpServletRequest.getHeader("X-Slack-Request-Timestamp");
		String signature = httpServletRequest.getHeader("X-Slack-Signature");

		String calculatedSignature = calculateSignature(requestTimestamp, body);

		if (!calculatedSignature.equals(signature)) {
			log.error("Signature verification failed! Expected '{}' but found '{}'", calculatedSignature, signature);
			log.error("Version: '{}'", VERSION);
			log.error("Timestamp: '{}'", requestTimestamp);
			log.error("Body: '{}'", body);

			if (requireCorrectSignature) {
				httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);

				return;
			}
		}

		PrintWriter writer = response.getWriter();
		if (command.isEmpty()) {
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);

			writer.println("Missing parameter: command");
		} else if (response_url.isEmpty()) {
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			writer.println("Missing parameter: response_url");
		} else {
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);

			onSlackCommand(new SlackCommandContext(command.get(), response_url.get(), data));
		}
	}

	private Map<String, String> parseBody(String body) {
		return TreeMap
			.ofAll(Arrays.stream(body.split("&")),
				   s -> {
					   String[] kv = s.split("=", 2);
					   return new Tuple2<>(kv[0], kv[1]);
				   }
			)
			.mapKeys(k -> URLDecoder.decode(k, StandardCharsets.UTF_8))
			.mapValues(v -> URLDecoder.decode(v, StandardCharsets.UTF_8));

	}

	private String readBody(HttpServletRequest httpServletRequest) throws IOException {
		ServletInputStream inputStream = httpServletRequest.getInputStream();
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			int i;

			while ((i = inputStream.read()) != -1) {
				bos.write(i);
			}
			bos.flush();

			return new String(bos.toByteArray());
		}

	}

	private String calculateSignature(String requestTimestamp, String body) throws IOException, ServletException {
		String signingSecret = getSignatureVerificationKey();

		if (signingSecret == null) {
			throw new ServletException("Missing verification key");
		}

		String baseString = String.join(":", VERSION, requestTimestamp, body);
		SecretKeySpec secret_key = new SecretKeySpec(signingSecret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
		Mac sha256_HMAC = Try
			.of(() -> Mac.getInstance("HmacSHA256"))
			.getOrElseThrow((Function<Throwable, ServletException>) ServletException::new);
		Try.run(() -> sha256_HMAC.init(secret_key));

		return "v0=" + HashUtil.byteArrayToString(sha256_HMAC.doFinal(baseString.getBytes()));

	}

	/**
	 * Returns the key for signature verification. This way we can check if the command was really sent by Slack
	 * @return The signature verification key
	 */
	protected abstract String getSignatureVerificationKey();

	/**
	 * Sets whether or not we're going to check the signature to see if Slack sent the request. Defaults to true. Should
	 * always be enabled unless testing locally
	 * @param enable {@code true} to enable signature verification. {@code false} to disable
	 */
	public static void setRequireCorrectSignature(boolean enable) {
		requireCorrectSignature = enable;
	}

	@Override
	public void destroy() {

	}

	/**
	 * Method that is called when a Slack command is received
	 * @param context Information about the command
	 */
	public abstract void onSlackCommand(SlackCommandContext context);

	/**
	 * Context of a given Slack command
	 */
	protected static class SlackCommandContext {
		private final String command;

		private final String responseUrl;

		private final Map<String,String> data;

		private SlackCommandContext(String command, String responseUrl, Map<String,String> data) {
			this.command = command;
			this.responseUrl = responseUrl;
			this.data = data;
		}

		/**
		 * The command that was issued
		 * @return The command
		 */
		@Nonnull
		public String getCommand() {
			return command;
		}

		/**
		 * Gets the parameter with the given name
		 * @param name The name
		 * @return Optionally the parameter value matching the given name
		 */
		@Nonnull
		public Option<String> getParameter(@Nonnull String name) {
			return data.get(name);
		}

		/**
		 * Start building a response to send to Slack's chat
		 * @param message The message to send
		 * @return A builder
		 */
		@Nonnull
		public ResponseBuilder postResponse(@Nonnull String message) {
			return new ResponseBuilder(message, responseUrl);
		}
	}

	/**
	 * Builder for creating a chat response for Slack
	 */
	protected static class ResponseBuilder {
		private final String message;

		private final String responseUrl;

		private final Array<String> attachments;

		private ResponseBuilder(String message, String responseUrl) {
			this(message, responseUrl, Array.empty());
		}

		private ResponseBuilder(String message, String responseUrl, Array<String> attachments) {
			this.message = message;
			this.responseUrl = responseUrl;
			this.attachments = attachments;
		}

		/**
		 * Adds a JSON attachment
		 * @param json The attachment
		 * @return This builder
		 */
		@Nonnull
		public ResponseBuilder withAttachment(@Nonnull String json) {
			return new ResponseBuilder(message, responseUrl, attachments.append(json));
		}

		/**
		 * Finalize the response and post it to Slack
		 * @param type The type of response
		 * @return An ActionResult, indicating either success or the reason for failure
		 */
		@Nonnull
		public ActionResult ofType(SlackResponseType type) {

			return TypedResult.attempt(() -> new Request.Builder()
				.post(RequestBody.create(MediaType.parse("application/json"), "{\n" +
					String.format("\t\"response_type\": \"%s\",\n", type.toSlackString()) +
					String.format("\t\"text\": \"%s\",\n", message.replace("\"", "\\\",")) +
					attachments.mkString("\t\"attachments\": [", ",\n\t\t", "\n\t]") +
					"}"))
				.url(responseUrl)
				.build()).flatMap(request -> {

				Response response = client.newCall(request).execute();

				if (response.isSuccessful()) {
					return TypedResult.ok(response.body().string());
				} else {
					return TypedResult.fail(response.message());
				}

			})
							  .asSimpleResult();
		}
	}

	/**
	 * How we should post the response to Slack
	 */
	protected enum SlackResponseType {
		/**
		 * Visible in the channel it was posted in
		 */
		InChannel("in_channel"),
		/**
		 * Visible only to the person issuing the command
		 */
		Ephemeral("ephemeral");

		private final String slackString;

		SlackResponseType(String slackString) {
			this.slackString = slackString;
		}

		/**
		 * Returns the current type in a format Slack understands
		 * @return The type
		 */
		public String toSlackString() {
			return slackString;
		}
	}
}
