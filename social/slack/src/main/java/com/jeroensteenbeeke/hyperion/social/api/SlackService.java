package com.jeroensteenbeeke.hyperion.social.api;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.AbstractRequest;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.oauth.OAuth20Service;

/**
 * Scribe service class for Slack
 */
public class SlackService extends OAuth20Service {

	private static final String TOKEN_PARAM = "token";

	/**
	 * Constructor
	 * @param api The API to connect to (typically an instance of SlackAPI)
	 * @param config The OAuth2 config to use
	 */
	public SlackService(DefaultApi20 api, OAuthConfig config) {
		super(api, config);
	}
	
	@Override
	public void signRequest(OAuth2AccessToken accessToken,
			AbstractRequest request) {
        request.addQuerystringParameter(TOKEN_PARAM, accessToken.getAccessToken());	
	}

}
