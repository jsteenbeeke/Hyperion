package com.jeroensteenbeeke.hyperion.social.beans.slack;

import java.io.IOException;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

import io.vavr.control.Option;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.jeroensteenbeeke.hyperion.social.api.SlackAPI;
import com.jeroensteenbeeke.lux.TypedResult;

/**
 * Handler class for Slack OAuth2 functionality. Applications that use the Slack functionality need to provide
 * an instance of this class for the integration to work
 */
public abstract class SlackHandler {
	private static final int STATUS_OK = 200;

	private static final String IDENTITY_URL = "https://slack.com/api/users.identity";
	
	private String urlPrefix;

	/**
	 * Returns the Client ID of the application
	 * @return The client ID
	 */
	@Nonnull
	public abstract String getClientId();

	/**
	 * Returns the Client Secret of the application
	 * @return The client secret
	 */
	@Nonnull
	public abstract String getClientSecret();

	/**
	 * Returns the base URL of the application
	 * @return The application base URL
	 */
	@Nonnull
	public abstract String getApplicationBaseUrl();

	/**
	 * Callback for when an error occurs
	 * @param message The error message
	 */
	public abstract void onError(@Nonnull String message);

	/**
	 * Returns the URL prefix. An absolute path to prefix to all URLs pertaining to callbacks
	 * @return the URL prefix
	 */
	public String getUrlPrefix() {
		return urlPrefix;
	}

	/**
	 * Sets the URL prefix
	 * @param urlPrefix The URL prefix
	 */
	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix.startsWith("/") ? urlPrefix : "/".concat(urlPrefix);
	}

	/**
	 * Returns a list of required OAuth2 scopes (space-separated)
	 * @return The scopes
	 */
	@Nonnull
	public String getScopes() {
		return "identity.basic";
	}

	/**
	 * Creates the OAuth2Service to use with Scribe
	 * @param state The user-specific state to use with the current flow
	 * @return A service
	 */
	@Nonnull
	public OAuth20Service createService(String state) {
		String callbackUrl = String.format("%s%s/slack/callback",
				getApplicationBaseUrl(), getUrlPrefix());
		
		while (callbackUrl.contains("//slack")) {
			callbackUrl = callbackUrl.replace("//slack", "/slack");
		}

		SlackAPI api = getSlackAPI();

		return new ServiceBuilder()
				.apiKey(getClientId())
				.apiSecret(getClientSecret())
				.scope(getScopes())
				.state(state)
				.callback(
						callbackUrl)
				.build(api);
	}

	/**
	 * Returns an instance of the SlackAPI
	 * @return A SlackAPI object
	 */
	protected SlackAPI getSlackAPI() {
		return SlackAPI.instance();
	}

	/**
	 * Returns the URL to use for fetching the user's identity after login
	 * @return The identity URL
	 */
	protected String getIdentityUrl() {
		return IDENTITY_URL;
	}

	/**
	 * Try to obtain information about the current user from Slack
	 * @param service The service to use
	 * @param accessToken The user's access token
	 * @return A TypedResult containing either the user's info as a JSON object, or an error message stating why the operation failed
	 */
	public TypedResult<JSONObject> getUserInfo(@Nonnull OAuth20Service service, @Nonnull OAuth2AccessToken accessToken) {
		final OAuthRequest request = new OAuthRequest(Verb.GET, getIdentityUrl(),
				service);
		service.signRequest(accessToken, request);
		
		final Response response = request.send();

		if (response.getCode() != STATUS_OK) {
			return TypedResult.fail("Could not request profile information");
		} else {
			JSONParser parser = new JSONParser();
			try {
				
				JSONObject profile = (JSONObject) parser.parse(response
						.getBody());
				
				return TypedResult.ok(profile);
			} catch (ParseException | IOException e) {
				return TypedResult.fail("Could not parse profile information: ".concat(e
						.getMessage()));
			}
		}

	}

	/**
	 * Method that is called when an access token has been successfully received
	 * @param service The service to use for subsequent requests
	 * @param accessToken The accessToken
	 */
	public abstract void onAccessTokenReceived(@Nonnull OAuth20Service service, @Nonnull OAuth2AccessToken accessToken);

	/**
	 * Returns the user/session-specific state for the current OAuth2 flow. Used to prevent replay attacks. Logic
	 * for making it user/session specific is the responsibility of the implementing class.
	 *
	 * @return Optionally the state.
	 */
	@Nonnull
	public abstract Option<String> getUserState();

	/**
	 * Sets the user/session-specific state for the current OAuth2 flow. Used to prevent replay attacks. Logic for making it
	 * user/session specific is the responsibility of the implementing class.
	 * @param state The state to set. This should be a unique non-guessable randomly generated String
	 */
	public abstract void setUserState(@Nonnull String state);

}
