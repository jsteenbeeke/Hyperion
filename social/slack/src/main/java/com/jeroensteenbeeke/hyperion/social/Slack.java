package com.jeroensteenbeeke.hyperion.social;

import org.apache.wicket.protocol.http.WebApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.jeroensteenbeeke.hyperion.social.beans.slack.SlackHandler;
import com.jeroensteenbeeke.hyperion.social.web.pages.slack.SlackCallbackPage;
import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationContextProvider;

/**
 * Initializer for Slack integration
 */
public enum Slack implements SocialMediumInitializer {
	/**
	 * Singleton instance
	 */
	integration;

	private static final Logger log = LoggerFactory.getLogger(Slack.class);

	@Override
	public <T extends WebApplication & ApplicationContextProvider> void initialize(
			T application, String prefix) {
		log.info("Initializing Slack login support");

		ApplicationContext context = application.getApplicationContext();
		SlackHandler slackHandler = context.getBean(SlackHandler.class);

		slackHandler.setUrlPrefix(prefix);

		String contextRelativeUrl = prefix.concat("/slack/callback");
		application.mountPage(contextRelativeUrl, SlackCallbackPage.class);

		log.info("Mounted Slack callback at {}", contextRelativeUrl);

	}

}
