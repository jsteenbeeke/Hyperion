package com.jeroensteenbeeke.hyperion.social.api;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.oauth.OAuth20Service;

/**
 * Scribe API class for Slack
 */
public class SlackAPI extends DefaultApi20 {
	private static class InstanceHolder {
        private static final SlackAPI INSTANCE = new SlackAPI();
    }

	/**
	 * Get the static instance of the Slack API
	 * @return The API
	 */
	public static SlackAPI instance() {
        return InstanceHolder.INSTANCE;
    }

	/**
	 * Constructor
	 */
	protected SlackAPI() {
	}
    
    @Override
    public String getAccessTokenEndpoint() {
    	return "https://slack.com/api/oauth.access";
    }
    
    
    @Override
    protected String getAuthorizationBaseUrl() {
    	return "https://slack.com/oauth/authorize";
    }
    
    @Override
    public OAuth20Service createService(OAuthConfig config) {
    	return new SlackService(this, config);
    }
    
}
