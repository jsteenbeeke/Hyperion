package com.jeroensteenbeeke.hyperion.social.web.pages.slack;

import java.io.IOException;

import javax.inject.Inject;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.util.string.StringValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.scribejava.core.oauth.OAuth20Service;
import com.jeroensteenbeeke.hyperion.social.beans.slack.SlackHandler;

/**
 * Callback page that handles Slack OAuth2 responses
 */
public class SlackCallbackPage extends WebPage {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory
			.getLogger(SlackCallbackPage.class);

	@Inject
	private SlackHandler slackHandler;

	/**
	 * Constructor
	 */
	public SlackCallbackPage() {
		StringValue stateParam = getRequest().getQueryParameters()
				.getParameterValue("state");
		StringValue codeParam = getRequest().getQueryParameters()
				.getParameterValue("code");

		if (stateParam.isEmpty() || codeParam.isEmpty()) {
			onError("Expected both state and code query parameters");
			return;
		}

		String state = stateParam
				.toOptionalString();
		
		if (slackHandler.getUserState().filter(state::equals).isEmpty()) {
			onError("Provided state parameter does not match expected state");
			return;
		}
		
		OAuth20Service service = slackHandler.createService(state);
		
		try {
			slackHandler.onAccessTokenReceived(service, service.getAccessToken(codeParam.toOptionalString()));
		} catch (IOException e) {
			onError(e.getMessage());
		}

	}

	private void onError(String message) {
		log.error(String.format("Twitter login failed: %s", message));

		slackHandler.onError(message);

	}

}
