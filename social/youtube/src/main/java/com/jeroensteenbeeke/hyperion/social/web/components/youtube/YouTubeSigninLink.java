package com.jeroensteenbeeke.hyperion.social.web.components.youtube;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.common.collect.ImmutableSet;
import com.jeroensteenbeeke.hyperion.social.beans.youtube.YouTubeHandler;
import com.jeroensteenbeeke.hyperion.util.Randomizer;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.flow.RedirectToUrlException;

import javax.inject.Inject;

/**
 * Link for initiating an OAuth2 login flow with YouTube
 */
public class YouTubeSigninLink extends Link<Void> {
	private static final long serialVersionUID = 1L;

	private static final String ACCESS_TYPE_OFFLINE = "offline";

	private static final String RESPONSE_TYPE_CODE = "code";

	@Inject
	private YouTubeHandler handler;

	/**
	 * Constructor
	 *
	 * @param id The wicket:id
	 */
	public YouTubeSigninLink(String id) {
		super(id);

	}

	@Override
	public void onClick() {
		handler
			.createAuthFlow().peek(authFlow -> {

			GoogleAuthorizationCodeRequestUrl authUrl = authFlow
				.newAuthorizationUrl();
			String state = Randomizer.random(17);
			handler.setState(state);

			authUrl.setState(state);
			authUrl.setAccessType(ACCESS_TYPE_OFFLINE);
			authUrl.setResponseTypes(ImmutableSet.of(RESPONSE_TYPE_CODE));
			authUrl.setRedirectUri(handler.getRedirectURL());

			throw new RedirectToUrlException(authUrl.build());
		}).onEmpty(() -> error("YouTube integration not properly configured"));
	}

}
