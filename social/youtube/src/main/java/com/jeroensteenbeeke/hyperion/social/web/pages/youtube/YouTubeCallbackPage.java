package com.jeroensteenbeeke.hyperion.social.web.pages.youtube;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Optional;

import javax.inject.Inject;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.util.string.StringValue;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.jeroensteenbeeke.hyperion.social.beans.youtube.YouTubeHandler;

/**
 * Callback page for YouTube OAuth2 flow
 */
public class YouTubeCallbackPage extends WebPage {
	private static final long serialVersionUID = 1L;

	private static final int MAX_ATTEMPTS = 3;

	@Inject
	private YouTubeHandler youTubeHandler;

	/**
	 * Constructor
	 */
	public YouTubeCallbackPage() {
		StringValue codeParam = getRequest().getQueryParameters()
											.getParameterValue("code");

		StringValue stateParam = getRequest().getQueryParameters()
											 .getParameterValue("state");

		if (codeParam == null || codeParam.isNull() || codeParam.isEmpty()
			|| stateParam == null || stateParam.isNull()
			|| stateParam.isEmpty()) {
			youTubeHandler.onError("Invalid response from YouTube");
			return;
		}

		String code = codeParam.toString();
		String state = stateParam.toString();

		if (youTubeHandler.getState().filter(state::equals).isEmpty()) {
			youTubeHandler.onError(String.format(
				"Provided state '%s' does not match generated state '%s'",
				state, youTubeHandler.getState()));
			return;
		}

		youTubeHandler
			.createAuthFlow().peek(flow -> {

			for (int attempt = 0; attempt <= MAX_ATTEMPTS; attempt++) {
				GoogleAuthorizationCodeTokenRequest tokenRequest = flow
					.newTokenRequest(code);

				try {
					GoogleTokenResponse tokenResponse = tokenRequest
						.setRedirectUri(youTubeHandler.getRedirectURL())
						.execute();

					String idTokenStr = tokenResponse.getIdToken();

					if (idTokenStr != null) {
						GoogleIdToken idToken = tokenResponse.parseIdToken();

						if (idToken.verify(youTubeHandler.createTokenVerifier())) {
							youTubeHandler.onAuthorized(tokenResponse, idToken);
						} else {
							youTubeHandler.onError("ID token verification failed");
						}
					} else {
						youTubeHandler.onError("ID token verification failed");
					}

				} catch (IOException ioe) {
					if (attempt >= MAX_ATTEMPTS) {
						youTubeHandler.onError(
							String.format("Unable to obtain access token: %s",
										  ioe.getMessage()));
					}
				} catch (GeneralSecurityException e) {
					youTubeHandler.onError(String.format(
						"ID token verification failed: %s", e.getMessage()));
				}
			}

			youTubeHandler.onError("Maximum login attempts exceeded");

		}).onEmpty(() -> youTubeHandler.onError("Could not create auth flow"));

	}

}
