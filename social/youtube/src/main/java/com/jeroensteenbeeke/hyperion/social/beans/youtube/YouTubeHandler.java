package com.jeroensteenbeeke.hyperion.social.beans.youtube;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import javax.annotation.Nonnull;

import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.*;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.youtube.YouTube;
import com.google.common.collect.Lists;
import com.jeroensteenbeeke.lux.TypedResult;

/**
 * Handler class for YouTube integration. Applications that use YouTube integration
 * are required to implement this class
 */
public abstract class YouTubeHandler {
	private static final Collection<String> SCOPES = Lists
			.newArrayList("https://www.googleapis.com/auth/youtube",
					"https://www.googleapis.com/auth/userinfo.profile");

	private static final String ACCESSTYPE = "offline";

	private static final String APPROVAL_PROMPT_TYPE = "force";

	public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	public static final JsonFactory JSON_FACTORY = new JacksonFactory();

	private String pathPrefix = "";

	/**
	 * Creates an instance of the YouTube API class
	 * @param provider The credential provider
	 * @return Optionally a YouTube instance
	 */
	public final Option<YouTube> createYouTube(
			@Nonnull IYouTubeCredentialProvider provider) {
		return createYouTube(getClientSecrets().map(provider));
	}

	/**
	 * Creates an instance of the YouTube API class
	 * @param creds Optionally the credentials. If this Option is not set, an empty option is returned
	 * @return Optionally a YouTube instance
	 */
	public final Option<YouTube> createYouTube(Option<Credential> creds) {
		return creds
				.map(c -> new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, c)
						.setApplicationName(getApplicationName()).build());

	}

	/**
	 * Creates an Oauth2 instance from the given credentials provider
	 * @param provider The credentials provider
	 * @return Optionally an Oauth2 instance
	 */
	public final Option<Oauth2> createOAuth2(
			@Nonnull IYouTubeCredentialProvider provider) {
		return createOAuth2(getClientSecrets().map(provider));
	}

	/**
	 * Creates an Oauth2 instance from the given credentials
	 * @param creds Optionally the credentials. If this Option is not set, an empty option is returned
	 * @return Optionally an Oauth2
	 */
	public final Option<Oauth2> createOAuth2(Option<Credential> creds) {
		return creds
				.map(c -> new Oauth2.Builder(HTTP_TRANSPORT, JSON_FACTORY, c)
						.build());
	}

	/**
	 * Creates a Google authorization flow
	 * @return Optionally an authorization flow (if properly configured)
	 */
	public final Option<GoogleAuthorizationCodeFlow> createAuthFlow() {
		Option<GoogleClientSecrets> clientSecrets = getClientSecrets();
		return clientSecrets.map(s -> {
			GoogleAuthorizationCodeFlow.Builder builder = new GoogleAuthorizationCodeFlow.Builder(
					HTTP_TRANSPORT, JSON_FACTORY, s, SCOPES);
			builder.setAccessType(ACCESSTYPE);
			builder.setApprovalPrompt(APPROVAL_PROMPT_TYPE);

			return builder.build();

		});

	}

	/**
	 * Builds the redirect URL for the OAuth2 callback
	 * @param baseUrl The base URL of the application
	 * @return The redirect URL
	 */
	protected final String buildRedirectUrl(String baseUrl) {
		String prefix = baseUrl.endsWith("/") ? baseUrl : baseUrl.concat("/");

		return prefix.concat(pathPrefix).concat("youtube/callback");
	}

	/**
	 * Returns the name of the application
	 * @return An application name
	 */
	@Nonnull
	public abstract String getApplicationName();

	/**
	 * Gets the client secrets for this application
	 * @return The client secrets
	 */
	@Nonnull
	public abstract Option<GoogleClientSecrets> getClientSecrets();

	/**
	 * Returns the credentials for the current user, if known
	 * @return Optionally the credentials
	 */
	@Nonnull
	public abstract Option<Credential> getCredential();

	/**
	 * Returns the redirect URL of the application
	 * @return The redirect URL
	 */
	@Nonnull
	public abstract String getRedirectURL();

	/**
	 * Returns the user/session-specific state for the current OAuth2 flow. Used to prevent replay attacks. Logic
	 * for making it user/session specific is the responsibility of the implementing class.
	 *
	 * @return Optionally the state.
	 */
	@Nonnull
	public abstract Option<String> getState();

	/**
	 * Sets the user/session-specific state for the current OAuth2 flow. Used to prevent replay attacks. Logic for making it
	 * user/session specific is the responsibility of the implementing class.
	 * @param state The state to set. This should be a unique non-guessable randomly generated String
	 */
	public abstract void setState(String state);

	/**
	 * Sets the path prefix for the current application
	 * @param prefix The prefix
	 */
	public final void setPathPrefix(String prefix) {
		String sanitized = prefix;

		if (!sanitized.isEmpty() && !sanitized.endsWith("/")) {
			sanitized = sanitized.concat("/");
		}
		while (sanitized.startsWith("/")) {
			sanitized = sanitized.substring(1);
		}

		this.pathPrefix = sanitized;
	}

	/**
	 * Callback method for errors
	 * @param errorMessage The error that occurred
	 */
	public abstract void onError(@Nonnull String errorMessage);

	/**
	 * Creates a verifier class for Google ID tokens
	 * @return The IdTokenVerifier
	 */
	public final GoogleIdTokenVerifier createTokenVerifier() {
		return new GoogleIdTokenVerifier(HTTP_TRANSPORT, JSON_FACTORY);
	}

	/**
	 * Refreshes the given token
	 * @param credential The credential to refresh
	 * @param clientSecrets The client identification data
	 * @return A TypedResult containing either the token response, or the reason why the refresh failed
	 */
	public final TypedResult<GoogleTokenResponse> refreshToken(GoogleCredential credential,
															   GoogleClientSecrets clientSecrets) {
		Details details = clientSecrets.getDetails();
		
		GoogleRefreshTokenRequest req = new GoogleRefreshTokenRequest(
				HTTP_TRANSPORT, JSON_FACTORY, credential.getRefreshToken(),
				details.getClientId(),
				details.getClientSecret());
		
		try {
			return TypedResult.ok(req.execute());
		} catch (IOException e) {
			return TypedResult.fail(e.getMessage());
		}

	}

	/**
	 * Callback method for follow-up actions after a successful authorization has been performed
	 * @param tokenResponse The token response
	 * @param idToken The ID token
	 */
	public abstract void onAuthorized(GoogleTokenResponse tokenResponse,
			GoogleIdToken idToken);

}
