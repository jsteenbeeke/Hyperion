package com.jeroensteenbeeke.hyperion.social;

import org.apache.wicket.protocol.http.WebApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.jeroensteenbeeke.hyperion.social.beans.youtube.YouTubeHandler;
import com.jeroensteenbeeke.hyperion.social.web.pages.youtube.YouTubeCallbackPage;
import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationContextProvider;

/**
 * Initializer for YouTube integration
 */
public enum YouTube implements SocialMediumInitializer {
	/**
	 * Singleton instance
	 */
	integration;

	private static final Logger log = LoggerFactory.getLogger(YouTube.class);

	@Override
	public <T extends WebApplication & ApplicationContextProvider> void initialize(
			T application, String prefix) {
		log.info("Initializing YouTube support");
		ApplicationContext context = application.getApplicationContext();
		YouTubeHandler youtubeHandler = context.getBean(YouTubeHandler.class);
		
		youtubeHandler.setPathPrefix(prefix);

		String contextRelativeUrl = prefix.concat("/youtube/callback");
		application.mountPage(contextRelativeUrl, YouTubeCallbackPage.class);

		log.info("Mounted YouTube callback at {}", contextRelativeUrl);

	}
}
