package com.jeroensteenbeeke.hyperion.social;

import org.apache.wicket.protocol.http.WebApplication;

import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationContextProvider;

/**
 * Initializer class. Defines method signature for initializers that can easily be integrated into Wicket applications.
 */
public interface SocialMediumInitializer {
	/**
	 * Initialize the given WebApplication for use with this social medium
	 * @param application The application to initialize
	 * @param prefix The prefix to use for callback URLs for OAuth
	 * @param <T> The type of application
	 */
	<T extends WebApplication & ApplicationContextProvider> void initialize(
			T application, String prefix);
}
